
shyft.dashboard.apps
====================


.. automodule:: shyft.dashboard.apps
   :members:
   :undoc-members:
   :show-inheritance:

**Submodules**


.. toctree::

   shyft.dashboard.apps.start_bokeh_apps

**Subpackages**

.. toctree::

   shyft.dashboard.apps.dtss_viewer
