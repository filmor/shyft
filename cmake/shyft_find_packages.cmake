include_guard()
include(FindPackageMessage)

find_package(fmt REQUIRED)

find_package(dlib REQUIRED)
if(dlib_FOUND)
    get_target_property(dlib_includes dlib::dlib INTERFACE_INCLUDE_DIRECTORIES)
    get_target_property(dlib_configs dlib::dlib IMPORTED_CONFIGURATIONS)
    foreach(dlib_config ${dlib_configs})
        get_target_property(dlib_location_${dlib_config} dlib::dlib IMPORTED_LOCATION_${dlib_config})
        list(APPEND dlib_locations ${dlib_location_${dlib_config}})
    endforeach()
    find_package_message(dlib "Found dlib: ${dlib_locations} (found version \"${dlib_VERSION}\")" "[${dlib_includes}][${dlib_locations}][v${dlib_VERSION}()]")
endif()

find_package(Armadillo REQUIRED)

if (NOT DEFINED OPENSSL_ROOT_DIR)
    if (DEFINED ENV{CONDA_PREFIX})
        # If conda, then try to use ssl from the conda env.
        set(OPENSSL_ROOT_DIR $ENV{CONDA_PREFIX})
    endif()
endif()
find_package(OpenSSL REQUIRED)

find_package(leveldb REQUIRED)
if(leveldb_FOUND)
    get_target_property(leveldb_includes leveldb::leveldb INTERFACE_INCLUDE_DIRECTORIES)
    get_target_property(leveldb_configs leveldb::leveldb IMPORTED_CONFIGURATIONS)
    foreach(leveldb_config ${leveldb_configs})
        get_target_property(leveldb_location_${leveldb_config} leveldb::leveldb IMPORTED_LOCATION_${leveldb_config})
        list(APPEND leveldb_locations ${leveldb_location_${leveldb_config}})
    endforeach()
    find_package_message(leveldb "Found LevelDB: ${leveldb_locations} (found version \"${leveldb_VERSION}\")" "[${leveldb_includes}][${leveldb_locations}][v${leveldb_VERSION}()]")
endif()

include(${CMAKE_CURRENT_LIST_DIR}/python_boost.cmake)

if (SHYFT_WITH_PYTHON)
    find_python_in_path()
    set(BOOST_PYTHON_VERSION ${python_version} CACHE INTERNAL "Version of Python that Boost.Python is built with")
endif()

set(BOOST_VERSION 1.76.0 CACHE STRING "Boost version to use")

find_boost_stuff()

if (MSVC)
    find_library(math_lib mkl_rt) # we use mkl, from conda pgk, ref. win_build_dependencies.sh
    if (math_lib)
        find_package_message(math_lib "Found Intel MKL: ${math_lib}" "[${math_lib}]")
    else()
        # for somewhat bw compat:
        find_library(math_lib libopenblas)
        if(math_lib)
            find_package_message(math_lib "Found OpenBLAS: ${math_lib}" "[${math_lib}]")
        else()
            message(WARNING "Did not find intel or openblas, incomplete dependencies for shyft on win")
        endif()
    endif()
endif()

set(arma_libs ${ARMADILLO_LIBRARIES} ${math_lib})

if(SHYFT_WITH_TESTS)
  find_package(doctest REQUIRED)
endif()

if(SHYFT_WITH_ENERGY_MARKET)
    # Required Qt components for ui export
    find_package(Qt5 COMPONENTS Widgets Charts REQUIRED HINTS /usr)
    if(SHYFT_WITH_SHOP)
        include(find_shop_license)

    endif()
endif()
