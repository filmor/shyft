find_program(CCACHE_FOUND ccache)
if(CCACHE_FOUND)
    set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE ccache)
endif(CCACHE_FOUND)

set(CMAKE_CXX_STANDARD 20)
# need some more work in code before we can set this
#set(CMAKE_CXX_VISIBILITY_PRESET hidden)

set(CMAKE_POSITION_INDEPENDENT_CODE ON)
set(THREADS_PREFER_PTHREAD_FLAG ON)

include_directories(${CMAKE_SOURCE_DIR}/cpp ${SHYFT_DEPENDENCIES}/include)

add_library(shyft_private_flags INTERFACE)
target_compile_features(shyft_private_flags INTERFACE cxx_std_${CMAKE_CXX_STANDARD})
target_compile_definitions(shyft_private_flags INTERFACE $<$<CONFIG:Debug>:SHYFT_DEBUG>)

add_library(shyft_public_flags INTERFACE)
target_compile_features(shyft_public_flags INTERFACE cxx_std_${CMAKE_CXX_STANDARD})
# note the BOOST_PHOENIX_STL_TUPLE_H_ is to avoid problem in 1.81 that caused multiple def sym at link time
target_compile_definitions(shyft_public_flags INTERFACE $<$<CONFIG:Debug>:SHYFT_DEBUG>
        ARMA_NO_DEBUG
        BOOST_PHOENIX_STL_TUPLE_H_
        BOOST_ALLOW_DEPRECATED_HEADERS BOOST_PYTHON_PY_SIGNATURES_PROPER_INIT_SELF_TYPE 
        BOOST_BIND_GLOBAL_PLACEHOLDERS BOOST_BEAST_SEPARATE_COMPILATION 
        BOOST_ERROR_CODE_HEADER_ONLY BOOST_VARIANT_MINIMIZE_SIZE 
        BOOST_MPL_CFG_NO_PREPROCESSED_HEADERS BOOST_MPL_LIMIT_LIST_SIZE=30 
        BOOST_MPL_LIMIT_VECTOR_SIZE=30)


if(MSVC)
    set(py_ext_suffix ".pyd") # Python extension use .pyd instead of .dll on Windows
    set(shyft_lib_type "STATIC")
    set(shyft_runtime "RUNTIME") #  when installing .dll/.pyd, requires special install to skip .lib on windows
    
    target_compile_options(shyft_public_flags INTERFACE -bigobj -MP4 -Zc:preprocessor)
    target_compile_definitions(shyft_public_flags INTERFACE
        _CRT_SECURE_NO_WARNINGS _SILENCE_ALL_CXX17_DEPRECATION_WARNINGS _WIN32_WINNT=0x0601 
        STRICT WIN32_LEAN_AND_MEAN NOMINMAX _HAS_DEPRECATED_IS_LITERAL_TYPE=1
        ARMA_DONT_USE_WRAPPER
    )

    # Apply patch from CMake 3.21.3 to support VS2022 toolset v143 when running with
    # older versions of CMake
    # (https://github.com/Kitware/CMake/blob/v3.21.3/Modules/Platform/Windows-MSVC.cmake#L70-L72).
    # Without this patch MSVC_TOOLSET_VERSION will have value 142 at this point,
    # and we simply overwrite it with 143 - without considering if actually trying
    # to build using previous toolset version 142 with VS2022.
    # Note that the CMake version included in VS2022 version 17.0.0 was labelled
    # "3.21.21080301-MSVC_2", and did not include the patch from CMake 3.21.3. Since
    # the version number is not directly comparable to standard CMake version
    # numbers, we currently include the patch for any 3.21 versions of CMake with
    # the "-MSVC" suffix, in addition to any standard CMake versions older than 3.21.3.
    if (CMAKE_VERSION MATCHES "^3\.21\.[0-9]+-MSVC" OR CMAKE_VERSION VERSION_LESS "3.21.3")
        if (MSVC_TOOLSET_VERSION EQUAL 142 AND MSVC_VERSION GREATER_EQUAL 1930)
            message(WARNING "Patching MSVC_TOOLSET_VERSION to value 143 instead of 142 due to MSVC_VERSION ${MSVC_VERSION} and CMAKE_VERSION ${CMAKE_VERSION}")
            set(MSVC_TOOLSET_VERSION 143)
        endif()
    endif()

else()
    if(MINGW)
        set(py_ext_suffix ".pyd")
        set(shyft_lib_type "STATIC") # need more work to make it shared libs
        set(shyft_runtime "RUNTIME") #  when installing .dll/.pyd, requires special install to skip .lib on windows
    else()
        set(py_ext_suffix ".so")  # linux/apple uses these
        set(shyft_runtime "")     # installing py runtime requires no special action on linux
        set(shyft_lib_type "SHARED")
    endif()
    if (NOT ${BOOST_VERSION} MATCHES "1.75.0" OR ${CMAKE_SYSTEM} MATCHES "el7.x86_64$" OR ${CMAKE_SYSTEM} MATCHES "el8.x86_64$" )
        target_compile_options(shyft_public_flags INTERFACE -Wno-deprecated-declarations) # RHEL compilers are older and generates a lot of noise
    else()
        target_compile_options(shyft_public_flags INTERFACE -Wno-deprecated-declarations -Wall -Wextra)  # for fresh arch, this should work and keep us clean
    endif()
endif()

install(
  TARGETS shyft_public_flags shyft_private_flags
  EXPORT shyft-targets
  LIBRARY DESTINATION ${SHYFT_INSTALL_LIBDIR}
  ARCHIVE DESTINATION ${SHYFT_INSTALL_LIBDIR}
  RUNTIME DESTINATION ${SHYFT_INSTALL_BINDIR})
