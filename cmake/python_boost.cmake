
#
# Function find_python_in_path
#
# execute python -c to find python, and extract variables using python modules
#
# on return export to parent scope:
#
# python_include
# python_lib
# python_lib_path
# python_numpy_include
# python_version (ex 3.6 , 3.7 etc.)
#
function(find_python_in_path)
    if (WIN32)
        # Executable name as used in conda.
        set(py_exe_name "python")
        # Get full path to executable by searching standard system environment variable
        # PATH, and only that, using find_program with all NO_... search options except
        # NO_SYSTEM_ENVIRONMENT_PATH.
        # (On Linux we can just call execute_process with executable name and it will
        # search PATH only, but on Windows it will search current application directory,
        # working directory, windows directories, and only as a last resort the PATH variable.
        # This means if there is a python.exe in the same directory as cmake.exe, which is
        # the case in a mingw environment, then execute_process with only the executable name
        # as command will always pick that one without ever considering PATH, and this could
        # be different from what will be used when running the executable outside of cmake.)
        find_program(py_exe_path ${py_exe_name} NO_PACKAGE_ROOT_PATH NO_CMAKE_PATH NO_CMAKE_ENVIRONMENT_PATH NO_CMAKE_SYSTEM_PATH NO_CMAKE_FIND_ROOT_PATH)
        if (NOT py_exe_path)
            message(FATAL_ERROR "Unable to find python executable in PATH")
        endif()
        # Move value from cache variable py_exe_path into normal variable py_exe so that a
        # a new search will be performed every time like in else case below (or could we
        # just as well keep it in cache?)
        set(py_exe ${py_exe_path})
        unset(py_exe_path CACHE)
    else()
        set(py_exe "python3")
    endif()

    execute_process(
        COMMAND ${py_exe} "-c" "from sysconfig import (get_paths,get_python_version);p=get_paths();import numpy as  np;cfg= ';'.join([p['include'],p['stdlib'],np.get_include(),get_python_version()]);print(cfg)"
        RESULT_VARIABLE rv
        OUTPUT_VARIABLE py_cfg
        ERROR_VARIABLE  py_cfg_error
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    if (rv)
        message(FATAL_ERROR "Errors occurred trying to exec ${py_exe}: ${py_cfg_error}")
        return()
    endif()
    list(GET py_cfg 0 py_include)
    list(GET py_cfg 1 py_lib_path)
    list(GET py_cfg 2 py_numpy_include)
    list(GET py_cfg 3 py_version)

    #message(STATUS " py_cfg: " ${py_cfg})
    #message(STATUS " py_include: ${py_include}")
    #message(STATUS " py_lib_path: ${py_lib_path}")
    #message(STATUS " py_numpy_include: ${py_numpy_include}")
    #message(STATUS " py_version: ${py_version}")

    set(python_include ${py_include} PARENT_SCOPE)
    set(python_lib_path ${py_lib_path} PARENT_SCOPE)
    set(python_numpy_include ${py_numpy_include} PARENT_SCOPE)
    set(python_version ${py_version} PARENT_SCOPE)
    string(REPLACE "." "" py_ver "${py_version}")
    
    if (MSVC)
        set(python_lib "${py_lib_path}\\..\\libs\\python${py_ver}.lib" PARENT_SCOPE)
    elseif (MINGW)
        set(python_lib "${py_lib_path}/../libpython${py_version}.dll.a" PARENT_SCOPE)
    elseif (${CMAKE_SYSTEM_NAME} STREQUAL "Linux")
        if (EXISTS "${py_lib_path}/../libpython${py_version}m.so")
            set(python_lib "${py_lib_path}/../libpython${py_version}m.so" PARENT_SCOPE)
        elseif ( EXISTS "${py_lib_path}/../libpython${py_version}.so")
            set(python_lib "${py_lib_path}/../libpython${py_version}.so" PARENT_SCOPE)
        else()
            set(python_lib "" PARENT_SCOPE)
        endif()
    elseif (${CMAKE_SYSTEM_NAME} STREQUAL "Darwin")
        set(python_lib "${py_lib_path}/../libpython${py_version}.dylib" PARENT_SCOPE)
    endif()
endfunction()

#
# Function find_boost_stuff
#
# Try to find suitable boost libraries, for a pre-defined list of components;
# serialization, filesystem, system, thread, atomic, and optionally also
# python (see below).
#
#
# Preconditions:
#
# BOOST_VERSION must be be set to minimum required version, e.g. 1.71.
#
# If BOOST_PYTHON_VERSION is set, then also include the python component
# according to the specified version. E.g. if set to value 3.7 then
# boost python component python37 will be included. It is then also assumed
# that the python library itself, for a matching version of python, will
# also be linked, i.e. variable python_lib from a call to find_python_in_path,
# e.g. python36 (win), or libpython-....so (linux).
#
#
# Output (variables set in parent_scope):
#
# boost_link_libraries containing link targets.
# boost_py_link_libraries, if BOOST_PYTHON_VERSION was set, containing
# the same as boost_link_libraries plus the boost python target.
#
macro(find_boost_stuff)

    # Boost can be found using the find module provided by the CMake installation, or
    # a package configuration files bundled with Boost. CMake will use the package
    # configuration files if present, unless option Boost_NO_BOOST_CMAKE is set.
    # One difference is that, while the built-in find module always looks for shared
    # libraries by default, using the package configuration file will prefer static
    # libraries on Windows. In shyft we currently use shared libraries, and therefore
    # force this regardless of the default.
    set(Boost_USE_STATIC_LIBS OFF)

    # OLD: Using find module.
    # Starting with version 1.70, CMake package configuration files are included in the Boost
    # build output, even when not building it using the newer boost-cmake project. When
    # building shyft, CMake then prefers using the package configuration files for finding boost,
    # but for a long time that did not work properly, typically it did not find boost_python,
    # and therefore we forced CMake to use its built-in find module instead, by enabling
    # Boost_NO_BOOST_CMAKE. But since updates of the built-in find module is typically
    # behind on boost versions supported, at least for older CMake versions as the one
    # bundled with Visual Studio, we then also needed to enable support for newer Boost
    # versions by setting additional variables. The result was the following:
    #    set(Boost_NO_BOOST_CMAKE TRUE)
    #    set(Boost_NO_WARN_NEW_VERSIONS ON)
    #    set(Boost_ADDITIONAL_VERSIONS "1.74" "1.74.0" "1.75" "1.75.0" "1.76" "1.76.0" "1.77" "1.77.0" "1.78" "1.78.0" "1.79" "1.79.0" "1.80" "1.80.0") # Assuming minimum CMake version is 3.19, add support for Boost 1.74 (first known in CMake 3.19.2), 1.75 (CMake 3.19.5), 1.76 (CMake 3.20.3), 1.77 (CMake 3.22.1), 1.78 (CMake 3.22.2), 1.79 (CMake 3.23.2), 1.80 (unknown as of CMake 3.24.0)
    # For more details, see:
    #    https://gitlab.kitware.com/cmake/cmake/-/issues/19402
    #    https://gitlab.kitware.com/cmake/cmake/issues/19656

    if (BOOST_PYTHON_VERSION)
        # Include Boost.Python, for the specified version of Python.
        # Note that when using the find module, the component name and resulting
        # target is named "pythonXY", where XY is a suffix representing Python's
        # major and minor version numbers (without the "." separator).
        # When using the package configuration file, the component name and target
        # are named simply "python", and we are supposed to instead set variable
        # Boost_PYTHON_VERSION to the wanted version number (in regular format "X.Y"),
        # if there is a need to specify one of several Boost.Python libraries present.
        # For backwards compatibility it accepts component name in the same "pythonXY"
        # format as the find module, and it creates an alias target with the same
        # name (which simply defines the main python target as an interface link
        # dependency), so we just use that for now to support both.
        string(REPLACE "." "" b_python_suffix "${BOOST_PYTHON_VERSION}")
        set(boost_py_pkg python${b_python_suffix})
    endif()

    find_package(Boost ${BOOST_VERSION} COMPONENTS ${boost_py_pkg} serialization filesystem system thread atomic chrono REQUIRED)

    set(boost_link_libraries Boost::disable_autolinking Boost::headers Boost::system Boost::filesystem Boost::serialization Boost::thread Boost::atomic Boost::chrono)

    if (BOOST_PYTHON_VERSION)
        # Add Python-specific variable, containing the same as boost_link_libraries
        # plus the boost python target. As noted above, the target naming is
        # different from the find module and the package configuration file.
        # The package config creates an alias target with version suffix matching
        # what the name of the target from the find module. But we are going to
        # to retrieve the library path using the location property later, and this
        # is only possible on the "real" target, so we make sure we use that one.
        get_target_property(b_lib Boost::python${b_python_suffix} LOCATION)
        if (b_lib)
            set(boost_py_link_libraries ${boost_link_libraries} Boost::python${b_python_suffix})
        else()
            set(boost_py_link_libraries ${boost_link_libraries} Boost::python)
        endif()
    endif()
endmacro()
