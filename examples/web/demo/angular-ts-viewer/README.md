# AngularTsViewer

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.1.

The purpose of this demo is to explore the possibilities using the dtss web_api.

The application is very simple:

Present a search-box input field, 
when user types in text, 
 then forward the text to the dtss web_api .find method.
 When the .find method returns, present the matching list.
 
 When the user select time-series in the list, forward that item to the ts-viewer.
 
 When the ts-view receives a new time-series, 
 then forward a read-request to the dtss_web_api,
 When the response is received,
 then render the time-series in highcharts.
 
 Developer setup:
 1. install npm into your distro
 2. use npm to update the dependencies for the project
 ```bash
 npm update
 ```
 
 
 To present/illustrate a the tabular view, 
 we use ngx-datatable:

 

 
 Optional (TODO:) when user changes view-period/zoom or resolution, 
                  then forward a new average (time-axis..) request
                  when the new response is received, then
                  render with new resolution. 
                  

## Development server


Run  `ng build --watch` to build the project, and keep building it when you change the source.

The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

We use shyft dtss also for serving the static content from the `dist/` directory.
 
To start the shyft dtss web-api, that includes setting up some test-data, sine-shaped  time-series, named ts1,ts2...

Run `python create_mock_time_series.py` for a dev server. Navigate to `http://localhost:7777/`. 

When you change the source-files, the build -watch will automatically rebuild, then refresh the browser to get the most recent version.


## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
