from shyft.energy_market import ui
from shyft import time_series as sa
from layout_callbacks import LayoutCallbacks
import argparse, time
from PySide2.QtWidgets import QApplication

# Create a QApplication singleton first
app = QApplication([])


class LayoutService:
    def __init__(self, port_number: int, doc_root: str, api_port_num: int):
        print(f"doc_root: {doc_root}")
        self.srv = ui.LayoutServer(doc_root)
        self.srv.set_listening_port(port_number)
        self.srv.fx = LayoutCallbacks()
        self.client = None
        self.doc_root = doc_root
        self.api_port = api_port_num

    def __enter__(self):
        self.srv.start_server()
        self.srv.start_web_api("0.0.0.0", self.api_port, self.doc_root + "/web", 1, 1)
        self.client = ui.LayoutClient(host_port=f"localhost:{self.srv.get_listening_port()}", timeout_ms=1000)
        print(f"Started LayoutServer with web API on port {self.srv.get_listening_port()} ({self.api_port} for web API)")
        return self.client

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.srv.stop_web_api()
        self.srv.stop_server()
        self.client.close()
        print(f"Stopped LayoutServer and -Client")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Argument parser for start Layout service")
    parser.add_argument('--port-num', metavar="port_num", type=int, nargs="+",
                        help="Port number for server", default=4444)
    parser.add_argument('--doc-root', metavar='doc_root', type=str, nargs='+',
                        help="Document root to serve layouts", default="")
    parser.add_argument('--web-port', metavar='web_port', default=4445,
                        help="Port number for web API", type=int, nargs='+')

    args = parser.parse_args()

    with LayoutService(args.port_num, args.doc_root[0], args.web_port) as client:
        # Add a simple Layout
        li = ui.LayoutInfo(1, "simple", '{"a": "some value", "b": {"ba": "nested", "bb": [1,2,3]}}')
        mi = ui.ModelInfo(li.id, li.name, sa.utctime_now(), "")
        client.store_model(li, mi)
        print("Added a simple layout to LayoutServer")
        while True:
            time.sleep(1)

