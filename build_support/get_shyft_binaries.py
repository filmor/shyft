from pathlib import Path
import shutil
import os
import sys
import platform


try:
    import shyft.api as sa

except ModuleNotFoundError:
    print("Use pip/conda to install shyft with your interpreter")
    print("pip install shyft")
    print("conda install shyft")
    sys.exit(1)

# Your cloned shyft repository
shyft_repository = Path(__file__).resolve().parent.parent.parent

# Shyft installed in your interpreters site-packages
shyft_api_path = Path(sa.__file__)
shyft_root = shyft_api_path.parent.parent

# Look for SHYFT_WORKSPACE environment variable
try:
    shyft_repo = Path(f"{os.environ['SHYFT_WORKSPACE']}/shyft")
except KeyError:
    os.environ["SHYFT_WORKSPACE"] = str(shyft_repository)
    print(f"SHYFT_WORKSPACE not defined, set to {os.environ['SHYFT_WORKSPACE']}")
    shyft_repo = Path(f"{os.environ['SHYFT_WORKSPACE']}/shyft")

# Copy binaries from site-packages to local repo
if platform.system() == "Linux":
    for i in shyft_root.glob('**/*.*'):
        if ".so" in i.name:
            source_file = i.as_posix()
            dest_file = shyft_repo / "python" / i.relative_to(shyft_root.parent)
            dest_file.parent.mkdir(parents=True, exist_ok=True)
            print(source_file)
            print(dest_file)
            shutil.copy(str(source_file), str(dest_file))

if platform.system() == "Windows":
    for i in shyft_root.glob('**/*.*'):
        if ".pyd" or ".dll" in i.name:
            source_file = i.as_posix()
            dest_file = shyft_repo / "python" / i.relative_to(shyft_root.parent)
            dest_file.parent.mkdir(parents=True, exist_ok=True)
            shutil.copy(str(source_file), str(dest_file))
print("Finished fetching binaries")
