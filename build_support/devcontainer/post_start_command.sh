#!/bin/bash
echo "Welcome to Shyft open source dev container!"
if [[ -x ./.devcontainer/devcontainer_init.sh ]]; then
    echo "execute devcontainer_init.sh"
    bash -x .devcontainer/devcontainer_init.sh
else
    echo "In case you need to customize startup, add .devcontainer/devcontainer_init.sh"
fi;
