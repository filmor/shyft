/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <shyft/time_series/common.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/time_series/convolve_policy.h>

namespace shyft::time_series {
    using namespace shyft::core;
    using std::string;
    using std::vector;
    using std::runtime_error;

    /** @brief convolve_w convolves a time-series with weights w
    *
    * The resulting time-series value(i) is the result of convolution (ts*w)|w.size()
    *  value(i) => ts(i-k+a)*w(k), k is [0..w.size()>, a is {0,w.size()/2,w.size()}
    *
    * The convolve_policy determines kernel alignment, and how to resolve boundaries
    * where the kernel does not fully overlap the input ts.
    * The default is BACKWARD | USE_NEAREST @ref convolve_policy
    *
    * @tparam Ts any time-series
    *
    */
    template<class Ts>
    struct convolve_w_ts {
       using ta_t=typename Ts::ta_t;
       using W= std::vector<double>;
        Ts ts;
        ts_point_fx fx_policy=POINT_AVERAGE_VALUE;
        W w;
        convolve_policy policy = convolve_policy::BACKWARD | convolve_policy::USE_NEAREST;
        bool bound = false;
        //-- default stuff, ct/copy etc goes here
        convolve_w_ts() = default;

        //-- useful ct goes here
        template<class A_, class W_>
        convolve_w_ts(A_ && tsx, W_ && w,
                      convolve_policy policy = convolve_policy::BACKWARD | convolve_policy::USE_NEAREST)
            :ts(std::forward<A_>(tsx)),
            w(std::forward<W_>(w)),
            policy(policy) {
            // verify stuff that we can verify without touching ts (possibly unbound)
            if( policy & convolve_policy::CENTER ) {
              if(w.size() % 2 == 0) {
                throw std::runtime_error("convolve_w_ts: kernel must not be even-sized for convolve_policy::CENTER");
              }
            }
            if ( ! e_needs_bind(d_ref(ts)) ) {
                local_do_bind();
                // as early as possible, so we do the test here in case of bound ts.
                if(policy & convolve_policy::CENTER && w.size() > ts.size())  {
                    throw std::runtime_error("convolve_w_ts: kernel size must not exceed ts size for convolve_policy::CENTER");
                }
            }
        }
        void do_bind() {
            ts.do_bind();
            local_do_bind();
        }
        void do_unbind() {
          ts.do_unbind();
          local_do_unbind();
        }
        bool needs_bind() const {
            return ! bound;
        }

        void local_do_bind() {
            if ( ! bound ) {
                fx_policy=d_ref(ts).point_interpretation();
                bound = true;
            }
        }
        void local_do_unbind() {
          if(bound) {
            bound=false;
          }
        }

        const ta_t& time_axis() const { return ts.time_axis(); }
        ts_point_fx point_interpretation() const { return fx_policy; }
        void set_point_interpretation(ts_point_fx point_interpretation) { fx_policy = point_interpretation; }

        point get(size_t i) const { return point(ts.time(i), value(i)); }

        size_t size() const { return ts.size(); }
        size_t index_of(utctime t) const { return ts.index_of(t); }
        utcperiod total_period() const { return ts.total_period(); }
        utctime time(size_t i) const { return ts.time(i); }

        //--
        double value(size_t i) const {
            if(policy & convolve_policy::CENTER && w.size() > ts.size())  {
                    throw std::runtime_error("convolve_w_ts: kernel size must not exceed ts size for convolve_policy::CENTER");
            }
            double v = 0.0;
            for (size_t j = 0;j<w.size();++j)
                v +=
                  policy & convolve_policy::FORWARD ?
                    //Forward looking kernel, boundary handling at the end of ts
                    i+w.size() < ts.size()+1+j ?
                      w[j] * ts.value(i+w.size()-1 - j) //Filter is inside ts
                    :
                      policy & convolve_policy::USE_NEAREST ? w[j] * ts.value(ts.size()-1) :
                      policy & convolve_policy::USE_ZERO ? 0.0 : nan
                  :
                    policy & convolve_policy::CENTER ?
                      // Centered kernel, boundary handling in both ends
                      j <= i+w.size()/2 && i+w.size()/2 < ts.size()+j ?
                        w[j] * ts.value(i+w.size()/2-j) // Filter is inside ts
                      :
                        j > i+w.size()/2 ? // boundary handling in the beginning
                          policy & convolve_policy::USE_NEAREST ? w[j] * ts.value(0) :
                          policy & convolve_policy::USE_ZERO ? 0.0 : nan
                        : // boundary handling in the end
                          policy & convolve_policy::USE_NEAREST ? w[j] * ts.value(ts.size()-1) :
                          policy & convolve_policy::USE_ZERO ? 0.0 : nan
                    :
                      //Backward looking kernel, boundary handling at the beginning of ts
                      j <= i ?
                        w[j] * ts.value(i - j) //Filter is inside ts
                      :
                        policy & convolve_policy::USE_NEAREST ? w[j] * ts.value(0) :
                        policy & convolve_policy::USE_ZERO ? 0.0 : nan
                  ;
            return  v;
        }
        double operator()(utctime t) const {
            return value(ts.index_of(t));
        }
        x_serialize_decl();
    };
    template<class TS> struct is_ts<convolve_w_ts<TS>> { static const bool value = true; };
    template<class TS> struct is_ts<shared_ptr<convolve_w_ts<TS>>> { static const bool value = true; };

}
//-- serialization support
x_serialize_export_key(shyft::time_series::convolve_w_ts<shyft::time_series::point_ts<shyft::time_axis::fixed_dt>>);
x_serialize_export_key(shyft::time_series::convolve_w_ts<shyft::time_series::point_ts<shyft::time_axis::generic_dt>>);
