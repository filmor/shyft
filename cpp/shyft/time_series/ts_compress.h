/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <vector>
#include <cmath>
#include <stdexcept>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>

namespace shyft::time_series {
    using std::vector;
    using std::string;
    using std::to_string;
    using std::isfinite;
    //using std::nan;
    using std::runtime_error;
    using shyft::core::utctime;
    using shyft::core::utctimespan;
    
    /** @brief a simple delta_compression algorithm
        * walk through a numeric vector, and make one callback for
        * each break-point that must be represented in a compressed form
        * based on accuracy and nan->number number->nan occurrences.
        *
        * The algorithm will make one delta_cb(value,i,i_end)
        * for each range found.
        * zero call-backs for 0 size
        * at most n callbacks for n-values (all very different values)
        */
    template <class T, class DeltaFx>
    inline void delta_compression(vector<T> const & v, T const accuracy, DeltaFx && delta_cb) {
        if (v.size() == 0)
            return ;
        size_t a = 0;// a for anchor
        bool finite_sequence = isfinite(v[a]);
        for (size_t i = 1;i < v.size();++i) {
            auto x = v[i];
            bool x_finite = isfinite(x);
            if (finite_sequence) {
                if (!x_finite || std::abs(v[a] - x)> accuracy) { // number... nan || number ...very-different-number
                    delta_cb(v[a],a,i);
                    a = i;
                    finite_sequence = x_finite;
                } // else number.. very-equal-number sequence
            } else {
                if (x_finite) { // nan .. number, stop sequence
                    delta_cb(v[a], a, i);
                    a = i;
                    finite_sequence = true;
                }// else nan-nan-nan sequence, just skip
            }
        }
        if (a < v.size())
            delta_cb(v[a], a, v.size());
    }

    // ts_compress_size(vector<T> v) -> size_t  
    //   used to determine if compression is smart. must be extremely fast.
    template<class T> 
    inline size_t ts_compress_size(const vector<T>  & v, const T accuracy) {
        size_t count = 0;
        delta_compression<T>(v, accuracy, [&count](T, size_t, size_t) {++count;});
        return count;
    }

    // ts_compress(ts_point<time_axis::fixed> ts) -> ts_point<time_axis::points>
    //   used to switch from fixed_dt repr. to ts_point repr.
    template<class TS,class VT=double,class TA=time_axis::generic_dt>
    inline TS ts_compress(const TS &ts, const VT accuracy) {
        vector<utctime> t;
        vector<VT> v;
        t.reserve(ts.size()+1);// worst case scenario reserve
        v.reserve(ts.size());
        delta_compression<VT>(ts.values(), accuracy, 
            [&t,&v, &ts](VT vt, size_t b, size_t /*e*/) {
                v.push_back(vt);  // save the break-point value
                t.push_back(ts.time_axis().period(b).start); // and the time-point
            }
        );
        if(v.size())
                t.push_back(ts.time_axis().period(ts.size()-1).end);

        return TS(TA{std::move(t)},v,ts.point_interpretation());
    }
#if 0
    // ts_decompress(ts_point<time_axis::points> ts,delta_t) -> ts_point<time_axis::fixed>
    //   used to switch from point repr to fixed repr
    template<class TS, class VT=double>
    inline TS ts_decompress(const TS &ts, utctimespan dt) {
        using ta_t= typename TS::ta_t;
        size_t n = ts.time_axis.size_in(dt);
        if (n > 0) {
            TS r(ta_t{ts.time_axis.period(0).start,dt,n},nan,ts.point_interpretation());
            size_t j=0;
            utctime t = r.time_axis.start;
            utctime t_end = ts.time_axis.period(0).end;
            for (size_t i = 0;;) {
                r.values[j++]=ts.values[i];// could be improved using std::copy(range, dest..)
                t += dt;
                if (t == t_end) { // if we reach exact time boundary
                    ++i;                       // skip to next section
                    if (i >= ts.time_axis.size())
                        break;
                    t_end = ts.time_axis.period(i).end;
                } else if (t > t_end) // if unaligned, then not supported
                    throw runtime_error(     // because we want loss-less conversion
                        "ts_decompress en-countered non-aligned time-point " + to_string(t.count())
                        + " vs. " + std::to_string(t_end.count())
                    );
            }
            return r;
        } else {
            return TS{};
        }
    }
#endif

}
