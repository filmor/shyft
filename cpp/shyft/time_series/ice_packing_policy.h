/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

namespace shyft::time_series {
    enum class ice_packing_temperature_policy {
        DISALLOW_MISSING,  ///< The result is NaN when temperature values are missing.
                           /// Missing values are both explicit NaN temperature values,
                           /// and at the beginning of the series where the scan window expend
                           /// past the start of the temperature series.
        ALLOW_INITIAL_MISSING,  ///< The result is NaN whenever explicit NaN temperature values are encountered.
                                /// However, the result is _not_ NaN at the beginning of the series where
                                /// the scan window extend past the end of the series.
        ALLOW_ANY_MISSING,  ///< The result is only NaN when all temperature values in a scan window are missing or NaN.
    };

}
