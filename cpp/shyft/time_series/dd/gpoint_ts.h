/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once
#include <shyft/time_series/dd/ipoint_ts.h>

namespace shyft::time_series::dd {// dd= dynamic_dispatch version of the time_series library, aiming at python api

/**
 * @brief gpoint_ts a generic concrete point_ts, a terminal, not an expression
 *
 * @details
 * The gpoint_ts is typically provided by repositories, that reads time-series
 * from some provider, like database, file(netcdf etc), providing a set of values
 * that are aligned to the specified time-axis.
 *
 */
struct gpoint_ts:ipoint_ts {
    gts_t rep;
    // To create gpoint_ts, we use const ref, move ct wherever possible:
    gpoint_ts(gts_t&&x):rep(move(x)){};
    gpoint_ts(const gts_t&x):rep(x){}
    // note (we would normally use ct template here, but we are aiming at exposing to python)
    gpoint_ts(const gta_t&ta,double fill_value,ts_point_fx point_fx=POINT_INSTANT_VALUE):rep(ta,fill_value,point_fx){}
    gpoint_ts(const gta_t&ta,const vector<double>& v,ts_point_fx point_fx=POINT_INSTANT_VALUE):rep(ta,v,point_fx) {}
    gpoint_ts(gta_t&&ta,double fill_value,ts_point_fx point_fx=POINT_INSTANT_VALUE):rep(move(ta),fill_value,point_fx){}
    gpoint_ts(gta_t&&ta,vector<double>&& v,ts_point_fx point_fx=POINT_INSTANT_VALUE):rep(move(ta),move(v),point_fx) {}
    gpoint_ts(const gta_t& ta,vector<double>&& v,ts_point_fx point_fx=POINT_INSTANT_VALUE):rep(ta,move(v),point_fx) {}

    // now for the gpoint_ts it self, constructors incl. move
    gpoint_ts() = default; // default for serialization conv
    // implement ipoint_ts contract:
    ts_point_fx point_interpretation() const override {return rep.point_interpretation();}
    void set_point_interpretation(ts_point_fx point_interpretation) override {rep.set_point_interpretation(point_interpretation);}
    const gta_t& time_axis() const override {return rep.time_axis();}
    utcperiod total_period() const override {return rep.total_period();}
    size_t index_of(utctime t) const override {return rep.index_of(t);}
    size_t size() const override {return rep.size();}
    utctime time(size_t i) const override {return rep.time(i);};
    double value(size_t i) const override {return rep.v[i];}
    double value_at(utctime t) const override {return rep(t);}
    vector<double> values() const override {return rep.v;}

    // implement some extra functions to manipulate the points
    void set(size_t i, double x) {rep.set(i,x);}
    void fill(double x) {rep.fill(x);}
    void scale_by(double x) {rep.scale_by(x);}
    gpoint_ts slice(int i0, int n) const  { return rep.slice(i0, n); }
    bool needs_bind() const override { return false;}
    void do_bind()  override {}
    void do_unbind() override {}
    gts_t & core_ts() {return rep;}
    const gts_t& core_ts() const {return rep;}
    ipoint_ts_ref evaluate(eval_ctx& ctx, ipoint_ts_ref const& shared_this) const override;
    shared_ptr<ipoint_ts> clone_expr() const override;
    void prepare(eval_ctx&ctx) const override;
    string stringify() const override;
    x_serialize_decl();
};

/** const cast shared_ptr to a mutable reference */
inline gpoint_ts& dref(shared_ptr<const gpoint_ts> const&ts) {return *const_cast<gpoint_ts*>(ts.get());}

}
x_serialize_export_key(shyft::time_series::dd::gpoint_ts);
