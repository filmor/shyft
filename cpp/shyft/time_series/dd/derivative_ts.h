/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once
#include <shyft/time_series/dd/ipoint_ts.h>

namespace shyft::time_series::dd {// dd= dynamic_dispatch version of the time_series library, aiming at python api
struct apoint_ts;//fwd
/**
* @brief The derivative_ts is used for providing ts derivate values over a time-axis
*
* @details
* Given a any ts, concrete, or an expression, provide the estimated 1'th order derivate on the
* intervals as provided by the time-series time-axis.
*
* The estimated 1'st order derivate is calculated based on the assumption that
* the f(t) represented of the time-series is and  estimate for the underlying signal.
*
* stair-case:
*    Then the signal f(t) is assumed to be the true-average over the non-nan area of the interval
*    and the 1'st order derivate estimate is created using a straight line between the
*    midpoint of each stair-case segment.
*
*   This can lead to anomalies a the beginning and the end of the derived time-series,
*    since the neighbouring points after and before the beginning and the end _might_
*    change the value, if they later are added into the computation source.
*
*
*  linear-between-points:
*     The signal f(t) is assumed to have a best estimated value equal to the straight line between the points
*     The 1'st order derivate is thus simply the 1'st order derivate of this straight line.
*     The resulting time-axis is exactly equal to the source time-axis,
*     and the resulting point-interpretation is stair-case (assuming constant derivate estimate over the interval)
*
* using the f(t) interpretation of the supplied ts (linear or stair case).
*
* The @ref ts_point_fx is always POINT_AVERAGE_VALUE for the result ts.
*
* @note left and right hand side of intervals with nans assume constant value the last half segment.
*
*/
struct derivative_ts :ipoint_ts {
    ipoint_ts_ref ts;///< time-series to differentiate
    derivative_method dm;///<derivative_method specification
    // useful constructors
    derivative_ts(const apoint_ts& ats,derivative_method dm=derivative_method::default_diff) : ts(ats.ts),dm(dm) {}
    derivative_ts(apoint_ts&& ats,derivative_method dm=derivative_method::default_diff) :ts(move(ats.ts)),dm(dm) {}
    derivative_ts(ipoint_ts_ref const &ts,derivative_method dm=derivative_method::default_diff): ts(ts),dm(dm) {}
    // std copy ct and assign
    derivative_ts()=default;
    // implement ipoint_ts contract:
    ts_point_fx point_interpretation() const override { return ts_point_fx::POINT_AVERAGE_VALUE; }
    virtual void set_point_interpretation(ts_point_fx /* point_interpretation*/) override { ; }
    const gta_t& time_axis() const override { return ts->time_axis(); }
    utcperiod total_period() const override { return ts?time_axis().total_period():utcperiod{}; }
    size_t index_of(utctime t) const override { return ts?ts->index_of(t):string::npos; }
    size_t size() const override { return ts?ts->size():0; }
    utctime time(size_t i) const override { return ts->time(i); };
    double value(size_t i) const override;
    double value_at(utctime t) const override;
    virtual vector<double> values() const override;
    bool needs_bind() const override { return ts->needs_bind();}
    void do_bind() override {dref(ts).do_bind();}
    void do_unbind() override {dref(ts).do_unbind();}
    ipoint_ts_ref evaluate(eval_ctx& ctx, ipoint_ts_ref const& shared_this) const override;
    shared_ptr<ipoint_ts> clone_expr() const override;
    string stringify() const override;
    void prepare(eval_ctx&ctx) const override;
    x_serialize_decl();
};
    
}
x_serialize_export_key(shyft::time_series::dd::derivative_ts);
