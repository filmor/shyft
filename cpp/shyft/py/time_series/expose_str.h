/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <shyft/py/api/expose_str.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/time_series/dd/apoint_ts.h>

namespace expose {
    template<> inline std::string str_(shyft::core::utctime const& o) { return shyft::core::calendar().to_string(o); }
    template<> inline std::string str_(shyft::core::utcperiod const& per) { return per.to_string(); }
    template<> inline std::string str_(shyft::time_axis::generic_dt const& ta) {
        // Note: This is not the implementation exposed as __str__/__repr__ on the
        // time axis object, that a separate implementation in cpp/shyft/py/time_series/api_utctime.cpp.
        // The current one is typically needed for classes that have time axis as a property and a
        // str_ implementation which calls str_ on its properties.
        std::string r("TimeAxis(");
        switch (ta.gt()) {
        case shyft::time_axis::generic_dt::generic_type::FIXED:
            r += "fixed,";
            break;
        case shyft::time_axis::generic_dt::generic_type::CALENDAR:
            r += "calendar,";
            break;
        case shyft::time_axis::generic_dt::generic_type::POINT:
            r += "point,";
            break;
        }
        r += str_(ta.total_period());
        return r + ")";
    }
    template<> inline std::string str_(shyft::time_series::dd::apoint_ts const& o) { return o.stringify(); }
}
