#include <shyft/py/api/boostpython_pch.h>
#include <shyft/py/energy_market/ui/ui_generators.h>
#include <shyft/version.h>

#include <cstdint>
#include <string>
#include <iterator>

#include <QObject>
#include <QWidget>
#include <QVersionNumber>

namespace shyft::web_api::generator {
    using output_iterator = std::back_insert_iterator<std::string>;
}

namespace expose { // Expose Python api for generating ui json configuration from Qt Widgets

using std::size_t;
using std::intptr_t;
using std::string;
using std::to_string;
using namespace std::string_literals;
using std::runtime_error;
namespace py=boost::python;

// Qt wrap cast from opaque handle to 'known' pointer.
#if 0
// With QtSide2 / Shiboken2 this fails with integer overflow error when caller args contains result from shiboken2.getCppPointer(widget)!
// TODO: The value, for a QWidget, is a tuple with two entries (due to multi inheritance?), and the second one seems to not be a valid pointer
// and causes the issue (even if we just try to retrieve the first entry of the tuple)!
template <typename T>
T* wrap(intptr_t ptr) {
    return reinterpret_cast<T*>(ptr);
}
template<class T>
static T x_arg(const py::tuple& args, size_t i) {
    if (py::len(args) + 1 < (int)i)
        throw runtime_error("missing arg #"s + to_string(i) + " in time"s);
    py::object o = args[i];
    py::extract<T> xtract_arg(o);
    return xtract_arg();
}
py::object export_qt(const py::tuple& args,const py::dict & kwargs) {
    const auto qw_handle = x_arg<intptr_t>(args, 0); // Get out the opaque handle.
    const auto* o = wrap<QObject>(qw_handle); // cast it to what we know it is
    if (const auto* p = dynamic_cast<const QWidget*>(o)) { // try widget
        std::string s;
        shyft::web_api::generator::output_iterator oi(s);
        shyft::web_api::generator::emit<decltype(oi), QWidget> oo(oi, *p);
        return py::object(s);
    }
    return py::object("ERROR: Not a widget, need a top-level widget to start with");
}
#else
py::object export_qt(const intptr_t& handle) { // Caller should supply shiboken2.getCppPointer(widget)[0]
    const auto* o = reinterpret_cast<QObject*>(handle);
    if (const auto* p = dynamic_cast<const QWidget*>(o)) {
        std::string s;
        shyft::web_api::generator::output_iterator oi(s);
        shyft::web_api::generator::emit<decltype(oi), QWidget>(oi, *p);
        return py::object(s);
    }
    return py::object("ERROR: Not a widget, need a top-level widget to start with");
}
#endif

void ui_cfg() {
    boost::python::scope().attr("qt_version") = string{ QT_VERSION_STR };
    //OLD: No use for qt_version, because "No to_python (by-value) converter found for C++ type: class QVersionNumber"!?
    //boost::python::scope().attr("qt_version")  = QVersionNumber{ QT_VERSION_MAJOR, QT_VERSION_MINOR, QT_VERSION_PATCH };
    //boost::python::scope().attr("qt_version_string") = string{ QT_VERSION_STR };
    //boost::python::scope().attr("qt_version_string") = string{ QT_VERSION_STR };

    py::enum_<shyft::web_api::generator::ItemDataProperty>("ItemDataProperty")
        .value("DataX", shyft::web_api::generator::ItemDataProperty::DataX)
        .value("DataY", shyft::web_api::generator::ItemDataProperty::DataY)
        .value("Decimals", shyft::web_api::generator::ItemDataProperty::Decimals)
        .value("ScaleX", shyft::web_api::generator::ItemDataProperty::ScaleX)
        .value("ScaleY", shyft::web_api::generator::ItemDataProperty::ScaleY)
        .value("ValidationX", shyft::web_api::generator::ItemDataProperty::ValidationX)
        .value("ValidationY", shyft::web_api::generator::ItemDataProperty::ValidationY)
        .value("Tags", shyft::web_api::generator::ItemDataProperty::Tags)
        .export_values()
        ;

#if 0
    py::def("export", py::raw_function(export_qt, 1));
#else
    py::def("export", &export_qt);
#endif
}

extern void ex_client_server();
}

BOOST_PYTHON_MODULE(_ui) {
    boost::python::docstring_options doc_options(true, true, false); // all except c++ signatures
    boost::python::scope().attr("__doc__") = "Shyft Energy Market user interface configuration api";
    boost::python::scope().attr("__version__") = shyft::_version_string();
    boost::python::scope().attr("shyft_with_stm") = true;
    expose::ui_cfg();
    expose::ex_client_server();
}
