#include <boost/python/def.hpp>
#include <boost/python/module.hpp>
#include <boost/python/scope.hpp>
#include <boost/python/docstring_options.hpp>

#include <shyft/version.h>

namespace py=boost::python;
BOOST_PYTHON_MODULE(_ui) {
    py::docstring_options doc_options(true, true, false);// all except c++ signatures
    py::scope().attr("__doc__") = "Shyft Energy Market user interface configuration api";
    py::scope().attr("__version__") = shyft::_version_string();
    py::scope().attr("shyft_with_stm") = false;
}
