# Compilation step for Python extensions

add_library(core SHARED
    api.cpp
    api_hydro_power_system.cpp
    api_time_series_support.cpp
    api_server.cpp
)

target_include_directories(core PRIVATE ${python_include} ${python_numpy_include})
target_link_libraries(
    core
    PRIVATE shyft_private_flags em_model_core ${boost_py_link_libraries} ${python_lib}
    PUBLIC shyft_public_flags
)

set_target_properties(core PROPERTIES
    OUTPUT_NAME core
    VISIBILITY_INLINES_HIDDEN TRUE
    PREFIX "_" # Python extensions do not use the 'lib' prefix
    SUFFIX ${py_ext_suffix}
    INSTALL_RPATH "$ORIGIN/../../lib"
)

install(TARGETS core ${shyft_runtime} DESTINATION ${SHYFT_PYTHON_DIR}/shyft/energy_market/core)

