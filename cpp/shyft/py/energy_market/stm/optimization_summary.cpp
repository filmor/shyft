#include <shyft/py/api/boostpython_pch.h>
#include <shyft/energy_market/stm/optimization_summary.h>
#include <shyft/py/energy_market/stm/expose_str.h>
#include <shyft/py/energy_market/py_attr_wrap.h>

#include <string>
#include <memory>
#include <fmt/core.h>

namespace py=boost::python;

namespace expose {
    using namespace shyft::energy_market;

    using shyft::energy_market::stm::optimization_summary;
    using std::string;
    using std::shared_ptr;

    template <> string str_(optimization_summary const& o) {
        return fmt::format("OptimizationSummary(total={},sum_penalties={},minor_penalties={},major_penalties={},grand_total={})",
                           o.total,
                           o.sum_penalties,
                           o.minor_penalties,
                           o.major_penalties,
                           o.grand_total);
    }


    void stm_optimization_summary() {

        auto os=py::class_<
                optimization_summary,
                py::bases<>,
                shared_ptr<optimization_summary>,
                boost::noncopyable
                >("_OptimizationSummary", "Summary of optimzation results.");// practial for testing., py::no_init);
        os
        .def_readonly("reservoir", &optimization_summary::reservoir, "Reservoir related summary.")
        .def_readonly("waterway", &optimization_summary::waterway, "Waterway related summary.")
        .def_readonly("gate", &optimization_summary::gate, "Gate related summary.")
        .def_readonly("spill", &optimization_summary::spill, "Spill related summary.")
        .def_readonly("bypass", &optimization_summary::bypass, "Bypass related summary.")
        .def_readonly("ramping", &optimization_summary::ramping, "Ramping related summary.")
        .def_readonly("reserve", &optimization_summary::reserve, "Reserve related summary.")
        .def_readonly("unit", &optimization_summary::unit, "Unit related summary.")
        .def_readonly("plant", &optimization_summary::plant, "Plant related summary.")
        .def_readonly("market", &optimization_summary::market, "Market related summary.")
        .def("flattened_attributes", +[](optimization_summary& self) { return make_flat_attribute_dict(self); }, "Flat dict containing all component attributes.")
        .def(py::self==py::self)
        .def(py::self!=py::self)
        ;
        add_proxy_property(os,"total",optimization_summary,total,"Total");
        add_proxy_property(os,"sum_penalties", optimization_summary,sum_penalties, "Total penalty cost.")
        add_proxy_property(os,"minor_penalties", optimization_summary,minor_penalties, "Total minor penalty cost.")
        add_proxy_property(os,"major_penalties", optimization_summary,major_penalties, "Total major penalty cost.")
        add_proxy_property(os,"grand_total", optimization_summary,grand_total, "Grand total.")
        expose_str_repr(os);

        {
            py::scope scope_os=os;
            auto opr = py::class_<optimization_summary::reservoir_, py::bases<>, boost::noncopyable>("_Reservoir", py::no_init);
            _add_proxy_property(opr,"end_value", optimization_summary::reservoir_,end_value, "[NOK] Value of remaining water after sim-end.");
            _add_proxy_property(opr,"sum_ramping_penalty", optimization_summary::reservoir_,sum_ramping_penalty, "[NOK] Sum penalty for all reservoirs for violating ramping constraints.");
            _add_proxy_property(opr,"sum_limit_penalty", optimization_summary::reservoir_,sum_limit_penalty, "[NOK] Sum penalty for all reservoirs for violating volume limits during simulation horizon.");
            _add_proxy_property(opr,"end_limit_penalty", optimization_summary::reservoir_,end_limit_penalty, "[NOK] Sum penalty for all reservoirs for violating volume limits at the end of simulation horizon.");
            _add_proxy_property(opr,"hard_limit_penalty", optimization_summary::reservoir_,hard_limit_penalty, "[NOK] Sum penalty for all reservoirs for violating the hard volume or level limits.");

            auto opw = py::class_<optimization_summary::waterway_, py::bases<>, boost::noncopyable>("_Waterway", py::no_init);
            _add_proxy_property(opw,"vow_in_transit", optimization_summary::waterway_,vow_in_transit, "[NOK] Value of water in transit.");
            _add_proxy_property(opw,"sum_discharge_fee", optimization_summary::waterway_,sum_discharge_fee, "[NOK] Sum of discharge fee.");
            _add_proxy_property(opw,"discharge_group_penalty", optimization_summary::waterway_,discharge_group_penalty, "[NOK] Discharge group penalty.");
            _add_proxy_property(opw,"discharge_group_ramping_penalty", optimization_summary::waterway_,discharge_group_penalty, "[NOK] Discharge group ramping penalty.");

            auto opg = py::class_<optimization_summary::gate_, py::bases<>, boost::noncopyable>("_Gate", py::no_init);
            _add_proxy_property(opg,"ramping_penalty", optimization_summary::gate_,ramping_penalty, "[NOK] Penalty for violating ramping constraints, for all gates and timesteps.");
            _add_proxy_property(opg,"discharge_cost", optimization_summary::gate_,discharge_cost, "[NOK] Sum discharge cost for all gates and timesteps.");
            _add_proxy_property(opg,"discharge_constraint_penalty", optimization_summary::gate_,discharge_constraint_penalty, "[NOK] Sum penalty for violating max/min constraints, for all gates and timesteps.");


            auto ops = py::class_<optimization_summary::spill_, py::bases<>, boost::noncopyable>("_Spill", py::no_init);
            _add_proxy_property(ops,"cost", optimization_summary::spill_,cost, "[NOK] Sum cost of spill for all spill gates gates and timesteps.");
            _add_proxy_property(ops,"physical_cost", optimization_summary::spill_,physical_cost, "[NOK] Sum cost of physical spill for all reservoirs and timesteps.");
            _add_proxy_property(ops,"nonphysical_cost", optimization_summary::spill_,nonphysical_cost, "[NOK] Sum cost of non-physical spill for all reservoirs and timesteps.");
            _add_proxy_property(ops,"physical_volume", optimization_summary::spill_,physical_volume, "[NOK] Sum of physical spill volume for all reservoirs and timesteps.");
            _add_proxy_property(ops,"nonphysical_volume", optimization_summary::spill_,nonphysical_volume, "[NOK] Sum of non-physical spill volume for all reservoirs and timesteps.");


            auto opb = py::class_<optimization_summary::bypass_, py::bases<>, boost::noncopyable>("_Bypass", py::no_init);
            _add_proxy_property(opb,"cost", optimization_summary::bypass_,cost, "[NOK] Sum cost of bypass for all bypass gates and timesteps.");

            auto opra = py::class_<optimization_summary::ramping_, py::bases<>, boost::noncopyable>("_Ramping", py::no_init);
            _add_proxy_property(opra,"ramping_penalty", optimization_summary::ramping_,ramping_penalty, "[NOK] Sum penalty for violating ramping constraints, for all constraints and timesteps.");

            auto opre = py::class_<optimization_summary::reserve_, py::bases<>, boost::noncopyable>("_Reserve", py::no_init);
            _add_proxy_property(opre,"violation_penalty", optimization_summary::reserve_,violation_penalty, "[NOK] Total penalty cost when reserve deviate below obligations.");
            _add_proxy_property(opre,"sale_buy", optimization_summary::reserve_,sale_buy, "[NOK] Reserve trade value.");
            _add_proxy_property(opre,"obligation_value", optimization_summary::reserve_,obligation_value, "[NOK] Obligation value.");

            auto opu = py::class_<optimization_summary::unit_, py::bases<>, boost::noncopyable>("_Unit", py::no_init);
            _add_proxy_property(opu,"startup_cost", optimization_summary::unit_,startup_cost, "[NOK] Total startup and shutdown cost.");
            _add_proxy_property(opu,"schedule_penalty", optimization_summary::unit_,schedule_penalty, "[NOK] Total penalty cost when generator schedule is violated.");

            auto opp = py::class_<optimization_summary::plant_, py::bases<>, boost::noncopyable>("_Plant", py::no_init);
            _add_proxy_property(opp,"production_constraint_penalty", optimization_summary::plant_,production_constraint_penalty, "[NOK] Total production cost when time-dependent max/min production is violated.");
            _add_proxy_property(opp,"discharge_constraint_penalty", optimization_summary::plant_,discharge_constraint_penalty, "[NOK] Total discharge cost when time-dependent max/min discharge is violated.");
            _add_proxy_property(opp,"schedule_penalty", optimization_summary::plant_,schedule_penalty, "[NOK] Total penalty cost for violating plant schedule in simulation horizon.");
            _add_proxy_property(opp,"ramping_penalty", optimization_summary::plant_,ramping_penalty, "[NOK] Total penalty cost for violating plant ramping constraints in simulation horizon.");

            auto opm = py::class_<optimization_summary::market_, py::bases<>, boost::noncopyable>("_Market", py::no_init);
            _add_proxy_property(opm,"sum_sale_buy", optimization_summary::market_,sum_sale_buy, "[NOK] Sum energy bought minus energy sold in the market for all timesteps.");
            _add_proxy_property(opm,"load_penalty", optimization_summary::market_,load_penalty, "[NOK] Sum penalty for violating load obligation for all markets and timesteps.");
            _add_proxy_property(opm,"load_value", optimization_summary::market_,load_value, "[NOK] Total load multiplied with marked sale price.");
        }
    }

}
