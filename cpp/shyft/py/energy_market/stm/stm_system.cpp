/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/


#include <shyft/py/api/boostpython_pch.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/energy_market/py_url_tag.h>
#include <shyft/py/energy_market/stm/expose_str.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/energy_market/py_tsm_expose.h>
#include <shyft/energy_market/stm/srv/dstm/ts_url_generator.h>
#include <fmt/core.h>
#include <stdexcept>
#include <shyft/time/utctime_utilities.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/power_module.h>
#include <shyft/energy_market/stm/unit_group.h>

namespace py=boost::python;

namespace expose {
    using namespace shyft::energy_market;
    using namespace std::string_literals;

    using shyft::time_series::dd::apoint_ts;

    using stm::power_module_;
    using stm::network_;
    using stm::energy_market_area_;
    using stm::stm_builder;

    using std::string;
    using std::string_view;
    using std::shared_ptr;

    template<> string str_(stm::energy_market_area::ts_triplet_ const& o) {
        return fmt::format("_TsTriplet(realised={},schedule={},result={})"
            ,str_(o.realised)
            ,str_(o.schedule)
            ,str_(o.result)
        );
    }

    template<> string str_(stm::energy_market_area::offering_ const& o) {
        return fmt::format("_Offering(bids={},usage={},price={})"
            ,str_(o.bids)
            ,str_(o.usage)
            ,str_(o.price)
        );
    }

    template<> string str_(stm::energy_market_area const& o) {
        return fmt::format("MarketArea(id={}, name={})"
            ,str_(o.id)
            ,str_(o.name)
        );
    }


    template<> string str_(stm::stm_system const& o) {
        return fmt::format("StmSystem(id={}, name={})"
            ,str_(o.id)
            ,str_(o.name)
        );
    }

    struct stm_sys_ext {
        static std::vector<char> to_blob(const  stm::stm_system_& m) {
            auto s=shyft::energy_market::stm::stm_system::to_blob(m);
            return std::vector<char>(s.begin(),s.end());
        }
        static stm::stm_system_ from_blob(std::vector<char>& blob) {
            std::string s(blob.begin(),blob.end());
            return shyft::energy_market::stm::stm_system::from_blob(s);
        }

        static power_module_ create_power_module(stm::stm_system_& sys, int id, const string& name, const string& json) { return stm_builder(sys).create_power_module(id, name, json); }
        static network_ create_network(stm::stm_system_& sys, int id, const string& name, const string& json) { return stm_builder(sys).create_network(id, name, json); }
        static energy_market_area_ create_market_area(stm::stm_system_& sys, int id, const string& name, const string& json) { return stm_builder(sys).create_market_area(id, name, json); }
        static bool patch(stm::stm_system_& sys,stm::stm_patch_op op, stm::stm_system_ const &p) {
            return p?stm::patch(sys,op,*p):false;
        }
    };


    void stm_system() {
        auto ma=py::class_<
            stm::energy_market_area,
            py::bases<>,
            shared_ptr<stm::energy_market_area>,
            boost::noncopyable
        >("MarketArea", 
            doc_intro("A market area, with load/price and other properties.")
            doc_details("Within the market area, there are zero or more energy-\n"
                        "producing/consuming units.")
            , py::no_init);
        ma
            .def(py::init<int, const string&, const string&, stm::stm_system_ &>(
                (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("stm_sys")),
                "Create market area for a stm system."))
            .def_readwrite("id",&stm::energy_market_area::id,"Unique id for this object.")
            .def_readwrite("name",&stm::energy_market_area::name,"Name for this object.")
            .def_readwrite("json",&stm::energy_market_area::json,"Json keeping any extra data for this object.")
            .add_property("tag",+[](const stm::energy_market_area& self){return url_tag(self);}, "Url tag.")
            .add_property("unit_group",&stm::energy_market_area::get_unit_group,&stm::energy_market_area::set_unit_group, "Getter and setter for unit group.")
            .def_readonly("contracts",&stm::energy_market_area::contracts,"List of contracts.")
            .def_readonly("busbars",&stm::energy_market_area::busbars,"List of busbars.")
            .def_readonly("demand",&stm::energy_market_area::demand,"The demand side describing the buyers of energy")
            .def_readonly("supply",&stm::energy_market_area::supply,"The supply side describing the producers of energy")
            .def("remove_unit_group",&stm::energy_market_area::remove_unit_group,(py::arg("self")),
                doc_intro("Remove the unit group associated with this market if available.")
                doc_parameters()
                doc_returns("unit_group","UnitGroup","The newly removed unit group.")
            )
            .def("transmission_lines_to",&stm::energy_market_area::transmission_lines_to,(py::arg("self"), py::arg("to")),
                doc_intro("Get transmission lines (from this) to MarketArea")
                doc_parameters()
                doc_parameter("to","MarketArea","MarketArea to get transmission lines to")
                doc_returns("transmission_line_list","TransmissionLineList","List of transmission lines to MarketArea")
            )
            .def("transmission_lines_from",&stm::energy_market_area::transmission_lines_from,(py::arg("self"), py::arg("from")),
                doc_intro("Remove the unit group associated with this market if available.")
                doc_parameters()
                doc_parameter("from","MarketArea","MarketArea to get transmission lines from")
                doc_returns("transmission_line_list","TransmissionLineList","List of transmission lines from MarketArea")
            )
            .def("create_busbar_derived_unit_group",&stm::energy_market_area::create_busbar_derived_unit_group,(py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json")),
                doc_intro("Add a unit-group of type 'production' to the system and to the market area.")
                doc_intro("The unit group will derive its units from the associated network busbars.")
                doc_intro("Units are automatically added/removed if busbars are added/removed on the network.")
                doc_parameters()
                doc_parameter("id","int","Unique id of the group.")
                doc_parameter("name","str","Unique name of the group.")
                doc_parameter("json","str","Json payload for py customization.")
                doc_returns("unit_group","UnitGroup","The newly created unit-group.")
            )
            .def("get_production",&stm::energy_market_area::get_production,(py::arg("self")),
                doc_intro("Get the sum of production contribution for all busbars")
                doc_parameters()
                doc_returns("production","TsTripletResult","Production contribution sum, as triplet: result,realised,schedule")
            )
            .def("get_consumption",&stm::energy_market_area::get_consumption,(py::arg("self")),
                doc_intro("Get the sum of consumption contribution for all busbars")
                doc_parameters()
                doc_returns("consumption","TsTripletResult","Consumption contribution sum, as triplet: result,realised,schedule")
            )
            .def("get_import",&stm::energy_market_area::get_import,(py::arg("self")),
                doc_intro("Get the total import of the market area.")
                doc_intro("If the actual sum of busbar flow is negative, import is 0.")
                doc_parameters()
                doc_returns("import","TsTripletResult","Market area import, as triplet: result,realised,schedule")
            )
            .def("get_export",&stm::energy_market_area::get_export,(py::arg("self")),
                doc_intro("Get the total export of the market area. (converted to positive value)")
                doc_intro("If the actual sum of busbar flow is positive, export is 0.")
                doc_parameters()
                doc_returns("export","TsTripletResult","Market area export, as triplet: result,realised,schedule")
            )
            .def("flattened_attributes", +[](stm::energy_market_area& self) { return make_flat_attribute_dict(self); }, "Flat dict containing all component attributes.")
            .def(py::self==py::self)
            .def(py::self!=py::self)
        ;
        expose_tsm(ma);
        expose_str_repr(ma);
        add_proxy_property(ma,"load", stm::energy_market_area,load, "[W] Load, time series.")
        add_proxy_property(ma,"price", stm::energy_market_area,price, "[Money/J] Price, time series.")
        add_proxy_property(ma,"max_buy", stm::energy_market_area,max_buy, "[W] Maximum buy, time series.")
        add_proxy_property(ma,"max_sale", stm::energy_market_area,max_sale, "[W] Maximum sale, time series.")
        add_proxy_property(ma,"buy", stm::energy_market_area,buy, "[W] Buy result, time series.")
        add_proxy_property(ma,"sale", stm::energy_market_area,sale, "[W] Sale result, time series.")
        add_proxy_property(ma,"production", stm::energy_market_area,production, "[W] Production result, time series.")
        add_proxy_property(ma,"reserve_obligation_penalty", stm::energy_market_area,reserve_obligation_penalty, "[Money/x] Obligation penalty, time series.")
        /* ns scope */{
            py::scope offering{ma};
            auto offr=py::class_<stm::energy_market_area::offering_,py::bases<>,boost::noncopyable>(
                "_Offering",
                doc_intro("describes actors (supply/demand) offering into the market"),
                py::no_init
            );
            offr
            .def_readonly("usage",&stm::energy_market_area::offering_::usage,"The amount consumed/used of the offering")
            .def_readonly("price",&stm::energy_market_area::offering_::price,"Given the usage, the effective price based on bids")
            ;
            expose_str_repr(offr);
            _add_proxy_property(offr,"bids",stm::energy_market_area::offering_,bids,"time dep x,y, x=price[Money/J],y=amount[W]")

            py::class_<stm::energy_market_area::ts_triplet_result,py::bases<>>(
                "TsTripletResult",
                doc_intro("describes .realised, .schedule and .result time-series"),
                py::no_init
            )
            .def_readonly("realised", &stm::energy_market_area::ts_triplet_result::realised, "SI-units, realised time-series, as in historical fact")
            .def_readonly("schedule", &stm::energy_market_area::ts_triplet_result::schedule, "SI-units, schedule, as in current schedule")
            .def_readonly("result", &stm::energy_market_area::ts_triplet_result::result, "SI-units, result, as provided by optimisation");

            auto ts3=py::class_<stm::energy_market_area::ts_triplet_,py::bases<>,boost::noncopyable>(
                "_TsTriplet",
                doc_intro("describes .realised, .schedule and .result time-series"),
                py::no_init
            );
            _add_proxy_property(ts3,"realised",stm::energy_market_area::ts_triplet_,realised,"SI-units, realised time-series, as in historical fact")
            _add_proxy_property(ts3,"schedule",stm::energy_market_area::ts_triplet_,schedule,"SI-units, schedule, as in current schedule")
            _add_proxy_property(ts3,"result",stm::energy_market_area::ts_triplet_,result,"SI-units, result, as provided by optimisation")
            expose_str_repr(ts3);
        }

        expose_vector_eq<stm::energy_market_area_>("MarketAreaList","A strongly typed list of MarketArea.",&stm::equal_attribute<std::vector<stm::energy_market_area_>>,false);
        auto ss= py::class_<
            stm::stm_system,
            py::bases<>,
            shared_ptr<stm::stm_system>,
            boost::noncopyable
        >("StmSystem", "A complete stm system, with market areas, and hydro power systems.", py::no_init);
        ss
            .def(py::init<int, const string&, const string&>(
                (py::arg("uid"), py::arg("name"), py::arg("json")), "Create stm system."))
            .def_readwrite("id",&stm::stm_system::id,"Unique id for this object.")
            .def_readwrite("name",&stm::stm_system::name,"Name for this object.")
            .def_readwrite("json",&stm::stm_system::json,"Json keeping any extra data for this object.")
            .def_readonly("market_areas",&stm::stm_system::market,"List of market areas.")
            .def_readonly("contracts",&stm::stm_system::contracts,"List of contracts.")
            .def_readonly("contract_portfolios",&stm::stm_system::contract_portfolios,"List of contract portfolios.")
            .def_readonly("hydro_power_systems",&stm::stm_system::hps,"List of hydro power systems.")
            .def_readonly("power_modules", &stm::stm_system::power_modules, "List of power modules.")
            .def_readonly("networks", &stm::stm_system::networks, "List of networks.")
            .def_readonly("summary", &stm::stm_system::summary, "Key result values from simulations.")
            .def_readonly("run_parameters", &stm::stm_system::run_params, "Run parameters.")
            .def_readonly("unit_groups",&stm::stm_system::unit_groups,
                doc_intro("Unit groups with constraints.")
                doc_intro("These groups holds a list of units, plus the constraints that apply to them.")
                doc_intro("During optimisation, the groups along with their constraints,")
                doc_intro("so that the optimization can take it into account when finding the")
                doc_intro("optimal solution.")
            )
            .def("create_power_module", &stm_sys_ext::create_power_module,
                (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json")=string("")),
                doc_intro("Create stm power module with unique uid.")
                doc_returns("power_module","PowerModule","The new power module.")
            )
            .def("create_network", &stm_sys_ext::create_network,
                (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json")=string("")),
                doc_intro("Create stm network with unique uid.")
                doc_returns("network","Network","The new network.")
            )
            .def("create_market_area", &stm_sys_ext::create_market_area,
                (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json")=string("")),
                doc_intro("Create stm energy market area with unique uid.")
                doc_returns("market_area","MarketArea","The new market area.")
            )
            .def("add_unit_group",&stm::stm_system::add_unit_group,(py::arg("self"),py::arg("id"),py::arg("name"),py::arg("json"), py::arg("group_type")=stm::unit_group_type{}),
                doc_intro("Add an empty named unit-group to the system.")
                doc_parameters()
                doc_parameter("id","int","Unique id of the group.")
                doc_parameter("name","str","Unique name of the group.")
                doc_parameter("json","str","Json payload for py customization.")
                doc_parameter("group_type","int", "One of UnitGroup.unit_group_type values, default 0.")
                doc_returns("unit_group","UnitGroup","The newly created unit-group.")
            )
            .def("result_ts_urls",+[](stm::stm_system const*s,string prefix){
                    return shyft::energy_market::stm::srv::dstm::ts_url_generator(prefix,*s);
                },
                 (py::arg("self"),py::arg("prefix")),
                 doc_intro("Generate and return all result time series urls for the specified model.")
                 doc_intro("Useful in the context of a dstm client, that have a get_ts method that accepts this list.")
                 doc_parameters()
                 doc_parameter("prefix","","Url prefix, like dstm://Mmodel_id.")
                 doc_returns("ts_urls","StringVector","A strongly typed list of strings with ready to use ts-urls.")
             )
            .def("to_blob",&stm_sys_ext::to_blob,(py::arg("self")),
                doc_intro("Serialize the model to a blob.")
                doc_returns("blob","ByteVector","Blob form of the model.")
            )
            .def("from_blob",&stm_sys_ext::from_blob,(py::arg("blob")),
                doc_intro("Re-create a stm system from a previously create blob.")
                doc_returns("stm_sys","StmSystem","A stm system including hydro-power-systems and markets.")
            ).staticmethod("from_blob")
            .def("patch",&stm_sys_ext::patch,(py::arg("self"),py::arg("op"),py::arg("p")),
                 doc_intro("Patch self with patch `p` using operation `op`.")
                 doc_parameters()
                 doc_parameter("op","","Operation is one of ADD,REMOVE_RELATIONS,REMOVE_OBJECTS")
                 doc_parameter("p","","The StmSystem describing the patch")
                 doc_returns("result","","True if patch operation and type was applied, false if not supported operation")
            )
            .def(py::self==py::self)
            .def(py::self!=py::self)
        ;
        expose_str_repr(ss);

        expose_vector_eq<shared_ptr<stm::stm_system>>("StmSystemList","Strongly typed list of StmSystem",&stm::equal_attribute<std::vector<shared_ptr<stm::stm_system>>>,false);

   }
}
