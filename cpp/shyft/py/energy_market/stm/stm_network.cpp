/** This file is part of Shyft. Copyright 2015-2022 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/energy_market/py_url_tag.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/energy_market/stm/expose_str.h>
#include <shyft/py/energy_market/py_tsm_expose.h>

#include <fmt/core.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/busbar.h>
#include <shyft/energy_market/stm/transmission_line.h>

namespace py=boost::python;

namespace expose {
    using namespace shyft::energy_market;

    using stm::network_;
    using stm::network_builder;
    using stm::transmission_line_;
    using stm::busbar_;

    using std::string;
    using std::shared_ptr;


    template<> string str_(stm::network const& o) {
        return fmt::format("Network(id={}, name='{}')"
            ,o.id
            ,o.name
        );
    }

    /** extensions to ease py expose */
    struct net_ext {
        // wrap all create calls via the network_builder to enforce build-rules
        static transmission_line_ create_transmission_line(network_& n, int id, const string& name, const string &json) {return network_builder(n).create_transmission_line(id,name,json);}
        static busbar_ create_busbar(network_ &n, int id, const string& name, const string &json){return network_builder(n).create_busbar(id,name,json);}
    };

    void stm_network() {
        auto c=py::class_<
            stm::network,
            py::bases<>,
            shared_ptr<stm::network>,
            boost::noncopyable
        >("Network",
          doc_intro("A network consisting of busbars and transmission lines."),
          py::no_init);
        c
            .def(py::init<int, const string&, const string&, stm::stm_system_&>(
                (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("sys")),
                "Create network with unique id and name for a stm system."))
            .def("create_transmission_line", &net_ext::create_transmission_line,
                (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json")=string("")),
                doc_intro("Create stm transmission line with unique uid.")
                doc_returns("transmission_line","TransmissionLine","The new transmission line.")
            )
            .def("create_busbar", &net_ext::create_busbar,
                (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json")=string("")),
                doc_intro("Create stm busbar with unique uid.")
                doc_returns("busbar","Busbar","The new busbar.")
            )
            .def_readwrite("id",&stm::network::id,"Unique id for this object.")
            .def_readwrite("name",&stm::network::name,"Name for this object.")
            .def_readwrite("json",&stm::network::json,"Json keeping any extra data for this object.")
            .add_property("tag", +[](const stm::network& self){return url_tag(self);}, "Url tag.")
            .def_readonly("transmission_lines",&stm::network::transmission_lines,"List of transmission lines.")
            .def_readonly("busbars", &stm::network::busbars, "List of busbars.")
            .def("__eq__", &stm::network::operator==)
            .def("__ne__", &stm::network::operator!=)
        ;
        expose_str_repr(c);
        expose_tsm(c);
        expose_vector_eq<stm::network_>("NetworkList", "A strongly typed list of Network.", &stm::equal_attribute<std::vector<stm::network_>>, false);
    }
}
