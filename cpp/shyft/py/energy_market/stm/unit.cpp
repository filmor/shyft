/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/py/energy_market/py_url_tag.h>
#include <shyft/py/energy_market/stm/expose_str.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/api/expose_from_list.h>
#include <shyft/py/energy_market/py_tsm_expose.h>
#include <shyft/py/api/expose_container.h>
#include <fmt/core.h>

namespace py=boost::python;

namespace expose {
    using namespace shyft::energy_market;

    using std::string;
    using std::shared_ptr;

    template<> string str_(stm::unit::production_::constraint_ const& o) {
        return fmt::format("Unit._Production._Constraint(min={}, max={})"
            ,str_(o.min)
            ,str_(o.max)
        );
    }

    template<> string str_(stm::unit::production_ const& o) {
        return fmt::format("Unit._Production(schedule={}, commitment={}, pump_commitment={}, result={}, constraint={}, realised={}, static_min={}, static_max={}, nominal={})"
            ,str_(o.schedule)
            ,str_(o.commitment)
            ,str_(o.pump_commitment)
            ,str_(o.result)
            ,str_(o.constraint)
            ,str_(o.realised)
            ,str_(o.static_min)
            ,str_(o.static_max)
            ,str_(o.nominal)
        );
    }

    template<> string str_(stm::unit::discharge_::constraint_ const& o) {
        return fmt::format("Unit._Discharge._Constraint(min={}, max={})"
            ,str_(o.min)
            ,str_(o.max)
        );
    }

    template<> string str_(stm::unit::discharge_ const& o) {
        return fmt::format("Unit._Discharge(schedule={}, result={}, constraint={})"
            ,str_(o.schedule)
            ,str_(o.result)
            ,str_(o.constraint)
        );
    }

    template<> string str_(stm::unit::pump_constraint_ const& o) {
        return fmt::format("Unit._Pump_Constraint(min_downstream_level={}"
            ,str_(o.min_downstream_level)
        );
    }

    template<> string str_(stm::unit::cost_ const& o) {
        return fmt::format("Unit._Cost(start={}, stop={}, pump_start={}, pump_stop={})"
            ,str_(o.start)
            ,str_(o.stop)
            ,str_(o.pump_start)
            ,str_(o.pump_stop)
        );
    }

    template<> string str_(stm::unit const& o) {
        return fmt::format("Unit(id={}, name={})"
            ,str_(o.id)
            ,str_(o.name)
        );
    }

    template<> string str_(stm::unit::reserve_::spec_ const& o) {
        return fmt::format("Unit._Reserve._Spec(schedule={},result={},min={}, max={},realised={})"
            ,str_(o.schedule)
            ,str_(o.result)
            ,str_(o.min)
            ,str_(o.max)
            ,str_(o.realised)
        );
    }
  
    template<> string str_(stm::unit::reserve_::pair_ const& o) {
        return fmt::format("Unit._Reserve._Pair(up={},down={})"
            ,str_(o.up)
            ,str_(o.down)
        );
    }
 
    template<> string str_(stm::unit::reserve_ const& /* o */) {
        //TODO: consider how to format this, potentially a lot of information.
        return "Unit._Reserve(fcr_n,afrr,mfrr,mfrr_static_min,rr,fcr_d,fcr_mip,frr,droop)";
    }

    template<> string str_(stm::unit::production_discharge_relation_ const& o) {
        return fmt::format("Unit._ProductionDischargeRelation(original={}, convex={}, final={})"
            ,str_(o.original)
            ,str_(o.convex)
            ,str_(o.final)
        );
    }

    template<> string str_(stm::unit::best_profit_ const& o) {
        return fmt::format("Unit._BestProfit(production={}, discharge={}, discharge_production_ratio={}, operating_zone={})"
            ,str_(o.production)
            ,str_(o.discharge)
            ,str_(o.discharge_production_ratio)
            ,str_(o.operating_zone)
        );
    }

    /**
     */
    void stm_unit() {
        // 
        // get type set from accessors ->
        // for_each(...)

        
        auto u=py::class_<
            stm::unit,
            py::bases<hydro_power::unit>,
            shared_ptr<stm::unit>,
            boost::noncopyable
        >("Unit", "Stm unit (turbine and generator assembly).", py::no_init);
        u
            .def(py::init<int, const string&, const string&, stm::stm_hps_ &>(
                (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("hps")),
                "Create unit with unique id and name for a hydro power system."))
            .def_readonly("discharge", &stm::unit::discharge, "Discharge attributes.")
            .def_readonly("pump_constraint", &stm::unit::pump_constraint, "Pump constraint attributes.")
            .def_readonly("production", &stm::unit::production, "Production attributes.")
            .def_readonly("cost", &stm::unit::cost, "Cost attributes.")
            .def_readonly("reserve",&stm::unit::reserve,"Operational reserve attributes.")
            .def_readonly("production_discharge_relation", &stm::unit::production_discharge_relation, "PQ curves.")
            .def_readonly("best_profit", &stm::unit::best_profit, "BP curves.")
            .add_property("tag", +[](const stm::unit& self){return url_tag(self);}, "Url tag.")

            .def("__eq__", &stm::unit::operator==)
            .def("__ne__", &stm::unit::operator!=)

            .def("flattened_attributes", +[](stm::unit& self) { return make_flat_attribute_dict(self); }, "Flat dict containing all component attributes.")
        ;
        expose_str_repr(u);
        expose_tsm(u);
        add_proxy_property(u,"effective_head",stm::unit,effective_head,
                           doc_intro("Effective head of the generator, time-dependent attribute.")
        )
        add_proxy_property(u,"generator_description",stm::unit,generator_description,
            doc_intro("Generator efficiency curve, time-dependent attribute.")
        )
        add_proxy_property(u,"turbine_description",stm::unit,turbine_description,
            doc_intro("Time-dependent description of turbine efficiency.") 
        )
        add_proxy_property(u,"pump_description",stm::unit,pump_description,
            doc_intro("Time-dependent description of pump efficiency.")
        )
        add_proxy_property(u,"unavailability",stm::unit,unavailability,
            doc_intro("Time series where time steps the unit is unavailable are marked with value 1, while a value nan or 0 means it is available.")
        )
        add_proxy_property(u,"pump_unavailability",stm::unit,pump_unavailability,
            doc_intro("Time series where time steps the unit is unavailable are marked with value 1, while a value nan or 0 means it is available.")
        )
        add_proxy_property(u,"priority",stm::unit,priority,
            doc_intro("Priority value for determining uploading order.")
        )

        expose_vector_eq<stm::unit_>("UnitList", "A strongly typed list of Units.",&stm::equal_attribute<std::vector<stm::unit_>>,false);

        {
            py::scope scope_unit=u;
            auto ud=py::class_<stm::unit::discharge_, py::bases<>, boost::noncopyable>(
                "_Discharge", 
                doc_intro("Unit.Discharge attributes, flow[m3/s]."),
                py::no_init
            );
            ud
                .def_readonly("constraint", &stm::unit::discharge_::constraint, "Constraint group.")
            ;
            expose_str_repr(ud);
            _add_proxy_property(ud, "schedule", stm::unit::discharge_,schedule, "Discharge schedule, time series.")
            _add_proxy_property(ud, "result", stm::unit::discharge_,result, "Discharge result, time series.")
            _add_proxy_property(ud, "realised", stm::unit::discharge_,realised, "Discharge realised, usually the result of non trivial computation based on the measured unit.production.realised.")

            auto up=py::class_<stm::unit::production_, py::bases<>, boost::noncopyable>(
                "_Production",
                doc_intro("Unit.Production attributes, effect[W]."), 
                py::no_init
            );
            up
                .def_readonly("constraint", &stm::unit::production_::constraint, "Constraint group.")
            ;
            expose_str_repr(up);
            _add_proxy_property(up,"result",stm::unit::production_,result,
                    doc_intro("[W] Production result, time series.")
            );
            _add_proxy_property(up, "schedule", stm::unit::production_, schedule, "Production schedule, time series.")
            _add_proxy_property(up, "commitment", stm::unit::production_, commitment, "Production commitment, time series.")
            _add_proxy_property(up, "pump_commitment", stm::unit::production_, pump_commitment, "Consumption commitment, time series.")
            _add_proxy_property(up, "static_min", stm::unit::production_, static_min, "Production minimum, time-dependent attribute.")
            _add_proxy_property(up, "static_max", stm::unit::production_, static_max, "Production maximum, time-dependent attribute.")
            _add_proxy_property(up, "nominal", stm::unit::production_, nominal, "Nominal production, or installed/rated/nameplate capacity, time-dependent attribute.")
            _add_proxy_property(up, "realised", stm::unit::production_, realised, "Historical production, time series.")

            auto uph=py::class_<stm::unit::pump_constraint_, py::bases<>, boost::noncopyable>(
                "_Pump_Constraint",
                doc_intro("Contains the pump constraints."),
                py::no_init
            );
            expose_str_repr(uph);
            _add_proxy_property(uph, "min_downstream_level", stm::unit::pump_constraint_, min_downstream_level, "Minimum level downstream pump.");

            auto uc=py::class_<stm::unit::cost_, py::bases<>, boost::noncopyable>(
                "_Cost",
                doc_intro("Unit.Cost contains the start/stop costs."),
                py::no_init
            );
            expose_str_repr(uc);
            _add_proxy_property(uc, "start", stm::unit::cost_, start, "Start cost, time series.")
            _add_proxy_property(uc, "stop", stm::unit::cost_, stop, "Stop cost, time series.")
            _add_proxy_property(uc, "pump_start", stm::unit::cost_, pump_start, "Start pump cost, time series.")
            _add_proxy_property(uc, "pump_stop", stm::unit::cost_, pump_stop, "Stop pump cost, time series.")

            {
                py::scope ud_scope=ud;
                auto udc=py::class_<stm::unit::discharge_::constraint_, py::bases<>, boost::noncopyable>(
                    "_Constraint", 
                    doc_intro("Constraints and limitations to the unit-flow."),
                    py::no_init
                );
                expose_str_repr(udc);
                _add_proxy_property(udc, "min", stm::unit::discharge_, constraint_::min, "Discharge constraint minimum, time series.")
                _add_proxy_property(udc, "max", stm::unit::discharge_, constraint_::max, "Discharge constraint maximum, time series.")
                _add_proxy_property(udc, "max_from_downstream_level", stm::unit::discharge_,constraint_::max_from_downstream_level, "Discharge maximum, as a function of downstream pressure/water level.")
            }

            {
                py::scope up_scope=up;
                auto upc=py::class_<stm::unit::production_::constraint_, py::bases<>,boost::noncopyable>(
                    "_Constraint", 
                    doc_intro("Contains the effect constraints to the unit."),
                    py::no_init
                );
                expose_str_repr(upc);
                _add_proxy_property(upc, "min", stm::unit::production_, constraint_::min, "Production constraint minimum, time series.")
                _add_proxy_property(upc, "max", stm::unit::production_, constraint_::max, "Production constraint maximum, time series.")
            }

            auto r=py::class_<stm::unit::reserve_, py::bases<>, boost::noncopyable>(
                "_Reserve",
                doc_intro("Unit._Reserve contains all operational reserve related attributes."),
                py::no_init
            );
            expose_str_repr(r);
            r
            .def_readonly("fcr_n", &stm::unit::reserve_::fcr_n,"FCR_n up, down attributes.")
            .def_readonly("fcr_d", &stm::unit::reserve_::fcr_d,"FCR_d up, down attributes.")
            .def_readonly("afrr", &stm::unit::reserve_::afrr,"aFRR up, down attributes.")
            .def_readonly("mfrr", &stm::unit::reserve_::mfrr,"mFRR up, down attributes.")
            .def_readonly("frr", &stm::unit::reserve_::frr,"FRR up, down attributes.")
            .def_readonly("rr", &stm::unit::reserve_::rr,"RR up, down attributes.")
            .def_readonly("droop", &stm::unit::reserve_::droop,"Droop attributes, related/common to fcr settings.")
            ;
            _add_proxy_property(r, "fcr_static_min", stm::unit::reserve_, fcr_static_min, "[W] Unit min-limit valid for FCR calculations (otherwise long running min is used).")
            _add_proxy_property(r, "fcr_static_max", stm::unit::reserve_, fcr_static_max, "[W] Unit max-limit valid for FCR calculations (otherwise long running max is used).")
            _add_proxy_property(r, "fcr_mip", stm::unit::reserve_, fcr_mip, "FCR flag.")
            _add_proxy_property(r, "mfrr_static_min", stm::unit::reserve_, mfrr_static_min, "mFRR minimum value.")
            _add_proxy_property(r, "droop_steps", stm::unit::reserve_, droop_steps,"General discrete droop steps, x=step number, y=droop settings value.")

            {
                py::scope r_scope=r;
                auto rs=py::class_<stm::unit::reserve_::spec_, py::bases<>,boost::noncopyable>(
                    "_Spec", 
                    doc_intro("Describes reserve specification, (.schedule, or min..result..max) SI-units is W, or droop (%)."),
                    py::no_init
                );
                _add_proxy_property(rs, "schedule", stm::unit::reserve_::spec_, schedule, "Reserve schedule.")
                _add_proxy_property(rs, "min", stm::unit::reserve_::spec_, min, "Reserve minimum of range if no schedule.")
                _add_proxy_property(rs, "max", stm::unit::reserve_::spec_, max, "Reserve minimum of range if no schedule.")
                _add_proxy_property(rs, "cost", stm::unit::reserve_::spec_, cost, "Reserve cost.")
                _add_proxy_property(rs, "result", stm::unit::reserve_::spec_, result, "Reserve result.")
                _add_proxy_property(rs, "penalty", stm::unit::reserve_::spec_, penalty, "Reserve penalty.")
                _add_proxy_property(rs, "realised", stm::unit::reserve_::spec_, realised, "Reserve realised.")

                expose_str_repr(rs);
                auto rp=py::class_<stm::unit::reserve_::pair_, py::bases<>,boost::noncopyable>(
                    "_Pair", 
                    doc_intro("Describes the up and down pair of reserve specification."),
                    py::no_init
                );
                rp.def_readonly("up", &stm::unit::reserve_::pair_::up, "Up reserve specification.");
                rp.def_readonly("down", &stm::unit::reserve_::pair_::down, "Down reserve specification.");
                expose_str_repr(rp);
            }

            auto upq=py::class_<stm::unit::production_discharge_relation_, py::bases<>, boost::noncopyable>(
                    "_ProductionDischargeRelation",
                    doc_intro("Describes the production discharge relation (PQ) curves of the unit."),
                    py::no_init
            );
            expose_str_repr(upq);
            _add_proxy_property(upq, "original", stm::unit, production_discharge_relation_::original, "Original PQ curve that includes non-convex regions, x=flow[m3/s], y=effect[W].")
            _add_proxy_property(upq, "convex", stm::unit, production_discharge_relation_::convex, "Convexified PQ curve that includes all the time-dependent operating limits and removes all the nonconcave points of the original PQ curve, the slope of each segment is non-increasing, x=flow[m3/s], y=effect[W].")
            _add_proxy_property(upq, "final", stm::unit, production_discharge_relation_::final, "Final PQ curve that is the final form included into the MILP optimization problem, the first point of the convex PQ curve is extended to Q=0, x=flow[m3/s], y=effect[W].")

            auto ubp=py::class_<stm::unit::best_profit_, py::bases<>, boost::noncopyable>(
                    "_BestProfit",
                    doc_intro("Describes the best profit (BP) curves of the unit."),
                    py::no_init
            );
            expose_str_repr(ubp);
            _add_proxy_property(ubp, "production", stm::unit, best_profit_::production, "Optimal production for the unit given a certain production level of the plant.")
            _add_proxy_property(ubp, "discharge", stm::unit, best_profit_::discharge, "Optimal discharge for the generator given a certain production level of the plant.")
            _add_proxy_property(ubp, "discharge_production_ratio", stm::unit, best_profit_::discharge_production_ratio, "Ratio between discharge and production for the generator given a certain production level of the plant.")
            _add_proxy_property(ubp, "operating_zone", stm::unit, best_profit_::operating_zone, "Optimal operating zone or Pelton needle combination for the generator given a certain production level of the plant.")
        }
    }
}
