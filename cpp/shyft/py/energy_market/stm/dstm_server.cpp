#include <mutex>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>
#include <stdexcept>

#include <boost/thread/locks.hpp>
#include <fmt/core.h>

#include <shyft/energy_market/stm/context.h>
#include <shyft/energy_market/stm/srv/dstm/client.h>
#include <shyft/energy_market/stm/srv/dstm/msg_defs.h>
#include <shyft/energy_market/stm/srv/dstm/server.h>
#include <shyft/energy_market/stm/srv/compute/manager.h>
#include <shyft/energy_market/stm/srv/dstm/server_logger.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/api/expose_optional.h>
#include <shyft/py/api/reflection.h>
#include <shyft/py/energy_market/py_model_client_server.h>
#include <shyft/py/energy_market/py_object_ext.h>
#include <shyft/py/scoped_gil.h>
#include <shyft/srv/db.h>
#include <shyft/web_api/energy_market/request_handler.h>

namespace shyft::energy_market::stm::srv::dstm {

    using shyft::py::scoped_gil_release;
    using shyft::py::scoped_gil_aquire;
    using shyft::web_api::energy_market::request_handler;
    namespace py=boost::python;

    struct py_client {
        std::mutex mx; ///< to enforce just one thread active on this client object at a time
        client impl;
        py_client(const std::string& host_port,int timeout_ms):impl{host_port,timeout_ms} {}
        ~py_client() { }

        std::string get_host_port() {
            return impl.c.host_port;
        }
        int get_timeout_ms() {
            return impl.c.timeout_ms;
        }
        bool is_open() const {
            return impl.c.is_open;
        }
        size_t get_reconnect_count() const {
            return impl.c.reconnect_count;
        }

        void close() {
            scoped_gil_release gil;
            std::unique_lock lck(mx);
            impl.close();
        }

        std::string get_server_version() {
            scoped_gil_release gil;
            std::unique_lock lck(mx);
            return impl.get_version_info();
        }

        bool create_model(std::string const& mid) {
            scoped_gil_release gil;
            std::unique_lock lck(mx);
            return impl.create_model(mid);
        }

        bool add_model(std::string const& mid, stm_system_ mdl){
            scoped_gil_release gil;
            std::unique_lock lck(mx);
            return impl.add_model(mid, mdl);
        }

        bool remove_model(std::string const& mid) {
            scoped_gil_release gil;
            std::unique_lock lck(mx);
            return impl.remove_model(mid);
        }

        bool clone_model(std::string const& old_mid, std::string const& new_mid) {
            scoped_gil_release gil;
            std::unique_lock lck(mx);
            return impl.clone_model(old_mid, new_mid);
        }

        bool rename_model(std::string const& old_mid, std::string const& new_mid) {
            scoped_gil_release gil;
            std::unique_lock lck(mx);
            return impl.rename_model(old_mid, new_mid);
        }

        std::vector<std::string> get_model_ids() {
            scoped_gil_release gil;
            std::unique_lock lck(mx);
            return impl.get_model_ids();
        }

        auto get_model_infos() {
            scoped_gil_release gil;
            std::unique_lock lck(mx);
            return impl.get_model_infos();
        }

        stm_system_ get_model(std::string const& mid) {
            scoped_gil_release gil;
            std::unique_lock lck(mx);
            return impl.get_model(mid);
        }

        bool optimize(const std::string& mid, const generic_dt& ta, const std::vector<shop::shop_command>& cmd,bool opt_only){
            scoped_gil_release gil;
            std::unique_lock lck(mx);
            return impl.optimize(mid, ta, cmd,opt_only);
        }

        bool start_tune(const std::string& mid){
            scoped_gil_release gil;
            std::unique_lock<std::mutex> lck(mx);
            return impl.start_tune(mid);
        }
        bool tune(const std::string& mid, const generic_dt& ta, const std::vector<shop::shop_command>& cmd){
            scoped_gil_release gil;
            std::unique_lock<std::mutex> lck(mx);
            return impl.tune(mid, ta, cmd);
        }
        bool stop_tune(const std::string& mid){
            scoped_gil_release gil;
            std::unique_lock<std::mutex> lck(mx);
            return impl.stop_tune(mid);
        }

        std::vector<log_entry> get_log(const std::string& mid){
            scoped_gil_release gil;
            std::unique_lock lck(mx);
            return impl.get_log(mid);
        }

        model_state get_state(const std::string& mid){
            scoped_gil_release gil;
            std::unique_lock lck(mx);
            return impl.get_state(mid);
        }
        auto evaluate_ts(const time_series::dd::ats_vector &tsv, utcperiod bind_period, bool use_ts_cached_read, bool update_ts_cache, utcperiod clip_period) {
            scoped_gil_release gil;
            std::unique_lock lck(mx);
            return impl.evaluate_ts(tsv, bind_period, use_ts_cached_read, update_ts_cache, clip_period);
        }

        bool evaluate_model(const std::string& mid, utcperiod bind_period, bool use_ts_cached_read, bool update_ts_cache, utcperiod clip_period) {
            scoped_gil_release gil;
            std::unique_lock lck(mx);
            return impl.evaluate_model(mid, bind_period, use_ts_cached_read, update_ts_cache, clip_period);
        }

        bool fx(const std::string& mid, const std::string &fx_arg){
            scoped_gil_release gil;
            std::unique_lock lck(mx);
            return impl.fx(mid,fx_arg);
        }

        ats_vector get_ts(const std::string &mid,const std::vector<std::string> &ts_urls) {
            scoped_gil_release gil;
            std::unique_lock lck(mx);
            return impl.get_ts(mid,ts_urls);
        }
        void set_ts(const std::string &mid,const ats_vector &tsv) {
            scoped_gil_release gil;
            std::unique_lock lck(mx);
            impl.set_ts(mid,tsv);
        }
        auto add_compute_server(const std::string &host_port) {
            scoped_gil_release gil;
            std::unique_lock lck(mx);
            return impl.add_compute_server(host_port);
        }
        optimization_summary_ get_optimization_summary(std::string const&mid) {
            scoped_gil_release gil;
            std::unique_lock lck(mx);
            return impl.get_optimization_summary(mid);
        }
        bool kill_optimization(const std::string& mid) {
            scoped_gil_release gil;
            std::unique_lock lck(mx);
            return impl.kill_optimization(mid);
        }
        //NOTE: just do manual conversions for now - jeh
        py::object set_attrs(const py::list &py_attrs){
            std::vector<std::pair<std::string,any_attr>> attrs;
            for(auto i:std::views::iota(0l,py::len(py_attrs))){
              py::extract<std::pair<std::string,any_attr>> py_attr_extract(py_attrs[i]);
              if(!py_attr_extract.check())
                throw std::runtime_error(
                  fmt::format("Invalid conversion from python object to pair of string and dstm attribute on {}'th element.",
                              i));
              attrs.push_back(py_attr_extract());
            }
            auto result = [&]{
              scoped_gil_release gil;
              std::unique_lock lck(mx);
              return impl.set_attrs(attrs);
            }();
            py::list py_result;
            for(bool v:result)
              py_result.append(v);
            return py_result;
        }
        auto get_attrs(const std::vector<std::string> &attrs) {
            auto result = [&]{
              scoped_gil_release gil;
              std::unique_lock lck(mx);
              return impl.get_attrs(attrs);
            }();
            py::list py_result;
            for(auto attr:result)
              if(attr)
                py_result.append(*attr);
              else
                py_result.append(py::object());
            return py_result;
        }
        bool reset_model(const std::string& mid) {
            scoped_gil_release gil;
            std::unique_lock lck(mx);
            return impl.reset_model(mid);
        }
        bool patch(const std::string& mid, stm::stm_patch_op op, stm::stm_system_ const&p) {
            scoped_gil_release gil;
            std::unique_lock lck(mx);
            return impl.patch(mid,op,p);
        }
        auto compute_server_status(){
          auto result = [&]{
            scoped_gil_release gil;
            std::unique_lock lck(mx);
            return impl.compute_server_status();
          }();
          py::list pyresult{};
          for(const auto &r:result)
            pyresult.append(py::object(r));
          return pyresult;
        }

    };

    /** @brief  Server implementation
     *
     *  TODO: consider use server as impl instead of inheritance, and use scoped_gil for all python exposed calls
     */
    struct py_server:server {
        request_handler bg_server;///< this object handle the requests from the web-api
        std::future<int> web_srv; ///< mutex
        py::object py_fx_cb;///< python callback that can be set by the py user

        std::string web_api_ip;///< Listening IP for web_api
        int web_api_port;////< Listening port for web API
        /** we need to handle errors when executing the user python code,
         */
        void handle_pyerror() {
            // from SO: https://stackoverflow.com/questions/1418015/how-to-get-python-exception-text
            namespace py=boost::python;
            using namespace boost;
            std::string msg{"unspecified error"};
            if(PyErr_Occurred()) {
                PyObject *exc,*val,*tb;
                py::object formatted_list, formatted;
                PyErr_Fetch(&exc,&val,&tb);
                py::handle<> hexc(exc),hval(py::allow_null(val)),htb(py::allow_null(tb));
                py::object traceback(py::import("traceback"));
                if (!tb) {
                    py::object format_exception_only{ traceback.attr("format_exception_only") };
                    formatted_list = format_exception_only(hexc,hval);
                } else {
                    py::object format_exception{traceback.attr("format_exception")};
                    if (format_exception) {
                         try {
                            formatted_list = format_exception(hexc, hval, htb);
                        } catch (...) { // any error here, and we bail out, no crash please
                            msg = "not able to extract exception info";
                        }
                    } else
                        msg="not able to extract exception info";
                }
                if (formatted_list) {
                    formatted = py::str("\n").join(formatted_list);
                    msg = py::extract<std::string>(formatted);
                }
            }
            py::handle_exception();
            PyErr_Clear();
            throw std::runtime_error(msg);
        }
        void py_do_notify_change(std::vector<std::string> const& urls) {
            scoped_gil_release gil;
            if(urls.size()==0) return;
            sm->notify_change(urls);
        }

        /** this is where we attempt to fire user specified callback, */
        bool py_do_fx(std::string const & mid, std::string const& fx_arg) {
            bool r{false};
            if (py_fx_cb.ptr() != Py_None) {
                scoped_gil_aquire gil;// we need to use the GIL here before trying to call python.
                try {
                    r = boost::python::call<bool>(py_fx_cb.ptr(), mid,fx_arg);
                } catch  (const boost::python::error_already_set&) {
                    handle_pyerror();
                }
            }
            return r;
        }

        py_server() {
            if (!PyEval_ThreadsInitialized()) {
                PyEval_InitThreads(); // ensure threads is enabled
            }
            bg_server.srv = this;
            // rig the c++ fx_cb to forward calls to this python-layer
            fx_cb = [&](std::string mid,std::string fx_args)->bool { return this->py_do_fx(mid,fx_args); };
        }

        void start_web_api(std::string host_ip, int port, std::string doc_root, int fg_threads, int bg_threads, bool tls_only){
            scoped_gil_release gil;
            if (!web_srv.valid()) {
                web_api_port = port;
                web_api_ip = host_ip;
                web_srv = std::async(std::launch::async,
                    [this, host_ip, port, doc_root, fg_threads, bg_threads,tls_only]()->int {
                        return shyft::web_api::run_web_server(
                            bg_server,
                            host_ip,
                            static_cast<unsigned short>(port),
                            std::make_shared<std::string>(doc_root),
                            fg_threads,
                            bg_threads,
                            tls_only // accept_plain == !tls_only
                            );
                    }
                );
            }
        }

        void stop_web_api() {
            scoped_gil_release gil;
            if(web_srv.valid()) {
                std::raise(SIGINT);
                (void) web_srv.get();
            }
        }

        int get_web_api_port() {
            if (web_srv.valid()) {
                return web_api_port;
            } else {
                return -1;
            }
        }

        std::string get_web_api_ip() {
            if (web_srv.valid()) {
                return web_api_ip;
            } else {
                return "";
            }
        }
        auto py_optimize(const std::string& mid, const generic_dt& ta, const std::vector<shop::shop_command>& cmd, bool opt_only) {
            scoped_gil_release gil;
            return do_optimize(mid,ta,cmd,opt_only);
        }
        auto py_evaluate_model(const std::string& mid, utcperiod bind_period, bool use_ts_cached_read, bool update_ts_cache, utcperiod clip_period) {
            scoped_gil_release gil;
            return do_evaluate_model(mid,bind_period,use_ts_cached_read,update_ts_cache,clip_period);
        }
        auto py_clone_model(std::string const& old_mid, std::string new_mid) {
            scoped_gil_release gil;
            return do_clone_model(old_mid,new_mid);
        }
        auto py_remove_model(std::string const& mid){
            scoped_gil_release gil;
            return do_remove_model(mid);
        }
        auto py_rename_model(std::string old_mid, std::string new_mid) {
            scoped_gil_release gil;
            return do_rename_model(old_mid,new_mid);
        }
        auto py_get_state(std::string const& mid) {
            scoped_gil_release gil;
            return do_get_state(mid);
        }
        auto py_get_model_ids() {
            scoped_gil_release gil;
            return do_get_model_ids();
        }
        auto py_get_model_infos() {
            scoped_gil_release gil;
            return do_get_model_infos();
        }
        auto py_add_model(std::string const& mid, stm_system_ mdl) {
            scoped_gil_release gil;
            return do_add_model(mid,mdl);
        }
        auto py_create_model(std::string const& mid){
            scoped_gil_release gil;
            return do_create_model(mid);
        }
        auto add_compute_server(const std::string& mid){
            scoped_gil_release gil;
            return do_add_compute_server(mid);
        }
        void stop_server(int ms) {
            scoped_gil_release gil;
            if(ms>0)
                server::set_graceful_close_timeout(ms);
            server::clear();
        }

        py::object py_apply(std::string mid,py::object action){
          scoped_gil_release gil;
          return models.mutate_or_throw(
            mid,
            [&](auto &&view){
              scoped_gil_aquire gil;
              try {
                return boost::python::call<py::object>(action.ptr(), view.model);
              } catch  (const boost::python::error_already_set&) {
                handle_pyerror();
                return py::object();
              }
            }).get();
        }

    };
}


namespace expose {
    using namespace boost::python;
    namespace py = boost::python;
    using std::string;
    using std::shared_ptr;

    using server = shyft::energy_market::stm::srv::dstm::py_server;
    using client = shyft::energy_market::stm::srv::dstm::py_client;
    using shyft::srv::model_info;
    using shyft::energy_market::stm::stm_system;
    using shyft::energy_market::stm::stm_system_;
    using namespace shyft::energy_market::stm::srv;
    using namespace shyft::energy_market::stm::srv::dstm;

    void dstm_client_server() {

        {
          using shyft::energy_market::stm::model_state;
          auto t_model_state =
            py::enum_<model_state>(
              "ModelState",
              doc_intro("Describes the possible state of the STM model"));
          shyft::py::expose_enumerators(t_model_state);
        }
        {
          using shyft::energy_market::stm::srv::compute::managed_server_state;
          auto t_managed_server_state =
            py::enum_<managed_server_state>(
              "ComputeServerState",
              doc_intro("Describes the possible states of a managed compute server"));
          shyft::py::expose_enumerators(t_managed_server_state);
        }
        {
          using shyft::energy_market::stm::srv::compute::managed_server_status;
          auto t_managed_server_state =
            py::class_<managed_server_status>(
              "ComputeServerStatus",
              doc_intro("Status of a managed compute server"))
            .def("__str__",
                 +[](const managed_server_status &s){
                   return fmt::format("{}",s);
                 })
            ;
          shyft::py::expose_members(t_managed_server_state);
        }
        {
            using shyft::energy_market::stm::stm_patch_op;
            auto t_patch_op=
            py::enum_<stm_patch_op>(
                "StmPatchOperation",
                doc_intro("Describes how a patch should be applied to the STM model")
            );
            shyft::py::expose_enumerators(t_patch_op);
        }
        class_<std::map<std::string, model_info,std::less<>>>("ModelInfoDict",
            doc_intro("A map from model keys to ModelInfo, containing skeleton information about a model."),
            init<>((py::arg("self")))
            )
            .def(py::map_indexing_suite<std::map<std::string, model_info,std::less<>>>());

        
        auto py_compute_srv = py::class_<compute::server, boost::noncopyable>(
            "ComputeServer",
            doc_intro("A server object running STM computations."))
            .def("start_server", &compute::server::start_server)
            .def("stop_server",
                 +[](compute::server &server,int ms){
                     scoped_gil_release gil;
                     if(ms>0)
                         server.set_graceful_close_timeout(ms);
                     server.clear();
                 },
                 (py::arg("self"),py::arg("timeout")=1000),
                 doc_intro("stop serving connections, gracefully.")
                 doc_see_also("start_server()"))
            .def("is_running", &compute::server::is_running)
            .def("set_listening_port", &compute::server::set_listening_port, (py::arg("self"),py::arg("port_no")),
                doc_intro("set the listening port for the service")
                doc_parameters()
                doc_parameter("port_no","int","a valid and available tcp-ip port number to listen on.")
                doc_paramcont("typically it could be 20000 (avoid using official reserved numbers)")
                doc_returns("nothing","None",""))
            .def("get_listening_port",&compute::server::get_listening_port, (py::arg("self")),
                 "returns the port number it's listening at for serving incoming request")
            .def("get_listening_ip", &compute::server::get_listening_ip, (py::arg("self")),
                 doc_intro("return the ip adress the server is listening on for serving incoming requests"))
            .def("set_listening_ip", &compute::server::set_listening_ip, (py::arg("self"),py::arg("ip")),
                doc_intro("set the listening port for the service")
                doc_parameters()
                doc_parameter("ip","str","ip or host-name to start listening on")
                doc_returns("nothing","None",""))
            .def("close",&compute::server::clear,(py::arg("self")),doc_intro("close and stop serving requests"))
            ;


        auto py_dstm = class_<server, boost::noncopyable>("DStmServer",
            doc_intro("A server object serving distributed, 'live' STM systems.\n"
                "The server contains an DTSS that handles time series for the models stored in the server."),
            init<>((py::arg("self")))
            )
            .def("start_server", &server::start_server)
            .def("set_listening_port", &server::set_listening_port, (py::arg("self"),py::arg("port_no")),
                doc_intro("set the listening port for the service")
                doc_parameters()
                doc_parameter("port_no","int","a valid and available tcp-ip port number to listen on.")
                doc_paramcont("typically it could be 20000 (avoid using official reserved numbers)")
                doc_returns("nothing","None","")
            )
            .def("add_container", &server::add_container, (py::arg("self"), py::arg("container_name"), py::arg("root_dir")),
                 doc_intro("Add a container to the server's DTSS.")
                 doc_parameters()
                 doc_parameter("container_name", "str", "name of container to create.")
                 doc_parameter("root_dir", "str", "Directory where container's time series are stored.")
                 doc_returns("nothing", "None", "")
            )
            .def("get_listening_port",&server::get_listening_port, (py::arg("self")),
                "returns the port number it's listening at for serving incoming request"
            )
            .def("get_listening_ip", &server::get_listening_ip, (py::arg("self")),
                doc_intro("return the ip adress the server is listening on for serving incoming requests")
            )

            .def("set_listening_ip", &server::set_listening_ip, (py::arg("self"),py::arg("ip")),
                doc_intro("set the listening port for the service")
                doc_parameters()
                doc_parameter("ip","str","ip or host-name to start listening on")
                doc_returns("nothing","None","")
            )
            .def("do_get_version_info", &server::do_get_version_info, (py::arg("self")),
                "returns the version number of the StsServer"
            )
            .def("set_master_slave_mode",&server::set_master,(py::arg("self"),py::arg("ip"),py::arg("port"),py::arg("master_poll_time"),py::arg("unsubscribe_threshold"),py::arg("unsubscribe_max_delay")),
                doc_intro(
                    "Set master-slave mode, redirecting all IO calls on this dtss to the master ip:port dtss.\n"
                    "This instance of the dtss is kept in sync with changes done on the master using subscription to changes on the master\n"
                    "Calculations, and caches are still done locally unloading the computational efforts from the master."
                )
                doc_parameters()
                doc_parameter("ip","str","The ip address where the master dtss is running")
                doc_parameter("port","int","The port number for the master dtss")
                doc_parameter("master_poll_time","time","[s] max time between each update from master, typicall 0.1 s is ok")
                doc_parameter("unsubscribe_threshold","int","minimum number of unsubscribed time-series before also unsubscribing from the master")
                doc_parameter("unsubscribe_max_delay","time","maximum time to delay unsubscriptions, regardless number")
            )
            .def("do_create_model", &server::py_create_model, (py::arg("self"), py::arg("mid")),
                doc_intro("Create a new model")
                doc_parameters()
                doc_parameter("mid", "str", "ID of new model")
                doc_returns("mdl", "StmSystem", "Empty model with ID set to 'mid'")
                doc_raises()
                doc_raise("RuntimeError", "If 'mid' already is ID of a model")
            )
            .def("do_add_model", &server::py_add_model, (py::arg("self"), py::arg("mid"), py::arg("mdl")),
                doc_intro("Add model to server")
                doc_parameters()
                doc_parameter("mid", "str", "ID/key to store model as.")
                doc_parameter("mdl", "StmSystem", "STM System to store in key 'mid'")
                doc_returns("success", "bool", "Returns True on success")
                doc_raises()
                doc_raise("RuntimeError", "If 'mid' is already an ID of a model.")
            )
            .def("do_remove_model", &server::py_remove_model, (py::arg("self"), py::arg("mid")),
                doc_intro("Remove model by ID.")
                doc_parameters()
                doc_parameter("mid", "str", "ID of model to remove.")
                doc_returns("success", "bool", "Returns True on success.")
                doc_raises()
                doc_raise("RuntimeError", "If model with ID 'mid' does not exist.")
            )
            .def("do_rename_model", &server::py_rename_model, (py::arg("self"), py::arg("old_mid"), py::arg("new_mid")),
                doc_intro("Rename a model")
                doc_parameters()
                doc_parameter("old_mid", "str", "ID of model to rename")
                doc_parameter("new_mid", "str", "New ID of model")
                doc_returns("success", "bool", "Returns True on success.")
                doc_raises()
                doc_raise("RuntimeError", "'new_mid' already stores a model")
                doc_raise("RuntimeError", "'old_mid' doesn't store a model to rename")
            )
            .def("do_clone_model", &server::py_clone_model, (py::arg("self"), py::arg("old_mid"), py::arg("new_mid")),
                doc_intro("Clone existing model with ID.")
                doc_parameters()
                doc_parameter("old_mid", "str", "ID of model to clone")
                doc_parameter("new_mid", "str", "ID to store cloned model against")
                doc_returns("success", "bool", "Returns True on success")
                doc_raises()
                doc_raise("RuntimeError", "'new_mid' already stores a model")
                doc_raise("RuntimeError", "'old_mid' doesn't store a model to clone")
            )
            .def("do_get_model_ids",&server::py_get_model_ids, (py::arg("self")),
                doc_intro("Get IDs of all models stored")
                doc_returns("id_list", "List[str]", "List of model IDs")
            )
            .def("do_get_model_infos", &server::py_get_model_infos, (py::arg("self")),
                doc_intro("Get model infos of all models stored")
                doc_returns("mi_list", "ModelInfoList", "Dict of (ModelKey, ModelInfo) for each model stored.")
                doc_see_also("shyft.energy_market.core.ModelInfo")
            )
            .def("do_get_state", &server::py_get_state, (py::arg("self"), py::arg("mid")),
                 doc_intro("Get state of stored model by ID")
                 doc_parameters()
                 doc_parameter("mid", "str", "ID of model to get state of")
                 doc_returns("state", "ModelState", "State of requested model")
                 doc_raises()
                 doc_raise("RuntimeError", "Unable to find model with ID 'mid'")
             )
            .def_readwrite("fx",&server::py_fx_cb,
                doc_intro("server-side callable function(lambda) that takes two parameters:")
                doc_intro("mid :  the model id")
                doc_intro("fx_arg: arbitrary string to pass to the server-side function")
                doc_intro("The server-side fx is called when the client (or web-api) invoke the c.fx(mid,fx_arg).")
                doc_intro("The signature of the callback function should be fx_cb(mid:str, fx_arg:str)->bool")
                doc_intro("This feature is simply enabling the users to tailor server-side functionality in python!")
                doc_intro("Examples:\n")
                doc_intro(
                    ">>> from shyft.energy_market.stm import Server\n"
                    ">>> s=Server()\n"
                    ">>> def my_fx(mid:str, fx_arg:str)->bool:\n"
                    ">>>     print(f'invoked with mid={mid} fx_arg={fx_arg}')\n"
                    ">>>   # note we can use captured Server s here!"
                    ">>>     return True\n"
                    ">>> # and then bind the function to the callback\n"
                    ">>> s.fx=my_fx\n"
                    ">>> s.start_server()\n"
                    ">>> : # later using client from anywhere to invoke the call\n"
                    ">>> fx_result=c.fx('my_model_id', 'my_args')\n\n"
                )
            )
            .def("do_optimize", &server::py_optimize, (py::arg("self"), py::arg("mid"), py::arg("ta"), py::arg("cmd"),py::arg("compute_node_mode")=false),
                doc_intro("Run SHOP optimization on a model")
                doc_parameters()
                doc_parameter("mid", "str", "ID of model to run optimization on")
                doc_parameter("ta", "TimeAxis", "Time span to run optimization over")
                doc_parameter("cmd", "List[ShopCommand]", "List of SHOP commands")
                doc_parameter("compute_node_mode","bool", "default false, for compute nodes set to true to minimize work done after optimize")
                doc_see_also("ShopCommand")
                doc_returns("success", "bool", "Stating whether optimization was started successfully or not.")
            )
            .def("do_evaluate_model", &server::py_evaluate_model, (py::arg("self"), py::arg("mid"), py::arg("bind_period"), py::arg("use_ts_cached_read")=true, py::arg("update_ts_cache")=false, py::arg("clip_period")=utcperiod{}),
                 doc_intro("Evaluate any unbound time series attributes of a model")
                 doc_parameters()
                 doc_parameter("mid", "str", "ID of model to evaluate")
                 doc_parameter("bind_period", "UtcPeriod", "Period for bind in evaluate.")
                 doc_parameter("use_ts_cached_read", "bool", "allow use of cached results. Use it for immutable data reads!")
                 doc_parameter("update_ts_cache", "bool", "when reading time-series, also update the cache with the data. Use it for immutable data reads!")
                 doc_parameter("clip_period", "UtcPeriod", "Period for clip in evaluate.")
                 doc_see_also("UtcPeriod")
                 doc_returns("bound", "bool", "Returns whether any of the model's attributes had to be bound.")
             )
            .def("start_web_api", &server::start_web_api, (py::arg("self"), py::arg("host_ip"), py::arg("port"),
                py::arg("doc_root"), py::arg("fg_threads")=2, py::arg("bg_threads")=4,py::arg("tls_only")=false ),
                doc_intro("Start a web API for communicating with server")
                doc_parameters()
                doc_parameter("host_ip", "str", "0.0.0.0 for any interface, 127.0.0.1 for local only, etc.")
                doc_parameter("port", "int", "port number to serve the web API on. Ensure it's available!")
                doc_parameter("doc_root", "str", "directory from which we will serve http/https documents.")
                doc_parameter("fg_threads", "int", "number of web API foreground threads, typical 1-4 depending on load.")
                doc_parameter("bg_threads", "int", "number of long running background thread workers to serve requests etc.")
                doc_parameter("tls_only","bool","default false, set to true to enforce tls sessions only.")
            )
            .def("stop_web_api", &server::stop_web_api, (py::arg("self")),
                doc_intro("Stops any ongoing web API service")
            )
            .def("get_web_api_port", &server::get_web_api_port, (py::arg("self")),
                doc_intro("Get listening port for web API. Returns -1 if not running")
            )
            .def("get_web_api_ip", &server::get_web_api_ip, (py::arg("self")),
                doc_intro("Get listening IP for web API. Returns empty string if not running.")
            )
            .def("notify_change", &server::py_do_notify_change, (py::arg("self"), py::arg("urls")),
                 doc_intro("Notify change on model urls, dstm://M..., so that changes are pushed to subscribers.")
                 doc_intro("In case the urls are wrong, misformed etc, there is no exception raised for that.")
                 doc_intro("It's the callers responsibility entirely to provide valid URLs")
                 doc_parameters()
                 doc_parameter("urls", "StringVector", "List of valid model-urls as kept by this server")
             )
            .def("close",&server::clear,(py::arg("self")),doc_intro("close and stop serving requests on the hpc binary socket interface"))
            .def("add_compute_server",&server::add_compute_server,(py::arg("self"),py::arg("host_port")),
                 doc_intro("add a compute node specified by it's address string of form host:port")
                 doc_parameters()
                 doc_parameter("host_port","str","the address of the dstm compute node service in the form of host:port")
            )
            .def("stop_server",&server::stop_server, (py::arg("self"),py::arg("timeout")=1000),
                doc_intro("stop serving connections, gracefully.")
                doc_see_also("start_server()")
            )
            .def("is_running",&server::is_running, (py::arg("self")),
                doc_intro("true if server is listening and running")
                doc_see_also("start_server()")
            )
            .def_readwrite("shared_lock_timeout",&server::shared_lock_timeout,
                           doc_intro("maximum time to wait for shared-lock on models when doing evaluate call(default 200 ms)"))
          .def("apply",&server::py_apply, (py::arg("self"),py::arg("mid"),py::arg("action")),
               doc_intro("Apply an action to a model.")
               doc_intro("")
               doc_intro("The action gets exclusive access to the model when applied.")
               doc_intro("Take care not to return something owned by the model itself,")
               doc_intro("as exclusive access will be lost on return")
               doc_parameters()
               doc_parameter("mid","str","ID of model")
               doc_parameter("action","Callable[[StmSystem],object]", "function to apply to model")
               doc_returns("obj", "object", "Object returned by the action"))
        ;

        class_<client, boost::noncopyable>("DStmClient",
            doc_intro("Client side for the DStmServer")
            doc_intro("")
            doc_intro("Takes care of message exchange to the remote server, using the supplied parameters.")
            doc_intro("It implements the message protocol of the server, sending message-prefix, ")
            doc_intro("arguments, waiting for the response, deserialize the response ")
            doc_intro("and handle it back to the user.")
            doc_see_also("DStmServer"),
            init<string, int>((py::arg("self"),py::arg("host_port"),py::arg("timeout_ms")))
            )
            .def_readonly("host_port", &client::get_host_port, "Endpoint network address of the remote server.")
            .def_readonly("timeout_ms", &client::get_timeout_ms, "Timout for remote server operations, in number milliseconds.")
            .def_readonly("is_open", &client::is_open, "If the connection to the remote server is (still) open.")
            .def_readonly("reconnect_count", &client::get_reconnect_count, "Number of reconnects to the remote server that have been performed.")
            .def("close", &client::close, (py::arg("self")),
                doc_intro("Close the connection. It will automatically reopen if needed.")
            )
            .def("get_server_version",&client::get_server_version, (py::arg("self")),
                doc_intro("Get version of remote server.")
                doc_returns("version", "str", "Server version string")
            )
            .def("create_model",&client::create_model, (py::arg("self"), py::arg("mid")),
                doc_intro("Create an empty model and store it server side.")
                doc_parameters()
                doc_parameter("mid", "str", "ID of new model")
                doc_returns("success", "bool", "Returns true on success")
            )
            .def("add_model", &client::add_model, (py::arg("self"), py::arg("mid"), py::arg("mdl")),
                doc_intro("Add model to server")
                doc_parameters()
                doc_parameter("mid", "str", "ID/key to store model as.")
                doc_parameter("mdl", "StmSystem", "STM System to store in key 'mid'")
                doc_returns("success", "bool", "Returns True on success")
            )
            .def("remove_model", &client::remove_model, (py::arg("self"), py::arg("mid")),
                doc_intro("Remove model by ID.")
                doc_parameters()
                doc_parameter("mid", "str", "ID of model to remove.")
                doc_returns("success", "bool", "Returns True on success.")
            )
            .def("rename_model",&client::rename_model, (py::arg("self"), py::arg("old_mid"), py::arg("new_mid")),
                doc_intro("Rename a model")
                doc_parameters()
                doc_parameter("old_mid", "str", "ID of model to rename")
                doc_parameter("new_mid", "str", "New ID of model")
                doc_returns("success", "bool", "Returns True on success.")
            )
            .def("clone_model", &client::clone_model, (py::arg("self"), py::arg("old_mid"), py::arg("new_mid")),
                doc_intro("Clone existing model with ID.")
                doc_parameters()
                doc_parameter("old_mid", "str", "ID of model to clone")
                doc_parameter("new_mid", "str", "ID to store cloned model against")
                doc_returns("success", "bool", "Returns True on success")
            )
            .def("get_model_ids", &client::get_model_ids, (py::arg("self")),
                doc_intro("Get IDs of all models stored")
                doc_returns("id_list", "List[str]", "List of model IDs")
            )
            .def("get_model_infos", &client::get_model_infos, (py::arg("self")),
                doc_intro("Get model infos of all models stored")
                doc_returns("mi_list", "ModelInfoList", "Dict of (ModelKey, ModelInfo) for each model stored.")
                doc_see_also("shyft.energy_market.core.ModelInfo")
            )
            .def("get_model", &client::get_model, (py::arg("self"), py::arg("mid")),
                doc_intro("Get a stored model by ID")
                doc_parameters()
                doc_parameter("mid", "str", "ID of model to get")
                doc_returns("mdl", "StmSystem", "Requested model")
            )
            .def("get_state", &client::get_state, (py::arg("self"), py::arg("mid")),
                 doc_intro("Get the state of a model by ID")
                 doc_parameters()
                 doc_parameter("mid", "str", "ID of model to get state of")
                 doc_returns("state", "ModelState", "State of requested model")
            )
            .def("optimize", &client::optimize, (py::arg("self"), py::arg("mid"), py::arg("ta"), py::arg("cmd"),py::arg("compute_node_mode")=false),
                doc_intro("Run optimization on a model")
                doc_parameters()
                doc_parameter("mid", "str", "ID of model to run optimization on")
                doc_parameter("ta", "TimeAxis", "Time span to run optimization over")
                doc_parameter("cmd", "List[ShopCommand]", "List of SHOP commands")
                doc_parameter("compute_node_mode","bool", "default false, for compute nodes set to true to minimize work done after optimize")
                doc_see_also("ShopCommand")
                doc_returns("success", "bool", "Stating whether optimization was started successfully or not.")
            )
            .def("start_tune", &client::start_tune, (py::arg("self"), py::arg("mid")),
                doc_intro("Start tuning a model")
                doc_parameters()
                doc_parameter("mid", "str", "ID of model to run optimization on")
                doc_returns("success", "bool", "Stating tuning was started successfully or not.")
            )
            .def("tune", &client::tune, (py::arg("self"), py::arg("mid"), py::arg("ta"), py::arg("cmd")),
                doc_intro("Run tuning optimization on a model")
                doc_parameters()
                doc_parameter("mid", "str", "ID of model to run optimization on")
                doc_parameter("ta", "TimeAxis", "Time span to run optimization over")
                doc_parameter("cmd", "List[ShopCommand]", "List of SHOP commands")
                doc_see_also("ShopCommand")
                doc_returns("success", "bool", "Stating whether tuning optimization was started successfully or not.")
            )
            .def("stop_tune", &client::stop_tune, (py::arg("self"), py::arg("mid")),
                doc_intro("Stop tuning a model")
                doc_parameters()
                doc_parameter("mid", "str", "ID of model to run optimization on")
                doc_returns("success", "bool", "Stating tuning was stopped successfully or not.")
            )
            .def("get_log", &client::get_log, (py::arg("self"), py::arg("mid")),
                 doc_intro("Get log for a model")
                 doc_parameters()
                 doc_parameter("mid", "str", "ID of model to get log for")
                 doc_returns("entries", "ShopLogEntryList", "List of log entries")
            )
            .def("get_optimization_summary", &client::get_optimization_summary, (py::arg("self"), py::arg("mid")),
                 doc_intro("Get the optimization summary of a model by ID")
                 doc_parameters()
                 doc_parameter("mid", "str", "ID of model to get state of")
                 doc_returns("summary", "", "The summary of last successful optimization. Values will be nan if not available")
            )
            .def("kill_optimization", &client::kill_optimization, (py::arg("self"), py::arg("mid")),
                 doc_intro("Request to kill a running optimization for a model by ID")
                 doc_parameters()
                 doc_parameter("mid", "str", "ID of model to get state of")
                 doc_returns("success", "bool", "true if the server-side accepted the kill signal (optimization was running)")
            )
            .def("evaluate_ts", &client::evaluate_ts, (py::arg("self"), py::arg("tsv"), py::arg("bind_period"), py::arg("use_ts_cached_read")=true, py::arg("update_ts_cache")=false, py::arg("clip_period")=utcperiod{}),
                 doc_intro("Evaluate any time series expressions")
                 doc_parameters()
                 doc_parameter("tsv", "TsVector", "Timeseries to evaluate")
                 doc_parameter("bind_period", "UtcPeriod", "Period for bind in evaluate.")
                 doc_parameter("use_ts_cached_read", "bool", "allow use of cached results. Use it for immutable data reads!")
                 doc_parameter("update_ts_cache", "bool", "when reading time-series, also update the cache with the data. Use it for immutable data reads!")
                 doc_parameter("clip_period", "UtcPeriod", "Period for clip in evaluate.")
                 doc_see_also("UtcPeriod")
                 doc_returns("result", "TsVector", "Returns the evaluated timeseries.")
             )
            .def("evaluate_model", &client::evaluate_model, (py::arg("self"), py::arg("mid"), py::arg("bind_period"), py::arg("use_ts_cached_read")=true, py::arg("update_ts_cache")=false, py::arg("clip_period")=utcperiod{}),
                 doc_intro("Evaluate any unbound time series attributes of a model")
                 doc_parameters()
                 doc_parameter("mid", "str", "ID of model to evaluate")
                 doc_parameter("bind_period", "UtcPeriod", "Period for bind in evaluate.")
                 doc_parameter("use_ts_cached_read", "bool", "allow use of cached results. Use it for immutable data reads!")
                 doc_parameter("update_ts_cache", "bool", "when reading time-series, also update the cache with the data. Use it for immutable data reads!")
                 doc_parameter("clip_period", "UtcPeriod", "Period for clip in evaluate.")
                 doc_see_also("UtcPeriod")
                 doc_returns("bound", "bool", "Returns whether any of the model's attributes had to be bound.")
             )
            .def("fx", &client::fx, (py::arg("self"), py::arg("mid"),py::arg("fx_arg")),
                 doc_intro("Execute the serverside fx, passing supplied arguments")
                 doc_parameters()
                 doc_parameter("mid", "str", "ID of model for the server-side fx")
                 doc_parameter("fx_arg","str","any argument passed to the server-side fx")
                 doc_returns("success", "bool", "true if call successfully done")
            )
            .def("get_ts", &client::get_ts, (py::arg("self"), py::arg("mid"),py::arg("ts_urls")),
                 doc_intro("Get the time-series from the model mid, as specified by ts_urls")
                 doc_parameters()
                 doc_parameter("mid", "str", "ID of model")
                 doc_parameter("ts_urls","StringVector","Strongly typed list of strings, urls, like dstm://Mmid/..")
                 doc_returns("time-series", "TsVector", "list of time-series as specifed by the list of ts_urls, same order")
            )
            .def("set_ts", &client::set_ts, (py::arg("self"), py::arg("mid"),py::arg("tsv")),
                 doc_intro("Set the time-series in the model mid, as specified by ts_urls in the tsv.")
                 doc_intro("If anyone is subscribers on time-series, or expressions affected, they are notified")
                 doc_parameters()
                 doc_parameter("mid", "str", "ID of model")
                 doc_parameter("tsv","TsVector","list of TimeSeries(ts_url,ts_with_values) ,ts_url, like dstm://Mmid/..")
            )
            .def("add_compute_server",&client::add_compute_server,(py::arg("self"),py::arg("host_port")),
                 doc_intro("add a compute node specified by it's address string of form host:port")
                 doc_parameters()
                 doc_parameter("host_port","str","the address of the dstm compute node service in the form of host:port")
            )
            .def("set_attrs", &client::set_attrs, (py::arg("self"), py::arg("attrs")),
                 doc_intro("Set a list of attributes specified by url.")
                 doc_intro("If anyone is subscribed on the attribute they are notified")
                 doc_parameters()
                 doc_parameter("attrs","list","list of pairs of dstm url and attribute.")
                 doc_returns("attrs","list[bool]","list of booleans indicating if attribute was set."))
            .def("get_attrs", &client::get_attrs, (py::arg("self"), py::arg("urls")),
                 doc_intro("Get a list of attributes specified by url.")
                 doc_intro("If an url is unable to be resolved None is returned in place of the attribute.")
                 doc_parameters()
                 doc_parameter("urls","list","list of dstm attribute urls."),
                 doc_returns("attrs","list","list of dstm attributes."))
            .def("reset_model", &client::reset_model, (py::arg("self"), py::arg("mid")),
                 doc_intro("Reset the model specified by mid.")
                 doc_parameters()
                 doc_parameter("mid","str","ID of model")
                 doc_returns("ok","bool","whether or not the model was reset."))
            .def("patch", &client::patch, (py::arg("self"), py::arg("mid"),py::arg("op"),py::arg("p")),
                 doc_intro("Patch the model `mid` with patch `p` using operation `op`.")
                 doc_parameters()
                 doc_parameter("mid","str","ID of model")
                 doc_parameter("op","","Operation is one of ADD,REMOVE_RELATIONS,REMOVE_OBJECTS")
                 doc_parameter("p","","The StmSystem describing the patch")
                 doc_returns("ok","bool","whether or not the model was reset."))
            .def("compute_server_status", &client::compute_server_status, (py::arg("self")),
                 doc_intro("Get status of managed compute servers")
                 doc_returns("status","List[ComputeServerStatus]","status of managed compute servers."))
            ;

    }

    void dstm_server_logging() {
        auto stm_log_levels = class_<dlib::log_level, boost::noncopyable>("LogLevel",
                                                    doc_intro("Logging level for STM logging utility."),
                                                    no_init
                                                   )
            .def(py::init<int, const char*>((py::arg("priority"),py::arg("name"))))
            .def_readonly("priority", &dlib::log_level::priority)
            .add_property("name", +[](dlib::log_level& ll) -> string { return string(ll.name); });
        // DLIB LOGGING LEVELS:
        // The naive approach of setting py::scope().attr("LALL") = dlib::LALL;
        // yields a SystemError with unexpected Exception on the python side. (Possibly due to my misunderstanding of the lifetime of globals)
        // Work around for now is to hard code log levels from http://dlib.net/dlib/logger/logger_kernel_abstract.h.html
        py::scope().attr("LALL") = stm_log_levels(std::numeric_limits<int>::min(), "ALL");
        py::scope().attr("LNONE") = stm_log_levels(std::numeric_limits<int>::max(), "NONE");
        py::scope().attr("LTRACE") = stm_log_levels(-100, "TRACE");
        py::scope().attr("LDEBUG") = stm_log_levels(0, "DEBUG");
        py::scope().attr("LINFO") = stm_log_levels(100, "INFO");
        py::scope().attr("LWARN") = stm_log_levels(200, "WARN");
        py::scope().attr("LERROR") = stm_log_levels(300, "ERROR");
        py::scope().attr("LFATAL") = stm_log_levels(400, "FATAL");

        class_<server_log_hook, boost::noncopyable>("LogConfig",
                                                    doc_intro("Class for controlling logging configuration"))
        .def(
            py::init<string>((py::arg("self"),py::arg("file")))
         )
        ;


        def("configure_logger", &configure_logger, (py::arg("config"),py::arg("log_level")),
            doc_intro("Configure logger with log level and what file to write to")
            doc_parameters()
            doc_parameter("config", "LogConfig", "Configuration for logger")
            doc_parameter("log_level", "LogLevel", "What log level is the lowest to report. ALL < TRACE < DEBUG < INFO < WARN < ERROR < FATAL")
            doc_returns("nothing", "None", "")
        );
    }

}
