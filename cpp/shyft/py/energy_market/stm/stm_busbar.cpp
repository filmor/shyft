/** This file is part of Shyft. Copyright 2015-2022 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/api/expose_from_list.h>
#include <shyft/py/energy_market/py_url_tag.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/energy_market/stm/expose_str.h>
#include <shyft/py/energy_market/py_tsm_expose.h>

#include <fmt/core.h>
#include <shyft/energy_market/stm/busbar.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/power_module.h>

namespace py=boost::python;

namespace expose {
    using namespace shyft::energy_market;

    using std::string;
    using std::shared_ptr;

    template<> string str_(stm::busbar const& o) {
        return fmt::format("Busbar(id={}, name='{}')"
            ,o.id
            ,o.name
        );
    }

    template<> string str_(stm::busbar::ts_triplet const& o) {
        return fmt::format("_TsTriplet(realised={},schedule={},result={})"
            ,str_(o.realised)
            ,str_(o.schedule)
            ,str_(o.result)
        );
    }

    template<> string str_(stm::unit_member const& o) {
        return fmt::format("_UnitMember(unit.id={}, unit.name='{}')"
            ,o.unit->id
            ,o.unit->name
        );
    }

    template<> string str_(stm::power_module_member const& o) {
        return fmt::format("_PowerModuleMember(unit.id={}, unit.name='{}')"
            ,o.power_module->id
            ,o.power_module->name
        );
    }

    void stm_busbar() {
        auto c=py::class_<
            stm::busbar,
            py::bases<>,
            shared_ptr<stm::busbar>,
            boost::noncopyable
        >("Busbar",
          doc_intro("A hub connected by transmission lines"),
          py::no_init);
        c
            .def(py::init<int, const string&, const string&, stm::network_&>(
                (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("net")),
                "Create busbar with unique id and name for a network."))
            .def_readwrite("id",&stm::busbar::id,"Unique id for this object.")
            .def_readwrite("name",&stm::busbar::name,"Name for this object.")
            .def_readwrite("json",&stm::busbar::json,"Json keeping any extra data for this object.")
            .add_property("tag", +[](const stm::busbar& self){return url_tag(self);}, "Url tag.")
            .def("__eq__", &stm::busbar::operator==)
            .def("__ne__", &stm::busbar::operator!=)
            .def("flattened_attributes", +[](stm::busbar& self) { return make_flat_attribute_dict(self); }, "Flat dict containing all component attributes.")
            .def_readonly("flow", &stm::busbar::flow, "Flow attributes.")
            .def_readonly("price", &stm::busbar::price, "Price attributes.")
            .def_readonly("power_modules", &stm::busbar::power_modules, "Power modules associated with this busbar")
            .def_readonly("units", &stm::busbar::units, "Units associated with this busbar")
            .def("get_transmission_lines_from_busbar", &stm::busbar::get_transmission_lines_from_busbar, "Get any transmission lines connected from this busbar")
            .def("get_transmission_lines_to_busbar", &stm::busbar::get_transmission_lines_to_busbar, "Get any transmission lines connected to this busbar")
            .def("get_market_areas", &stm::busbar::get_market_areas, "Get any market areas associated with this busbar")
            .def("add_to_start_of_transmission_line", &stm::busbar::add_to_start_of_transmission_line, "Add this busbar to the start of a transmission line")
            .def("add_to_end_of_transmission_line", &stm::busbar::add_to_end_of_transmission_line, "Add this busbar to the end of a transmission line")
            .def("add_power_module", &stm::busbar::add_power_module, "Associate a (time-dependent) power module to this busbar")
            .def("remove_power_module", &stm::busbar::remove_power_module, "Remove a (time-dependent) power module from this busbar")
            .def("add_unit", &stm::busbar::add_unit, "Associate a (time-dependent) unit to this busbar")
            .def("remove_unit", &stm::busbar::remove_unit, "Remove a (time-dependent) unit from this busbar")
            .def("add_to_market_area", &stm::busbar::add_to_market_area, "Associate a market area to this busbar")
            
        ;
        expose_str_repr(c);
        expose_tsm(c);

        auto pmm=py::class_<
                stm::power_module_member,
                py::bases<>,
                shared_ptr<stm::power_module_member>,
                boost::noncopyable
            >("_PowerModuleMember", "A power_mobule busbar member, refers to a specific power_module along with the time-series that takes care of the temporal membership/contribution to the busbar sum.", py::no_init);

            pmm.def_readonly("power_module",&stm::power_module_member::power_module
               ,doc_intro("The power_module this member represents.")
            );
            add_proxy_property(pmm,"active",stm::power_module_member,active,
                doc_intro("[unit-less] if available, this time-series is multiplied with the contribution from the power_module.")
            );
        expose_str_repr(pmm);

        auto um=py::class_<
                stm::unit_member,
                py::bases<>,
                shared_ptr<stm::unit_member>,
                boost::noncopyable
            >("_UnitMember", "A unit busbar member, refers to a specific unit along with the time-series that takes care of the temporal membership/contribution to the busbar sum.", py::no_init);

            um.def_readonly("unit",&stm::unit_member::unit
               ,doc_intro("The unit this member represents.")
            );
            add_proxy_property(um,"active",stm::unit_member,active,
                doc_intro("[unit-less] if available, this time-series is multiplied with the contribution from the unit.")
            );
        expose_str_repr(um);

        using UnitMemberList=std::vector<stm::unit_member_>;
        auto uml=py::class_<UnitMemberList>("_UnitMemberList", "A strongly typed list of Busbar._UnitMember.");
        uml
            .def(py::vector_indexing_suite<UnitMemberList, true>())
            .def("__init__",
                construct_from<UnitMemberList>(py::default_call_policies(),(py::arg("busbar_unit_members."))),
                "Construct from list."
            )
        ;
        expose_str_repr(uml);

        using PowerModuleMemberList=std::vector<stm::power_module_member_>;
        auto pmml=py::class_<PowerModuleMemberList>("_PowerModuleMemberList", "A strongly typed list of Busbar._PowerModuleMember.");
        pmml
            .def(py::vector_indexing_suite<PowerModuleMemberList, true>())
            .def("__init__",
                construct_from<PowerModuleMemberList>(py::default_call_policies(),(py::arg("busbar_power_module_members."))),
                "Construct from list."
            )
        ;
        expose_str_repr(pmml);

        expose_vector_eq<stm::busbar_>("BusbarList", "A strongly typed list of Busbar.", &stm::equal_attribute<std::vector<stm::busbar_>>, false);

        {
            py::scope scope_unit=c;
            auto p=py::class_<stm::busbar::ts_triplet, py::bases<>, boost::noncopyable>(
                "_TsTriplet", 
                doc_intro("Describes .realised, .schedule and .result time-series"),
                py::no_init
            );
            _add_proxy_property(p, "realised", stm::busbar::ts_triplet,realised, "SI-units, realised time-series, as in historical fact")
            _add_proxy_property(p, "schedule", stm::busbar::ts_triplet,schedule, "SI-units, schedule, as in current schedule")
            _add_proxy_property(p, "result", stm::busbar::ts_triplet,result, "SI-units, result, as provided by optimisation")
            expose_str_repr(p);
        }
    }
}
