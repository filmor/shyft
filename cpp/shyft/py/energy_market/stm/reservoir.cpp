/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/py/api/boostpython_pch.h>
#include <shyft/py/api/expose_from_list.h>
#include <shyft/py/energy_market/py_url_tag.h>
#include <shyft/py/energy_market/stm/expose_str.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/energy_market/py_tsm_expose.h>
#include <fmt/core.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/energy_market/stm/reservoir.h>


namespace py=boost::python;

namespace expose {
    using namespace shyft::energy_market;

    using shyft::time_series::dd::apoint_ts;

    using std::string;
    using std::shared_ptr;

    template<> string str_(stm::reservoir::level_::constraint_ const& o) {
        return fmt::format("_Constraint(min={}, max={})"
            ,str_(o.min)
            ,str_(o.max)
        );
    }

    template<> string str_(stm::reservoir::level_ const& o) {
        return fmt::format("_Level(regulation_min={}, regulation_max={}, realised={}, schedule={}, result={}, constraint={})"
            ,str_(o.regulation_min)
            ,str_(o.regulation_max)
            ,str_(o.realised)
            ,str_(o.schedule)
            ,str_(o.result)
            ,str_(o.constraint)
        );
    }

    template<> string str_(stm::reservoir::volume_::constraint_::tactical_ const& o) {
        return fmt::format("_Tactical(min={}, max={})"
            ,str_(o.min)
            ,str_(o.max)
        );
    }

    template<> string str_(stm::reservoir::volume_::constraint_ const& o) {
        return fmt::format("_Constraint(min={}, max={}, tactical={})"
            ,str_(o.min)
            ,str_(o.max)
            ,str_(o.tactical)
        );
    }

    template<> string str_(stm::reservoir::volume_::slack_ const& o) {
        return fmt::format("_Slack(lower={}, uppper={})"
            ,str_(o.lower)
            ,str_(o.upper)
        );
    }

    template<> string str_(stm::reservoir::volume_::cost_::cost_curve_ const& o) {
        return fmt::format("_CostCurve(curve={}, penalty={})"
            ,str_(o.curve)
            ,str_(o.penalty)
        );
    }

    template<> string str_(stm::reservoir::volume_::cost_ const& o) {
        return fmt::format("_Cost(flood={}, peak={})"
            ,str_(o.flood)
            ,str_(o.peak)
        );
    }

    template<> string str_(stm::reservoir::volume_ const& o) {
        return fmt::format("_Volume(static_max={}, schedule={}, realised= {}, result={}, penalty={}, constaint={}, slack={}, cost={})"
            ,str_(o.static_max)
            ,str_(o.schedule)
            ,str_(o.realised)
            ,str_(o.result)
            ,str_(o.penalty)
            ,str_(o.constraint)
            ,str_(o.slack)
            ,str_(o.cost)
        );
    }

    template<> string str_(stm::reservoir::ramping_ const& o) {
        return fmt::format("_Ramping(level_down={}, level_up={})"
            ,str_(o.level_down)
            ,str_(o.level_up)
        );
    }

    template<> string str_(stm::reservoir::inflow_ const& o) {
        return fmt::format("_Inflow(schedule={}, realised={}, result={})"
            ,str_(o.schedule)
            ,str_(o.realised)
            ,str_(o.result)
        );
    }

    template<> string str_(stm::reservoir::water_value_::result_ const& o) {
        return fmt::format("_Result(local_volume={}, global_volume={}, local_energy={})"
            ,str_(o.local_volume)
            ,str_(o.global_volume)
            ,str_(o.local_energy)
        );
    }

    template<> string str_(stm::reservoir::water_value_ const& o) {
        return fmt::format("_Water_value(endpoint_desc={}, result={})"
            ,str_(o.endpoint_desc)
            ,str_(o.result)
        );
    }


    template<> string str_(stm::reservoir const& o) {
        return fmt::format("Reservoir(id={}, name={})"
            ,str_(o.id)
            ,str_(o.name)
        );
    }

    void stm_reservoir() {
        auto r=py::class_<
            stm::reservoir,
            py::bases<hydro_power::reservoir>,
            shared_ptr<stm::reservoir>, 
            boost::noncopyable
        >("Reservoir","Stm reservoir.",py::no_init);
        r
            .def(py::init<int, const string&,const string&, stm::stm_hps_ &>(
                (py::arg("uid"),py::arg("name"),py::arg("json"),py::arg("hps")),
                "Create reservoir with unique id and name for a hydro power system."))


            .add_property("level", &stm::reservoir::level, "Level attributes.")
            .add_property("tag", +[](const stm::reservoir& self){return url_tag(self);}, "Url tag.")
            .def_readonly("volume", &stm::reservoir::volume, "Volume attributes.")
            .def_readonly("inflow", &stm::reservoir::inflow, "Inflow attributes.")
            .def_readonly("ramping", &stm::reservoir::ramping, "Ramping attributes.")
            .def_readonly("water_value", &stm::reservoir::water_value, "Water-value attributes.")
            .def(py::self==py::self)
            .def(py::self!=py::self)

            .def("flattened_attributes", +[](stm::reservoir& self) { return make_flat_attribute_dict(self); }, "Flat dict containing all component attributes.")
            ;
        expose_str_repr(r);
        expose_tsm(r);

        add_proxy_property(r,"volume_level_mapping", stm::reservoir,volume_level_mapping, "Volume capacity description, time-dependent x=[masl], y=[m3].")

        expose_vector_eq<stm::reservoir_>("ReservoirList", "A strongly typed list of Reservoirs.",&stm::equal_attribute<std::vector<stm::reservoir_>>,false);

        {
            py::scope scope_r=r;
            auto rl=py::class_<stm::reservoir::level_, py::bases<>, boost::noncopyable>("_Level", py::no_init);
            rl
                .def_readonly("constraint", &stm::reservoir::level_::constraint, "Level constraint attributes.")
            ;
            expose_str_repr(rl);
            _add_proxy_property(rl,"regulation_min", stm::reservoir::level_,regulation_min, "[masl] Lowest regulated water level, time-dependent attribute.")
            _add_proxy_property(rl,"regulation_max", stm::reservoir::level_,regulation_max, "[masl] Highest regulated water level, time-dependent attribute.")
            _add_proxy_property(rl,"realised", stm::reservoir::level_,realised, "[masl] Historical water level, time series.")
            _add_proxy_property(rl,"schedule", stm::reservoir::level_,schedule, "[masl] Level schedule, time series.")
            _add_proxy_property(rl,"result", stm::reservoir::level_,result, "[masl] Level results, time series.")

            auto rv=py::class_<stm::reservoir::volume_, py::bases<>, boost::noncopyable>("_Volume", py::no_init);
            rv
                .def_readonly("constraint", &stm::reservoir::volume_::constraint, "Volume constraint attributes.")
                .def_readonly("slack", &stm::reservoir::volume_::slack, "Slack attributes.")
            ;
            expose_str_repr(rv);
            _add_proxy_property(rv,"static_max", stm::reservoir::volume_,static_max, "[m3] Maximum regulated volume, time-dependent attribute.")
            _add_proxy_property(rv,"realised", stm::reservoir::volume_,realised, "[m3] Historical volume, time series.")
            _add_proxy_property(rv,"schedule", stm::reservoir::volume_,schedule, "[m3] Volume schedule, time series.")
            _add_proxy_property(rv,"result", stm::reservoir::volume_,result, "[m3] Volume results, time series.")
            _add_proxy_property(rv,"penalty", stm::reservoir::volume_,penalty, "[m3] Volume penalty, time series.")

            auto rwv=py::class_<stm::reservoir::water_value_, py::bases<>, boost::noncopyable>("_WaterValue", py::no_init);
            rwv
                .def_readonly("result", &stm::reservoir::water_value_::result, "Optimizer detailed watervalue results.")
            ;
            expose_str_repr(rwv);

            _add_proxy_property(rwv,"endpoint_desc", stm::reservoir::water_value_,endpoint_desc, "[money/joule] Fixed water-value endpoint.")
            {
                py::scope scope_rwv=rwv;
                auto rwvr=py::class_<stm::reservoir::water_value_::result_, py::bases<>, boost::noncopyable>("_Result", py::no_init);
                _add_proxy_property(rwvr,"local_volume", stm::reservoir::water_value_::result_,local_volume, "[money/m3] Resulting water-value.")
                _add_proxy_property(rwvr,"global_volume", stm::reservoir::water_value_::result_,global_volume, "[money/m3] Resulting water-value.")
                _add_proxy_property(rwvr,"local_energy", stm::reservoir::water_value_::result_,local_energy, "[money/joule] Resulting water-value.")
                _add_proxy_property(rwvr,"end_value", stm::reservoir::water_value_::result_,end_value, "[money] Resulting water-value at the endpoint.")
                expose_str_repr(rwvr);

            }
            auto ri=py::class_<stm::reservoir::inflow_, py::bases<>, boost::noncopyable>("_Inflow", py::no_init);
            expose_str_repr(ri);
            _add_proxy_property(ri,"schedule", stm::reservoir::inflow_,schedule, "[m3/s] Inflow input, time series.")
            _add_proxy_property(ri,"realised", stm::reservoir::inflow_,realised, "[m3/s] Historical inflow, time series.")
            _add_proxy_property(ri,"result", stm::reservoir::inflow_,result, "[m3/s]  Inflow result, time series.")

            auto rr=py::class_<stm::reservoir::ramping_, py::bases<>, boost::noncopyable>("_Ramping", py::no_init);
            expose_str_repr(rr);
            _add_proxy_property(rr,"level_down", stm::reservoir::ramping_,level_down, "[m/s] Max level change down, time series.")
            _add_proxy_property(rr,"level_up", stm::reservoir::ramping_,level_up, "[m/s] Max level change up, time series.")

            {
                py::scope rl_scope=rl;
                auto rlc=py::class_<stm::reservoir::level_::constraint_, py::bases<>, boost::noncopyable>("_Constraints", py::no_init);
                expose_str_repr(rlc);
                _add_proxy_property(rlc,"min", stm::reservoir::level_::constraint_,min, "[masl] Level constraint minimum, time series.")
                _add_proxy_property(rlc,"max", stm::reservoir::level_::constraint_,max, "[masl] Level constraint maximum, time series.")
            }
            
            {
                py::scope rv_scope=rv;
                auto rvc=py::class_<stm::reservoir::volume_::constraint_, py::bases<>, boost::noncopyable>("_Constraints", py::no_init);
                rvc
                    .def_readonly("tactical", &stm::reservoir::volume_::constraint_::tactical, "Tactical volume constraint attributes.")
                ;
                expose_str_repr(rvc);
                _add_proxy_property(rvc,"min", stm::reservoir::volume_::constraint_,min, "[m3] Volume constraint minimum, time series.")
                _add_proxy_property(rvc,"max", stm::reservoir::volume_::constraint_,max, "[m3] Volume constraint maximum, time series.")
                
                {
                    py::scope rvc_scope=rvc;
                    auto rvct=py::class_<stm::reservoir::volume_::constraint_::tactical_,py::bases<>, boost::noncopyable>("_Tactical", py::no_init);
                    rvct
                        .def_readonly("min", &stm::reservoir::volume_::constraint_::tactical_::min, "[masl],[money] Tactical minimum volume constraint, PenaltyConstraint.")
                        .def_readonly("max", &stm::reservoir::volume_::constraint_::tactical_::max, "[masl],[money] Tactical maximum volume constraint, PenaltyConstraint.")
                    ;
                    expose_str_repr(rvct);
                }

                auto rvs=py::class_<stm::reservoir::volume_::slack_,py::bases<>, boost::noncopyable>("_Slack", py::no_init);
                expose_str_repr(rvs);
                _add_proxy_property(rvs,"lower", stm::reservoir::volume_::slack_,lower,"[m3] Lower volume slack, time series.")
                _add_proxy_property(rvs,"upper", stm::reservoir::volume_::slack_,upper, "[m3] Upper volume slack, time series.")

                auto rvco=py::class_<stm::reservoir::volume_::cost_, py::bases<>, boost::noncopyable>("_Cost", py::no_init);
                rvco
                    .def_readonly("flood", &stm::reservoir::volume_::cost_::flood, "Cost applied to volume.")
                    .def_readonly("peak", &stm::reservoir::volume_::cost_::peak, "Cost applied to the highest volume reached.")
                ;
                expose_str_repr(rvco);

                {
                    py::scope rvc_scope=rvco;
                    auto rvcc=py::class_<stm::reservoir::volume_::cost_::cost_curve_,py::bases<>, boost::noncopyable>("_CostCurve", py::no_init);
                    expose_str_repr(rvcc);
                    _add_proxy_property(rvcc,"curve", stm::reservoir::volume_::cost_::cost_curve_,curve,"[m3],[money/m3] Volume cost curve.")
                    _add_proxy_property(rvcc,"penalty", stm::reservoir::volume_::cost_::cost_curve_,penalty, "[money] Volume penalty, time series.")
                }
            }
        }
    }
}
