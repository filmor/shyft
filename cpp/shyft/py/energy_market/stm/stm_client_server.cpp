/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>

#include <shyft/py/energy_market/py_model_client_server.h>
#include <shyft/srv/db.h>

#include <shyft/energy_market/stm/stm_system.h>

namespace expose {
    void stm_client_server() {
        using model=shyft::energy_market::stm::stm_system;
        using client_t = shyft::py::energy_market::py_client<shyft::srv::client<model>>;
        using srv_t = shyft::py::energy_market::py_server<shyft::srv::server<shyft::srv::db<model>>>;

        shyft::py::energy_market::expose_client<client_t>("StmClient",
            "The client api for the stm repository server."
        );
        shyft::py::energy_market::expose_server<srv_t>("StmServer",
            "The server-side component for the stm energy_market model repository."
        );

    }
}
