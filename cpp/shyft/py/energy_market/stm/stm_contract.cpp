/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/api/expose_from_list.h>
#include <shyft/py/energy_market/py_url_tag.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/energy_market/stm/expose_str.h>
#include <shyft/py/energy_market/py_tsm_expose.h>

#include <fmt/core.h>
#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/stm/contract_portfolio.h>

namespace py=boost::python;

namespace expose {
    using namespace shyft::energy_market;

    using shyft::time_series::dd::apoint_ts;
    using shyft::core::utctime;

    using std::string;
    using std::shared_ptr;

    template<> string str_(stm::contract const& o) {
        return fmt::format("Contract(id={}, name='{}')"
            ,o.id
            ,o.name
        );
    }

    template<> string str_(stm::contract_portfolio const& o) {
        return fmt::format("ContractPortfolio(id={}, name='{}')"
            ,o.id
            ,o.name
        );
    }

    template<> string str_(stm::contract_relation const& o) {
        return fmt::format("_ContractRelation(related.id={}, relation_type='{}')"
            ,o.related->id
            ,o.relation_type
        );
    }

    void stm_contract() {
        auto c=py::class_<
            stm::contract,
            py::bases<>,
            shared_ptr<stm::contract>,
            boost::noncopyable
        >("Contract",
          doc_intro("A contract between two parties, seller and buyer, for sale of a product at a given price.")
          doc_intro("The contract can refer to specific power-plant, unit group, or even other contracts.")
          ,
          py::no_init);
        c
            .def(py::init<int, const string&, const string&, stm::stm_system_&>(
                (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("sys")),
                "Create contract with unique id and name for a stm system."))
            .def_readwrite("id",&stm::contract::id,"Unique id for this object.")
            .def_readwrite("name",&stm::contract::name,"Name for this object.")
            .def_readwrite("json",&stm::contract::json,"Json keeping any extra data for this object.")
            .add_property("tag", +[](const stm::contract& self){return url_tag(self);}, "Url tag.")
            .def_readonly("power_plants",&stm::contract::power_plants,"List of associated power plants.")
            .def_readonly("relations",&stm::contract::relations,"List of related contracts.")
            .def_readonly("constraint",&stm::contract::constraint, "Constrant of this contract.")
            .def(py::self==py::self)
            .def(py::self!=py::self)
            .def("flattened_attributes", +[](stm::contract& self) { return make_flat_attribute_dict(self); }, "Flat dict containing all component attributes.")
            .def("get_portfolios", &stm::contract::get_portfolios, "Get any portfolios this contract is associated with. Convenience for search in ContractPortfolio.contracts.")
            .def("add_to_portfolio", &stm::contract::add_to_portfolio, "Add this contract to specified portfolio. Convenience for appending to ContractPortfolio.contracts.")
            .def("get_market_areas", &stm::contract::get_energy_market_areas, "Get any energy market areas this contract is associated with. Convenience for search in MarketArea.contracts.")
            .def("add_to_market_area", &stm::contract::add_to_energy_market_area, "Add this contract to specified energy market area. Convenience for appending to MarketArea.contracts.")
            .def("add_relation",&stm::contract::add_relation,(py::arg("self"),py::arg("id"), py::arg("contract"),py::arg("relation_type")),
                doc_intro("Add a contract as a relation from this contract")
                doc_parameters()
                doc_parameter("id","Id","The relation id (must be unique for this contract)")
                doc_parameter("contract","Contract","The contract to be added as a relation")
                doc_parameter("relation_type","RelationType","A free to use integer to describe relation type")
            )
            .def("remove_relation",&stm::contract::remove_relation,(py::arg("self"),py::arg("contract_relation")),
                doc_intro("Remove relation to contract.")
                doc_parameters()
                doc_parameter("contract_relation","ContractRelation","The relation to be removed")
            )
        ;
        expose_str_repr(c);
        expose_tsm(c);
        add_proxy_property(c,"quantity", stm::contract, quantity, "[J/s] Contract quantity, in rate units, so that integrated over contract period gives total volume.")
        add_proxy_property(c,"price", stm::contract, price, "[money/J] Contract price.")
        add_proxy_property(c,"fee", stm::contract, fee, "[money/s] Any contract fees, in rate units so that fee over the contract period gives total fee.")
        add_proxy_property(c,"revenue", stm::contract, revenue, "[money/s] Calculated revenue, in rate units, so that integrated of the contract period gives total revenue volume")
        add_proxy_property(c,"parent_id", stm::contract, parent_id, "Optional reference to parent (contract). Typically for forwardes/futures, that is splitted into shorter terms as time for the delivery is approaching.")
        add_proxy_property(c,"active", stm::contract, active, "Contract status (dead/alive).")
        add_proxy_property(c,"validation", stm::contract, validation, "Validation status (and whether the contract has been validated or not).")
        add_proxy_property(c,"buyer", stm::contract,buyer,"The name of the buyer party of the contract.")
        add_proxy_property(c,"seller", stm::contract,seller,"The name of the seller party of the contract.")
        add_proxy_property(c, "options", stm::contract, options, "Defines optional price/volume curves for future trading.")

        {
            py::scope scope_contract=c;

            auto cc = py::class_<stm::contract::constraint_, py::bases<>, boost::noncopyable>(
                    "Constraint_",
                    doc_intro("Contract.Constraint attributes"),
                    py::no_init
                    );
            _add_proxy_property(cc, "min_trade", stm::contract::constraint_, min_trade, "Minimum quantity (volume) that must be traded for this contract.");
            _add_proxy_property(cc, "max_trade", stm::contract::constraint_, max_trade, "Maximum quantity (volume) that must be traded for this contract.");
            _add_proxy_property(cc, "ramping_up", stm::contract::constraint_, ramping_up, "Max quantity (volume) to ramp up between timesteps for this contract.");
            _add_proxy_property(cc, "ramping_down", stm::contract::constraint_, ramping_down, "Max quantity (volume) to ramp down between timesteps for this contract.");
            _add_proxy_property(cc, "ramping_up_penalty_cost", stm::contract::constraint_, ramping_up_penalty_cost, "Penalty for violating ramping up limit.");
            _add_proxy_property(cc, "ramping_down_penalty_cost", stm::contract::constraint_, ramping_down_penalty_cost, "Penalty for violating ramping down limit.");
        }

        expose_vector_eq<stm::contract_>("ContractList", "A strongly typed list of Contract.", &stm::equal_attribute<std::vector<stm::contract_>>, false);

        auto cp=py::class_<
            stm::contract_portfolio,
            py::bases<>,
            shared_ptr<stm::contract_portfolio>,
            boost::noncopyable
        >("ContractPortfolio",
          doc_intro("Stm contract portfolio represents a set of contracts, so that the sum of interesting contract properties can be evaluated and compared."),
          py::no_init);
        cp
            .def(py::init<int, const string&, const string&, stm::stm_system_&>(
                (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("sys")),
                "Create contract portfolio with unique id and name for a stm system."))
            .def_readwrite("id",&stm::contract_portfolio::id,"Unique id for this object.")
            .def_readwrite("name",&stm::contract_portfolio::name,"Name for this object.")
            .def_readwrite("json",&stm::contract_portfolio::json,"Json keeping any extra data for this object.")
            .add_property("tag", +[](const stm::contract_portfolio& self){return url_tag(self);}, "Url tag.")
            .def_readonly("contracts",&stm::contract_portfolio::contracts,"List of contracts.")
            .def(py::self==py::self)
            .def(py::self!=py::self)
            .def("flattened_attributes", +[](stm::contract_portfolio& self) { return make_flat_attribute_dict(self); }, "Flat dict containing all component attributes.")
        ;
        expose_str_repr(cp);
        expose_tsm(cp);
        add_proxy_property(cp,"quantity", stm::contract_portfolio, quantity, "[J/s] normally sum of contracts, unit depends on the contracts.")
        //add_proxy_property(c,"price", stm::contract_portfolio, price, "[money] Price of the portfolio, normally average over contracts.")
        add_proxy_property(cp,"fee", stm::contract_portfolio, fee, "[money/s] Fees of the portfolio, normally sum of contracts.")
        add_proxy_property(cp,"revenue", stm::contract_portfolio, revenue, "[money/s] Calculated revenue.")

        expose_vector_eq<stm::contract_portfolio_>("ContractPortfolioList", "A strongly typed list of ContractPortfolio.", &stm::equal_attribute<std::vector<stm::contract_portfolio_>>, false);
        {
            py::scope sc=c;
            auto cr=py::class_<
            stm::contract_relation,
            py::bases<>,
            shared_ptr<stm::contract_relation>,
            boost::noncopyable
            >("_ContractRelation",
              doc_intro("A relation to another contract, where the relation-type is a user specified integer.")
              doc_intro("This allows building and maintaining contract system that have internal rules/constraints")
              doc_intro("There is a minimal set of rules, like avoiding circularities that are enforced")
              ,
              py::no_init);

            cr.def(py::self==py::self);
            cr.def(py::self!=py::self);
            cr.def_readonly("related",&stm::contract_relation::related
                ,doc_intro("The related contract")
            );
            cr.def_readonly("id",&stm::contract_relation::id
                ,doc_intro("The id of the relation")
            );
            cr.add_property("owner",&stm::contract_relation::owner_,doc_intro("ref. to the contract that owns this relation"));


            add_proxy_property(cr,"relation_type",stm::contract_relation,relation_type,
                doc_intro("Free to use integer to describe relation type")
            );
            expose_str_repr(cr);

            using ContractRelationList=std::vector<stm::contract_relation_>;
            auto crl=py::class_<ContractRelationList>("_ContractRelationList", "A strongly typed list of Contracts._ContractRelation.");
            crl
                .def(py::vector_indexing_suite<ContractRelationList, true>())
                .def("__init__",
                    construct_from<ContractRelationList>(py::default_call_policies(),(py::arg("contract_relation."))),
                    "Construct from list."
                )
            ;
            expose_str_repr(crl);
        }
    }
}
