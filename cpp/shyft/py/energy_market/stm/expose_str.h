/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <shyft/py/api/expose_str.h>
#include <shyft/py/time_series/expose_str.h>
#include <shyft/energy_market/stm/attribute_types.h>

namespace expose {
    extern template std::string str_(const shyft::energy_market::hydro_power::xy_point_curve& m);
    extern template std::string str_(const shyft::energy_market::hydro_power::point& m);
    extern template std::string str_(const shyft::energy_market::hydro_power::turbine_description &m);
    extern template std::string str_(const shyft::energy_market::stm::t_xy_::element_type& m);
    extern template std::string str_(const shyft::energy_market::stm::t_xyz_::element_type& m);
    extern template std::string str_(const shyft::energy_market::stm::xy_point_curve_with_z_list::value_type& o);
    extern template std::string str_(const shyft::energy_market::stm::t_xyz_list_::element_type& m);
    extern template std::string str_(const shyft::energy_market::stm::xy_point_curve_with_z_list& o);
    extern template std::string str_(const shyft::energy_market::stm::t_turbine_description_::element_type& m);
    extern template std::string str_(const shyft::energy_market::core::absolute_constraint& m);
    extern template std::string str_(const shyft::energy_market::core::penalty_constraint& m);
}
