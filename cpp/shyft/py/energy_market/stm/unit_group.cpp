/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <cstring>
#include <ranges>

#include <shyft/py/api/boostpython_pch.h>
#include <shyft/py/energy_market/py_object_ext.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/py/energy_market/stm/expose_str.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/api/expose_from_list.h>
#include <shyft/py/energy_market/py_url_tag.h>
#include <shyft/py/energy_market/py_tsm_expose.h>

#include <boost/describe/enum_to_string.hpp>
#include <boost/mp11/algorithm.hpp>
#include <fmt/core.h>

namespace py=boost::python;

namespace expose {
    using namespace shyft::energy_market;

    using shyft::energy_market::stm::energy_market_area;

    using std::string;
    using std::shared_ptr;
    
    inline std::string group_type_string(stm::unit_group_type gt) {
        std::string_view lower = boost::describe::enum_to_string(gt,"unknown_group_type");
        auto upper = std::views::transform(lower,[](auto c){ return std::toupper(c); });
        return std::string{std::ranges::begin(upper),std::ranges::end(upper)};
    }

    template<> string str_(stm::unit_group const& o) {
        return fmt::format("UnitGroup(id={}, name={}, group_type={})"
            ,o.id
            ,o.name
            ,group_type_string(o.group_type) //.value_or(0)
        );
    }
    
    template<> string str_(stm::unit_group_member const& o) {
        return fmt::format("_Member(unit.id={}, unit.name='{}')"
            ,o.unit->id
            ,o.unit->name
        );
    }
    template<> string str_(stm::unit_group::obligation_ const& o) {
        return fmt::format("_Obligation(schedule={}, cost={},result={},penalty={})"
            ,str_(o.schedule)
            ,str_(o.cost)
            ,str_(o.result)
            ,str_(o.penalty)
        );
    }
    template<> string str_(stm::unit_group::delivery_ const& o) {
        return fmt::format("_Delivery(schedule={}, result={},realised={})"
            ,str_(o.schedule)
            ,str_(o.result)
            ,str_(o.realised)
        );
    }

    void stm_unit_group() {
        
        auto ug=py::class_<
            stm::unit_group,
            py::bases<>,
            shared_ptr<stm::unit_group>,
            boost::noncopyable
        >("UnitGroup", "A a group of Units, with constraints applicable to the sum of the unit-features (production, flow...), that the optimization can take into account.", py::no_init);
        ug
        .def_readwrite("id",&stm::unit_group::id,"Unique id for this group.")
        .def_readwrite("name",&stm::unit_group::name,"Name of the group.")
        .def_readwrite("json",&stm::unit_group::json,"Json payload to adapt py-extension.")
        .add_property("tag", +[](const stm::unit_group& self){return url_tag(self);}, "Url tag.")
        .def_readwrite("group_type",&stm::unit_group::group_type, "Unit group-type, one of GroupType enum, fcr_n_up, fcr_n_down etc.")
        .def_readonly("members",&stm::unit_group::members)
        .add_property("market_area",&stm::unit_group::get_energy_market_area,"Getter market area.")
        .def("add_unit",&stm::unit_group::add_unit,(py::arg("self"),py::arg("unit"),py::arg("active")),
            doc_intro("Adds a unit to the group, maintaining any needed expressions.")
            doc_parameters()
            doc_parameter("unit","Unit","The unit to be added to the group (sum expressions automagically updated).")
            doc_parameter("active","TimeSeries","Determine the temporal group-member-ship, if empty Ts, then always member.")
        )
        .def("remove_unit",&stm::unit_group::remove_unit,(py::arg("self"),py::arg("unit")),
            doc_intro("Remove a nunit from the group maintaining any needed expressions.")
            doc_parameters()
            doc_parameter("unit","Unit","The unit to be removed from the group (sum expressions automagically updated).")
        )
        .def("_update_sum_expressions",&stm::unit_group::update_sum_expressions,(py::arg("self")),
             doc_intro("update the sum-expressions, in the case this has not been done, or is out of sync.")
         )
        .def_readonly("obligation",&stm::unit_group::obligation,
                      doc_intro("Obligation schedule, cost, results and penalty.")
        )
        .def_readonly("delivery",&stm::unit_group::delivery,
                      doc_intro("Sum of product/obligation delivery schedule,result and realised for unit-members.")
        )
        .def(py::self==py::self)
        .def(py::self!=py::self)
        .add_property("obj", &py_object_ext<stm::unit_group>::get_obj, &py_object_ext<stm::unit_group>::set_obj)
        .def("flattened_attributes", +[](stm::unit_group& self) { return make_flat_attribute_dict(self); }, "Flat dict containing all component attributes.")

        ;
        expose_str_repr(ug);
        expose_tsm(ug);
        add_proxy_property(ug,"production",stm::unit_group,production,
            doc_intro("[W] the sum resulting production for this unit-group.")
        );
        add_proxy_property(ug,"flow",stm::unit_group,flow,
            doc_intro("[m3/s] the sum resulting water flow for this unit-group.")
        );
        
        using UnitGroupList=std::vector<stm::unit_group_>;
        auto ul=py::class_<UnitGroupList>("UnitGroupList", "A strongly typed list of UnitsGroups.");
        ul
            .def(py::vector_indexing_suite<UnitGroupList, true>())
            .def("__init__",
                 construct_from<UnitGroupList>(py::default_call_policies(),(py::arg("unit_groups"))),
                "Construct from list."
            )
            .def("__eq__",+[](UnitGroupList const&a, UnitGroupList const &b)->bool {
                 return stm::equal_attribute(a,b);
                }
             )
            .def("__ne__",+[](UnitGroupList const&a, UnitGroupList const &b)->bool {
                 return !stm::equal_attribute(a,b);
                }
             )
        ;

        expose_str_repr(ul);
        /** scope unit_group_member like UnitGroup._Member*/{
            py::scope up_scope=ug;
            using ugt=stm::unit_group_type;
            auto t_ugt = py::enum_<ugt>("unit_group_type",
                doc_intro(
                "The unit-group type specifies the purpose of the group, and thus also how\n"
                "it is mapped to the optimization as constraint. E.g. operational reserve fcr_n.up.\n"
                "Current mapping to optimizer/shop:\n\n"
                "    *        FCR* :  primary reserve, instant response, note, sensitivity set by droop settings on the unit \n"
                "    *       AFRR* : automatic frequency restoration reserve, ~ minute response\n"
                "    *       MFRR* : it is the manual frequency restoration reserve, ~ 15 minute response\n"
                "    *         FFR : NOT MAPPED, fast frequency restoration reserve, 49.5..49.7 Hz, ~ 1..2 sec response\n"
                "    *        RR*  : NOT MAPPED, replacement reserve, 40..60 min response\n"
                "    *      COMMIT : currently not mapped\n"
                "    *  PRODUCTION : used for energy market area unit groups\n\n"
                ));
            boost::mp11::mp_for_each<boost::describe::describe_enumerators<ugt>>(
                [&](auto e){
                    auto e_name = group_type_string(e.value);
                    t_ugt.value(e_name.c_str(),e.value);
                });
            t_ugt.export_values()
            ;
            auto ugo=py::class_<stm::unit_group::obligation_,py::bases<>,boost::noncopyable>(
                "_Obligation",
                doc_intro("This describes the obligation aspects of the unit group that the members should satisfy."),
                py::no_init
            );
            expose_str_repr(ugo);
            _add_proxy_property(ugo,"schedule",stm::unit_group::obligation_,schedule, "[W] schedule/target that should be met.");
            _add_proxy_property(ugo,"cost",stm::unit_group::obligation_,cost, "[money/W] the cost of not satisfying the schedule.");
            _add_proxy_property(ugo,"result",stm::unit_group::obligation_,result, "[W] the resulting target/slack met after optimization.");
            _add_proxy_property(ugo,"penalty",stm::unit_group::obligation_,penalty, "[money] If target violated, the cost of the violation.");
            
            auto ugd=py::class_<stm::unit_group::delivery_,py::bases<>,boost::noncopyable>(
                "_Delivery",
                doc_intro("This describes the sum of product delivery aspects for the units."),
                py::no_init
            );
            expose_str_repr(ugd);
            _add_proxy_property(ugd,"schedule",stm::unit_group::delivery_,schedule, "[product-unit] sum schedule");
            _add_proxy_property(ugd,"result",stm::unit_group::delivery_,result, "[product-unit] sum result");
            _add_proxy_property(ugd,"realised",stm::unit_group::delivery_,realised, "[product-unit] sum realised, as historical fact");

            auto ugm=py::class_<
                stm::unit_group_member,
                py::bases<>,
                shared_ptr<stm::unit_group_member>,
                boost::noncopyable
            >("_Member", "A unit group member, refers to a specific unit along with the time-series that takes care of the temporal membership/contribution to the unit_group sum.", py::no_init);

            ugm.def_readonly("unit",&stm::unit_group_member::unit
               ,doc_intro("The unit this member represents.")
            );
            add_proxy_property(ugm,"active",stm::unit_group_member,active,
                doc_intro("[unit-less] if available, this time-series is multiplied with the contribution from the unit.")
            );
            expose_str_repr(ugm);

            using UnitGroupMemberList=std::vector<stm::unit_group_member_>;
            auto ugml=py::class_<UnitGroupMemberList>("_MemberList", "A strongly typed list of UnitsGroup._Member.");
            ugml
                .def(py::vector_indexing_suite<UnitGroupMemberList, true>())
                .def("__init__",
                    construct_from<UnitGroupMemberList>(py::default_call_policies(),(py::arg("unit_group_members."))),
                    "Construct from list."
                )
            ;
            expose_str_repr(ugml);
            
        }
    }
}
