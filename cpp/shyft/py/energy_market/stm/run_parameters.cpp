/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/py/energy_market/stm/expose_str.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/api/expose_from_list.h>
#include <shyft/py/api/expose_optional.h>
#include <shyft/py/energy_market/py_tsm_expose.h>
#include <fmt/core.h>

namespace py=boost::python;

namespace expose {
    using namespace shyft::energy_market;

    using shyft::core::utctime;

    using std::string;
    using std::shared_ptr;

    using TimestampedString = std::pair<utctime, string>;
    using MessageList = std::vector<TimestampedString>;

    template<> string str_(TimestampedString const& o) {
        return fmt::format("TimeStampedString({}, '{}')"
            ,str_(o.first)
            ,str_(o.second)
        );
    }

    template<> string str_(stm::run_parameters const& o) {
        return fmt::format("RunParameters(id={}, name={}, n_inc_runs={}, n_full_runs={}, head_opt={}, run_time_axis={}, fx_log={})"
            ,o.id
            ,o.name
            ,o.n_inc_runs //.value_or(0)
            ,o.n_full_runs //.value_or(0)
            ,o.head_opt // .value_or(false)
            ,str_(o.run_time_axis)
            ,str_(o.fx_log)
       );
    }

    void stm_run_parameters() {
        // later we need to relocate this
        register_optional<int16_t>();
        register_optional<bool>();
        
        auto ts=py::class_<TimestampedString>("TimestampedString", "A string with a corresponding timestamp.", py::no_init);
            ts
            .def_readonly("time", &TimestampedString::first)
            .def_readonly("message", &TimestampedString::second)
        ;
        expose_str_repr(ts);

        auto ml= py::class_<MessageList>("MessageList", "A strongly typed list of str.");
        ml
            .def(py::vector_indexing_suite<MessageList, true>())
            .def("__init__",
                 construct_from<MessageList>(py::default_call_policies(),(py::arg("messages"))),
                "Construct from list of messages."
            )
            .add_property("exists",+[](MessageList*l)->bool{return l?l->size()>0:false;})
        ;
        expose_str_repr(ml);

        auto rp=py::class_<
            stm::run_parameters,
            py::bases<>,
            shared_ptr<stm::run_parameters>,
            boost::noncopyable
        >("RunParameters", "A set of parameters from a simulation- or optimization run.", py::no_init);
        rp
            .def_readwrite("n_inc_runs", &stm::run_parameters::n_inc_runs, "Number of incremental runs.")
            .def_readwrite("n_full_runs", &stm::run_parameters::n_full_runs, "Number of full runs.")
            .def_readwrite("head_opt", &stm::run_parameters::head_opt, "Whether head optimization is turned on.")
            .def(py::self==py::self)
            .def(py::self!=py::self)
        ;
        expose_str_repr(rp);
        expose_tsm(rp);

        //add_proxy_property(rp,"n_inc_runs", stm::run_parameters,n_inc_runs, "Number of incremental runs.")
        //add_proxy_property(rp,"n_full_runs", stm::run_parameters,n_full_runs, "Number of full runs.")
        //add_proxy_property(rp,"head_opt", stm::run_parameters,head_opt, "Whether head optimization is turned on.")
        add_proxy_property(rp,"fx_log", stm::run_parameters,fx_log, "Messages from the fx-callback.")
        add_proxy_property(rp,"run_time_axis",stm::run_parameters,run_time_axis,"The time axis for the SHOP run.")

        def_a_wrap<MessageList>("_message_list");
    }
}
