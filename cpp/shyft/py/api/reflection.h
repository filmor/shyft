#pragma once

#include <cctype>
#include <ranges>
#include <string_view>

#include <boost/mp11/algorithm.hpp>
#include <boost/describe/enumerators.hpp>
#include <boost/describe/members.hpp>
#include <boost/python/enum.hpp>

#include <shyft/core/reflection.h>
#include <shyft/core/reflection/formatters.h>

namespace shyft::py {

  template <reflected_enum T>
  void expose_enumerators(boost::python::enum_<T> &py_type) {
    boost::mp11::mp_for_each<boost::describe::describe_enumerators<T>>([&](auto tag) {
      std::string name = tag.name;
      std::ranges::transform(name, name.begin(), [](unsigned char c) {
        return std::toupper(c);
      });
      py_type.value(name.c_str(), tag.value);
    });
    py_type.export_values();
  }

  template <reflected_struct T, typename... O>
  void expose_members(boost::python::class_<T, O...> &py_type) {
    boost::mp11::mp_for_each<boost::describe::describe_members<T, boost::describe::mod_public>>([&](auto tag) {
      py_type.def_readwrite(tag.name, tag.pointer);
    });
  }

}
