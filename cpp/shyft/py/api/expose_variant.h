#pragma once

#include <string>
#include <utility>
#include <variant>
#include <type_traits>

#include <boost/python/object.hpp>
#include <boost/python/extract.hpp>
#include <boost/python/to_python_converter.hpp>
#include <boost/python/data_members.hpp>

namespace expose {
  namespace py = boost::python;

  namespace detail {

    /** to python converter for std::variant<T...> support
         *
         * If the std::variant is set, provide the value
         * otherwise, return None
         *
         * @tparam T the optinal type, like
         */
    template <typename... T>
    struct to_python_variant {
      static PyObject* convert(const std::variant<T...>& obj) {
        return std::visit([](const auto& obj) { return boost::python::to_python_value<decltype(obj)>()(obj); }, obj);
      }
    };

    /** helper to enable python pass value or none as setter to std::variant args/members
         */
    template <typename... T>
    struct from_python_variant {
      from_python_variant() {
        py::converter::registry::push_back(&convertible, &construct, py::type_id< std::variant<T...> >());
      }

      static bool _is_none(PyObject* obj_ptr) {
        return obj_ptr ? obj_ptr == py::object().ptr() : false;
      }

      static void* convertible(PyObject* obj_ptr) {
        if (_is_none(obj_ptr)) // ensure to passthrough None, to clear the variant
          return obj_ptr;
        if ((py::extract<T>{obj_ptr}.check() || ...))
          return obj_ptr;
        return nullptr;
      }

      static void construct(PyObject* obj_ptr, py::converter::rvalue_from_python_stage1_data* data) {
        void* storage = ((py::converter::rvalue_from_python_storage<std::variant<T...> >*) data)->storage.bytes;
        if (_is_none(obj_ptr)) {
          new (storage) std::variant<T...>();
        } else {
          auto try_construct = [&]<typename U>(std::type_identity<U>) {
            py::extract<U> extract{obj_ptr};
            if (!extract.check())
              return false;
            new (storage) std::variant<T...>(extract());
            return true;
          };
          if (!(try_construct(std::type_identity<T>{}) || ...))
            new (storage) std::variant<T...>();
        }
        data->convertible = storage;
      }
    };
  }

  /** @brief register_variant<T>() provides std::variant<T> converters
     * @param a std::variant<T...> tag
     */
  template <class... T>
  void register_variant(std::type_identity<std::variant<T...>>) {
    py::to_python_converter<std::variant<T...>, detail::to_python_variant<T...>>();
    detail::from_python_variant<T...>();
  }

}
