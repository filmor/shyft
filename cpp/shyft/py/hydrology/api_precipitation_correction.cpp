/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>

#include <shyft/hydrology/methods/precipitation_correction.h>

namespace expose {

    void precipitation_correction() {
        using namespace shyft::core::precipitation_correction;
        namespace py=boost::python;
        py::class_<parameter>("PrecipitationCorrectionParameter")
            .def(py::init<double>((py::arg("scale_factor")),"creates parameter object according to parameters"))
            .def_readwrite("scale_factor",&parameter::scale_factor,"default =1.0")
            ;
        py::class_<calculator>("PrecipitationCorrectionCalculator",
                "Scales precipitation with the specified scale factor"
            )
            .def(py::init<double>((py::arg("scale_factor")),"create a calculator using supplied parameter"))
            .def("calc",&calculator::calc,(py::arg("precipitation")),"returns scale_factor*precipitation\n")
            ;
     }
}
