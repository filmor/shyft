/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>

#include <shyft/hydrology/methods/skaugen.h>

namespace expose {

    void skaugen_snow() {
        using namespace shyft::core::skaugen;
        namespace py=boost::python;
        
        py::class_<parameter>("SkaugenParameter")
        .def(py::init<double,double,double,double,double,double,double,double>((py::arg("alpha_0"),py::arg("d_range"),py::arg("unit_size"),py::arg("max_water_fraction"),py::arg("tx"),py::arg("cx"),py::arg("ts"),py::arg("cfr")),"create parameter object with specifed values"))
        .def(py::init<const parameter&>((py::arg("p")),"create a clone of p"))
        .def_readwrite("alpha_0",&parameter::alpha_0,"default = 40.77")
        .def_readwrite("d_range",&parameter::d_range,"default = 113.0")
        .def_readwrite("unit_size",&parameter::unit_size,"default = 0.1")
        .def_readwrite("max_water_fraction",&parameter::max_water_fraction,"default = 0.1")
        .def_readwrite("tx",&parameter::tx,"default = 0.16")
        .def_readwrite("cx",&parameter::cx,"default = 2.5")
        .def_readwrite("ts",&parameter::ts,"default = 0.14")
        .def_readwrite("cfr",&parameter::cfr,"default = 0.01")
         ;

        py::class_<state>("SkaugenState")
         .def(py::init<double,py::optional<double,double,double,double,double,double>>((py::arg("nu"),py::arg("alpha"),py::arg("sca"),py::arg("swe"),py::arg("free_water"),py::arg("residual"),py::arg("num_units")),"create a state with specified values"))
         .def(py::init<const state&>((py::arg("s")),"create a clone of s"))
         .def_readwrite("nu",&state::nu,"")
         .def_readwrite("alpha",&state::alpha,"")
         .def_readwrite("sca",&state::sca,"")
         .def_readwrite("swe",&state::swe,"")
         .def_readwrite("free_water",&state::free_water,"")
         .def_readwrite("residual",&state::residual,"")
         .def_readwrite("num_units",&state::num_units,"")
         ;

        py::class_<response>("SkaugenResponse")
         .def_readwrite("sca",&response::sca,"snow-covered area in fraction")
         .def_readwrite("swe",&response::swe,"snow water equivalient [mm] over cell-area")
         .def_readwrite("outflow",&response::outflow,"from snow-routine in [mm/h] over cell-area")
         .def_readwrite("total_stored_water",&response::swe,"same as swe(deprecated)")
         ;

        typedef  calculator<parameter,state,response> SkaugenCalculator;
        py::class_<SkaugenCalculator>("SkaugenCalculator",
                "Skaugen snow model method\n"
                "\n"
                "This algorithm uses theory from Skaugen \n"
                "\n"
            )
            .def("step",&SkaugenCalculator::step,(py::arg("delta_t"),py::arg("parameter"),py::arg("temperature"),py::arg("precipitation"),py::arg("radiation"),py::arg("wind_speed"),py::arg("state"),py::arg("response")),"steps the model forward delta_t seconds, using specified input, updating state and response")
            ;

    }
}
