/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>
#include <future>
#include <mutex>
#include <atomic>
#include <shyft/hydrology/srv/server.h>
#include <shyft/hydrology/srv/client.h>
#include <shyft/py/scoped_gil.h>

namespace expose {
    using std::mutex;
    using std::unique_lock;
    using std::string;
    using std::vector;
    using std::shared_ptr;
    using shyft::hydrology::srv::client;
    using shyft::hydrology::srv::server;
    using shyft::hydrology::srv::rmodel_type;
    using shyft::hydrology::srv::parameter_variant_t;
    using shyft::hydrology::srv::state_variant_t;
    using shyft::hydrology::srv::gta_t;
    using shyft::hydrology::srv::calibration_options;
    using shyft::hydrology::srv::calibration_status;
    using shyft::core::model_calibration::target_specification;
    
    namespace py=boost::python;
    using namespace shyft::py;
    using shyft::time_series::dd::apoint_ts;
    using shyft::core::q_adjust_result;
    
    namespace { 
    static std::atomic_size_t py_client_count{0}; // just to help python get an indicator of number of connections pr. process
    
    /**
     * @brief a util class that combines gil release with client mutex unique_lock
     */
    struct safe {
        safe(mutex&mx):lck{mx}{}
        unique_lock<mutex> lck;
        scoped_gil_release gil;
    };
    

    /** @brief A  client 
     *
     * @details
     * This class takes care of  python gil and mutex(ref safe) , ensuring that any attempt using
     * multiple python threads will be serialized.
     * gil is released while the call is in progress.
     * 
     */
    struct py_client {
        using client_t=client;
        mutex mx; ///< to enforce just one thread active on this client object at a time
        client_t impl;
        py_client(const std::string& host_port,int timeout_ms):impl{host_port,timeout_ms} {
            ++py_client_count;
        }
        ~py_client() { --py_client_count;}
        static size_t get_client_count() { return py_client_count.load();}
        py_client()=delete;
        py_client(py_client const&) = delete;
        py_client(py_client &&) = delete;
        py_client& operator=(py_client const&o) = delete;

        string get_host_port() {
            return impl.c.host_port;
        }
        int get_timeout_ms() {
            return impl.c.timeout_ms;
        }
        bool is_open() const {
            return impl.c.is_open;
        }
        size_t get_reconnect_count() const {
            return impl.c.reconnect_count;
        }

        void close(int /*timeout_ms*/=1000) {
            safe _{mx};
            impl.close();
        }

        string get_server_version() {
            safe _{mx};
            return impl.get_version_info();
        }
        
        vector<string> get_model_ids() {
            safe _{mx};
            return impl.get_model_ids();
        }
        
        bool remove_model(string const& mid) {
            safe _{mx};
            return impl.remove_model(mid);
        }
        
        bool rename_model(string const& mid, string const&new_mid) {
            safe _{mx};
            return impl.rename_model(mid,new_mid);
        }
        
        bool create_model(string const& mid, 
                          rmodel_type mtype, 
                          vector <shyft::core::geo_cell_data> const& gcd){
            safe _{mx};
            return impl.create_model(mid, mtype, gcd);
        }
        
        state_variant_t get_state_variant(string const& mid, const shyft::api::cids_t& cids) {
            safe _{mx};
            return impl.get_state(mid, cids);
        }
        
        bool set_initial_state(string const& mid) {
            safe _{mx};
            return impl.set_initial_state(mid);
        }
        
        bool set_state_variant(string const& mid, state_variant_t sv){
            safe _{mx};
            return impl.set_state(mid, sv);
        }
        
        template<class S>
        bool set_state(string const& mid, S s) { return set_state_variant(mid,state_variant_t{s}); }


        bool set_region_parameter_variant(string const& mid, 
                       parameter_variant_t pv){
            safe _{mx};
            return impl.set_region_parameter(mid, pv);
        }
        
        bool set_catchment_parameter_variant(string const& mid, 
                       parameter_variant_t pv,size_t cid){
            safe _{mx};
            return impl.set_catchment_parameter(mid, pv,cid);
        }
        parameter_variant_t get_catchment_parameter(string const& mid, size_t cid){
            safe _{mx};
            return impl.get_catchment_parameter(mid, cid);
        }
        bool has_catchment_parameter(string const& mid, size_t cid){
            safe _{mx};
            return impl.has_catchment_parameter(mid, cid);
        }
        void remove_catchment_parameter(string const& mid, size_t cid){
            safe _{mx};
            impl.remove_catchment_parameter(mid, cid);
        }
        
        parameter_variant_t get_region_parameter(string const& mid){
            safe _{mx};
            return impl.get_region_parameter(mid);
        }
        gta_t get_time_axis(string const& mid) {
            safe _{mx};
            return impl.get_time_axis(mid);
        }
        
        template<class P>
        bool set_region_parameter(string const& mid, P p) { return set_region_parameter_variant(mid,parameter_variant_t{p}); }

        template<class P>
        bool set_catchment_parameter(string const& mid, P p, size_t cid) { return set_catchment_parameter_variant(mid,parameter_variant_t{p},cid); }
        
        bool run_interpolation(string const& mid, 
                         const shyft::core::interpolation_parameter& ip_parameter,
                         const shyft::time_axis::generic_dt& ta, 
                         const shyft::api::a_region_environment& r_env, 
                         bool best_effort){
            safe _{mx};
            return impl.run_interpolation(mid, ip_parameter, ta, r_env, best_effort);
        }
        
        bool run_cells(string const& mid,size_t use_ncore,int start_step,int n_steps){
            safe _{mx};
            return impl.run_cells(mid,use_ncore,start_step,n_steps);
        }
        
        q_adjust_result adjust_q(string const& mid, const shyft::api::cids_t& indexes, double wanted_q,size_t start_step=0,double scale_range=3.0,double scale_eps=1e-3,size_t max_iter=300,size_t n_steps=1){
            safe _{mx};
            return impl.adjust_q(mid, indexes, wanted_q,start_step,scale_range,scale_eps,max_iter,n_steps);
        }
        
        apoint_ts get_discharge(string const& mid, const shyft::api::cids_t& indexes, shyft::core::stat_scope ix_type){
            safe _{mx};
            return impl.get_discharge(mid, indexes,ix_type);
        }
        
        apoint_ts get_temperature(string const& mid, const shyft::api::cids_t& indexes, shyft::core::stat_scope ix_type){
            safe _{mx};
            return impl.get_temperature(mid, indexes,ix_type);
        }
        
        apoint_ts get_precipitation(string const& mid, const shyft::api::cids_t& indexes, shyft::core::stat_scope ix_type){
            safe _{mx};
            return impl.get_precipitation(mid, indexes,ix_type);
        }
        
        apoint_ts get_snow_swe(string const& mid, const shyft::api::cids_t& indexes, shyft::core::stat_scope ix_type){
            safe _{mx};
            return impl.get_snow_swe(mid, indexes,ix_type);
        }
        
        apoint_ts get_charge(string const& mid, const shyft::api::cids_t& indexes, shyft::core::stat_scope ix_type){
            safe _{mx};
            return impl.get_charge(mid, indexes,ix_type);
        }
        
        apoint_ts get_snow_sca(string const& mid, const shyft::api::cids_t& indexes, shyft::core::stat_scope ix_type){
            safe _{mx};
            return impl.get_snow_sca(mid, indexes,ix_type);
        }
        
        apoint_ts get_radiation(string const& mid, const shyft::api::cids_t& indexes, shyft::core::stat_scope ix_type){
            safe _{mx};
            return impl.get_radiation(mid, indexes,ix_type);
        }
        
        apoint_ts get_wind_speed(string const& mid, const shyft::api::cids_t& indexes, shyft::core::stat_scope ix_type){
            safe _{mx};
            return impl.get_wind_speed(mid, indexes,ix_type);
        }
        
        apoint_ts get_rel_hum(string const& mid, const shyft::api::cids_t& indexes, shyft::core::stat_scope ix_type){
            safe _{mx};
            return impl.get_rel_hum(mid, indexes,ix_type);
        }
        
        bool set_catchment_calculation_filter(string const& mid, const std::vector<int64_t>& catchment_id_list){
            safe _{mx};
            return impl.set_catchment_calculation_filter(mid, catchment_id_list);
        }
        
        bool revert_to_initial_state(string const& mid){
            safe _{mx};
            return impl.revert_to_initial_state(mid);
        }
        
        bool clone_model(string const& old_mid, string const& new_mid) {
            safe _{mx};
            return impl.clone_model(old_mid, new_mid);
        }
        
        bool copy_model(string const& old_mid, string const& new_mid) {
            safe _{mx};
            return impl.copy_model(old_mid, new_mid);
        }
        
        bool set_state_collection(string const& mid, int64_t catchment_id, bool on_or_off) {
            safe _{mx};
            return impl.set_state_collection(mid, catchment_id, on_or_off);
        }
        
        bool set_snow_sca_swe_collection(string const& mid, int64_t catchment_id, bool on_or_off) {
            safe _{mx};
            return impl.set_snow_sca_swe_collection(mid, catchment_id, on_or_off);
        }
        
        bool is_cell_env_ts_ok(string const& mid) {
            safe _{mx};
            return impl.is_cell_env_ts_ok(mid);
        }
        
        bool is_calculated(string const& mid, size_t cid) {
            safe _{mx};
            return impl.is_calculated(mid, cid);
        }
        
        bool start_calibration(string const& mid,parameter_variant_t const& p_start, parameter_variant_t const& p_min,parameter_variant_t const& p_max, vector<target_specification> const& spec, calibration_options opt) {
            if(p_start.which()!=p_min.which() || p_start.which() != p_max.which())
                throw std::runtime_error("All parameters passed to calibration must be for the same model type");
            safe _{mx};
            return impl.start_calibration(mid, p_start,p_min,p_max,spec,opt);
        }
        calibration_status check_calibration(string const& mid) {
            safe _{mx};
            return impl.check_calibration(mid);
        }
        bool cancel_calibration(string const& mid) {
            safe _{mx};
            return impl.cancel_calibration(mid);
        }

        std::vector<shyft::core::geo_cell_data> get_geo_cell_data(string const &mid) {
            safe _{mx};
            return impl.get_geo_cell_data(mid);
        }

        shyft::api::a_region_environment get_region_env(string const &mid) {
            safe _{mx};
            return impl.get_region_env(mid);
        }
        
        shyft::core::interpolation_parameter get_interpolation_parameter(string const &mid) {
            safe _{mx};
            return impl.get_interpolation_parameter(mid);
        }
        
        string get_description(string const &mid) {
            safe _{mx};
            return impl.get_description(mid);
        }
        bool set_description(string const &mid,string const&description) {
            safe _{mx};
            return impl.set_description(mid,description);
        }
        bool do_fx(string const&mid,string const&fx_args) {
            safe _{mx};
            return impl.fx(mid,fx_args);
        }
        void close_conn() {//weird, close is not a name we can use here..
            safe _{mx};
            impl.close();
        }
    };

    /** @brief The server side component for a model repository
     *
     * 
     * This class wraps/provides the server-side 
     * suitable for exposure to python.
     * 
     */
    
    struct py_server  {
        server impl;
        py::object py_fx_cb;///< python callback that can be set by the py user
       void handle_pyerror() {
            // from SO: https://stackoverflow.com/questions/1418015/how-to-get-python-exception-text
            namespace py=boost::python;
            using namespace boost;
            std::string msg{"unspecified error"};
            if(PyErr_Occurred()) {
                PyObject *exc,*val,*tb;
                py::object formatted_list, formatted;
                PyErr_Fetch(&exc,&val,&tb);
                py::handle<> hexc(exc),hval(py::allow_null(val)),htb(py::allow_null(tb));
                py::object traceback(py::import("traceback"));
                if (!tb) {
                    py::object format_exception_only{ traceback.attr("format_exception_only") };
                    formatted_list = format_exception_only(hexc,hval);
                } else {
                    py::object format_exception{traceback.attr("format_exception")};
                    if (format_exception) {
                        try {
                            formatted_list = format_exception(hexc, hval, htb);
                        } catch (...) { // any error here, and we bail out, no crash please
                            msg = "not able to extract exception info";
                        }
                    } else
                        msg="not able to extract exception info";
                }
                if (formatted_list) {
                    formatted = py::str("\n").join(formatted_list);
                    msg = py::extract<std::string>(formatted);
                }
            }
            py::handle_exception();
            PyErr_Clear();
            throw std::runtime_error(msg);
        }
        py_server() {
            if (!PyEval_ThreadsInitialized()) {
                PyEval_InitThreads();// ensure threads-is enabled
            }
            impl.fx_cb = [&](string mid,string fx_args)->bool { return this->py_do_fx(mid,fx_args); };
        }
        size_t get_alive_connections() {return impl.alive_connections.load();}
        void set_listening_port(int port) {impl.set_listening_port(port);}
        int  get_listening_port() { return impl.get_listening_port();}
        
        void set_max_connections(int  n) {impl.set_max_connections(size_t(n));}
        int  get_max_connections() { return int(impl.get_max_connections());}
        
        void set_listening_ip(string const& ip) {impl.set_listening_ip(ip);}
        string  get_listening_ip() {return impl.get_listening_ip();}
        
        int start_server() {return impl.start_server(); }
        void stop_server(int timeout_ms) { 
            impl.set_graceful_close_timeout(timeout_ms);
            impl.clear(); 
        }
        
        string get_version_info() {return impl.do_get_version_info();}
        
        bool is_running() { return impl.is_running(); }
        
        ~py_server() { }

        //-- py exposed functions, to be used by the server-side python:
        vector<string> get_model_ids() {
            scoped_gil_release gil;
            return impl.do_get_model_ids();
        }
        shyft::hydrology::srv::model_variant_t get_model(string mid) {
            scoped_gil_release gil;
            return impl.get_model(mid);
        }
        /** this is where we attempt to fire user specified callback, */
        bool py_do_fx(string const & mid, string const& fx_arg) {
            bool r{false};
            if (py_fx_cb.ptr() != Py_None) {
                scoped_gil_aquire gil;// we need to use the GIL here before trying to call python.
                try {
                    r = boost::python::call<bool>(py_fx_cb.ptr(), mid,fx_arg);
                } catch  (const boost::python::error_already_set&) {
                    handle_pyerror();
                }
            }
            return r;
        }
        //-- get rid of stuff that would not work
         py_server(py_server const&)=delete;
         py_server(py_server&&)=delete;
         py_server& operator=(py_server const &)=delete;
         py_server& operator=(py_server&&)=delete;
    };

    void expose_common() {
        py::enum_<rmodel_type> ("RegionModelType","Ref to DrmClient, used do specify what remote region-model type to create")
        .value("PT_GS_K",rmodel_type::pt_gs_k)
        .value("PT_GS_K_OPT",rmodel_type::pt_gs_k_opt)
        .value("PT_SS_K",rmodel_type::pt_ss_k)
        .value("PT_SS_K_OPT",rmodel_type::pt_ss_k_opt)
        .value("PT_HS_K",rmodel_type::pt_hs_k)
        .value("PT_HS_K_OPT",rmodel_type::pt_hs_k_opt)
        .value("PT_HPS_K",rmodel_type::pt_hps_k)
        .value("PT_HPS_K_OPT",rmodel_type::pt_hps_k_opt)
        .value("R_PM_GS_K",rmodel_type::r_pm_gs_k)
        .value("R_PM_GS_K_OPT",rmodel_type::r_pm_gs_k_opt)
        .value("PT_ST_K",rmodel_type::pt_st_k)
        .value("PT_ST_K_OPT",rmodel_type::pt_st_k_opt)
        .value("PT_ST_HBV",rmodel_type::pt_st_hbv)
        .value("PT_ST_K_HBV",rmodel_type::pt_st_hbv_opt)
        .value("R_PT_GS_K",rmodel_type::r_pt_gs_k)
        .value("R_PT_GS_K_OPT",rmodel_type::r_pt_gs_k_opt)
        .export_values();
    }

    template <class T>
    struct x_ext {
        static T* x_self(const py::tuple& args) {
            py::object self = args[0];
            py::extract<T*> xtract_self(self);
            return xtract_self();
        }
        static std::tuple<T*,string> x_self_mid(py::tuple args) {
            if (py::len(args) < 2)
                throw std::runtime_error("require at least 2 args: self, mid");
            return std::make_tuple(py::extract<T*>(args[0])(),py::extract<string>(args[1])());
        }
    };

    struct server_ext:x_ext<py_server> {

        static py::object get_model(py::tuple args, py::dict /*kwargs*/) {
            auto [self,mid] = x_self_mid(args);
            auto s = self->get_model(mid);
            return boost::apply_visitor(
                [](const auto&x) -> py::object {
                    return py::object(x);
                }
                ,s);
        }
    };

    struct client_ext:x_ext<py_client> {

        static py::object get_state(py::tuple args, py::dict /*kwargs*/) {
            auto [self,mid] = x_self_mid(args);
            vector<int64_t> cids;
            if(py::len(args)==3) {
                py::extract<vector<int64_t>> x_cids(args[2]);
                cids=x_cids();
            }
            auto s = self->get_state_variant(mid, cids);
            return boost::apply_visitor(
                [](const auto&x) -> py::object {
                    return py::object(x);
                }
                ,s);
        }

        static py::object get_catchment_parameter(py::tuple args, py::dict /*kwargs*/) {
            if (py::len(args) < 3)
                throw std::runtime_error("require args: self, mid,cid");
            auto [self,mid] = x_self_mid(args);
            
            auto cid=py::extract<int>(args[2])();
            auto s = self->get_catchment_parameter(mid, cid);
            return boost::apply_visitor(
                [](const auto&x) -> py::object {
                    return py::object(x);
                }
                ,s);
        }
        
        static py::object get_region_parameter(py::tuple args, py::dict /*kwargs*/) {
            auto [self,mid] = x_self_mid(args);
            auto s = self->get_region_parameter(mid);
            return boost::apply_visitor(
                [](const auto&x) -> py::object {
                    return py::object(x);
                }
                ,s);
        }
    };
}
    namespace {
        using namespace shyft::core;
        using shyft::api::cell_state_with_id;
        using shyft::hydrology::srv::model_variant_t;
        template<class T>    
        struct p_variant_from_p { // class to support automagic a_wrap<A> -> A in python
            static void construct( T& src, parameter_variant_t* dst) {
                new (dst) parameter_variant_t(std::make_shared<T>(src)); // this will inc-ref a shared ptr, copy-construct a value-type, like we want.
            };
        };


        template<class T>    
        struct state_variant_from_state { // class to support automagic a_wrap<A> -> A in python
            static void construct( T& src, state_variant_t* dst) {
                new (dst) state_variant_t(src); // this will inc-ref a shared ptr, copy-construct a value-type, like we want.
            };
        };
        
        template <class T>
        void reg_parameter_to_variant_converter() {
            py::fx_implicitly_convertible<T,parameter_variant_t,p_variant_from_p<T>>();
        }


        template <class T>
        void reg_state_to_variant_converter() {
            using namespace shyft::core;
            py::fx_implicitly_convertible<T,state_variant_t,state_variant_from_state<T>>();
        }

        
        void reg_parameter_to_variant_converters() {
            // register all parameter converters here so that 
            // we automagically converts a parameter to it's internal parameter_variant_t
            reg_parameter_to_variant_converter<pt_gs_k::parameter_t>();
            reg_parameter_to_variant_converter<pt_ss_k::parameter_t>();
            reg_parameter_to_variant_converter<pt_hs_k::parameter_t>();
            reg_parameter_to_variant_converter<pt_hps_k::parameter_t>();
            reg_parameter_to_variant_converter<r_pm_gs_k::parameter_t>();
            reg_parameter_to_variant_converter<pt_st_k::parameter_t>();
            reg_parameter_to_variant_converter<pt_st_hbv::parameter_t>();
            reg_parameter_to_variant_converter<r_pt_gs_k::parameter_t>();
        }

        void reg_state_to_variant_converters() {
            reg_state_to_variant_converter<shared_ptr<vector<cell_state_with_id<pt_gs_k::state_t>>>>();
            reg_state_to_variant_converter<shared_ptr<vector<cell_state_with_id<pt_ss_k::state_t>>>>();
            reg_state_to_variant_converter<shared_ptr<vector<cell_state_with_id<pt_hs_k::state_t>>>>();
            reg_state_to_variant_converter<shared_ptr<vector<cell_state_with_id<pt_hps_k::state_t>>>>();
            reg_state_to_variant_converter<shared_ptr<vector<cell_state_with_id<r_pm_gs_k::state_t>>>>();
            reg_state_to_variant_converter<shared_ptr<vector<cell_state_with_id<pt_st_k::state_t>>>>();
            reg_state_to_variant_converter<shared_ptr<vector<cell_state_with_id<pt_st_hbv::state_t>>>>();
            reg_state_to_variant_converter<shared_ptr<vector<cell_state_with_id<r_pt_gs_k::state_t>>>>();
        }

    }
    /** @brief Expose to python the client side api for a model-repository of type M
     * 
     * @tparam M the model type, same requirements as for shyft::srv::client
     */
    
    void expose_client() {
        using cm=py_client;
        using shyft::api::cell_state_with_id;
        using namespace shyft::core;
        reg_parameter_to_variant_converters();
        reg_state_to_variant_converters();
        py::class_<cm,boost::noncopyable>(
            "DrmClient",
            doc_intro(
                "Distributed region model client provides all needed functionality to transfer\n"
                "Shyft region-models, and then run simulations/optimizations"
            ),
            py::no_init
            )
            .def(py::init<string const&,int>(
                (py::arg("self"),py::arg("host_port"),py::arg("timeout_ms")),
                "TODO"
                )
            )
            .add_static_property("total_clients", &cm::get_client_count) // docstring not supported for add_static_property
            .def_readonly("host_port", &cm::get_host_port, "Endpoint network address of the remote server.")
            .def_readonly("timeout_ms", &cm::get_timeout_ms, "Timout for remote server operations, in number milliseconds.")
            .def_readonly("is_open", &cm::is_open, "If the connection to the remote server is (still) open.")
            .def_readonly("reconnect_count", &cm::get_reconnect_count, "Number of reconnects to the remote server that have been performed.")
            .def("close", &cm::close_conn, (py::arg("self")),
                doc_intro("Close the connection. It will automatically reopen if needed.")
            )
            .def("get_server_version",&cm::get_server_version,
                doc_intro("returns the server version")
                doc_returns("version","str","Server version string")
            )
            .def("create_model",&cm::create_model,
                (py::arg("self"), py::arg("mid"), py::arg("mtype"), py::arg("gcd")),
                doc_intro("create the model")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("mtype", "enum rmodel_type", "rmodel_type.pt_gs_k rmodel_type.pt_gs_k_opt or similar model type")
                doc_parameter("gcd", "GeoCellDataVector", "strongly typed list of GeoCellData")
                doc_returns("", "bool", "true if succeeded")
            )
            .def("remove_model",&cm::remove_model,
                (py::arg("self"), py::arg("mid")),
                doc_intro("remove the specified model")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_returns("", "bool", "true if succeeded")
            )
            .def("rename_model",&cm::rename_model,
                (py::arg("self"), py::arg("mid"),py::arg("new_mid")),
                doc_intro("rename the specified model")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("new_mid","str", "the new wanted model-id for the model")
                doc_returns("", "bool", "true if succeeded")
            )
            .def("clone_model",&cm::clone_model,
                (py::arg("self"), py::arg("mid"),py::arg("new_mid")),
                doc_intro("clone the specified model")
                doc_parameters()
                doc_parameter("mid", "str", "original model")
                doc_parameter("new_mid","str", "the model-id for the cloned model")
                doc_returns("", "bool", "true if succeeded")
            )
            .def("copy_model",&cm::copy_model,
                (py::arg("self"), py::arg("mid"),py::arg("new_mid")),
                doc_intro("full copy of the specified model")
                doc_parameters()
                doc_parameter("mid", "str", "original model")
                doc_parameter("new_mid","str", "the model-id for the copied model")
                doc_returns("", "bool", "true if succeeded")
            )
            .def("get_model_ids",&cm::get_model_ids,
                (py::arg("self")),
                doc_intro("returns a list of model ids (mids) that is alive and known at the remote server")
                doc_returns("", "StringList", "Strongly typed list of strings naming the available models on the drms")
            )
            //-- overloading all stack states goes here (alternative: register implicit convert from T -> variant<T> for state etc)
            .def("set_state",&cm::set_state_variant,
                (py::arg("self"), py::arg("mid"), py::arg("state")),
                doc_intro("set the cell state")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("state", "StateWithIdVector", "strongly typed list of stack state with id vector")
                doc_returns("", "bool", "true if succeeded")
            )
            
            .def("get_state",raw_function(client_ext::get_state),
                //(py::arg("self"), py::arg("mid"), py::arg("indexes")),
                doc_intro("Extract cell state for the optionaly specified catchment ids, cids")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("indexes","IntVector","list of catchment-id's, if empty, extract all")
                doc_returns("","xStateWithIdVector","strongly typed list of stack state with identifier for the cells")
            )
            .def("get_region_parameter",raw_function(client_ext::get_region_parameter),
                //(py::arg("self"), py::arg("mid")),
                doc_intro("Get the region model parameter settings")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_returns("","XXXParameter","The region parameter for the model")
            )
            .def("get_catchment_parameter",raw_function(client_ext::get_catchment_parameter),
                //(py::arg("self"), py::arg("mid"), py::arg("cid")),
                doc_intro("Get the effective catchment parameter for the specified catchment")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("cid", "int", "catchment identifier")
                doc_returns("","XXXParameter","The catchment parameter for specified catchment")
            )
            .def("has_catchment_parameter",&cm::has_catchment_parameter,(py::arg("self"),py::arg("mid"),py::arg("cid")),
                doc_intro("Get the current time-axis of the model")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("cid", "int", "catchment identifier")
                doc_returns("","bool","True if catchment has its own parameter setting")
             )
            .def("remove_catchment_parameter",&cm::remove_catchment_parameter,(py::arg("self"),py::arg("mid"),py::arg("cid")),
                doc_intro("Remove the specified catchments local parameter, and set it to the region parameter")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("cid", "int", "catchment identifier")
             )
            .def("get_time_axis",&py_client::get_time_axis,(py::arg("self"),py::arg("mid")),
                doc_intro("Get the current time-axis of the model")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_returns("","TimeAxis","The region model time-axis")                
            )
            .def("cancel_calibration",&cm::cancel_calibration,(py::arg("self"),py::arg("mid")),
                doc_intro("Cancels any ongoing not finished calibration for the specified model")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_returns("","bool","indicating success")                
            )
            .def("start_calibration",&cm::start_calibration,(py::arg("self"),py::arg("mid"),py::arg("p_start"),py::arg("p_min"),py::arg("p_max"),py::arg("spec"),py::arg("opt")),
                doc_intro("Start calibration with specified parameters for the model given by 'mid'")
                doc_parameters()
                doc_parameter("mid", "", "model identifier")
                doc_parameter("p_start", "", "starting parameters")
                doc_parameter("p_min", "", "min-range for parameters")
                doc_parameter("p_max", "", "max-range for parameters")
                doc_parameter("spec", "", "goal function specification, including reference series and weights")
                doc_parameter("opt", "", "optimizer options, like BOBYQA, GLOBAL,SCEUA or DREAM etc.")                
                doc_returns("","bool","indicating success")                
            )
            .def("check_calibration",&cm::check_calibration,(py::arg("self"),py::arg("mid")),
                doc_intro("Check ongoing calibration, return intermediate status with trace, or final status with trace and resulting parameters")
                doc_parameters()
                doc_parameter("mid", "", "model identifier")
                doc_returns("","","Calibration Status class with information and results")                
            )
   
            
            //-- overloading all stack parameters goes here (see alt. using impl. type conversion registration for state)
            .def("set_region_parameter",&cm::set_region_parameter_variant,
                 (py::arg("self"), py::arg("mid"), py::arg("parameter")),
                 doc_intro("set region parameter")
                 doc_parameters()
                 doc_parameter("mid", "str", "model identifier")
                 doc_parameter("parameter", "XXXXParameter", "strongly typed method stack parameter")
                 doc_returns("", "bool", "true if succeeded")
             )
            .def("set_catchment_parameter",&cm::set_catchment_parameter_variant,
                 (py::arg("self"), py::arg("mid"), py::arg("parameter"),py::arg("cid")),
                 doc_intro("set catchment parameter")
                 doc_parameters()
                 doc_parameter("mid", "str", "model identifier")
                 doc_parameter("parameter", "XXXXParameter", "strongly typed method stack parameter")
                 doc_parameter("cid","int","catchment id that should get the supplied parameter")
                 doc_returns("", "bool", "true if succeeded")
             )

            //--
            .def("run_interpolation",&cm::run_interpolation,
                (py::arg("self"), py::arg("mid"), py::arg("ip_parameter"), py::arg("ta"), py::arg("r_env"), py::arg("best_effort")),
                doc_intro("initializes the cell environment and project region environment time_series to cells.")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("ip_parameter", "InterpolationParameter", "interpolation parameter")
                doc_parameter("ta", "TimeAxis", "time axis")
                doc_parameter("r_env", "ARegionEnvironment", "region environment containing all geo-located sources")
                doc_parameter("best_effort", "bool", "best effort type interpolation")
                doc_returns("", "bool", "true if succeeded")
            )
            
            .def("run_cells",&cm::run_cells,
                (py::arg("self"), py::arg("mid"),py::arg("use_ncore")=0,py::arg("start_step")=0,py::arg("n_steps")=0
                ),
                doc_intro("Run calculation over the specified time axis. Requires that cells and environment have been initialized and interpolated.")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("use_ncore","int","default 0, means autodetect core, otherwise you can specify 1..n")
                doc_parameter("start_step","int","default 0, from start step of time-axis")
                doc_parameter("n_steps","int","default 0, means run entire time-axis, 1..n means number of steps from start_step")
                doc_returns("", "bool", "true if succeeded")
            )
            
            .def("adjust_q",&cm::adjust_q,
                (py::arg("self"), py::arg("mid"), py::arg("indexes"), py::arg("wanted_q"),
                    py::arg("start_step")=0,py::arg("scale_range")=3.0,py::arg("scale_eps")=1e-3,py::arg("max_iter")=300,py::arg("n_steps")=1
                ),
                doc_intro("State adjustment to achieve wanted/observed flow")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("indexes", "List[int]", "cell ids that should be adjusted")
                doc_parameter("wanted_q", "float", "the average flow first time-step we want to achieve, in m**3/s")
                doc_parameter("start_step","int","time-axis start step index, default=0")
                doc_parameter("scale_range","float","default=3, min,max= scale0/scale_range.. scale0*scale_range")
                doc_parameter("scale_eps","float","default 1e-3, stop iteration when scale-change is less than this")
                doc_parameter("max_iter","int","default=300, stop searching for solution after this limit is reached")
                doc_parameter("n_steps","int","default=1, number of steps on time-axis to average to match wanted flow")
                doc_returns("", "q_adjust_result", "obtained flow in m**3/s. This can deviate from wanted flow due to model and state constraints. q_r is optimized q and q_0 is unadjusted.")
            )
            
            .def("get_discharge",&cm::get_discharge,
                (py::arg("self"), py::arg("mid"), py::arg("indexes"),py::arg("index_type")=shyft::core::stat_scope::catchment_ix),
                doc_intro("get the discharge")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("indexes", "List[int]", "indexes to be retrieved, the index_type tells if it's cells or catchment indexes")
                doc_parameter("index_type","stat_scope","stat_scope.(cell|catchment) denotes the type of indexes passed. cell returns sum of cell indexes, catchment returns sum of catchments")
                doc_returns("", "TimeSeries", "returns sum  for catcment_ids")
            )
            
            .def("get_temperature",&cm::get_temperature,
                (py::arg("self"), py::arg("mid"), py::arg("indexes"),py::arg("index_type")=shyft::core::stat_scope::catchment_ix),
                doc_intro("get the temperature")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("indexes", "List[int]", "indexes to be retrieved, the index_type tells if it's cells or catchment indexes")
                doc_parameter("index_type","stat_scope","stat_scope.(cell|catchment) denotes the type of indexes passed. cell returns sum of cell indexes, catchment returns sum of catchments")
                doc_returns("", "TimeSeries", "returns sum  for catcment_ids")
            )
            
            .def("get_precipitation",&cm::get_precipitation,
                (py::arg("self"), py::arg("mid"), py::arg("indexes"),py::arg("index_type")=shyft::core::stat_scope::catchment_ix),
                doc_intro("get the precipitation")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("indexes", "List[int]", "indexes to be retrieved, the index_type tells if it's cells or catchment indexes")
                doc_parameter("index_type","stat_scope","stat_scope.(cell|catchment) denotes the type of indexes passed. cell returns sum of cell indexes, catchment returns sum of catchments")
                doc_returns("", "TimeSeries", "returns sum  for catcment_ids")
            )
            
            .def("get_snow_swe",&cm::get_snow_swe,
                (py::arg("self"), py::arg("mid"), py::arg("indexes"),py::arg("index_type")=shyft::core::stat_scope::catchment_ix),
                doc_intro("get the snow_swe")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("indexes", "List[int]", "indexes to be retrieved, the index_type tells if it's cells or catchment indexes")
                doc_parameter("index_type","stat_scope","stat_scope.(cell|catchment) denotes the type of indexes passed. cell returns sum of cell indexes, catchment returns sum of catchments")
                doc_returns("", "TimeSeries", "returns sum  for catcment_ids")
            )
            
            .def("get_charge",&cm::get_charge,
                (py::arg("self"), py::arg("mid"), py::arg("indexes"),py::arg("index_type")=shyft::core::stat_scope::catchment_ix),
                doc_intro("get the charge")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("indexes", "List[int]", "indexes to be retrieved, the index_type tells if it's cells or catchment indexes")
                doc_parameter("index_type","stat_scope","stat_scope.(cell|catchment) denotes the type of indexes passed. cell returns sum of cell indexes, catchment returns sum of catchments")
                doc_returns("", "TimeSeries", "returns sum  for catcment_ids")
            )
            
            .def("get_snow_sca",&cm::get_snow_sca,
                (py::arg("self"), py::arg("mid"), py::arg("indexes"),py::arg("index_type")=shyft::core::stat_scope::catchment_ix),
                doc_intro("get the snow_sca")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("indexes", "List[int]", "indexes to be retrieved, the index_type tells if it's cells or catchment indexes")
                doc_parameter("index_type","stat_scope","stat_scope.(cell|catchment) denotes the type of indexes passed. cell returns sum of cell indexes, catchment returns sum of catchments")
                doc_returns("", "TimeSeries", "returns sum  for catcment_ids")
            )
            
            .def("get_radiation",&cm::get_radiation,
                (py::arg("self"), py::arg("mid"), py::arg("indexes"),py::arg("index_type")=shyft::core::stat_scope::catchment_ix),
                doc_intro("get the radiation")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("indexes", "List[int]", "indexes to be retrieved, the index_type tells if it's cells or catchment indexes")
                doc_parameter("index_type","stat_scope","stat_scope.(cell|catchment) denotes the type of indexes passed. cell returns sum of cell indexes, catchment returns sum of catchments")
                doc_returns("", "TimeSeries", "returns sum  for catcment_ids")
            )
            
            .def("get_wind_speed",&cm::get_wind_speed,
                (py::arg("self"), py::arg("mid"), py::arg("indexes"),py::arg("index_type")=shyft::core::stat_scope::catchment_ix),
                doc_intro("get the wind speed")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("indexes", "List[int]", "indexes to be retrieved, the index_type tells if it's cells or catchment indexes")
                doc_parameter("index_type","stat_scope","stat_scope.(cell|catchment) denotes the type of indexes passed. cell returns sum of cell indexes, catchment returns sum of catchments")
                doc_returns("", "TimeSeries", "returns sum  for catcment_ids")
            )
            
            .def("get_rel_hum",&cm::get_rel_hum,
                (py::arg("self"), py::arg("mid"), py::arg("indexes"),py::arg("index_type")=shyft::core::stat_scope::catchment_ix),
                doc_intro("get the rel_hum")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("indexes", "List[int]", "indexes to be retrieved, the index_type tells if it's cells or catchment indexes")
                doc_parameter("index_type","stat_scope","stat_scope.(cell|catchment) denotes the type of indexes passed. cell returns sum of cell indexes, catchment returns sum of catchments")
                doc_returns("", "TimeSeries", "returns sum  for catcment_ids")
            )
            
            .def("set_catchment_calculation_filter", &cm::set_catchment_calculation_filter,
                 (py::arg("self"), py::arg("mid"),py::arg("catchment_id_list")),
                 doc_intro("set catchment calculation filter")
                 doc_parameters()
                 doc_parameter("mid", "str", "model identifier")
                 doc_parameter("catchment_id_list", "List[int]", "list of catchment indexes")
                 doc_returns("", "bool", "true if succeeded")
             )
            .def("set_current_state_as_initial_state", &cm::set_initial_state,
                 (py::arg("self"), py::arg("mid")),
                 doc_intro("set current state as initial state for the region model.\n"
                           "the method .revert_to_initial_state will restore this state.\n")
                 doc_parameters()
                 doc_parameter("mid", "str", "model identifier")
                 doc_returns("", "bool", "true if succeeded")
             )            
            .def("revert_to_initial_state", &cm::revert_to_initial_state,
                 (py::arg("self"), py::arg("mid")),
                 doc_intro("revert model to initial state, notice that this \n"
                 "require that the set_current_state_as_initial_state() or run_cells() is called.\n"
                 "The latter will set initial state if not already done.")
                 doc_parameters()
                 doc_parameter("mid", "str", "model identifier")
                 doc_returns("", "bool", "true if succeeded")
             )
            .def("set_state_collection", &cm::set_state_collection,
                 (py::arg("self"), py::arg("mid"), py::arg("catchment_id"), py::arg("on_or_off")),
                 doc_intro("enable state collection for specified or all cells")
                 doc_parameters()
                 doc_parameter("mid", "str", "model identifier")
                 doc_parameter("catchment_id", "int", "to enable state collection for, -1 means turn on/off for all")
                 doc_parameter("on_or_off", "bool", "on_or_off true|or false")
                 doc_returns("", "bool", "true if succeeded")
             )
            
            .def("set_snow_sca_swe_collection", &cm::set_snow_sca_swe_collection,
                 (py::arg("self"), py::arg("mid"), py::arg("catchment_id"), py::arg("on_or_off")),
                 doc_intro("enable/disable collection of snow sca|sca for calibration purposes")
                 doc_parameters()
                 doc_parameter("mid", "str", "model identifier")
                 doc_parameter("catchment_id", "int", "to enable snow calibration for, -1 means turn on/off for all")
                 doc_parameter("on_or_off", "bool", "on_or_off true|or false")
                 doc_returns("", "bool", "true if succeeded")
             )
            
            .def("is_cell_env_ts_ok",&cm::is_cell_env_ts_ok,
                (py::arg("self"), py::arg("mid")),
                doc_intro("check if all values in cell.env_ts for selected calculation-filter is non-nan, i.e. valid numbers")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_returns("", "bool", "true if ok, false if not ok")
            )
            
            .def("is_calculated",&cm::is_calculated,
                (py::arg("self"), py::arg("mid"), py::arg("catchment_id")),
                doc_intro("using the catchment_calculation_filter to decide if discharge etc. are calculated.")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("cid", "int", "catchment id")
                doc_returns("", "bool", "true if catchment id is calculated during runs")
            )
            .def("get_geo_cell_data",&cm::get_geo_cell_data,
                (py::arg("self"), py::arg("mid")),
                doc_intro("returns the geo cell data vector for the specified model.")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_returns("", "GeoCellDataVector", "The GeoCellDataVector for the specified model")
            )

            .def("get_region_env",&cm::get_region_env,
                (py::arg("self"), py::arg("mid")),
                doc_intro("returns the current region-env for the specified model.")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_returns("", "ARegionEnvironment", "The region envirionment for the specified model")
            )
            .def("get_interpolation_parameter",&cm::get_interpolation_parameter,
                (py::arg("self"), py::arg("mid")),
                doc_intro("returns the current interpolation parameters  used to interpolate region-env to the cells.")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_returns("", "InterpolationParameter", "The interpolation parameter for the specified model")
            )
            .def("get_description",&cm::get_description,
                (py::arg("self"), py::arg("mid")),
                doc_intro("returns the user specified description string, typically a json formatted string.")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_returns("", "str", "The  description(json string) for the specified model")
            )
            .def("set_description",&cm::set_description,
                (py::arg("self"), py::arg("mid"),py::arg("description")),
                doc_intro("Set the user specified description string, typically a json formatted string.")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_returns("", "bool", "True if successful")
            )
            .def("fx",&cm::do_fx,
                (py::arg("self"), py::arg("mid"),py::arg("fx_args")),
                doc_intro("Invoke the serverside fx callback, on the exlusively locked model given by mid")
                doc_parameters()
                doc_parameter("mid", "str", "model identifier")
                doc_parameter("fx_args","str","args to the callback, typically a json string")
                doc_returns("", "bool", "the server side evaluation result, as pr. callback return value")
            )
        ;
    }
    
    /** @brief a function to expose a server
     * 
     * 
     * This class simply expose the methods of the above py_server
     * 
     */
    
    void expose_server() {
        
        using srv=py_server;
            py::class_<srv, boost::noncopyable >("DrmServer",
                doc_intro(
                    "The DRMS, distributed region model server provides the server part that implements\n"
                    "the needed functionality to create, run and get results from hydrology region-models\n."
                    "Togehter with the DrmClient, it allow the Shyft hydrology region-model operations to be\n"
                    "scaled out on several servers, giving providing support for system and analysis of any size\n"
                ),
                py::init<>((py::arg("self")),
                        doc_intro("Creates a server object .")
                )
            )
            .def("set_listening_port", &srv::set_listening_port, (py::arg("self"),py::arg("port_no")),
                doc_intro("set the listening port for the service")
                doc_parameters()
                doc_parameter("port_no","int","a valid and available tcp-ip port number to listen on.")
                doc_paramcont("typically it could be 20000 (avoid using official reserved numbers)")
                doc_returns("nothing","None","")
            )
            .def("set_listening_ip", &srv::set_listening_ip, (py::arg("self"),py::arg("ip")),
                doc_intro("set the listening port for the service")
                doc_parameters()
                doc_parameter("ip","str","ip or host-name to start listening on")
                doc_returns("nothing","None","")
            )
            .def("start_server",&srv::start_server,(py::arg("self")),
                doc_intro("start server listening in background, and processing messages")
                doc_see_also("set_listening_port(port_no),is_running")
                doc_returns("port_no","in","the port used for listening operations, either the value as by set_listening_port, or if it was unspecified, a new available port")
            )
            .def("set_max_connections",&srv::set_max_connections,(py::arg("self"),py::arg("max_connect")),
                doc_intro("limits simultaneous connections to the server (it's multithreaded, and uses on thread pr. connect)")
                doc_parameters()
                doc_parameter("max_connect","int","maximum number of connections before denying more connections")
                doc_see_also("get_max_connections()")
            )
            .def("get_max_connections",&srv::get_max_connections, (py::arg("self")),
                doc_intro("returns the maximum number of connections to be served concurrently"))
            .def("stop_server",&srv::stop_server, (py::arg("self")),
                doc_intro("stop serving connections, gracefully.")
                doc_see_also("start_server()")
            )
            .def("is_running",&srv::is_running, (py::arg("self")),
                doc_intro("true if server is listening and running")
                doc_see_also("start_server()")
            )
            .def("get_listening_port",&srv::get_listening_port, (py::arg("self")),
                "returns the port number it's listening at for serving incoming request"
            )
            .add_property("alive_connections",&srv::get_alive_connections,
                doc_intro("returns currently alive connections to the server")
            )
            .def("get_model_ids",&srv::get_model_ids,
                (py::arg("self")),
                doc_intro("returns a list of model ids (mids) that is alive and known in the server")
                doc_returns("", "StringList", "Strongly typed list of strings naming the available models on the drms")
            )
            .def("get_model",raw_function(server_ext::get_model),
                //(py::arg("self"),py::arg("mid")),
                doc_intro(
                    "returns the named model, so it can be inspected/modified\n"
                    "WIP: needs to be sure that there is no activity on the model from the remote side\n"
                )
                doc_returns("", "Model", "Of the type as set by the client")
            )
            .def_readwrite("fx",&srv::py_fx_cb,
                doc_intro("server-side callable function(lambda) that takes two parameters:")
                doc_intro("mid :  the model id")
                doc_intro("fx_arg: arbitrary string to pass to the server-side function")
                doc_intro("The server-side fx is called when the client (or web-api) invoke the c.fx(mid,fx_arg).")
                doc_intro("The signature of the callback function should be fx_cb(mid:str, fx_arg:str)->bool")
                doc_intro("This feature is simply enabling the users to tailor server-side functionality in python!")
                doc_intro("NOTE: Currently we do take exclusive lock on the model id, so that we avoid race conditions")
                doc_intro("Examples:\n")
                doc_intro(
                    ">>> from shyft.hydrology import DrmServer\n"
                    ">>> s=Server()\n"
                    ">>> def my_fx(mid:str, fx_arg:str)->bool:\n"
                    ">>>     print(f'invoked with mid={mid} fx_arg={fx_arg}')\n"
                    ">>>   # note we can use captured Server s here!"
                    ">>>     return True\n"
                    ">>> # and then bind the function to the callback\n"
                    ">>> s.fx=my_fx\n"
                    ">>> s.start_server()\n"
                    ">>> : # later using client from anywhere to invoke the call\n"
                    ">>> fx_result=c.fx('my_model_id', 'my_args')\n\n"
                )
            )

            ;
    }
    void drms() {
        expose_common();
        expose_client();
        expose_server();
    }
}
