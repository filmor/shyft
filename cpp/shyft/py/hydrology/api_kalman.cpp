/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>
#include <shyft/hydrology/api/api.h>
#include <shyft/hydrology/region_model.h>
#include <shyft/time_series/periodic.h>
#include <shyft/hydrology/spatial/kalman.h>

namespace expose {
    namespace py=boost::python;
    namespace sa = shyft::api;
    namespace sc = shyft::core;
    namespace sta = shyft::time_axis;

    typedef std::vector<sc::geo_point> geo_point_vector;
    typedef std::vector<sa::TemperatureSource> geo_temperature_vector;
    typedef std::shared_ptr<geo_temperature_vector> geo_temperature_vector_;
    typedef std::vector<sa::PrecipitationSource> geo_precipitation_vector;
    typedef std::shared_ptr<geo_precipitation_vector> geo_precipitation_vector_;
    typedef shyft::time_series::dd::ats_vector apoint_ts_vector; // this type is already exposed in api, so we can use it directly


    static void kalman_parameter() {
        typedef shyft::core::kalman::parameter KalmanParameter;
        py::class_<KalmanParameter>(
            "KalmanParameter",
            "Defines the parameters that is used to tune the kalman-filter algorithm for temperature type of signals")
            .def(py::init<py::optional<int,double,double,double,double>>((py::arg("n_daily_observations"),py::arg("hourly_correlation"),py::arg("covariance_init"),py::arg("std_error_bias_measurements"),py::arg("ratio_std_w_over_v")), "Constructs KalmanParameter with default or supplied values"))
            .def(py::init<const KalmanParameter&>((py::arg("const_ref")),"clone the supplied KalmanParameter"))
            .def_readwrite("n_daily_observations",&KalmanParameter::n_daily_observations,"int:  default = 8 each 24hour, every 3 hour")
            .def_readwrite("hourly_correlation",&KalmanParameter::hourly_correlation,"float: default=0.93, correlation from one-hour to the next")
            .def_readwrite("covariance_init",&KalmanParameter::covariance_init,"float:  default=0.5,  for the error covariance P matrix start-values")
            .def_readwrite("std_error_bias_measurements",&KalmanParameter::std_error_bias_measurements,"float: default=2.0, st.dev for the bias measurements")
            .def_readwrite("ratio_std_w_over_v",&KalmanParameter::ratio_std_w_over_v,"float: default=0.06, st.dev W /st.dev V ratio")
        ;
    }
    static std::vector<double> kalman_x(const shyft::core::kalman::state &s) {return arma::conv_to<std::vector<double>>::from(s.x);}
    static std::vector<double> kalman_k(const shyft::core::kalman::state &s) { return arma::conv_to<std::vector<double>>::from(s.k); }
    /** flattens supplied matrix into a vector row by col */
    static std::vector<double> arma_flatten(const arma::mat&m) {
        std::vector<double> r;
        for (arma::uword i = 0;i < m.n_rows;++i) {
            auto row = arma::conv_to<std::vector<double>>::from(m.row(i));
            for (auto v : row) r.push_back(v);
        }
        return r;// return flatten vector rows repeated n_cols
    }
    static std::vector<double> kalman_P(const shyft::core::kalman::state &s) { return arma_flatten(s.P);}
    static std::vector<double> kalman_W(const shyft::core::kalman::state &s) { return arma_flatten(s.W);}

    static void kalman_state() {
        typedef shyft::core::kalman::state KalmanState;
        py::class_<KalmanState>("KalmanState",
            "keeps the state of the specialized kalman-filter:\n"
            "    * x : vector[n=n_daily_observations] best estimate\n"
            "    * k : vector[n], gain factors\n"
            "    * P : matrix[nxn], covariance\n"
            "    * W : noise[nxn]\n"
            "\n"
            )
            .def(py::init<int, double, double, double>((py::arg("n_daily_observations"),py::arg("covariance_init"),py::arg("hourly_correlation"),py::arg("process_noise_init")), "create a state based on supplied parameters"))
            .def(py::init<const KalmanState&>((py::arg("clone")), "clone the supplied state"))
            .def(py::init<>("construct a default state"))
            .def("size", &KalmanState::size, "returns the size of the state, corresponding to n_daily_observations")
            .def("get_x", kalman_x, (py::arg("state")),"returns a copy of current bias estimate x").staticmethod("get_x")
            .def("get_k", kalman_k, (py::arg("state")), "returns a copy of current kalman gain k").staticmethod("get_k")
            .def("get_P", kalman_P, (py::arg("state")), "returns a copy of current kalman covariance matrix P").staticmethod("get_P")
            .def("get_W", kalman_W, (py::arg("state")), "returns a copy of current kalman noise matrix W").staticmethod("get_W")
        ;
    }
    static void kalman_filter() {
        typedef shyft::core::kalman::filter KalmanFilter;
        py::class_<KalmanFilter>(
            "KalmanFilter",
            doc_intro(
                "Specialized kalman filter for temperature (e.g.:solar-driven bias patterns)\n"
                "The observation point(t, v) is folded on to corresponding period of the\n"
                "day(number of periods in a day is parameterized, typically 8).\n"
                "A simplified kalman filter algorithm using the forecast bias as\n"
                "the state - variable.\n"
                "Observed bias(fc - obs) is feed into the filter and establishes the\n"
                "kalman best predicted estimates(x) for the bias.\n"
                "This bias can then be used as a correction to forecast in the future\n"
                "to compensate for systematic forecast errors.\n"
                "Credits: Thanks to met.no for providing the original source for this algorithm.\n"
            )
            doc_see_also("https://en.wikipedia.org/wiki/Kalman_filter")
            )
            .def(py::init<>("Construct a filter with default KalmanParameter"))
            .def(py::init<shyft::core::kalman::parameter>((py::arg("p")), "Construct a filter with the supplied parameter"))
            .def("create_initial_state",&KalmanFilter::create_initial_state,"returns initial state, suitable for starting, using the filter parameters")
            .def("update",&KalmanFilter::update,(py::arg("observed_bias"),py::arg("t"),py::arg("state")),
                doc_intro("update the with the observed_bias for a specific period starting at utctime t")
                doc_parameters()
                doc_parameter("observed_bias","float","nan if no observation is available otherwise obs - fc")
                doc_parameter("t","time","utctime of the start of the observation period, this filter utilizes daily solar patterns, so time in day - cycle is the only important aspect.")
                doc_parameter("state","KalmanState","contains the kalman state, that is updated by the function upon return")
                )
            .def_readwrite("parameter",&KalmanFilter::p,"The KalmanParameter used by this filter")
            ;

    }
    ///thin wrappers to fwd the call from py to c++
    static void update_with_forecast_geo_ts_and_obs(
        shyft::core::kalman::bias_predictor& bp,
        geo_temperature_vector_ fc,
        const shyft::time_series::dd::apoint_ts& obs,
        const shyft::time_axis::generic_dt &ta) {
        std::vector<shyft::time_series::dd::apoint_ts> fc_ts_set;
        for (auto& geo_ts : *fc)
            fc_ts_set.push_back(geo_ts.ts);
        bp.update_with_forecast(fc_ts_set, obs, ta);
    }
    static void update_with_forecast_ts_and_obs(
            shyft::core::kalman::bias_predictor& bp,
        const apoint_ts_vector& fc_ts_set,
        const shyft::time_series::dd::apoint_ts& obs,
        const shyft::time_axis::generic_dt &ta) {
        bp.update_with_forecast(fc_ts_set, obs, ta);
    }
    static shyft::time_series::dd::apoint_ts compute_running_bias(
        shyft::core::kalman::bias_predictor& bp,
        const shyft::time_series::dd::apoint_ts& fc_ts,
        const shyft::time_series::dd::apoint_ts& obs,
        const shyft::time_axis::generic_dt &ta) {
        if (ta.gt() != ta.FIXED)
            throw std::runtime_error("The supplied time-axis must be of type FIXED for the compute_running_bias function");
        return bp.compute_running_bias<shyft::time_series::dd::apoint_ts>(fc_ts, obs, ta.f());
    }

    static void kalman_bias_predictor() {
        typedef shyft::core::kalman::bias_predictor KalmanBiasPredictor;

        py::class_<KalmanBiasPredictor>(
            "KalmanBiasPredictor",
            "A bias predictor using a daily pattern KalmanFilter for temperature (etc.)\n"
            "(tbd)"
            )
            .def(py::init<>("Constructs a bias predictor with default filter, parameters and state"))
            .def(py::init<const shyft::core::kalman::filter&>((py::arg("filter")),"create a bias predictor with specified filter"))
            .def(py::init<const shyft::core::kalman::filter&,const shyft::core::kalman::state&>((py::arg("filter"),py::arg("state")), "create a bias predictor with specified filter and initial state"))
            .def("update_with_geo_forecast", update_with_forecast_geo_ts_and_obs,(py::arg("bias_predictor"),py::arg("temperature_sources"),py::arg("observation_ts"),py::arg("time_axis")),
                   doc_intro("update the bias-predictor with forecasts and observation\n"
                   "After the update, the state is updated with new kalman estimates for the bias, .state.x\n")
                doc_parameters()
                doc_parameter("bias_predictor","KalmanBiasPredictor","The bias predictor object it self")
                doc_parameter("temperature_sources","TemperatureSourceVector","ta set of forecasts, in the order oldest to the newest."
                "Note that the geo part of source is not used in this context, only the ts"
                "with periods covering parts of the observation_ts and time_axis supplied")
                doc_parameter("observation_ts","Timeseries","the observation time-series")
                doc_parameter("time_axis" , "TimeAxis","covering the period/timesteps to be updated,"
                "e.g. yesterday, 3h resolution steps, according to the points in the filter")
            ).staticmethod("update_with_geo_forecast")
            .def("update_with_forecast_vector",update_with_forecast_ts_and_obs,(py::arg("bias_predictor"),py::arg("temperature_sources"),py::arg("observation_ts"),py::arg("time_axis")),
                   doc_intro("update the bias-predictor with forecasts and observation\n"
                   "After the update, the state is updated with new kalman estimates for the bias, .state.x\n")
                doc_parameters()
                doc_parameter("bias_predictor","KalmanBiasPredictor","The bias predictor object it self")
                doc_parameter("temperature_sources","TsVector","a set of forecasts, in the order oldest to the newest."
                "With the periods covering parts of the observation_ts and time_axis supplied")
                doc_parameter("observation_ts","TimeSeries","the observation time-series")
                doc_parameter("time_axis" ,"TimeAxis",    "covering the period/timesteps to be updated, "
                "e.g. yesterday, 3h resolution steps, according to the points in the filter")
            ).staticmethod("update_with_forecast_vector")
            .def("compute_running_bias_ts", compute_running_bias, (py::arg("bias_predictor"),py::arg("forecast_ts"),py::arg("observation_ts"),py::arg("time_axis")),
                doc_intro("compute the running bias timeseries,\n"
                "using one 'merged' - forecasts and one observation time - series.\n"
                "\n"
                "Before each day - period, the bias - values are copied out to form\n"
                "a continuous bias prediction time-series.\n")
                doc_parameters()
                doc_parameter("bias_predictor","KalmanBiasPredictor","The bias predictor object it self")
                doc_parameter("forecast_ts","TimeSeries","a merged forecast ts, with period covering the observation_ts and time_axis supplied")
                doc_parameter("observation_ts","TimeSeries","the observation time-series")
                doc_parameter("time_axis","TimeAxis","covering the period/timesteps to be updated, e.g. yesterday, 3h resolution steps, according to the points in the filter")
                doc_returns("bias_ts","TimeSeries", "With ts( time_axis,bias_vector,POINT_AVERAGE),  computed running bias-ts")
            ).staticmethod("compute_running_bias_ts")

            .def_readonly("filter",&KalmanBiasPredictor::f,"KalmanFilter:  kalman filter with parameters")
            .def_readwrite("state",&KalmanBiasPredictor::s,"KalmanState: state of the predictor")
            ;
    }
    void kalman() {
        kalman_parameter();
        kalman_state();
        kalman_filter();
        kalman_bias_predictor();
    }
}
