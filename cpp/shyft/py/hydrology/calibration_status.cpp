#include <shyft/py/api/boostpython_pch.h>
#include <shyft/hydrology/srv/msg_types.h>

namespace expose {
    using std::string;
    using std::vector;
    using std::shared_ptr;
    using shyft::hydrology::srv::calibration_status;
    using shyft::hydrology::srv::parameter_variant_t;
    namespace py=boost::python;
    using cs=calibration_status;
    namespace {
        struct cs_ext {
            static cs* x_self(const py::tuple& args) {
                py::object self = args[0];
                py::extract<cs*> xtract_self(self);
                return xtract_self();
            }

            static py::object mk_param(parameter_variant_t const&pv) {
                return boost::apply_visitor(
                    [](auto const&x) -> py::object {
                        return x?py::object(*x) : py::object();}
                    ,pv
                );
            }

            static py::object trace_parameter(py::tuple args, py::dict /*kwargs*/) {
                if (py::len(args) < 2)
                    throw std::runtime_error("require args: self, i");
                auto self=x_self(args);
                auto ci=py::extract<int>(args[1])();
                if( !(ci>=0 && size_t(ci)<self->p_trace.size()))
                    throw std::runtime_error("trace_parameter:index out of range:"+std::to_string(ci));
                return mk_param(self->p_trace[ci]);
            }
            
            static py::object result_parameter(py::tuple args, py::dict /*kwargs*/) {
                if (py::len(args) < 1)
                    throw std::runtime_error("require args: self");
                auto self=x_self(args);
                return mk_param(self->p_result);
            }

        };
    }
    
    void api_calibration_status(){
        py::class_<calibration_status>("CalibrationStatus",
            doc_intro(
                "The CalibrationStatus is returned from drms, or local call to the region model calibration object."
                "It provides status, progress, and current trace of parameters and goal function values"
            ),
            py::no_init
            )
            .def_readonly("running", &cs::running, "True if the calibration is still running")
            .def_readonly("trace_goal_function_values",&cs::f_trace,"goal function values obtained so far")
            .add_property("trace_size",+[](cs*o)->int {return o->f_trace.size();},
                doc_intro("returns the size of the parameter-trace")
                doc_see_also("trace_goal_function_value,trace_parameter")
            )
            .def("trace_goal_function_value",+[](cs*o,int i)->double{return i>=0&&size_t(i)<o->f_trace.size()?o->f_trace[i]:shyft::nan;},(py::arg("self"),py::arg("i")),
                doc_intro("returns the i'th goal function value")
            )
            .def("trace_parameter",raw_function(cs_ext::trace_parameter),
                 //(py::arg("self"),py::arg("i")),                     
                doc_intro("returns the i'th parameter tried, corresponding to the ")
                doc_intro("i'th trace_goal_function value")
            )
            .def("result",raw_function(cs_ext::result_parameter),
                 //(py::arg("self"))                     
                doc_intro("returns resulting calibrated parameter if available")
            )
        ;
    }
}
