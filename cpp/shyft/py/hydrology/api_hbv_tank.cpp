/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/py/api/boostpython_pch.h>

#include <shyft/hydrology/methods/hbv_tank.h>

namespace expose {
    void hbv_tank() {
        using namespace shyft::core::hbv_tank;
        namespace py=boost::python;

        py::class_<parameter>("HbvTankParameter")
            .def(py::init<py::optional<double, double, double, double, double, double, double, double, double>>((
                   py::arg("uz1"),py::arg("uz2"),
                   py::arg("kuz0"),py::arg("kuz1"),py::arg("kuz2"),
                   py::arg("perc"),py::arg("klz"),py::arg("ce"),py::arg("cevpl")), "create parameter object with specifed values"))
            .def_readwrite("uz1", &parameter::uz1, "[ mm ] Mid-threshold for upper ground water zone. default=10")
            .def_readwrite("uz2", &parameter::uz2, "[ mm ] High-threshold for upper ground water zone. default=50")
            .def_readwrite("kuz0", &parameter::kuz0, "[ 1/step ] Slow response coefficient upper ground water zone. default=0.05")
            .def_readwrite("kuz1", &parameter::kuz1, "[ 1/step ] Mid response coefficient upper ground water zone. default=0.1")
            .def_readwrite("kuz2", &parameter::kuz2, "[ 1/step ] Fast response coefficient upper ground water zone. default=0 (not active)")
            .def_readwrite("perc", &parameter::perc, "[ mm/step ] Perculation to lower ground water zone. default=0.6")
            .def_readwrite("klz", &parameter::klz, "[ 1/step ] Slow response coefficient lower ground water zone. default=0.05")
            .def_readwrite("ce", &parameter::ce, "[ mm/deg/day ] Evapotranspiration constant (degree-day-factor). default=0.17/24")
            .def_readwrite("cevpl", &parameter::cevpl, "[ unitless ], adjustment factor for potential evapotranspiration on lakes. default=1.1")
            ;

        py::class_<state>("HbvTankState")
            .def(py::init<py::optional<double,double>>((py::arg("uz"),py::arg("lz")), "create a state with specified values"))
            .def_readwrite("uz", &state::uz, "Upper ground water zone content [mm]")
            .def_readwrite("lz", &state::lz, "Lower ground water zone content [mm]")
            ;

        py::class_<response>("HbvTankResponse")
            .def(py::init<py::optional<double, double, double, double, double, double>>((
                   py::arg("quz0"),py::arg("quz1"),py::arg("quz2"),
                   py::arg("qlz"),py::arg("elake"),py::arg("perculation")), "create response object with specifed values"))
            .def_readonly("quz", &response::quz, "[mm/step] Total for upper zone")
            .def_readwrite("quz0", &response::quz0, "[mm/step] Upper zone slow response")
            .def_readwrite("quz1", &response::quz1, "[mm/step] Upper zone mid response")
            .def_readwrite("quz2", &response::quz2, "[mm/step] Upper zone fast response")
            .def_readwrite("qlz", &response::qlz, "[mm/step] Total for lower zone")
            .def_readwrite("elake", &response::elake, "[ mm/step ] Evaporation from lake")
            .def_readwrite("perculation", &response::perc_effective, "[ mm/step ] Effective perculation from upper to lower")
            .def_readonly("q", &response::q, "[mm/step] Total discharge from tank")
            ;

        typedef  calculator HbvTankCalculator;
        py::class_<HbvTankCalculator>("HbvTankCalculator",
                    doc_intro("Computing water through the HBV ground water zone of the HBV model.")
                    doc_intro("\n")
                    doc_intro("Reference:\n")
                    doc_intro(" * Nils Roar Sæhlthun: The Nordic HBV model 1996 https://publikasjoner.nve.no/publication/1996/publication1996_07.pdf\n")
                    doc_intro("\n")
                    doc_notes()
                    doc_note("Lake and glacier are treated in the stack"),
                    py::no_init
                )
            .def(py::init<const parameter&, double>((py::arg("parameter"),py::arg("lake_fraction")), "creates a calculator with given parameters"))
            .def("step", &HbvTankCalculator::step, (py::arg("self"),py::arg("state"),py::arg("from_soil"),py::arg("precip"),py::arg("t2m")),
                doc_intro("One step of the model, given state, parameters and input.\n")
                doc_intro("Updates the state and response.\n")
                doc_parameters()
                doc_parameter("state","HbvSoilState"," param of type S, in/out, ref template parameters")
                doc_parameter("response","HbvSoilResponse"," param of type R, in/out, ref template parameters")
                doc_parameter("from_soil","float","[mm/step] Inflow to upper zone (from soil moisture)")
                doc_parameter("precip","float","[mm/step] Routed precipitation. Assumed that it is corrected for lake fraction already.")
                doc_parameter("t2m","float","[deg C] Air-temperature at 2m (proxy for water temperature)")
                doc_parameter("lake_fraction","float","[unitless] Lake-fraction of the cell. Not directly used/desribed in ref.'Nils Roar', but introduced here to adopt to the stack.")
            )
            ;
    }
}
