/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>

#include  <shyft/hydrology/methods/hbv_actual_evapotranspiration.h>

namespace expose {
    using namespace shyft::core::hbv_actual_evapotranspiration;
    namespace py=boost::python;

    void hbv_actual_evapotranspiration() {
        py::class_<parameter>("HbvActualEvapotranspirationParameter")
            .def(py::init<py::optional<double>>((py::arg("lp")), "a new object with specified parameters"))
            .def_readwrite("lp", &parameter::lp, "typical value 150")
            ;
        py::class_<response>("HbvActualEvapotranspirationResponse")
            .def_readwrite("ae", &response::ae)
            ;
        py::def("HbvActualEvapotranspirationCalculate_step", calculate_step,
            (py::arg("soil_moisture"),py::arg("potential_evapotranspiration"),py::arg("lp"),py::arg("snow_fraction"),py::arg("dt")),
            doc_intro("actual_evapotranspiration calculates actual evapotranspiration, returning same unit as input pot.evap")
            doc_intro("based on supplied parameters, formula like (1.0 - snow_fraction)*(soil_moisture < lp ? pot_evapo*(soil_moisture / lp):pot_evapo)")
            doc_parameters()
            doc_parameter("soil_moisture","","[mm] water level eqvivalent in ground, ae goes to zero if ground is drying out")
            doc_parameter("potential_evapotranspiration","","[mm/x], x time-unit")
            doc_parameter("lp","","[mm] soil_moisture threshold, lp typically 150")
            doc_parameter("snow_fraction",""," 0..1 there is to ae over snow surface, so snow_fraction 1.0, yields 0.0")
            doc_parameter("dt","","[s] timestep length, currently not part of the formula")
            doc_returns("actual evapotranspiration","","")
        );
    }
}
