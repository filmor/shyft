#include <shyft/py/api/boostpython_pch.h>
#include <fmt/core.h>
#include <boost/python/import.hpp>
#include <shyft/hydrology/srv/msg_types.h>
#include <shyft/py/scoped_gil.h>

namespace expose {
    using std::shared_ptr;
    using std::string;
    using std::vector;
    using co=shyft::hydrology::srv::calibration_options;
    using om=shyft::hydrology::srv::optimizer_method;
    using shyft::core::utctime;
    
    namespace py=boost::python;
    
    struct optimizer_model {

        vector<double> p_min;
        vector<double> p_max;
        py::object fx;///< callback for notify

        optimizer_model(vector<double> const& p_min, vector<double> const& p_max, py::object fx)
        :p_min {p_min}, p_max {p_max}, fx {fx} {
            if (p_min.size() <= 1 || p_min.size() != p_max.size()) {
                throw std::runtime_error("The dimension of lower and upper parameter bounds must be equal and greater than 1.");
            }
            rig_fx();
        }
        
        void rig_fx() {
            if (!PyEval_ThreadsInitialized()) {
                PyEval_InitThreads();// ensure threads-is enabled
            }
        }

        vector<double> to_vector(const dlib::matrix<double, 0, 1> &params) const {
            vector<double> x;
            x.reserve(params.nr());
            for (auto i = 0; i < params.nr(); ++i) {
                x.emplace_back(params(i));
            }
            return x;
        }

        double operator()(dlib::matrix<double, 0, 1> const &params) {
            auto x = to_vector(params);
            return py_goal_function(from_scaled(x));
        }

        double operator()(vector<double> const &x) {
            return py_goal_function(from_scaled(x));
        }

        void handle_pyerror() {
            // from SO: https://stackoverflow.com/questions/1418015/how-to-get-python-exception-text
            using namespace boost::python;
            using namespace boost;
            std::string msg{"unspecified error"};
            if(PyErr_Occurred()) {
                PyObject *exc,*val,*tb;
                object formatted_list, formatted;
                PyErr_Fetch(&exc,&val,&tb);
                handle<> hexc(exc),hval(allow_null(val)),htb(allow_null(tb));
                object traceback(import("traceback"));
                if (!tb) {
                    object format_exception_only{ traceback.attr("format_exception_only") };
                    formatted_list = format_exception_only(hexc,hval);
                } else {
                    object format_exception{traceback.attr("format_exception")};
                    if (format_exception) {
                        try {
                            formatted_list = format_exception(hexc, hval, htb);
                        } catch (...) { // any error here, and we bail out, no crash please
                            msg = "not able to extract exception info";
                        }
                    } else
                        msg="not able to extract exception info";
                }
                if (formatted_list) {
                    formatted = py::str("\n").join(formatted_list);
                    msg = extract<std::string>(formatted);
                }
            }
            handle_exception();
            PyErr_Clear();
            throw std::runtime_error(msg);
        }

        double py_goal_function(vector<double> const& x) {
            double r = shyft::nan;
            if(fx.ptr() != Py_None) {
                shyft::py::scoped_gil_aquire gil;
                try {
                    r = boost::python::call<double,vector<double>>(fx.ptr(),x);
                } catch  (const boost::python::error_already_set&) {
                    handle_pyerror();
                }
            } else {
                // Consider old fashioned printouts.. 
                // but leave that to py callback instead.
            }
            return r;
        }

        double goal_function(vector<double> const& x) {
            shyft::py::scoped_gil_release gil;
            return py_goal_function(x);
        }

        vector<double> to_scaled(vector<double>& p) {
            vector<double> p_s;
            p_s.reserve(p.size());
            for (size_t i = 0; i < p.size(); ++i)
                p_s.emplace_back((p[i] - p_min[i])/(p_max[i] - p_min[i]));
            return p_s;
        }
        
        vector<double> from_scaled(const dlib::matrix<double, 0, 1> scaled_params) {
            return from_scaled(to_vector(scaled_params));
        }

        vector<double> from_scaled(const vector<double>& p_s) const {
            std::vector<double> p;
            p.reserve(p_s.size());
            for (size_t i = 0; i < p_s.size(); ++i)
                p.emplace_back((p_max[i] - p_min[i])*p_s[i] + p_min[i]);
            return p;
        }

        vector<double> min_bobyqa(vector<double>& x, int max_n_evaluations, double tr_start, double tr_stop) {
            shyft::py::scoped_gil_release gil;
            shyft::core::model_calibration::min_bobyqa(*this, x, max_n_evaluations, tr_start, tr_stop);
            return x;
        }
        
        vector<double> min_global(vector<double>& x, int max_n_evaluations, double max_seconds, double solver_epsilon) {
            shyft::py::scoped_gil_release gil;
            shyft::core::model_calibration::min_global(*this, x, max_n_evaluations, max_seconds, solver_epsilon);
            return x;
        }
        
        vector<double> min_dream(vector<double>& x, int max_n_evaluations) {
            shyft::py::scoped_gil_release gil;
            shyft::core::model_calibration::min_dream(*this, x, max_n_evaluations);
            return x;
        }

        vector<double> min_sceua(vector<double>& x, size_t max_n_evaluations, double x_eps = 0.0001, double y_eps = 0.0001) {
            shyft::py::scoped_gil_release gil;
            shyft::core::model_calibration::min_sceua(*this, x, max_n_evaluations, x_eps, y_eps);
            return x;
        }

        vector<double> optimize(vector<double> x, co cal_opt) {
            if (x.size() <= 1) {
                throw std::runtime_error("The dimension of x must be greater than 1.");
            }
            if (cal_opt.method == om::BOBYQA) {
                return min_bobyqa(x, cal_opt.max_n_iterations, cal_opt.tr_start, cal_opt.tr_stop);
            } else if (cal_opt.method == om::GLOBAL) {
                return min_global(x, cal_opt.max_n_iterations, shyft::core::to_seconds(cal_opt.time_limit), cal_opt.solver_epsilon);
            } else if (cal_opt.method == om::DREAM) {
                return min_dream(x, cal_opt.max_n_iterations);
            } else if (cal_opt.method == om::SCEUA) {
                return min_sceua(x, cal_opt.max_n_iterations, cal_opt.x_epsilon, cal_opt.y_epsilon);
            } else {
                throw std::invalid_argument("received invalid optimizer method");
            }
        }

        string to_str() const {
            return "ParameterOptimizer(p_min, p_max)";
        }
     };

    void api_parameter_optimizer(){
        auto pyco=py::class_<optimizer_model>(
            "ParameterOptimizer", 
            doc_intro("The ParameterOptimizer allows for testing and comparing optimizer methods for the same inital parameters and goal function."), 
            py::no_init
        )
        .def(py::init<vector<double>const&, vector<double>const&, py::object>((py::arg("p_min"), py::arg("p_max"), py::arg("fx")), 
            doc_intro("Initialises a ParameterOptimizer instance.")
            doc_parameter("p_min","Union[DoubleVector[float], DoubleVector[int]]", "the lower parameter bound in at least two-dimensional space.")
            doc_parameter("p_max","Union[DoubleVector[float], DoubleVector[int]]", "the upper parameter bound in at least two-dimensional space.")
            doc_parameter("fx","Callable", "the goal function to be used in the parameter optimization problem.")
            )
        )
        .def_readonly("p_min", &optimizer_model::p_min, "lower parameter bound in at least two-dimensional space")
        .def_readonly("p_max", &optimizer_model::p_max, "upper parameter bound in at least two-dimensional space")
        .def_readwrite("fx", &optimizer_model::fx, "goal function for which parameters are optimized")

        .def("goal_function", &optimizer_model::goal_function, (py::arg("self"), py::arg("x")), 
            doc_intro("The goal function to be used in the optimization problem.")
            doc_parameter("x","Union[DoubleVector[float], DoubleVector[int]]", "the point in at least two-dimensional space at which the goal function is evaluated.")
            doc_returns("y","DoubleVector[float]","The function value f(x) at point x.")
        )
        .def("optimize", &optimizer_model::optimize, (py::arg("self"), py::arg("x"), py::arg("calibration_option")), 
            doc_intro("Run the optimizer for given parameters and configuration.")
            doc_parameter("x","Union[DoubleVector[float], DoubleVector[int]]", "the initial point in at least two-dimensional space for the optimization problem.")
            doc_parameter("calibration_option","CalibrationOption","the calibration for the various optimization methods.")
            doc_returns("x_opt","DoubleVector[float]","The optimal parameters for which the optimum of f(x) is found.")
            doc_see_also("CalibrationOption")
        )
        .def("__str__", &optimizer_model::to_str, (py::arg("self")))
        .def("__repr__", &optimizer_model::to_str, (py::arg("self")))
        ;
    }

    void api_calibration_options(){
        auto pyco=py::class_<co>("CalibrationOption",
            doc_intro(
            "The CalibrationOption controls how the calibration optmizer is run regarding method and termination criterias.\n"
            "Notice that the different optmizers do have variations in the termination criterias.\n"
            // add more description here..  
            )
        )
        .def(py::init<om,py::optional<int,utctime,double,double,double,double,double>>(
                (py::arg("method"),py::arg("max_iterations"),py::arg("time_limit"),py::arg("solver_epsilon"),py::arg("x_epsilon"),py::arg("y_epsilon"),py::arg("tr_start"),py::arg("tr_stop")),
                doc_intro("Initialises a ParameterOptimizer instance.")
                doc_parameter("method","OptimizerMethod","select which parameter optimizer to use.")
                doc_parameter("max_iterations","int","stop after this number of iterations (default 1500).")
                doc_parameter("time_limit","float","GLOBAL: don't run the global search longer than this (default 0, off).")
                doc_parameter("solver_epsilon","float","GLOBAL: select which parameter optimizer to use (default 0.001).")
                doc_parameter("x_epsilon","float","SCEUA: stop when normalized parameters x is within this range in variation.")
                doc_parameter("y_epsilon","float","SCEUA: stop when goal function is stable within this range (not improving any further).")
                doc_parameter("tr_start","float","BOBYQA: trust region start (default 0.1).")
                doc_parameter("tr_stop","float","BOBYQA: trust region stop/end (default 1e-5).")
            )
        )
        .def_readwrite("method", &co::method, "select which parameter optimizer to use")
        .def_readwrite("max_iterations", &co::max_n_iterations, "stop after this number of iterations (default 1500)")
        .def_readwrite("time_limit", &co::time_limit, "GLOBAL: don't run the global search longer than this (default 0, off)")
        .def_readwrite("solver_epsilon", &co::solver_epsilon, "GLOBAL: select which parameter optimizer to use (default 0.001)")
        .def_readwrite("x_epsilon", &co::x_epsilon, "SCEUA: stop when normalized parameters x is within this range in variation")
        .def_readwrite("y_epsilon", &co::y_epsilon, "SCEUA: stop when goal function is stable within this range (not improving any further)")
        .def_readwrite("tr_start", &co::tr_start, "BOBYQA: trust region start (default 0.1)")
        .def_readwrite("tr_stop", &co::tr_stop, "BOBYQA: trust region stop/end (default 1e-5)")

        .def("bobyqa",
            +[](int max_iterations,double tr_start,double tr_stop){
                co r{om::BOBYQA,size_t(max_iterations)};
                r.tr_start=tr_start;r.tr_stop=tr_stop;
                return r;
        },
            (py::arg("max_iterations"),py::arg("tr_start"),py::arg("tr_stop")),
            doc_intro("Construct bobyqa options.")
            doc_parameter("max_iterations","int","stop after this number of iterations (default 1500).")
            doc_parameter("tr_start","float","BOBYQA: trust region start (default 0.1).")
            doc_parameter("tr_stop","float","trust region stop/end (default 1e-5).")
            doc_returns("co","CalibrationOption","An instance of CalibrationOption for the relevant optmization algorithm.")
        ).staticmethod("bobyqa")

        .def("global_search",
            +[](int max_iterations,utctime time_limit, double solver_epsilon ){
                co r{om::GLOBAL,size_t(max_iterations),time_limit,solver_epsilon};
                return r;
            },
            (py::arg("max_iterations"),py::arg("time_limit"),py::arg("solver_epsilon")),
            doc_intro("Construct global search options")
            doc_parameter("max_iterations","int","stop after this number of iterations (default 1500).")
            doc_parameter("time_limit","float","don't run the global search longer than this (default 0, off).")
            doc_parameter("solver_epsilon","float","select which parameter optimizer to use (default 0.001).")
            doc_returns("co","CalibrationOption","An instance of CalibrationOption for the relevant optmization algorithm.")
        ).staticmethod("global_search")

        .def("sceua",
            +[](int max_iterations,double x_epsilon, double y_epsilon ){
                co r{om::SCEUA,size_t(max_iterations)};
                r.x_epsilon=x_epsilon;
                r.y_epsilon=y_epsilon;
                return r;
        },
            (py::arg("max_iterations"),py::arg("x_epsilon"),py::arg("y_epsilon")),
            doc_intro("Construct sceua options")
            doc_parameter("max_iterations","int","stop after this number of iterations (default 1500).")
            doc_parameter("x_epsilon","float","stop when normalized parameters x is within this range in variation.")
            doc_parameter("y_epsilon","float","stop when goal function is stable within this range (not improving any further).")
            doc_returns("co","CalibrationOption","An instance of CalibrationOption for the relevant optmization algorithm.")            
        ).staticmethod("sceua")

        .def("dream",
            +[](int max_iterations ){
                co r{om::DREAM,size_t(max_iterations)};
                return r;
        },
            (py::arg("max_iterations")),
            doc_intro("Construct dream options")  
            doc_parameter("max_iterations","int","stop after this number of iterations (default 1500).")
            doc_returns("co","CalibrationOption","An instance of CalibrationOption for the relevant optmization algorithm.")             
        ).staticmethod("dream")
        ;
        //{
        //    py::scope _x=pyco;
            py::enum_<om>("OptimizerMethod",
                doc_intro("Shyft calibration allow the user to select between one of these parameter optimizer methods.")
            )
            .value("BOBYQA", om::BOBYQA)
            .value("GLOBAL", om::GLOBAL)
            .value("DREAM", om::DREAM)
            .value("SCEUA", om::SCEUA)
            .export_values()
            ;
        //}
    }
}
