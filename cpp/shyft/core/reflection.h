#pragma once

#include <concepts>
#include <type_traits>

#include <boost/describe/bases.hpp>
#include <boost/describe/class.hpp>
#include <boost/describe/enum.hpp>
#include <boost/describe/members.hpp>
#include <boost/describe/enumerators.hpp>
#include <boost/hof/detail/forward.hpp>
#include <boost/mp11/algorithm.hpp>
#include <boost/hana/detail/struct_macros.hpp>
#include <boost/hana/find_if.hpp>
#include <boost/hana/pair.hpp>
#include <boost/preprocessor/punctuation/remove_parens.hpp>

#include <shyft/core/utility.h>

namespace shyft {

#define SHYFT_FWD(x) static_cast<decltype(x) &&>(x)

#define SHYFT_DEFINE_ENUM(name, underlying_type, enumerators) \
  BOOST_DEFINE_FIXED_ENUM_CLASS(name, underlying_type, BOOST_PP_REMOVE_PARENS(enumerators))

#define SHYFT_DEFINE_STRUCT(name, bases, members) BOOST_DESCRIBE_CLASS(name, bases, members, (), ())

  template <typename T>
  concept reflected_enum = boost::describe::has_describe_enumerators<T>::value;

  template <reflected_enum T>
  inline constexpr auto enumerator_count = boost::mp11::mp_size<boost::describe::describe_enumerators<T>>::value;

  template <typename T>
  concept reflected_struct = boost::describe::has_describe_bases<T>::value
                          && boost::describe::has_describe_members<T>::value && !std::is_union_v<T>;

  template <typename T>
  requires(std::is_enum_v<T>)
  constexpr auto etoi(T t) {
    return static_cast<std::underlying_type_t<T>>(t);
  }

  template <reflected_enum T>
  constexpr auto with_enum(T t, auto &&f) {
    return boost::mp11::mp_with_index<enumerator_count<T>>(etoi(t), [g = SHYFT_FWD(f)](auto t) {
      return std::invoke(g, std::integral_constant<T, T{t()}>{});
    });
  }

  template <reflected_enum T>
  constexpr auto with_enum_or(T t, auto &&f, auto &&g) {
    static_assert(std::unsigned_integral<std::underlying_type_t<T>>);
    constexpr auto N = enumerator_count<T>;
    if (etoi(t) > N)
      return std::invoke(g);
    return with_enum(t, SHYFT_FWD(f));
  }

  template <auto member_ptr>
  inline constexpr auto hana_member_name = [] {
    return boost::hana::first(
      boost::hana::find_if(boost::hana::accessors<object_type_t<member_ptr>>(), [](auto accessor) {
        return std::is_same<
          std::remove_cvref_t<decltype(boost::hana::second(accessor))>,
          boost::hana::struct_detail::member_ptr<decltype(member_ptr), member_ptr>>{};
      }).value());
  }();

}
