/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <shyft/time/utctime_utilities.h>

#ifndef _WIN32
    #if __GNUC__ > 7
        //#define SHYFT_USE_STD_FS 1
        // due to a feature in boost::filesystem 1.70 we must switch, sooner or later
        // and from g++ 8 onwards we have standard file-system support.
    #endif
#endif

#ifdef SHYFT_USE_STD_FS

    #include <filesystem>
    namespace fs = std::filesystem;
    inline shyft::core::utctime fs_to_utctime(fs::file_time_type ft) {
            return std::chrono::duration_cast<shyft::core::utctime>(ft.time_since_epoch());
    }
    
#else

    #include <boost/filesystem.hpp>
    namespace fs = boost::filesystem;// std::experimental::filesystem; // when we resolve lexial_relative path missing in ms.        

    inline shyft::core::utctime fs_to_utctime(time_t ft) {
            return std::chrono::duration_cast<shyft::core::utctime>(shyft::core::seconds(ft));
    }

#endif


