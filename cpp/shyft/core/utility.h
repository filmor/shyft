#pragma once

#include <concepts>
#include <memory>
#include <type_traits>
#include <vector>

namespace shyft {

  template <bool P, typename T>
  using add_const_if_t = std::conditional_t<P, std::add_const_t<T>, T>;

  template <typename T, typename U>
  using const_like_t = add_const_if_t<std::is_const_v<U>, std::remove_reference_t<T>>;

  template <typename T, typename U>
  using ref_like_t = std::conditional_t<
    std::is_reference_v<U>,
    std::conditional_t<
      std::is_lvalue_reference_v<U>,
      std::add_lvalue_reference_t<std::remove_reference_t<T>>,
      std::add_rvalue_reference_t<std::remove_reference_t<T>>>,
    std::remove_reference_t<T>>;

  template <typename T, typename U>
  using qual_like_t = ref_like_t<const_like_t<std::remove_reference_t<T>, U>, U>;

  namespace detail {
    template <auto>
    struct member_type_impl;

    template <typename T, typename U, T U::*member_ptr>
    struct member_type_impl<member_ptr> {
      using type = T;
    };
    template <auto>
    struct object_type_impl;

    template <typename T, typename U, T U::*member_ptr>
    struct object_type_impl<member_ptr> {
      using type = U;
    };
  }

  template <auto member_ptr>
  using member_type_t = typename detail::member_type_impl<member_ptr>::type;
  template <auto member_ptr>
  using object_type_t = typename detail::object_type_impl<member_ptr>::type;

  namespace detail {
    template <typename T>
    struct is_std_shared_ptr_impl : std::false_type { };

    template <typename T>
    struct is_std_shared_ptr_impl<std::shared_ptr<T>> : std::true_type { };
  }

  template <typename T>
  constexpr bool is_std_shared_ptr = detail::is_std_shared_ptr_impl<T>::value;

  namespace detail {
    template <typename T>
    struct is_std_vector_impl : std::false_type { };

    template <typename T, typename A>
    struct is_std_vector_impl<std::vector<T, A>> : std::true_type { };
  }

  template <typename T>
  constexpr bool is_std_vector = detail::is_std_vector_impl<T>::value;

  template <auto C>
  using constant_t = std::integral_constant<decltype(C), C>;
  template <auto C>
  inline constexpr constant_t<C> constant_v{};

  template <typename T>
  using identity_t = std::type_identity<T>;
  template <typename T>
  inline constexpr identity_t<T> identity_v{};

  inline constexpr struct ignore_t {
    constexpr ignore_t(auto &&...) noexcept {
    }

    constexpr void operator=(auto &&) const noexcept {
    }

    void operator()(auto &&...) const noexcept {
    }
  } ignore{};

}
