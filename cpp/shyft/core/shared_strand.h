#pragma once

#include <atomic>
#include <array>
#include <concepts>
#include <mutex>
#include <shared_mutex>
#include <type_traits>
#include <utility>

#include <boost/asio/detail/config.hpp>
#include <boost/asio/detail/strand_executor_service.hpp>
#include <boost/asio/detail/type_traits.hpp>
#include <boost/asio/execution/blocking.hpp>
#include <boost/asio/execution/executor.hpp>
#include <boost/asio/execution_context.hpp>
#include <boost/asio/is_executor.hpp>

#include <boost/asio/detail/push_options.hpp>

namespace shyft {

  namespace detail {

    struct shared_strand_impl;

    struct shared_strand_context {
      std::mutex service_mutex;
      shared_strand_impl* strands;
    };

    struct shared_strand_service;

    template <typename Function, typename Allocator>
    struct shared_strand_allocator_binder {
      using allocator_type = Allocator;

      Function function;
      Allocator allocator;

      auto get_allocator() const noexcept {
        return allocator;
      }

      void operator()() {
        function();
      }
    };

    enum class shared_strand_mode : bool {
      shared,
      unique
    };

    struct shared_strand_op {
      using operation_type = shared_strand_op;
      using func_type = void (*)(void*, shared_strand_op*, const boost::system::error_code&, std::size_t);

      func_type func = nullptr;
      shared_strand_op* next_ = nullptr;
      shared_strand_mode mode = shared_strand_mode::shared;

      explicit shared_strand_op(func_type func)
        : func{func} {
      }

      void complete(void* owner, const boost::system::error_code& ec, std::size_t bytes_transferred) {
        func(owner, this, ec, bytes_transferred);
      }

      void destroy() {
        func(nullptr, this, boost::system::error_code(), 0);
      }

      ~shared_strand_op() {
      }

      struct deleter {
        void operator()(shared_strand_op* ptr) const {
          delete ptr;
        }
      };

      using pointer = std::unique_ptr<shared_strand_op, deleter>;
    };

    struct shared_strand_impl {

      std::mutex* mutex = nullptr;
      bool locked = false, shutdown = false;
      shared_strand_mode mode = shared_strand_mode::shared;

      std::atomic<std::size_t> counter;

      boost::asio::detail::op_queue<shared_strand_op> waiting_queue;

      shared_strand_impl* next;
      shared_strand_impl* prev;
      shared_strand_context* context;

      ~shared_strand_impl() {
        std::scoped_lock lock(context->service_mutex);
        if (context->strands == this)
          context->strands = next;
        if (prev)
          prev->next = next;
        if (next)
          next->prev = prev;
      }

      friend shared_strand_op::pointer enqueue(shared_strand_impl& impl, shared_strand_op::pointer op) {
        std::scoped_lock lock{*impl.mutex};
        if (impl.shutdown)
          return nullptr;
        else if (
          !std::exchange(impl.locked, true) || (impl.mode == op->mode && impl.mode == shared_strand_mode::shared)) {
          impl.mode = op->mode;
          impl.counter++;
          return op;
        } else {
          impl.waiting_queue.push(op.release());
          return nullptr;
        }
      }
    };

    template <typename Executor>
    struct shared_strand_invoker {

      std::shared_ptr<shared_strand_impl> impl;
      Executor executor;
      shared_strand_op::pointer op;

      void operator()() {
        struct on_invoker_exit {
          shared_strand_invoker* self;

          ~on_invoker_exit() {
            if (--(self->impl->counter) != 0)
              return;

            std::scoped_lock lock{*self->impl->mutex};
            if (self->impl->counter != 0) //FIXME: crap! - jeh
              return;

            std::size_t queued = 0;
            while (auto op = self->impl->waiting_queue.front()) {

              if (queued == 0)
                self->impl->mode = op->mode;
              else if (queued != 0 && op->mode == shared_strand_mode::unique)
                break;

              self->impl->waiting_queue.pop();
              ++queued;
              auto executor = self->executor; //FIXME: swap decay-copy in c++23 - jeh
              boost::asio::defer(
                executor,
                shared_strand_invoker{
                  .impl = self->impl, .executor = self->executor, .op = shared_strand_op::pointer{op}});
              if (op->mode == shared_strand_mode::unique)
                break;
            }
            self->impl->counter = queued;
            self->impl->locked = (queued > 0);
          }
        };

        on_invoker_exit on_exit = {this};
        (void) on_exit;
        boost::system::error_code ec;
        op.release()->complete(impl.get(), ec, 0);
      }
    };

    auto make_shared_strand_invoker(
      const std::shared_ptr<shared_strand_impl>& impl,
      auto& executor,
      const auto& allocator,
      shared_strand_op::pointer op) {
      return shared_strand_allocator_binder{
        .function =
          shared_strand_invoker{
                                .impl = impl,
                                .executor = boost::asio::prefer(executor, boost::asio::execution::outstanding_work.tracked),
                                .op = std::move(op)},
        .allocator = allocator
      };
    }

    template <shared_strand_mode mode, typename Function, typename Allocator>
    auto enqueue_shared_strand_invoker(shared_strand_impl& impl, Function&& function, const Allocator& allocator) {
      using op = boost::asio::detail::executor_op<Function, Allocator, shared_strand_op>;
      typename op::ptr op_ptr{boost::asio::detail::addressof(allocator), op::ptr::allocate(allocator), 0};
      op_ptr.p = new (op_ptr.v) op(std::move(function), allocator);
      op_ptr.p->mode = mode;
      shared_strand_op::pointer o(op_ptr.p);
      op_ptr.v = op_ptr.p = nullptr;
      return enqueue(impl, std::move(o));
    }

    struct shared_strand_service : boost::asio::detail::execution_context_service_base<shared_strand_service> {

      static constexpr std::size_t num_mutexes = 193;

      shared_strand_context context{};
      std::array<std::unique_ptr<std::mutex>, num_mutexes> strand_mutexes{};
      std::size_t salt{};

      explicit shared_strand_service(boost::asio::execution_context& context)
        : boost::asio::detail::execution_context_service_base<shared_strand_service>(context) {
      }

      void shutdown() {
        boost::asio::detail::op_queue<shared_strand_op> ops;

        std::scoped_lock service_lock(context.service_mutex);

        auto strand = context.strands;
        while (strand) {
          {
            std::scoped_lock strand_lock{*strand->mutex};
            strand->shutdown = true;
            ops.push(strand->waiting_queue);
          }
          strand = strand->next;
        }
      }

      template <typename Executor>
      static auto& use(const Executor& ex) {
        if constexpr (boost::asio::can_query_v<Executor, boost::asio::execution::context_t>)
          return use_service<detail::shared_strand_service>(boost::asio::query(ex, boost::asio::execution::context));
        else
          return use_service<detail::shared_strand_service>(ex.context());
      }

      template <typename Executor>
      static auto make_strand_impl(const Executor& ex) {
        auto& service = use(ex);

        auto strand = std::make_shared<shared_strand_impl>();
        std::scoped_lock lock(service.context.service_mutex);

        const auto mutex_index = [&] {
          auto mutex_index = reinterpret_cast<std::size_t>(strand.get());
          mutex_index += (mutex_index >> 3);
          mutex_index ^= (service.salt++) + 0x9e3779b9 + (mutex_index << 6) + (mutex_index >> 2);
          mutex_index %= num_mutexes;
          return mutex_index;
        }();

        if (!service.strand_mutexes[mutex_index].get())
          service.strand_mutexes[mutex_index].reset(new std::mutex);
        strand->mutex = service.strand_mutexes[mutex_index].get();

        strand->next = service.context.strands;
        strand->prev = nullptr;
        if (service.context.strands)
          service.context.strands->prev = strand.get();
        service.context.strands = strand.get();
        strand->context = &service.context;
        return strand;
      }
    };

  }

  template <typename Executor, detail::shared_strand_mode mode = detail::shared_strand_mode::shared>
  struct shared_strand {
    Executor executor = Executor{};
    std::shared_ptr<detail::shared_strand_impl> impl = detail::shared_strand_service::make_strand_impl(executor);

    auto get_inner_executor() const {
      return executor;
    }

    auto unique() const
      requires(mode == detail::shared_strand_mode::shared)
    {
      return shared_strand<Executor, detail::shared_strand_mode::unique>{.executor = executor, .impl = impl};
    }

    friend bool operator==(const shared_strand& a, const shared_strand& b) noexcept {
      return a.impl == b.impl;
    }

    friend bool operator!=(const shared_strand& a, const shared_strand& b) noexcept {
      return a.impl != b.impl;
    }

    auto& context() const noexcept {
      if constexpr (boost::asio::can_query_v<const Executor&, boost::asio::execution::context_t>)
        return boost::asio::query(executor, boost::asio::execution::context);
      else
        return executor.context();
    }

    void on_work_started() const noexcept {
      executor.on_work_started();
    }

    void on_work_finished() const noexcept {
      executor.on_work_finished();
    }

    template <typename Property>
      requires(boost::asio::can_query_v<const Executor&, Property>)
    auto query(const Property& property) const
      noexcept(noexcept(boost::asio::is_nothrow_query_v<const Executor&, Property>)) {
      if constexpr (std::is_convertible_v<Property, boost::asio::execution::blocking_t>) {
        boost::asio::execution::blocking_t result = boost::asio::query(executor, property);
        return result == boost::asio::execution::blocking.always ? boost::asio::execution::blocking.possibly : result;
      } else
        return boost::asio::query(executor, property);
    }

    template <typename Property>
      requires(
        boost::asio::can_require_v<const Executor&, Property>
        && !std::is_convertible_v<Property, boost::asio::execution::blocking_t::always_t>)
    auto require(const Property& p) const
      noexcept(noexcept(boost::asio::is_nothrow_require_v<const Executor&, Property>)) {
      return shared_strand{.executor = boost::asio::require(executor, p), .impl = impl};
    }

    template <typename Property>
      requires(
        boost::asio::can_prefer_v<const Executor&, Property>
        && !std::is_convertible_v<Property, boost::asio::execution::blocking_t::always_t>)
    auto prefer(const Property& p) const
      noexcept(noexcept(boost::asio::is_nothrow_prefer_v<const Executor&, Property>)) {
      return shared_strand{.executor = boost::asio::prefer(executor, p), .impl = impl};
    }

    void dispatch(auto&& f, const auto& a) const {
      if (auto op = detail::enqueue_shared_strand_invoker<mode>(*impl, std::move(f), a))
        boost::asio::dispatch(executor, make_shared_strand_invoker(impl, executor, a, std::move(op)));
    }

    void post(auto&& f, const auto& a) const {
      if (auto op = detail::enqueue_shared_strand_invoker<mode>(*impl, std::move(f), a))
        boost::asio::post(executor, make_shared_strand_invoker(impl, executor, a, std::move(op)));
    }

    void defer(auto&& f, const auto& a) const {
      if (auto op = detail::enqueue_shared_strand_invoker<mode>(*impl, std::move(f), a))
        boost::asio::defer(executor, make_shared_strand_invoker(impl, executor, a, std::move(op)));
    }
  };

  template <typename Executor>
    requires boost::asio::is_executor<Executor>::value
  shared_strand<Executor> make_shared_strand(const Executor& ex) {
    return shared_strand<Executor>(ex);
  }
}

namespace boost::asio::traits {

#if !defined(BOOST_ASIO_HAS_DEDUCED_EQUALITY_COMPARABLE_TRAIT)

  template <typename Executor>
  struct equality_comparable<shyft::core::shared_strand<Executor>> {
    static constexpr bool is_valid = true;
    static constexpr bool is_noexcept = true;
  };

#endif // !defined(BOOST_ASIO_HAS_DEDUCED_EQUALITY_COMPARABLE_TRAIT)

#if !defined(BOOST_ASIO_HAS_DEDUCED_QUERY_MEMBER_TRAIT)

  template <typename Executor, typename Property>
    requires(can_query_v<const Executor&, Property>)
  struct query_member<shyft::core::shared_strand<Executor>, Property> {
    static constexpr bool is_valid = true;
    static constexpr bool is_noexcept = is_nothrow_query_v<Executor, Property>;
    using result_type = std::conditional_t<
      std::is_convertible_v<Property, execution::blocking_t>,
      execution::blocking_t,
      typename query_result<Executor, Property>::type>;
  };

#endif // !defined(BOOST_ASIO_HAS_DEDUCED_QUERY_MEMBER_TRAIT)

#if !defined(BOOST_ASIO_HAS_DEDUCED_REQUIRE_MEMBER_TRAIT)

  template <typename Executor, typename Property>
    requires(
      can_require_v<const Executor&, Property> && !std::is_convertible_v<Property, execution::blocking_t::always_t>)
  struct require_member<shyft::core::shared_strand<Executor>, Property> {
    static constexpr bool is_valid = true;
    static constexpr bool is_noexcept = is_nothrow_require_v<Executor, Property>;
    using result_type = shyft::core::shared_strand<std::decay_t<typename require_result<Executor, Property>::type>>;
  };

#endif // !defined(BOOST_ASIO_HAS_DEDUCED_REQUIRE_MEMBER_TRAIT)

#if !defined(BOOST_ASIO_HAS_DEDUCED_PREFER_MEMBER_TRAIT)

  template <typename Executor, typename Property>
    requires(
      can_prefer_v<const Executor&, Property> && !std::is_convertible_v<Property, execution::blocking_t::always_t>)
  struct prefer_member<shyft::core::shared_strand<Executor>, Property> {
    static constexpr bool is_valid = true;
    static constexpr bool is_noexcept = is_nothrow_prefer_v<Executor, Property>;
    using result_type = shyft::core::shared_strand<std::decay_t<prefer_result<Executor, Property>::type>>;
  };

#endif // !defined(BOOST_ASIO_HAS_DEDUCED_PREFER_MEMBER_TRAIT)

}

#include <boost/asio/detail/pop_options.hpp>
