#pragma once

#include <type_traits>

#include <boost/describe/bases.hpp>
#include <boost/describe/enum_to_string.hpp>
#include <boost/describe/enumerators.hpp>
#include <boost/describe/members.hpp>
#include <boost/hof/detail/forward.hpp>
#include <boost/mp11/algorithm.hpp>
#include <fmt/core.h>

#include <shyft/core/reflection.h>

namespace shyft::reflection {

  template <reflected_enum T, typename Char>
  struct enum_formatter {
    using U = std::underlying_type_t<T>;

    fmt::formatter<fmt::basic_string_view<Char>, Char> sf_;
    fmt::formatter<U, Char> nf_;

    constexpr auto parse(fmt::basic_format_parse_context<Char> &ctx) {
      auto i1 = sf_.parse(ctx);
      auto i2 = nf_.parse(ctx);

      if (i1 != i2)
        ctx.error_handler().on_error("invalid format");

      return i1;
    }

    auto format(const T &t, fmt::buffer_context<Char> &ctx) const {

      char const *s = boost::describe::enum_to_string(t, 0);

      if (s)
        return sf_.format(s, ctx);
      else
        return nf_.format(static_cast<U>(t), ctx);
    }
  };

  template <reflected_struct T, typename Char>
  struct struct_formatter {

    constexpr auto parse(fmt::basic_format_parse_context<Char> &ctx) {
      auto it = ctx.begin(), end = ctx.end();

      if (it != end && *it != '}')
        ctx.error_handler().on_error("invalid format");

      return it;
    }

    auto format(const T &t, fmt::buffer_context<Char> &ctx) const {
      using namespace boost::describe;

      using Bd = describe_bases<T, mod_any_access>;
      using Md = describe_members<T, mod_any_access>;

      auto out = ctx.out();

      *out++ = '{';

      bool first = true;

      boost::mp11::mp_for_each<Bd>([&](auto D) {
        if (!first)
          *out++ = ',';

        first = false;

        out = fmt::format_to(out, " {}", (typename decltype(D)::type const &) t);
      });

      boost::mp11::mp_for_each<Md>([&](auto D) {
        if (!first)
          *out++ = ',';

        first = false;

        out = fmt::format_to(out, " .{}={}", D.name, t.*D.pointer);
      });

      if (!first)
        *out++ = ' ';

      *out++ = '}';

      return out;
    }
  };

#define SHYFT_DEFINE_STRUCT_FORMATTER(C) \
  template <typename Char> \
  struct fmt::formatter<C, Char> : shyft::reflection::struct_formatter<C, Char> { };

#define SHYFT_DEFINE_ENUM_FORMATTER(C) \
  template <typename Char> \
  struct fmt::formatter<C, Char> : shyft::reflection::enum_formatter<C, Char> { };

}
