#pragma once
/** This file is part of Shyft. Copyright 2015-2019 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <boost/geometry/srs/projections/dpar.hpp>
#include <utility>
#include <stdexcept>
#include <string>
#include <algorithm>

namespace boost::geometry::projections::detail {
    namespace epsg_data {
        using namespace srs::dpar;

        /**
         * @brief simplest possible pod to capture a parameter definition
         * @note: its imperative that this is really a simple POD, otherwise compiletimes explodes
         */
        struct epsg_p_def {
            int e_type{-1};///< unique enum type, one of name_xxx enums. -1 means not defined
            int e_val{0};  ///< sub-enum type, one of value_xxx enums
            int n_args{0}; ///< number of floating point args, dependent on e_type,e_val
            double args[7];///< most parameters are 1, towgs84 3 or 7

            /** @brief bounds and range check access to args, using n_args */
            double arg(size_t i) const {
                if (i>=size_t(n_args)) {
                    throw std::runtime_error("epsg_p_def: out of range arg idx for n="
                    + std::to_string(n_args)
                    + ", supplied idx="+ std::to_string(i)
                    + ", e_type="+ std::to_string(e_type)
                    + ", e_val="+ std::to_string(e_val)
                    );
                }
                return args[i];
            }
            void no_args() const {
                if (n_args) {
                    throw std::runtime_error(
                        "epsg_p_def: unused args n=" +std::to_string(n_args)
                    + ", e_type="+ std::to_string(e_type)
                    + ", e_val="+ std::to_string(e_val)
                    );
                }
            }

            template<class Fx>
            void apply_parameter(Fx &&fx) const {
                constexpr auto in_range=[](int x,int from,int to) noexcept ->bool  {return x>=from &&x<to;};
                if(e_type<0) return;

                if(in_range(e_type,name_f::a,name_r::alpha)) {
                    fx(name_f(e_type),arg(0));
                } else if(in_range(e_type,name_r::alpha,name_i::aperture)) {
                    fx(name_r(e_type),arg(0));
                } else if(in_range(e_type,name_i::aperture,name_be::czech)) {
                    fx(name_i(e_type),e_val);no_args();
                } else if(in_range(e_type,name_be::czech,name_datum::datum)) {
                        fx(name_be(e_type));no_args();
                } else if(in_range(e_type,name_datum::datum,name_ellps::ellps)) {
                    fx(name_datum(e_type),value_datum(e_val));no_args();
                } else if(in_range(e_type,name_ellps::ellps,name_mode::mode)) {
                    fx(name_ellps(e_type),value_ellps(e_val));no_args();
                } else if(in_range(e_type,name_mode::mode,name_nadgrids::nadgrids)) {
                    fx(name_mode(e_type),value_mode(e_val));no_args();
                } else if(in_range(e_type,name_orient::orient,name_pm::pm)) {
                    fx(name_orient(e_type),value_orient(e_val));no_args();
                } else if(in_range(e_type,name_pm::pm,name_proj::o_proj)) {
                    // it could be ID, or it could be angle, then n_args==1
                    if(n_args==1) { // pm, angle
                        fx(name_pm(e_type),arg(0));
                    } else {//two enums, pm. value_pm
                        fx(name_pm(e_type),value_pm(e_val));no_args();
                    }
                } else if(in_range(e_type,name_proj::o_proj,name_sweep::sweep)) {
                    fx(name_proj(e_type),value_proj(e_val));no_args(); // usually picked as first, should not occure in stream afterwards.
                } else if(in_range(e_type,name_sweep::sweep,name_towgs84::towgs84)) {
                    fx(name_sweep(e_type),value_sweep(e_val));no_args();
                } else if(in_range(e_type,name_towgs84::towgs84,name_units::units)) {
                    if(n_args ==3) {
                        fx(name_towgs84(e_type),srs::detail::towgs84<>(args[0],args[1],args[2]));
                    } else if(n_args==7){
                        fx(name_towgs84(e_type),srs::detail::towgs84<>(args[0],args[1],args[2],args[3],args[4],args[5],args[6]));
                    } else {
                        throw std::runtime_error ("towgs84 must be 3 or 7 args, supplied value:"+std::to_string(n_args));
                    }
                } else if(in_range(e_type,name_units::units,name_units::vunits+1)) {
                    fx(name_units(e_type),value_units(e_val));no_args();
                } else {
                    throw std::runtime_error ("missed code "+std::to_string(e_type));
                }
                return ;
            }
        };

        /**
         * @brief  pod to keep one epsg def, a set of epsg_p_defs.
         */
        struct epsg_def {
            static constexpr size_t n_defs{13}; ///< current maximum number of parameters of one transform.
            epsg_p_def p_defs[n_defs];// currently number of p_defs are

            /**
             * @brief create boost geometry parameters based on p_defs.
             * @return a parameters<> ready to use based on definitions.
             */
            parameters<double> mk_parameters() const {
                if(p_defs[0].e_type>=0) {
                    parameters<double> r(value_proj(p_defs[0].e_val));//assuming the first arg is always the projection code.
                    const auto apply_arg=[&r](auto... args) {
                        r(std::forward<decltype(args)>(args)...);
                    };
                    for(size_t i=1;i<n_defs;++i) {
                        if(p_defs[i].e_type<0) break;// default init gives -1, and we stop processing at the first -1
                        p_defs[i].apply_parameter(apply_arg);// apply the arg by using the operator(...) of parameters
                    }
                    return r;
                }
                return {};
            }
        };

        /**
         * @brief Simple POD to represent one epsg transform.
         */
        struct epsg_entry {
            int code;      ///< the epsg code
            epsg_def data; ///< the epsg_def that makes up the parameters for the transform.
        };

    }

    srs::dpar::parameters<> epsg_to_parameters_fast(int code);
    std::vector<int> all_epsg_codes() ;

}
