#include <shyft/web_api/web_api_generator.h>

namespace shyft::web_api::generator {
    template<class OutputIterator>
    find_ts_request_generator<OutputIterator>::find_ts_request_generator():find_ts_request_generator::base_type(g_) {
        g_ = "find {\"request_id\":\"" << ka::string[ka::_1=phx::bind(&find_ts_request::request_id, ka::_val)]
        <<   "\",\"find_pattern\":\""<< ka::string[ka::_1=phx::bind(&find_ts_request::find_pattern, ka::_val)]
        <<   "\"}"
        ;
        g_.name("find_ts_request");
    }
    template struct find_ts_request_generator<generator_output_iterator>;

}
