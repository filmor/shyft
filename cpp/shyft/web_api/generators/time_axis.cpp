/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/web_api/web_api_generator.h>


namespace shyft::web_api::generator {

    template<class OutputIterator>
    fixed_dt_generator<OutputIterator>::
    fixed_dt_generator(): fixed_dt_generator::base_type(pg) {
        using ka::int_;
        pg = ka::lit("{\"t0\":") << time_[ka::_1=phx::bind(&fixed_dt::t, ka::_val)]
            << ka::lit(",\"dt\":") << time_[ka::_1=phx::bind(&fixed_dt::dt, ka::_val)]
            << ka::lit(",\"n\":") << int_[ka::_1=phx::bind(&fixed_dt::n, ka::_val)]
            << ka::lit("}");
        pg.name("fixed_dt");
    }

    template<class OutputIterator>
    calendar_dt_generator<OutputIterator>::
    calendar_dt_generator():calendar_dt_generator::base_type(pg) {
        using ka::int_;

        pg = ka::lit("{\"calendar\":") << "\"" << ka::string[ka::_1=phx::bind(&calendar_dt::get_tz_name,ka::_val)] << "\""
            << ka::lit(",\"t0\":") << time_[ka::_1=phx::bind(&calendar_dt::t, ka::_val)]
            << ka::lit(",\"dt\":") << time_[ka::_1=phx::bind(&calendar_dt::dt, ka::_val)]
            << ka::lit(",\"n\":") << int_[ka::_1=phx::bind(&calendar_dt::n, ka::_val)]
            << ka::lit("}");
        pg.name("calendar_dt");
    }

    template<class OutputIterator>
    point_dt_generator<OutputIterator>::
    point_dt_generator(): point_dt_generator::base_type(pg) {
        pg = ka::lit("{\"time_points\":[")
            << (t_ % ",")[ka::_1 = phx::bind(&point_dt::t, ka::_val)] << ka::lit(",")<<t_[ka::_1 = phx::bind(&point_dt::t_end, ka::_val)]
            << ka::lit("]}");
        pg.name("point_dt");
    }

    template<class OutputIterator>
    generic_dt_generator<OutputIterator>::
    generic_dt_generator(): generic_dt_generator::base_type(pg) {
        using ka::int_;
        constexpr auto gti=[](auto&&me)->int { return static_cast<int>(me.gt());};// phx needs precise type to compile
        constexpr auto ta_f=[](auto&&me)->fixed_dt {return me.f();};
        constexpr auto ta_c=[](auto&&me)->calendar_dt {return me.c();};
        constexpr auto ta_p=[](auto&&me)->point_dt {return me.p();};

        pg =
            ( (&int_(0)[ka::_1 = phx::bind(gti, ka::_val)] <<  f_[ka::_1 = phx::bind(ta_f, ka::_val)])
            | (&int_(1)[ka::_1 = phx::bind(gti, ka::_val)] <<  c_[ka::_1 = phx::bind(ta_c, ka::_val)])
            | (&int_(2)[ka::_1 = phx::bind(gti, ka::_val)] <<  p_[ka::_1 = phx::bind(ta_p, ka::_val)])
            )
            ;
    }

    template struct fixed_dt_generator<generator_output_iterator>;
    template struct calendar_dt_generator<generator_output_iterator>;
    template struct point_dt_generator<generator_output_iterator>;
    template struct generic_dt_generator<generator_output_iterator>;


}
