#pragma once
/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/web_api/targetver.h>

//
// Copyright (c) 2016-2017 Vinnie Falco (vinnie dot falco at gmail dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//
// Official repository: https://github.com/boostorg/beast
//


//------------------------------------------------------------------------------
// NOTE:  This is all based on  Vinnie Falco boost.beast work,
// example: Advanced server, flex (plain + SSL)
// With minor adaptions and changes to allow for
// dispatch to long running background tasks.
//
//------------------------------------------------------------------------------
#include <boost/beast/version.hpp>

#if BOOST_BEAST_VERSION < 248
#pragma message("this require boost beast >= 1.70")
#endif
//-- BOOST 1.70 and new BEAST simplifies a lot, breaking changes though.

#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/asio/bind_executor.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/asio/strand.hpp>
#include <shyft/web_api/server_certificate.h>
#include <shyft/web_api/bg_work_result.h>
#include <shyft/core/subscription.h>
//#include <shyft/dtss/dtss_subscription.h>
#include <optional>
#include <algorithm>
#include <cstdlib>
#include <functional>
#include <iosfwd>
#include <memory>
#include <string>
#include <thread>
#include <vector>
#include <mutex>
#include <queue>

//#include <iostream>


namespace shyft::web_api {
namespace beast = boost::beast;                 // from <boost/beast.hpp>
namespace http = beast::http;                   // from <boost/beast/http.hpp>
namespace websocket = beast::websocket;         // from <boost/beast/websocket.hpp>
namespace net = boost::asio;                    // from <boost/asio.hpp>
namespace ssl = boost::asio::ssl;               // from <boost/asio/ssl.hpp>
using tcp = boost::asio::ip::tcp;               // from <boost/asio/ip/tcp.hpp>
using std::recursive_mutex; // 
using std::scoped_lock;
// Return a reasonable mime type based on the extension of a file.
extern beast::string_view mime_type( beast::string_view path );

// Append an HTTP rel-path to a local filesystem path.
// The returned path is normalized for the platform.
extern std::string path_cat( beast::string_view base,beast::string_view path );

// Report a failure
extern void fail( beast::error_code ec, char const* what );

// handle error, return
#define return_on_error(ec,diag) if((ec)) {fail((ec),(diag));return;}


    /** @brief bg_worker, bacground work
     *
     * This class have the role to enable dispatching messages to bacground_work class
     * that have it's own boost::asio::io_context.
     * This is to ensure that the front-end threads serving the web is not exhausted
     * doing long running operations.
     *
     * This class is used as parameter to the root of the session-handlers
     * so it is quite essential (but not complex) for the server-framework.
     *
     * Since we would like to use the same web-server framework for
     * different backend services, we keep the server-class implementation
     * as a template parameter.
     *
     * Typically the server class S, does stuff like this:
     *
     *  -# parse input (text/buffer from web)
     *  -# compute/do stuff based on parsed input parameters
     *  -# generate output/response(s) as multi_buffer/buffer
     *  -# then post/replie(s) back to front-end io-threads running the io-services.
     *
     *  @tparam S bacground server providing the do_the_work(msg)->bg_work_result method
     */
    template <class S>
    struct bg_worker {
        S& server;///< not owning, lifetime exeeds this object
        net::io_context& ioc;///< post bg work to this ioc
    };
    
    /** This function produces an HTTP response for the given
     * request. The type of the response object depends on the
     * contents of the request, so the interface requires the
     * caller to pass a generic lambda for receiving the response.
     */
    template<
        class Body, class Allocator,
        class Send>
    void
    handle_request( beast::string_view doc_root,http::request<Body, http::basic_fields<Allocator>>&& req, Send&& send ) {
        // Returns a bad request response
        auto const bad_request =
        [&req]( beast::string_view why ) {
            http::response<http::string_body> res {http::status::bad_request, req.version() };
            res.set( http::field::server, BOOST_BEAST_VERSION_STRING );
            res.set( http::field::content_type, "text/html" );
            res.keep_alive( req.keep_alive() );
            res.body() = std::string( why );
            res.prepare_payload();
            return res;
        };

        // Returns a not found response
        auto const not_found =
        [&req]( beast::string_view target ) {
            http::response<http::string_body> res {http::status::not_found, req.version() };
            res.set( http::field::server, BOOST_BEAST_VERSION_STRING );
            res.set( http::field::content_type, "text/html" );
            res.keep_alive( req.keep_alive() );
            res.body() = "The resource '" + std::string( target ) + "' was not found.";
            res.prepare_payload();
            return res;
        };

        // Returns a server error response
        auto const server_error =
        [&req]( beast::string_view what ) {
            http::response<http::string_body> res {http::status::internal_server_error, req.version() };
            res.set( http::field::server, BOOST_BEAST_VERSION_STRING );
            res.set( http::field::content_type, "text/html" );
            res.keep_alive( req.keep_alive() );
            res.body() = "An error occurred: '" + std::string( what ) + "'";
            res.prepare_payload();
            return res;
        };

        // Make sure we can handle the method
        if ( req.method() != http::verb::get &&
                req.method() != http::verb::head )
            return send( bad_request( "Unknown HTTP-method" ) );

        // Request path must be absolute and not contain "..".
        if ( req.target().empty() ||
                req.target() [0] != '/' ||
                req.target().find( ".." ) != beast::string_view::npos )
            return send( bad_request( "Illegal request-target" ) );

        // Build the path to the requested file
        std::string path = path_cat( doc_root, req.target() );
        if ( req.target().back() == '/' )
            path.append( "index.html" );

        // Attempt to open the file
        beast::error_code ec;
        http::file_body::value_type body;
        body.open( path.c_str(), beast::file_mode::scan, ec );

        // Handle the case where the file doesn't exist
        if ( ec == beast::errc::no_such_file_or_directory ) {
            // route the path here
            path=path_cat(doc_root,"/index.html");
            body.open(path.c_str(),beast::file_mode::scan,ec);
            if ( ec == beast::errc::no_such_file_or_directory )
                return send( not_found( path.c_str()  ) );
        }
        // Handle an unknown error
        if ( ec )
            return send( server_error( ec.message() ) );

        // Cache the size since we need it after the move
        auto const size = body.size();

        // Respond to HEAD request
        if ( req.method() == http::verb::head ) {
            http::response<http::empty_body> res {http::status::ok, req.version() };
            res.set( http::field::server, BOOST_BEAST_VERSION_STRING );
            res.set( http::field::content_type, mime_type( path ) );
            res.content_length( size );
            res.keep_alive( req.keep_alive() );
            return send( std::move( res ) );
        }

        // Respond to GET request
        http::response<http::file_body> res {
            std::piecewise_construct,
            std::make_tuple( std::move( body ) ),
            std::make_tuple( http::status::ok, req.version() )
        };
        res.set( http::field::server, BOOST_BEAST_VERSION_STRING );
        res.set( http::field::content_type, mime_type( path ) );
        res.content_length( size );
        res.keep_alive( req.keep_alive() );
        return send( std::move( res ) );
    }

    /** This function produces an HTTP response for the given
     * request. The type of the response object depends on the
     * contents of the request, so the interface requires the
     * caller to pass a generic lambda for receiving the response.
     */
    template<
        class Body, class Allocator,
        class Send>
    void
    handle_no_auth_request(http::request<Body, http::basic_fields<Allocator>>&& req, Send&& send ) {
        // Returns a bad request response
        auto const no_auth_request =
        [&req]( beast::string_view why ) {
            http::response<http::string_body> res {http::status::unauthorized, req.version() };
            res.set( http::field::server, BOOST_BEAST_VERSION_STRING );
            res.set( http::field::content_type, "text/html" );
            res.set( http::field::www_authenticate,"Basic Realm=\"beast login\"");
            res.keep_alive( req.keep_alive() );
            res.body() = std::string( why );
            res.prepare_payload();
            return res;
        };
        return send( no_auth_request("Supplied creds/missing not correct"));
    }

    /** web socket session handler
    *
    * This class is common base-class , so that plain ws, and ssl ws is derived from
    * this using CRTP pattern. Somewhat involved in our case, - but the boiler plate
    * from beast was using this, so we just extend it somewhat to allow to
    * dispatch work to the bacground worker io-service, that does
    * the service-kind of work.
    * 
    * subscriptions: the bg work result subscription information that this class
    *                need to own, so that subscription is removed when the 
    *                socket cease to exist.
    *                currently it is the bgw that parse and do message handling,
    *                and passes the results to the foreground thread/fiber when
    *                done. The response, flat-buffer, is feed out to the client,
    *                in addition, we keep the subscritions, and inspect them
    *                using a timer at suitable frequency.
    *
    * @tparam Derived<bg_work> a nested template parameter (a parameter that is it self a template)
    * @tparam bg_work the background worker class that keeps the io_context and the server type.
    *
    */
    template<class Derived,class bg_work>
    class websocket_session {
        // Access the derived class, this is part of
        // the Curiously Recurring Template Pattern idiom.
        Derived& derived() { return static_cast<Derived&>( *this );}

        beast::flat_buffer buffer_;///< incoming messages are continously read into this, using async read.
        beast::flat_buffer out_buffer_;///< ongoing write keeps outgoing message here, on completion, it's cleared, then refilled from wq
        bg_work& bgw_;///< real work is processed in the bgw.io fiber, which is also could be multicore.
        bool ongoing_write{false};///< true if async write response is ongoing,
        std::queue<beast::flat_buffer> wq;///< bgw_ attempts to send more data is queued into wq if ongoing_write is true
        std::shared_ptr<net::steady_timer> timer_;//derived().ws().get_executor().context(),std::chrono::steady_clock::time_point_max());
        std::chrono::milliseconds check_timeout{5};
        void start_timer() {
            if(!timer_)
                timer_=std::make_shared<net::steady_timer>(derived().ws().get_executor());
            timer_->expires_after(check_timeout);
            timer_->async_wait([me=derived().shared_from_this()](beast::error_code ec)->void {
                if(ec==boost::asio::error::operation_aborted)
                    return;// ignore this one, it means timer was started with some other timeout.
                return_on_error( ec, "timer" ) // just bail out on cancel
                me->on_subscription_timer_check();
            });
        }
        
        void stop_timer() {
            if(timer_) {
                timer_->expires_at(std::chrono::steady_clock::time_point::max());
            }
        }
        
        std::vector<shyft::core::subscription::observer_base_> subscriptions;///< live subscriptions goes here
        void add_subscription(shyft::core::subscription::observer_base_ x) {
            subscriptions.emplace_back(std::move(x));// or .emplace_back(std::move(x)) to avoid ref.hit
            if( subscriptions.size()==1u) start_timer();
        }
        void remove_subscription(std::string const&s_id) {
            for(size_t i=0;i<subscriptions.size();++i) {
                if(subscriptions[i]->request_id==s_id) {
                    subscriptions.erase(subscriptions.begin()+i);
                    if(subscriptions.size()==0u) stop_timer();
                    return;
                }
            }
        }
        void on_subscription_timer_check() {
            for(auto const & sub:subscriptions) {
                if(sub->has_changed()) {
                    // post real work to bg-thread.
                    net::post(bgw_.ioc,[sub_job=sub,me=derived().shared_from_this()]{ // me=shared_from_this ensure the session stays around while doing back work.
                        // need to post back to this ioc/executor, because several responses might be in the pipeline.
                        net::post(me->derived().ws().get_executor(),[me,sub_job=std::move(sub_job)]{// move into the inner context, really needed
                            me->on_bg_work_ready(beast::error_code {},me->bgw_.server.do_subscription_work(std::move(sub_job)));// finally, move on to the call
                        });
                    });
                }
            }
            if(subscriptions.size()) {
                timer_->expires_after(check_timeout);
                timer_->async_wait([me=derived().shared_from_this()](beast::error_code ec)->void {
                    if(ec==boost::asio::error::operation_aborted)
                        return;// ignore this one, it means timer was started with some other timeout.
                    return_on_error( ec, "timer" ) // just bail out on cancel
                    me->on_subscription_timer_check();
                });
            }
        }
        void subscription_cleanup() {
            subscriptions.clear();
            // stop timer.
        }
        // Start the asynchronous operation
        template<class Body, class Allocator>
        void do_accept( http::request<Body, http::basic_fields<Allocator>> req ) {
            // Set suggested/shorter ping//pong intervals timeout settings for the websocket
            auto opt=websocket::stream_base::timeout::suggested( beast::role_type::server );
            opt.idle_timeout=std::chrono::seconds(100);// allow ping frame from server to happen more often to avoid firewall/timeouts
            derived().ws().set_option(opt);

            derived().ws().set_option(  // Set a decorator to change the Server of the handshake
                websocket::stream_base::decorator( []( websocket::response_type& res ) {
                    res.set( http::field::server,"shyft-web-api/boost-beast" );
                }
                )
            );
            derived().ws().read_message_max(100*1024*1024ull);// 100MB
            derived().ws().async_accept( req,  // Accept the websocket handshake
                                         beast::bind_front_handler(
                                             &websocket_session::on_accept,
                                             derived().shared_from_this()
                                         )
                                       );
        }

        void on_accept( beast::error_code ec ) {
            return_on_error( ec, "accept" )
            do_read(); // Read a message
        }

        void do_read() {
            // Read a message into our buffer
            derived().ws().async_read( buffer_,
                                       beast::bind_front_handler(
                                           &websocket_session::on_read,
                                           derived().shared_from_this()
                                       )
                                     );
        }

        void on_read( beast::error_code ec, std::size_t bytes_transferred ) {
            boost::ignore_unused( bytes_transferred );
            if ( ec == websocket::error::closed ) {// This indicates that the websocket_session was closed
                subscription_cleanup();// stop timer etc.
                stop_timer();
                return;
            }

            return_on_error( ec, "sock_read" )
            // Echo the message
                        //---
            // here buffer_ is complete message
            // forward the text/message to the ws-handler
            auto req=boost::beast::buffers_to_string(buffer_.data());
            buffer_.consume(buffer_.size());// empty the buffer,
            // on_bg_work_ready(beast::error_code{},bgw_.server.do_the_work(std::move(req)));// working
            net::post(bgw_.ioc,[reqst=std::move(req),me=derived().shared_from_this()]{ // me=shared_from_this ensure the session stays around while doing back work.
                // need to post back to this ioc/executor, because several responses might be in the pipeline.
                net::post(me->derived().ws().get_executor(),[me,reqstx=std::move(reqst)]{// move into the inner context, really needed
                    me->on_bg_work_ready(beast::error_code {},me->bgw_.server.do_the_work(std::move(reqstx)));// finally, move on to the call
                });
            });
            do_read();// just continue reading more messages, the write will complete once bg worker is done
        }
        void on_bg_work_ready(beast::error_code ec, bg_work_result r) {
            return_on_error(ec,"bg-work failed")
            if(r.unsubscribe_id.size()>0) {
                remove_subscription(r.unsubscribe_id);
            } else if (r.subscription) {
                add_subscription(std::move(r.subscription));
            }
            if(r.response.size()==0)
                return;// no response, to write, is also possible
            if(ongoing_write){ // just push the buffer back to wq, and
                wq.emplace(std::move(r.response));// it will be chained out when the ongoing write is completed.
            } else {// no ongoing write activity, then we start shipping out this message,
                ongoing_write=true;// signal to next bg_work ready to queue the work
                out_buffer_=std::move(r.response); //moving into buffer_, ensure that buffer_ outlives async_write
                derived().ws().text(true);
                derived().ws().async_write( out_buffer_.data(),
                beast::bind_front_handler(
                    &websocket_session::on_write,
                        derived().shared_from_this()
                    )
                );
            }
        }

        /** Called each time bgw is done with the job */
        void on_write( beast::error_code ec,std::size_t bytes_transferred ) {
            boost::ignore_unused( bytes_transferred );
            if(ec) { // disaster, we drop out all we have, including pending writes.
                fail(ec,"on_write failed cleanup socket");
                ongoing_write=false;
                wq= decltype(wq){};// clear contents.
                subscription_cleanup();// stop timer etc.
                stop_timer();
                return;
            }
            out_buffer_.consume( out_buffer_.size() );  // Clear the buffer
            if( wq.empty()) {
                ongoing_write=false;// we are done, signal we can take next write response directly
            } else { // handle pending
                out_buffer_=std::move(wq.front());
                wq.pop();
                derived().ws().text(true);
                derived().ws().async_write( out_buffer_.data(),
                beast::bind_front_handler(
                    &websocket_session::on_write,
                        derived().shared_from_this()
                    )
                );
            }
        }

    public:
        
        explicit websocket_session(bg_work& bgw):bgw_{bgw}{}
        // Start the asynchronous operation
        template<class Body, class Allocator>
        void run( http::request<Body, http::basic_fields<Allocator>> req ) {
            do_accept( std::move( req ) );  // Accept the WebSocket upgrade request
        }
    };


    /** @brief handles ssl web-socket session
    *
    * Using the base-class websocket_session, adding and keeping the tcp::socket
    * for this session. Notice that all ws business as well as handling messages is
    * performed by the websocket_session base-class. This class is only responsible
    * for keeping the web-socket available, and handling upgrade/close/timeouts.
    *
    * @tparam bg_work the bacground worker type that will handle our requests,
    *
    * @see websocket_session, plain_websocket_session
    */
    template <class bg_work>
    struct plain_websocket_session
        : public websocket_session<plain_websocket_session<bg_work>,bg_work>
        , public std::enable_shared_from_this<plain_websocket_session<bg_work>> {
        using base=websocket_session<plain_websocket_session<bg_work>,bg_work>;
        using stream_t =websocket::stream<beast::tcp_stream>;

        explicit // Create the session
        plain_websocket_session( beast::tcp_stream&& stream,bg_work& bgw)
            :base(bgw), ws_( std::move( stream ) ) {
        }

        // Called by the base class
        stream_t& ws() {return ws_;}
        private:
            stream_t ws_;
    };


    /** @brief handles ssl web-socket session
    *
    * Using the base-class websocket_session, adding and keeping the tcp::socket
    * for this session. Notice that all ws business as well as handling messages is
    * performed by the websocket_session base-class. This class is only responsible
    * for keeping the web-socket available, and handling upgrade/close/timeouts.
    *
    * @tparam bg_work the bacground worker type that will handle our requests,
    *
    * @see websocket_session, plain_websocket_session
    */

    template <class bg_work>
    struct ssl_websocket_session
        : public websocket_session<ssl_websocket_session<bg_work>,bg_work>
        , public std::enable_shared_from_this<ssl_websocket_session<bg_work>> {
    
        using base=websocket_session<ssl_websocket_session<bg_work>,bg_work>;
        using stream_t = websocket::stream<beast::ssl_stream<beast::tcp_stream>>;

        // Create the ssl_websocket_session
        explicit ssl_websocket_session( beast::ssl_stream<beast::tcp_stream>&& stream, bg_work& bgw )
            :base(bgw),ws_( std::move( stream ) ) {
        }

        // Called by the base class
        stream_t& ws() { return ws_;}
        private:
            stream_t ws_;
    };

    //------------------------------------------------------------------------------

    template<class Body, class Allocator,class bg_work>
    void make_websocket_session( beast::tcp_stream stream, http::request<Body, http::basic_fields<Allocator>> req, bg_work& bgw) {
        std::make_shared<plain_websocket_session<bg_work>> ( std::move( stream ),bgw )->run( std::move( req ) );
    }

    template<class Body, class Allocator,class bg_work>
    void make_websocket_session( beast::ssl_stream<beast::tcp_stream> stream, http::request<Body, http::basic_fields<Allocator>> req, bg_work& bgw ) {
        std::make_shared<ssl_websocket_session<bg_work>> ( std::move( stream ),bgw )->run( std::move( req ) );
    }

    /** @brief common base http-session handler  for both plain and ssl
    *
    * This class handles http-sessions, used as base-class for the plain and ssl based
    * web-handlers, so that we can keep common code in one place.
    * It's using CRTP, as the boost beast boiler-plate code did,
    * but extended with the bg_work class so that we *could* dispatch
    * time-consuming work to the background io_context and server.
    *
    * @tparam Derived<bg_work> A template parameter taking bg_work as parameter
    * @tparam bg_work the bacground worker containing io_context ioc and a server that deals with the requests
    *
    */
    template<class Derived, class bg_work>
    class http_session {
        // Access the derived class, this is part of
        // the Curiously Recurring Template Pattern idiom.
        Derived& derived() { return static_cast<Derived&>( *this );}

        // This queue is used for HTTP pipelining.
        class queue {
                enum { limit = 8 };// Maximum number of responses we will queue
                // The type-erased, saved work item
                struct work {
                    virtual ~work() = default;
                    virtual void operator()() = 0;
                };
                http_session& self_;
                std::vector<std::unique_ptr<work>> items_;

            public:
                explicit queue( http_session& self ): self_( self ) {
                    static_assert( limit > 0, "queue limit must be positive" );
                    items_.reserve( limit );
                }

                // Returns `true` if we have reached the queue limit
                bool is_full() const { return items_.size() >= limit; }

                // Called when a message finishes sending
                // Returns `true` if the caller should initiate a read
                bool on_write() {
                    BOOST_ASSERT( ! items_.empty() );
                    auto const was_full = is_full();
                    items_.erase( items_.begin() );
                    if ( ! items_.empty() )
                        ( *items_.front() )();
                    return was_full;
                }

                // Called by the HTTP handler, ref to template<..> handle_message(...Send&&) to send a response.
                template<bool isRequest, class Body, class Fields>
                void operator()( http::message<isRequest, Body, Fields>&& msg ) {
                    // This holds a work item
                    struct work_impl : work {
                        http_session& self_;
                        http::message<isRequest, Body, Fields> msg_;

                        work_impl( http_session& self, http::message<isRequest, Body, Fields>&& msg )
                            : self_( self )
                            , msg_( std::move( msg ) ) {
                        }

                        void operator()() {
                            http::async_write( self_.derived().stream(), msg_,
                                               beast::bind_front_handler(
                                                   &http_session::on_write,
                                                   self_.derived().shared_from_this(),
                                                   msg_.need_eof()
                                               )
                                             );
                        }
                    };

                    // Allocate and store the work
                    items_.push_back( std::make_unique<work_impl> ( self_, std::move( msg ) ) );
                    // If there was no previous work, start this one
                    if ( items_.size() == 1 )
                        ( *items_.front() )();
                }
        };

        std::shared_ptr<std::string const> doc_root_;
        queue queue_;

        // The parser is stored in an optional container so we can
        // construct it from scratch it at the beginning of each new message.
        std::optional<http::request_parser<http::string_body>> parser_;

    protected:
        beast::flat_buffer buffer_;
        bg_work& bgw_;
    public:
        // Construct the session
        http_session( beast::flat_buffer buffer, std::shared_ptr<std::string const> const& doc_root, bg_work&bgw)
            : doc_root_( doc_root )
            , queue_( *this )
            , buffer_( std::move( buffer ) )
            , bgw_{bgw}{
        }

        void do_read() {
            // Construct a new parser for each message
            parser_.emplace();

            // Apply a reasonable limit to the allowed size
            // of the body in bytes to prevent abuse.
            parser_->header_limit(1024*128);
            parser_->body_limit( 1024*128 );

            // Set the timeout.
            beast::get_lowest_layer( derived().stream() ).expires_after( std::chrono::seconds( 30 ) );

            // Read a request using the parser-oriented interface
            http::async_read( derived().stream(),buffer_, *parser_,
                              beast::bind_front_handler(
                                  &http_session::on_read,
                                  derived().shared_from_this()
                              )
                            );
        }

        void
        on_read( beast::error_code ec, std::size_t bytes_transferred ) {
            boost::ignore_unused( bytes_transferred );

            // This means they closed the connection
            if ( ec == http::error::end_of_stream  || ec == beast::error::timeout)
                return derived().do_eof();
                        
            return_on_error( ec, "http:read" )
            if(bgw_.server.auth.needed()) {
                string auth_token{parser_->get()[http::field::authorization]};//maybe avoid alloc, here require another .auth.valid signature
                if(!bgw_.server.auth.valid(auth_token)) {
                    handle_no_auth_request(parser_->release(),queue_);
                    if (!queue_.is_full()) do_read();
                    return;//bail out, the request did not pass through required auth
                }
            }
            // See if it is a WebSocket Upgrade
            if ( websocket::is_upgrade( parser_->get() ) ) {
                // Disable the timeout.
                // The websocket::stream uses its own timeout settings.
                beast::get_lowest_layer( derived().stream() ).expires_never();

                // Create a websocket session, transferring ownership
                // of both the socket and the HTTP request.
                return make_websocket_session( derived().release_stream(),parser_->release(),bgw_ );
            }

            // Send the response
            handle_request( *doc_root_, parser_->release(), queue_ );

            // If we aren't at the queue limit, try to pipeline another request
            if ( ! queue_.is_full() )
                do_read();
        }

        void
        on_write( bool close, beast::error_code ec, std::size_t bytes_transferred ) {
            boost::ignore_unused( bytes_transferred );

            return_on_error( ec, "write" );

            if ( close ) {
                // This means we should close the connection, usually because
                // the response indicated the "Connection: close" semantic.
                return derived().do_eof();
            }

            // Inform the queue that a write completed
            if ( queue_.on_write() ) {
                // Read another request
                do_read();
            }
        }
    };

    /** @brief plain http session handler
    *
    * This class uses the http_session as base class to implement
    * a plain http session. This class is only responsible for
    * keeping the tcp socket, timeout/termination. All real work
    * is done in the base class handler.
    *
    * @see http_session for detailed description
    * @tparam bg_work bacground worker type
    */
    template <class bg_work>
    class plain_http_session
        : public http_session<plain_http_session<bg_work>,bg_work>
        , public std::enable_shared_from_this<plain_http_session<bg_work>> {
        beast::tcp_stream stream_;
    public:
        using base=http_session<plain_http_session<bg_work>,bg_work>;
        // Create the session
        plain_http_session( beast::tcp_stream&& stream, beast::flat_buffer&& buffer,std::shared_ptr<std::string const> const& doc_root, bg_work& bgw )
            : base ( std::move( buffer ),doc_root,bgw)
            , stream_( std::move( stream ) ) {
        }

        // Start the session
        void run() {this->do_read();}

        // Called by the base class
        beast::tcp_stream& stream() { return stream_; }

        // Called by the base class
        beast::tcp_stream release_stream() { return std::move( stream_ );}

        // Called by the base class
        void do_eof() {
            beast::error_code ec;
            stream_.socket().shutdown( tcp::socket::shutdown_send, ec );
            // At this point the connection is closed gracefully
        }
    };

    /** @brief ssl http (https) session handler
    *
    * This class uses the http_session as base class to implement
    * a  https session. This class is only responsible for
    * keeping the tcp socket, timeout/termination. All real work
    * is done in the base class handler.
    *
    * @see http_session for detailed description
    * @tparam bg_work bacground worker type
    */

    template<class bg_work>
    class ssl_http_session
        : public http_session<ssl_http_session<bg_work>,bg_work>
        , public std::enable_shared_from_this<ssl_http_session<bg_work>> {
        beast::ssl_stream<beast::tcp_stream> stream_;

    public:
        using base=http_session<ssl_http_session<bg_work>,bg_work>;
        using base::buffer_,base::do_read;
        // Create the http_session
        ssl_http_session( beast::tcp_stream&& stream, ssl::context& ctx, beast::flat_buffer&& buffer, std::shared_ptr<std::string const> const& doc_root, bg_work& bgw )
            : base ( std::move( buffer ), doc_root,bgw )
            , stream_( std::move( stream ), ctx ) {
        }

        // Start the session
        void run() {
            beast::get_lowest_layer( stream_ ).expires_after( std::chrono::seconds( 30 ) );    // Set the timeout.
            // Perform the SSL handshake
            // Note, this is the buffered version of the handshake.
            stream_.async_handshake( ssl::stream_base::server,buffer_.data(),
                                     beast::bind_front_handler(
                                         &ssl_http_session::on_handshake,
                                         this->shared_from_this()
                                     )
                                   );
        }

        // Called by the base class
        beast::ssl_stream<beast::tcp_stream>& stream() { return stream_; }

        // Called by the base class
        beast::ssl_stream<beast::tcp_stream>  release_stream() { return std::move( stream_ );}

        // Called by the base class
        void do_eof() {
            beast::get_lowest_layer( stream_ ).expires_after( std::chrono::seconds( 30 ) );   // Set the timeout.
            stream_.async_shutdown(
                beast::bind_front_handler(
                    &ssl_http_session::on_shutdown, // Perform the SSL shutdown
                    this->shared_from_this()
                )
            );
        }

    private:
        void on_handshake( beast::error_code ec,std::size_t bytes_used ) {
            return_on_error( ec, "handshake.https" )
            // Consume the portion of the buffer used by the handshake
            buffer_.consume( bytes_used );
            do_read();
        }

        void on_shutdown( beast::error_code ec ) {
            return_on_error( ec, "shutdown.https" )
            // At this point the connection is closed gracefully
        }
    };

    /** @brief detect session, and forward it
    *
    * Detects SSL handshakes
    * and launches either a http-plain or https session object that
    * handle the request(s), doing the real work.
    * at that stage the socket_ ,ctx_, doc_root_ and buffer_
    * is handed over to that new session.
    * the strand_ is local, not forwarded, as it's seem to be
    * just a short-hand for socket_.get_executor(), ref. constructor.
    *
    * @tparam bg_work the background worker type containing io_context and server to do long running work
    */
    template<class bg_work>
    class detect_session : public std::enable_shared_from_this<detect_session<bg_work>> {
        beast::tcp_stream stream_;
        ssl::context& ctx_;
        std::shared_ptr<std::string const> doc_root_;
        beast::flat_buffer buffer_;
        bg_work& bgw_;
        bool tls_only_{false};
    public:
        explicit detect_session(
            tcp::socket&& socket,
            ssl::context& ctx,
            std::shared_ptr<std::string const> const& doc_root,
            bg_work& bgw,
            bool tls_only=false)
            : stream_( std::move( socket ) )
            , ctx_( ctx )
            , doc_root_( doc_root )
            , bgw_{bgw}
            , tls_only_{tls_only}{
        }

        // Launch the detector
        void run() {
            // Set the timeout.
            stream_.expires_after( std::chrono::seconds( 30 ) );

            beast::async_detect_ssl( stream_, buffer_,
                                     beast::bind_front_handler(
                                         &detect_session::on_detect,
                                         this->shared_from_this()
                                     )
                                   );
        }

        void
        on_detect( beast::error_code ec, boost::tribool result ) {
            return_on_error( ec, "detect" )

            if ( result ) { // Launch SSL session
                std::make_shared<ssl_http_session<bg_work>> ( std::move( stream_ ),ctx_,std::move( buffer_ ),doc_root_,bgw_ )->run();
            } else if (!tls_only_) { // Launch plain session, only if allowed
                std::make_shared<plain_http_session<bg_work>> ( std::move( stream_ ),std::move( buffer_ ),doc_root_,bgw_ )->run();
            } else {
                // we just goes silent here, otherwise, a massive set of packages will generate huge logs.
                // alternatively, we could also later add a counter for non tls attempts, to be monitoried.
                // ec = beast::error_code{int(boost::system::errc::errc_t::connection_refused),boost::system::system_category()};
                // return_on_error(ec,"plain session refused");
            }
        }
    };

    /** @brief the listener
    *
    *  Accepts incoming connections and launches the sessions.
    *  -That is, what it *really* does, is that it launces the
    *  detect_session that takes the work determining http or https
    *  before forwarding all the stash (socket_, ctx_ doc_root_)
    *  to either http or https handlers.
    *
    *  Since  work/sessions is originated from there, it needs to keep the
    *  bg_work class instance that is common for all work, so that it can
    *  forward that to the detectect-session (that forward it to ws-session or http-session when done)
    *
    *
    * @tparam bg_work the background worker class to do long running work, needed to create forwarders
    */
    template <class bg_work>
    struct listener : public std::enable_shared_from_this<listener<bg_work>> {
        using base=std::enable_shared_from_this<listener<bg_work>>;
        listener( net::io_context& ioc, ssl::context& ctx, tcp::endpoint endpoint, std::shared_ptr<std::string const> const& doc_root, bg_work& bgw ,bool tls_only=false)
            : ioc_( ioc )
            , ctx_( ctx )
            , acceptor_( net::make_strand( ioc ) )
            , doc_root_( doc_root )
            , bgw_{bgw}
            , tls_only_{tls_only}{
            beast::error_code ec;
            acceptor_.open( endpoint.protocol(), ec ); return_on_error( ec,"open" )
            acceptor_.set_option( net::socket_base::reuse_address( true ), ec ); return_on_error( ec,"set_option" )
            acceptor_.bind( endpoint, ec ); return_on_error( ec,"bind" )
            acceptor_.listen( net::socket_base::max_listen_connections, ec ); return_on_error( ec,"listen" )
        }

        // Start accepting incoming connections
        void run() {
            do_accept();
        }

    private:
        using base::shared_from_this;
        net::io_context& ioc_;
        ssl::context& ctx_;
        tcp::acceptor acceptor_;
        std::shared_ptr<std::string const> doc_root_;
        bg_work& bgw_;
        bool tls_only_{false};

        void
        do_accept() {
            // The new connection gets its own strand
            acceptor_.async_accept( net::make_strand( ioc_ ),
                beast::bind_front_handler(
                    &listener::on_accept,
                    shared_from_this()
                )
            );
        }

        void
        on_accept( beast::error_code ec, tcp::socket socket ) {
            if ( ec ) {
                fail( ec, "accept" );
            } else {
                // Create the detector http_session and run it
                std::make_shared<detect_session<bg_work>> (
                    std::move( socket ),
                    ctx_,
                    doc_root_,
                    bgw_, tls_only_
                )->run();
            }
            do_accept(); // Accept another connection
        }
    };
    #undef return_on_error
    // use extern template to minimize compile time, and control object file size
    extern template struct bg_worker<base_request_handler>;
    extern template class ssl_http_session< bg_worker<base_request_handler>>;
    extern template class plain_http_session< bg_worker<base_request_handler>>;
    extern template class detect_session<bg_worker<base_request_handler>>;
    extern template struct listener<bg_worker<base_request_handler>>;
}

