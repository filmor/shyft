
#include  <shyft/web_api/dtss_web_api.h>

//------------------------------------------------------------------------------
namespace shyft::web_api {
    
    int start_web_server(request_handler& bg_server, string address_s,unsigned short port,shared_ptr<string const> doc_root,int threads, int bg_threads, bool tls_only) {
        return run_web_server(bg_server, address_s,port,doc_root,threads,bg_threads, tls_only);
    }
    
}
