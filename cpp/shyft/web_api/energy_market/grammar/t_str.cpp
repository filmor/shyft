#include <shyft/web_api/energy_market/grammar.h>

namespace shyft::web_api::grammar {
    template<typename Iterator, typename Skipper>
    t_str_grammar<Iterator, Skipper>::t_str_grammar():
        t_str_grammar::base_type(start, "timestamped string") {
        start = lit("[") >> time_ >> "," >> str_ >> lit("]");
        on_error<fail>(start, error_handler(_4, _3, _2));
    }

    template struct t_str_grammar<request_iterator_em, request_skipper_em>;
}
