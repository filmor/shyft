
#include <shyft/web_api/energy_market/grammar/ts_url.h>

namespace shyft::web_api::grammar {

    inline tp_id mk_tp_id(char tp,int64_t id) noexcept {return tp_id{tp,id};}
    
    template<typename Iterator, typename Skipper>
    tp_id_grammar<Iterator, Skipper>::tp_id_grammar():
    tp_id_grammar::base_type(g_, "tp_id")
    {
        using boost::spirit::qi::no_skip;
        using boost::spirit::qi::long_long;
        //
        g_ =  (no_skip[char_("a-zA-Z")]>no_skip[long_long])[_val = phx::bind(mk_tp_id, _1, _2)];

        on_error<fail>(g_, error_handler(_4, _3, _2));
    }

    template<typename Iterator, typename Skipper>
    dstm_path_grammar<Iterator, Skipper>::dstm_path_grammar(path_resolver resolver):
    dstm_path_grammar::base_type(ts_, "dstm_ts_url"), resolver{resolver}
    {
        mid_ = lexeme[+(char_- "/")];
        attr_id_ = lexeme[+(char_)];
        ts_ = (lit("dstm://M") > mid_          // starts with prefix and model id
                > '/' > ( tp_id_ % lit('/') ) // followed by / and at least one T id sep with /
                > '.' >> attr_id_              // followed by a dot and attrid.
               ) [_val = phx::bind(resolver, _1, _2, _3)];

        on_error<fail>(ts_, error_handler(_4, _3, _2));
    }

    template<typename Iterator, typename Skipper>
    stm_url_grammar<Iterator, Skipper>::stm_url_grammar():
      stm_url_grammar::base_type(url_, "stm_url")
    {
        mid_ = lexeme[+(char_- "/")];
        attr_id_ = lexeme[+(char_)];
        url_ = (lit("dstm://M") > mid_          // starts with prefix and model id
                > '/' > ( tp_id_ % lit('/') ) // followed by / and at least one T id sep with /
                > '.' >> attr_id_              // followed by a dot and attrid.
          );

        on_error<fail>(url_, error_handler(_4, _3, _2));
    }

    template struct stm_url_grammar<request_iterator_t, request_skipper_t>;
    template struct dstm_path_grammar<request_iterator_t, request_skipper_t>;
    template struct tp_id_grammar<request_iterator_t, request_skipper_t>;
    
}
