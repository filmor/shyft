#pragma once
#include <cstdint>
namespace shyft::web_api::grammar {

    /**
     * @brief simple type to hold type coded as char and its id, like U123 for unit
     */
    struct tp_id {
        int tp{0}; ///< any int will do here
        std::int64_t id{0};///< the id could be 64bits, and ms c++ is 32bits on int, need to be specific

        auto operator<=>(const tp_id &) const = default;
    };

}
