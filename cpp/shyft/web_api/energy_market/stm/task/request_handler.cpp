#include <shyft/web_api/energy_market/stm/task/request_handler.h>
#include <shyft/web_api/energy_market/srv/generators.h>
#include <shyft/energy_market/stm/srv/task/stm_task.h>

namespace shyft::web_api::energy_market::stm::task {

    using shyft::energy_market::stm::srv::stm_case_;
    using shyft::energy_market::stm::srv::model_ref;
    using shyft::energy_market::stm::srv::model_ref_;
    using shyft::web_api::generator::emit;
    namespace ka = boost::spirit::karma;

    bool request_handler::handle_request(const request& req, bg_work_result& resp) {
        if(!super::handle_request(req, resp)) {
            // Handling of additional request types goes here.
            if (req.keyword == "add_case") {
                return handle_add_case_request(req.request_data, resp);
            } else if (req.keyword == "remove_case") {
                return handle_remove_case_request(req.request_data, resp);
            } else if (req.keyword == "get_case") {
                return handle_get_case_request(req.request_data, resp);
            } else if (req.keyword == "update_case") {
                return handle_update_case_request(req.request_data, resp);
            } else if (req.keyword == "add_model_ref") {
                return handle_add_model_ref_request(req.request_data, resp);
            } else if (req.keyword == "remove_model_ref") {
                return handle_remove_model_ref_request(req.request_data, resp);
            } else if (req.keyword == "get_model_ref") {
                return handle_get_model_ref_request(req.request_data, resp);
            } else if (req.keyword == "fx") {
                return handle_fx_request(req.request_data,resp);
            }
            return false;
        }
        return true;
    }
    
    bool request_handler::handle_fx_request(const json& data, bg_work_result& resp) {
        auto req_id = data.required<string>("request_id");
        auto mid = data.required<int>("model_id");
        auto args = data.required<string>("args");
        auto result=srv->do_fx(mid,args);
        string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
        auto sink = std::back_inserter(response);
        {
            ka::generate(sink, ka::bool_, result);
        }
        *sink++ = '}';
        resp.copy_response(response);
        return true;
    }

    bool request_handler::handle_add_case_request(const json& data, bg_work_result& resp) {
        auto req_id = data.required<string>("request_id");
        auto mid = data.required<int>("mid");
        auto cse = std::make_shared<stm_case>(data.required<stm_case>("case"));
        // Store cse in session:
        auto task = srv->db.read_model(mid);
        task->add_case(cse);
        model_info mi;
        if (!srv->db.try_get_info_item(mid, mi)) {
            mi = model_info(task->id, task->name, task->created, task->json);
        }

        // As a result, we generate whether we can find the run in the provided session.
        srv->db.store_model(task, mi);
        task = srv->db.read_model(mid);
        bool result = std::any_of(task->cases.begin(), task->cases.end(),
            [&cse](auto const& r) {
                if (!cse && r) return true;
                if (!cse || !r) return false;
                return *cse == *r;
            }
        );
        // Generate response:
        string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
        auto sink = std::back_inserter(response);
        {
            ka::generate(sink, ka::bool_, result);
        }
        *sink++ = '}';
        resp.copy_response(response);
        return true;
    }

    bool request_handler::handle_remove_case_request(const json& data, bg_work_result& resp) {
        auto req_id = data.required<string>("request_id");
        auto mid = data.required<int>("mid");
        auto cid = data.optional<int>("cid");
        auto cname = data.optional<string>("cname");

        bool result = false;
        auto session = srv->db.read_model(mid);
        // Attempt to remove run:
        if (cid) {
            result = session->remove_case(*cid);
        } else if (cname) {
            result = session->remove_case(*cname);
        }
        // Update server is run was removed:
        if (result) {
            model_info mi;
            if (!srv->db.try_get_info_item(mid, mi)) {
                mi = model_info(session->id, session->name, session->created, session->json);
            }
            srv->db.store_model(session, mi);
            // TODO: Notify change to subscription manager:
        }

        // Generate response:
        string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
        auto sink = std::back_inserter(response);
        {
            ka::generate(sink, ka::bool_, result);
        }
        *sink++ = '}';
        resp.copy_response(response);
        return true;
    }

    bool request_handler::handle_get_case_request(const json& data, bg_work_result& resp) {
        auto req_id = data.required<string>("request_id");
        auto mid = data.required<int>("mid");
        auto cid = data.optional<int>("cid");
        auto cname = data.optional<string>("cname");

        stm_case_ result = nullptr;
        // Get result:
        auto session = srv->db.read_model(mid);
        if (cid) {
            result = session->get_case(*cid);
        } else if (cname) {
            result = session->get_case(*cname);
        }
        // Generate response:
        string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
        auto sink = std::back_inserter(response);
        {
            emit(sink, result);
        }
        *sink++ = '}';
        resp.copy_response(response);
        return true;
    }

    bool request_handler:: handle_update_case_request(const json& data, bg_work_result& resp) {
        auto req_id = data.required<string>("request_id");
        auto mid = data.required<int>("mid");
        auto cse = std::make_shared<stm_case>(data.required<stm_case>("case"));

        auto session = srv->db.read_model(mid);
        auto success = session->update_case(*cse);
        if(success) {
            model_info mi;
            if (!srv->db.try_get_info_item(mid, mi)) {
                mi = model_info(session->id, session->name, session->created, session->json);
            }
            srv->db.store_model(session, mi);

            // Generate response:
            string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
            auto sink = std::back_inserter(response);
            {
                emit(sink, success);
            }
            *sink++ = '}';
            resp.copy_response(response);
            return true;
        }
        return false;
    }


    bool request_handler::handle_add_model_ref_request(const json& data, bg_work_result& resp) {
        // Retrieve request data:
        auto req_id = data.required<string>("request_id");
        auto mid = data.required<int>("mid");
        auto cid = data.required<int>("cid");
        auto mr = std::make_shared<model_ref>(data.required<model_ref>("model_ref"));

        bool result = false;
        auto session = srv->db.read_model(mid);
        auto cse = session->get_case(cid);
        if (cse) {
            auto presize = cse->model_refs.size();
            cse->add_model_ref(mr);
            if (cse->model_refs.size() > presize) {
                result = true;
            }
        }
        // Update session on server if model reference was added successfully.
        if (result) {
            model_info mi;
            if (!srv->db.try_get_info_item(mid, mi)) {
                mi = model_info(session->id, session->name, session->created, session->json);
            }
            srv->db.store_model(session, mi);
        }
        // Generate response:
        string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
        auto sink = std::back_inserter(response);
        {
            ka::generate(sink, ka::bool_, result);
        }
        *sink++ = '}';
        resp.copy_response(response);
        return true;
    }

    bool request_handler::handle_remove_model_ref_request(const json& data, bg_work_result& resp) {
        // retrieve request data
        auto req_id = data.required<string>("request_id");
        auto mid = data.required<int>("mid");
        auto cid = data.required<int>("cid");
        auto mkey = data.required<string>("model_key");

        // Do server action:
        bool result = false;
        auto session = srv->db.read_model(mid);
        auto cse = session->get_case(cid);
        if (cse) {
            result = cse->remove_model_ref(mkey);
        }

        // Update session on server if model reference was successfully removed
        if (result) {
            model_info mi;
            if (!srv->db.try_get_info_item(mid, mi)) {
                mi = model_info(session->id, session->name, session->created, session->json);
            }
            srv->db.store_model(session, mi);
        }

        // Generate response:
        string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
        auto sink = std::back_inserter(response);
        {
            ka::generate(sink, ka::bool_, result);
        }
        *sink++ = '}';
        resp.copy_response(response);
        return true;
    }

    bool request_handler::handle_get_model_ref_request(const json& data, bg_work_result& resp) {
        // Retrieve request data:
        auto req_id = data.required<string>("request_id");
        auto mid = data.required<int>("mid");
        auto cid = data.required<int>("cid");
        auto mkey = data.required<string>("model_key");

        // Do server action:
        model_ref_ mr = nullptr;
        auto session = srv->db.read_model(mid);
        auto cse = session->get_case(cid);
        if (cse) {
            mr = cse->get_model_ref(mkey);
        }

        // Generate response:
        string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
        auto sink = std::back_inserter(response);
        {
            emit(sink, mr);
        }
        *sink++ = '}';
        resp.copy_response(response);
        return true;
    }
}
