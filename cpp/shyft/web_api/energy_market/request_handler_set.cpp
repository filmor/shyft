#include <shyft/energy_market/a_wrap.h>
#include <shyft/energy_market/constraints.h>
#include <shyft/energy_market/stm/context.h>
#include <shyft/energy_market/stm/srv/dstm/dstm_subscription.h>
#include <shyft/energy_market/stm/srv/dstm/ts_magic_merge.h>
#include <shyft/mp.h>
#include <shyft/web_api/energy_market/generators.h>
#include <shyft/web_api/energy_market/generators/hydro_power.h>
#include <shyft/web_api/energy_market/grammar.h>
#include <shyft/web_api/energy_market/srv/generators.h>
#include <shyft/web_api/generators/json_struct.h>

namespace shyft::web_api::energy_market {
    using namespace shyft::web_api::generator;
    namespace mp = shyft::mp;
    namespace hana = boost::hana;

    using shyft::srv::model_info;
    using shyft::energy_market::stm::stm_hps;
    using shyft::energy_market::stm::reservoir;
    using shyft::energy_market::stm::power_plant;
    using shyft::energy_market::stm::waterway;
    using shyft::energy_market::stm::unit;
    using shyft::energy_market::stm::unit_group;
    using shyft::energy_market::stm::unit_group_member;
    using shyft::energy_market::stm::contract;
    using shyft::energy_market::stm::contract_relation;
    using shyft::energy_market::stm::transmission_line;
    using shyft::energy_market::stm::busbar;
    using shyft::energy_market::stm::power_module_member;
    using shyft::energy_market::stm::unit_member;
    using shyft::energy_market::stm::energy_market_area;
    using shyft::energy_market::stm::catchment;
    using shyft::energy_market::stm::stm_system;
    using shyft::energy_market::stm::run_parameters;

    using shyft::energy_market::core::constraint_base;
    using shyft::energy_market::core::absolute_constraint;
    using shyft::energy_market::core::penalty_constraint;
    using shyft::energy_market::stm::subscription::proxy_attr_observer;
    using shyft::energy_market::stm::subscription::proxy_attr_observer_;
    using shyft::energy_market::stm::srv::dstm::ts_merge_result;
    using shyft::energy_market::stm::srv::dstm::ts_magic_merge_values;
    using shyft::web_api::grammar::phrase_parser;

    using shyft::time_series::dd::aref_ts;
    using shyft::time_series::dd::gpoint_ts;
    using shyft::time_series::dd::abin_op_ts;
    using shyft::time_series::dd::ts_as;
    using shyft::energy_market::attr_traits::exists;
     
    /** @brief Helper function for disambiguating which array to put
     * subscription in, based on value type
     * @tparam T1
     * @tparam Ts
     * @return
     */
    template<class T1, class... Ts>
    constexpr bool is_one_of() noexcept {
        return (std::is_same_v<T1, Ts> || ...);
    }


    /** @brief handler class for setting attributes on a model.
     *
     */
    class set_attribute_handler {
            // Attributes:
            server* const srv; ///<Pointer to the server we need access to to handle time series correctly
            map<string, vector<string>> subs;///<Keep track of what attributes have been changed
            bool merge; ///<Whether to merge values by the "shotgun method", i.e. keep all values that don't directly overlap.
            bool recreate;///<Remove the old value entirely
            bool cache_on_write;///< if true(default), also cache shyft-time-series.
            string mkey;///<Model key that will need to be prefixed to all subscription updates.
        public:
            set_attribute_handler(server* const srv, bool merge=false, bool recreate=false, const string& mid="", bool cache_on_write=true)
                : srv{srv}, merge{merge}, recreate{recreate},cache_on_write{cache_on_write}, mkey{mid}
            {
                subs["time-series"] = {};
                subs["other"] = {};
            }

            /** @brief Need a override for attribute_value_type-variant, to safely unpack:
             *
             */
            template<class V>
            string apply(V& old_val, attribute_value_type const& new_val) {
                auto f = [this,&old_val](auto arg) { return this->apply(old_val, arg); };
                return boost::apply_visitor(f, new_val);
            }

            // Apply new_val to old_val based on the state of this, i.e. the values of merge, recreate and the server
            // to communicate with.
            template<class V>
            string apply(V& old_val, V const& new_val) {
                 // Update with new values:
                string res;
                if (exists(old_val)) {
                    res = merge_values(old_val, new_val);
                } else {
                    old_val = new_val;
                    res = "OK";
                }
                return res;
            }

            // string apply(uint16_t& old_val, int const& new_val) {
            //     return apply(old_val, static_cast<uint16_t>(new_val));
            // }

            template<class V, class U>
            string apply(V& /*old_val*/, U const& /*new_val*/) {
                return "type mismatch";
            }

            // Notify accrued changes to the subscription managers:
            void notify_changes() {
                // Propagate/notify changes due to write of attributes (e.g. time-series)
                if (srv->sm) srv->sm->notify_change(subs["other"]);
                srv->dtss->sm->notify_change(subs["time_series"]);// notify the dtss sm about time-series changes
            }

            template <class T>
            string make_subscription_url(T const&t, std::string_view attr_id) const {
                string sub_id ="dstm://M" + mkey;
                sub_id.reserve(30);
                auto rbi = std::back_inserter(sub_id);
                t.generate_url(rbi);
                *rbi++ = '.';
                sub_id+=attr_id;
                return sub_id;
            }

            template<class T>
            void add_tsm_subscription(T const&t,std::string_view attr_id) {
                subs["time_series"].push_back(make_subscription_url(t,attr_id));
            }

            template <class Struct, class LeafAccessor>
            void add_sub_id(Struct const& t, LeafAccessor&& la) {
                auto sub_id=make_subscription_url(t, mp::leaf_accessor_id_str(la));
                // Get value type of leaf_accessor:
                //auto value_type = mp::leaf_accessor_type(la);
                using V = typename decltype(+mp::leaf_accessor_type(std::declval<LeafAccessor>()))::type;
                // Send to correct subscription manager based on type:
                if constexpr (is_one_of<V, apoint_ts, absolute_constraint, penalty_constraint>()) {
                    subs["time_series"].push_back(sub_id);
                } else {
                    subs["other"].push_back(sub_id);
                }
            }

    private:
        string merge_values(uint16_t &old_v, const  uint16_t new_v) {
            old_v=new_v;// just assign it.
            return "OK";
        }
        string merge_values(apoint_ts& old_val, const apoint_ts& new_val) {
            if(!srv)
                return "Cannot set time series without dstm server.";
            switch(ts_magic_merge_values(srv->dtss,old_val,new_val,merge,recreate,cache_on_write) ) {
                case ts_merge_result::saved_to_dtss:return "stored to dtss";
                case ts_merge_result::fail_expression:return "Time series is an expression. Cannot be set.";
                case ts_merge_result::fail_dtss: return "Cannot set dtss time series without dtss.";
                default:
                case ts_merge_result::merged: return "OK";
            }

        }

        string merge_values(absolute_constraint& old_val, const absolute_constraint& new_val) {
            merge_values(old_val.limit, new_val.limit);
            merge_values(old_val.flag, old_val.flag);
            return "OK";
        }

        string merge_values(penalty_constraint& old_val, const penalty_constraint& new_val) {
            merge_values(old_val.limit, new_val.limit);
            merge_values(old_val.flag, new_val.flag);
            merge_values(old_val.cost, new_val.cost);
            merge_values(old_val.penalty, new_val.penalty);
            return "OK";
        }


        template <class T>
        string merge_values(shared_ptr<map<utctime, shared_ptr<T>>>& old_val, const shared_ptr<map<utctime, shared_ptr<T>>>& new_val) {
            // We want to overwrite, so insert doesn't do the trick:
            if (!recreate || merge) {
                for (auto const &kv : *new_val) {
                    (*old_val)[kv.first] = kv.second;
                }
            } else {
                old_val = new_val;
            }
            return "OK";
        }
        string merge_values(string& old_val, string const & new_val) {
            old_val = new_val;
            return "OK";
        }
        template <class T>
        string merge_values(T& old_val, T const & new_val) {
            old_val = new_val;
            return "NOT OK";
        }
    };

    auto get_attr_struct(vector<json> const& attr_vals, string const& attr_id) {
        return std::find_if(attr_vals.begin(), attr_vals.end(),
            [&attr_id](json const& element) -> bool {
                // Here we check if the json structure e contains the key "attribute_id",
                // and if so, if it compares equal to the provided attr_id.
                auto opt = element.optional<string>("attribute_id"); // Get compiler error: "invalid use of 'class std::optional<std::__cxx11::basic_string<char>>" if not cast
                if (opt) {
                    return *opt == attr_id;
                } else {
                    return false;
                }
            }
        );
    }

    template<class V>
    pair<bool, string> check_attribute_rules(const std::string& attribute_id) {
        if constexpr (std::is_same_v<V,shyft::energy_market::stm::contract_relation>) {
            if (attribute_id == "relation_type") {
                return {false, "immutable attribute"};
            }
        }
        return {true, ""};
    }

    /** @brief
     *
     * @tparam T : Type whose attributes we want to change. Assumed to be a hana-struct
     * @param t : The instance of type T whose attribute values we want to change
     * @param attr_vals : The values to set or merge in t's attributes. Each entry of the vector requires the
     *   following:
     *      * "attribute_id" : <string> (ID of the attribute to change)
     *      * "value" : The value to be inserted/merged in.
     * @return a list of json. Each entry is the attribute_id that has been processed together with a "status",
     *  stating whether the change was successful or if it  encountered an error.
     */
    template<class T>
    vector<json> set_attribute_values(T& t, vector<json> const& attr_vals, set_attribute_handler& handler) {
        // Initialize result:
        vector<json> result;
        result.reserve(attr_vals.size());

        // Go through each attribute:
        constexpr auto attr_paths = mp::leaf_accessors(hana::type_c<T>);
        hana::for_each(attr_paths,
            [&t, &attr_vals, &result, &handler](auto m) {
                auto attr_id = string(mp::leaf_accessor_id_str(m));
                auto it = get_attr_struct(attr_vals, attr_id);
                if (it != attr_vals.end()) { // If we found the current attribute amongst the ones to change
                    // Initialize structure to be inserted into result
                    json attr_res;
                    attr_res["attribute_id"] = attr_id;
                    // Get out value to be inserted/merged:
                    auto value = it->required("value"); // value is now of type value_type -- a boost::variant
                    // Set value
                    auto f = [&handler, &t, &m, &attr_id](auto arg) {
                        if (const auto [ok, status] = check_attribute_rules<T>(attr_id); !ok)
                            return status;

                        auto& attr = mp::leaf_access(t,m);
                        return handler.apply(attr, arg);
                    };
                    // Alternatively, we could have used hana::partial like:
                    // auto f = hana::partial([&handler](auto& attr, auto arg) { return handler.apply(attr, arg); }, mp::leaf_access(t,m));
                    attr_res["status"] = boost::apply_visitor(f, value);
                    // Add subscription ID to notify change later:
                    handler.add_sub_id(t, m);
                    // Insert into result vector:
                    result.push_back(attr_res);
                }
            }
        );
        if constexpr (!std::is_same_v<T,shyft::energy_market::stm::unit_group_member> &&
                      !std::is_same_v<T,shyft::energy_market::stm::unit_member> &&
                      !std::is_same_v<T,shyft::energy_market::stm::power_module_member> &&
                      !std::is_same_v<T,shyft::energy_market::stm::contract_relation>) {
            for(auto const& a:attr_vals) {
                auto attr_id=a.required<string>("attribute_id");
                if(attr_id.rfind("ts.",0)==0) {
                    auto value=a.required<attribute_value_type>("value");
                    json attr_res;
                    attr_res["attribute_id"] = attr_id;
                    auto tsi= t.tsm.find(attr_id.substr(3));
                    if(tsi==t.tsm.end()) {
                        attr_res["status"]="not found";
                    } else {
                        attr_res["status"] = handler.apply(tsi->second,value);
                        handler.add_tsm_subscription(t,attr_id);// could work.
                    }
                    result.push_back(attr_res);
                } else if (attr_id == "json" ) {
                    auto value=a.required<string>("value");
                    json attr_res;
                    attr_res["attribute_id"] = attr_id;
                    attr_res["status"] = handler.apply(t.json,value);                    
                    result.push_back(attr_res);
                }// else alraedy handled
            }
        }
        return result;
    }
    
    struct check_empty_visitor : public boost::static_visitor<bool> {
        template<typename T>
        bool operator()(const vector<T>& t) const {
            return t.size() == 0;
        }
        
        template<typename T>
        bool operator()(const T& ) const {
            return true;
        }
    };
    /** @brief Sets a list of proxy attributes from a list of T specified by data.
     *
     * @tparam T container type for proxy attributes (reservoir, unit, waterway &c.)
     * @tparam Tc container type of base type (So we can do dynamic casting)
     *      This template parameters is required in this solution due to invariation,
     *      i.e. even if T extends Tc, vector<T> DOES NOT extend vector<Tc>
     * @tparam Fx a callable(int,ptr_type const&)->bool used to locate the wanted component
     * 
     * @param srv ref to server so that we can notify on subscription (server.sm subscription manager)
     * @param vecvals the vector with a series of references to instances of T.
     * @param data: JSON struct specifying which components, and which attributes to retrieve
     *      Requires keys:
     *          @key component_ids: vector<int> of IDs of which containers to retrieve from.
     *          @key attribute_ids: vector<int> of IDs of which attributes to retrieve for each container.
     *          @key values: vector<attribute_value_type> of values to set for each component and attribute
     *              Must have length equal to |component_ids| * |attribute_ids|.
     * @param handler: the handler we use to record/repeat the request (incase subscribe)
     * @param fx: The callable(int cid,ptr_type const&c)->bool, that should return true if cid matches id of comp
     * @return vector with json
     */
    template<class T, class Tc,class Fx>
    vector<json> set_attribute_values_vector_fx(vector<shared_ptr<Tc>> const& vecvals, vector<json> const& data, set_attribute_handler& handler, Fx&& fx){
        vector<json> result;

        shared_ptr<T> comp;
        // Iterate over each element of the json-list:
        for (auto &comp_data : data) {
            // Each element of the vector is required to have:
            // int component_id
            // vector<json> attribute_data
            auto cid = comp_data.required<int>("component_id");
            auto temp = comp_data.required("attribute_data"); // This pattern is required because an empty list would not parse successfully to vector<json>
            vector<json> attr_vals;
            if (!boost::apply_visitor(check_empty_visitor(), temp)) {
                attr_vals = comp_data.required<vector<json>>("attribute_data");
            }
            // Initialize json to store results for current component:
            json comp_struct;
            comp_struct["component_id"] = cid;
            // Get out component:
            auto it = find_if(vecvals.begin(), vecvals.end(), [cid,&fx](auto e) { return fx(cid,e); });
            if (it != vecvals.end()) {
                // We need to downcast the component because of inheritance.
                if ( (comp = dynamic_pointer_cast<T>(*it)) ) {
                    comp_struct["status"] = set_attribute_values(*comp, attr_vals, handler); // set_attribute_values(comp, attr_vals....)
                } else {
                    comp_struct["status"] = "Unable to cast hydro component";
                }
            } else {
                comp_struct["status"] = "Unable to find component";
            }
            // Add component's json to result list:
            result.push_back(comp_struct);
        }
        return result;
    }

    /** ref set_attribute_values_vector_fx, this one with a ready-shipped fx that compares to c->id */
    template<class T, class Tc>
    vector<json> set_attribute_values_vector(vector<shared_ptr<Tc>> const& vecvals, vector<json> const& data, set_attribute_handler& handler){
        return set_attribute_values_vector_fx<T,Tc>(vecvals,data,handler,[](int cid,shared_ptr<Tc>const&c){return cid==c->id;});
    }    
        
    bg_work_result request_handler::handle_set_attribute_request(const json &data) {
        // Get model and HPS:
        auto mid = data.required<string>("model_key");

        return srv->models.mutate_or_throw(
          mid,
          [&](auto &&view){
            auto mdl = view.model;

            // Prepare response:
            auto req_id = data.required<string>("request_id");
            std::string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");

            auto merge = boost::get_optional_value_or(data.optional<bool>("merge"), false);
            auto recreate = boost::get_optional_value_or(data.optional<bool>("recreate"), false);
            auto cache_on_write=boost::get_optional_value_or(data.optional<bool>("cache_on_write"), true);
            // Set up set_attribute_handler;
            set_attribute_handler handler(srv, merge, recreate, mid,cache_on_write);

            auto sink = std::back_inserter(response);
            //---- EMIT DATA: ----//
            {

              emit_object<decltype(sink)> oo(sink);
              oo.def("model_key", mid);
              auto n_sections=0u;
              // some tools to auto roll out the types:
              using hana::type_c;
              /**
               * oo_def checks if the json data have the specified key,
               * then if it is there, get the specs from it, and apply them to
               * the corresponding energy market model object.
               */
              auto oo_def=[&n_sections,&handler](auto &oo,json const&data, std::string const& name,auto const& dv, auto tp) {
                using d_type= typename decltype(tp)::type;//std::remove_cvref_t<decltype(dv)>::value_type::element_type;
                auto j_data=data.template optional<vector<json>>(name); // check if there is a json section with the given name
                if(j_data && (*j_data).size()) {// if yes, then
                  ++n_sections;//just count so we keep track
                  oo.def(name,set_attribute_values_vector<d_type>(dv,*j_data,handler)); // does all needed magic rendering json
                }
              };
              /// short hand when the type to process is the same as vector type (e.g.stm only)
              auto oo_defx=[&oo_def](auto & oo, auto const&data, std::string const& name,auto const& dv) {
                using d_type=typename std::remove_cvref_t<decltype(dv)>::value_type::element_type;// pull out the type from vector<shared_ptr<T>>
                return oo_def(oo,data,name,dv,type_c<d_type>);
              };

              auto hps_data=data.optional<vector<json>>("hps");
              if(hps_data && (*hps_data).size()) {
                n_sections++;
                oo.def_fx("hps",[&mdl,mid,&hps_data,&oo_def](auto sink) {
                  emit_vector_fx(sink,*hps_data,[&mdl,mid,&oo_def](auto oi,json const&hpsd) {
                    auto hps_id = hpsd.required<int>("hps_id");
                    auto it = std::find_if(mdl->hps.begin(), mdl->hps.end(), [&hps_id](auto ihps) { return ihps->id == hps_id; });
                    if (it == mdl->hps.end()) {
                      throw runtime_error( string("Uanble to find HPS ") + std::to_string(hps_id) + string(" in model '") + mid + "'");
                    }
                    auto hps = *it;
                    emit_object<decltype(oi)> oo(oi);
                    oo.def("hps_id", hps->id);
                    oo_def(oo,hpsd,"reservoirs"  , hps->reservoirs,type_c<reservoir>);
                    oo_def(oo,hpsd,"units"       , hps->units,type_c<unit>);
                    oo_def(oo,hpsd,"power_plants", hps->power_plants,type_c<power_plant>);
                    oo_def(oo,hpsd,"waterways"   , hps->waterways,type_c<waterway>);
                    auto gts=hps->gates();// needed as a instance here, inline in next line will fail(would require by value, move, etc.
                    oo_def(oo,hpsd,"gates"       , gts,type_c<gate>);
                    oo_def(oo,hpsd,"catchments", hps->catchments, type_c<catchment>);
                  });
                });
              }


              oo_defx(oo,data,"markets",mdl->market);
            
              auto contract_data=data.optional<vector<json>>("contracts");
              if(contract_data && (*contract_data).size()) {
                n_sections++;
                oo.def_fx("contracts",[&handler,&mdl,mid,&contract_data](auto sink) {
                  emit_vector_fx(sink,*contract_data,[&handler,&mdl,mid](auto oi,json const& cd) {
                    auto contract_id = cd.required<int>("component_id");
                    emit_object<decltype(oi)> oo(oi);
                    oo.def("component_id", contract_id); // we can tell which component it is,
                    auto it = std::find_if(mdl->contracts.begin(), mdl->contracts.end(), [&contract_id](auto c) { return c->id == contract_id; });
                    if (it == mdl->contracts.end()) {
                      oo.def("status","Unable to find component");// and we did not find it, just a message, no throw
                    } else {
                      auto contract = *it;
                      // set contract attributes here
                      auto temp = cd.required("attribute_data"); // This pattern is required because an empty list would not parse successfully to vector<json>
                      vector<json> attr_vals;
                      if (!boost::apply_visitor(check_empty_visitor(), temp)) {
                        attr_vals = cd.required<vector<json>>("attribute_data");
                      }

                      oo.def("status",set_attribute_values(*contract, attr_vals, handler)); // set_attribute_values(comp, attr_vals....)
                      // Set contract relation attributes
                      if (auto comp_attrs = cd.optional<vector<json>>("relations"); comp_attrs) {
                        oo.def("relations", set_attribute_values_vector_fx<contract_relation>(
                                 contract->relations, *comp_attrs, handler,
                                 [](int id,shared_ptr<contract_relation> const&cr){return cr->id==id;}));
                      }
                      if (auto comp_attrs = cd.optional<vector<json>>("power_plants"); comp_attrs) {
                        oo.def("power_plants", set_attribute_values_vector_fx<power_plant>(
                                 contract->power_plants, *comp_attrs, handler,
                                 [](int id,shared_ptr<power_plant> const&pp){return pp->id==id;}));
                      }
                    }
                  });
                });
              }


              oo_defx(oo,data,"contract_portfolios",mdl->contract_portfolios);
              oo_defx(oo,data,"power_modules",mdl->power_modules);

              auto network_data=data.optional<vector<json>>("networks"); // check if there is a json section with the given name
              if (network_data && (*network_data).size()) {
                ++n_sections;//just count so we keep track
                oo.def_fx("networks",[&handler,&mdl,mid,&network_data](auto sink) {
                  emit_vector_fx(sink,*network_data,[&handler,&mdl,mid](auto oi, json const& networkd) {
                    auto network_id = networkd.required<int>("component_id");
                    auto it = std::find_if(mdl->networks.begin(), mdl->networks.end(),[&network_id](auto inet) { return inet->id == network_id; });
                    if (it == mdl->networks.end()) {
                      throw runtime_error( string("Unable to find Network ") + std::to_string(network_id) + string(" in model '") + mid + "'");
                    }
                    auto network = *it;
                    emit_object<decltype(oi)> oo(oi);
                    oo.def("component_id", network->id);
                    if (auto tl_attrs = networkd.optional<vector<json>>("transmission_lines"); tl_attrs) {
                      oo.def("transmission_lines", set_attribute_values_vector_fx<transmission_line>(network->transmission_lines, *tl_attrs, handler,
                                                                                                     [](int tid,shared_ptr<transmission_line> const&tl){return tid==tl->id;}));
                    }
                    if (auto busbar_data = networkd.optional<vector<json>>("busbars"); busbar_data && (*busbar_data).size() > 0) {
                      oo.def_fx("busbars",[&handler, &network, network_id, &busbar_data](auto sink) {
                        emit_vector_fx(sink, *busbar_data,[&handler, &network, network_id](auto oi, json const& busbard) {
                          auto busbar_id = busbard.required<int>("component_id");
                          auto it = std::find_if(network->busbars.begin(), network->busbars.end(),[&busbar_id](auto ibus) { return ibus->id == busbar_id; });
                          if (it == network->busbars.end()) {
                            throw runtime_error( string("Unable to find Busbar ") + std::to_string(busbar_id) + string(" in network '") + std::to_string(network_id) + "'");
                          }
                          auto busbar = *it;
                          auto temp = busbard.required("attribute_data"); // This pattern is required because an empty list would not parse successfully to vector<json>
                          vector<json> attr_vals;
                          if (!boost::apply_visitor(check_empty_visitor(), temp)) {
                            attr_vals = busbard.required<vector<json>>("attribute_data");
                          }
                          emit_object<decltype(oi)> oo(oi);
                          oo.def("status",set_attribute_values(*busbar, attr_vals, handler)); // set_attribute_values(comp, attr_vals....)

                          if (auto unit_members = busbard.optional<vector<json>>("units"); unit_members) {
                            oo.def("units", set_attribute_values_vector_fx<unit_member>(
                                     busbar->units, *unit_members, handler,
                                     [](int cid,shared_ptr<unit_member> const&um){return um->unit->id==cid;}
                                     ));
                          }
                          if (auto power_module_members = busbard.optional<vector<json>>("power_modules"); power_module_members) {
                            oo.def("power_modules", set_attribute_values_vector_fx<power_module_member>(
                                     busbar->power_modules, *power_module_members, handler,
                                     [](int cid,shared_ptr<power_module_member> const&pm){return pm->power_module->id==cid;}
                                     ));
                          }
                        });
                      });
                    }
                  });
                });
              }

              auto ug_data=data.optional<vector<json>>("unit_groups");
              if(ug_data && (*ug_data).size()) {
                n_sections++;
                oo.def_fx("unit_groups",[&handler,&mdl,mid,&ug_data](auto sink) {
                  emit_vector_fx(sink,*ug_data,[&handler,&mdl,mid](auto oi,json const& ugd) {
                    auto ug_id = ugd.required<int>("component_id");
                    emit_object<decltype(oi)> oo(oi);
                    oo.def("component_id", ug_id); // we can tell which component it is,

                    auto it = std::find_if(mdl->unit_groups.begin(), mdl->unit_groups.end(), [&ug_id](auto iug) { return iug->id == ug_id; });
                    if (it == mdl->unit_groups.end()) {
                      oo.def("status","Unable to find component");// and we did not find it, just a message, no throw
                    } else {
                      auto ug = *it;
                      // set unit_group attributes here
                      auto temp = ugd.required("attribute_data"); // This pattern is required because an empty list would not parse successfully to vector<json>
                      vector<json> attr_vals;
                      if (!boost::apply_visitor(check_empty_visitor(), temp)) {
                        attr_vals = ugd.required<vector<json>>("attribute_data");
                      }
                      oo.def("status",set_attribute_values(*ug, attr_vals, handler)); // set_attribute_values(comp, attr_vals....)
                      // Set unit-group member attributes(is_active, and we could consider unit attribute propagation) 
                      auto comp_attrs = ugd.optional<vector<json>>("members");
                      if (comp_attrs) oo.def("members",
                                             set_attribute_values_vector_fx<unit_group_member>(
                                               ug->members, *comp_attrs, handler,
                                               [](int cid,shared_ptr<unit_group_member> const&ugm){return ugm->unit->id==cid;}
                                               )
                        );
                    }
                  });
                });
              }
            
            
              if(n_sections==0) {
                throw runtime_error( string("Currently we require the hps or market attribute: request_id= ") +  req_id);
              }
            }
            handler.notify_changes();
            //---- RETURN RESPONSE: ------//
            response += "}";
            return bg_work_result{response};
          }).get();
    }
}
