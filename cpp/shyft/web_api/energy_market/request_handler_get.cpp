#include <boost/describe/enum_to_string.hpp>

#include <shyft/energy_market/a_wrap.h>
#include <shyft/energy_market/constraints.h>
#include <shyft/energy_market/stm/context.h>
#include <shyft/energy_market/stm/model.h>
#include <shyft/energy_market/stm/srv/dstm/dstm_subscription.h>
#include <shyft/energy_market/stm/srv/dstm/ts_magic_merge.h>
#include <shyft/mp.h>
#include <shyft/web_api/energy_market/generators.h>
#include <shyft/web_api/energy_market/generators/hydro_power.h>
#include <shyft/web_api/energy_market/grammar.h>
#include <shyft/web_api/energy_market/srv/generators.h>
#include <shyft/web_api/generators/json_struct.h>

namespace shyft::web_api::energy_market {
    using namespace shyft::web_api::generator;
    namespace mp = shyft::mp;
    namespace hana = boost::hana;

    using shyft::srv::model_info;
    using shyft::energy_market::stm::stm_hps;
    using shyft::energy_market::stm::reservoir;
    using shyft::energy_market::stm::power_plant;
    using shyft::energy_market::stm::waterway;
    using shyft::energy_market::stm::unit;
    using shyft::energy_market::stm::unit_group;
    using shyft::energy_market::stm::unit_group_member;
    using shyft::energy_market::stm::contract;
    using shyft::energy_market::stm::contract_relation;
    using shyft::energy_market::stm::transmission_line;
    using shyft::energy_market::stm::busbar;
    using shyft::energy_market::stm::power_module_member;
    using shyft::energy_market::stm::unit_member;
    using shyft::energy_market::stm::energy_market_area;
    using shyft::energy_market::stm::catchment;
    using shyft::energy_market::stm::stm_system;
    using shyft::energy_market::stm::run_parameters;

    using shyft::energy_market::core::constraint_base;
    using shyft::energy_market::core::absolute_constraint;
    using shyft::energy_market::core::penalty_constraint;
    using shyft::energy_market::stm::subscription::proxy_attr_observer;
    using shyft::energy_market::stm::subscription::proxy_attr_observer_;
    using shyft::energy_market::stm::srv::dstm::ts_merge_result;
    using shyft::energy_market::stm::srv::dstm::ts_magic_merge_values;
    using shyft::web_api::grammar::phrase_parser;

    using shyft::time_series::dd::aref_ts;
    using shyft::time_series::dd::gpoint_ts;
    using shyft::time_series::dd::abin_op_ts;
    using shyft::time_series::dd::ts_as;
    using shyft::energy_market::attr_traits::exists;
    
   /** @brief visitor class for dispatching how to read attributes based on type
     * 
     */
    struct read_proxy_handler : public boost::static_visitor<attribute_value_type> {
        struct extract_period: boost::static_visitor<utcperiod>{
            utcperiod def;
            utcperiod operator() (utcperiod const&p) const { return p;}
            utcperiod operator() (vector<int> const&v) const {
                if(v.size()!=2)
                    throw std::runtime_error("parsing read_period failed, needs exact format of [from,to]");

                return utcperiod{from_seconds(v[0]),from_seconds(v[1])};
            }
            template<typename T>
            utcperiod operator() (T const&) const {return def;}
        };

        read_proxy_handler(const json& data, server * srv, std::function<bg_work_result(json const&)>&& cb): srv{srv} {
            // Set up for subscription:
            auto osub = boost::get_optional_value_or(data.optional<bool>("subscribe"), false);
            use_cache = boost::get_optional_value_or(data.optional<bool>("cache"), true);
            if (osub) {
                json sub_data = data;
                sub_data.m.erase("subscribe");
                subscription = std::make_shared<proxy_attr_observer>(srv, data.required<string>("request_id"), sub_data,
                    std::move(cb));
                subscription->recalculate();//important: since we emit version 0, set initial value of terminals to 0
            }
            
            // Setting up read_type, -period and time_axis;
            read_type = boost::get_optional_value_or(data.optional<string>("read_type"), "read");
            if (read_type == "percentiles")
                percentiles = data.required<vector<int>>("percentiles");
            ta = data.required<generic_dt>("time_axis");
            //read_period = boost::get_optional_value_or(data.optional<utcperiod>("read_period"), ta.total_period());
            auto rp=data.optional("read_period");
            if(rp) {
                extract_period epv;//epv.def=ta.total_period();
                read_period = boost::apply_visitor(epv,*rp);
                if(read_period == epv.def)
                    throw std::runtime_error("dstm: failed to parse read_period attribute from the request");
            } else {
                read_period=ta.total_period();
            }
        }
        
        attribute_value_type operator()(apoint_ts const& ts) {
            apoint_ts nts;
            if (ts.needs_bind()) { // Case 1: The series is unbound:
                if ( !srv || !(srv->dtss) )
                    throw std::runtime_error("Dtss has to be set to read unbound time series.");
                dtss::ts_vector_t tsv;
                auto tsc=ts.clone_expr();
                tsv.emplace_back(tsc);
                try {
                    tsv = srv->do_evaluate_ts(tsv,read_period,  use_cache, true, read_period);// use_cache as pr. request(default true) also update cache
                    //tsv=srv->dtss->do_evaluate_ts_vector(read_period,tsv,use_cache,true, read_period);// try the old..
                } catch (std::runtime_error &re) {
                    srv->slog<<dlib::LERROR<<"Failed to evaluate "<<tsc.stringify()<<" diag:"<<re.what();
                    return std::string("not found");
                }
                nts = tsv[0];
            } else {
                nts = ts;
            }
            // TODO: the percentiles, or should we say percentile, ..
            //       could be used, if set, accepting one percentile
            //       combined with time-axis
            if(ta.size()==0) {
                return nts;// just return the time-series as is, no average, or resample
            }
            if(nts.point_interpretation()==time_series::POINT_AVERAGE_VALUE) { // we promise to deliver points to valid for the time-axis
                return nts.average(ta); // stair-case -> use true average, makes a lot of sense
            } else { // linear, point-in-time state-variables, we use resampling to the exact provided time-axis (hmm. maybe plus one timepoint?? or should user supply resample ta?)
                apoint_ts rts(ta,1.0,time_series::POINT_INSTANT_VALUE);
                return nts.use_time_axis_from(rts);
            }

        }

        template<typename V>
        attribute_value_type operator()(shared_ptr<map<utctime,V>> const& tv) {
            // We only read the values that are in the read_period:
            auto res = make_shared<map<utctime, V>>();
            utctime tx=min_utctime; // keeps track of entries <= read_period.start
            V x; // the one entry that was found closest to read_period.start
            for (auto const& kv : *tv) {
                if (kv.first>read_period.start && kv.first< read_period.end){
                    res->insert(res->end(),kv);//within the period, add it.
                } else if(kv.first <= read_period.start) { //add entry just at start, or first before start
                    if(kv.first > tx) { // better candidate found
                        tx=kv.first;// record the time of the candidate
                        x=kv.second;// and the value, to be inserted when we are done with the loop.
                    }
                }
            }
            if(tx != min_utctime) {
                (*res)[tx]=x;//insert the most recent entry before the read period
            }
            return res;
        }

        template<typename V>
        attribute_value_type operator()(V const& v) {
            return v;

        }
        
        proxy_attr_observer_ subscription = nullptr;
        string read_type;
        vector<int> percentiles;
        generic_dt ta;
        utcperiod read_period;
        server * srv;
        bool use_cache{true};

    };


    template<class T>
    vector<json> get_proxy_attributes(
        T const & t,
        vector<string> const& attr_ids,
        read_proxy_handler& rph)
    {
        // Set up result:
        vector<json> attr_vals;
        vector<bool> found(attr_ids.size(),false);// keep track of which attributes we actually found so we an report error
        size_t count=0;// count number we find, so we can return quickly
        auto has_id_x=[&attr_ids,&found,&count](const char*id) {
            for(auto i=0u;i<attr_ids.size();++i) {
                if(!strcmp(attr_ids[i].c_str(),id)) {
                    found[i]=true; // mark it as found
                    ++count;
                    return true;// and return
                }
            }
            return false;// not found/wanted.
        };
        // Iterate over each attribute, except ts map
        auto constexpr attribute_paths = mp::leaf_accessors(hana::type_c<T>);
        hana::for_each(
            attribute_paths, // The sequence
            [&](auto m) {
                // 0. Check if current id is in the specified list:
                if (has_id_x(mp::leaf_accessor_id_str(m))) {
                    json attr_struct;
                    attr_struct["attribute_id"] = std::string(mp::leaf_accessor_id_str(m));
                    auto val = mp::leaf_access(t,m);
                    if (exists(val)) {
                        attr_struct["data"] = rph(mp::leaf_access(t,m));
                    } else {
                        attr_struct["data"] = string("not found");
                    }
                    if (rph.subscription) {
                        rph.subscription->add_subscription(t, m);
                    }
                    attr_vals.emplace_back(attr_struct);
                }
            }
        );
        // iterate over the .tsm[].. attributes, unless, we already did find all we searched for
        if(count!=attr_ids.size() ) { // we lack some, let's check the .tsm[].. if they are there.
            if constexpr (!std::is_same<unit_group_member, T>::value && 
                          !std::is_same<unit_member, T>::value &&
                          !std::is_same<power_module_member, T>::value &&
                          !std::is_same<contract_relation, T>::value) { // only if T has .tsm[]
                //std::cout<<"search for missing members "<<t.id<<",name="<<t.name<< std::endl;
                for(auto i=0u;i<found.size();++i) {
                    if(!found[i]) {
                        //std::cout<<" search for "<< attr_ids[i]<<std::endl;
                        if(!t.tsm.empty() || attr_ids[i].rfind("ts.",0)==0 ) {
                            //std::cout<<" ts.xxx "<< attr_ids[i].substr(3)<<std::endl;
                            
                            auto f=t.tsm.find(attr_ids[i].substr(3));
                            if( f!= t.tsm.end()) {
                                found[i]=true;++count;

                                json attr_struct;
                                attr_struct["attribute_id"] = attr_ids[i];
                                apoint_ts val=f->second;
                                if(exists(val)) {
                                    attr_struct["data"] = rph(val);
                                } else {
                                    attr_struct["data"] = string("not found");
                                }
                                if (rph.subscription) {
                                    rph.subscription->add_ts_map_subscription(t,attr_ids[i]);
                                }
                                attr_vals.emplace_back(attr_struct);
                            } 
                        } else if(attr_ids[i]=="json") {
                            found[i]=true;++count;
                            json attr_struct;
                            attr_struct["attribute_id"] = attr_ids[i];
                            attr_struct["data"] = t.json;
                            attr_vals.emplace_back(attr_struct);
                        }
                    }
                }
            }
        }
        
        if(count!=attr_ids.size()) { // we got a request that ask for not existent attributes.
            for(auto i=0u;i<found.size();++i) {
                if (!found[i]) {
                    json r;
                    r["attribute_id"] = attr_ids[i];// just pretend we have it
                    r["data"] = string("attribute not found");//
                    attr_vals.emplace_back(r);
                }
            }
        }
        return attr_vals;
    }


    /** @brief Reads a list of proxy attributes from a list of T specified by data.
     *
     * @tparam T container type for proxy attributes (reservoir, unit, waterway &c.)
     * @tparam Tc container type of base type (So we can do dynamic casting)
     *      This template parameters is required in this solution due to invariation,
     *      i.e. even if T extends Tc, vector<T> DOES NOT extend vector<Tc>
     * @tparam Fx a callable (int cid, shared_ptr<Tc> const&el)->bool if el->id matches id
     * @param vecvals the vector with a series of references to instances of T.
     * @param data: JSON struct specifying which components, and which attributes to retrieve
     *      Requires keys:
     *          @key component_ids: vector<int> of IDs of which containers to retrieve from.
     *          @key attribute_ids: vector<int> of IDs of which attributes to retrieve for each container.
     * @param rph: instance of handler, used in innermost loop to handle the different value types.
     * @param fx: the callable that compares component-id, cid, to the 'id' of the Tc.'id' (usually ->id)
     * @return
     */
    template<class T, class Tc, class Fx>
    vector<json> get_attribute_value_table_fx(vector<shared_ptr<Tc>> const& vecvals, vector<json> const& data, read_proxy_handler& rph,Fx&&fx){
        vector<json> result;
        // Get out component_ids and attribute_ids:
        for(auto const &cdata:data) {
            auto cid = cdata.required<int>("component_id");
            auto attr_ids = cdata.required<vector<string>>("attribute_ids");
            shared_ptr<T> comp;
            // For each attribute container:
            {
                json comp_struct;
                comp_struct["component_id"] = cid;
                // Find container in vector:
                auto it = find_if(vecvals.begin(), vecvals.end(), [&cid,&fx](auto const& el) {
                    return fx(cid,el);//->id == cid;
                });
                if (it == vecvals.end()) {
                    comp_struct["component_data"] = string("Unable to find component");
                } else {
                    comp = dynamic_pointer_cast<T>(*it);
                    comp_struct["component_data"] = get_proxy_attributes(*comp,attr_ids, rph);
                }
                result.push_back(comp_struct);
            }
        }
        return result;
    }

    /** ref to get_attribute_value_table_fx, this is with a prefabricated fx that compares the id of objects to cid */
    template<class T, class Tc>
    auto get_attribute_value_table(vector<shared_ptr<Tc>> const& vecvals, vector<json> const& data, read_proxy_handler& rph){
        return get_attribute_value_table_fx<T,Tc>(vecvals,data,rph,[](int cid,shared_ptr<Tc> const& c){return c->id==cid;});
    }

    /** @brief generate a response for a read_attributes request */
    bg_work_result request_handler::handle_read_attribute_request(const json &data) {
        // Get model and HPS:
        auto mid = data.required<string>("model_key");
        return srv->models.observe_or_throw(
          mid,
          [&](auto &&view){
            auto &mdl = view.model;

            // Prepare response:
            auto req_id = data.required<string>("request_id");
        
            // We use a read_proxy_handler (visitor) to hold subscription and read data,
            // and pass that along to the innermost loop over components and attributes
            read_proxy_handler vis(data, srv,
                                   [this](json const& data) {
                                     return handle_read_attribute_request(data);
                                   }
              );
        
            std::string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
            auto sink = std::back_inserter(response);
            size_t n_sections=0;// count that the request do contain a section.
            using hana::type_c;
            //---- EMIT DATA: ----//
            {
              emit_object<decltype(sink)> oo(sink);
              //tools for oo emitters,
              auto oo_def=[&n_sections,&vis](auto &oo,json const&data,std::string const& name,auto const& dv, auto tp) {
                using d_type= typename decltype(tp)::type;
                auto j_data=data.template optional<vector<json>>(name); // check if there is a json section with the given name
                if(j_data) {// if yes, then
                  ++n_sections;//just count so we keep track
                  oo.def(name,get_attribute_value_table<d_type>(dv,*j_data,vis)); // does all needed magic rendering json
                }
              };
              // convinience: auto deduce type for  vectors in the stm namespace,
              auto oo_defx=[&oo_def](auto &oo,json const&data,std::string const& name,auto const& dv) {
                using d_type= typename std::remove_cvref_t<decltype(dv)>::value_type::element_type;// flip out the type T from vector<shared_ptr<T>>
                oo_def(oo,data,name,dv,type_c<d_type>);
              };

              oo.def("model_key", mid);
              auto hps_data=data.optional<vector<json>>("hps");

              if(hps_data && (*hps_data).size()) {
                n_sections++;
                oo.def_fx("hps",[&vis,&mdl,mid,&hps_data,&oo_def](auto sink) {
                  emit_vector_fx(sink,*hps_data,[&vis,&mdl,mid,&oo_def](auto oi, json const&hpsd) {
                    auto hps_id = hpsd.required<int>("hps_id");
                    auto it = std::find_if(mdl->hps.begin(), mdl->hps.end(),[&hps_id](auto ihps) { return ihps->id == hps_id; });
                    if (it == mdl->hps.end()) {
                      throw runtime_error( string("Unable to find HPS ") + std::to_string(hps_id) + string(" in model '") + mid + "'");
                    }
                    auto hps = *it;
                    emit_object<decltype(oi)> oo(oi);
                    oo.def("hps_id", hps->id);
                    oo_def(oo,hpsd,"reservoirs"  ,hps->reservoirs,type_c<reservoir>);
                    oo_def(oo,hpsd,"units"       ,hps->units,type_c<unit>);
                    oo_def(oo,hpsd,"power_plants",hps->power_plants,type_c<power_plant>);
                    oo_def(oo,hpsd,"waterways"   ,hps->waterways,type_c<waterway>);
                    auto gts=hps->gates();// important.. gates are return by value, and get_attr.. takes const ref.
                    oo_def(oo,hpsd,"gates"       ,gts,type_c<gate>);
                    oo_def(oo,hpsd,"catchments"  ,hps->catchments,type_c<catchment>);
                  });
                });
              }

              oo_defx(oo,data,"markets",mdl->market);
   
              auto contract_data=data.optional<vector<json>>("contracts");
              if(contract_data && (*contract_data).size()) {
                n_sections++;
                oo.def_fx("contracts",[&vis,&mdl,mid,&contract_data](auto sink) {
                  emit_vector_fx(sink,*contract_data,[&vis,&mdl,mid](auto oi, json const&cd) {
                    auto contract_id = cd.required<int>("component_id");
                    auto it = std::find_if(mdl->contracts.begin(), mdl->contracts.end(),[&contract_id](auto c) { return c->id == contract_id; });
                    if (it == mdl->contracts.end()) {
                      throw runtime_error( string("Unable to find contract ") + std::to_string(contract_id) + string(" in model '") + mid + "'");
                    }
                    auto contract = *it;
                        
                    // first emit contract attribute (if any)
                    emit_object<decltype(oi)> oo(oi);
                    oo.def("component_id", contract->id);// ensure to identify the contract regardless.
                        
                    if (auto attr_ids = cd.optional<vector<string>>("attribute_ids"); attr_ids ) { //any local attribute, like obligation.schedule etc.
                      oo.def("component_data",get_proxy_attributes(*contract,*attr_ids, vis));
                    }
                    if (auto r_attrs = cd.optional<vector<json>>("relations"); r_attrs) {
                      oo.def("relations", get_attribute_value_table_fx<contract_relation>(contract->relations, *r_attrs, vis,
                                                                                          [](int id,shared_ptr<contract_relation> const&cr){return id==cr->id;}));
                    }
                    if (auto p_attrs = cd.optional<vector<json>>("power_plants"); p_attrs) {
                      oo.def("power_plants", get_attribute_value_table_fx<power_plant>(contract->power_plants, *p_attrs, vis,
                                                                                       [](int id,shared_ptr<power_plant> const&p){return id==p->id;}));
                    }
                  });
                });
              }

              oo_defx(oo,data,"contract_portfolios",mdl->contract_portfolios);
              oo_defx(oo,data,"power_modules",mdl->power_modules);

              auto network_data=data.optional<vector<json>>("networks"); // check if there is a json section with the given name
              if (network_data && (*network_data).size()) {
                ++n_sections;//just count so we keep track
                oo.def_fx("networks",[&vis,&mdl,mid,&network_data](auto sink) {
                  emit_vector_fx(sink,*network_data,[&vis,&mdl,mid](auto oi, json const& networkd) {
                    auto network_id = networkd.required<int>("component_id");
                    auto it = std::find_if(mdl->networks.begin(), mdl->networks.end(),[&network_id](auto inet) { return inet->id == network_id; });
                    if (it == mdl->networks.end()) {
                      throw runtime_error( string("Unable to find Network ") + std::to_string(network_id) + string(" in model '") + mid + "'");
                    }
                    auto network = *it;
                    emit_object<decltype(oi)> oo(oi);
                    oo.def("component_id", network->id);
                    if (auto tl_attrs = networkd.optional<vector<json>>("transmission_lines"); tl_attrs) {
                      oo.def("transmission_lines", get_attribute_value_table_fx<transmission_line>(network->transmission_lines, *tl_attrs, vis,
                                                                                                   [](int tid,shared_ptr<transmission_line> const&tl){return tid==tl->id;}));
                    }
                    if (auto busbar_data = networkd.optional<vector<json>>("busbars"); busbar_data && (*busbar_data).size() > 0) {
                      oo.def_fx("busbars",[&vis, &network, network_id, &busbar_data](auto sink) {
                        emit_vector_fx(sink, *busbar_data,[&vis, &network, network_id](auto oi, json const& busbard) {
                          auto busbar_id = busbard.required<int>("component_id");
                          auto it = std::find_if(network->busbars.begin(), network->busbars.end(),[&busbar_id](auto ibus) { return ibus->id == busbar_id; });
                          if (it == network->busbars.end()) {
                            throw runtime_error( string("Unable to find Busbar ") + std::to_string(busbar_id) + string(" in network '") + std::to_string(network_id) + "'");
                          }
                          auto busbar = *it;
                          emit_object<decltype(oi)> oo(oi);
                          oo.def("component_id", busbar->id);

                          if (auto attr_ids = busbard.optional<vector<string>>("attribute_ids"); attr_ids ) {
                            oo.def("component_data", get_proxy_attributes(*busbar, *attr_ids, vis));  //any local attribute, like price.result etc.
                          }
                          if (auto unit_members = busbard.optional<vector<json>>("units"); unit_members) {
                            oo.def("units",// note that we use unit_member->unit->id as member id
                                   get_attribute_value_table_fx<unit_member>(busbar->units, *unit_members, vis,
                                                                             [](int cid,shared_ptr<unit_member> const&um){return cid==um->unit->id;}));
                          }
                          if (auto power_module_members = busbard.optional<vector<json>>("power_modules"); power_module_members) {
                            oo.def("power_modules", // note that we use power_module_member->power_module->id as member id
                                   get_attribute_value_table_fx<power_module_member>(busbar->power_modules, *power_module_members, vis,
                                                                                     [](int cid,shared_ptr<power_module_member> const&pm){return cid==pm->power_module->id;}));
                          }
                        });
                      });
                    }
                  });
                });
              }

              auto ug_data=data.optional<vector<json>>("unit_groups");
              if(ug_data && (*ug_data).size()) {
                n_sections++;
                oo.def_fx("unit_groups",[&vis,&mdl,mid,&ug_data](auto sink) {
                  emit_vector_fx(sink,*ug_data,[&vis,&mdl,mid](auto oi, json const&ugd) {
                    auto ug_id = ugd.required<int>("component_id");
                    auto it = std::find_if(mdl->unit_groups.begin(), mdl->unit_groups.end(),[&ug_id](auto iug) { return iug->id == ug_id; });
                    if (it == mdl->unit_groups.end()) {
                      throw runtime_error( string("Unable to find unit_groups ") + std::to_string(ug_id) + string(" in model '") + mid + "'");
                    }
                    auto ug = *it;
                        
                    // first emit unit-group attribute (if any)
                    emit_object<decltype(oi)> oo(oi);
                    oo.def("component_id", ug->id);// ensure to identify the unit group regardless.
                    auto attr_ids = ugd.optional<vector<string>>("attribute_ids");
                    if( attr_ids ) {//any local attribute, like obligation.schedule etc.
                      oo.def("component_data",get_proxy_attributes(*ug,*attr_ids, vis));
                    }                        
                    // then, while in this unit-group, also check if there are member attributes requested: members 
                    auto comp_attrs = ugd.optional<vector<json>>("members");// from members we can retrieve is_active, maybe later some more..
                    if (comp_attrs) oo.def("members",// notice that we use group-member->unit->id as member id
                                           get_attribute_value_table_fx<unit_group_member>(ug->members, *comp_attrs, vis,
                                                                                           [](int cid,shared_ptr<unit_group_member> const&ugm){return cid==ugm->unit->id;}));
                  });
                });
              }
              if(n_sections==0) {
                throw runtime_error( string("Currently we require the hps or markets sections in the request: request_id= ") +  req_id);
              }
            }
            //---- RETURN RESPONSE: ------//
            response += "}";
            if (vis.subscription)
              return bg_work_result{response, std::move(vis.subscription)};
            else
              return bg_work_result{response};
          }).get();
    }
   template<class T>
    vector<json> get_attribute_values(T const& t, read_proxy_handler& rph) {
        constexpr auto attr_paths = mp::leaf_accessors(hana::type_c<T>);
        vector<json> result{};
        result.reserve(mp::leaf_accessor_count(hana::type_c<T>));
        hana::for_each(
            attr_paths,
            [&result, &t, &rph](auto m) {
                json attr_struct;
                attr_struct["attribute_id"] = string(mp::leaf_accessor_id_str(m));
                attr_struct["data"] = mp::leaf_access(t,m);
                if (rph.subscription) {
                    rph.subscription->add_subscription(t, m);
                }
                result.push_back(attr_struct);
            }
        );

        return result;
    }
    /** @brief handle run_params request
     *
     */
    bg_work_result request_handler::handle_run_params_request(const json& data) {
        auto req_id = data.required<string>("request_id");
        auto model_key = data.required<string>("model_key");
        return srv->models.observe_or_throw(
          model_key,
          [&](auto &&view){

            auto &mdl = view.model;
            // Generate result:
            json ndata = data;
            ndata["time_axis"] = generic_dt(); // Time_axis is required
            read_proxy_handler vis(ndata, srv, [this](json const& data) { return handle_run_params_request(data); });
            json result;
            result["model_key"] = model_key;
            result["values"] = get_attribute_values((mdl->run_params), vis);
            // Generate response:
            std::string response = "";
            auto sink = std::back_inserter(response);
            {
              emit_object<decltype(sink)> oo(sink);
              oo.def("request_id", req_id)
                .def("result", result);
            }
            if (vis.subscription)
              return bg_work_result{response, std::move(vis.subscription)};
            else
              return bg_work_result{response};
          }).get();
    }
    bg_work_result request_handler::handle_opt_summary_request(const json& data) {
        auto req_id = data.required<string>("request_id");
        auto model_key = data.required<string>("model_key");
        return srv->models.observe_or_throw(
          model_key,
          [&](auto &&view){
            auto &mdl = view.model;
            // Generate result:
            json ndata = data;
            ndata["time_axis"] = generic_dt(); // Time_axis is required
            read_proxy_handler vis(ndata, srv, [this](json const& data) { return handle_opt_summary_request(data); });
            json result;
            result["model_key"] = model_key;
            result["values"] = get_attribute_values(*(mdl->summary), vis);
            // Generate response:
            std::string response = "";
            auto sink = std::back_inserter(response);
            {
              emit_object<decltype(sink)> oo(sink);
              oo.def("request_id", req_id)
                .def("result", result);
            }
            if (vis.subscription)
              return bg_work_result{response, std::move(vis.subscription)};
            else
              return bg_work_result{response};
          }).get();
    }

    /** @brief handle get_log request */
    bg_work_result request_handler::handle_get_log_request(const json& data) {
        auto req_id = data.required<string>("request_id");
        auto model_key = data.required<string>("model_key");

        // Generate response
        std::string response = R"_({"request_id":")_" + req_id + R"_(","result":)_";
        auto sink = std::back_inserter(response);

        auto log = srv->do_get_log(model_key); // Needs to be in guards because log_entry isn't exposed otherwise.
        log_entry_generator<decltype(sink)> msg_;
        ka::rule<decltype(sink), vector<shyft::energy_market::stm::log_entry>()> g = ka::lit('[') << -(msg_ % ',') << ka::lit(']');
        {
            generate(sink, g, log);
        }
        *sink++ = '}';
        return bg_work_result{response};
    }
    bg_work_result request_handler::handle_get_state_request(const json& data) {
        auto req_id = data.required<string>("request_id");
        auto model_key = data.required<string>("model_key");

        auto state = srv->do_get_state(model_key);

        // Prepare response:
        std::string response = string("{\"request_id\":\"") + req_id + string("\",\"result\":");
        auto sink = std::back_inserter(response);
        // Emit bare-bones model structure:
        {
            emit_object<decltype(sink)> oo(sink);
            oo.def("state", boost::describe::enum_to_string(state, "unknown"));
        }
        response += "}";
        return bg_work_result{ response };
    }
    
}
