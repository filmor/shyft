#pragma once
#include <shyft/web_api/energy_market/generators.h>
#include <memory>
#include <shyft/energy_market/hydro_power/hydro_component.h>
#include <shyft/energy_market/hydro_power/waterway.h>
#include <shyft/energy_market/hydro_power/power_plant.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/stm/attribute_types.h>

namespace shyft::web_api::generator {
    using xy_point=shyft::energy_market::hydro_power::point;
    using shyft::energy_market::hydro_power::xy_point_curve;
    using shyft::energy_market::hydro_power::xy_point_curve_with_z;
    using shyft::energy_market::hydro_power::turbine_operating_zone;
    using shyft::energy_market::hydro_power::turbine_description;
    using shyft::energy_market::stm::xy_point_curve_with_z_list;

    using shyft::energy_market::hydro_power::hydro_connection;
    using shyft::energy_market::hydro_power::connection_role;
    using shyft::energy_market::hydro_power::hydro_component;
    using shyft::energy_market::hydro_power::reservoir;
    using shyft::energy_market::hydro_power::unit;
    using shyft::energy_market::hydro_power::waterway;
    using shyft::energy_market::hydro_power::gate;
    using shyft::energy_market::hydro_power::power_plant;
    using shyft::energy_market::hydro_power::hydro_power_system;

    //-----------------
    // here we place the emitters for our classes
    // like one line each attribute.
    //

    template <class OutputIterator>
    struct emit<OutputIterator, hydro_connection> {
        emit(OutputIterator &oi, hydro_connection const &o) {
            emit_object<OutputIterator> oo(oi);
            oo
            .def_fx("role", [r = o.role](OutputIterator &oi) {
                switch (r) {
                    case connection_role::main:
                        emit<OutputIterator, const char*>(oi, "main"); break;
                    case connection_role::bypass:
                        emit<OutputIterator, const char*>(oi, "bypass"); break;
                    case connection_role::flood:
                        emit<OutputIterator, const char*>(oi, "flood"); break;
                    case connection_role::input:
                        emit<OutputIterator, const char*>(oi, "input"); break;
                }
            })
            .def_fx("target", [r = o.target.get()](OutputIterator &oi) -> void {
                    if (r) {
                        *oi++ = quote;
                        if (dynamic_cast<waterway *>(r)) {*oi++ = 'W';}
                        else if (dynamic_cast<unit *>(r)) {*oi++ = 'A';}
                        else if (dynamic_cast<reservoir *>(r)) {*oi++ = 'R';}
                        else {*oi++ = '?'; *oi++ = quote; return;}//TODO: consider throw runtime_error()
                        emit<OutputIterator,int64_t>(oi, r->id);
                        *oi++ = quote;
                    } else {
                        emit_null(oi);
                    }
                }
            );
        }
    };
    x_emit_vec(hydro_connection);

    template <class OutputIterator>
    struct emit<OutputIterator, hydro_component> {
        emit(OutputIterator &oi, hydro_component const &o) {
            emit_object<OutputIterator> oo(oi);
            oo
                .def("id", o.id)
                .def("name", o.name)
                .def("json", o.json)
                .def("upstreams", o.upstreams)
                .def("downstreams", o.downstreams);
        }
    };

    x_emit_shared_ptr(hydro_component);

    template <class OutputIterator>
    struct emit<OutputIterator, power_plant> {
        emit(OutputIterator &oi, power_plant const &o) {
            emit_object<OutputIterator> oo(oi);
            std::vector<int64_t> a_ids;
            for (auto const &a:o.units) if (a) a_ids.push_back(a->id); // output by ref, id only
            oo
                .def("id", o.id)
                .def("name", o.name)
                .def("json", o.json)
                .def("units", a_ids);
        }
    };
    x_emit_shared_ptr( power_plant );
    x_emit_vec(std::shared_ptr<power_plant>);
    //template void emit_shared_ptr<em_output_iterator, power_plant>(em_output_iterator&, std::shared_ptr<power_plant> const&);

    template <class OutputIterator>
    struct emit<OutputIterator, gate> {
        emit(OutputIterator &oi, gate const &o) {
            emit_object<OutputIterator> oo(oi);
            oo.def("id", o.id).def("name", o.name).def("json", o.json);
        }
    };
    x_emit_shared_ptr(gate);
    x_emit_vec(std::shared_ptr<gate>);

    template<class OutputIterator>
    struct emit<OutputIterator, waterway> {
        emit(OutputIterator &oi, waterway const &o) {
            emit_object<OutputIterator> oo(oi);
            oo
                .def("type", (const char*) "W")
                .def("hydro_component", static_cast<const hydro_component &>(o))
                .def("gates", o.gates);
        }
    };
    x_emit_shared_ptr( waterway );
    x_emit_vec( std::shared_ptr<waterway>);

    //-- just for testing, a complete emitter for hydro-power system
    template<class OutputIterator,class HC>
    void emit_hc(OutputIterator&oi,const char* type_code, HC const& o) {
        emit_object<OutputIterator> oo(oi);
        oo
        .def("type",type_code)
        .def("hydro_component",static_cast<const hydro_component&>(o))
        ;
    }

    template<class OutputIterator>
    struct emit<OutputIterator, unit> {
        emit(OutputIterator &oi, unit const &o) { emit_hc(oi, "A", o); }
    };

    template<class OutputIterator>
    struct emit<OutputIterator, reservoir> {
        emit(OutputIterator &oi, reservoir const &o) { emit_hc(oi, "R", o); }
    };

    x_emit_shared_ptr(unit);
    x_emit_vec(std::shared_ptr<unit>);
    x_emit_shared_ptr(reservoir);
    x_emit_vec(std::shared_ptr<reservoir>);

    template<class OutputIterator>
    struct emit<OutputIterator, hydro_power_system> {
        emit(OutputIterator &oi, hydro_power_system const &o) {
            emit_object<OutputIterator> oo(oi);
            oo
                .def("id", o.id)
                .def("name", o.name)
                .def("created", o.created)
                .def("units", o.units)
                .def("waterways", o.waterways)
                .def("reservoirs", o.reservoirs)
                .def("power_plants", o.power_plants);
        }
    };
    x_emit_shared_ptr(hydro_power_system);

}
