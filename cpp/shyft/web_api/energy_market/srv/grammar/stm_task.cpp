#include <shyft/web_api/energy_market/srv/grammar.h>
#include <memory>

namespace shyft::web_api::grammar {

    void set_stm_run(shared_ptr<stm_case>& r, stm_case i) {
        r = std::make_shared<stm_case>(i);
    }
    template <class Iterator, class Skipper>
    stm_session_grammar<Iterator, Skipper>::stm_session_grammar():
        stm_session_grammar::base_type(start, "stm_session")
    {
        labels_ = lit('[') >> -(string_ % ',') >> lit(']');
        run_ptr_ = run_[phx::bind(set_stm_run,_val, _1)];
        runs_ = lit('[') >> -(run_ptr_ % ',') >> lit(']');

        start = lit('{')
            >> lit("\"id\"") >> ':' >> int_[phx::bind(&stm_task::id, _val) = _1]
            >> ',' >> lit("\"name\"") >> ':' >> string_[phx::bind(&stm_task::name, _val) = _1]
            >> (-(',' >> lit("\"created\"") >> ':' >> time_))
                [phx::bind(&stm_task::created, _val) = phx::bind([](auto const&t){return t?*t:core::utctime_now();}, _1)]
            >> (-(',' >> lit("\"json\"") >> ':' >> string_))
                [phx::bind(&stm_task::json, _val) = phx::bind(get_value_or<string>, _1, string(""))]
            >> (-(',' >> lit("\"labels\"") >> ':' >> labels_))
                [phx::bind(&stm_task::labels, _val) = phx::bind(get_value_or<vector<string>>, _1, vector<string>{})]
            >> (-(',' >> lit("\"cases\"") >> ':' >> runs_))
                [phx::bind(&stm_task::cases, _val) = phx::bind(get_value_or<vector<stm_case_>>, _1, vector<stm_case_>{})]
            >> (-(',' >> lit("\"base_model\"") >> ':' >> mr_))
                [phx::bind(&stm_task::base_mdl, _val) = phx::bind(get_value_or<model_ref>, _1, model_ref())]
            >> (-(',' >> lit("\"task_name\"") >> ':' >> string_))
                [phx::bind(&stm_task::task_name, _val) = phx::bind(get_value_or<string>, _1, string(""))]
            >> lit('}');
        on_error<fail>(start, error_handler(_4, _3, _2));
    }
    template struct stm_session_grammar<request_iterator_t, request_skipper_t>;
}

