/** This file is part of Shyft. Copyright 2015-2021 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <string>
#include <vector>
#include <memory>
#include <shyft/web_api/bg_work_result.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/dtss/dtss_subscription.h>
#include <shyft/dtss/time_series_info.h>
#include <shyft/dtss/queue_msg.h>

namespace shyft::dtss { // fwd decl. only
    //template <class CD>
    struct server;//fwd template server class
    //struct standard_dtss_dispatcher;
}

namespace shyft::web_api {
    using std::string;
    using std::vector;
    using std::shared_ptr;
    using shyft::core::utctime;
    using shyft::core::utcperiod;
    using shyft::core::no_utctime;
    using response_buffer=boost::beast::flat_buffer;

    using dtss_server=dtss::server;
    using shyft::dtss::ts_info;
    using shyft::time_series::dd::ats_vector;


    /**
     * @brief evaluate request
     */
    struct read_ts_request {
        string request_id;///< client side request id, that is echoed back with the response
        utcperiod read_period;///< the read_period, to use for reading unbound terminals
        utcperiod clip_period;///< the period to clip the resulting time-series against(they could be longer)
        bool cache{false};///< if true use cache, any missing fragments will be read and stashed to cache
        vector<string> ts_ids;///< ts_ids, but later, we could allow expressions
        bool subscribe{false};///< if true, enable subscribe for this.
        bool ts_fmt {false};///< if true, generate response in pure ts format, otherwise plot-friendly
        bool operator==(read_ts_request const&o) const {return cache==o.cache && read_period==o.read_period && clip_period==o.clip_period && ts_ids==o.ts_ids && subscribe==o.subscribe&& ts_fmt==o.ts_fmt;}
        bool operator!=(read_ts_request const&o) const {return !operator==(o);}
    };

    /** @brief request all info about the web api
     *
     * Provides static information
     */
    struct info_request {
        std::string request_id;
        bool operator==(info_request const&o) const noexcept {return request_id==o.request_id;}
        bool operator!=(info_request const&o) const {return !operator==(o);}
    };

    struct info_reply {
        std::string request_id;
        std::string ex_info;///< informal text about info
        bool operator==(info_reply const&o) const noexcept {return request_id==o.request_id&& ex_info==o.ex_info;}
        bool operator!=(info_reply const&o) const {return !operator==(o);}
    };

    struct request_reply {
        std::string request_id;
        std::string ex_info;///< if empty, all ok, otherwise explanation in programmer-readable text
        bool operator==(request_reply const&o) const noexcept {return request_id==o.request_id&& ex_info==o.ex_info;}
        bool operator!=(request_reply const&o) const {return !operator==(o);}
    };


    /** @brief unsubscripe message
     *
     * The read/percentile/average requests allow subscription.
     * This message cancel a specific subscription.
     */
    struct unsubscribe_request {
        std::string request_id;
        std::string subscription_id;///< the subscription_id, maybe: if blank, remove all subscriptions?
        bool operator==(unsubscribe_request const&o) const noexcept {return request_id==o.request_id&& subscription_id==o.subscription_id;}
        bool operator!=(unsubscribe_request const&o) const {return !operator==(o);}
    };

    /**
     * @brief find time-series request
     * @details
     * providing a search pattern, and
     * returning back a list of time-series
     *
     *
     */
    struct find_ts_request {
        std::string request_id;
        std::string find_pattern;
        bool operator==(find_ts_request const&o) const {return request_id==o.request_id && find_pattern== o.find_pattern;}
        bool operator!=(find_ts_request const&o) const {return !operator==(o);}
    };

    /**
    * @brief remove time-series request
    * @details
    * providing a search ts_url and remove ts url if found
    *
    */
    struct remove_ts_request {
      std::string request_id;
      std::vector<std::string> ts_urls;
      bool operator==(remove_ts_request const&o) const {return request_id==o.request_id && ts_urls== o.ts_urls;}
      bool operator!=(remove_ts_request const&o) const {return !operator==(o);}
    };


/** @brief find response pod
    */
    struct find_reply {
        string request_id;
        vector<ts_info> ts_infos;
        bool operator==(find_reply const&o) const { return request_id==o.request_id && ts_infos==o.ts_infos;}
        bool operator!=(find_reply const&o) const { return !operator==(o);}
    };

    /**
     * @brief response for read,average and percentile requests
     */
    struct tsv_reply {
        string request_id;
        ats_vector tsv;
        bool operator==(tsv_reply const&o) const { return request_id==o.request_id && tsv==o.tsv;}
        bool operator!=(tsv_reply const&o) const { return !operator==(o);}
    };

    /**
     * @brief message for the average ts request
     * @details
     * The average ts-request do average of a read-request data before returning
     * the result back to the client.
     * This way it's easy to do some data-reduction and alignment on the server side.
     * E.g. If you where to plot 100 years of hourly data, you could choose to use the
     * to plot the month averages to get a reasonable amount of data back for plotting.
     * If you need to provide more accurate information, also consider the percentile
     * function that do percentiles of the read-result (reducing the amount of data),
     * and providing statistical information for each time-step.
     */
    struct average_ts_request {
        string request_id;///< client side request id, that is echoed back with the response
        utcperiod read_period;///< the read_period, to use for reading unbound terminals
        shyft::time_axis::generic_dt ta;///< time-axis for average calculations in the post-processsing step.
        bool cache{false};///< if true use cache, any missing fragments will be read and stashed to cache
        vector<string> ts_ids;///< ts_ids, but later, we could allow expressions
        bool subscribe{false};///< if true, enable subscribe for this.
        bool ts_fmt {false};///< if true, generate response in pure ts format, otherwise plot-friendly
        bool operator==(average_ts_request const&o) const {return cache==o.cache && read_period==o.read_period && ta==o.ta && ts_ids==o.ts_ids&& subscribe==o.subscribe&& ts_fmt==o.ts_fmt;;}
        bool operator!=(average_ts_request const&o) const {return !operator==(o);}
    };

    /**
     * @brief The message for the percentile of ts request
     * @details
     * Contains enough information to implement the evaluate/read time-series,
     * then on that result, evaluate the percentiles over the specified time-axis.
     *
     */
    struct percentile_ts_request {
        string request_id;///< client side request id, that is echoed back with the response
        utcperiod read_period;///< the read_period, to use for reading unbound terminals
        shyft::time_axis::generic_dt ta;///< time-axis for average calculations in the post-processsing step.
        std::vector<int> percentiles;///< spec for percentiles, or statistical properties we could rather say
        bool cache{false};///< if true use cache, any missing fragments will be read and stashed to cache
        vector<string> ts_ids;///< ts_ids, but later, we could allow expressions
        bool subscribe{false};///< if true, enable subscribe for this.
        bool ts_fmt {false};///< if true, generate response in pure ts format, otherwise plot-friendly
        bool operator==(percentile_ts_request const&o) const {return percentiles==o.percentiles && cache==o.cache && read_period==o.read_period && ta==o.ta && ts_ids==o.ts_ids && subscribe==o.subscribe && ts_fmt==o.ts_fmt;}
        bool operator!=(percentile_ts_request const&o) const {return !operator==(o);}
    };



    /**
     * @brief message for storing time-series
     * @details
     * Used to store, update-existing(possibly merge points), create if missing, or entirely rewrite
     * a set of time-series.
     *
     * The response? request-id plus diagnostics if something did not work out
     *
     */
    struct store_ts_request {
        string request_id;///< client side request id, that is echoed back with the response
        bool merge_store{false}; ///< true: merge points only, that is, keep all existing points, also in affected periods, update/insert those who are update/new.
                          ///< --false: store as if first wipe-out supplied ts.total_period(), then insert points(if any).
                          ///< note: that the supplied ts has to match the existing ts with respect to time-axis/linear,stair defs.
        bool recreate_ts{false}; ///< if true, any existing time-series will be wiped out entirely, and replaced with the definitions of the passed ts.
        bool cache{false};       ///< if true, stash to cache while writing
        ats_vector tsv;   ///< stuff to stash to some-storage
    };
    //-- queue related request/response classes


    struct q_list_request {
        string request_id;///< client side request id, that is echoed back with the response
        bool operator==(q_list_request const&o) const {
            return request_id==o.request_id;
        }
        bool operator!=(q_list_request const&o) const {return !operator==(o);}
    };
    struct q_list_response {
        string request_id;///< client side request id, that is echoed back with the response
        vector<string> q_names;
        bool operator==(q_list_response const&o) const {
            return request_id==o.request_id && q_names==o.q_names;
        }
        bool operator!=(q_list_response const&o) const {return !operator==(o);}
    };

    struct q_info_request {
        string request_id;///< client side request id, that is echoed back with the response
        string q_name;
        string msg_id;
        bool operator==(q_info_request const&o) const {
            return request_id==o.request_id && q_name == o.q_name && msg_id == o.msg_id;
        }
        bool operator!=(q_info_request const&o) const {return !operator==(o);}
    };
    struct q_info_response {
        string request_id;///< client side request id, that is echoed back with the response
        dtss::queue::msg_info info;
        bool operator==(q_info_response const&o) const {
            return request_id==o.request_id && info==o.info;
        }
        bool operator!=(q_info_response const&o) const {return !operator==(o);}
    };

    struct q_infos_request {
        string request_id;///< client side request id, that is echoed back with the response
        string q_name;
        bool operator==(q_infos_request const&o) const {
            return request_id==o.request_id && q_name == o.q_name;
        }
        bool operator!=(q_infos_request const&o) const {return !operator==(o);}
    };
    struct q_infos_response {
        string request_id;///< client side request id, that is echoed back with the response
        vector<dtss::queue::msg_info> infos;
        bool operator==(q_infos_response const&o) const {
            return request_id==o.request_id && infos==o.infos;
        }
        bool operator!=(q_infos_response const& o) const {return !operator==(o);}
    };

    struct q_size_request {
        string request_id;///< client side request id, that is echoed back with the response
        string q_name;
        bool operator==(q_size_request const& o) const {
            return request_id==o.request_id && q_name == o.q_name;
        }
        bool operator!=(q_size_request const& o) const {return !operator==(o);}
    };

    struct q_size_response {
        string request_id;///< client side request id, that is echoed back with the response
        int64_t count{0};
        bool operator==(q_size_response const& o) const {
            return request_id==o.request_id && count==o.count;
        }
        bool operator!=(q_size_response const& o) const {return !operator==(o);}
    };

    struct q_maintain_request {
        string request_id;///< client side request id, that is echoed back with the response
        string q_name;
        bool   keep_ttl_items{true};///< keep ttl items according to their ttl
        bool flush_all{false};///< if true flush all items/ reset queue
        bool operator==(q_maintain_request const& o) const {
            return request_id==o.request_id && q_name == o.q_name && flush_all == o.flush_all;
        }
        bool operator!=(q_maintain_request const& o) const {return !operator==(o);}
    };

    // response is usual diag response.


    /**
     * @brief message q_put
     * @details
     * Used to queue a mesasge, tsv-vector, and a message into the queue
     * The response is usual diagnostics.
     */
    struct q_put_request {
        string request_id;///< client side request id, that is echoed back with the response
        string q_name;    ///< the name of the queue
        string msg_id;    ///< the unique msg_id
        string descript;  ///< description to place along with the msg
        utctime ttl{no_utctime};      ///< time to live
        ats_vector tsv;   ///< stuff to stash to some-storage

        bool operator==(q_put_request const&o) const {
            return request_id==o.request_id&& q_name==o.q_name && msg_id == o.msg_id &&
            descript == o.descript && ttl == o.ttl && tsv == o.tsv;
        }
        bool operator!=(q_put_request const&o) const {return !operator==(o);}
    };

    /**
     * @brief q_get request
     * @details
     * A non-blocking request/poll for the next message in the queue.
     *
     * The response is q_get_response, that will contain the payload of a message if there was
     * anyone available.
     *
     */
    struct q_get_request {
        string request_id;///< client side request id, that is echoed back with the response
        string q_name;    ///< the name of the queue
        bool operator==(q_get_request const&o) const {
            return request_id==o.request_id&& q_name==o.q_name ;
        }
        bool operator!=(q_get_request const&o) const {return !operator==(o);}
    };

    struct q_done_request {
        string request_id;///< client side request id, that is echoed back with the response
        string q_name;    ///< the name of the queue
        string msg_id;     ///< the name of the message we are done with
        string diagnostics;//< the diagnostics message for the done status
        bool operator==(q_done_request const&o) const {
            return request_id==o.request_id&& q_name==o.q_name && msg_id==o.msg_id &&  diagnostics==o.diagnostics ;
        }
        bool operator!=(q_done_request const&o) const {return !operator==(o);}
    };


    struct q_get_response {
        string request_id;         ///< client side request id, that is echoed back with the response
        dtss::queue::tsv_msg_ msg; ///< null or { info: .., tsv: [..]}

        bool has_msg() const { return msg!=nullptr;}
        dtss::queue::tsv_msg const& get_msg() const {
            if(msg==nullptr)
                throw std::runtime_error("q_get_response:: attempt to deref null msg");
            return *msg;

        }
        bool operator==(q_get_response const&o) const {
            return request_id==o.request_id&& ((msg==o.msg)||(msg&& o.msg && *msg == *o.msg)) ;
        }
        bool operator!=(q_get_response const&o) const {return !operator==(o);}
    };


    /**
     * @brief request_handler for the dtss
     * @details
     * responses
     * -- they are usually TsVector or TsInfo
     * background service that
     * have a shared_ptr<dtss_server>
     * the boost beast foreground io-service receives ws messages
     * posts those to the bg thread for processing
     * the bg worker does folloing:
     * parses the request using boost.spirit.qi
     * forward the request to the dtss  dtss classical results
     * using boost.spirit.karma, generate the json-like response
     * emit the result to the output buffer
     * forward it to the boost beast io-services
     */
    struct request_handler:base_request_handler {
        dtss_server* srv{nullptr}; ///< not owning, and the lifetime of srv outlives the request-handler
        //std::atomic_bool running;///<true if listen active and ready to run. false otherwise
        bg_work_result do_the_work(string const & request) override;
        bg_work_result do_subscription_work(shyft::core::subscription::observer_base_ const& eo) override;
    };

    int start_web_server(request_handler& bg_server, string address_s,unsigned short port,shared_ptr<string const> doc_root,int threads,int bg_threads,bool tls_only=false);
}
