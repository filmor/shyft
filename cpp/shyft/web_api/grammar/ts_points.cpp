#include <shyft/web_api/web_api_grammar.h>
#include <boost/fusion/adapted/std_tuple.hpp>

namespace shyft::web_api::grammar {
 
    template<typename Iterator,typename Skipper>
    ts_points_grammar<Iterator,Skipper>::ts_points_grammar() : ts_points_grammar::base_type(start,"ts_points") {
        // simply accept [[t,v]*]
        start =
        '[' >>
         -(( '[' >> t_ > ',' > (double_|nan_symbol) >> ']') % ',') >>
        ']'
        ;
        start.name("ts_points");
        on_error<fail>(start, error_handler(_4, _3, _2));
    }

    template struct ts_points_grammar<request_iterator_t,request_skipper_t>;
       
}
