#include <shyft/web_api/web_api_grammar.h>

namespace shyft::web_api::grammar {

inline remove_ts_request mk_remove_ts_request(std::string const&req_id, std::vector<std::string> const& ts_urls) {
    return remove_ts_request{req_id,ts_urls};
}

template<typename Iterator,typename Skipper>
remove_ts_request_grammar<Iterator,Skipper>::remove_ts_request_grammar() : remove_ts_request_grammar::base_type(start,"remove_ts_request") {

    start = (
        lit("remove") >lit('{')
            > lit("\"request_id\"")   > ':' > quoted_string > ','
            > lit("\"ts_urls\"")       > ':' > '[' > (quoted_string % ',') > lit(']') >
        lit('}')
    )
    [ _val = phx::bind(mk_remove_ts_request,_1,_2) ];
    start.name("remove");
    on_error<fail>(start, error_handler(_4, _3, _2));
}

template struct remove_ts_request_grammar<request_iterator_t,request_skipper_t>;
}
