#include <shyft/web_api/web_api_grammar.h>

namespace shyft::web_api::grammar {

    inline find_ts_request mk_find_ts_request(std::string const&req_id, std::string const& pattern) {
        return find_ts_request{req_id,pattern};
    }

    template<typename Iterator,typename Skipper>
    find_ts_request_grammar<Iterator,Skipper>::find_ts_request_grammar() : find_ts_request_grammar::base_type(start,"find_ts_request") {

            start = (
                 lit("find") >lit('{')
                    > lit("\"request_id\"")   > ':' > quoted_string > ','
                    > lit("\"find_pattern\"" ) > ':' > quoted_string >
                 lit('}')
            )
            [ _val = phx::bind(mk_find_ts_request,_1,_2) ];
            start.name("find_ts_request");
            on_error<fail>(start, error_handler(_4, _3, _2));
        }

        template struct find_ts_request_grammar<request_iterator_t,request_skipper_t>;
}
