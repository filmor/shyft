#include <shyft/web_api/web_api_grammar.h>

namespace shyft::web_api::grammar {

    template<typename Iterator,typename Skipper>
    quoted_string_grammar<Iterator,Skipper>::quoted_string_grammar():quoted_string_grammar::base_type(start,"quoted_string") {
        start = '"' >> content >> '"';
        content = *(~char_("\\\"") | escape_char);
        escape_char = ( "\\" >> char_("\\\"") )  |
            ( char_("\\") );          //< In the case \n, \t we don't want to remove the backslash
        //start %= lexeme['"' >> +(char_ - '"') >> '"'];
        start.name("quoted_string");
        on_error<fail>(start, error_handler(_4, _3, _2));
    }

    template struct quoted_string_grammar<request_iterator_t,request_skipper_t>;
}
