/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <boost/spirit/include/qi.hpp>
#include <boost/phoenix.hpp>

#include <boost/spirit/include/karma.hpp>
#include <boost/spirit/include/karma_string.hpp>


#include <string_view>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>

#include <shyft/dtss/time_series_info.h>
#include <shyft/dtss/geo.h>
#include <shyft/web_api/dtss_web_api.h>   // looking for find_request

#include <shyft/web_api/generators/escaped_string.h>

// -- we need some more stuff into the dd namespace to ease working with spirit::karma:
//

namespace shyft::web_api::generator {
    namespace qi = boost::spirit::qi;
    namespace ka=boost::spirit::karma;
    namespace phx=boost::phoenix;
    using std::string;
    using qi::double_;
    using shyft::time_series::dd::apoint_ts;
    using shyft::time_series::dd::gta_t;
    using geo_ts_id=shyft::dtss::geo::ts_id;
    using shyft::time_series::ts_point_fx;
    using shyft::time_series::point;
    using shyft::core::to_seconds;
    using shyft::dtss::ts_info;
    using shyft::core::utctime;
    using shyft::core::utcperiod;
    using shyft::time_axis::fixed_dt;
    using shyft::time_axis::calendar_dt;
    using shyft::time_axis::point_dt;
    using shyft::time_axis::generic_dt;




    // ref to: https://www.boost.org/doc/libs/1_69_0/libs/spirit/doc/html/spirit/karma/reference/numeric/real_number.html
    template <typename T>
    struct time_policy : ka::real_policies<T> {
        using base_policy_type=ka::real_policies<T>;
        static unsigned int precision(T) { return 6; }
        static int floatfield(T) {return base_policy_type::fmtflags::fixed;}
    };

    /**
     * @brief double number json formatting
     * @details we represent nan/inf as null, and we use max digits to avoid loosing accuracy
     */
    template <typename T>
    struct dnan_policy : ka::real_policies<T> {
        using base_policy_type=ka::real_policies<T>;
        static unsigned int precision(T) { return std::numeric_limits<T>::digits10; } // 12 digits

        template <class CharEncoding,class Tag,class OutputIterator>
        static bool nan(OutputIterator&sink,T,bool) {
            return ka::string_inserter<CharEncoding,Tag>::call(sink,"null");
        }

        template <class CharEncoding,class Tag,class OutputIterator>
        static bool inf(OutputIterator&sink,T,bool) {
            return ka::string_inserter<CharEncoding,Tag>::call(sink,"null");
        }
    };



    /** @brief grammar for emitting utctime
    *
    * generates double_|null
    */
    template<class OutputIterator>
    struct utctime_generator:ka::grammar<OutputIterator,utctime()> {
        utctime_generator();
        ka::rule<OutputIterator,utctime()> pg;
        ka::real_generator<double, time_policy<double> > time_;
    };

    /** 
     * @brief grammer for emitting a time,value point
     *
     */
    template<class OutputIterator>
    struct point_generator:ka::grammar<OutputIterator,point()> {
        point_generator();
        ka::rule<OutputIterator,point()> pg;
        utctime_generator<OutputIterator> time_;
        ka::real_generator<double, dnan_policy<double> > d_;
    };
    
    /** @brief grammar for emitting utcperiod
    *
    * generates [ double_,double_ ] | null
    */
    template<class OutputIterator>
    struct utcperiod_generator:ka::grammar<OutputIterator,utcperiod()> {

        utcperiod_generator();
        ka::rule<OutputIterator,utcperiod()> pg;
        utctime_generator<OutputIterator> time_;
    };

    /** @brief grammar for emitting different types of time axes
     *
     */
    template<class OutputIterator>
    struct fixed_dt_generator: ka::grammar<OutputIterator, fixed_dt()> {

        fixed_dt_generator();
        ka::rule<OutputIterator, fixed_dt()> pg;
        utctime_generator<OutputIterator> time_;
    };

    template<class OutputIterator>
    struct calendar_dt_generator: ka::grammar<OutputIterator, calendar_dt()> {
        calendar_dt_generator();
        ka::rule<OutputIterator, calendar_dt()> pg;
        utctime_generator<OutputIterator> time_;
    };

    template<class OutputIterator>
    struct point_dt_generator: ka::grammar<OutputIterator, point_dt()> {

        point_dt_generator();
        ka::rule<OutputIterator, point_dt()> pg;
        utctime_generator<OutputIterator> t_;

    };

    template<class OutputIterator>
    struct generic_dt_generator: ka::grammar<OutputIterator, generic_dt()> {
        generic_dt_generator();

        ka::rule<OutputIterator, generic_dt()> pg;
        fixed_dt_generator<OutputIterator> f_;
        calendar_dt_generator<OutputIterator> c_;
        point_dt_generator<OutputIterator> p_;
    };

    /** @brief a time-series generator
     * @details
     * if !pure_ts: e.g. suitable for plott/table visualisation
     *  outputs:
     *    {pfx:(true|false),data:[[t,v],..]}
     *  or if empty
     *    {pfx:null,data:null}
     * else: suitable for math/downstream computations and visualization
     *    { id:, pfx:..,time_axis:{}, values:[] }
     */
    template<class OutputIterator>
    struct apoint_ts_generator:ka::grammar<OutputIterator,apoint_ts()> {

        apoint_ts_generator(bool pure_ts=false);
        ka::rule<OutputIterator,apoint_ts()> tsg;
        point_generator<OutputIterator> pt_;
        ka::real_generator<double, dnan_policy<double> > d_;
        generic_dt_generator<OutputIterator> ta_;
    };

   /** @brief a time-series vector generator
    *
    * @details
    * outputs:
    *   [ts1,.., tsn]
    */
    template<class OutputIterator>
    struct atsv_generator:ka::grammar<OutputIterator,std::vector<apoint_ts>()> {

        atsv_generator(bool pure_ts=false);
        ka::rule<OutputIterator,std::vector<apoint_ts>()> tsg;
        apoint_ts_generator<OutputIterator> ats_;
    };

    /** @brief generator for ts_info
    *
    * ts_info provides a minimal set of dtss time-series
    * and is the result-item of the find_ts_request.
    * @see find_ts_request
    */
    template<class OutputIterator>
    struct ts_info_generator:ka::grammar<OutputIterator,ts_info()> {
        ts_info_generator();
        ka::rule<OutputIterator,ts_info()> pg;
        utctime_generator<OutputIterator> time_;
        utcperiod_generator<OutputIterator> period_;
    };

    /** @brief a ts_info vector generator
    *
    * outputs:[ ts_infos ]
    * @see ts_info_generator
    */
    template<class OutputIterator>
    struct ts_info_vector_generator:ka::grammar<OutputIterator,std::vector<ts_info>()> {

        ts_info_vector_generator();

        ka::rule<OutputIterator,std::vector<ts_info>()> tsig;
        ts_info_generator<OutputIterator> tsi_;
    };

    template<class OutputIterator>
    struct find_ts_request_generator:ka::grammar<OutputIterator,find_ts_request()> {
        find_ts_request_generator();
        ka::rule<OutputIterator,find_ts_request()> g_;
    };

    template<class OutputIterator>
    struct remove_ts_request_generator:ka::grammar<OutputIterator,remove_ts_request()> {
      remove_ts_request_generator();
      ka::rule<OutputIterator,remove_ts_request()> g_;
    };

    template<class OutputIterator>
    struct read_ts_request_generator:ka::grammar<OutputIterator,read_ts_request()> {
        read_ts_request_generator();
        ka::rule<OutputIterator,read_ts_request()> g_;
        utcperiod_generator<OutputIterator> p_;
    };

    template<class OutputIterator>
    struct store_ts_request_generator:ka::grammar<OutputIterator,store_ts_request()> {
        store_ts_request_generator();
        ka::rule<OutputIterator,store_ts_request()> g_;
        atsv_generator<OutputIterator> tsv_;
    };


    template<class OutputIterator>
    struct average_ts_request_generator:ka::grammar<OutputIterator,average_ts_request()> {
        average_ts_request_generator();
        ka::rule<OutputIterator,average_ts_request()> g_;
        utcperiod_generator<OutputIterator> p_;
        generic_dt_generator<OutputIterator> ta_;
    };

    template<class OutputIterator>
    struct percentile_ts_request_generator:ka::grammar<OutputIterator, percentile_ts_request()> {
        percentile_ts_request_generator();
        ka::rule<OutputIterator,percentile_ts_request()> g_;
        utcperiod_generator<OutputIterator> p_;
        generic_dt_generator<OutputIterator> ta_;
    };

    /** geo://<geo_db>/v/g/e/t */
    template<class OutputIterator>
    struct geo_ts_url_generator: ka::grammar<OutputIterator, geo_ts_id() > {
        geo_ts_url_generator(std::string prefix="geo://");
        ka::rule<OutputIterator, geo_ts_id()> pg;
        string prefix;
    };

    //-- queue related generators
    using namespace dtss;

    template<class OutputIterator>
    struct q_put_request_generator:ka::grammar<OutputIterator,q_put_request()> {
        q_put_request_generator();
        ka::rule<OutputIterator,q_put_request()> g_;
        utctime_generator<OutputIterator> t_;
        atsv_generator<OutputIterator> tsv_;
    };

    template<class OutputIterator>
    struct q_get_request_generator:ka::grammar<OutputIterator,q_get_request()> {
        q_get_request_generator();
        ka::rule<OutputIterator,q_get_request()> g_;
    };

    template<class OutputIterator>
    struct q_msg_info_generator:ka::grammar<OutputIterator,queue::msg_info()> {
        q_msg_info_generator();
        ka::rule<OutputIterator,queue::msg_info()> g_;
        escaped_string_generator<OutputIterator> es_;
        utctime_generator<OutputIterator> t_;
    };

    template<class OutputIterator>
    struct q_tsv_msg_generator:ka::grammar<OutputIterator,queue::tsv_msg()> {
        q_tsv_msg_generator();
        ka::rule<OutputIterator,queue::tsv_msg()> g_;
        q_msg_info_generator<OutputIterator> info_;
        atsv_generator<OutputIterator> tsv_;
    };

    template<class OutputIterator>
    struct q_get_response_generator:ka::grammar<OutputIterator,q_get_response()> {
        q_get_response_generator();
        ka::rule<OutputIterator,q_get_response()> g_;
        q_tsv_msg_generator<OutputIterator> tsv_msg_;
    };

    //--
    template<class OutputIterator>
    struct q_list_request_generator:ka::grammar<OutputIterator,q_list_request()> {
        q_list_request_generator();
        ka::rule<OutputIterator,q_list_request()> g_;
    };
    template<class OutputIterator>
    struct q_list_response_generator:ka::grammar<OutputIterator,q_list_response()> {
        q_list_response_generator();
        ka::rule<OutputIterator,q_list_response()> g_;
    };
    template<class OutputIterator>

    struct q_info_request_generator:ka::grammar<OutputIterator,q_info_request()> {
        q_info_request_generator();
        ka::rule<OutputIterator,q_info_request()> g_;
    };
    template<class OutputIterator>
    struct q_info_response_generator:ka::grammar<OutputIterator,q_info_response()> {
        q_info_response_generator();
        ka::rule<OutputIterator,q_info_response()> g_;
        q_msg_info_generator<OutputIterator> info_;
    };
    template<class OutputIterator>

    struct q_infos_request_generator:ka::grammar<OutputIterator,q_infos_request()> {
        q_infos_request_generator();
        ka::rule<OutputIterator,q_infos_request()> g_;
    };

    template<class OutputIterator>
    struct q_infos_response_generator:ka::grammar<OutputIterator,q_infos_response()> {
        q_infos_response_generator();
        ka::rule<OutputIterator,q_infos_response()> g_;
        q_msg_info_generator<OutputIterator> info_;
    };

    template<class OutputIterator>
    struct q_size_request_generator:ka::grammar<OutputIterator,q_size_request()> {
        q_size_request_generator();
        ka::rule<OutputIterator,q_size_request()> g_;
    };
    template<class OutputIterator>
    struct q_size_response_generator:ka::grammar<OutputIterator,q_size_response()> {
        q_size_response_generator();
        ka::rule<OutputIterator,q_size_response()> g_;
    };

    template<class OutputIterator>
    struct q_maintain_request_generator:ka::grammar<OutputIterator,q_maintain_request()> {
        q_maintain_request_generator();
        ka::rule<OutputIterator,q_maintain_request()> g_;
    };

    template<class OutputIterator>
    struct q_done_request_generator:ka::grammar<OutputIterator,q_done_request()> {
        q_done_request_generator();
        ka::rule<OutputIterator,q_done_request()> g_;
        escaped_string_generator<OutputIterator> es_;
    };

    using generator_output_iterator = std::back_insert_iterator<string>;
    extern template struct point_generator<generator_output_iterator>;
    extern template struct ts_info_generator<generator_output_iterator>;
    extern template struct utctime_generator<generator_output_iterator>;
    extern template struct utcperiod_generator<generator_output_iterator>;
    extern template struct apoint_ts_generator<generator_output_iterator>;
    extern template struct fixed_dt_generator<generator_output_iterator>;
    extern template struct calendar_dt_generator<generator_output_iterator>;
    extern template struct point_dt_generator<generator_output_iterator>;
    extern template struct generic_dt_generator<generator_output_iterator>;
    extern template struct atsv_generator<generator_output_iterator>;
    extern template struct find_ts_request_generator<generator_output_iterator>;
    extern template struct store_ts_request_generator<generator_output_iterator>;
    //-- queue related
    extern template struct q_get_response_generator<generator_output_iterator>;
    extern template struct q_tsv_msg_generator<generator_output_iterator>;
    extern template struct q_msg_info_generator<generator_output_iterator>;
    extern template struct q_put_request_generator<generator_output_iterator>;
    extern template struct q_get_request_generator<generator_output_iterator>;
    extern template struct q_list_request_generator<generator_output_iterator>;
    extern template struct q_list_response_generator<generator_output_iterator>;
    extern template struct q_info_request_generator<generator_output_iterator>;
    extern template struct q_info_response_generator<generator_output_iterator>;
    extern template struct q_infos_request_generator<generator_output_iterator>;
    extern template struct q_infos_response_generator<generator_output_iterator>;
    extern template struct q_size_request_generator<generator_output_iterator>;
    extern template struct q_size_response_generator<generator_output_iterator>;
    extern template struct q_maintain_request_generator<generator_output_iterator>;
    extern template struct q_done_request_generator<generator_output_iterator>;
}

