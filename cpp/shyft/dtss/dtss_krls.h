#pragma once

#include <functional>
#include <string>
#include <vector>
#include <map>
#include <string>
#include <memory>

#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/dtss/time_series_info.h>
#include <shyft/dtss/ts_db_interface.h>

namespace shyft::dtss {
    using std::unique_ptr;
    namespace ta = shyft::time_axis;
    namespace ts = shyft::time_series;
    using shyft::core::to_seconds64;
    using shyft::core::utctime_from_seconds64;



    /** @brief  Encapsulation of file io functionality.
     */
    
    struct krls_pred_db_impl; // fwd to impl in detail dtss_krls.cpp and detail/krls_db.h
    class krls_pred_db:public its_db {

    public:
        using gta_t = shyft::time_axis::generic_dt;
        using gts_t = shyft::time_series::point_ts<gta_t>;
        using ts_vector_t = shyft::time_series::dd::ats_vector;
        using queries_t = std::map<std::string, std::string>;
        using cb_fx=std::function<ts_vector_t(std::string const&ts_id, utcperiod period, bool use_ts_cached_read, bool update_ts_cache)>;
    private:
        unique_ptr<krls_pred_db_impl> impl;
 
    public:
        krls_pred_db();
        ~krls_pred_db();

        /** Constructs a krls_pred_db with specified container root */
        
        krls_pred_db(const std::string& root_dir,cb_fx cb);

        krls_pred_db(const krls_pred_db &) = delete;
        krls_pred_db(krls_pred_db &&) = delete;

        krls_pred_db & operator=(const krls_pred_db &) = delete;
        krls_pred_db & operator=(krls_pred_db &&) = delete;

        /*  Container API
         * =============== */

    public:
        void save(const std::string & fn, const gts_t & ts, bool overwrite = true, const queries_t & queries = queries_t{}) override;
        void save(size_t n, fx_ts_item_t const& fx_item,bool overwrite, const queries_t & queries = queries_t{}) override;
        gts_t read(const std::string & fn, core::utcperiod period, const queries_t & queries = queries_t{}) override;
        void remove(const std::string & fn, const queries_t & queries = queries_t{}) override;
        ts_info get_ts_info(const std::string & fn, const queries_t & queries = queries_t{}) override;
        std::vector<ts_info> find(const std::string & match, const queries_t & queries = queries_t{}) override;

        /*  KRLS container API
         * ==================== */

        void register_rbf_series(
            const std::string & fn, const std::string & source_url,  // series filename and source url
            const core::utcperiod & period,  // period to train
            const core::utctimespan dt, const ts::ts_point_fx point_fx,
            const std::size_t dict_size, const double tolerance,  // general parameters
            const double gamma  // rbf kernel parameters
        );
        void update_rbf_series(
            const std::string & fn,  // series filename
            const core::utcperiod & period,  // period to train
            const bool allow_gap_periods = false
        ) ;

        void move_predictor(
            const std::string & from_fn,
            const std::string & to_fn,
            const bool overwrite = false
        ) ;

        gts_t predict_time_series(
            const std::string & fn,
            const gta_t & ta
        ) ;

    };

}
