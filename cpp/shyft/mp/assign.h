/** This file is part of Shyft. Copyright 2015-2023 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <shyft/mp.h>

namespace shyft::mp {
    /**
     * @brief assign same leaf attributes from source s to destination d
     * @details
     * This function assignes all leaf attributes of s to d.
     * Note that the type of s and d can be different.
     * Becomes useful during de-serialization of old structs, that needs to be assigned to recent type.
     * It throws if the d attribute can not be statically casted and assigned to the s attribute.
     * @param d destination hana struct
     * @param s source hana struct
     * @note finally got it compile equally on gcc/win using hint from JEH, where we
     * use c++ 20 feature of typename for lambdas, so that the compiler path
     * to generate the expressions are dependent on types, rather than constexpr folding.
     */

    constexpr auto leaf_access_assign=[](auto &d,auto const&s ) constexpr {
        hana::for_each(leaf_accessors(hana::type_c<std::remove_cvref_t<decltype(s)>>),
            [&d,&s]<typename A> (A ) constexpr {// use <typename A> lambda feature to capture the type here(its hana::type_c)
                hana::for_each(leaf_accessors(hana::type_c<std::remove_cvref_t<decltype(d)>>),
                    [=,&d,&s]<typename XA> (XA) constexpr {
                        if constexpr (leaf_accessor_id(XA{}) == leaf_accessor_id(A{})) {
                            using L = typename decltype(accessor_ptr_type(leaf_accessor(XA{})))::type;
                            if constexpr (requires { leaf_access(d,XA{}) = static_cast<L>(leaf_access(s,A{}) ); }) {
                                leaf_access(d,XA{}) = static_cast<L>(leaf_access(s,A{}));
                            } else {
                                throw std::runtime_error("deserialize from earlier version:specified wrong extract type for attribute path :"+ std::string(leaf_accessor_id_str(A{})));
                            }
                        }
                    }
                );
            }
        );
    };

}
