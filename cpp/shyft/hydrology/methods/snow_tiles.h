/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
///    Copyright 2012 Statkraft Energi A/S
///
///    This file is part of Shyft.
///
///    Shyft is free software: you can redistribute it and/or modify it under the terms of
/// the GNU Lesser General Public License as published by the Free Software Foundation,
/// either version 3 of the License, or (at your option) any later version.
///
///    Shyft is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
/// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
/// PURPOSE.  See the    GNU Lesser General Public License for more details.
///
///    You should have received a copy of the GNU Lesser General Public License along with
/// Shyft, usually located under the Shyft root directory in two files named COPYING.txt
/// and COPYING_LESSER.txt.    If not, see <http://www.gnu.org/licenses/>.
///
/// Adapted from early enki method programmed by Kolbjørn Engeland and Sjur Kolberg
///

#pragma once

#include <string>
#include <vector>
#include <algorithm>
#include <cmath>
#include <tuple>

#include <boost/math/distributions.hpp>
#include <boost/math/quadrature/trapezoidal.hpp>

#include <shyft/core/math_utilities.h>
#include <shyft/core/core_serialization.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/hydrology/methods/hbv_snow_common.h>

namespace shyft::core::snow_tiles{
    
    using std::tuple;
    using std::vector;
    using std::string;
    using std::tie;
    using std::max;
    using std::make_tuple;
    using namespace hbv_snow_common;
    using boost::math::gamma_distribution;
    using boost::math::quadrature::trapezoidal;
    const double zero_mass_tol = 1.0e-10;

    /** @brief parameter pack for the snow-tiles calculator
        * 
        */
    struct parameter {
        private:
            double shape{2.0};///<  shape parameter of the Gamma distribution defining the multiplication factors for snowfall,
                                    /// unit (-), range [0.1, inf], private parameter accessible through p.get_shape() and p.set_shape()
            vector<double> area_fractions {0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1}; ///< area fractions of the individual tiles, unit (-), range per tile [0.01, 1],
                                                                ///  the sum of the elements in the vector must equal 1
            mutable vector<double> multiply;/// mutable, because we evaluate it lazy in a const fx.
            
            inline void ensure_init() const {
                if (multiply.size()==0) {
                    multiply = compute_inverse_gamma(shape);
                    normalize_vector_mean(multiply);
                }
            }
        public:
            double tx{0.0}; ///< threshold temperature determining precipitation phase, unit (C), range [-4, 4]
            double cx{1.0};///< degree-day melt factor, unit (mm/C/day), range [0, 30]
            double ts{0.0};///< threshold temperature for melt onset, unit (C), range [-4, 4]
            double lwmax{0.1};///<max liquid water content given as a fraction of ice in the snowpack, unit (-), range [0, 1]
            double cfr{0.5};///<  refreeze coefficient, unit (-), range [0, 1]

        parameter() { // note: we can not call ensure_init, since that takes to long time, have to be lazy! 
        }
        
        explicit parameter(double shape, double tx=0.0, double cx=1.0, double ts=0.0, double lwmax=0.1, double cfr=0.5, vector<double> area_fractions = {0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1})
            :  shape(shape), area_fractions(area_fractions),  tx(tx), cx(cx), ts(ts), lwmax(lwmax), cfr(cfr) {
                multiply = compute_inverse_gamma(shape);
                normalize_vector_mean(multiply);
        }
        
        bool operator==(parameter const&o) const {
            return nan_equal(shape,o.shape)&& nan_equal(tx,o.tx)&&nan_equal(cx,o.cx)&&nan_equal(ts,o.ts)&&nan_equal(lwmax,o.lwmax)&&nan_equal(cfr,o.cfr)&& nan_equal(area_fractions,o.area_fractions) && nan_equal(multiply,o.multiply);
        }
        
        bool operator!=(parameter const&o) const {return !operator==(o);}

        const vector<double>& get_area_fractions() const { return area_fractions;}
        
        inline double area_fraction(size_t i) const noexcept {return area_fractions[i];}        

        void set_area_fractions(vector<double> new_area_fractions) {
            double sum = 0;
            for (auto& n : new_area_fractions)
                sum += n;
            if ((abs(sum-1)>zero_mass_tol) || (*min_element(new_area_fractions.begin(), new_area_fractions.end())<0.01))
                throw std::runtime_error("Area fractions must sum up to 1 and single elements must cover at least 0.01 of total area");
            area_fractions=new_area_fractions;
            multiply = compute_inverse_gamma(shape);
            normalize_vector_mean(multiply);
        }

        double get_shape() const { return shape; }

        void set_shape(const double new_shape) {
            if (fabs(shape-new_shape) > zero_mass_tol)  {
                multiply = compute_inverse_gamma(shape = new_shape);
                normalize_vector_mean(multiply);
            }
        }
        
        size_t number_of_tiles() const {return area_fractions.size();}

        const vector<double>& get_multiply() const { 
            //const_cast<parameter*>(this)->
            ensure_init();
            return multiply; 
        }

        vector<double> compute_inverse_gamma(const double shape) const {
            vector<double> mean_prob;
            const double mean = 1.0;
            const double scale = mean/shape;
            gamma_distribution<>myGamma (shape, scale);
            const double nr_intervals = area_fractions.size();
            double interval_start = 0;
            double interval_stop = area_fractions[0];
            auto f = [myGamma](double x) { return quantile(myGamma, x); };
            for (int i = 0; i<nr_intervals-1; ++i)  {// integration until second last interval, last interval in handled separately
                const double integral = trapezoidal(f, interval_start, interval_stop);
                const double res = integral / (interval_stop-interval_start);
                mean_prob.push_back(res);
                interval_start += area_fractions[i];
                interval_stop += area_fractions[i+1];
            }
            interval_stop -= 0.00001; // Setting upper boundary for integration of last interval.
                                        // Quantile function of gamma distribution: x->1: quantile(Gamma, x)->inf; Can lead to overvlow error in quantile(Gamma, x) when integrating over the last interval
            const double integral = trapezoidal(f, interval_start, interval_stop);
            const double res = integral / (interval_stop-interval_start);
            mean_prob.push_back(res);
            return mean_prob;
        }

        static void normalize_vector_mean(vector<double>& vec)  {// normalize vector, so that mean=1
            double sum = 0;
            for (auto& n : vec)
                sum += n;
            sum /= vec.size();
            for (auto& n : vec) 
                n /= sum;
        }
        x_serialize_decl();
    };

    struct state {
        vector<double> fw{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        vector<double> lw {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        state()=default;
        state(vector<double> fw , vector<double> lw ) : fw(fw), lw(lw) {
            if(fw.size()!=lw.size()) 
                throw std::runtime_error("snow_tiles::state fw and lw must have same size");
        }

        bool operator==(const state& x) const { return nan_equal(fw,x.fw)&& nan_equal(lw,x.lw);}
        bool operator!=(const state&x) const {return !operator==(x);}
        
        x_serialize_decl();
    };

    struct response {
        double outflow{0.0};///< mm/h
        double swe {0.0}; ///< approximate swe representative for the time-step
        double sca {0.0};///< approximate sca representative for the time-step
    };

    /** @brief Temperature index snow melt model including a tiling approach for snow distribution
    *
    * This algorithm uses a tiling approach to model the snow distribution within each cell. The user defines the area
    * of each tile, given as a fraction of total cell area. The amount of snow falling within the tiles is given by a
    * multiplier defined using a Gamma distribution. The mean of the multipliers has to be 1 to assure mass conservation. 
    * The shape parameter of the Gamma distribution is considered a calibration parameter. The snowpack development in 
    * each tile is simulated using a standard temperature index approach, potential melt is assumed to be uniformly 
    * distributed over the tiles.
    *
    */
    struct calculator {
        const parameter p;

        explicit calculator(const parameter &p) : p(p) {}

        /** @brief split precipitation into solid and liquid phase
        *
        *  @doi
        *   Kavetski, D., and G. Kuczera (2007), Model smoothing strategies to remove microscale
        *   discontinuities and spurious secondary optima in objective functions in hydrological
        *  calibration, Water Resour. Res., 43, W03411, doi:10.1029/2006WR005195.
        *
        *  See Equation 8 for precipitation phase, and Equation 13 for melting/refreezing.
        * 
        * @return tuple(solid,liquid)
        */
        
        static inline tuple<double, double> split_precipitation(double prec_mm_h, double temp, double tx) {
            double m_phase = 0.5;
            double frac_snowfall = 1. / (1. + exp( (temp - tx) / m_phase ));
            double psolid = prec_mm_h * frac_snowfall;
            double pliquid = prec_mm_h - psolid;
            return tuple<double, double>(psolid, pliquid);
        }

        static inline  double compute_potmelt(double temp, double cx, double ts, double step_in_days) {
            const double m_melt = 0.5;
            double t_m = (temp - ts) / m_melt;
            return  cx * step_in_days * m_melt * (t_m + std::log(1. + exp(-t_m)));
        }

        static inline double compute_potrefreeze(double temp, double cx, double cfr, double ts, double step_in_days) {
            const double m_refreeze = 0.5;
            double t_m = -(temp - ts) / m_refreeze;
            return  cfr * cx * step_in_days * m_refreeze * (t_m + std::log(1. + exp(-t_m)));
        }

        static inline double limit_fluxes(double flux_value, double available_water) {
            return std::min(flux_value, available_water);
        }

        static inline tuple<double, double, double> update_states(double fw, double lw, double psolid, double pliquid, double act_melt, double act_refreeze, double lwmax) {
            double outflow;

            fw += psolid + act_refreeze - act_melt;
            lw += pliquid + act_melt - act_refreeze;

            if (lw > fw * lwmax) {
                outflow = lw - fw * lwmax;
                lw = fw * lwmax;
            } else {
                outflow = 0.0;
            }
            fw = max(fw, 0.0);
            lw = max(lw, 0.0);
            outflow = max(outflow, 0.0);

            return make_tuple(fw, lw, outflow);
        }

        void step(state &s, response &r, utctime t0, utctime t1, double prec_mm_h, double temp) const {
            const double step_in_days = to_seconds(t1 - t0) / 86400.0;
            const double dt_hours = to_seconds(t1 - t0)/3600.0;
            const double prec_mm_step = prec_mm_h*dt_hours;

            /* const  invariiant after init */ 
            double psolid, pliquid;
            tie(psolid, pliquid) = split_precipitation(prec_mm_step, temp,p.tx);
            const double potmelt = compute_potmelt(temp, p.cx, p.ts, step_in_days);
            const double potrefreeze = compute_potrefreeze(temp, p.cx, p.cfr, p.ts, step_in_days);

            r.outflow = r.swe = r.sca = 0.0;
            constexpr double physical_swe_zero_limit=0.01;// 0.01 mm, if (lw+fw) then convert remaining to outflow
            auto const& multiply_factor=p.get_multiply();
            for (size_t i = 0; i < s.fw.size(); ++i) {
                double psolid_tile = psolid * multiply_factor[i];//.multiply_factor(i);
                double actmelt = limit_fluxes(potmelt, s.fw[i]+psolid_tile);
                double actrefreeze = limit_fluxes(potrefreeze, s.lw[i]);
                double outflow;
                tie(s.fw[i], s.lw[i], outflow) = update_states(s.fw[i], s.lw[i], psolid_tile, pliquid, actmelt, actrefreeze, p.lwmax);
                auto swe_tile =s.fw[i] + s.lw[i];
                if(swe_tile < physical_swe_zero_limit) {
                    outflow += swe_tile;// flush to outflow, keep massbalance
                    s.fw[i]=s.lw[i]=swe_tile=0.0;
                }
                r.swe += p.area_fraction(i) *swe_tile;
                r.outflow += p.area_fraction(i)*outflow/dt_hours; // -> mm/h
                if (s.fw[i] > 0)
                    r.sca +=p.area_fraction(i);
            }
        }
    };
}; // namespace shyft::core::snow_tiles
x_serialize_export_key(shyft::core::snow_tiles::state);
x_serialize_export_key(shyft::core::snow_tiles::parameter);
