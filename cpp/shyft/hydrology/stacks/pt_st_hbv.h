/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <string>
#include <vector>
#include <algorithm>
#include <limits>
#include <stdexcept>

#include <shyft/core/core_serialization.h>

#include <shyft/hydrology/methods/priestley_taylor.h>
#include <shyft/hydrology/methods/hbv_soil.h>
#include <shyft/hydrology/methods/hbv_tank.h>
#include <shyft/hydrology/methods/snow_tiles.h>
#include <shyft/hydrology/methods/glacier_melt.h>
#include <shyft/hydrology/methods/precipitation_correction.h>
#include <shyft/core/unit_conversion.h>
#include <shyft/hydrology/routing.h>
#include <shyft/hydrology/mstack_param.h>

namespace shyft::core::pt_st_hbv {
    using std::vector;
    using std::string;
    using std::runtime_error;


    struct parameter {
        typedef priestley_taylor::parameter pt_parameter_t;
        typedef snow_tiles::parameter snow_parameter_t;
        typedef hbv_soil::parameter hbv_soil_parameter_t;
        typedef hbv_tank::parameter hbv_tank_parameter_t;
        typedef precipitation_correction::parameter precipitation_correction_parameter_t;
        typedef glacier_melt::parameter glacier_parameter_t;
        typedef routing::uhg_parameter routing_parameter_t;
        typedef mstack_parameter mstack_parameter_t;

        pt_parameter_t pt;
        snow_parameter_t st;
        hbv_soil_parameter_t  soil;
        hbv_tank_parameter_t  tank;
        precipitation_correction_parameter_t p_corr;
        glacier_parameter_t gm;
        routing_parameter_t routing;
        mstack_parameter_t msp;
        parameter(const pt_parameter_t& pt,
                    const snow_parameter_t& snow,
                    const hbv_soil_parameter_t& soil,
                    const hbv_tank_parameter_t& tank,
                    const precipitation_correction_parameter_t& p_corr,
                    glacier_parameter_t gm = glacier_parameter_t(),
                    routing_parameter_t routing=routing_parameter_t(),
                    mstack_parameter_t msp=mstack_parameter_t()
                  ) // for backwards compatibility pass default glacier parameter
         : pt{pt}, st{snow}, soil{soil},tank{tank}, p_corr{p_corr}, gm{gm},routing{routing},msp{msp} { /* Do nothing */ }

        parameter()=default;
        parameter(const parameter&)=default;
        parameter(parameter&&)=default;
        parameter& operator=(const parameter &c)=default;
        parameter& operator=(parameter&&c)=default;
        bool operator==(parameter const &o) const {return
            tank == o.tank
            && soil == o.soil
            && st==o.st 
            && gm==o.gm 
            && p_corr==o.p_corr 
            && pt==o.pt 
            && routing==o.routing 
            && msp==o.msp;
        }
        bool operator!=(parameter const &o) const {return !operator==(o);}
        ///< Calibration support, size is the total number of calibration parameters
        size_t size() const { return 28; }
        void set(const vector<double>& p) {
            if (p.size() != size())
                throw runtime_error("pt_st_hbv parameter accessor: .set size missmatch");
            int i = 0;
            soil.fc= p[i++];
            soil.lpdel= p[i++];
            soil.beta =  p[i++];
            soil.infmax =  p[i++];
            tank.uz1 =  p[i++];
            tank.uz2 =  p[i++];;
            tank.kuz0 =  p[i++];
            tank.kuz1 =  p[i++];
            tank.kuz2 =  p[i++];
            tank.perc =  p[i++];
            tank.klz =  p[i++];
            tank.ce =  p[i++];
            tank.cevpl =  p[i++];
            st.set_shape(p[i++]);
            st.tx = p[i++];
            st.cx = p[i++];
            st.ts = p[i++];
            st.lwmax = p[i++];
            st.cfr = p[i++];
            gm.dtf = p[i++];
            p_corr.scale_factor = p[i++];
            pt.albedo = p[i++];
            pt.alpha = p[i++];
            routing.velocity = p[i++];
            routing.alpha = p[i++];
            routing.beta  = p[i++];
            gm.direct_response = p[i++];
            msp.reservoir_direct_response_fraction=p[i++];
        }
        //
        ///< calibration support, get the value of i'th parameter
        double get(size_t i) const {
            switch (i) {
                case 0: return soil.fc;
                case 1: return soil.lpdel;
                case 2: return soil.beta;
                case 3: return soil.infmax;
                case 4: return tank.uz1;
                case 5: return tank.uz2;
                case 6: return tank.kuz0;
                case 7: return tank.kuz1;
                case 8: return tank.kuz2;
                case 9: return tank.perc;
                case 10:return tank.klz;
                case 11:return tank.ce;
                case 12:return tank.cevpl;
                case 13:return st.get_shape();
                case 14:return st.tx;
                case 15:return st.cx;
                case 16:return st.ts;
                case 17:return st.lwmax;
                case 18:return st.cfr;
                case 19:return gm.dtf;
                case 20:return p_corr.scale_factor;
                case 21:return pt.albedo;
                case 22:return pt.alpha;
                case 23:return routing.velocity;
                case 24:return routing.alpha;
                case 25:return routing.beta;
                case 26:return gm.direct_response;
                case 27:return msp.reservoir_direct_response_fraction;
            default:
                throw runtime_error("pt_st_hbv parameter accessor:.get(i) Out of range.");
            }
            return 0.0;
        }

        ///< calibration and python support, get the i'th parameter name
        string get_name(size_t i) const {
            static const char *names[] = {
                "soil.fc",
                "soil.lpdel",
                "soil.beta",
                "soil.infmax",
                "tank.uz1",
                "tank.uz2",
                "tank.kuz0",
                "tank.kuz1",
                "tank.kuz2",
                "tank.perc",
                "tank.klz",
                "tank.ce",
                "tank.cevpl",
                "st.shape",
                "st.tx",
                "st.cx",
                "st.ts",
                "st.lwmax",
                "st.cfr",
                "gm.dtf",
                "p_corr.scale_factor",
                "pt.albedo",
                "pt.alpha",
                "routing.velocity",
                "routing.alpha",
                "routing.beta",
                "gm.direct_response",
                "msp.reservoir_direct_response_fraction"
            };
            if (i >= size())
                throw runtime_error("pt_st_hbv parameter accessor:.get_name(i) Out of range.");
            return names[i];
        }
        
        x_serialize_decl();
    };


    struct state {
        typedef snow_tiles::state snow_state_t;
        typedef hbv_soil::state hbv_soil_state_t;
        using hbv_tank_state_t=hbv_tank::state;

        snow_state_t snow;
        hbv_soil_state_t soil;
        hbv_tank_state_t tank;
        state() {}
        state(const snow_state_t& snow, const hbv_soil_state_t& soil,hbv_tank_state_t const &tank)
         : snow{snow}, soil{soil},tank{tank} { /* Do nothing */ }
        bool operator==(const state& x) const {return snow==x.snow && soil==x.soil && tank==x.tank;}

        /**adjust states with the specified scale-factor
        * to support the process of tuning output of a step
        * to a specified observed/wanted average
        * 
        * Very approximate implementation here
        */
        void adjust_q(double scale_factor) {
            soil.sm *= scale_factor;
            tank.uz *= scale_factor;
            tank.lz *= scale_factor;
        }

        x_serialize_decl();
    };

    struct response {
        typedef priestley_taylor::response pt_response_t;
        typedef snow_tiles::response snow_response_t;
        pt_response_t pt;
        snow_response_t snow;
        hbv_soil::response soil;
        hbv_tank::response tank;
        double gm_melt_m3s{0.0};
        // Stack response
        double total_discharge{0.0};
        double charge_m3s{0.0};
    };



    template<template <typename, typename> class A, class R, class T_TS, class P_TS, class WS_TS, class RH_TS, class RAD_TS, class T,
    class S, class GEOCELLDATA, class P, class SC, class RC >
    void run(const GEOCELLDATA& geo_cell_data,
        const P& parameter,
        const T& time_axis, int start_step, int  n_steps,
        const T_TS& temp,
        const P_TS& prec,
        const WS_TS& /*wind_speed*/,
        const RH_TS& rel_hum,
        const RAD_TS& rad,
        S& state,
        SC& state_collector,
        RC& response_collector
        ) {
        // Access time series input data through accessors of template A (typically a direct accessor).
        using temp_accessor_t = A < T_TS, T > ;
        using prec_accessor_t = A < P_TS, T > ;
        using rel_hum_accessor_t = A < RH_TS, T > ;
        using rad_accessor_t = A < RAD_TS, T > ;

        auto temp_accessor = temp_accessor_t(temp, time_axis);
        auto prec_accessor = prec_accessor_t(prec, time_axis);
        auto rel_hum_accessor = rel_hum_accessor_t(rel_hum, time_axis);
        auto rad_accessor = rad_accessor_t(rad, time_axis);

        R response;

        const double lake_fraction = geo_cell_data.land_type_fractions_info().lake();
        const double reservoir_fraction = geo_cell_data.land_type_fractions_info().reservoir();
        const double glacier_fraction = geo_cell_data.land_type_fractions_info().glacier();
        const double snow_storage_fraction = geo_cell_data.land_type_fractions_info().snow_storage();// on this part, snow builds up, and melts.-> season time-response
        const double land_fraction = 1 - lake_fraction - glacier_fraction - reservoir_fraction;

        const double gm_direct = parameter.gm.direct_response; //glacier melt directly out of cell
        const double gm_routed = 1-gm_direct; // glacier melt routed through hbv
        const double hbvt_routed_water_prec_fraction = reservoir_fraction * (1.0 - parameter.msp.reservoir_direct_response_fraction) + lake_fraction;
        const double res_direct_response_fraction = (reservoir_fraction* parameter.msp.reservoir_direct_response_fraction);
        const double hbv_fraction = 1. - res_direct_response_fraction - glacier_fraction * gm_direct;

        const double cell_area_m2 = geo_cell_data.area();
        const double snow_coverable_area_m2 = cell_area_m2*snow_storage_fraction;
        const double glacier_area_m2 = geo_cell_data.area()*glacier_fraction;

        const bool hbv_response = hbv_fraction * cell_area_m2 > 0.1;// 0.1 m2 resonable zero (do not waste time on routine for very small cells)

        // Initialize the method stack
        precipitation_correction::calculator p_corr(parameter.p_corr.scale_factor);
        priestley_taylor::calculator pt(parameter.pt.albedo, parameter.pt.alpha);
        snow_tiles::calculator snow_tiles(parameter.st);

        hbv_soil::calculator soil(parameter.soil, land_fraction);
        hbv_tank::calculator tank(parameter.tank, lake_fraction);

        if(!hbv_response) { //notice that if no hbv area, there is no hbv response, its set to 0.0
            state.soil={};
            response.soil={};
            state.tank={};
            response.tank={};
        }
        
        const size_t i_begin = n_steps > 0 ? start_step : 0;
        const size_t i_end = n_steps > 0 ? start_step + n_steps : time_axis.size();
        for (size_t i = i_begin; i < i_end; ++i) {
            utcperiod period = time_axis.period(i);
            double temp = temp_accessor.value(i);
            double rad = rad_accessor.value(i);
            double rel_hum = rel_hum_accessor.value(i);
            double prec = p_corr.calc(prec_accessor.value(i));
            state_collector.collect(i, state);///< @note collect the state at the beginning of each period (the end state is saved anyway)

            snow_tiles.step(state.snow, response.snow, period.start, period.end, prec, temp); // outputs mm/h, interpreted as over the entire area
            // note: scale response with snow_storage_fraction

            response.gm_melt_m3s = glacier_melt::step(parameter.gm.dtf, temp, snow_coverable_area_m2*response.snow.sca, glacier_area_m2);// m3/s, that is, how much flow from the snow free glacier parts
            double gm_mmh= shyft::m3s_to_mmh(response.gm_melt_m3s, cell_area_m2);

            if(hbv_response) {
                response.pt.pot_evapotranspiration = pt.potential_evapotranspiration(temp, rad, rel_hum)*to_seconds(calendar::HOUR);// mm/s -> mm/h, interpreted as over the entire area(!)
                soil.step(/*TODO: consider period.start,period.end,*/
                            state.soil,response.soil,
                            response.snow.outflow * land_fraction,
                            response.pt.pot_evapotranspiration,
                            std::max(response.snow.sca,glacier_fraction) // no evap from sca or glacier. using max(...) as we assume snow accumulates on glacier first
                ); ///< all units mm/h over 'same' area
                tank.step(/*period.start,period.end,*/
                            state.tank,response.tank,
                            response.soil.inuz + gm_routed * (gm_mmh + response.snow.outflow * glacier_fraction),
                            prec*hbvt_routed_water_prec_fraction,
                            temp
                ); ///< all units mm/h over 'same' area
            }

            double ae_direct_reservoir = tank.calc_elake(temp);
            response.total_discharge =
                    (std::max(0.0, prec - ae_direct_reservoir) * res_direct_response_fraction  // when it rains, remove evap from direct response
                    +  gm_direct * (gm_mmh + response.snow.outflow * glacier_fraction) // glacier melt direct response + snow melt on glacier, direct response
                    + response.tank.q());
                
            // Not needed for hbv, but for now included in basic cell statistics. For simplicity, set to nan
            response.charge_m3s = shyft::nan;

            // correction for snow_storage_fraction to make it apply pr. total cell
            response.snow.swe *= snow_storage_fraction;// scale to pr. /total/ cell area mm/h
            response.snow.outflow *= snow_storage_fraction;
            // response.snow.sca *= snow_storage_fraction; // for now not doing this

            // Possibly save the calculated values using the collector callbacks.
            response_collector.collect(i, response);
            if(i+1==i_end)
                state_collector.collect(i+1, state);///< @note last iteration,collect the final state as well.

        }
        response_collector.set_end_response(response);
    }

}
x_serialize_export_key(shyft::core::pt_st_hbv::state);
x_serialize_export_key(shyft::core::pt_st_hbv::parameter);
