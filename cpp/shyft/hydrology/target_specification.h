/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <vector>
#include <memory>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/core/core_serialization.h>

namespace shyft::core::model_calibration {
using shyft::time_series::dd::apoint_ts;

/** @brief calc_type to provide simple start of more than NS critera, first extension is diff of sum 2 */
enum target_spec_calc_type {
    NASH_SUTCLIFFE, //
    KLING_GUPTA, ///< ref. Gupta09, Journal of Hydrology 377(2009) 80-91
    ABS_DIFF,///< abs-difference suitable for periodic water-balance calculations, cell-charge uses relative scale max (abs sim).
    RMSE ///< root mean squared and scaled to average observed value
};

/** @brief property_type for target specification */
enum target_property_type {
    DISCHARGE,
    SNOW_COVERED_AREA,
    SNOW_WATER_EQUIVALENT,
    ROUTED_DISCHARGE,
    CELL_CHARGE
};

/** @brief The target_specification describes one component of the overall goal-functions
 * 
 * @details The target specification contains:
 * -# a target ts (the observed quantity)
 * -# a list of catchment ids (zero-based), that denotes the catchment.discharges that should equal the target ts.
 * -# a scale_factor that is used to construct the final goal function as
 *
 * goal_function = sum of all: scale_factor*(1 - nash-sutcliff factor) or KLING-GUPTA
 *
 */

struct target_specification {
    using target_time_series_t=apoint_ts;
    target_specification()
        : scale_factor(1.0), calc_mode(NASH_SUTCLIFFE), catchment_property(DISCHARGE), s_r(1.0), s_a(1.0), s_b(1.0) {
    }
    target_specification(const target_specification& c)
        : ts(c.ts), catchment_indexes(c.catchment_indexes),river_id(c.river_id), scale_factor(c.scale_factor),
        calc_mode(c.calc_mode), catchment_property(c.catchment_property), s_r(c.s_r), s_a(c.s_a), s_b(c.s_b), uid(c.uid) {
    }
    target_specification(target_specification&&c)
        : ts(std::move(c.ts)),
        catchment_indexes(std::move(c.catchment_indexes)),river_id(c.river_id),
        scale_factor(c.scale_factor), calc_mode(c.calc_mode), catchment_property(c.catchment_property),
        s_r(c.s_r), s_a(c.s_a), s_b(c.s_b), uid(c.uid) {
    }
    target_specification& operator=(target_specification&& c) {
        ts = std::move(c.ts);
        catchment_indexes = move(c.catchment_indexes);
        river_id = c.river_id;
        scale_factor = c.scale_factor;
        calc_mode = c.calc_mode;
        catchment_property = c.catchment_property;
        s_r = c.s_r; s_a = c.s_a; s_b = c.s_b;
        uid = c.uid;
        return *this;
    }
    target_specification& operator=(const target_specification& c) {
        if (this == &c) return *this;
        ts = c.ts;
        catchment_indexes = c.catchment_indexes;
        river_id = c.river_id;
        scale_factor = c.scale_factor;
        calc_mode = c.calc_mode;
        catchment_property = c.catchment_property;
        s_r = c.s_r; s_a = c.s_a; s_b = c.s_b;
        uid = c.uid;
        return *this;
    }
    bool operator==(const target_specification& x) const {
        return catchment_indexes == x.catchment_indexes && catchment_property == x.catchment_property && river_id==x.river_id;
    }
    /** @brief Constructs a target specification element for calibration, specifying all needed parameters
        *
        * @param ts the target time-series that contain the target/observed discharge values
        * @param cids  a vector of the catchment ids in the model that together should add up to the target time-series
        * @param scale_factor the weight that this target_specification should have relative to the possible other target_specs.
        * @param calc_mode how to calculate goal-function nash-sutcliffe or the more advanced and flexible kling-gupta
        * @param s_r scale factor r in kling-gupta
        * @param s_a scale factor a in kling-gupta
        * @param s_b scale factor b in kling-gupta
        * @param catchment_property_ determines if this target specification is for DISCHARGE|SNOW_COVERED_AREA|SNOW_WATER_EQUIVALENT
        * @param uid a user supplied uid, string, to help user correlate this target to external data-sources
        */
    target_specification(const apoint_ts& ts, const vector<int64_t>& cids, double scale_factor,
        target_spec_calc_type calc_mode = NASH_SUTCLIFFE, double s_r = 1.0,
        double s_a = 1.0, double s_b = 1.0, target_property_type catchment_property_ = DISCHARGE, std::string uid = "")
        : ts(ts), catchment_indexes(cids), scale_factor(scale_factor),
        calc_mode(calc_mode), catchment_property(catchment_property_), s_r(s_r), s_a(s_a), s_b(s_b), uid(uid) {
    }
    /** @brief Constructs a target specification element for calibration, specifying all needed parameters
    *
    * @param ts the target time-series that contain the target/observed discharge values
    * @param rid  identifies the river id in the model that this target specifies
    * @param scale_factor the weight that this target_specification should have relative to the possible other target_specs.
    * @param calc_mode how to calculate goal-function nash-sutcliffe or the more advanced and flexible kling-gupta
    * @param s_r scale factor r in kling-gupta
    * @param s_a scale factor a in kling-gupta
    * @param s_b scale factor b in kling-gupta
    * @param uid a user supplied uid, string, to help user correlate this target to external data-sources
    */
    target_specification(const apoint_ts& ts, int64_t river_id, double scale_factor,
        target_spec_calc_type calc_mode = NASH_SUTCLIFFE, double s_r = 1.0,
        double s_a = 1.0, double s_b = 1.0, std::string uid = "")
        : ts(ts), river_id(river_id), scale_factor(scale_factor),
        calc_mode(calc_mode), catchment_property(ROUTED_DISCHARGE), s_r(s_r), s_a(s_a), s_b(s_b), uid(uid) {
    }
    apoint_ts ts; ///< The target ts, - any type that is time-series compatible
    std::vector<int64_t> catchment_indexes; ///< the catchment_indexes that denotes the catchments in the model that together should match the target ts
    int64_t river_id=0;///< in case of catchment_property = ROUTED_DISCHARGE, this identifies the river id to get discharge for
    double scale_factor; ///<< the scale factor to be used when considering multiple target_specifications.
    target_spec_calc_type calc_mode;///< *NASH_SUTCLIFFE, KLING_GUPTA
    target_property_type catchment_property;///<  *DISCHARGE,SNOW_COVERED_AREA, SNOW_WATER_EQUIVALENT, ROUTED_DISCHARGE
    double s_r; ///< KG-scalefactor for correlation
    double s_a; ///< KG-scalefactor for alpha (variance)
    double s_b; ///< KG-scalefactor for beta (bias)
    std::string uid;///< external user specified id associated with this target spec.
    x_serialize_decl();
};


}

x_serialize_export_key(shyft::core::model_calibration::target_specification);
