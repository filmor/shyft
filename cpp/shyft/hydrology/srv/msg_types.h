#pragma once


/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <string>
#include <cstdint>
#include <memory>
#include <vector>

#include <boost/variant.hpp> // boost::variant supports serialization
#include <shyft/hydrology/srv/msg_defs.h>

#include <shyft/time/utctime_utilities.h>
#include <shyft/hydrology/api/api.h>  // looking for region environment
#include <shyft/hydrology/api/api_state.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/hydrology/model_calibration.h>

namespace shyft::hydrology::srv {

    using std::vector;
    using std::shared_ptr;
    using std::make_shared;
    using std::string;
    using shyft::core::utctime;
    //using cellstate_t = shyft::api::cell_state_with_id<shyft::core::api::pt_gs_k::state>;
    using shyft::time_series::dd::apoint_ts;
    using shyft::core::region_model;
    using namespace shyft::core;
    using shyft::api::a_region_environment;
    
    using shyft::api::cell_state_with_id;
    using shyft::core::model_calibration::optimizer;
    
    /** wrap state into variant that support serialization and python exposure automagically */
    using state_variant_t=boost::variant<
        shared_ptr<vector<cell_state_with_id<pt_gs_k::state_t>>>,
        shared_ptr<vector<cell_state_with_id<pt_ss_k::state_t>>>,
        shared_ptr<vector<cell_state_with_id<pt_hs_k::state_t>>>,
        shared_ptr<vector<cell_state_with_id<pt_hps_k::state_t>>>,
        shared_ptr<vector<cell_state_with_id<r_pm_gs_k::state_t>>>,
        shared_ptr<vector<cell_state_with_id<pt_st_k::state_t>>>,
        shared_ptr<vector<cell_state_with_id<pt_st_hbv::state_t>>>,
        shared_ptr<vector<cell_state_with_id<r_pt_gs_k::state_t>>>
    >;

    /** wrap state into variant that support serialization and python exposure automagically */
    using parameter_variant_t=boost::variant<
        shared_ptr<pt_gs_k::parameter>,
        shared_ptr<pt_ss_k::parameter>,
        shared_ptr<pt_hs_k::parameter>,
        shared_ptr<pt_hps_k::parameter>,
        shared_ptr<r_pm_gs_k::parameter>,
        shared_ptr<pt_st_k::parameter>,
        shared_ptr<pt_st_hbv::parameter>,
        shared_ptr<r_pt_gs_k::parameter>
    >;
    
    /** enumerate the supported model types above (also exposed as constants to python)
     * The _opt enum should map to the discharge/snow only models used for speed/resource optimal templates.
     */
    enum struct rmodel_type:int8_t {
        pt_gs_k,pt_gs_k_opt,
        pt_ss_k,pt_ss_k_opt,
        pt_hs_k,pt_hs_k_opt,
        pt_hps_k,pt_hps_k_opt,
        r_pm_gs_k,r_pm_gs_k_opt,
        pt_st_k,pt_st_k_opt,
        pt_st_hbv,pt_st_hbv_opt,
        r_pt_gs_k,r_pt_gs_k_opt
    };
    
    using model_variant_t=boost::variant<
        shared_ptr<region_model<pt_gs_k::cell_complete_response_t,a_region_environment>>,
        shared_ptr<region_model<pt_gs_k::cell_discharge_response_t,a_region_environment>>,
        
        shared_ptr<region_model<pt_ss_k::cell_complete_response_t,a_region_environment>>,
        shared_ptr<region_model<pt_ss_k::cell_discharge_response_t,a_region_environment>>,

        shared_ptr<region_model<pt_hs_k::cell_complete_response_t,a_region_environment>>,
        shared_ptr<region_model<pt_hs_k::cell_discharge_response_t,a_region_environment>>,

        shared_ptr<region_model<pt_hps_k::cell_complete_response_t,a_region_environment>>,
        shared_ptr<region_model<pt_hps_k::cell_discharge_response_t,a_region_environment>>,

        shared_ptr<region_model<r_pm_gs_k::cell_complete_response_t,a_region_environment>>,
        shared_ptr<region_model<r_pm_gs_k::cell_discharge_response_t,a_region_environment>>,
        
        shared_ptr<region_model<pt_st_k::cell_complete_response_t,a_region_environment>>,
        shared_ptr<region_model<pt_st_k::cell_discharge_response_t,a_region_environment>>,

        shared_ptr<region_model<pt_st_hbv::cell_complete_response_t,a_region_environment>>,
        shared_ptr<region_model<pt_st_hbv::cell_discharge_response_t,a_region_environment>>,
        
        shared_ptr<region_model<r_pt_gs_k::cell_complete_response_t,a_region_environment>>,
        shared_ptr<region_model<r_pt_gs_k::cell_discharge_response_t,a_region_environment>>

        // hbv-stack not supported due to quality issues.
        
        >;

    using calibration_variant_t=boost::variant<
        shared_ptr< optimizer<region_model<pt_gs_k::cell_complete_response_t,a_region_environment>>>,
        shared_ptr< optimizer<region_model<pt_gs_k::cell_discharge_response_t,a_region_environment>>>,
        
        shared_ptr<optimizer<region_model<pt_ss_k::cell_complete_response_t,a_region_environment>>>,
        shared_ptr<optimizer<region_model<pt_ss_k::cell_discharge_response_t,a_region_environment>>>,

        shared_ptr<optimizer<region_model<pt_hs_k::cell_complete_response_t,a_region_environment>>>,
        shared_ptr<optimizer<region_model<pt_hs_k::cell_discharge_response_t,a_region_environment>>>,

        shared_ptr<optimizer<region_model<pt_hps_k::cell_complete_response_t,a_region_environment>>>,
        shared_ptr<optimizer<region_model<pt_hps_k::cell_discharge_response_t,a_region_environment>>>,

        shared_ptr<optimizer<region_model<r_pm_gs_k::cell_complete_response_t,a_region_environment>>>,
        shared_ptr<optimizer<region_model<r_pm_gs_k::cell_discharge_response_t,a_region_environment>>>,
        
        shared_ptr<optimizer<region_model<pt_st_k::cell_complete_response_t,a_region_environment>>>,
        shared_ptr<optimizer<region_model<pt_st_k::cell_discharge_response_t,a_region_environment>>>,

        shared_ptr<optimizer<region_model<pt_st_hbv::cell_complete_response_t,a_region_environment>>>,
        shared_ptr<optimizer<region_model<pt_st_hbv::cell_discharge_response_t,a_region_environment>>>,
        
        shared_ptr<optimizer<region_model<r_pt_gs_k::cell_complete_response_t,a_region_environment>>>,
        shared_ptr<optimizer<region_model<r_pt_gs_k::cell_discharge_response_t,a_region_environment>>>

        // hbv-stack not supported due to quality issues.
        
        >;

    /** @brief calibration status informs about the drms calibration process 
     */    
    struct calibration_status {
        vector<parameter_variant_t> p_trace; ///< the current parameter trace
        vector<double> f_trace; ///< the current goal function trace
        bool running{false}; ///< true if still running otherwise false
        parameter_variant_t p_result;///< eventual results
        x_serialize_decl();
    };
    
    
    enum optimizer_method:int8_t {
        BOBYQA,
        GLOBAL,
        DREAM,
        SCEUA
    };
    
    /** @brief optimizer parameter and selector
     *
     * @details the purpose to help the drms system protocol by keeping
     * a data-description of the wanted optimizer along with options
     * that differs slightly between each of them.
     */
    struct calibration_options {
        optimizer_method method{optimizer_method::BOBYQA};
        // common
        size_t max_n_iterations{1500};
        utctime time_limit{0};
        // global
        double solver_epsilon{0.0001};
        // sceua
        double x_epsilon{0.0001};
        double y_epsilon{0.0001};
        // bobyqa
        double tr_start{0.1};
        double tr_stop{1e-5};
        calibration_options()=default;
        calibration_options(optimizer_method m,
                            size_t max_n_iterations=1500,
                            utctime time_limit=utctime(0),
                            double solver_epsilon=0.001,
                            double x_epsilon=0.001,
                            double y_epsilon=0.001,
                            double tr_start=0.1,
                            double tr_stop=1e-5)
        :method{m},max_n_iterations{max_n_iterations},
        time_limit{time_limit},
        solver_epsilon{solver_epsilon},
        x_epsilon{x_epsilon},y_epsilon{y_epsilon},
        tr_start{tr_start},tr_stop{tr_stop}{
            
        }
        // dreams have currently no specific limits.
        x_serialize_decl();
    };
}
x_serialize_export_key(shyft::hydrology::srv::calibration_options);
x_serialize_export_key(shyft::hydrology::srv::calibration_status);
