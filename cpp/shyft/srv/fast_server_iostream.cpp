// Copyright (C) 2006  Davis E. King (davis@dlib.net)
// License: Boost Software License   See LICENSE.txt for the full license.
#include <shyft/srv/fast_server_iostream.h>
#include <iostream>

namespace shyft::srv {

const dlib::logger fast_server_iostream::_dLog{"dlib.fast_server_iostream"};

void fast_server_iostream::on_connect (dlib::connection& con) {
    bool my_fault = true;
    uint64 this_con_id=0;
    try
    {
        con.disable_nagle();
        dlib::sockstreambuf buf(&con);
        std::istream in(&buf);
        std::ostream out(&buf);
        in.tie(&out);

        // add this connection to the con_map
        {
            auto_mutex M(m);
            this_con_id = next_id;
            dlib::connection* this_con = &con;
            con_map.add(this_con_id,this_con);
            this_con_id = next_id;
            ++next_id;
        }

        my_fault = false;
        on_connect(
            in,
            out,
            con.get_foreign_ip(),
            con.get_local_ip(),
            con.get_foreign_port(),
            con.get_local_port(),
            this_con_id
        );

        // remove this connection from the con_map
        {
            auto_mutex M(m);
            dlib::connection* this_con{nullptr};
            uint64 junk{0};
            con_map.remove(this_con_id,junk,this_con);
        }

    }
    catch (std::bad_alloc&)
    {
        // make sure we remove this connection from the con_map
        {
            auto_mutex M(m);
            if (con_map.is_in_domain(this_con_id))
            {
                dlib::connection* this_con{nullptr};
                uint64 junk{0};
                con_map.remove(this_con_id,junk,this_con);
            }
        }

        _dLog << dlib::LERROR << "We ran out of memory in server_iostream::on_connect()";
        // if this is an escaped exception from on_connect then let it fly!
        // Seriously though, this way it is obvious to the user that something bad happened
        // since they probably won't have the dlib logger enabled.
        if (!my_fault)
            throw;
    }
}
fast_server_iostream::fast_server_iostream() :
            next_id(0)
        {}

fast_server_iostream::~fast_server_iostream()
        {
            dlib::server::clear();
        }

void fast_server_iostream::shutdown_connection (uint64 id){
    auto_mutex M(m);
    if (con_map.is_in_domain(id)){
        con_map[id]->shutdown();
    }
}

}
