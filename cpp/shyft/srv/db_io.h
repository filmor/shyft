#pragma once
#include <mutex>
#include <shyft/srv/model_info.h>
#include <shyft/core/lru_cache.h>

namespace shyft::srv {
    using std::string;
    using std::vector;
    using std::shared_ptr;
    using std::mutex;
    using std::lock_guard;
    using std::unordered_map;

    using shyft::core::lru_cache;
    using shyft::core::utcperiod;
    using srv::model_info;

    struct db_io{
        mutable std::atomic_int64_t uid{0};

        virtual ~db_io(){};
        virtual vector<model_info> get_model_infos(vector<int64_t>const & mids)=0;
        virtual vector<model_info> get_model_infos(vector<int64_t>const & mids, utcperiod per)=0;
        virtual bool update_model_info(int64_t mid,model_info const &mi)=0;
        virtual int64_t store_model_blob( string const& m, model_info const &mi)=0;
        virtual string read_model_blob(int64_t mid) const=0;
        virtual int64_t remove_model(int64_t mid)=0;

        int64_t mk_unique_model_id() {return ++uid;}

        bool try_get_info_item(int64_t mid,model_info &m) const {
            lock_guard<mutex> guard(mx);
            return info_cache.try_get_item(mid,m);
        }
        void flush_cache() {
            lock_guard<mutex> guard(mx);
            info_cache.flush();
        }
        void set_cache_cap(size_t cap) {
            lock_guard<mutex> guard(mx);
            info_cache.set_capacity(cap);
        }
      protected:
        using info_cache_t= lru_cache<int64_t, model_info, unordered_map>;
        mutable mutex mx; ///< mutex to protect access to c and cs
        mutable info_cache_t info_cache{100000};///<< we aim to keep all info items in cache, max 200k seems reasonable

        void add_info_item(int64_t mid,model_info const&mi) const {
            lock_guard<mutex> guard(mx);
            if(info_cache.size()*2> info_cache.get_capacity()) {
                info_cache.set_capacity(info_cache.size()*2);// grow cap to double sz.
            }
            info_cache.add_item(mid,mi);
            if(mid > uid) uid=mid;//important! update the uid seed to max if needed.
        }

        void remove_info_item(int64_t mid) const {
            lock_guard<mutex> guard(mx);
            info_cache.remove_item(mid);
        }
    };

}
