/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <memory>
#include <string>
#include <sstream>
#include <cstdint>
#include <exception>
#include <regex>
#include <atomic>
#include <fstream>
#include <unordered_map>
#include <list>
#include <mutex>
#include <optional>
#include <shyft/core/core_archive.h>
#include <shyft/time/utctime_utilities.h>

#include <boost/serialization/vector.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/unique_ptr.hpp>
#include <boost/serialization/weak_ptr.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/variant.hpp>
#include <boost/serialization/nvp.hpp>

#include <shyft/core/fs_compat.h>
#include <shyft/srv/model_info.h>
#include <shyft/srv/db_io.h>
#include <shyft/srv/db_level.h>
#include <shyft/core/subscription.h>
#include <shyft/core/lru_cache.h>

namespace shyft::srv {


    using std::string;
    using std::to_string;    
    using std::stoi;
    using std::vector;
    using std::exception;
    using std::runtime_error;
    using std::shared_ptr;
    using std::ifstream;
    using std::ofstream;
    using std::unordered_map;
    using std::list;
    using std::pair;
    using std::mutex;
    using std::lock_guard;

    using shyft::core::core_iarchive;
    using shyft::core::core_oarchive;
    using shyft::core::core_arch_flags;
    using shyft::core::lru_cache;
    using shyft::core::utcperiod;

    using shyft::core::subscription::manager_;

    /** @brief db-storage for models of type M
    *
    * This class is responsible for storing models to a backing-store 
    * file-system, along with model-info, that contains useful, shorthand
    * information about the models.
    * 
    * Assumptions: 
    *    1. one server for each root-directory.
    *    2. the server is responsible for ALL updates/changes to models (so no file-copy without restart server is supported)    
    * 
    * @note : the assumption about one server pr. root_dir can/should 
    *         be enforced using lock file. .e.g. boost::interprocess::file_lock
    * 
    * (model-info,models) are stored with as leveldb in root_dir/mi root_dir/md
    * Old filebased models are automigrated on first acccess from root_dir/( <id>.m.db, <id>.i.db)
    * 
    * @tparam M
    *   A serializable model type
    */
    template<class M, class db_io=db_level>
    struct db{
        using model_t=M;// export model-type
        db_io io;
        manager_ sm = std::make_shared<shyft::core::subscription::manager>();

        db(string const&root_dir):io{root_dir} {}

        shared_ptr<M> read_model(int64_t mid) const {
            auto blob = io.read_model_blob(mid);

            shared_ptr<M> r;
            {
            std::istringstream xmli(blob, std::ios::binary);
            core_iarchive is(xmli,core_arch_flags);
            is>>r;
            }
            r->id=mid;// enforce, late model-id is equal to file/id and content, ref todo in store model above(also enforced on client read side!)
            return r;
        }

        vector<model_info> get_model_infos(vector<int64_t>const & mids){
            return io.get_model_infos(mids);
        }
        vector<model_info> get_model_infos(vector<int64_t>const & mids, utcperiod per){
            return io.get_model_infos(mids, per);
        }
        bool update_model_info(int64_t mid,model_info const &mi){
            auto r = io.update_model_info(mid, mi);
            // A change has been made to model infos, so we notify the subscription manager:
            sm->notify_change("model_infos");
            return r;
        }
        int64_t store_model_blob( string const& m, model_info const &mi){
            auto r = io.store_model_blob(m,mi);
            sm->notify_change({"model_infos", "mid=" + to_string(r)});
            return r;
        }

        int64_t store_model(shared_ptr<M> const& m, model_info const &mi){
            //mi.id = m->id;
            std::ostringstream xmls(std::ios::binary);
            /* scope this, ensure archive is flushed/destroyd */
            {
                core_oarchive oa(xmls,core_arch_flags);
                oa << m;
            }
            xmls.flush();
            return store_model_blob(xmls.str(), mi);
        }

        string read_model_blob(int64_t mid) const {
            return io.read_model_blob(mid);
        }
        int64_t remove_model(int64_t mid){
            auto r = io.remove_model(mid);
            sm->notify_change({"mid=" + to_string(mid), "model_infos"});
            return r;

        }

        bool try_get_info_item(int64_t mid,model_info &m) const{
            return io.try_get_info_item(mid, m);
        }

        int64_t mk_unique_model_id(){
            return io.mk_unique_model_id();
        }

        int64_t find_max_model_id(bool fill_cache=false){
            return io.find_max_model_id(fill_cache);
        }

    };
}
