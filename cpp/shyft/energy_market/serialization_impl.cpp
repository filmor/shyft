/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
//
// 1. first include std stuff and the headers for
// files with serializeation support
//

#include <shyft/energy_market/id_base.h>

#include <shyft/energy_market/market/model.h>
#include <shyft/energy_market/market/model_area.h>
#include <shyft/energy_market/market/power_line.h>
#include <shyft/energy_market/market/power_module.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/hydro_power/hydro_component.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/power_plant.h>
#include <shyft/energy_market/hydro_power/waterway.h>
#include <shyft/energy_market/hydro_power/catchment.h>
#include <shyft/srv/model_info.h>
#include <shyft/energy_market/srv/run.h>
#include <shyft/core/core_archive.h>
#include <shyft/core/core_serialization.h>
#include <shyft/time/utctime_utilities.h>
// then include stuff you need like vector,shared, base_obj,nvp etc.

#include <boost/serialization/vector.hpp>
#include <boost/serialization/set.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/unique_ptr.hpp>
#include <boost/serialization/weak_ptr.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/variant.hpp>
#include <boost/serialization/nvp.hpp>
#include <shyft/core/boost_serialization_flat_map.h>

#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wunused-parameter"
#endif
//
// 2. Then implement each class serialization support
//

namespace shyft::energy_market{
    
    em_handle::destroy_t em_handle::destroy=nullptr; 
    
}

using namespace boost::serialization;
using namespace shyft::core;

template <class Archive>
void shyft::energy_market::srv::run::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & core_nvp("id",id)
    & core_nvp("name",name)
    & core_nvp("created",created)
    & core_nvp("json",json)
    & core_nvp("mid",mid)
    & core_nvp("state",state)
    ;
}


template <class Archive>
void shyft::energy_market::id_base::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & core_nvp("id",id)
    & core_nvp("name",name)
    & core_nvp("json",json)
    ;
    if (file_version > 0) {
        ar
        & core_nvp("tsm",tsm)
        ;
    }
}


template <class Archive>
void shyft::energy_market::market::model::serialize(Archive & ar, const unsigned int file_version) {
    if(file_version < 1) {
        ar & core_nvp("id",id) & core_nvp("name",name) & core_nvp("json",json);
    } else {
        ar&core_nvp("super",base_object<id_base>(*this));
    }
    ar
    & core_nvp("created",created)
    & core_nvp("area",area)
    & core_nvp("power_lines",power_lines)
    ;
}

template <class Archive>
void shyft::energy_market::market::model_area::serialize(Archive & ar, const unsigned int file_version) {
    if(file_version < 1) {
        ar & core_nvp("id", id)& core_nvp("name", name)& core_nvp("json",json);
    } else {
        ar & core_nvp("super",base_object<id_base>(*this));
    }
    ar
    & core_nvp("power_modules", power_modules)
    & core_nvp("hps",detailed_hydro)
    ;
}

template <class Archive>
void shyft::energy_market::market::power_line::serialize(Archive & ar, const unsigned int file_version) {
    if(file_version<1) {
        ar
        & core_nvp("id", id)
        & core_nvp("name", name)
        & core_nvp("json",json)
        ;
    } else {
        ar& core_nvp("super",base_object<id_base>(*this));
    }
    ar
    & core_nvp("area_1", area_1)
    & core_nvp("area_2", area_2)
    ;

}

template <class Archive>
void shyft::energy_market::market::power_module::serialize(Archive & ar, const unsigned int file_version) {
    if(file_version < 1) {
        ar
        & core_nvp("id", id)
        & core_nvp("name", name)
        & core_nvp("json",json)
        ;
    } else {
        ar & core_nvp("super",base_object<id_base>(*this));
    }
}


template <class Archive>
void shyft::energy_market::hydro_power::point::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & core_nvp("x", x)
    & core_nvp("y",y)
    ;
}

template <class Archive>
void shyft::energy_market::hydro_power::xy_point_curve::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & core_nvp("points", points)
    ;
}

template <class Archive>
void shyft::energy_market::hydro_power::xy_point_curve_with_z::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & core_nvp("z", z)
    & core_nvp("xy_curve", xy_curve)
    ;
}

template <class Archive>
void shyft::energy_market::hydro_power::xyz_point_curve::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & core_nvp("curves", curves)
    ;
}


template <class Archive>
void shyft::energy_market::hydro_power::turbine_operating_zone::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & core_nvp("production_min", production_min)
    & core_nvp("production_max", production_max)
    & core_nvp("efficiency_curves", efficiency_curves)
    ;
    if(file_version ==0 && !Archive::is_saving::value) {
        production_nominal=production_max;
        fcr_max=production_max;//bw compat, use max/min as values for fcr
        fcr_min=production_min;
    } else {
        ar
        & core_nvp("production_nominal",production_nominal)
        & core_nvp("fcr_min",fcr_min)
        & core_nvp("fcr_max",fcr_max)
        ;
    }
}

template <class Archive>
void shyft::energy_market::hydro_power::turbine_description::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & core_nvp("efficiencies", operating_zones) // keeping old serialize name, but it does not matter, ignored in core_nvp
    ;
}

template <class Archive>
void shyft::energy_market::hydro_power::hydro_power_system::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & core_nvp("id", id)
    & core_nvp("name",name)
    & core_nvp("created",created)
    & core_nvp("reservoirs",reservoirs)
    & core_nvp("aggregates",units )
    & core_nvp("water_routes",waterways )
    & core_nvp("catchments", catchments)
    & core_nvp("power_stations",power_plants )
    ;
}

template <class Archive>
void shyft::energy_market::hydro_power::hydro_connection::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & core_nvp("role", role)
    & core_nvp("target", target)
    ;
}

template <class Archive>
void shyft::energy_market::hydro_power::hydro_component::serialize(Archive & ar, const unsigned int file_version) {
    if(file_version == 0) {
        ar
        & core_nvp("hps", hps)
        & core_nvp("id",id)
        & core_nvp("name", name)
        & core_nvp("ds", downstreams)
        & core_nvp("us", upstreams)
        ;
    } else {
        ar
        & core_nvp("super",base_object<id_base>(*this))
        & core_nvp("hps",hps)
        & core_nvp("ds",downstreams)
        & core_nvp("us",upstreams)
        ;
    }
}

template <class Archive>
void shyft::energy_market::hydro_power::reservoir::serialize(Archive & ar, const unsigned int file_version) {
    if(file_version==0) {
        ar
        & core_nvp("hc", base_object<shyft::energy_market::hydro_power::hydro_component>(*this))
        & core_nvp("id", id)
        & core_nvp("name", name)
        & core_nvp("json", json)
        ;
    } else {
        ar
        & core_nvp("hc", base_object<shyft::energy_market::hydro_power::hydro_component>(*this))
        ;
    }
}


template <class Archive>
void shyft::energy_market::hydro_power::unit::serialize(Archive & ar, const unsigned int file_version) {
    if(file_version<2 ) {
        ar
        & core_nvp("hc", base_object<shyft::energy_market::hydro_power::hydro_component>(*this))
        & core_nvp("id", id)
        & core_nvp("name", name)
        & core_nvp("json", json)
        ;
        if(file_version ==0 && !Archive::is_saving::value) {
            shyft::energy_market::hydro_power::power_plant_ pp;// first version was a shared ptr.
            ar & core_nvp("station",pp);
            pwr_station=pp;// now it's a weak ptr.
        } else {
            ar & core_nvp("station",pwr_station);
        }
        ;
    } else {
        ar
        & core_nvp("hc",base_object<shyft::energy_market::hydro_power::hydro_component>(*this))
        & core_nvp("station",pwr_station)
        ;
    }
}

template <class Archive>
void shyft::energy_market::hydro_power::power_plant::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & core_nvp("id_base",base_object<shyft::energy_market::id_base>(*this))
    & core_nvp("hps", hps)
    & core_nvp("aggregates",units)
    ;
}

template <class Archive>
void shyft::energy_market::hydro_power::waterway::serialize(Archive& ar, const unsigned int file_version) {
    ar
    & core_nvp("hc", base_object<shyft::energy_market::hydro_power::hydro_component>(*this))
    ;
    if(file_version == 0 ) {
        ar
        & core_nvp("id", id)
        & core_nvp("name", name)
        & core_nvp("json", json)
        ;
    }
    ar
    & core_nvp("gates",gates)
    ;

}

template <class Archive>
void shyft::energy_market::hydro_power::gate::serialize(Archive& ar, const unsigned int file_version) {
    if(file_version<2) {
        if(file_version ==0 && !Archive::is_saving::value) {
            shyft::energy_market::hydro_power::waterway_ ww;// first version was a shared ptr.
            ar & core_nvp("wtr",ww);
            wtr=ww;// now it's a weak ptr.
        } else {
            ar & core_nvp("wtr",wtr);
        }
        ar
        & core_nvp("id", id)
        & core_nvp("name", name)
        & core_nvp("json", json)
        ;
    } else {
        ar
        & core_nvp("super",base_object<shyft::energy_market::id_base>(*this))
        & core_nvp("wtr",wtr)
        ;
    }
}

template <class Archive>
void shyft::energy_market::hydro_power::catchment::serialize(Archive& ar, const unsigned int file_version) {
    if(file_version < 1) {
        ar
        & core_nvp("id", id)
        & core_nvp("name", name)
        & core_nvp("json", json)
        & core_nvp("hps", hps)
        ;
    } else {
        ar
        & core_nvp("super",base_object<shyft::energy_market::id_base>(*this))
        & core_nvp("hps",hps)
        ;
    }
}

//-- fix the rest:
x_serialize_instantiate_and_register(shyft::energy_market::id_base);
//x_serialize_instantiate_and_register(shyft::srv::model_info);
x_serialize_instantiate_and_register(shyft::energy_market::srv::run);
x_serialize_instantiate_and_register(shyft::energy_market::market::model);
x_serialize_instantiate_and_register(shyft::energy_market::market::model_area);
x_serialize_instantiate_and_register(shyft::energy_market::market::power_line);
x_serialize_instantiate_and_register(shyft::energy_market::market::power_module);
x_serialize_instantiate_and_register(shyft::energy_market::hydro_power::point);
x_serialize_instantiate_and_register(shyft::energy_market::hydro_power::xy_point_curve);
x_serialize_instantiate_and_register(shyft::energy_market::hydro_power::xy_point_curve_with_z);
x_serialize_instantiate_and_register(shyft::energy_market::hydro_power::xyz_point_curve);
x_serialize_instantiate_and_register(shyft::energy_market::hydro_power::turbine_operating_zone);
x_serialize_instantiate_and_register(shyft::energy_market::hydro_power::turbine_description);
x_serialize_instantiate_and_register(shyft::energy_market::hydro_power::reservoir);
x_serialize_instantiate_and_register(shyft::energy_market::hydro_power::hydro_connection);
x_serialize_instantiate_and_register(shyft::energy_market::hydro_power::hydro_component);
x_serialize_instantiate_and_register(shyft::energy_market::hydro_power::hydro_power_system);
x_serialize_instantiate_and_register(shyft::energy_market::hydro_power::unit);
x_serialize_instantiate_and_register(shyft::energy_market::hydro_power::power_plant );
x_serialize_instantiate_and_register(shyft::energy_market::hydro_power::waterway );
x_serialize_instantiate_and_register(shyft::energy_market::hydro_power::gate);
x_serialize_instantiate_and_register(shyft::energy_market::hydro_power::catchment);
