#pragma once

namespace shyft::energy_market {

    /**
     * @brief handle for a python object.
     */
    struct em_handle{

        typedef void(*destroy_t)(void*);
        void *obj{nullptr};
        static destroy_t destroy;
        
        em_handle(){};
        explicit em_handle(void*o):obj{o}{}
        em_handle(const em_handle&)=delete;
        em_handle& operator=(em_handle const&)=delete;
        
        em_handle(em_handle&& o) {
            obj = o.obj;
            o.obj = nullptr;
        }
        
        em_handle& operator=(em_handle&& o) {
            if(&o != this) {
                cleanup();
                obj = o.obj;
                o.obj = nullptr;
            }
            return *this;    
        }
        
        ~em_handle(){
            cleanup();
        }
        
        private:
            void cleanup() {
                if(obj && destroy){
                    (*destroy)(obj);
                    obj = nullptr;
                } 
            }
    };
    
} // namespace shyft::energy_market
