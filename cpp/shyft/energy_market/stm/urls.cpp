#include <shyft/energy_market/stm/urls.h>

#include <functional>
#include <string>
#include <string_view>
#include <utility>
#include <variant>
#include <vector>

#include <shyft/core/utility.h>
#include <shyft/energy_market/stm/model.h>
#include <shyft/web_api/energy_market/grammar/ts_url.h>
#include <shyft/web_api/energy_market/tp_id.h>

namespace shyft::energy_market::stm {

  std::vector<std::string> url_planning_inputs(std::string_view model_id, const stm_system &model) {
    std::vector<std::string> result;
    url_with_planning_inputs(model_id, model, [&](ignore_t, std::string_view url) {
      result.push_back(std::string(url));
    });
    return result;
  }

  std::vector<std::string> url_planning_outputs(std::string_view model_id, const stm_system &model) {
    std::vector<std::string> result;
    url_with_planning_outputs(model_id, model, [&](ignore_t, std::string_view url) {
      result.push_back(std::string(url));
    });
    return result;
  }

  bool url_parse(url_parse_buffer &buffer, const char *ptr) {
    // FIXME: move this - jeh
    std::apply(
      [](auto &...buffer) {
        (buffer.clear(), ...);
      },
      buffer);
    shyft::web_api::grammar::stm_url_grammar<const char *> grammar{};
    return web_api::grammar::phrase_parser(ptr, grammar, buffer);
  }

  bool url_set_attr(stm_system &model, const url_parse_buffer &buffer, const any_attr &attr) {
    auto &&[model_id, comp_path, attr_path] = buffer;
    return stm::url_resolve(
      model,
      comp_path,
      attr_path,
      [&]<typename T>(T &&attribute) {
        return std::visit(
          [&](auto &&target_attribute) {
            if constexpr (requires { attribute = std::move(target_attribute); }) {
              attribute = std::move(target_attribute);
              return true;
            }
            return false;
          },
          attr);
      },
      [] {
        return false;
      });
  }

  std::optional<any_attr> url_get_attr(const stm_system &model, const url_parse_buffer &buffer) {
    auto &&[model_id, comp_path, attr_path] = buffer;
    return stm::url_resolve(
      model,
      comp_path,
      attr_path,
      [&](auto &&attribute) -> std::optional<any_attr> {
        if constexpr (requires { any_attr{attribute}; })
          return any_attr{attribute};
        return std::nullopt;
      },
      [&]() -> std::optional<any_attr> {
        return std::nullopt;
      });
  }

}
