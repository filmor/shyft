#pragma once
#include <string>
#include <vector>
#include <memory>
#include <shyft/mp.h>
#include <shyft/core/core_serialization.h>
#include <shyft/energy_market/id_base.h>
#include <shyft/energy_market/stm/stm_system.h>

namespace shyft::energy_market::stm {
    using std::string;
    using std::vector;
    using std::shared_ptr;

    /** @brief Network
     *
     * Transmission network, containing busbars connected by transmission_lines
     */
    struct network : id_base {
        using super = id_base;

        network() { mk_url_fx(this); }
        network(int id, const string& name, const string& json, const stm_system_& sys)
            : super{id,name,json,{},{}},sys{sys} { mk_url_fx(this); }

        bool operator==(const network& other) const;
        bool operator!=(const network& other) const { return !(*this == other); }

        /** @brief generate an almost unique, url-like string for a reservoir.
         *
         * @param rbi: back inserter to store result
         * @param levels: How many levels of the url to include.
         * 		levels == 0 includes only this level. Use level < 0 to include all levels.
         * @param placeholders: The last element of the vector states wethers to use the reservoir ID
         * 		in the url or a placeholder. The remaining vector will be used in subsequent levels of the url.
         * 		If the vector is empty, the function defaults to not using placeholders.
         * @return
         */
        void generate_url(std::back_insert_iterator<string>& rbi, int levels = -1, int template_levels = -1) const;

        stm_system_ sys_() const { return sys.lock(); }
        stm_system__ sys; ///< Reference up to the 'owning' optimization system.
        //notice: when adding attributes here, no automagic, you have to do version step!
        BOOST_HANA_DEFINE_STRUCT(network,
            (vector<transmission_line_>, transmission_lines), ///< List of transmission lines in the network
            (vector<busbar_>, busbars) ///< List of busbars in the network
        );

        x_serialize_decl();
    };
    using network_=shared_ptr<network>;
    using network__=weak_ptr<network>;

    struct network_builder {
        network_ n;
        explicit network_builder(network_ net) : n(net) {}
        transmission_line_ create_transmission_line(int id, const string& name, const string& json);
        busbar_ create_busbar(int id, const string& name, const string& json);
    };
}

x_serialize_export_key(shyft::energy_market::stm::network);
