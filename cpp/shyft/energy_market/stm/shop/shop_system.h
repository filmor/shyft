/*
 * Integration of stm with shop api via proxy.
 */
#pragma once

#include <memory>
#include <vector>
#include <iosfwd>

#include <shyft/energy_market/stm/shop/shop_adapter.h>
#include <shyft/energy_market/stm/shop/shop_emitter.h>
#include <shyft/energy_market/stm/shop/shop_command.h>
#include <shyft/energy_market/stm/shop/shop_commander.h>
#include <shyft/energy_market/stm/shop/export/shop_export.h>

namespace shyft::energy_market::stm::shop {

  using shyft::core::to_seconds64;

  /** @brief shop_system allows optimization  of a stm_system
   *  @details
   *  The class is responsible for facilitating and keeping the
   *  correlated state between the descriptive stm system model
   *  and the corresponding optimization shop-model.
   *  It delegates the work to shop_emitter, shop_adapter and shop_commander.
   *
   *
   */
  struct shop_system {
    shop_api api;         ///< Shop api proxy object, could be private, but is set public for testing.
    shop_adapter adapter; ///< Basic stm to shop object adapter, takes care of 1-1 emitting tasks helper for emitter.
    shop_emitter emitter; ///< The shop exchange interface, that is used to orchestrate shop, notice that emitter have
                          ///< refs to the above api and emitter.
    shop_commander commander; ///< Shop is a stateful live system and commander helps us interact with the instance.

    time_axis::generic_dt time_axis; ///< Optimization time-axis.

    // Constructors
    shop_system(const time_axis::generic_dt& time_axis, string prefix = "");
    ~shop_system();

    string get_version_info() {
      return api.get_version_info();
    }

    void set_logging_to_stdstreams(bool on = true) {
      api.set_logging_to_stdstreams(on);
    }

    void set_logging_to_files(bool on = true) {
      api.set_logging_to_files(on);
    }

    std::vector<log_entry> get_log_buffer(int limit = 1024) {
      return api.get_log_buffer(limit);
    }

    void emit(const stm_system& stm); ///< Emit stm system to shop, typical step 1 of an optimization.
    void collect(stm_system& stm);    ///< Collect results from shop into stm system, typical step 3 of an optimization.
    void complete(stm_system& stm);   ///< Update stm system, performing post-process calculations based on collected
                                      ///< results from shop, typical step 4 of an optimization.

    // Get some default/singleton shop objects not completely represented
    // in stm but may be relevant (temporary solution, to be refactored?)
    shop_global_settings get_global_settings();
    shop_objective get_average_objective();

    // Export the entire system as yaml formatted string (using function from shop api)
    string export_yaml(bool input_only = false, bool compress_txy = false, bool compress_connection = false) {
      return api.dump_yaml(input_only, compress_txy, compress_connection);
    }

    // Export topology graph as dot formatted string into stream (custom implementation)
    void export_topology(bool all, bool raw, std::ostream& destination) const;
    // Export data as json formatted string into stream (custom implementation)
    void export_data(bool all, std::ostream& destination) const;

    // Ease of use functions
    static void optimize(
      stm_system& stm,
      const generic_dt& time_axis,
      const std::vector<shop_command>& commands,
      bool logging_to_stdstreams = false,
      bool logging_to_files = false,
      string prefix = "");

    // Low level utility functions
    void command_raw(
      string command) { // Note: Send commands using the newer api function to supply entire command string directly,
                        // instead of using our own ShopCommand/ShopCommander, but note that this lacks support for
                        // quoted arguments and therefore also e.g. slash and space in object arguments, and it bypasses
                        // some features implemented in shop_system::command.
      api.execute_cmd_string(command);
    }

    vector<string>
      get_executed_commands_raw() { // Note: Using a newer api feature to return raw command strings directly from the
                                    // api, which will not parse them into our own shop_command, but note that this does
                                    // not wrap quotes around oobject arguments containing spaces etc and will therefore
                                    // not necessarily show a valid command string accepted by command_raw.
      return api.get_executed_cmd_strings();
    }

    static void environment(std::ostream& destination);

    static void set_time_axis(shop_api& api, const utcperiod& period, const utctimespan& t_step);
    static void set_time_axis(shop_api& api, const shyft::time_axis::generic_dt& time_axis);
  };

}
