/*
 * Define external log functions called from Shop.lib (global callbacks).
 *
 * The current sintef SHOP legacy logging interface lacks
 * support for proper multithreaded operations, both when it
 * comes to the technical interface (global external hooks),
 * and the interface of those hooks(lacking the context from which
 * the log messages belongs to (which optimization, which system?)
 * Work by Sintef is ongoing to overcome this.
 *
 * Not in use.
 */

void SHOP_log_info(char* ) {}
void SHOP_log_warning(char* ) {}
void SHOP_log_error(char* ) {}
void SHOP_exit(void*) {}
