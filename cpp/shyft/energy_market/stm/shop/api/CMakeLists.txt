#
# CMake build script for  C++ API around
# Sintef's C API for Shop.
#
#


#
# Settings
#

include(FetchContent)

string(TOLOWER ${CMAKE_SYSTEM_NAME} PLATFORM_ID) # To be able to use ${PLATFORM_ID} where generator expression is not supported ($<LOWER_CASE:$<PLATFORM_ID>>)

# Shop
set(SHOP_VERSION_DEFAULT 15.2.1.2)
set(SHOP_VERSION ${SHOP_VERSION_DEFAULT} CACHE STRING "Shop version to build for")
option(INCLUDE_SHOP_LICENSE "Include Shop license file in the package" ON)
option(INCLUDE_CPLEX_LIBRARY "Include Cplex runtime library in the package" ON)
set(CPLEX_VERSION 20.1.0 CACHE STRING "Version of Cplex runtime library to include in the package when option INCLUDE_CPLEX_LIBRARY is set (used only to set default value of CPLEX_FILE)")

if(NOT DEFINED ENV{SHYFT_SHOP_PKG_TOKEN} OR NOT DEFINED ENV{SHYFT_SHOP_PKG_URL})
    message(FATAL_ERROR
        "When SHYFT_WITH_SHOP is enabled,\n then env. variables SHYFT_SHOP_PKG_TOKEN and SHYFT_SHOP_PKG_URL must be provided."
        "Using gitlab generic package registry as source the format of these should be like:\n"
        "SHYFT_SHOP_PKG_TOKEN=PRIVATE-TOKEN:token-goes-here-nospace\n"
        "SHYFT_SHOP_PKG_URL=https://gitlab.com/api/v4/projects/projectid-goes-here/packages/generic\n"
        "and we expect packages license,cplex,shop to be available for the requested versions. For reference:\n"
        "https://docs.gitlab.com/ee/user/packages/generic_packages"
        )
else()
    set(SHYFT_SHOP_PKG_TOKEN "$ENV{SHYFT_SHOP_PKG_TOKEN}")
    set(SHYFT_SHOP_PKG_URL "$ENV{SHYFT_SHOP_PKG_URL}")
endif()

FetchContent_Declare(
    license
    URL ${SHYFT_SHOP_PKG_URL}/license/1.0/license.tar.gz
    HTTP_HEADER  ${SHYFT_SHOP_PKG_TOKEN}
    DOWNLOAD_EXTRACT_TIMESTAMP true
)

FetchContent_Declare(
    cplex
    URL ${SHYFT_SHOP_PKG_URL}/cplex/${CPLEX_VERSION}/cplex.tar.gz
    HTTP_HEADER  ${SHYFT_SHOP_PKG_TOKEN}
    DOWNLOAD_EXTRACT_TIMESTAMP true
)

FetchContent_Declare(
    shop
    URL ${SHYFT_SHOP_PKG_URL}/shop/${SHOP_VERSION}/shop.tar.gz
    HTTP_HEADER  ${SHYFT_SHOP_PKG_TOKEN}
    DOWNLOAD_EXTRACT_TIMESTAMP true
)

FetchContent_MakeAvailable(license cplex shop)  # done immediately, and cached in build-dir/_deps

set(SHOP_DIR ${shop_SOURCE_DIR})
#if (NOT EXISTS ${SHOP_DIR})
#    message(FATAL_ERROR "Shop directory does not exist: ${SHOP_DIR}")
#endif()
set(SHOP_LIB_DIR ${SHOP_DIR}/lib/${PLATFORM_ID})
set(SHOP_INCLUDE_DIR ${SHOP_DIR}/include)

# Cplex optimizer shared library external runtime dependency
string(REPLACE "." "" CPLEX_NAME "cplex${CPLEX_VERSION}")
set(CPLEX_LIB_DIR ${cplex_SOURCE_DIR}/lib/${PLATFORM_ID})
set(CPLEX_FILE ${CPLEX_LIB_DIR}/${CMAKE_SHARED_LIBRARY_PREFIX}${CPLEX_NAME}${CMAKE_SHARED_LIBRARY_SUFFIX} CACHE FILEPATH "Cplex runtime library to include in the package when option INCLUDE_CPLEX_LIBRARY is set (overrides CPLEX_VERSION)")

# Shop license file
set(SHOP_LICENSE_FILE ${license_SOURCE_DIR}/SHOP_license.dat CACHE FILEPATH "Shop license file to include in the package when option INCLUDE_SHOP_LICENSE is set")
get_filename_component(SHOP_LICENSE_FILE_NAME ${SHOP_LICENSE_FILE} NAME) # Extract name from license path, referenced from shop_api-config.cmake.in


##
# Expose main build variables
#
message("-- shop_api ${SHOP_VERSION}")
set(shop_api_VERSION ${SHOP_VERSION})
set(shop_api_INCLUDE_DIRS ${SHOP_INCLUDE_DIR})
set(shop_api_LIBRARY_ROOT_DIR ${SHOP_LIB_DIR})
if(MSVC)
    set(shop_api_LIBRARY_DIR ${shop_api_LIBRARY_ROOT_DIR}/$<IF:$<CONFIG:Debug>,debug,release>)
    set(shop_api_LIBRARIES
        optimized ${shop_api_LIBRARY_ROOT_DIR}/release/${CMAKE_STATIC_LIBRARY_PREFIX}shop${CMAKE_STATIC_LIBRARY_SUFFIX}
        debug ${shop_api_LIBRARY_ROOT_DIR}/debug/${CMAKE_STATIC_LIBRARY_PREFIX}shop${CMAKE_STATIC_LIBRARY_SUFFIX})
else()
    set(shop_api_LIBRARY_DIR ${shop_api_LIBRARY_ROOT_DIR}/release)
    set(shop_api_LIBRARIES
        ${shop_api_LIBRARY_ROOT_DIR}/release/${CMAKE_STATIC_LIBRARY_PREFIX}shop${CMAKE_STATIC_LIBRARY_SUFFIX})
endif()

# Handle optional Shop license

find_shop_license_file(FALSE)
if(shop_license_file AND shop_license_statement AND shop_license_date)
    find_package_message(shop_license "Found Shop license: ${shop_license_statement} (issued ${shop_license_date})" "[${shop_license_file}][${shop_license_statement}][${shop_license_date}]")
else()
    find_package_message(shop_license "No Shop license: Only unlicensed features will be supported" "[]")
endif()

set(CPLEX_INTERFACE_FILE ${shop_api_LIBRARY_DIR}/shop_cplex_interface${CMAKE_SHARED_LIBRARY_SUFFIX})

# Expose variable for shop license - optional runtime dependency

#
# When exposing the c++ api, ensure to provide the deps
#

#
# Expose target
#
add_library(shop_api INTERFACE)
target_include_directories(shop_api INTERFACE
    $<BUILD_INTERFACE:${shop_api_INCLUDE_DIRS}>  # when building internally, this is how we work
    $<INSTALL_INTERFACE:${SHYFT_INSTALL_INCLUDEDIR}>  # shyft development component installed, use shyft include  TODO: check
)

target_link_libraries(shop_api INTERFACE ${shop_api_LIBRARIES} ${CMAKE_DL_LIBS})
# Libdl required by libshop.a when building with glibc older than 2.34 where it is not included, used to run-time dynamic link shop_cplex_interface.so
#target_link_options(shop_api INTERFACE $<$<AND:$<CXX_COMPILER_ID:MSVC>,$<CONFIG:RelWithDebInfo>>:/IGNORE:4099>) # Shop release version is without debug information so might want to supress MSVC warning about missing .pdb files

target_compile_definitions(shop_api INTERFACE
    SHYFT_WITH_SHOP
    SHOP_API_VERSION=${shop_api_VERSION}
)


#
# Installing for development, as part of shyft c++/api
#
install(FILES ${shop_SOURCE_DIR}/include/shop_lib_interface.h
    COMPONENT development
    EXCLUDE_FROM_ALL
    DESTINATION ${SHYFT_INSTALL_INCLUDEDIR}
)

install(FILES ${SHOP_LICENSE_FILE}
    COMPONENT development
    EXCLUDE_FROM_ALL
    DESTINATION ${SHYFT_INSTALL_LIBDIR}
)

install(
    FILES
        ${shop_api_LIBRARY_DIR}/${CMAKE_STATIC_LIBRARY_PREFIX}shop${CMAKE_STATIC_LIBRARY_SUFFIX}
        ${shop_api_LIBRARY_DIR}/shop_cplex_interface${CMAKE_SHARED_LIBRARY_SUFFIX}
    COMPONENT development
    EXCLUDE_FROM_ALL
    DESTINATION ${SHYFT_INSTALL_LIBDIR}
    PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)
#
# Install Shop symbol files into debug subdirectory of lib, if performing debug build with Visual C++.
#
if (MSVC)
    install(
        FILES
            ${SHOP_LIB_DIR}/debug/shop.pdb
            ${SHOP_LIB_DIR}/debug/shop_cplex_interface.pdb
        COMPONENT development
        EXCLUDE_FROM_ALL
        DESTINATION ${SHYFT_INSTALL_LIBDIR}
        CONFIGURATIONS Debug
        PERMISSIONS OWNER_READ OWNER_WRITE GROUP_READ WORLD_READ)
endif()

# Install Cplex optimizer shared library external runtime dependency into lib (optional)
if (INCLUDE_CPLEX_LIBRARY)
    message(STATUS "Including Cplex library version ${CPLEX_VERSION}")
    if (NOT EXISTS ${CPLEX_FILE})
        message(WARNING "Cplex library \"${CPLEX_FILE}\" does not exist. You must add it before performing install, or reconfigure with option INCLUDE_CPLEX_LIBRARY turned off.")
    endif()
    install(
        FILES ${CPLEX_FILE}
        COMPONENT development
        EXCLUDE_FROM_ALL
        DESTINATION ${SHYFT_INSTALL_LIBDIR}
        PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)
else()
    message(STATUS "Not including Cplex library (option INCLUDE_CPLEX_LIBRARY=${INCLUDE_CPLEX_LIBRARY})")
endif()


#
# Expose variables useful for shyft build
#   parent scope on variables that goes out (we would like to minimize those in favour of target properties)
set(shop_license_file "${shop_license_file}" PARENT_SCOPE)
if(INCLUDE_SHOP_LICENSE)
    set(shop_api_LICENSE_FILE ${SHOP_LICENSE_FILE} PARENT_SCOPE)
endif()
set(shop_api_CPLEX_INTERFACE_FILE "${CPLEX_INTERFACE_FILE}" PARENT_SCOPE)
# Expose variable for cplex optimizer shared library - optional runtime dependency
if(INCLUDE_CPLEX_LIBRARY)
    set(shop_api_CPLEX_FILE ${CPLEX_FILE} PARENT_SCOPE)
endif()

if(SHYFT_WITH_SHOP_API_GEN)
    message(STATUS "Generating executable for creating new c++/api for sintef/shop")
    option(SHYFT_WITH_SHOP_API_REGEN "Run executable to automatically generate headers after build" OFF)
    # Executable tool
    set(target shop_cxx_api_generator)
    # Exluded from all since the consumers of these header files do not have a dependency
    # set up on this target. You need to manually run this before you build the downstream
    # targets.
    add_executable(${target} EXCLUDE_FROM_ALL main.cpp)
    target_link_libraries(${target} shop_api)

    if(SHYFT_WITH_SHOP_API_REGEN)
        # Custom command to execute the executable and generate headers with pre-defined paths.
        message(STATUS "Will run executable post build to generate default headers")
        message(STATUS "  ${CMAKE_CURRENT_LIST_DIR}/shop_enums.h")
        message(STATUS "  ${CMAKE_CURRENT_LIST_DIR}/shop_api.h")
        add_custom_command(TARGET ${target} POST_BUILD
            COMMAND ${target} "\"${CMAKE_CURRENT_LIST_DIR}/shop_enums.h\"" "\"${CMAKE_CURRENT_LIST_DIR}/shop_api.h\""
            COMMENT "Running executable post build to generate default headers")
    endif()
endif()
