#pragma once
#include <shop_lib_interface.h> // External Shop library
#include <algorithm>
#include <iostream>
#include <map>
#include <memory>
#include <ranges>
#include <string>
#include <vector>

namespace shop_cxx_api_generator {

using namespace std;
using namespace string_literals;

static const char* indentation = "    ";

//
// Helper function for printing an enum from specified set of entries
//
template<typename T> // T = iterable string-string (name-identifier) array-like type, typically vector<string>.
void print_enum1(ostream& os, const string& enumName, const T& enumEntries) {
    os << "enum class " << enumName << "{\n";
    for (auto it = cbegin(enumEntries); it != cend(enumEntries); ++it)
        os << indentation << *it << ",\n";
    os << "};\n"
        << "using " << enumName << "_info_t = std::tuple<" << enumName << ", const char* const>;\n"
        << "static constexpr " << enumName << "_info_t " << enumName << "_info[] {\n";
    for (auto it = cbegin(enumEntries); it != cend(enumEntries); ++it)
        os << indentation << "{ " << enumName << "::" << *it << ", \"" << *it << "\"},\n";
    os << "};\n";
};
template<typename T> // T = iterable string-string (name-identifier) map-like type, typically map<string,string> if order does not matter, or vector<pair<string,string>> if insert order needs to be kept.
void print_enum2(ostream& os, const string& enumName, const T& enumEntries) {
    os << "enum class " << enumName << "{\n";
    for (auto it = cbegin(enumEntries); it != cend(enumEntries); ++it)
        os << indentation << it->second << ",\n";
    os << "};\n"
        << "using " << enumName << "_info_t = std::tuple<" << enumName << ", const char* const>;\n"
        << "static constexpr " << enumName << "_info_t " << enumName << "_info[] {\n";
    for (auto it = cbegin(enumEntries); it != cend(enumEntries); ++it)
        os << indentation << "{ " << enumName << "::" << it->second << ", \"" << it->first << "\"},\n";
    os << "};\n";
};
template<typename T> // T = iterable string-{string,int} (name-{identifier,value}) map-like type, typically map<string,pair<string,int>> if order does not matter, or vector<pair<string,pair<string,int>>> if insert order needs to be kept.
void print_enum3(ostream& os, const string& enumName, const T& enumEntries) {
    os << "enum class " << enumName << "{\n";
    for (auto it = cbegin(enumEntries); it != cend(enumEntries); ++it)
        os << indentation << it->second.first << " = " << it->second.second << ",\n";
    os << "};\n"
        << "using " << enumName << "_info_t = std::tuple<" << enumName << ", const char* const>;\n"
        << "static constexpr " << enumName << "_info_t " << enumName << "_info[] {\n";
    for (auto it = cbegin(enumEntries); it != cend(enumEntries); ++it)
        os << indentation << "{ " << enumName << "::" << it->second.first << ", \"" << it->first << "\"},\n";
    os << "};\n";
};

//
// Generate enum representing object types
//
void print_object_types(ShopSystem* shop, ostream& os)
{
    const int n = ShopGetObjectTypeCount(shop);
    vector<pair<string,string>> enumEntries;
    enumEntries.reserve(n);
    for (int i = 0; i < n; ++i) {
        const char* v = ShopGetObjectTypeName(shop, i);
        enumEntries.emplace_back(v, v);
    }
    print_enum2(os, "shop_object_type", enumEntries);
}

//
// Generate various enums and structs for representing attribute information 
//
auto print_attribute_info_types(ShopSystem* shop, ostream& os,
    map<string, string>& dataTypes, map<string, string>& xUnits, map<string, string>& yUnits)
{
    // Collect attribute info
    auto makeUnitIdentifier = [](string unit) { // Helper func for converting unit string into valid enum identifier
        if (unit == "%") {
            unit = "percent";
        } else {
            size_t p = unit.find('/', 0);
            while (p != string::npos) {
                unit[p] = '_';
                unit.insert(p + 1, "per_");
                p = unit.find('/', p + 5);
            }
            transform(unit.begin(), unit.end(), unit.begin(), ::tolower);
        }
        return unit;
    };
    for (int i = 0, n = ShopGetObjectTypeCount(shop); i < n; ++i) {
        int nAttributes = 256; // NB: Must be large enough to include all attributes for every type (as of Shop API v14.4.1.1 type 13, global_settings, is largest with 164 attributes)
        int attributeIndexList[256]; // Array of size according to nAttributes
        ShopGetObjectTypeAttributeIndices(shop, i, nAttributes, attributeIndexList);
        for (int i = 0; i < nAttributes; ++i) {
            int attributeIndex = attributeIndexList[i];
            char objectTypeName[64]; char attributeName[64];
            char attributeDataFuncName[64]; char attributeDatatype[64];
            char xUnit[64]; char yUnit[64];
            bool isObjectParam, isInput, isOutput;
            ShopGetAttributeInfo(shop, attributeIndex, objectTypeName, isObjectParam,
                attributeName, attributeDataFuncName, attributeDatatype, xUnit, yUnit, isInput, isOutput);
            string dataTypeIdentifier, xUnitIdentifier, yUnitIdentifier;
            if (dataTypes.find(attributeDatatype) == dataTypes.end())
                dataTypes[attributeDatatype] = "shop_"s + attributeDatatype;
            if (xUnits.find(xUnit) == xUnits.end())
                xUnits[xUnit] = makeUnitIdentifier(xUnit);
            if (yUnits.find(yUnit) == yUnits.end())
                yUnits[yUnit] = makeUnitIdentifier(yUnit);
        }
    }

    // Print attribute data type enum
    print_enum2(os, "shop_data_type", dataTypes);
    os << "\n";

    // Print attribute X unit enum
    print_enum2(os, "shop_x_unit", xUnits);
    os << "\n";

    // Print attribute Y unit enum
    print_enum2(os, "shop_y_unit", yUnits);
    os << "\n";

    // Print attribute info struct
    os
        << "struct shop_attribute_info {\n"
        << indentation << "shop_data_type data_type;\n"
        << indentation << "shop_x_unit x_unit;\n"
        << indentation << "shop_y_unit y_unit;\n"
        << indentation << "bool is_object_parameter;\n"
        << indentation << "bool is_input;\n"
        << indentation << "bool is_output;\n"
        << "};\n";
}

//
// Generate enum representing attribute types for a single object type
//
void print_attribute_types_for_object_type(ShopSystem* shop, ostream& os, const int objectTypeIndex,
    const map<string, string>& dataTypes, const map<string, string>& xUnits, const map<string, string>& yUnits)
{
    const char* const objectTypeName = ShopGetObjectTypeName(shop, objectTypeIndex);
    os << "enum class shop_" << objectTypeName << "_attribute " << "{\n";
    int nAttributes = 256; // NB: Must be large enough to include all attributes for every type (as of Shop API v14.4.1.1 type 13, global_settings, is largest with 164 attributes)
    int attributeIndexList[256]; // Array of size according to nAttributes
    ShopGetObjectTypeAttributeIndices(shop, objectTypeIndex, nAttributes, attributeIndexList);
    for (int i = 0; i < nAttributes; ++i) {
        int attributeIndex = attributeIndexList[i];
        const char* attributeName = ShopGetAttributeName(shop, attributeIndex);
        os << indentation << attributeName << " = " << attributeIndex << ",\n";
    }
    os << "};\n";
    if (nAttributes > 0) {
        os  << "using " << objectTypeName << "_attribute_info_t = std::tuple<shop_" << objectTypeName << "_attribute, const char* const, shop_attribute_info>;\n"
            << "static constexpr " << objectTypeName << "_attribute_info_t shop_" << objectTypeName << "_attribute_info[] {\n";
        for (int i = 0; i < nAttributes; ++i) {
            int attributeIndex = attributeIndexList[i];
            char objectTypeName[64]; char attributeName[64];
            char attributeDataFuncName[64]; char attributeDatatype[64];
            char xUnit[64]; char yUnit[64];
            bool isObjectParam, isInput, isOutput;
            ShopGetAttributeInfo(shop, attributeIndex, objectTypeName, isObjectParam,
                attributeName, attributeDataFuncName, attributeDatatype, xUnit, yUnit, isInput, isOutput);
            string dataTypeIdentifier, xUnitIdentifier, yUnitIdentifier;
            if (dataTypes.find(attributeDatatype) != dataTypes.end())
                dataTypeIdentifier = dataTypes.at(attributeDatatype);
            if (xUnits.find(xUnit) != xUnits.end())
                xUnitIdentifier = xUnits.at(xUnit);
            if (yUnits.find(yUnit) != yUnits.end())
                yUnitIdentifier = yUnits.at(yUnit);
            os  << indentation << "{ shop_" << objectTypeName << "_attribute::" << attributeName << ", "
                << "\"" << attributeName << "\", "
                << "{ "
                << "shop_data_type::" << dataTypeIdentifier << ", "
                << "shop_x_unit::" << xUnitIdentifier << ", "
                << "shop_y_unit::" << yUnitIdentifier << ", "
                << "/*is_object_parameter*/ " << (isObjectParam ? "true" : "false") << ", "
                << "/*is_input*/ " << (isInput ? "true" : "false") << ", "
                << "/*is_output*/ " << (isOutput ? "true" : "false")
                << " }"
                << " },\n";
        }
        os << "};\n";
    }
}

//
// Generate enum representing attribute types for a single object type
//

string api_class_name(string_view sintef_name) {
    if (sintef_name == "generator") return "unit";
    if (sintef_name == "plant") return "power_plant";
    return string(sintef_name);
}

//
// Extract enum entries representing relation types
//
inline vector<string> get_relation_types(ShopSystem* shop,const char * objectTypeName)
{
    int nRelationTypes, maxRelationTypeLength;
    if (!ShopGetRelationTypesDimensions(shop, objectTypeName, nRelationTypes, maxRelationTypeLength) || nRelationTypes <= 0)
        return {};
    vector<unique_ptr<char[]>> relationTypeBuffers(nRelationTypes);
    for (int i = 0; i < nRelationTypes; ++i) {
        relationTypeBuffers[i] = make_unique_for_overwrite<char[]>(maxRelationTypeLength + 1);
        relationTypeBuffers[i].get()[0] = '\0';
    }
    vector<char *> relationTypePtrs(nRelationTypes);
    ranges::transform(relationTypeBuffers,
                      ranges::begin(relationTypePtrs),
                      [](auto &r){ return r.get(); });

    if (!ShopGetRelationTypes(shop, objectTypeName, relationTypePtrs.data()))
        return {};

    vector<string> relationTypes(nRelationTypes);
    ranges::transform(relationTypePtrs,
                      ranges::begin(relationTypes),
                      [](auto *p){
                          return string(p);
                      });
    return relationTypes;
}

void print_shop_proxy_class_begin(ostream&os, string_view class_name, int o_type) {
    auto c_name = api_class_name(class_name);
    os << "template<class A>\n";
    os << "struct " << c_name << ":obj<A," << o_type << "> {\n";
    os << "    using super=obj<A," << o_type << ">;\n";
    os << "    " << c_name << "()=default;\n";
    os << "    " << c_name << "(A* s,int oid):super(s, oid) {}\n";
    os << "    " << c_name << "(const " << c_name << "& o):super(o) {}\n";
    os << "    " << c_name << "(" << c_name << "&& o):super(std::move(o)) {}\n";
    os << "    " << c_name << "& operator=(const " << c_name << "& o) {\n";
    os << "        super::operator=(o);\n";
    os << "        return *this;\n";
    os << "    }\n";
    os << "    " << c_name << "& operator=(" << c_name << "&& o) {\n";
    os << "        super::operator=(std::move(o));\n";
    os << "        return *this;\n";
    os << "    }\n";
    os << "    // attributes\n";
}
void print_shop_proxy_attribute(ostream&os, string_view class_name, string_view attr_name, int aid, string_view a_type, bool read_only,string_view x_unit,string_view y_unit) {
    string attr_type;
    if (a_type == "int") attr_type = "int";
    else if (a_type == "int_array") attr_type = "vector<int>";
    else if (a_type == "double") attr_type = "double";
    else if (a_type == "double_array") attr_type = "vector<double>";
    else if (a_type == "txy") attr_type = "typename A::_txy";
    else if (a_type == "xy") attr_type = "typename A::_xy";
    else if (a_type == "xy_array") attr_type = "vector<typename A::_xy>";
    else if (a_type == "xyt") attr_type = "map<int64_t, typename A::_xy>";
    if (attr_type.size()) {
        os << "    " << (read_only ? "ro" : "rw") << "<" << api_class_name(class_name) << "," << aid << "," << attr_type << "," << x_unit << "," << y_unit << "> " << attr_name << "{this}; // x[" << x_unit << "],y[" << y_unit << "]\n";
    } else {
        os << "    //--TODO: " << (read_only ? "ro" : "rw") << "<" << api_class_name(class_name) << "," << aid << "," << a_type << "," << x_unit << "," << y_unit << "> " << attr_name << "{this}; // x[" << x_unit << "],y[" << y_unit << "]\n";
        cout << "Skipping proxy attribute for " << class_name << "." << attr_name << " of unsupported type " << a_type << "\n";
    }
}
void print_shop_proxy_class_end(ostream& os) {
    os << "};\n";
}

void print_shop_proxy_relation_enum(ShopSystem *shop, ostream &os, const char *objectTypeName) {
    if(auto relationTypes = get_relation_types(shop, objectTypeName);
       !relationTypes.empty())
    {
        os << indentation << "enum relation {\n";
        for(const auto &relationType:relationTypes){
            auto enumEntry = relationType;
            ranges::replace(enumEntry, ' ', '_');
            os << indentation << indentation << enumEntry << ",\n" ;
        }
        os << indentation << "};\n";
        os << indentation << "static constexpr const char *relation_name(relation R){\n"
           << indentation << indentation << "switch(R){\n"
            ;
        for(const auto &relationType:relationTypes){
            auto enumEntry = relationType;
            ranges::replace(enumEntry, ' ', '_');
            os << indentation << indentation << "case relation::" << enumEntry << ": return \"" << relationType << "\";\n" ;
        }
        os << indentation << indentation << "default: return nullptr;\n"
           << indentation << indentation << "}\n"
           << indentation << "}\n"
            ;
    }
}

void print_shop_proxy_for_object_type(ShopSystem* shop, ostream& os, const int objectTypeIndex,
    const map<string, string>& dataTypes, const map<string, string>& xUnits, const map<string, string>& yUnits)
{
    const char* const objectTypeName = ShopGetObjectTypeName(shop, objectTypeIndex);
    print_shop_proxy_class_begin(os, objectTypeName, objectTypeIndex);

    print_shop_proxy_relation_enum(shop, os, objectTypeName);

    int nAttributes = 256; // NB: Must be large enough to include all attributes for every type (as of Shop API v14.4.1.1 type 13, global_settings, is largest with 164 attributes)
    int attributeIndexList[256]; // Array of size according to nAttributes
    ShopGetObjectTypeAttributeIndices(shop, objectTypeIndex, nAttributes, attributeIndexList);
    for (auto attributeIndex : std::views::take(attributeIndexList,nAttributes)) {
        char objectTypeName[64]; char attributeName[64];
        char attributeDataFuncName[64]; char attributeDatatype[64];
        char xUnit[64]; char yUnit[64];
        bool isObjectParam, isInput, isOutput;
        ShopGetAttributeInfo(shop, attributeIndex, objectTypeName, isObjectParam,
            attributeName, attributeDataFuncName, attributeDatatype, xUnit, yUnit, isInput, isOutput);
        string dataTypeIdentifier, xUnitIdentifier, yUnitIdentifier;
        if (dataTypes.find(attributeDatatype) != dataTypes.end())
            dataTypeIdentifier = dataTypes.at(attributeDatatype);
        if (xUnits.find(xUnit) != xUnits.end())
            xUnitIdentifier = xUnits.at(xUnit);
        if (yUnits.find(yUnit) != yUnits.end())
            yUnitIdentifier = yUnits.at(yUnit);
        print_shop_proxy_attribute(os, objectTypeName, attributeName, attributeIndex, attributeDatatype, isOutput,xUnitIdentifier,yUnitIdentifier);
    }
    print_shop_proxy_class_end(os);
}

void print_shop_proxy_header_begin(ostream& os) {
    os << "#pragma once\n";
    os << "// auto genereated files based on active version of Sintef SHOP api dll\n";
    os << "#include \"shop_proxy.h\"\n";
    os << "namespace shop {\n\n";
    os << "using proxy::obj;\n";
    os << "using proxy::rw;\n";
    os << "using proxy::ro;\n";
    os << "using namespace proxy::unit;\n\n";
}

void print_shop_proxy_header_end(ostream& os) {
    os << "\n}\n";
}

//
// Generate enum representing attribute types for all object types
//
void print_attribute_types(ShopSystem* shop, ostream& os, ostream&proxy_os)
{
    // Print attributes for each object, and collect unique attribute information such as data type, unit etc.
    map<string, string> dataTypes, xUnits, yUnits;
    print_attribute_info_types(shop, os, dataTypes, xUnits, yUnits);
    print_shop_proxy_header_begin(proxy_os);
    for (int i = 0, n = ShopGetObjectTypeCount(shop); i < n; ++i) {
        os << "\n";
        print_attribute_types_for_object_type(shop, os, i, dataTypes, xUnits, yUnits);
        print_shop_proxy_for_object_type(shop,proxy_os, i, dataTypes, xUnits, yUnits);
    }
    print_shop_proxy_header_end(proxy_os);
}

//
// Generate enum representing relation types
//
void print_relation_types(ShopSystem* shop, ostream& os)
{
    for (int i = 0, n = ShopGetObjectTypeCount(shop); i < n; ++i) {
        const char* const objectTypeName = ShopGetObjectTypeName(shop, i);
        vector<pair<string, string>> enumEntries;
        ranges::transform(get_relation_types(shop, objectTypeName),
                          back_inserter(enumEntries),
                          [](const string &relationType){
                              auto enumEntry = relationType;
                              ranges::replace(enumEntry, ' ', '_');
                              return pair{relationType,enumEntry};
                          });
        if(!enumEntries.empty())
            print_enum2(os, string("shop_") + objectTypeName + "_relation", enumEntries);

    }
}

//
// Generate enum representing command types
//
void print_command_types(ShopSystem* shop, ostream& os)
{
    const int nCommandTypeBuffer = 1000;
    vector<char*> commandTypes(nCommandTypeBuffer);
    for (int i = 0; i < nCommandTypeBuffer; ++i)
        commandTypes[i] = new char[128];
    int nCommandTypes = nCommandTypeBuffer;
    if (ShopGetCommandTypesInSystem(shop, nCommandTypes, &commandTypes[0])) {
        vector<pair<string, string>> enumEntries;
        for (int i = 0; i < nCommandTypes; ++i) {
            string enumEntry = commandTypes[i];
            replace(enumEntry.begin(), enumEntry.end(), ' ', '_');
            enumEntries.emplace_back(commandTypes[i], enumEntry);
        }
        print_enum2(os, "shop_command", enumEntries);
    }
    for (int i = 0; i < nCommandTypeBuffer; ++i)
        delete[] commandTypes[i];
}

//
// Generate various other enums and structs
//
void print_other(ShopSystem* /*shop*/, ostream& os)
{
    print_enum2(os, "shop_severity", vector<pair<string, string>>({
        {"ok","OK"}
    }));
    os << "\n";
    print_enum1(os, "shop_time_resolution_unit", vector<string>({
        {"hour"},
        {"minute"}
    }));
}
}
