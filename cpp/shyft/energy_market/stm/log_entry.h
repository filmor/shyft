#pragma once
#include <cstdint>
#include <string>
#include <boost/hana/define_struct.hpp>
#include <shyft/core/core_serialization.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::energy_market::stm {

enum class log_severity : uint8_t {
    information,
    warning,
    error
};

struct log_entry {
    BOOST_HANA_DEFINE_STRUCT(log_entry,
        (log_severity,severity),
        (std::string,message),
        (int,code),
        (shyft::core::utctime,time));

    auto operator<=>(const log_entry &) const = default;

    x_serialize_decl();
};

}
