#pragma once
#include <shyft/srv/msg_defs.h>

namespace shyft::energy_market::stm::srv::task {
    /** @brief extension of message types from shyft::energy_market::srv
     */
    struct message_type : shyft::srv::message_type {
        static constexpr type ADD_CASE = 8;
        static constexpr type REMOVE_CASE_ID = 9;
        static constexpr type REMOVE_CASE_NAME = 10;
        static constexpr type GET_CASE_ID = 11;
        static constexpr type GET_CASE_NAME = 12;
        static constexpr type ADD_MODEL_REF = 13;
        static constexpr type REMOVE_MODEL_REF = 14;
        static constexpr type GET_MODEL_REF = 15;
        static constexpr type FX = 16;
        static constexpr type UPDATE_CASE = 17;

    };

    using msg=shyft::core::msg_util<message_type>; ///< so that we get low level functions adapted to our enum type
}
