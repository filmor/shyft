#include <shyft/energy_market/stm/srv/compute/client.h>

#include <algorithm>
#include <cstdint>
#include <functional>
#include <iterator>
#include <memory>
#include <optional>
#include <ranges>
#include <stdexcept>
#include <string>
#include <string_view>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <variant>
#include <vector>

#include <fmt/core.h>

#include <shyft/energy_market/stm/context.h>
#include <shyft/energy_market/stm/model.h>
#include <shyft/energy_market/stm/shop/shop_command.h>
#include <shyft/energy_market/stm/urls.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/dd/compute_ts_vector.h>
#include <shyft/time_series/dd/resolve_ts.h>
#include <shyft/time_series/time_axis.h>

namespace shyft::energy_market::stm::srv::compute {

  namespace {
    struct eval_ts_hash {
      using is_transparent = void;

      template <typename T>
      auto operator()(const T &t) const {
        return std::hash<T>{}(t);
      }
    };

    struct eval_ts_ {
      using dtss_map_ = std::unordered_map< std::string, time_series::dd::ipoint_ts_ref, eval_ts_hash, std::equal_to<>>;
      using dstm_map_ = std::unordered_map<std::string, time_series::dd::ipoint_ts_ref, eval_ts_hash, std::equal_to<>>;

      dtss::server &dtss;
      std::string_view model_id;
      const stm_system &model;

      dtss_map_ dtss_map;
      dstm_map_ dstm_map;

      std::vector<std::pair<time_series::dd::apoint_ts, std::string>> ts_attrs;

      std::shared_ptr<const time_series::dd::ipoint_ts>
        resolve(std::shared_ptr<const time_series::dd::ipoint_ts> &ref, std::string_view url) {

        if (!ref->needs_bind()) // only resolve ref's that needs bind, keep old values
          return ref;
        auto url_model_id = stm::url_peek_model_id(url);
        if (url_model_id.empty()) { // not dstm://, then dtss, remember unique set of urls, and the  the patches
          auto it = dtss_map.find(url);
          if (it == dtss_map.end())
            return dtss_map.emplace(std::string(url), ref->clone_expr()).first->second;
          else
            return it->second;
        } else if (url_model_id != model_id)
          throw std::runtime_error(fmt::format(
            "failed to resolve dstm timeseries: '{}', mismatched models '{}' != '{}'", url, model_id, url_model_id));

        if (auto already_resolved = dstm_map.find(url); already_resolved != dstm_map.end())
          return already_resolved->second;

        auto attr = url_get_attr(model, std::string(url));
        if (!attr || !std::get_if<time_series::dd::apoint_ts>(&*attr))
          return nullptr;
        auto ts_attr = std::get_if<time_series::dd::apoint_ts>(&*attr);
        if (!ts_attr->ts)
          return nullptr;
        auto result = ts_attr->needs_bind() ? ts_attr->clone_expr().ts : ts_attr->ts;
        return dstm_map.emplace(url, result).first->second;
      }

      void add(const time_series::dd::apoint_ts &ts, std::string_view url) {
        ts_attrs.push_back({ts.needs_bind() ? ts.clone_expr() : ts, std::string(url)});
      }

      void operator()(const auto &bind_period) {
        for (auto &[ts, url] : ts_attrs)
          if (!time_series::dd::resolve_ts(
                [&](auto &&...x) {
                  return resolve(SHYFT_FWD(x)...);
                },
                ts))
            throw std::runtime_error(fmt::format("failed to resolve dstm timeseries: '{}'", url));


        if (!dtss_map.empty()) {
          std::vector<std::string> dtss_urls(dtss_map.size());
          std::ranges::copy(std::views::keys(dtss_map), dtss_urls.data());

          const bool use_cached_ts_read = true;
          const bool update_ts_cache = false;
          auto resolved_dtss_refs = dtss.do_read(dtss_urls, bind_period, use_cached_ts_read, update_ts_cache);
          if (resolved_dtss_refs.size() != dtss_urls.size())
            throw std::runtime_error("failed to resolve dtss timeseries");

          // FIXME: in C++23 use zip - jeh
          auto dtss_it = dtss_map.begin();
          for (auto i : std::views::iota(std::size_t{0}, dtss_urls.size())) {
            auto &dtss_ref = (dtss_it++)->second;
            apoint_ts(dtss_ref).bind(resolved_dtss_refs[i]);
          }
        }

        for (auto &[ts, url] : ts_attrs)
          ts.do_bind();

        {
          auto [it, end] = std::ranges::remove_if(ts_attrs, [&](auto &ts) {
            return !ts.first.ts;
          });
          ts_attrs.erase(it, end);
        }
        if (ts_attrs.empty())
          return;

        time_series::dd::ats_vector tsv(ts_attrs.size());
        for (auto i : std::views::iota(0ul, tsv.size()))
          tsv[i] = std::move(ts_attrs[i].first);
        tsv = time_series::dd::deflate_ts_vector<time_series::dd::apoint_ts>(tsv);
        for (auto i : std::views::iota(0ul, tsv.size()))
          ts_attrs[i].first = std::move(tsv[i]);
      }
    };
  }

  set_attrs_request set_plan_attrs_request(
    dtss::server &server,
    std::string_view model_id,
    const stm_system &model,
    const generic_dt &time_axis) {

    eval_ts_ eval_ts{server, model_id, model,{},{},{}};
    set_attrs_request request;
    url_with_planning_inputs(model_id, model, [&]<typename T>(T attr, std::string_view url) {
      if constexpr (std::is_same_v<T, time_series::dd::apoint_ts>)
        eval_ts.add(attr, url);
      else
        request.attrs.push_back(std::pair{std::string(url), any_attr{attr}});
    });

    // FIXME: add to protocol!
    const auto bind_period = [&] {
      auto period = time_axis.total_period();
      return shyft::core::utcperiod(period.start - shyft::core::deltahours(24 * 7), period.end);
    }();
    eval_ts(bind_period);
    request.attrs.reserve(request.attrs.size() + eval_ts.ts_attrs.size());
    std::ranges::transform(eval_ts.ts_attrs, std::back_inserter(request.attrs), [&](auto &&ts_attr) {
      return std::pair{std::move(ts_attr.second), any_attr{std::move(ts_attr.first)}};
    });
    return request;
  }

  get_attrs_request get_plan_attrs_request(std::string_view model_id, const stm_system &model) {
    get_attrs_request request{.urls = stm::url_planning_outputs(model_id, model)};
    return request;
  }

  shyft::core::utctime send_input_and_plan(
    dtss::server &dtss,
    std::string_view model_id,
    stm_system &model,
    client &client,
    const generic_dt &time_axis,
    const std::vector<shop::shop_command> &shop_commands) {

    const auto shop_commands_timelimit = shop::calc_suggested_timelimit(shop_commands);
    model.run_params.run_time_axis = time_axis;
    client.send(set_plan_attrs_request(dtss, model_id, model, time_axis));
    client.plan({.time_axis = time_axis, .commands = std::move(shop_commands)});
    return shop_commands_timelimit;
  }

  void get_plan_result(
    std::string_view model_id,
    shyft::core::subscription::manager *dtss_sm,
    shyft::core::subscription::manager &sm,
    stm_system &model,
    client &client) {
    {
      auto [summary] = client.get_plan({});
      *(model.summary) = *summary;
    }

    auto get_attrs_request = compute::get_plan_attrs_request(model_id, model);
    auto &result_urls = get_attrs_request.urls;
    auto [result] = client.send(get_attrs_request);

    {
      // TODO: avoid copies (have set_attrs take a range), use zip in 23 - jeh
      std::vector<std::pair<std::string, any_attr>> attrs;
      attrs.reserve(result.size());
      std::ranges::copy(
        std::views::transform(
          std::views::filter(
            std::views::iota(std::size_t{0}, result_urls.size()),
            [&](auto index) {
              return result[index] != std::nullopt;
            }),
          [&](auto index) {
            return std::pair{result_urls[index], *result[index]};
          }),
        std::back_inserter(attrs));
      stm::url_set_attrs(model, attrs);
    }

    std::string model_id_(model_id);
    rebind_expression(model, model_id_, true); // FIXME: string-view - jeh
    for (const auto &g : model.unit_groups)
      g->update_sum_expressions();

    auto model_prefix = url_fmt_header(model_id);

    if (dtss_sm)
      dtss_sm->notify_change(result_urls);

    sm.notify_change(result_urls);
    for (const auto &g : model.unit_groups)
      sm.notify_change(g->all_urls(model_prefix));
    sm.notify_change(model.summary->all_urls(model_prefix));
    sm.notify_change(model.run_params.all_urls(model_prefix));
  }

}
