#pragma once

#include <atomic>
#include <cstdint>
#include <exception>
#include <iosfwd>
#include <memory>
#include <shared_mutex>
#include <string>
#include <vector>

#include <dlib/uintn.h>

#include <shyft/energy_market/stm/log_entry.h>
#include <shyft/energy_market/stm/srv/compute/protocol.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/srv/fast_server_iostream.h>

namespace shyft::energy_market::stm::srv::compute {

  struct server : shyft::srv::fast_server_iostream {

    struct worker_ {
      std::string model_id;
      std::shared_ptr<stm_system> model;
      std::vector<log_entry> log;
      std::exception_ptr exception;
      std::jthread thread;
    };

    std::shared_mutex mutex;
    compute::state state;
    worker_ worker;

    int start_server();

    /** @brief handle one client connection
     *
     * Reads messages/requests from the client,
     * - act and perform request,
     * - return response
     * for as long as the client keep the connection
     * open. Only one client supported.
     *
     */
    void on_connect(
      std::istream& in,
      std::ostream& out,
      const std::string& /*foreign_ip*/,
      const std::string& /*local_ip*/,
      unsigned short /*foreign_port*/,
      unsigned short /*local_port*/,
      dlib::uint64 /*connection_id*/
      ) override;
  };

}
