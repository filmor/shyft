#include <shyft/energy_market/stm/srv/compute/server.h>

#include <algorithm>
#include <concepts>
#include <chrono>
#include <csignal>
#include <exception>
#include <iostream>
#include <iterator>
#include <future>
#include <memory>
#include <optional>
#include <stdexcept>
#include <string>
#include <string_view>
#include <thread>
#include <utility>
#include <vector>

#include <boost/serialization/vector.hpp>
#include <boost/serialization/optional.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/outcome/result.hpp>
#include <dlib/logger.h>
#include <fmt/core.h>
#include <fmt/ranges.h>

#include <shyft/core/core_archive.h>
#include <shyft/core/core_serialization.h>
#include <shyft/core/dlib_utils.h>
#include <shyft/core/reflection.h>
#include <shyft/core/reflection/serialization.h>
#if defined(SHYFT_WITH_SHOP)
#include <shyft/energy_market/stm/shop/shop_system.h>
#endif
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/urls.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/aref_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/web_api/energy_market/grammar/ts_url.h>

namespace shyft::energy_market::stm::srv::compute {

  namespace {
    dlib::logger logger{"compute"};
  }

  int server::start_server() {
    if (get_listening_port() == 0) {
      start_async();
      while (is_running() && get_listening_port() == 0)
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    } else {
      start_async();
    }
    auto port_num = get_listening_port();
    state = compute::state::idle;
    return port_num;
  }

  struct session {

    const std::string &ip;
    unsigned short port;

    void log(dlib::log_level l, std::string_view m) const {
      logger << l << "[dstm-compute]:" << fmt::format("from client {}:{} - ", ip, port) << m;
    }
  };

  template <message_tag M>
  inline auto expect_server_state(server &server, const request<M> &, std::same_as<state> auto... expected_states) {
    if (state actual_state = server.state; ((actual_state != expected_states) && ...))
      throw std::runtime_error(fmt::format(
        "Unexpected state for {} request, expected one of '{}', got '{}'.",
        M,
        std::array{expected_states...},
        actual_state));
  }

  inline auto handle_worker_error(server &server) {
    if (server.worker.exception)
      std::rethrow_exception(std::exchange(server.worker.exception, nullptr));
  }

  auto make_watchdog_task(server &server, session &session, auto watchdog_timeout, auto worker_task) {
    return [&, watchdog_timeout = std::move(watchdog_timeout), worker_task = std::move(worker_task)]() mutable {
      std::promise<void> worker_promise{};
      auto worker_future = worker_promise.get_future();
      std::jthread([&] {
        try {
          worker_task();
        } catch (...) {
          {
            std::unique_lock lock{server.mutex};
            server.worker.exception = std::current_exception();
          }
        }
        worker_promise.set_value();
      }).detach();
      if (worker_future.wait_for(watchdog_timeout) == std::future_status::timeout) {
        session.log(dlib::LERROR, "Timeout reached, terminating.");
        std::terminate();
      }
      {
        std::unique_lock lock{server.mutex};
        server.state = state::done;
      }
    };
  }

  inline auto expect_model(std::string_view model_id, const std::shared_ptr<stm_system> &model) {
    if (!model)
      throw std::runtime_error(fmt::format("Invalid model '{}'.", model_id));
  }

  inline auto unbind_model(stm_system &model) {
    stm_ts_operation tsop{[](time_series::dd::apoint_ts &ats) {
      ats.do_unbind();
      return true;
    }};
    tsop.apply(model);
  }

  inline auto expect_model_input(session &session, std::string_view model_id, stm_system &model) {
    std::vector<std::string> unbound_attrs;
    url_with_planning_inputs(model_id, model, [&]<typename Attr>(const Attr &attr, std::string_view url) {
      if constexpr (std::is_same_v<Attr, time_series::dd::apoint_ts>)
        if (attr.needs_bind())
          unbound_attrs.push_back(std::string(url));
    });
    if (!unbound_attrs.empty())
      session.log(dlib::LWARN, fmt::format("Unbound model '{}': {}.", model_id, unbound_attrs));
  }

  inline auto reset_model_output(std::string_view model_id, stm_system &model) {
    url_with_planning_outputs(model_id, model, [&]<typename Attr>(Attr &attr, [[maybe_unused]] std::string_view url) {
      if constexpr (std::is_same_v<Attr, time_series::dd::apoint_ts>)
        attr = time_series::dd::apoint_ts{};
    });
  }

  start_reply handle(server &server, session &, start_request &&request) {
    expect_model(request.model_id, request.model);
    unbind_model(*request.model);
    std::unique_lock lock{server.mutex};
    expect_server_state(server, request, state::idle);
    server.state = state::started;
    server.worker = server::worker_{
      .model_id = std::move(request.model_id),
      .model = std::move(request.model),
      .log = {},
      .exception = {},
      .thread = {}};
    return {};
  }

  get_status_reply handle(server &server, session &, const get_status_request &request) {
    std::shared_lock lock{server.mutex};
    get_status_reply r{.state = server.state, .log{}};
    if (request.log_index)
      std::ranges::copy(
        std::views::drop(server.worker.log, std::min(server.worker.log.size(), *request.log_index)),
        std::back_inserter(r.log));
    return r;
  }

  get_attrs_reply handle(server &server, session &, const get_attrs_request &request) {
    std::shared_lock lock{server.mutex};
    expect_server_state(server, request, state::started, state::done);
    handle_worker_error(server);
    return {.attrs = stm::url_get_attrs(*server.worker.model, request.urls)};
  }

  set_attrs_reply handle(server &server, session &, const set_attrs_request &request) {
    std::unique_lock lock(server.mutex);
    expect_server_state(server, request, state::started, state::done);
    handle_worker_error(server);
    return {.attrs = stm::url_set_attrs(*server.worker.model, request.attrs)};
  }

  stop_reply handle(server &server, session &, const stop_request &request) {
    std::unique_lock lock{server.mutex};
    expect_server_state(server, request, state::started, state::done);
    server.state = state::idle;
    server.worker = server::worker_{};
    return {};
  }

  struct plan_task {

    compute::server &server;
    time_axis::generic_dt time_axis;
    std::vector<shop::shop_command> commands;

    void operator()() {
#if !defined(SHYFT_WITH_SHOP)
      throw std::runtime_error("plan requests are not supported by this server");
#else
      shop::shop_system shop_model{time_axis, fmt::format("dstm://M{}", server.worker.model_id)};
      auto collect_shop_logs = [&] {
        // NOTE: assume shop get_log_buffer call is thread safe - jeh
        auto shop_log = shop_model.get_log_buffer();
        if (shop_log.empty())
          return;
        std::unique_lock lock{server.mutex};
        auto &stm_log = server.worker.log;
        stm_log.reserve(stm_log.size() + stm_log.size());
        std::ranges::copy(shop_log, std::back_inserter(stm_log));
      };

      auto &stm_model = *server.worker.model;

      {
        auto wants_single_operating_zone = [](const auto &command) {
          return command.keyword == "print" && command.specifier == "bp_curves" && command.has_nohikkemikk();
        };
        shop_model.adapter.enforce_single_operating_zone = std::ranges::any_of(commands, wants_single_operating_zone);
      }
      shop_model.emit(stm_model);

      collect_shop_logs();
      for (auto &command : commands) {
        command.remove_nohikkemikk();
        shop_model.commander.execute(command);
        collect_shop_logs();
      }
      shop_model.collect(stm_model);
      shop_model.complete(stm_model);
      collect_shop_logs();
#endif
    }
  };

  plan_reply handle(server &server, session &session, plan_request &&request) {
    plan_reply reply;

    {
      std::unique_lock lock{server.mutex};
      expect_server_state(server, request, state::started, state::done);
      server.state = state::running;
      server.worker.log.clear();
    }
    expect_model_input(session, server.worker.model_id, *server.worker.model);
    reset_model_output(server.worker.model_id, *server.worker.model);
    const auto timeout = calc_suggested_timelimit(request.commands);
    server.worker.thread = std::jthread(make_watchdog_task(
      server,
      session,
      timeout,
      plan_task{.server = server, .time_axis = std::move(request.time_axis), .commands = std::move(request.commands)}));
    return reply;
  }

  get_plan_reply handle(server &server, session &, const get_plan_request &request) {
    std::shared_lock lock(server.mutex);
    expect_server_state(server, request, state::done);
    handle_worker_error(server);
    return {.summary = server.worker.model->summary};
  }

  void server::on_connect(
    std::istream &in,
    std::ostream &out,
    const std::string &foreign_ip,
    [[maybe_unused]] const std::string &local_ip,
    unsigned short foreign_port,
    [[maybe_unused]] unsigned short local_port,
    [[maybe_unused]] dlib::uint64 connection_id) {
    session session{.ip = foreign_ip, .port = foreign_port};

    try {
      message_tag tag{};
      while (in.peek() != EOF) {
        if(!reflection::read_blob(in, tag))
          break;
        with_enum_or(
          tag,
          [&](auto tag) {
            request<tag> request{};
            {
              shyft::core::core_iarchive in_archive(in, shyft::core::core_arch_flags);
              in_archive >> request;
            }
            try {
              auto reply = handle(*this, session, std::move(request));
              reflection::write_blob(out, reply.tag());
              {
                shyft::core::core_oarchive out_archive(out, shyft::core::core_arch_flags);
                out_archive << reply;
              }
            } catch (const std::exception &error) {
              session.log(dlib::LERROR, error.what());
              reflection::write_blob(out, error_tag);
              write_error_message(out, {error.what()});
            }
          },
          [&] {
            session.log(dlib::LWARN, fmt::format("unexpected message tag: got {}\n", etoi(tag)));
          });
      }
    } catch (const std::exception &error) {
      session.log(dlib::LERROR, fmt::format("unhandled exception: {}\n", error.what()));
    }
  }

}
