#pragma once

#include <iostream>
#include <vector>
#include <stdexcept>
#include <string_view>

#include <boost/preprocessor/list/for_each.hpp>
#include <boost/preprocessor/tuple/to_list.hpp>
#include <fmt/core.h>

#include <shyft/core/core_archive.h>
#include <shyft/core/core_serialization.h>
#include <shyft/core/dlib_utils.h>
#include <shyft/core/subscription.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/dtss/dtss.h>
#include <shyft/energy_market/stm/srv/compute/protocol.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/urls.h>

namespace shyft::energy_market::stm::srv::compute {

  struct client {
    shyft::core::srv_connection connection;

    template <message_tag request_tag>
    auto send(request<request_tag> request) {

      reply<request_tag> reply{};

      if (!connection.is_open)
        connection.open();

      auto &stream = *connection.io;

      write_blob(stream, request_tag);
      {
        shyft::core::core_oarchive out(stream, shyft::core::core_arch_flags);
        out << request;
      }

      message_tag reply_tag{};
      if (!read_blob(stream, reply_tag))
        throw dlib::socket_error("failed reading error tag");

      if (reply_tag == request_tag) {
        shyft::core::core_iarchive in(stream, shyft::core::core_arch_flags);
        in >> reply;
      } else if (reply_tag == error_tag) {
        if (auto maybe_error_message = try_read_error_message(stream))
          throw std::runtime_error(*maybe_error_message);
        throw dlib::socket_error("failed reading error message");
      } else
        throw std::runtime_error(fmt::format("unexpected message tag: expected {}, got {}\n", request_tag, reply_tag));
      return reply;
    }

#define SHYFT_LAMBDA(r, data, elem) \
  elem##_reply elem(elem##_request request) { \
    return send(std::move(request)); \
  }
    BOOST_PP_LIST_FOR_EACH(SHYFT_LAMBDA, _, BOOST_PP_TUPLE_TO_LIST(SHYFT_STM_COMPUTE_PROTOCOL))
#undef SHYFT_LAMBDA
  };

  // FIXME: drop redundant model-ids - jeh
  set_attrs_request set_plan_attrs_request(std::string_view, const stm_system &);
  get_attrs_request get_plan_attrs_request(std::string_view, const stm_system &);
  shyft::core::utctime send_input_and_plan(
    dtss::server &,
    std::string_view,
    stm_system &,
    client &,
    const generic_dt &,
    const std::vector<shop::shop_command> &);
  void get_plan_result(
    std::string_view,
    shyft::core::subscription::manager *,
    shyft::core::subscription::manager &,
    stm_system &,
    client &);

}
