#pragma once
#include <string>
#include <dlib/logger.h>

namespace shyft::energy_market::stm::srv::dstm {
  using std::string;
  using std::ofstream;

  /**
   * @brief Configuration class for Dlib logger:
   */
  class server_log_hook {
    ofstream fout; ///< if configured, as in open, make logs flow into this
   public:
    string log_file;                            ///< name of file to log to
    dlib::log_level current_level{dlib::LWARN}; ///< only log to file for these levels
    server_log_hook();
    server_log_hook(const string& log_file);
    server_log_hook(server_log_hook const &) = delete;
    server_log_hook(server_log_hook&&) = delete;
    server_log_hook& operator=(server_log_hook const &) = delete;
    server_log_hook& operator=(server_log_hook&&) = delete;

    void log(
      const string& logger_name,
      const dlib::log_level& ll,
      const dlib::uint64 thread_id,
      const char* message_to_log);
    static void configure(string const & fname, const dlib::log_level ll);
  };

  extern void configure_logger(server_log_hook& hook, const dlib::log_level& ll);
}
