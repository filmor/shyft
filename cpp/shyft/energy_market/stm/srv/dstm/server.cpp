#include <shyft/energy_market/stm/srv/dstm/server.h>

#include <algorithm>
#include <map>
#include <memory>
#include <ranges>
#include <stdexcept>
#include <string>
#include <string_view>
#include <vector>

#include <boost/serialization/map.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/vector.hpp>
#include <fmt/core.h>

#include <shyft/core/boost_serialization_std_opt.h>
#include <shyft/core/boost_serialization_std_variant.h>
#include <shyft/core/core_archive.h>
#include <shyft/energy_market/stm/context.h>
#include <shyft/energy_market/stm/model.h>
#include <shyft/energy_market/stm/srv/compute/manager.h>
#include <shyft/energy_market/stm/srv/dstm/client.h>
#include <shyft/energy_market/stm/srv/dstm/ts_magic_merge.h>
#include <shyft/energy_market/stm/srv/dstm/ts_url_generator.h>
#include <shyft/energy_market/stm/srv/dstm/ts_url_resolver.h>
#include <shyft/energy_market/stm/urls.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/dd/compute_ts_vector.h>
#include <shyft/time_series/dd/resolve_ts.h>
#include <shyft/version.h>
#include <shyft/web_api/energy_market/grammar/ts_url.h>

namespace shyft::energy_market::stm::srv::dstm {

  using shyft::core::core_iarchive;
  using shyft::core::core_oarchive;
  using shyft::core::core_arch_flags;

  dlib::logger server::slog{"dstm"};

  server::~server() {
    compute_janitor.stop();
    execution_context.join();
  }

  server::server()
    : execution_context{}
    , models{make_shared_models(execution_context.executor())}
    , compute_manager{}
    , compute_janitor{compute::make_janitor(execution_context.executor(), std::chrono::milliseconds(1000))} {
    setup_dtss();
  }

  /** start the server in background, return the listening port used in case it was set unspecified */
  int server::start_server() {
    if (get_listening_port() == 0) {

      start_async();
      while (is_running() && get_listening_port() == 0) // because dlib do not guarantee that listening port is set
        std::this_thread::sleep_for(std::chrono::milliseconds(10)); // upon return, so we have to wait until it's done
    } else {
      start_async();
    }
    auto port_num = get_listening_port();
    // for debug/trace in c++ : slog.set_level(dlib::LINFO);

    compute_janitor.start(compute_manager);
    slog << dlib::LINFO << "Started server on port " << port_num;
    return port_num;
  }

  /** Set up the dtss */
  void server::setup_dtss() {
    // in slave mode, all non dstm reads are passed to the master
    dtss = std::make_unique<ts_server>([this](const id_vector_t& ts_ids, utcperiod p) -> ts_vector_t {
      return this->dtss_read_callback(ts_ids, p);
    });
  }

  /** add container to dtss */
  void server::add_container(const std::string& container_name, const std::string& root_dir) {
    if (!dtss)
      throw std::runtime_error("Dtss hasn't been set. Call server::setup_dtss() before adding container.");
    dtss->add_container(container_name, root_dir);
  }

  /** @brief callback function for reading dstm:// time series */
  ts_vector_t server::dtss_read_callback(const id_vector_t& ts_ids, utcperiod /*p*/) {
    ts_vector_t r;
    r.reserve(ts_ids.size());
    shyft::web_api::grammar::dstm_path_grammar<const char*> pth_{ts_url_resolver(this)};
    for (auto const & ts_id : ts_ids) {
      apoint_ts ts;
      auto ok_parse = shyft::web_api::grammar::phrase_parser(ts_id.c_str(), pth_, ts);
      if (ok_parse) {
        auto rts = std::dynamic_pointer_cast<time_series::dd::aref_ts const>(ts.ts);
        if (rts && rts->rep) {      // if aref ts and its, bound..
          ts = apoint_ts(rts->rep); // (should we?)strip off the ref.name, only return ts.
        }
        r.push_back(ts);
      } else
        throw std::runtime_error(fmt::format("Unable to parse ts_id '{}'", ts_id));
    }
    return r;
  }

  namespace detail {

    void do_set_ts_impl(
      auto& dtss,
      std::string const & mid,
      auto& model,
      ats_vector const & tsv,
      bool /*merge_assign*/,
      bool rebind) {
      apoint_ts target; // not used actually, but needed for the phrase_parser arg.
      for (auto const & tsx : tsv) {
        auto rts = std::dynamic_pointer_cast<time_series::dd::aref_ts const>(tsx.ts);
        if (!rts)
          throw std::runtime_error(
            fmt::format("Ts with no  ref. url can not be assigned, attempt on model id '{}'", mid));

        bool ok_parse{false};
        using path_grammar = shyft::web_api::grammar::dstm_path_grammar<const char*>;
        using shyft::web_api::grammar::phrase_parser;
        if (!rts->rep) { // a simple symbolic assign should be allowed, it nullifies whatever is there
          scoped_ts_url_resolver_assign resolve_simply_assign{&model, mid, tsx};
          ok_parse = phrase_parser(rts->id.c_str(), path_grammar{resolve_simply_assign}, target);
        } else {
          scoped_ts_url_resolver_assign_fx resolve_magic_assign{
            [&](apoint_ts& lhs) { // 3 cases.. plain(or bound), unound(dtss)ref, or illegal assign to expr.
              ts_magic_merge_values(dtss, lhs, apoint_ts{rts->rep}, false, true, true);
            },
            &model,
            mid};
          ok_parse = phrase_parser(rts->id.c_str(), path_grammar{resolve_magic_assign}, target);
        }
        if (!ok_parse)
          throw std::runtime_error(fmt::format("Unable to parse ts_id '{}'", rts->id));

        if (dtss)
          dtss->sm->notify_change(rts->id); // we notify on each item, because of possible exception
      }
      if (rebind)
        rebind_expression(
          model, mid, false); // rebind/update expressions on the model, but do not fill nan_ts  for missing ts
    }

  }

  void server::do_set_ts(std::string const & mid, ats_vector const & tsv, bool merge_assign, bool rebind) {
    models
      .mutate_or_throw(
        mid,
        [&](auto&& view) {
          detail::do_set_ts_impl(dtss, mid, *view.model, tsv, merge_assign, rebind);
        })
      .get();
  }

  ats_vector server::do_get_ts(const std::string& mid, const vector<string>& ts_ids) {
    return models
      .observe_or_throw(
        mid,
        [&](auto&& view) {
          auto& model = view.model;
          utcperiod p; // not yet used.
          ts_vector_t r;
          r.reserve(ts_ids.size());
          shyft::web_api::grammar::dstm_path_grammar<const char*> pth_{
            scoped_ts_url_resolver{model.get(), mid}
          };
          for (auto const & ts_id : ts_ids) {
            apoint_ts ts;
            auto ok_parse = shyft::web_api::grammar::phrase_parser(ts_id.c_str(), pth_, ts);
            if (ok_parse)
              r.push_back(ts);
            else
              throw std::runtime_error(fmt::format("Unable to parse ts_id '{}'", ts_id));
          }
          return r;
        })
      .get();
  }

  void server::set_master(
    std::string ip,
    int port,
    double master_poll_time,
    size_t unsubscribe_min_threshold,
    double unsubscribe_max_delay) {
    if (!dtss)
      throw std::runtime_error("Dtss hasn't been set. Call server::setup_dtss() before adding container.");
    dtss->set_master(ip, port, master_poll_time, unsubscribe_min_threshold, unsubscribe_max_delay);
  }

  /** @brief get current api version */
  std::string server::do_get_version_info() {
    return fmt::format("{}.{}.{}", shyft::_version.major, shyft::_version.minor, shyft::_version.patch);
  }

  optimization_summary_ server::do_get_optimization_summary(const std::string& mid) {
    return models
      .observe_or_throw(
        mid,
        [](auto&& view) {
          return view.model->summary;
        })
      .get();
  }

  /** @brief create a new model with id */
  bool server::do_create_model(std::string const & mid) {
    return models.add(mid).get();
  }

  /** @brief add existing model with id */
  bool server::do_add_model(std::string const & mid, stm_system_ mdl) {
    return models.add(mid, mdl).get();
  }

  /** @brief remove (free up mem etc) model by id */
  bool server::do_remove_model(std::string const & mid) {
    return models.remove(mid).get();
  }

  /** @brief rename a model by id */
  bool server::do_rename_model(std::string old_mid, string new_mid) {
    if (!do_clone_model(old_mid, new_mid))
      return false;
    return do_remove_model(old_mid);
  }

  /** @brief clone existing model with id */
  bool server::do_clone_model(std::string const & old_mid, std::string new_mid) {
    return models.copy(old_mid, new_mid).get();
  }

  /** @brief get models, returns a string list with model identifiers */
  std::vector<std::string> server::do_get_model_ids() {
    return models
      .observe([&](const auto& models) {
        std::vector<std::string> r(models.size());
        std::ranges::copy(std::views::keys(models), r.data());
        return r;
      })
      .get();
  }

  std::map<std::string, model_info, std::less<>> server::do_get_model_infos() {
    auto infos = models
                   .observe([&](const auto& models) {
                     std::map<std::string, model_info, std::less<>> infos;
                     for (const auto& [key, shared_model] : models) {
                       auto model = shared_model->model;
                       infos[key] = model_info(model->id, model->name, no_utctime, model->json);
                     }
                     return infos;
                   })
                   .get();
    slog << dlib::LTRACE << "Returning info for all models";
    return infos;
  }

  model_state server::do_get_state(std::string const & mid) {
    return models
      .observe_or_throw(
        mid,
        [](auto&& view) {
          return view.info->state;
        })
      .get();
  }

  using client_ = shared_ptr<shyft::energy_market::stm::srv::dstm::client>;

  bool server::is_optimization_running(const std::string& mid) {
    try {
      return do_get_state(mid) == model_state::running;
    } catch (const std::exception& e) {
      slog << dlib::LWARN << e.what();
    }
    return false;
  }

  bool server::do_add_compute_server(std::string host_port) {
    return compute_manager.manage(host_port);
  }

  namespace detail {

    auto start_plan_task(
      dtss::server& dtss,
      auto& shared_model,
      const std::string& model_id,
      compute::client& client,
      const generic_dt& time_axis,
      const std::vector<shop::shop_command>& commands) {
      return shared_model
        .mutate([&](auto&& view) {
          auto limit = compute::send_input_and_plan(dtss, model_id, *view.model, client, time_axis, commands);
          view.info->state = model_state::running;
          return limit;
        })
        .get();
    }

    auto tick_plan_task(auto& shared_model, const std::vector<log_entry>& log) {
      return shared_model
        .mutate([&](auto&& view) {
          std::ranges::copy(log, std::back_inserter(view.info->log));
        })
        .get();
    }

    template <model_state next>
    auto done_plan_task(auto& server, auto& shared_model, const std::string& model_id, compute::client& client) {
      static_assert(next == model_state::tuning || next == model_state::finished);
      return shared_model
        .mutate([&](auto&& view) {
          auto& [model, info] = view;
          compute::get_plan_result(model_id, server.dtss ? server.dtss->sm.get() : nullptr, *server.sm, *model, client);
          info->state = next;
          return next == model_state::finished;
        })
        .get();
    }

    auto fail_plan_task(auto& shared_model, std::string_view what) {
      return shared_model
        .mutate([&](auto&& view) {
          server::slog << dlib::LERROR << "compute error: failed with " << what;
          view.info->state = model_state::failed;
        })
        .get();
    }

    auto dead_plan_task(auto& shared_model, std::string_view what) {
      return shared_model
        .mutate([&](auto&& view) {
          server::slog << dlib::LERROR << "compute error: died with " << what;
          view.info->state = model_state::failed;
        })
        .get();
    }

  }

  /** @brief start SHOP optimization on a model
   *  returns whether the shop optimization was started or not.
   */
  bool server::do_optimize(
    const std::string& model_id,
    const generic_dt& time_axis,
    const std::vector<shop::shop_command>& commands,
    [[maybe_unused]] bool opt_only) {

    if (!dtss) {
      slog << dlib::LWARN << "optimize: Missing dtss";
      return false;
    }

    auto shared_model = models.find(model_id).get();
    if (!shared_model) {
      slog << dlib::LWARN << fmt::format("optimize: Invalid model '{}'", model_id);
      return false;
    }
    return shared_model
      ->mutate([&](auto&& view) {
        auto&& [model, info] = view;

        if (stm_system_needs_bind(*model)) {
          slog << dlib::LERROR << "optimize('" << model_id
               << "'): Cannot run optimization on model with unbound attributes.\n"
               << "\tConsider evaluating model before running SHOP optimization.";
          return false;
        }

        if (info->state != model_state::idle) {
          slog << dlib::LWARN << "optimize: Optimization is already running on '" << model_id << "'";
          return false;
        }

        // NOTE:
        //     find an unassigned compute node...
        //     assign to model...
        //     poll for the result (for now)...
        //     - jeh
        auto compute_server = compute_manager.assign(model_id, model);
        if (!compute_server) {
          slog << dlib::LERROR << "optimize('" << model_id << "'): No resources available.";
          return false;
        }

        info->log.clear();
        compute_server->task([this, shared_model, model_id, time_axis, commands, compute_server](auto compute_task) {
          using enum compute::task_tag;
          if constexpr (compute_task.tag == start)
            return detail::start_plan_task(*dtss, *shared_model, model_id, compute_task.client, time_axis, commands);
          else if constexpr (compute_task.tag == tick)
            return detail::tick_plan_task(*shared_model, compute_task.log);
          else if constexpr (compute_task.tag == done)
            return detail::done_plan_task<model_state::finished>(*this, *shared_model, model_id, compute_task.client);
          else if constexpr (compute_task.tag == fail)
            return detail::fail_plan_task(*shared_model, compute_task.what);
          else
            return detail::dead_plan_task(*shared_model, compute_task.what);
        });
        return true;
      })
      .get();
  }

  bool server::do_start_tune(std::string_view model_id) {
    return models
      .mutate_or_throw(
        model_id,
        [&](auto&& view) {
          auto&& [model, info] = view;
          if (info->state == model_state::running || info->state == model_state::tuning)
            return false;
          auto compute_server = compute_manager.assign(model_id, model);
          if (!compute_server)
            return false;
          info->state = model_state::tuning;
          return true;
        })
      .get();
  }

  namespace detail {
    std::shared_ptr<compute::managed_server> expect_tuning_model(auto& server, std::string_view model_id, auto& info) {

      if (info.state != model_state::tuning) {
        server.slog << dlib::LWARN << fmt::format("tune: '{}' not in tuning mode", model_id);
        return nullptr;
      }

      auto compute_server = server.compute_manager.assigned(model_id);
      if (!compute_server) {
        server.slog << dlib::LWARN << fmt::format("tune: No compute server for '{}'", model_id);
        info.state = model_state::failed;
        return nullptr;
      }
      return compute_server;
    }
  }

  bool server::do_tune(
    std::string_view model_id,
    const generic_dt& time_axis,
    const std::vector<shop::shop_command>& commands) {

    if (!dtss) {
      slog << dlib::LWARN << "tune: Missing dtss";
      return false;
    }

    auto shared_model = models.find(model_id).get();
    if (!shared_model) {
      slog << dlib::LWARN << fmt::format("tune: Invalid model '{}'", model_id);
      return false;
    }

    return shared_model
      ->mutate([&](auto&& view) {
        auto&& [model, info] = view;

        auto compute_server = detail::expect_tuning_model(*this, model_id, *view.info);
        if (!compute_server)
          return false;

        info->log.clear();
        compute_server->task(
          [&, time_axis, commands, compute_server, shared_model, model_id = std::string(model_id)](auto compute_task) {
            using enum compute::task_tag;
            if constexpr (compute_task.tag == start)
              return detail::start_plan_task(*dtss, *shared_model, model_id, compute_task.client, time_axis, commands);
            else if constexpr (compute_task.tag == tick)
              return detail::tick_plan_task(*shared_model, compute_task.log);
            else if constexpr (compute_task.tag == done)
              return detail::done_plan_task<model_state::tuning>(*this, *shared_model, model_id, compute_task.client);
            else if constexpr (compute_task.tag == fail)
              return detail::fail_plan_task(*shared_model, compute_task.what);
            else
              return detail::dead_plan_task(*shared_model, compute_task.what);
          });
        return true;
      })
      .get();
  }

  bool server::do_stop_tune(std::string_view model_id) {
    return models
      .mutate_or_throw(
        model_id,
        [&](auto&& view) {
          auto compute_server = detail::expect_tuning_model(*this, model_id, *view.info);
          if (!compute_server)
            return false;

          view.info->state = model_state::finished;
          compute_server->unassign();
          return true;
        })
      .get();
  }

  std::vector<log_entry> server::do_get_log(const std::string& mid) {
    return models
      .observe_or_throw(
        mid,
        [](auto&& view) {
          return view.info->log;
        })
      .get();
  }

  /**
   * @brief Evaluate any unbound time series of a model
   */
  bool server::evaluate_stm_system(
    stm_system const & mdl,
    utcperiod bind_period,
    bool use_ts_cached_read,
    bool update_ts_cache,
    utcperiod /* not used clip_period */) {
    ts_vector_t tsv;
    stm_ts_operation find_unbound{[&tsv](apoint_ts& ats) -> bool {
      if (!ats.needs_bind())
        return false;
      tsv.push_back(ats);
      return true;
    }};
    find_unbound.apply(const_cast<stm_system&>(mdl));
    dtss->do_bind_ts(bind_period, tsv, use_ts_cached_read, update_ts_cache); // only recursive//
    return tsv.size() > 0;
  }

  /**
   * @brief Check whether any attributes in an stm_system needs bind_period
   */
  bool server::stm_system_needs_bind(const stm_system& mdl) {
    stm_ts_operation needs_bind{[](apoint_ts& ats) -> bool {
      return ats.needs_bind();
    }};
    return needs_bind.apply(const_cast<stm_system&>(mdl));
  }

  struct do_evaluate_ts_hash {
    using is_transparent = void;

    template <typename T>
    auto operator()(const T& t) const {
      return std::hash<T>{}(t);
    }
  };

  time_series::dd::ats_vector server::do_evaluate_ts(
    time_series::dd::ats_vector& tsv,
    utcperiod bind_period,
    bool use_ts_cached_read,
    bool update_ts_cache,
    utcperiod clip_period) {

    using time_series::dd::ipoint_ts_ref;
    using ts_ptr_t = ipoint_ts_ref*; ///< Keep refs to where to patch in rseolved result

    std::unordered_map<std::string, std::vector<ts_ptr_t>, do_evaluate_ts_hash, std::equal_to<>>
      dtss_map; ///< keep unique map of dtss refs.
    std::unordered_map<std::string, ipoint_ts_ref, do_evaluate_ts_hash, std::equal_to<>>
      dstm_resolved; ///< resolve only once

    auto resolver = [&](std::shared_ptr<const time_series::dd::ipoint_ts>& ref, std::string_view url)
      -> std::shared_ptr<const time_series::dd::ipoint_ts> {
      if (!ref->needs_bind()) // only resolve ref's that needs bind, keep old values
        return ref;
      auto model_id = stm::url_peek_model_id(url);
      if (model_id.empty()) { // not dstm://, then dtss, remember unique set of urls, and the  the patches
        auto it = dtss_map.find(url);
        if (it == dtss_map.end())
          dtss_map.emplace(std::string(url), std::vector{std::addressof(ref)});
        else
          it->second.push_back(std::addressof(ref));
        return ref;
      }

      if (auto already_resolved = dstm_resolved.find(url); already_resolved != dstm_resolved.end())
        return already_resolved->second;

      auto result =
        models
          .observe_or(
            model_id,
            [&](auto&& view) -> std::shared_ptr<const time_series::dd::ipoint_ts> {
              auto& model = view.model;
              shyft::web_api::grammar::dstm_path_grammar<const char*> url_grammar{
                dstm::scoped_ts_url_resolver{model.get(), std::string{model_id}}
              };

              time_series::dd::apoint_ts resolved_ts;
              std::string url_(url);
              auto ok_parse = shyft::web_api::grammar::phrase_parser(
                url_.c_str(), url_grammar, resolved_ts); // FIXME: string_view overload... -jeh
              if (!ok_parse || !resolved_ts.ts)
                return nullptr;
              return resolved_ts.ts->needs_bind()
                     ? resolved_ts.ts->clone_expr() // NOTE: consider just always cloning ... - jeh
                     : resolved_ts.ts;
            },
            [&]() -> std::shared_ptr<const time_series::dd::ipoint_ts> {
              return nullptr;
            })
          .get();
      return dstm_resolved[std::string(url)] = result;
    };

    for (time_series::dd::apoint_ts& ts : tsv)
      if (!time_series::dd::resolve_ts(resolver, ts))
        throw std::runtime_error("failed to resolve dstm timeseries");

    if (!dtss_map.empty()) {

      std::vector<std::string> dtss_urls(dtss_map.size());
      std::ranges::copy(std::views::keys(dtss_map), dtss_urls.data());

      auto resolved_dtss_refs = dtss->do_read(dtss_urls, bind_period, use_ts_cached_read, update_ts_cache);
      if (resolved_dtss_refs.size() != dtss_urls.size())
        throw std::runtime_error("failed to resolve dtss timeseries");

      // FIXME: in C++23 use zip - jeh
      auto dtss_it = dtss_map.begin();
      for (auto i : std::views::iota(std::size_t{0}, dtss_urls.size())) // now plain iteration/assign
        for (auto dtss_ref : (dtss_it++)->second)
          apoint_ts(*dtss_ref).bind(
            resolved_dtss_refs[i]); // .. to really optimize: we could do std::move on the last element
    }
    for (time_series::dd::apoint_ts& ts : tsv)
      ts.do_bind();

    time_series::dd::ats_vector tsv_results{time_series::dd::deflate_ts_vector<time_series::dd::apoint_ts>(tsv)};
    if (clip_period.valid())
      tsv_results = clip_to_period(tsv_results, clip_period);
    return tsv_results;
  }

  bool server::do_evaluate_model(
    const std::string& mid,
    utcperiod bind_period,
    bool use_ts_cached_read,
    bool update_ts_cache,
    utcperiod clip_period) {
    return models
      .mutate_or_throw(
        mid,
        [&](auto&& view) {
          auto& model = *view.model;
          auto evaluated = evaluate_stm_system(model, bind_period, use_ts_cached_read, update_ts_cache, clip_period);
          auto prefix = fmt::format("dstm://M{}", mid); /// update unit expressions, with notify.
          for (const auto& g : model.unit_groups) { // need to update sum expressions after getting results filled in
            g->update_sum_expressions();
            dtss->sm->notify_change(g->all_urls(prefix)); // notify change ug production etc.
          }
          return evaluated;
        })
      .get();
  }

  bool server::do_fx(std::string mid, std::string action) {
    return fx_cb ? fx_cb(mid, action) : false;
  }

  std::vector<bool> server::do_set_attrs(const std::vector<std::pair<std::string, any_attr>>& attrs) {

    web_api::grammar::stm_url_result url_result;
    auto&& [url_model, url_comp_path, url_attr] = url_result;

    shyft::web_api::grammar::stm_url_grammar<const char*> url_grammar{};
    std::vector<bool> result(attrs.size(), false);
    for (auto i : std::views::iota(0ul, attrs.size())) {

      url_model.clear();
      url_comp_path.clear();
      url_attr.clear();
      const auto& [url, attr] = attrs[i];
      if (!web_api::grammar::phrase_parser(url.c_str(), url_grammar, url_result))
        continue;

      result[i] =
        models
          .mutate_or(
            url_model,
            [&](auto&& view) {
              return stm::url_resolve(
                *view.model,
                url_comp_path,
                url_attr,
                [&]<typename T>(T&& attribute) {
                  return std::visit(
                    [&](auto&& target_attribute) {
                      if constexpr (requires { attribute = std::move(target_attribute); }) {
                        attribute = std::move(target_attribute);
                        return true;
                      }
                      return false;
                    },
                    attr);
                },
                [] {
                  return false;
                });
            },
            [] {
              return false;
            })
          .get();
    }
    return result;
  }

  std::vector<std::optional<any_attr>> server::do_get_attrs(const std::vector<std::string>& attr_urls) {

    web_api::grammar::stm_url_result url_result;
    auto&& [url_model, url_comp_path, url_attr] = url_result;

    shyft::web_api::grammar::stm_url_grammar<const char*> url_grammar{};
    std::vector<std::optional<any_attr>> attrs(attr_urls.size(), std::nullopt);
    for (auto i : std::views::iota(0ul, attr_urls.size())) {

      url_model.clear();
      url_comp_path.clear();
      url_attr.clear();

      const auto& url = attr_urls[i];

      if (!web_api::grammar::phrase_parser(url.c_str(), url_grammar, url_result))
        continue;

      attrs[i] =
        models
          .observe_or(
            url_model,
            [&](auto&& view) {
              return stm::url_resolve(
                *view.model,
                url_comp_path,
                url_attr,
                [&](auto&& attribute) -> std::optional<any_attr> {
                  if constexpr (requires { any_attr{attribute}; })
                    return any_attr{attribute};
                  return std::nullopt;
                },
                [&]() -> std::optional<any_attr> {
                  return std::nullopt;
                });
            },
            []() -> std::optional<any_attr> {
              return std::nullopt;
            })
          .get();
    }
    return attrs;
  }

  bool server::do_reset_model(const std::string& mid) {

    return models
      .mutate_or(
        mid,
        [&](auto&& view) {
          stm_ts_operation tsop{[](time_series::dd::apoint_ts& ats) {
            ats.do_unbind();
            return true;
          }};
          tsop.apply(*view.model);
          return true;
        },
        [&] {
          return false;
        })
      .get();
  }

  bool server::do_patch_model(std::string mid, stm_patch_op op, stm_system_ p) {
    if (p == nullptr)
      return false;
    return models
      .mutate_or_throw(
        mid,
        [&](auto&& view) {
          return patch(view.model, op, *p);
        })
      .get();
  }

  /**
   * @brief handle one client connection
   * @details
   * Reads messages/requests from the clients,
   * - act and perform request,
   * - return response
   * for as long as the client keep the connection
   * open.
   *
   */
  void server::on_connect(
    std::istream& in,
    std::ostream& out,
    const std::string& foreign_ip,
    const std::string& local_ip,
    unsigned short foreign_port,
    unsigned short local_port,
    dlib::uint64 /*connection_id*/
  ) {

    using shyft::core::core_iarchive;
    using shyft::core::core_oarchive;
    try {
      while (in.peek() != EOF) {
        auto msg_type = msg::read_type(in);
        try {
          switch (msg_type) {

          case message_type::VERSION_INFO: {
            auto result = do_get_version_info();              // get result
            msg::write_type(message_type::VERSION_INFO, out); // then send
            core_oarchive oa(out, core_arch_flags);
            oa << result;
          } break;

          case message_type::CREATE_MODEL: {
            core_iarchive ia(in, core_arch_flags); // create the stream
            std::string mid;
            ia >> mid;
            auto result = do_create_model(mid);               // get result
            msg::write_type(message_type::CREATE_MODEL, out); // then send
            core_oarchive oa(out, core_arch_flags);
            oa << result;
          } break;

          case message_type::ADD_MODEL: {
            core_iarchive ia(in, core_arch_flags); // create the stream
            std::string mid;
            stm_system_ mdl;
            ia >> mid >> mdl;
            auto result = do_add_model(mid, mdl);          // get result
            msg::write_type(message_type::ADD_MODEL, out); // then send
            core_oarchive oa(out, core_arch_flags);
            oa << result;
          } break;

          case message_type::REMOVE_MODEL: {
            core_iarchive ia(in, core_arch_flags); // create the stream
            std::string mid;
            ia >> mid;
            auto result = do_remove_model(mid);               // get result
            msg::write_type(message_type::REMOVE_MODEL, out); // then send
            core_oarchive oa(out, core_arch_flags);
            oa << result;
          } break;

          case message_type::RENAME_MODEL: {
            core_iarchive ia(in, core_arch_flags); // create the stream
            string old_mid, new_mid;
            ia >> old_mid >> new_mid;
            auto result = do_rename_model(old_mid, new_mid);  // get result
            msg::write_type(message_type::RENAME_MODEL, out); // then send
            core_oarchive oa(out, core_arch_flags);
            oa << result;
          } break;

          case message_type::CLONE_MODEL: {
            core_iarchive ia(in, core_arch_flags); // create the stream
            string old_mid, new_mid;
            ia >> old_mid >> new_mid;
            auto result = do_clone_model(old_mid, new_mid);  // get result
            msg::write_type(message_type::CLONE_MODEL, out); // then send
            core_oarchive oa(out, core_arch_flags);
            oa << result;
          } break;

          case message_type::GET_MODEL_IDS: {
            auto result = do_get_model_ids();                  // get result
            msg::write_type(message_type::GET_MODEL_IDS, out); // then send
            core_oarchive oa(out, core_arch_flags);
            oa << result;
          } break;

          case message_type::GET_MODEL_INFOS: {
            auto result = do_get_model_infos();                  // get result
            msg::write_type(message_type::GET_MODEL_INFOS, out); // then send
            core_oarchive oa(out, core_arch_flags);
            oa << result;
          } break;

          case message_type::GET_MODEL: {
            core_iarchive ia(in, core_arch_flags); // create the stream
            std::string mid;
            ia >> mid;

            models
              .observe_or_throw(
                mid,
                [&](auto&& view) {
                  msg::write_type(message_type::GET_MODEL, out);
                  core_oarchive oa(out, core_arch_flags);
                  oa << std::const_pointer_cast<stm_system>(view.model);
                })
              .get();

          } break;
          case message_type::OPTIMIZE: {
            core_iarchive ia(in, core_arch_flags); // create the stream
            std::string mid;
            generic_dt ta;
            std::vector<shop::shop_command> cmd;
            bool opt_only{false};
            ia >> mid >> ta >> cmd >> opt_only;
            auto result = do_optimize(mid, ta, cmd, opt_only);
            msg::write_type(message_type::OPTIMIZE, out); // then send
            core_oarchive oa(out, core_arch_flags);
            oa << result;
          } break;
          case message_type::GET_LOG: {
            core_iarchive ia(in, core_arch_flags);
            std::string mid;
            ia >> mid;
            auto result = do_get_log(mid);
            msg::write_type(message_type::GET_LOG, out);
            core_oarchive oa(out, core_arch_flags);
            oa << result;
          } break;
          case message_type::GET_STATE: {
            core_iarchive ia(in, core_arch_flags);
            std::string mid;
            ia >> mid;
            auto result = do_get_state(mid);
            msg::write_type(message_type::GET_STATE, out);
            core_oarchive oa(out, core_arch_flags);
            oa << result;
          } break;
          case message_type::SET_STATE: {
            core_iarchive ia(in, core_arch_flags);
            std::string mid;
            model_state x;
            ia >> mid >> x;
            // NOTE:  deprecated - jeh
            msg::write_type(message_type::SET_STATE, out);
          } break;
          case message_type::FX: {
            core_iarchive ia(in, core_arch_flags);
            std::string mid, fx_arg;
            ia >> mid >> fx_arg;
            auto result = do_fx(mid, fx_arg);
            msg::write_type(message_type::FX, out);
            core_oarchive oa(out, core_arch_flags);
            oa << result;
          } break;
          case message_type::EVALUATE_MODEL: {
            core_iarchive ia(in, core_arch_flags);
            std::string mid;
            bool use_cache, update_cache;
            utcperiod bind_period, clip_period;
            ia >> mid >> bind_period >> use_cache >> update_cache >> clip_period;
            auto result = do_evaluate_model(mid, bind_period, use_cache, update_cache, clip_period);
            msg::write_type(message_type::EVALUATE_MODEL, out);
            core_oarchive oa(out, core_arch_flags);
            oa << result;
          } break;
          case message_type::GET_TS: {
            core_iarchive ia(in, core_arch_flags);
            std::string mid;
            std::vector<string> ts_urls;
            ia >> mid >> ts_urls;
            auto result = do_get_ts(mid, ts_urls);
            msg::write_type(message_type::GET_TS, out);
            core_oarchive oa(out, core_arch_flags);
            oa << result;
          } break;
          case message_type::SET_TS: {
            core_iarchive ia(in, core_arch_flags);
            std::string mid;
            ats_vector tsv;
            ia >> mid >> tsv;
            do_set_ts(mid, tsv, false, true); // replace(no merge points), and do rebind
            msg::write_type(message_type::SET_TS, out);
            core_oarchive oa(out, core_arch_flags);
          } break;
          case message_type::ADD_COMPUTE_NODE: {
            core_iarchive ia(in, core_arch_flags);
            std::string host_port;
            ia >> host_port;
            auto ok = do_add_compute_server(host_port);
            msg::write_type(message_type::ADD_COMPUTE_NODE, out);
            core_oarchive oa(out, core_arch_flags);
            oa << ok;
          } break;
          case message_type::GET_OPTIMIZATION_SUMMARY: {
            core_iarchive ia(in, core_arch_flags);
            std::string mid;
            ia >> mid;
            auto result = do_get_optimization_summary(mid);
            msg::write_type(message_type::GET_OPTIMIZATION_SUMMARY, out);
            core_oarchive oa(out, core_arch_flags);
            oa << result;
          } break;
          case message_type::KILL_OPTIMIZATION: {
            core_iarchive ia(in, core_arch_flags);
            std::string mid;
            ia >> mid;
            // NOTE: deprecated - jeh
            const auto can_kill = false;
            {
              msg::write_type(message_type::KILL_OPTIMIZATION, out);
              core_oarchive oa(out, core_arch_flags);
              oa << can_kill;
            }
          } break;
          case message_type::EVALUATE_TS: {
            core_iarchive ia(in, core_arch_flags); // create the stream

            time_series::dd::ats_vector tsv;
            utcperiod bind_period;
            bool use_ts_cached_read, update_ts_cache;
            utcperiod clip_result;
            {
              time_series::dd::compressed_ts_expression compressed_tsv;
              ia >> compressed_tsv;
              tsv = time_series::dd::expression_decompressor::decompress(compressed_tsv);
            }
            ia >> bind_period >> use_ts_cached_read >> update_ts_cache >> clip_result;
            auto result = do_evaluate_ts(tsv, bind_period, use_ts_cached_read, update_ts_cache, clip_result);
            msg::write_type(message_type::EVALUATE_TS, out);
            core_oarchive oa(out, core_arch_flags);
            oa << result;
          } break;

          case message_type::SET_ATTRS: {
            core_iarchive ia(in, core_arch_flags); // create the stream
            std::vector<std::pair<std::string, any_attr>> attrs;
            ia >> attrs;
            auto result = do_set_attrs(attrs);
            msg::write_type(message_type::SET_ATTRS, out);
            core_oarchive oa(out, core_arch_flags);
            oa << result;
          } break;

          case message_type::GET_ATTRS: {
            core_iarchive ia(in, core_arch_flags); // create the stream
            std::vector<std::string> attr_urls;
            ia >> attr_urls;
            auto attrs = do_get_attrs(attr_urls);
            msg::write_type(message_type::GET_ATTRS, out);
            core_oarchive oa(out, core_arch_flags);
            oa << attrs;
          } break;

          case message_type::START_TUNE: {
            core_iarchive ia(in, core_arch_flags); // create the stream
            std::string mid;
            ia >> mid;
            auto result = do_start_tune(mid);
            msg::write_type(message_type::START_TUNE, out); // then send
            core_oarchive oa(out, core_arch_flags);
            oa << result;
          } break;

          case message_type::TUNE: {
            core_iarchive ia(in, core_arch_flags); // create the stream
            std::string mid;
            generic_dt ta;
            std::vector<shop::shop_command> cmd;
            ia >> mid >> ta >> cmd;
            auto result = do_tune(mid, ta, cmd);
            msg::write_type(message_type::TUNE, out); // then send
            core_oarchive oa(out, core_arch_flags);
            oa << result;
          } break;

          case message_type::STOP_TUNE: {
            core_iarchive ia(in, core_arch_flags); // create the stream
            std::string mid;
            ia >> mid;
            auto result = do_stop_tune(mid);
            msg::write_type(message_type::STOP_TUNE, out); // then send
            core_oarchive oa(out, core_arch_flags);
            oa << result;
          } break;

          case message_type::RESET_MODEL: {
            core_iarchive ia(in, core_arch_flags);
            std::string mid;
            ia >> mid;
            auto result = do_reset_model(mid);
            msg::write_type(message_type::RESET_MODEL, out);
            core_oarchive oa(out, core_arch_flags);
            oa << result;
          } break;

          case message_type::PATCH_MODEL: {
            core_iarchive ia(in, core_arch_flags); // create the stream
            std::string mid;
            stm_patch_op op;
            stm_system_ mdl;
            ia >> mid >> op >> mdl;
            auto result = do_patch_model(mid, op, mdl);
            msg::write_type(message_type::PATCH_MODEL, out); // then send
            core_oarchive oa(out, core_arch_flags);
            oa << result;
          } break;

          case message_type::COMPUTE_NODE_INFO: {
            auto result = compute_manager.status();
            msg::write_type(message_type::COMPUTE_NODE_INFO, out); // then send
            core_oarchive oa(out, core_arch_flags);
            oa << result;
          } break;

          default:
            throw std::runtime_error(fmt::format("Server got unknown message type: {}", (int) msg_type));
          }
        } catch (std::exception const & e) {
          msg::send_exception(e, out);
        }
      }
    } catch (...) {
      // exit the loop and close connection
      slog << dlib::LERROR << "dstm-service: failed and cleanup connection from '" << foreign_ip << "'@" << foreign_port
           << ", served at local '" << local_ip << "'@" << local_port << "\n";
    }
  }
}
