#include <shyft/energy_market/stm/srv/dstm/ts_url_generator.h>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/stm/contract_portfolio.h>
#include <shyft/energy_market/stm/power_module.h>
#include <shyft/energy_market/a_wrap.h>

namespace {


  using shyft::energy_market::stm::stm_system;
  using shyft::energy_market::stm::reservoir;
  using shyft::energy_market::stm::unit;
  using shyft::energy_market::stm::unit_group;
  using shyft::energy_market::stm::power_plant;
  using shyft::energy_market::stm::gate;
  using shyft::energy_market::stm::waterway;
  using shyft::energy_market::stm::energy_market_area;
  using shyft::energy_market::stm::contract;
  using shyft::energy_market::stm::contract_portfolio;
  using shyft::energy_market::stm::power_module;
  using shyft::energy_market::proxy_attr;

#define mk_ts_url(prefix, o, attr) proxy_attr(o, #attr, o.attr).url(prefix)

  using std::string;
  using std::vector;
  using std::dynamic_pointer_cast;

  /** @brief stm_result_ts_url_generator
   * @details
   * Given stm_system and a prefix(like dstm://Mm1),
   * generate a list of all ts-urls that
   * are results from algorithms like sintef/shop, powel/sim, water/flow
   *
   */
  struct stm_result_ts_url_generator {
    stm_system const & stm;
    string const & prefix;

    stm_result_ts_url_generator(stm_system const & s, string const & prefix)
      : stm{s}
      , prefix{prefix} {
    }

    void add(energy_market_area& mkt, vector<string>& r) const {
      r.push_back(mk_ts_url(prefix, mkt, buy));
      r.push_back(mk_ts_url(prefix, mkt, sale));
      r.push_back(mk_ts_url(prefix, mkt, reserve_obligation_penalty));
      r.push_back(mk_ts_url(prefix, mkt.demand.usage, result));
      r.push_back(mk_ts_url(prefix, mkt.demand.price, result));
      r.push_back(mk_ts_url(prefix, mkt.supply.usage, result));
      r.push_back(mk_ts_url(prefix, mkt.supply.price, result));
    }

    void add(contract& ctr, vector<string>& r) const {
      r.push_back(mk_ts_url(prefix, ctr, revenue));
    }

    void add(contract_portfolio& ctp, vector<string>& r) const {
      r.push_back(mk_ts_url(prefix, ctp, revenue));
    }

    void add(power_module& pm, vector<string>& r) const {
      r.push_back(mk_ts_url(prefix, pm.power, result));
    }

    void add(reservoir const & rsv, vector<string>& r) const {
      r.push_back(mk_ts_url(prefix, rsv.volume, result));
      r.push_back(mk_ts_url(prefix, rsv.level, result));
      r.push_back(mk_ts_url(prefix, rsv.volume, penalty));

      // from inflow calculation
      r.push_back(mk_ts_url(prefix, rsv.inflow, result));

      // water-values
      r.push_back(mk_ts_url(prefix, rsv.water_value.result, local_energy));
      r.push_back(mk_ts_url(prefix, rsv.water_value.result, local_volume));
      r.push_back(mk_ts_url(prefix, rsv.water_value.result, global_volume));
      r.push_back(mk_ts_url(prefix, rsv.water_value.result, end_value));
    }

    void add(unit_group const & a, vector<string>& r) const {
      r.push_back(mk_ts_url(prefix, a.obligation, penalty));
      r.push_back(mk_ts_url(prefix, a.obligation, result));
    }

    void add(power_plant const & pp, vector<string>& r) const {
      r.push_back(mk_ts_url(prefix, pp.production, result));
      r.push_back(mk_ts_url(prefix, pp.production, realised));
      r.push_back(mk_ts_url(prefix, pp.production, instant_max));
      r.push_back(mk_ts_url(prefix, pp.discharge, result));
      r.push_back(mk_ts_url(prefix, pp.discharge, realised));
    }

    void add(unit const & u, vector<string>& r) const {
      r.push_back(mk_ts_url(prefix, u.production, result));
      r.push_back(mk_ts_url(prefix, u.discharge, result));
      r.push_back(mk_ts_url(prefix, u.discharge, realised));
      r.push_back(mk_ts_url(prefix, u, effective_head));

      // from operational reserve (frequency control)
      r.push_back(mk_ts_url(prefix, u.reserve.fcr_n.up, result));
      r.push_back(mk_ts_url(prefix, u.reserve.fcr_n.down, result));
      r.push_back(mk_ts_url(prefix, u.reserve.fcr_d.up, result));
      r.push_back(mk_ts_url(prefix, u.reserve.fcr_d.down, result));
      r.push_back(mk_ts_url(prefix, u.reserve.afrr.up, result));
      r.push_back(mk_ts_url(prefix, u.reserve.afrr.down, result));
      r.push_back(mk_ts_url(prefix, u.reserve.rr.up, result));
      r.push_back(mk_ts_url(prefix, u.reserve.rr.down, result));
      r.push_back(mk_ts_url(prefix, u.reserve.frr.up, penalty));
      r.push_back(mk_ts_url(prefix, u.reserve.frr.down, penalty));
      r.push_back(mk_ts_url(prefix, u.reserve.fcr_n.up, penalty));
      r.push_back(mk_ts_url(prefix, u.reserve.fcr_n.down, penalty));
      r.push_back(mk_ts_url(prefix, u.reserve.fcr_d.up, penalty));
      r.push_back(mk_ts_url(prefix, u.reserve.fcr_d.down, penalty));
      r.push_back(mk_ts_url(prefix, u.reserve.droop, result));
    }

    void add(waterway const & wtr, vector<string>& r) const {
      r.push_back(mk_ts_url(prefix, wtr.discharge, result));
      // constraints on water way (shop discharge groups)
      r.push_back(mk_ts_url(prefix, wtr.discharge.penalty.result, constraint_min));
      r.push_back(mk_ts_url(prefix, wtr.discharge.penalty.result, constraint_max));
      r.push_back(mk_ts_url(prefix, wtr.discharge.penalty.result, ramping_down));
      r.push_back(mk_ts_url(prefix, wtr.discharge.penalty.result, ramping_up));
      r.push_back(mk_ts_url(prefix, wtr.discharge.penalty.result, accumulated_min));
      r.push_back(mk_ts_url(prefix, wtr.discharge.penalty.result, accumulated_max));
    }

    void add(gate const & gt, vector<string>& r) const {
      r.push_back(mk_ts_url(prefix, gt.discharge, result));
    }

    /** @brief generate all result urls for the specified stm sys and prefix */
    vector<string> operator()() const {
      vector<string> r;
      r.reserve(1000);
      for (const auto& m : stm.market) {
        if (const auto& mkt = dynamic_pointer_cast<energy_market_area>(m)) {
          add(*mkt, r);
        }
      }
      for (const auto& c : stm.contracts) {
        add(*c, r);
      }
      for (const auto& p : stm.contract_portfolios) {
        add(*p, r);
      }
      for (const auto& p : stm.power_modules) {
        add(*p, r);
      }
      for (const auto& ug : stm.unit_groups) {
        add(*ug, r);
      }
      for (const auto& hps : stm.hps) {
        for (const auto& rx : hps->reservoirs) {
          if (const auto& rsv = dynamic_pointer_cast<reservoir>(rx)) {
            add(*rsv, r);
          }
        }

        for (const auto& p : hps->power_plants) {
          if (const auto& pp = dynamic_pointer_cast<power_plant>(p)) {
            add(*pp, r);
          }
        }

        for (const auto& a : hps->units) {
          if (const auto& u = dynamic_pointer_cast<unit>(a)) {
            add(*u, r);
          }
        }
        for (const auto& w : hps->waterways) {
          if (const auto& wtr = dynamic_pointer_cast<waterway>(w)) {
            add(*wtr, r);
            for (auto const & g : wtr->gates) {
              if (const auto& gt = dynamic_pointer_cast<gate>(g)) {
                add(*gt, r);
              }
            }
          }
        }
      }
      return r;
    }
  };

#undef mk_ts_url

}

namespace shyft::energy_market::stm::srv::dstm {
  std::vector<std::string> ts_url_generator(std::string const & prefix, stm_system const & s) {
    return stm_result_ts_url_generator{s, prefix}();
  }
}
