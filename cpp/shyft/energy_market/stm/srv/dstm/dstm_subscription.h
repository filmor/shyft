#pragma once
#include <functional>
#include <shyft/core/subscription.h>
#include <shyft/dtss/dtss_subscription.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/stm/srv/dstm/server.h>
#include <shyft/energy_market/a_wrap.h>
#include <shyft/web_api/json_struct.h>
#include <shyft/web_api/bg_work_result.h>
#include <shyft/web_api/energy_market/request_handler.h>
#include <shyft/time_series/dd/ipoint_ts.h>

namespace shyft::energy_market::stm::subscription {
  using std::string;

  using shyft::time_series::dd::ats_vector;
  using shyft::time_series::dd::ts_as;
  using shyft::time_series::dd::gpoint_ts;
  using shyft::time_series::dd::aref_ts;

  using shyft::core::subscription::observer_base;
  using shyft::dtss::subscription::ts_expression_observer_;
  using shyft::dtss::subscription::ts_expression_observer;
  using shyft::core::subscription::manager_;
  using shyft::core::subscription::observable_;
  using shyft::web_api::energy_market::json;
  using shyft::web_api::bg_work_result;

  using shyft::energy_market::stm::srv::dstm::server;
  using shyft::energy_market::stm::stm_system_;
  using shyft::energy_market::stm::reservoir;
  using shyft::energy_market::stm::power_plant;
  using shyft::energy_market::stm::unit;
  using shyft::energy_market::stm::waterway;
  using shyft::energy_market::stm::catchment;
  using shyft::energy_market::proxy_attr;

  /**
   * @brief observer class for attributes in an stm_system/stm_hps
   * @details
   * The purpose of this class is to establish publish//subscribe observer,
   * so that we can support web-api with subscription.
   * It extends the classical dtss/generic observer with the specifics related to
   * dstm, with respect to attributes (not only time-series), as well
   * as possible nested expressions through dstm://M refrences.
   * The class life-time is usually controlled by the web-api socket(boost beast) lifetime,
   * and the ref to the server, is likewise assumed to outlive the lifetime of the subscription.
   */
  struct proxy_attr_observer : observer_base {
    static constexpr const char* prefix = "dstm://M"; // Initialize sub_id

    proxy_attr_observer(
      server* srv,
      string const & request_id,
      json const & data,
      std::function<bg_work_result(json const &)>&& cb)
      : observer_base(srv->sm, request_id)
      , srv{srv}
      , request_data{data}
      , response_cb{std::move(cb)} {
      mid = boost::get<string>(request_data.required("model_key"));
      // In the case that we use the dtss to keep subscriptions for time series:
      if (srv->dtss) {
        ts_sm = srv->dtss->sm;
      } else
        ts_sm = srv->sm;
    }

    virtual bool recalculate() override {
      auto updated = has_changed();
      published_version = terminal_version();
      return updated;
    }

    virtual int64_t terminal_version() const noexcept override {
      int64_t r = observer_base::terminal_version();
      for (auto& sub : ts_subs)
        r += sub->terminal_version();
      return r;
    }

    bg_work_result re_emit_response() const {
      return response_cb(request_data);
    }

    /**
     * @brief add nested dstm sub-expressions
     *
     * @details
     * When adding subscriptions, we need to take special care of dstm:// symbolic references, because they can hide
     * expressions. It can be a reference to just a terminal, like a result of simulation, or it can be a ref to lets
     * say pp.production.result= sum_of(unit dstm refs) In the latter case, we would like to add the terminals of the
     * nested expression, e.g. pp.production.result, so that we indeed do detect a need for re-evaluation if any of
     * those (nested expressions) are updated. The algo needed takes an expression, find terminals, those that are
     * dstm://M.. are then resolved, hunting the next terminals. etc. the nested terminals needs to be added to the
     * ts-vector as well, so that if changes, we get the notification. This function aims to do this, so it's recursive.
     * @tparam Fx a callable that resolves a list of dstm://M like urls to their corresponding model time-series,
     * apoint_ts
     * @param tsv the ts-vector to add more dependenent expressions into
     * @param ts the actual ts(expression), that might have dstm://M like terminals
     * @param resolve_url_to_tsv the callable, usually srv->do_get_ts(..), that does the heavy work of parse/lookup
     */
    template <typename Fx>
    static void add_resolve_nested_dstm_expressions(ats_vector& tsv, apoint_ts const & ts, Fx&& resolve_url_to_tsv) {
      auto biv = ts.find_ts_bind_info();
      std::vector<string> dstm_urls;
      for (auto const & bi : biv)
        if (bi.reference.rfind(prefix, 0) == 0) // find dstm://M
          dstm_urls.emplace_back(bi.reference);

      if (dstm_urls.empty())
        return;
      auto dstm_tsv = resolve_url_to_tsv(dstm_urls); // get the dstm://M like time-series here,

      constexpr auto is_expression = [](apoint_ts const & x) noexcept { // detect if a ts is an expression.
        return x.ts == nullptr || ts_as<gpoint_ts>(x.ts)
               ? false
               : true; // all except nullptr and gpoint_ts are expressions to be monitored
      };

      for (auto const & dstm_ts : dstm_tsv) {
        if (is_expression(dstm_ts)) {
          tsv.push_back(dstm_ts); // add this to the expressions we need to monitor, and
          add_resolve_nested_dstm_expressions(tsv, dstm_ts, resolve_url_to_tsv); // recursively add nested dstm:://M...
        }
      }
    }

    /**
     * @brief  add a ts subscription
     * @details Add a named ts subscription,
     * @param sub_id like dstm://something, or shyft://something to the subcribe set.
     * @param ts any ts rep, expression etc.
     * @return true if sub was added, false if already in the sub-set
     */
    bool add_ts_subscription(std::string const & sub_id, apoint_ts const & ts) {
      // Check that we are not already subscribing to it:
      auto it = std::find_if(ts_subs.begin(), ts_subs.end(), [&sub_id](auto el) {
        return el->request_id == sub_id;
      });
      if (it == ts_subs.end()) {
        dtss::ts_vector_t tsv;
        if (!ts.ts) {
          tsv.emplace_back(apoint_ts(sub_id));
        } else if (ts_as<gpoint_ts>(ts.ts)) {      // In the case gpoint_ts, all we have are values,
          tsv.emplace_back(apoint_ts(sub_id, ts)); // so we just append the sub_id to the subscription to the dtss.
        } else if (auto sts = ts_as<aref_ts>(ts.ts)) {
          if (
            sts->needs_bind()
            && !(sts->id.rfind(prefix, 0) == 0)) { // We are subscribing to an unbound series stored on the dtss
            tsv.emplace_back(
              ts); // In this case, the attribute is considered void of values, and id starts with shyft://...
          } else {
            tsv.emplace_back(
              apoint_ts(sub_id, ts)); // Here, the attribute time series has been bound, by e.g. evaluate_stm_system.
          }
        } else {
          tsv.emplace_back(
            ts); // Everything else, like expressions... and important, watch out for nested dstm://M expressions
          add_resolve_nested_dstm_expressions(tsv, ts, [this](std::vector<string> const & ts_urls) {
            return srv->do_get_ts(mid, ts_urls);
          });
        }
        auto new_sub = std::make_shared<ts_expression_observer>(ts_sm, sub_id, tsv, [](ats_vector) -> ats_vector {
          return ats_vector{};
        });
        ts_subs.emplace_back(new_sub);
        return true;
      } else {
        return false;
      }
    }

    /**
     * @brief add subscription to a .tsm[key] time-series
     * @tparam Struct type of object, like reservoir, unit
     * @param t cref to the object
     * @param key the ts-map key, as in the url, that is with the ts.prefix, e.g. "ts.revenue"
     * @return true if a subscription was added, false if already in the existing sub-set
     */
    template <class Struct>
    bool add_ts_map_subscription(Struct& t, string const & key) {
      apoint_ts xx;
      auto pa = proxy_attr(t, key, xx); // only looking for the url so ts does not matter
      auto sub_id = pa.url(prefix + mid);
      auto fts = t.tsm.find(key.substr(3)); // skip ts.
      if (fts == t.tsm.end())
        throw std::runtime_error("sw-error:add_ts_map_subscription failed to lookup its ts");
      return add_ts_subscription(sub_id, fts->second);
    }

    /** @brief Add an observable to terminals based on an attribute of a Struct.
     * Returns true if new subscription was added.
     *
     * @tparam Struct : The type of the owning object. E.g. reservoir, waterway &c.
     * @tparam LeafAccessor : The attribute to add subscription to. Here represented by the leaf accessor to it
     *  (nested member function pointers + attribute names) See shyft/mp.h
     */
    template <class Struct, class LeafAccessor>
    bool add_subscription(Struct const & t, LeafAccessor&& la) {
      using V = typename decltype(+mp::leaf_accessor_type(std::declval<LeafAccessor>()))::type;
      auto pa = proxy_attr(t, mp::leaf_accessor_id_str(la), mp::leaf_access(t, la));
      string sub_id = pa.url(prefix + mid);
      // Handling depending on value type of attribute (want dtss to handle time series)
      if constexpr (std::is_same_v<V, apoint_ts>) {
        return add_ts_subscription(sub_id, mp::leaf_access(t, la));
      } else { // The case that the value type is not time-series.
        auto subject = sm->add_subscription(sub_id)[0];
        // Check that it's not already part of terminals
        auto it = std::find_if(terminals.begin(), terminals.end(), [&subject](auto el) {
          return el->id == subject->id;
        });
        if (it == terminals.end()) {
          terminals.emplace_back(subject);
          return true;
        } else {
          return false;
        }
      }
    }

    // Member variables:
    server* srv;    /// not owning, have to outlive this instance, the server, because we need it for resolving urls
    manager_ ts_sm; ///  subscription manager for time series
    vector<ts_expression_observer_>
      ts_subs;         /// Subscriptions to time series, which should be handled by the dtss, if present
    json request_data; /// The data containing what attributes, what components, what model to subscribe to.
    string mid;        /// the model id we are working on (the unique, run-time/in-memory string known to client/server)
    std::function<bg_work_result(json const &)> response_cb; /// callback function to be called when value of observed
                                                             /// is updated and response has to be re emitted.
  };

  using proxy_attr_observer_ = std::shared_ptr<proxy_attr_observer>;
}
