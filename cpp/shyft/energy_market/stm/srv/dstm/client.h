#pragma once

#include <string>
#include <cstdint>
#include <exception>

#include <shyft/energy_market/stm/context.h>
#include <shyft/energy_market/stm/log_entry.h>
#include <shyft/energy_market/stm/optimization_summary.h>
#include <shyft/energy_market/stm/shop/shop_command.h>
#include <shyft/energy_market/stm/srv/compute/manager.h>
#include <shyft/energy_market/stm/srv/dstm/msg_defs.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/srv/fast_iosockstream.h>
#include <shyft/srv/model_info.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/time_axis.h>

namespace shyft::energy_market::stm::srv::dstm {

  using shyft::core::srv_connection;
  using shyft::srv::model_info;
  using shyft::time_series::dd::ats_vector;

  /**
   * @brief a client
   *
   *
   * This class take care of message exchange to the remote server,
   * using the supplied connection parameters.
   *
   * It implements the message protocol of the server, sending
   * message-prefix, arguments, waiting for response
   * deserialize the response and handle it back to the user.
   *
   * @see server
   *
   */
  struct client {
    srv_connection c;

    client(std::string host_port, int timeout_ms = 1000);

    std::string get_version_info();

    bool create_model(std::string const & mid);
    bool add_model(std::string const & mid, std::shared_ptr<stm_system> mdl);
    bool remove_model(std::string const & mid);
    bool rename_model(std::string const & old_mid, std::string const & new_mid);
    bool clone_model(std::string const & old_mid, std::string const & new_mid);
    std::vector<std::string> get_model_ids();
    std::map<std::string, model_info, std::less<>> get_model_infos();
    std::shared_ptr<stm_system> get_model(std::string const & mid);

    bool optimize(
      const std::string& mid,
      const generic_dt& ta,
      const std::vector<shop::shop_command>& cmd,
      bool opt_only = false);

    bool start_tune(const std::string& mid);
    bool tune(const std::string& mid, const generic_dt&, const std::vector<shop::shop_command>&);
    bool stop_tune(const std::string& mid);

    /** @brief get SHOP log for model.
     */
    std::vector<log_entry> get_log(const std::string& mid);

    std::shared_ptr<optimization_summary> get_optimization_summary(const std::string& mid);

    /** @brief get state of a model:
     */
    model_state get_state(const std::string& mid);

    /** @brief set state of a model
     *
     * @details it is not possible to set state running
     */
    void set_state(const std::string& mid, model_state x);

    /** @brief exeute fx(mid,fx_arg) on the server side
     */
    bool fx(const std::string& mid, const std::string& fx_arg);

    /** @brief evaluate some time-series expressions
     */
    time_series::dd::ats_vector evaluate_ts(
      const time_series::dd::ats_vector& tsv,
      utcperiod bind_period,
      bool use_ts_cached_read = true,
      bool update_ts_cache = true,
      utcperiod clip_result = utcperiod{});

    /** @brief evaluate any unbound time series attributes in a model.
     */
    bool evaluate_model(
      const std::string& mid,
      utcperiod bind_period,
      bool use_ts_cached_read,
      bool update_ts_cache,
      utcperiod clip_period = utcperiod{});

    /** @brief close, until needed again, the server connection
     *
     */
    void close();

    /** @brief send kill signal to server to kill an ongoing optimization of a model (if any)
     */
    bool kill_optimization(const std::string& mid);

    /**
     * @brief get_ts from model
     * @details
     * Given model id, and  ts-urls, resolve and return ts_vector
     * Used from python/c++ to read time-series attached to the model.
     * Similar to the web-api, using same underlying mechanisms.
     * @param mid the model id,the ts_urls should all refer to this model id
     * @param ts_urls, list of dstm://Mmid/path..
     * @return resolved list of time-series, 1 to 1 as for the request
     */
    ats_vector get_ts(const std::string& mid, const std::vector<std::string>& ts_urls);

    /**
     * @brief set_ts into the model
     * @details
     * Given model id, and  ts-urls, resolve and set the supplied time-series
     * accoring to the supplied ts-urls. They should all refer to mid.
     * Used from python/c++ to set time-series attached to the model.
     * Similar to the web-api, using same underlying mechanisms,
     * including notify change to subscribers
     * @param mid the model id,the ts_urls should all refer to this model id
     * @param ts_urls, list of dstm://Mmid/path..
     *
     */
    void set_ts(const std::string& mid, const ats_vector& tsv);

    /**
     * @brief add compute node by host_port
     * @details
     * add the specified host:port to the server so that optimizations can be run
     * on the specified node. If the host:port is already added this is a no-op.
     * Note that if the host port in question is marked for removal(pending remove),
     * this mark is cleared(so that it's not removed).
     *
     * @param host_port formatted as a validhost:port address
     */
    bool add_compute_server(const std::string& address);

    /**
     * @brief set_attrs on model
     * @param attrs
     * @return
     * A boolean vector indicating which attributes were successfully set.
     */
    std::vector<bool> set_attrs(const std::vector<std::pair<std::string, any_attr>>& attrs);
    /**
     * @brief get_attrs on model
     * @param attr_urls
     * @return
     * Returns the resolved attributes or a nullopt if resolution failed.
     */
    std::vector<std::optional<any_attr>> get_attrs(const std::vector<std::string>& attr_urls);

    /** @brief reset model
     */
    bool reset_model(const std::string& mid);

    /**
     * @brief patch model
     * @param mind the model id
     * @param op the patch operation, add, remove ,remove relation
     * @param p the patch to apply
     */
    bool patch(const std::string& mid, stm_patch_op op, stm_system_ const & p);

    std::vector<compute::managed_server_status> compute_server_status();
  };

}
