#pragma once
#include <string>
#include <vector>
#include <memory>
#include <shyft/mp.h>
#include <shyft/core/core_serialization.h>
#include <shyft/energy_market/id_base.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/stm_system.h>

namespace shyft::energy_market::stm {
    using std::string;
    using std::vector;
    using std::shared_ptr;

    struct unit;
    using unit_ = shared_ptr<unit>;

    struct unit_member {
        unit_member();
        unit_member(busbar* owner, unit_ const& u, apoint_ts const& active);
        bool operator==(const unit_member& other) const;
        bool operator!=(const unit_member& other) const { return !( *this == other); }
        void generate_url(std::back_insert_iterator<string>& rbi, int levels = -1, int template_levels = -1) const;
        int64_t id() const;

        busbar* owner{};
        url_fx_t url_fx;
        unit_ unit; 
        BOOST_HANA_DEFINE_STRUCT(unit_member,
            (apoint_ts, active) ///< [unit_less] if not set, always member, if set, active(t) determines the accumulated sum.
        );

        x_serialize_decl();
    };
    using unit_member_ = shared_ptr<unit_member>;

    struct power_module_member {
        power_module_member();
        power_module_member(busbar* owner, power_module_ const& u, apoint_ts const& active);
        bool operator==(const power_module_member& other) const;
        bool operator!=(const power_module_member& other) const { return !( *this == other); }
        void generate_url(std::back_insert_iterator<string>& rbi, int levels = -1, int template_levels = -1) const;
        int64_t id() const;

        busbar* owner{};
        url_fx_t url_fx;
        power_module_ power_module; 
        BOOST_HANA_DEFINE_STRUCT(power_module_member,
            (apoint_ts, active) ///< [unit_less] if not set, always member, if set, active(t) determines the accumulated sum.
        );

        x_serialize_decl();
    };
    using power_module_member_ = shared_ptr<power_module_member>;

    /** @brief Busbar
     *
     * Busbar, a hub connected by transmission lines
     */
    struct busbar : id_base {
        using super = id_base;

        busbar() { mk_url_fx(this); }
        busbar(int id, const string& name, const string& json, const network_& net)
            : super{id,name,json,{},{}},net{net} { mk_url_fx(this); }

        bool operator==(const busbar& other) const;
        bool operator!=(const busbar& other) const { return !(*this == other); }

        /** @brief generate an almost unique, url-like string for a reservoir.
         *
         * @param rbi: back inserter to store result
         * @param levels: How many levels of the url to include.
         * 		levels == 0 includes only this level. Use level < 0 to include all levels.
         * @param placeholders: The last element of the vector states wethers to use the reservoir ID
         * 		in the url or a placeholder. The remaining vector will be used in subsequent levels of the url.
         * 		If the vector is empty, the function defaults to not using placeholders.
         * @return
         */
        void generate_url(std::back_insert_iterator<string>& rbi, int levels = -1, int template_levels = -1) const;

        network_ net_() const { return net.lock(); }
        network__ net; ///< Reference up to the 'owning' network.
        busbar_ shared_from_this() const; // Get shared pointer from this, via network.

        vector<unit_member_> units;
        vector<power_module_member_> power_modules;

        void add_unit(unit_ u, apoint_ts const& active_ts);
        void remove_unit(unit_);
        void add_power_module(power_module_, apoint_ts const& active_ts);
        void remove_power_module(power_module_);

        struct ts_triplet {
            url_fx_t url_fx;
            BOOST_HANA_DEFINE_STRUCT(ts_triplet,
                (apoint_ts,realised), ///< SI unit, as in historical fact
                (apoint_ts,schedule), ///< the current schedule
                (apoint_ts,result)    ///< the optimal/simulated/estimated result
            );
        };

        BOOST_HANA_DEFINE_STRUCT(busbar,
            (ts_triplet, flow),      ///< flow in/out of busbar, W, J/s, flow in=negative, flow out=positive
            (ts_triplet, price)      ///< price
        );

        vector<transmission_line_> get_transmission_lines_from_busbar() const;
        vector<transmission_line_> get_transmission_lines_to_busbar() const;
        vector<energy_market_area_> get_market_areas() const;

        void add_to_start_of_transmission_line(transmission_line_) const;
        void add_to_end_of_transmission_line(transmission_line_) const;
        void add_to_market_area(energy_market_area_) const;

        x_serialize_decl();
    };
    using busbar_ = shared_ptr<busbar>;
    using busbar__ = weak_ptr<busbar>;
}

x_serialize_export_key(shyft::energy_market::stm::busbar);
x_serialize_export_key(shyft::energy_market::stm::unit_member);
x_serialize_export_key(shyft::energy_market::stm::power_module_member);
BOOST_CLASS_VERSION(shyft::energy_market::stm::busbar, 1);
//BOOST_CLASS_VERSION(shyft::energy_market::stm::unit_member, 1);
//BOOST_CLASS_VERSION(shyft::energy_market::stm::power_module_member, 1);
