#pragma once

#include <shyft/core/core_serialization.h>
#include <shyft/core/core_archive.h>


#include <shyft/energy_market/constraints.h>

#include <shyft/energy_market/hydro_power/power_plant.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/catchment.h>
#include <shyft/energy_market/hydro_power/waterway.h>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/run_parameters.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/stm/contract_portfolio.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/transmission_line.h>
#include <shyft/energy_market/stm/busbar.h>
#include <shyft/energy_market/stm/power_module.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/reservoir_aggregate.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/optimization_summary.h>
#include <shyft/energy_market/stm/log_entry.h>

#include <shyft/energy_market/stm/srv/task/stm_task.h>

// stuff needed to ensure vector<T>, map<K,V> etc. are automagically serializable
#include <boost/serialization/vector.hpp>
#include <boost/serialization/variant.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/unique_ptr.hpp>
#include <boost/serialization/weak_ptr.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/version.hpp>

#include <sstream>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <shyft/energy_market/constraints.h>
#include <shyft/mp.h>


namespace shyft::energy_market::stm {
    namespace hana = boost::hana;
    namespace mp = shyft::mp;

    template <class Archive,class T>
    void serialize_stm_attributes(T& o, Archive& ar) {
        hana::for_each(mp::leaf_accessors(hana::type_c<T>), [&](auto a){
            ar & shyft::core::core_nvp(mp::leaf_accessor_id_str(a), mp::leaf_access(o, a)); }
        );
    }

    /**
     * @brief serialize a hana struct T
     * @details
     * Serialize/Deserialize such that we can have automatic bw compat for adding attributes.
     * We do this by using  n { attrname attrval } layout.
     * Allowing us to detect/know number of attributes.
     * @tparam Archive a boost archive class
     * @tparam T a struct, with  boost hana struct members
     * @param o the object to be (de)serialized.
     * @param ar the archive to be used,
     *
     */
    template <class Archive,class T>
    void serialize_named_attributes(T& o, Archive& ar) {
        if constexpr(Archive::is_saving::value) {
            size_t sz=hana::value(hana::length(mp::leaf_accessors(hana::type_c<T>)));
            ar&shyft::core::core_nvp("S_size",sz);
            hana::for_each(mp::leaf_accessors(hana::type_c<T>),
                [&](auto &&a){
                    std::string aa=mp::leaf_accessor_id_str(a);
                    ar
                    &shyft::core::core_nvp("a",aa)
                    &shyft::core::core_nvp("v",mp::leaf_access(o,a));
                }
            );
        } else {
            size_t sz;
            ar & shyft::core::core_nvp("S_size",sz);
            for(size_t i=0;i<sz;++i) {
                std::string aa;
                ar & shyft::core::core_nvp("a",aa);
                bool found{false};
                hana::for_each(mp::leaf_accessors(hana::type_c<T>),
                    [&](auto &&a){
                        if(aa==mp::leaf_accessor_id_str(a) ) {
                            ar & shyft::core::core_nvp("v",mp::leaf_access(o,a));
                            found=true;
                        }
                    }
                );
                if(!found)
                    throw std::runtime_error("Unknown attribute not found in archive:"+aa);
            }
        }
    }



}
