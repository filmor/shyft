#pragma once
#include <string>
#include <vector>
#include <memory>
#include <shyft/mp.h>
#include <shyft/core/core_serialization.h>
#include <shyft/energy_market/id_base.h>
#include <shyft/energy_market/stm/stm_system.h>

namespace shyft::energy_market::stm {
    using std::string;
    using std::vector;
    using std::shared_ptr;

    /** @brief Power module
     *  Represents consumption/production. If positive then production, if negative then consumption.
     *  A power module is connected to a busbar, which represent a node in a transmission network
     */
    struct power_module : id_base {
        using super = id_base;

        power_module() { mk_url_fx(this); }
        power_module(int id, const string& name, const string& json, const stm_system_& sys)
            : super{id,name,json,{},{}},sys{sys} { mk_url_fx(this); }

        /** @brief generate an almost unique, url-like string for a reservoir.
         *
         * @param rbi: back inserter to store result
         * @param levels: How many levels of the url to include.
         * 		levels == 0 includes only this level. Use level < 0 to include all levels.
         * @param placeholders: The last element of the vector states wethers to use the reservoir ID
         * 		in the url or a placeholder. The remaining vector will be used in subsequent levels of the url.
         * 		If the vector is empty, the function defaults to not using placeholders.
         * @return
         */
        void generate_url(std::back_insert_iterator<string>& rbi, int levels = -1, int template_levels = -1) const;

        bool operator==(const power_module& other) const;
        bool operator!=(const power_module& other) const { return !(*this == other); }

        stm_system_ sys_() const { return sys.lock(); }
        stm_system__ sys; ///< Reference up to the 'owning' optimization system.

        struct power_ {
            url_fx_t url_fx;// needed by .url(...) to python exposure
            BOOST_HANA_DEFINE_STRUCT(power_,
                (apoint_ts, realised), ///< Historical fact. W, J/s, if positive then production, if negative then consumption
                (apoint_ts, schedule), ///< the current schedule. W, J/s, if positive then production, if negative then consumption
                (apoint_ts, result)    ///< the optimal/simulated/estimated result. W, J/s, if positive then production, if negative then consumption
            );
        };

        BOOST_HANA_DEFINE_STRUCT(power_module,
            (power_, power)
        );

        x_serialize_decl();
    };
    using power_module_=shared_ptr<power_module>;
}

x_serialize_export_key(shyft::energy_market::stm::power_module);
BOOST_CLASS_VERSION(shyft::energy_market::stm::power_module, 1);
