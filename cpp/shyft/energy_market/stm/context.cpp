#include <shyft/energy_market/stm/context.h>

#include <vector>
#include <memory>

#include <shyft/web_api/energy_market/tp_id.h>
#include <shyft/web_api/energy_market/grammar/ts_url.h>
//FIXME: break this dep - jeh
#include <shyft/energy_market/stm/srv/dstm/ts_url_resolver.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/aref_ts.h>

namespace shyft::energy_market::stm {

  bool rebind_expression(stm_system &mdl, const std::string &new_mkey, bool nan_fill_missing) {
    // important with the trailing / to avoid mixing up any future possible multimodelkeys
    auto mdl_prefix = "dstm://M" + new_mkey + "/";
    time_series::dd::apoint_ts missing_fill =
      nan_fill_missing
        ? time_series::dd::apoint_ts{mdl.run_params.run_time_axis, shyft::nan, time_series::ts_point_fx::POINT_AVERAGE_VALUE}
        : time_series::dd::apoint_ts{}; // restore null fills
    stm_ts_operation r{
      [&, nan_fill_missing](time_series::dd::apoint_ts &ats){
      auto atsv = ats.find_ts_bind_info();
      std::vector<time_series::dd::ts_bind_info> restore_info;
      std::vector<time_series::dd::apoint_ts> restore_ts;
      bool rebind_done = false;
      shyft::web_api::grammar::dstm_path_grammar<const char*> pth_{
        srv::dstm::scoped_ts_url_resolver{&mdl, new_mkey}
      };
      for (auto& tsi : atsv) {
        if (tsi.reference.rfind(mdl_prefix, 0) != 0) {
          auto rts = std::dynamic_pointer_cast<time_series::dd::aref_ts const>(tsi.ts.ts);
          if (rts) {
            restore_ts.push_back(time_series::dd::apoint_ts{rts->rep});
            restore_info.push_back(tsi);
          }
          continue;
        }
        time_series::dd::apoint_ts xts;
        auto ok_parse = shyft::web_api::grammar::phrase_parser(tsi.reference.c_str(), pth_, xts);
        if (ok_parse) { // gts is now ref to the ts attribute, we want to bind this to the tsi.ts
          restore_info.push_back(tsi);
          if (!xts.needs_bind()) { // only formal requirement, is that it is bound
            restore_ts.push_back(xts);
          } else {
            restore_ts.push_back(missing_fill);
          }
          rebind_done = true;
        }
      }
      if (rebind_done) {
        ats.do_unbind(); // do ats.unbind(ts) operation, that resets all expression trees from top down to this tsi.ts.
        // restore all series that where wiped out in the line above
        bool all_bound{true};
        for (auto i = 0u; i < restore_info.size(); ++i) {
          if (restore_ts[i].ts)
            restore_info[i].ts.bind(restore_ts[i]); // now put back those time-series other than dstm://M<thismodel>, we
                                                    // want those unmodified.
          else                                      // ref. missing_fill above
            all_bound = false;                      // at least one ts did not have required terminal bound
        }
        if (all_bound)   // only if all bound, otherwise the next will throw
          ats.do_bind(); // finally, restore computational structure
      }
      return rebind_done;
    }};
    return r.apply(mdl);
  }

}
