#pragma once
#include <algorithm>
#include <array>
#include <cstdint>
#include <functional>
#include <ranges>
#include <string_view>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

#include <boost/hana/ext/std/tuple.hpp>
#include <boost/hana/fold.hpp>
#include <boost/hana/type.hpp>
#include <boost/hana/count.hpp>
#include <boost/hana/unpack.hpp>
#include <boost/preprocessor/list/for_each.hpp>
#include <boost/preprocessor/tuple/to_list.hpp>
#include <fmt/compile.h>
#include <fmt/format.h>

#include <shyft/mp.h>
#include <shyft/core/reflection.h>
#include <shyft/energy_market/stm/model.h>
#include <shyft/energy_market/stm/functional.h>
#include <shyft/web_api/energy_market/tp_id.h>
#include <shyft/web_api/energy_market/grammar/ts_url.h>

namespace shyft::energy_market::stm {

  inline constexpr char url_prefix[] = "dstm://M";

  inline auto url_fmt_header(std::string_view model_id) {
    return fmt::format(FMT_COMPILE("{}{}"), url_prefix, model_id);
  }

  /** @brief
   *  utility function to extract the model-id of an stm-url
   */
  constexpr std::string_view url_peek_model_id(std::string_view url) {
    if (url.size() <= 8)
      return {};
    if (!url.starts_with(url_prefix))
      return {};
    url.remove_prefix(std::size(url_prefix) - 1);
    auto end = url.find('/'); //TODO: suboptimal - jeh
    if (end == url.npos)
      end = url.find('.');
    if (end != url.npos)
      url.remove_suffix(url.size() - end);
    return url;
  }

  using url_step = web_api::grammar::tp_id;

  constexpr char url_step_type_null = 0;

  template <auto member_ptr, char identifier, typename member_type = member_type_t<member_ptr>>
  constexpr auto url_child() {
    return std::tuple{constant_v<member_ptr>, constant_v<identifier>, identity_v<member_type>};
  }

  template <typename T>
  constexpr auto url_children(identity_t<T>) {
    return std::tuple{};
  }

  constexpr auto url_children(identity_t<hydro_power::waterway>) {
    using T = hydro_power::waterway;
    return std::make_tuple(url_child<&T::gates, 'G', stm::gate>());
  }

  constexpr auto url_children(identity_t<stm::waterway>) {
    return url_children(identity_t<hydro_power::waterway>{});
  }

  constexpr auto url_children(identity_t<hydro_power::hydro_power_system>) {
    using T = hydro_power::hydro_power_system;
    return std::make_tuple(
      url_child<&T::units, 'U', stm::unit>(),
      url_child<&T::waterways, 'W', stm::waterway>(),
      url_child<&T::reservoirs, 'R', stm::reservoir>(),
      url_child<&T::catchments, 'C', stm::catchment>(),
      url_child<&T::power_plants, 'P', stm::power_plant>());
  }

  constexpr auto url_children(identity_t<stm::busbar>) {
    using T = stm::busbar;
    return std::make_tuple(
      url_child<&T::units, 'M', stm::unit_member>(), url_child<&T::power_modules, 'P', stm::power_module_member>());
  }

  constexpr auto url_children(identity_t<stm::network>) {
    using T = stm::network;
    return std::make_tuple(
      url_child<&T::transmission_lines, 't', stm::transmission_line>(), url_child<&T::busbars, 'b', stm::busbar>());
  }

  constexpr auto url_children(identity_t<stm::unit_group>) {
    using T = stm::unit_group;
    return std::make_tuple(url_child<&T::members, 'M', stm::unit_group_member>());
  }

  constexpr auto url_children(identity_t<stm::stm_hps>) {
    using T = stm::stm_hps;
    return std::tuple_cat(
      url_children(identity_t<hydro_power::hydro_power_system>{}),
      std::make_tuple(url_child<&T::reservoir_aggregates, 'A', stm::reservoir_aggregate>()));
  }

  constexpr auto url_children(identity_t<stm::stm_system>) {
    using T = stm::stm_system;
    return std::make_tuple(
      url_child<&T::hps, 'H', stm::stm_hps>(),
      url_child<&T::market, 'm', stm::energy_market_area>(),
      url_child<&T::contracts, 'c', stm::contract>(),
      url_child<&T::contract_portfolios, 'p', stm::contract_portfolio>(),
      url_child<&T::networks, 'n', stm::network>(),
      url_child<&T::power_modules, 'P', stm::power_module>(),
      url_child<&T::unit_groups, 'u', stm::unit_group>());
  }

  template <typename Component>
  inline constexpr std::size_t url_comp_child_count_v =
    std::tuple_size_v<decltype(url_children(identity_t<std::remove_cvref_t<Component>>{}))>;

  template <auto member_ptr>
  constexpr char url_step_type() {
    constexpr auto children = url_children(identity_t<object_type_t<member_ptr>>{});
    char type = url_step_type_null;
    auto impl = [&](auto child) {
      if constexpr (requires { std::get<0>(child)() == member_ptr; })
        if (std::get<0>(child)() == member_ptr)
          type = std::get<1>(child)();
    };
    std::apply(
      [&](auto... child) {
        (impl(child), ...);
      },
      children);

    return type;
  }

  template <auto member_ptr>
  constexpr url_step url_make_step(std::int64_t id) {
    return {.tp = url_step_type<member_ptr>(), .id = id};
  }

  using url_root_t = stm_system;

  template <typename T>
  concept url_root = std::same_as<std::remove_cvref_t<T>, stm_system>;

  template <typename Component, typename Function>
  concept url_attr_invocable = std::invocable<Function>;

  template <typename Component, typename Function, typename Fallback>
  using url_with_attr_result_t = std::invoke_result_t<Fallback>;

  namespace detail {

    template <typename Result, typename Component, typename Function>
    using url_with_attr_dispatch_t = Result (*)(Component &&, Function &&);

    template <typename Result, typename Component, typename Function>
    using url_with_attr_entry_t = std::pair<std::string_view, url_with_attr_dispatch_t<Result, Component, Function>>;

    template < typename Result, typename Accessor, typename Component, typename Function>
    constexpr Result url_with_attr_dispatch(Component &&component, Function &&function) {
      return std::invoke(SHYFT_FWD(function), mp::leaf_access(SHYFT_FWD(component), Accessor{}));
    }

    constexpr std::string_view url_with_attr_string_view(const char *c) {
      std::size_t n = 0;
      while (c[n] != '\0')
        ++n;
      return {c, n};
    }

    template <typename Result, typename Component, typename Function>
    constexpr url_with_attr_entry_t<Result, Component, Function> url_with_attr_entry(auto attr_path) {
      using Dispatch = url_with_attr_dispatch_t<Result, Component, Function>;
      constexpr Dispatch dispatch =
        &url_with_attr_dispatch< Result, std::remove_cvref_t<decltype(attr_path)>, Component, Function>;
      using Entry = url_with_attr_entry_t<Result, Component, Function>;
      return Entry{url_with_attr_string_view(mp::leaf_accessor_id_str(attr_path)), dispatch};
    }

    namespace detail {

      template <typename Result, typename Component, typename Function>
      struct make_url_with_attr_table {
        constexpr auto operator()(auto... attr_path) const {
          using Entry = url_with_attr_entry_t<Result, Component, Function>;
          return std::array<Entry, sizeof...(attr_path)>{
            url_with_attr_entry<Result, Component, Function>(attr_path)...};
        }
      };

    }

    // NOTE: move into function as static constexpr in 23 - jeh
    template <typename Result, typename Component, typename Function>
    constexpr auto url_with_attr_table_impl() {
      constexpr auto attrs = mp::leaf_accessors(boost::hana::type_c<std::remove_cvref_t<Component>>);
      return boost::hana::unpack(attrs, detail::make_url_with_attr_table<Result, Component, Function>{});
    }

    template <typename Result, typename Component, typename Function>
    inline constexpr auto url_with_attr_table = url_with_attr_table_impl<Result, Component, Function>();
  }

  template <typename Component, typename Function, typename Fallback>
  constexpr auto url_with_attr(Component &&component, std::string_view attr, Function &&function, Fallback &&fallback) {
    using Result = url_with_attr_result_t<Component, Function, Fallback>;
    if (attr.starts_with("ts.")) {
      if constexpr (requires { component.tsm; }) { // works for any .tsm, could use tsm concept to be more precise
        attr.remove_prefix(3);
        if constexpr (!std::is_const_v<std::remove_reference_t<Component>>) { // if mutable, then allow new entries
          return std::invoke(
            SHYFT_FWD(function), component.tsm.emplace(attr, time_series::dd::apoint_ts{}).first->second);
        } else {
          if (auto f = component.tsm.find(attr); f != component.tsm.end()) { // if const, then only if exists entry
            return std::invoke(SHYFT_FWD(function), f->second);
          } else {
            return std::invoke(SHYFT_FWD(fallback));
          }
        }
      } else {
        return std::invoke(SHYFT_FWD(fallback));
      }
    } else {
      for (auto [entry_attr, entry_dispatch] : detail::url_with_attr_table<Result, Component, Function>)
        if (attr == entry_attr)
          return std::invoke(entry_dispatch, SHYFT_FWD(component), SHYFT_FWD(function));
      return std::invoke(SHYFT_FWD(fallback));
    }
  }

  template <typename Target, typename Component>
  constexpr auto url_check_attr(Component &&component, std::string_view attr) {
    return url_with_attr(
      SHYFT_FWD(component),
      attr,
      []<typename SubComp>(SubComp &&) {
        return std::is_same_v<Target, std::remove_reference_t<SubComp>>;
      },
      +[] {
        return false;
      });
  }

  // FIXME:
  //   constrain on invoke with each subcomponent
  //   - jeh
  template <typename Component, typename Function>
  concept url_subcomp_invocable = std::invocable<Function>;

  template <typename Component, typename Function, typename Fallback>
  using url_with_subcomp_result_t = std::invoke_result_t<Fallback>;

  namespace detail {

    template <typename Result, typename Component, typename Function, typename Fallback>
    using url_with_subcomp_dispatch_t = Result (*)(Component &&, std::int64_t, Function &&, Fallback &&);

    template <typename Result, typename Component, typename Function, typename Fallback>
    using url_with_subcomp_entry_t =
      std::pair<std::int64_t, url_with_subcomp_dispatch_t<Result, Component, Function, Fallback>>;

    template < typename SubComponent, typename AccessSubComponent, typename AccessComponent, typename Component>
    constexpr const_like_t<SubComponent, std::remove_reference_t<Component>> *url_with_subcomp_access(
      std::vector<std::shared_ptr<AccessSubComponent>>(AccessComponent::*member),
      Component &&component,
      std::int64_t subcomponent_id) {

      auto &&subcomponents = SHYFT_FWD(component).*member;

      auto it = std::ranges::find_if(subcomponents, [=](const auto &subcomponent) {
        return component_id(*subcomponent) == subcomponent_id;
      });
      if (it == std::ranges::end(subcomponents))
        return nullptr;
      return dynamic_cast<const_like_t<SubComponent, std::remove_reference_t<Component>> *>(it->get());
    }

    template < typename Result, typename SubComponent, typename Component, typename Function, typename Fallback>
    constexpr Result url_with_subcomp_dispatch(
      Component &&component,
      std::int64_t subcomponent_id,
      Function &&function,
      Fallback &&fallback) {
      using MemberPointer = std::tuple_element_t<0, SubComponent>;
      using SubComponentType = typename std::tuple_element_t<2, SubComponent>::type;
      if (
        auto subcomponent_ptr = url_with_subcomp_access<SubComponentType>(
          MemberPointer::value, SHYFT_FWD(component), subcomponent_id))
        return std::invoke(SHYFT_FWD(function), *subcomponent_ptr);
      return std::invoke(SHYFT_FWD(fallback));
    }

    template < typename Result, typename SubComponent, typename Component, typename Function, typename Fallback>
    constexpr auto url_with_subcomp_entry() {
      using Char = std::tuple_element_t<1, SubComponent>;

      using Dispatch = url_with_subcomp_dispatch_t<Result, Component, Function, Fallback>;
      constexpr Dispatch dispatch = &url_with_subcomp_dispatch<Result, SubComponent, Component, Function, Fallback>;
      return url_with_subcomp_entry_t<Result, Component, Function, Fallback>{std::int64_t{Char::value}, dispatch};
    }

    template <typename Result, typename Component, typename Function, typename Fallback>
    constexpr auto url_with_subcomp_table() {
      using Entry = url_with_subcomp_entry_t<Result, Component, Function, Fallback>;
      return std::apply(
        []<typename... SubComponent>(SubComponent...) {
          return std::array<Entry, sizeof...(SubComponent)>{
            url_with_subcomp_entry<Result, SubComponent, Component, Function, Fallback>()...};
        },
        url_children(identity_t<std::remove_cvref_t<Component>>{}));
    }
  }

  template <typename Component, typename Function, typename Fallback>
  constexpr auto url_with_subcomp(Component &&component, url_step step, Function &&function, Fallback &&fallback) {
    using Result = url_with_subcomp_result_t<Component, Function, Fallback>;
    // NOTE: make static in c++23 - jeh
    constexpr auto table = detail::url_with_subcomp_table<Result, Component, Function, Fallback>();
    for (auto [id, entry] : table)
      if (id == step.tp)
        return std::invoke(entry, SHYFT_FWD(component), step.id, SHYFT_FWD(function), SHYFT_FWD(fallback));
    return std::invoke(SHYFT_FWD(fallback));
  }

  template <typename Target, typename Component>
  constexpr auto url_check_subcomp(Component &&component, url_step step) {
    return url_with_subcomp(
      SHYFT_FWD(component),
      step,
      []<typename SubComp>(SubComp &&) {
        return std::is_same_v<Target, std::remove_reference_t<SubComp>>;
      },
      +[] {
        return false;
      });
  }

  namespace detail {
    constexpr auto url_max_path_length_impl(std::tuple<>) {
      return std::size_t{0};
    }

    template <typename... U>
    constexpr auto url_max_path_length_impl(std::tuple<U...>) {
      return std::size_t{1}
           + std::ranges::max({url_max_path_length_impl(url_children(std::tuple_element_t<2, U>{}))...});
    }
  }

  template <typename T>
  constexpr std::size_t url_max_path_length(identity_t<T>) {
    return detail::url_max_path_length_impl(url_children(identity_t<std::remove_cvref_t<T>>{}));
  }

  constexpr std::size_t url_max_path_length() {
    return boost::hana::fold(root_component_types, std::size_t{0}, [](auto c, auto t) {
      return std::max(c, url_max_path_length(t));
    });
  }

  inline constexpr auto url_max_id_string_length = std::numeric_limits<decltype(id_base::id)>::digits10 + 1;
  inline constexpr auto url_max_root_string_length = std::size(url_prefix) + url_max_id_string_length;

  template <typename T>
  constexpr std::size_t url_max_path_string_length(identity_t<T> t) {
    auto n = url_max_path_length(t);
    return n * (1 + url_max_id_string_length) + n;
  }

  constexpr std::size_t url_max_path_string_length() {
    return boost::hana::fold(root_component_types, std::size_t{0}, [](auto c, auto t) {
      return std::max(c, url_max_path_string_length(t));
    });
  }

  template <typename C>
  constexpr std::size_t url_max_attr_string_length(identity_t<C>) {
    return boost::hana::fold(
      mp::leaf_accessors(boost::hana::type_c<std::remove_cvref_t<C>>), std::size_t{0}, [](auto c, auto a) {
        return std::max(c, boost::hana::size(mp::leaf_accessor_id(a))());
      });
  }

  template <typename C>
  constexpr std::size_t url_max_string_length(identity_t<C> c) {
    return url_max_root_string_length + url_max_path_string_length(c) + url_max_attr_string_length(c);
  }

  constexpr std::size_t url_max_string_length() {
    return boost::hana::fold(root_component_types, std::size_t{0}, [](auto c, auto t) {
      return std::max(c, url_max_string_length(t));
    });
  }

  using url_string_buffer = fmt::basic_memory_buffer<char, url_max_string_length() + 1>;

  template <typename T>
  concept url_path = std::ranges::input_range<T> && std::same_as<std::ranges::range_value_t<T>, url_step>;

  namespace detail {
    template <
      std::size_t I,
      std::size_t N,
      typename Component,
      url_path Path = std::initializer_list<url_step>,
      typename Function,
      typename Fallback>
    constexpr auto url_with_path_impl(
      Component &&component,
      auto &&step_iterator,
      auto &&step_sentinel,
      Function &&function,
      Fallback &&fallback) {

      if (step_iterator == step_sentinel)
        return std::invoke(SHYFT_FWD(function), SHYFT_FWD(component));
      if constexpr (I >= N)
        return std::invoke(SHYFT_FWD(fallback));
      else
        return url_with_subcomp(
          SHYFT_FWD(component),
          *step_iterator,
          [&](auto &&subcomponent) {
            std::ranges::advance(step_iterator, 1);
            return url_with_path_impl<I + 1, N>(
              SHYFT_FWD(subcomponent), step_iterator, step_sentinel, SHYFT_FWD(function), SHYFT_FWD(fallback));
          },
          SHYFT_FWD(fallback));
    }

  }

  template <typename Component, url_path Path = std::initializer_list<url_step>, typename Function, typename Fallback>
  constexpr auto url_with_path(Component &&component, Path &&path, Function &&function, Fallback &&fallback) {
    return detail::url_with_path_impl<0, url_max_path_length(identity_t<std::remove_cvref_t<Component>>{})>(
      SHYFT_FWD(component), std::ranges::begin(path), std::ranges::end(path), SHYFT_FWD(function), SHYFT_FWD(fallback));
  }

  template <typename Target, typename Component, url_path Path = std::initializer_list<url_step>>
  constexpr auto url_check_path(Component &&component, Path &&path) {
    return url_with_path(
      SHYFT_FWD(component),
      SHYFT_FWD(path),
      []<typename SubComp>(SubComp &&) {
        return std::is_same_v<Target, std::remove_reference_t<SubComp>>;
      },
      +[] {
        return false;
      });
  }

  template <typename Component, url_path Path = std::initializer_list<url_step>, typename Function, typename Fallback>
  constexpr auto
    url_resolve(Component &&component, Path &&path, std::string_view attr, Function &&function, Fallback &&fallback) {
    return url_with_path(
      SHYFT_FWD(component),
      SHYFT_FWD(path),
      [&](auto &&subcomponent) {
        return url_with_attr(SHYFT_FWD(subcomponent), attr, SHYFT_FWD(function), SHYFT_FWD(fallback));
      },
      SHYFT_FWD(fallback));
  }

  template <typename Target, typename Component, url_path Path = std::initializer_list<url_step>>
  constexpr auto url_check(Component &&component, Path &&path, std::string_view attr) {
    return url_resolve(
      SHYFT_FWD(component),
      SHYFT_FWD(path),
      attr,
      []<typename Attr>(Attr &&) {
        return std::is_same_v<Target, std::remove_reference_t<Attr>>;
      },
      +[] {
        return false;
      });
  }

  constexpr auto url_format_root_to(auto out, std::string_view model) {
    return fmt::format_to(out, "{}{}", url_prefix, model);
  }

  constexpr auto url_format_step_to(auto out, const url_step &step) {
    const auto &[tp, id] = step;
    out = fmt::format_to(out, "/{}{}", (char) tp, id);
    return out;
  }

  template <url_path Path = std::initializer_list<url_step>>
  constexpr auto url_format_path_to(auto out, Path &&path) {
    for (const auto &step : path)
      out = url_format_step_to(out, step);
    return out;
  }

  constexpr auto url_format_attr_to(auto out, std::string_view attr) {
    return fmt::format_to(out, ".{}", attr);
  }

  template <url_path Path = std::initializer_list<url_step>>
  constexpr auto url_format_to(auto out, std::string_view model, Path &&path, std::string_view attr) {
    out = url_format_root_to(out, model);
    out = url_format_path_to(out, path);
    out = url_format_attr_to(out, attr);
    return out;
  }

  template <url_path Path = std::initializer_list<url_step>>
  constexpr auto url_format(std::string_view model_id, Path &&path, std::string_view attr) {
    std::string url;
    url.reserve(url_max_string_length());
    url_format_to(std::back_inserter(url), model_id, path, attr);
    return url;
  }

  template <auto... member_ptr>
  inline constexpr auto url_attr_path = std::tuple<std::integral_constant<decltype(member_ptr), member_ptr>...>{};

  // clang-format off

  template <typename T>
  constexpr auto url_planning_input_paths(identity_t<T>) {
    if constexpr (std::is_same_v<T, stm::unit_group>)
      return std::make_tuple(
        url_attr_path<&T::obligation, &T::obligation_::schedule>,
        url_attr_path<&T::obligation, &T::obligation_::cost>
      );
    else if constexpr (std::is_same_v<T, stm::energy_market_area>)
      return std::make_tuple(
        url_attr_path<&T::price>,
        url_attr_path<&T::max_sale>,
        url_attr_path<&T::max_buy>,
        url_attr_path<&T::load>,
        url_attr_path<&T::demand, &T::offering_::bids>,
        url_attr_path<&T::supply, &T::offering_::bids>
      );
    else if constexpr (std::is_same_v<T, stm::contract>)
      return std::make_tuple(
        url_attr_path<&T::options>,
        url_attr_path<&T::constraint, &T::constraint_::min_trade>,
        url_attr_path<&T::constraint, &T::constraint_::max_trade>,
        url_attr_path<&T::constraint, &T::constraint_::ramping_up>,
        url_attr_path<&T::constraint, &T::constraint_::ramping_down>,
        url_attr_path<&T::constraint, &T::constraint_::ramping_up_penalty_cost>,
        url_attr_path<&T::constraint, &T::constraint_::ramping_down_penalty_cost>
      );
    else if constexpr (std::is_same_v<T, stm::reservoir>) // FIXME: reads gates also...
      return std::make_tuple(
        url_attr_path<&T::inflow, &T::inflow_::realised>,
        url_attr_path<&T::inflow, &T::inflow_::schedule>,
        url_attr_path<&T::level, &T::level_::constraint, &T::level_::constraint_::max>,
        url_attr_path<&T::level, &T::level_::constraint, &T::level_::constraint_::min>,
        url_attr_path<&T::level, &T::level_::realised>,
        url_attr_path<&T::level, &T::level_::regulation_max>,
        url_attr_path<&T::level, &T::level_::regulation_min>,
        url_attr_path<&T::level, &T::level_::schedule>,
        url_attr_path<&T::ramping, &T::ramping_::level_down>,
        url_attr_path<&T::ramping, &T::ramping_::level_up>,
        url_attr_path<&T::volume, &T::volume_::constraint, &T::volume_::constraint_::max>,
        url_attr_path<&T::volume, &T::volume_::constraint, &T::volume_::constraint_::min>,
        url_attr_path<&T::volume, &T::volume_::constraint, &T::volume_::constraint_::tactical, &T::volume_::constraint_::tactical_::max, &penalty_constraint::cost>,
        url_attr_path<&T::volume, &T::volume_::constraint, &T::volume_::constraint_::tactical, &T::volume_::constraint_::tactical_::max, &penalty_constraint::flag>,
        url_attr_path<&T::volume, &T::volume_::constraint, &T::volume_::constraint_::tactical, &T::volume_::constraint_::tactical_::max, &penalty_constraint::limit>,
        url_attr_path<&T::volume, &T::volume_::constraint, &T::volume_::constraint_::tactical, &T::volume_::constraint_::tactical_::min, &penalty_constraint::cost>,
        url_attr_path<&T::volume, &T::volume_::constraint, &T::volume_::constraint_::tactical, &T::volume_::constraint_::tactical_::min, &penalty_constraint::flag>,
        url_attr_path<&T::volume, &T::volume_::constraint, &T::volume_::constraint_::tactical, &T::volume_::constraint_::tactical_::min, &penalty_constraint::limit>,
        url_attr_path<&T::volume, &T::volume_::cost, &T::volume_::cost_::flood, &T::volume_::cost_::cost_curve_::curve>,
        url_attr_path<&T::volume, &T::volume_::cost, &T::volume_::cost_::peak,  &T::volume_::cost_::cost_curve_::curve>,
        url_attr_path<&T::volume, &T::volume_::schedule>,
        url_attr_path<&T::volume, &T::volume_::slack, &T::volume_::slack_::lower>,
        url_attr_path<&T::volume, &T::volume_::slack, &T::volume_::slack_::upper>,
        url_attr_path<&T::volume, &T::volume_::static_max>,
        url_attr_path<&T::volume_level_mapping>,
        url_attr_path<&T::water_value, &T::water_value_::endpoint_desc>
      );
    else if constexpr (std::is_same_v<T, stm::unit>)
      return std::make_tuple(
        url_attr_path<&T::cost, &T::cost_::pump_start>,
        url_attr_path<&T::cost, &T::cost_::pump_stop>,
        url_attr_path<&T::cost, &T::cost_::start>,
        url_attr_path<&T::cost, &T::cost_::stop>,
        url_attr_path<&T::discharge, &T::discharge_::constraint, &T::discharge_::constraint_::max>,
        url_attr_path<&T::discharge, &T::discharge_::constraint, &T::discharge_::constraint_::max_from_downstream_level>,
        url_attr_path<&T::discharge, &T::discharge_::constraint, &T::discharge_::constraint_::min>,
        url_attr_path<&T::discharge, &T::discharge_::schedule>,
        url_attr_path<&T::generator_description>,
        url_attr_path<&T::priority>,
        url_attr_path<&T::production, &T::production_::commitment>,
        url_attr_path<&T::production, &T::production_::constraint, &T::production_::constraint_::max>,
        url_attr_path<&T::production, &T::production_::constraint, &T::production_::constraint_::min>,
        url_attr_path<&T::production, &T::production_::nominal>,
        url_attr_path<&T::production, &T::production_::pump_commitment>,
        url_attr_path<&T::production, &T::production_::realised>,
        url_attr_path<&T::production, &T::production_::schedule>,
        url_attr_path<&T::production, &T::production_::static_max>,
        url_attr_path<&T::production, &T::production_::static_min>,
        url_attr_path<&T::pump_unavailability>,
        url_attr_path<&T::pump_constraint, &T::pump_constraint_::min_downstream_level>,
        url_attr_path<&T::reserve, &T::reserve_::afrr, &T::reserve_::pair_::down, &T::reserve_::spec_::max>,
        url_attr_path<&T::reserve, &T::reserve_::afrr, &T::reserve_::pair_::down, &T::reserve_::spec_::min>,
        url_attr_path<&T::reserve, &T::reserve_::afrr, &T::reserve_::pair_::down, &T::reserve_::spec_::schedule>,
        url_attr_path<&T::reserve, &T::reserve_::afrr, &T::reserve_::pair_::up, &T::reserve_::spec_::cost>,
        url_attr_path<&T::reserve, &T::reserve_::afrr, &T::reserve_::pair_::up, &T::reserve_::spec_::max>,
        url_attr_path<&T::reserve, &T::reserve_::afrr, &T::reserve_::pair_::up, &T::reserve_::spec_::min>,
        url_attr_path<&T::reserve, &T::reserve_::afrr, &T::reserve_::pair_::up, &T::reserve_::spec_::schedule>,
        url_attr_path<&T::reserve, &T::reserve_::droop, &T::reserve_::spec_::cost>,
        url_attr_path<&T::reserve, &T::reserve_::droop, &T::reserve_::spec_::max>,
        url_attr_path<&T::reserve, &T::reserve_::droop, &T::reserve_::spec_::min>,
        url_attr_path<&T::reserve, &T::reserve_::droop, &T::reserve_::spec_::schedule>,
        url_attr_path<&T::reserve, &T::reserve_::droop_steps>,
        url_attr_path<&T::reserve, &T::reserve_::fcr_d, &T::reserve_::pair_::down, &T::reserve_::spec_::cost>,
        url_attr_path<&T::reserve, &T::reserve_::fcr_d, &T::reserve_::pair_::down, &T::reserve_::spec_::max>,
        url_attr_path<&T::reserve, &T::reserve_::fcr_d, &T::reserve_::pair_::down, &T::reserve_::spec_::min>,
        url_attr_path<&T::reserve, &T::reserve_::fcr_d, &T::reserve_::pair_::down, &T::reserve_::spec_::schedule>,
        url_attr_path<&T::reserve, &T::reserve_::fcr_d, &T::reserve_::pair_::up, &T::reserve_::spec_::cost>,
        url_attr_path<&T::reserve, &T::reserve_::fcr_d, &T::reserve_::pair_::up, &T::reserve_::spec_::max>,
        url_attr_path<&T::reserve, &T::reserve_::fcr_d, &T::reserve_::pair_::up, &T::reserve_::spec_::min>,
        url_attr_path<&T::reserve, &T::reserve_::fcr_d, &T::reserve_::pair_::up, &T::reserve_::spec_::schedule>,
        url_attr_path<&T::reserve, &T::reserve_::fcr_mip>,
        url_attr_path<&T::reserve, &T::reserve_::fcr_n, &T::reserve_::pair_::down, &T::reserve_::spec_::cost>,
        url_attr_path<&T::reserve, &T::reserve_::fcr_n, &T::reserve_::pair_::down, &T::reserve_::spec_::max>,
        url_attr_path<&T::reserve, &T::reserve_::fcr_n, &T::reserve_::pair_::down, &T::reserve_::spec_::min>,
        url_attr_path<&T::reserve, &T::reserve_::fcr_n, &T::reserve_::pair_::down, &T::reserve_::spec_::schedule>,
        url_attr_path<&T::reserve, &T::reserve_::fcr_n, &T::reserve_::pair_::up, &T::reserve_::spec_::cost>,
        url_attr_path<&T::reserve, &T::reserve_::fcr_n, &T::reserve_::pair_::up, &T::reserve_::spec_::max>,
        url_attr_path<&T::reserve, &T::reserve_::fcr_n, &T::reserve_::pair_::up, &T::reserve_::spec_::min>,
        url_attr_path<&T::reserve, &T::reserve_::fcr_n, &T::reserve_::pair_::up, &T::reserve_::spec_::schedule>,
        url_attr_path<&T::reserve, &T::reserve_::fcr_static_max>,
        url_attr_path<&T::reserve, &T::reserve_::fcr_static_min>,
        url_attr_path<&T::reserve, &T::reserve_::mfrr, &T::reserve_::pair_::down, &T::reserve_::spec_::cost>,
        url_attr_path<&T::reserve, &T::reserve_::mfrr, &T::reserve_::pair_::down, &T::reserve_::spec_::cost>,
        url_attr_path<&T::reserve, &T::reserve_::mfrr, &T::reserve_::pair_::down, &T::reserve_::spec_::max>,
        url_attr_path<&T::reserve, &T::reserve_::mfrr, &T::reserve_::pair_::down, &T::reserve_::spec_::min>,
        url_attr_path<&T::reserve, &T::reserve_::mfrr, &T::reserve_::pair_::down, &T::reserve_::spec_::schedule>,
        url_attr_path<&T::reserve, &T::reserve_::mfrr, &T::reserve_::pair_::up, &T::reserve_::spec_::cost>,
        url_attr_path<&T::reserve, &T::reserve_::mfrr, &T::reserve_::pair_::up, &T::reserve_::spec_::max>,
        url_attr_path<&T::reserve, &T::reserve_::mfrr, &T::reserve_::pair_::up, &T::reserve_::spec_::min>,
        url_attr_path<&T::reserve, &T::reserve_::mfrr, &T::reserve_::pair_::up, &T::reserve_::spec_::schedule>,
        url_attr_path<&T::reserve, &T::reserve_::mfrr_static_min>,
        url_attr_path<&T::unavailability>
      );
    else if constexpr (std::is_same_v<T, stm::power_plant>)
      return std::make_tuple(
        url_attr_path<&T::best_profit, &T::best_profit_::dynamic_water_value>,
        url_attr_path<&T::discharge, &T::discharge_::constraint_max>,
        url_attr_path<&T::discharge, &T::discharge_::constraint_min>,
        url_attr_path<&T::discharge, &T::discharge_::intake_loss_from_bypass_flag>,
        url_attr_path<&T::discharge, &T::discharge_::ramping_down>,
        url_attr_path<&T::discharge, &T::discharge_::ramping_up>,
        url_attr_path<&T::discharge, &T::discharge_::schedule>,
        url_attr_path<&T::mip>,
        url_attr_path<&T::outlet_level>,
        url_attr_path<&T::production, &T::production_::constraint_max>,
        url_attr_path<&T::production, &T::production_::constraint_min>,
        url_attr_path<&T::production, &T::production_::merge_tolerance>,
        url_attr_path<&T::production, &T::production_::ramping_down>,
        url_attr_path<&T::production, &T::production_::ramping_up>,
        url_attr_path<&T::production, &T::production_::schedule>,
        url_attr_path<&T::unavailability>
      );
    else if constexpr (std::is_same_v<T, stm::waterway>)
      return std::make_tuple(
        url_attr_path<&T::discharge, &T::discharge_::reference>,
        url_attr_path<&T::discharge, &T::discharge_::realised>,
        url_attr_path<&T::discharge, &T::discharge_::constraint, &T::discharge_::constraint_::accumulated_max>,
        url_attr_path<&T::discharge, &T::discharge_::constraint, &T::discharge_::constraint_::accumulated_min>,
        url_attr_path<&T::discharge, &T::discharge_::constraint, &T::discharge_::constraint_::min>,
        url_attr_path<&T::discharge, &T::discharge_::constraint, &T::discharge_::constraint_::max>,
        url_attr_path<&T::discharge, &T::discharge_::constraint, &T::discharge_::constraint_::ramping_up>,
        url_attr_path<&T::discharge, &T::discharge_::constraint, &T::discharge_::constraint_::ramping_down>,
        url_attr_path<&T::discharge, &T::discharge_::penalty, &T::discharge_::penalty_::cost, &T::discharge_::penalty_::cost_::constraint_min>,
        url_attr_path<&T::discharge, &T::discharge_::penalty, &T::discharge_::penalty_::cost, &T::discharge_::penalty_::cost_::constraint_max>,
        url_attr_path<&T::discharge, &T::discharge_::penalty, &T::discharge_::penalty_::cost, &T::discharge_::penalty_::cost_::ramping_up>,
        url_attr_path<&T::discharge, &T::discharge_::penalty, &T::discharge_::penalty_::cost, &T::discharge_::penalty_::cost_::ramping_down>,
        url_attr_path<&T::discharge, &T::discharge_::penalty, &T::discharge_::penalty_::cost, &T::discharge_::penalty_::cost_::accumulated_min>,
        url_attr_path<&T::discharge, &T::discharge_::penalty, &T::discharge_::penalty_::cost, &T::discharge_::penalty_::cost_::accumulated_max>,
        url_attr_path<&T::discharge, &T::discharge_::static_max>,
        url_attr_path<&T::geometry, &T::geometry_::diameter>,
        url_attr_path<&T::geometry, &T::geometry_::length>,
        url_attr_path<&T::geometry, &T::geometry_::z0>,
        url_attr_path<&T::geometry, &T::geometry_::z1>,
        url_attr_path<&T::head_loss_coeff>
      );
    else if constexpr (std::is_same_v<T, stm::gate>)
      return std::make_tuple(
        url_attr_path<&T::cost>,
        url_attr_path<&T::discharge, &T::discharge_::constraint, &T::discharge_::constraint_::max>,
        url_attr_path<&T::discharge, &T::discharge_::constraint, &T::discharge_::constraint_::min>,
        url_attr_path<&T::discharge, &T::discharge_::merge_tolerance>,
        url_attr_path<&T::discharge, &T::discharge_::realised>,
        url_attr_path<&T::discharge, &T::discharge_::schedule>,
        url_attr_path<&T::discharge, &T::discharge_::static_max>,
        url_attr_path<&T::flow_description>,
        url_attr_path<&T::flow_description_delta_h>,
        url_attr_path<&T::opening, &T::opening_::constraint, &T::opening_::constraint_::continuous>,
        url_attr_path<&T::opening, &T::opening_::constraint, &T::opening_::constraint_::positions>,
        url_attr_path<&T::opening, &T::opening_::realised>,
        url_attr_path<&T::opening, &T::opening_::schedule>
      );
    else
      return std::make_tuple();
  }

  template <typename T>
  constexpr auto url_planning_output_paths(identity_t<T>) {
    if constexpr (std::is_same_v<T, stm::energy_market_area>)
      return std::make_tuple(
        url_attr_path<&T::buy>,
        url_attr_path<&T::demand, &T::offering_::price, &T::ts_triplet_::result>,
        url_attr_path<&T::demand, &T::offering_::usage, &T::ts_triplet_::result>,
        url_attr_path<&T::reserve_obligation_penalty>,
        url_attr_path<&T::sale>,
        url_attr_path<&T::supply, &T::offering_::price, &T::ts_triplet_::result>,
        url_attr_path<&T::supply, &T::offering_::usage, &T::ts_triplet_::result>
      );
    else if constexpr (std::is_same_v<T, stm::contract>)
      return std::make_tuple(url_attr_path<&T::quantity>);
    else if constexpr (std::is_same_v<T, stm::reservoir>)
      return std::make_tuple(
        url_attr_path<&T::inflow, &T::inflow_::result>,
        url_attr_path<&T::level, &T::level_::result>,
        url_attr_path<&T::volume, &T::volume_::cost, &T::volume_::cost_::flood,&T::volume_::cost_::cost_curve_::penalty>,
        url_attr_path<&T::volume, &T::volume_::cost, &T::volume_::cost_::peak, &T::volume_::cost_::cost_curve_::penalty>,

        url_attr_path<&T::volume, &T::volume_::penalty>,
        url_attr_path<&T::volume, &T::volume_::result>,
        url_attr_path<&T::volume,&T::volume_::constraint,&T::volume_::constraint_::tactical,&T::volume_::constraint_::tactical_::max,&penalty_constraint::penalty>,
        url_attr_path<&T::volume,&T::volume_::constraint,&T::volume_::constraint_::tactical,&T::volume_::constraint_::tactical_::min,&penalty_constraint::penalty>,
        url_attr_path<&T::water_value, &T::water_value_::result, &T::water_value_::result_::end_value>,
        url_attr_path<&T::water_value, &T::water_value_::result, &T::water_value_::result_::global_volume>,
        url_attr_path<&T::water_value, &T::water_value_::result, &T::water_value_::result_::local_energy>,
        url_attr_path<&T::water_value, &T::water_value_::result, &T::water_value_::result_::local_volume>
      );
    else if constexpr (std::is_same_v<T, stm::power_plant>)
      return std::make_tuple(
        url_attr_path<&T::best_profit, &T::best_profit_::cost_average>,
        url_attr_path<&T::best_profit, &T::best_profit_::cost_commitment>,
        url_attr_path<&T::best_profit, &T::best_profit_::cost_marginal>,
        url_attr_path<&T::best_profit, &T::best_profit_::discharge>,
        url_attr_path<&T::discharge, &T::discharge_::result>,
        url_attr_path<&T::gross_head>,
        url_attr_path<&T::production, &T::production_::result>,
        url_attr_path<&T::production, &T::production_::instant_max>
      );
    else if constexpr (std::is_same_v<T, stm::unit>)
      return std::make_tuple(
        url_attr_path<&T::best_profit, &T::best_profit_::discharge>,
        url_attr_path<&T::best_profit, &T::best_profit_::discharge_production_ratio>,
        url_attr_path<&T::best_profit, &T::best_profit_::operating_zone>,
        url_attr_path<&T::best_profit, &T::best_profit_::production>,
        url_attr_path<&T::discharge, &T::discharge_::result>,
        url_attr_path<&T::effective_head>,
        url_attr_path<&T::production, &T::production_::result>,
        url_attr_path<&T::production_discharge_relation, &T::production_discharge_relation_::convex>,
        url_attr_path<&T::production_discharge_relation, &T::production_discharge_relation_::final>,
        url_attr_path<&T::production_discharge_relation, &T::production_discharge_relation_::original>,
        url_attr_path<&T::reserve, &T::reserve_::afrr, &T::reserve_::pair_::down, &T::reserve_::spec_::result>,
        url_attr_path<&T::reserve, &T::reserve_::afrr, &T::reserve_::pair_::up, &T::reserve_::spec_::result>,
        url_attr_path<&T::reserve, &T::reserve_::droop, &T::reserve_::spec_::result>,
        url_attr_path<&T::reserve, &T::reserve_::fcr_d, &T::reserve_::pair_::down, &T::reserve_::spec_::result>,
        url_attr_path<&T::reserve, &T::reserve_::fcr_d, &T::reserve_::pair_::up, &T::reserve_::spec_::result>,
        url_attr_path<&T::reserve, &T::reserve_::fcr_n, &T::reserve_::pair_::down, &T::reserve_::spec_::result>,
        url_attr_path<&T::reserve, &T::reserve_::fcr_n, &T::reserve_::pair_::up, &T::reserve_::spec_::result>,
        url_attr_path<&T::reserve, &T::reserve_::frr, &T::reserve_::pair_::down, &T::reserve_::spec_::result>,
        url_attr_path<&T::reserve, &T::reserve_::frr, &T::reserve_::pair_::up, &T::reserve_::spec_::result>,
        url_attr_path<&T::reserve, &T::reserve_::mfrr, &T::reserve_::pair_::down, &T::reserve_::spec_::result>,
        url_attr_path<&T::reserve, &T::reserve_::mfrr, &T::reserve_::pair_::up, &T::reserve_::spec_::result>
      );
    else if constexpr (std::is_same_v<T, stm::waterway>)
      return std::make_tuple(
        url_attr_path<&T::discharge, &T::discharge_::result>,
        url_attr_path<&T::discharge,&T::discharge_::penalty,&T::discharge_::penalty_::result,&T::discharge_::penalty_::result_::constraint_min>,
        url_attr_path<&T::discharge,&T::discharge_::penalty,&T::discharge_::penalty_::result,&T::discharge_::penalty_::result_::constraint_max>,
        url_attr_path<&T::discharge,&T::discharge_::penalty,&T::discharge_::penalty_::result,&T::discharge_::penalty_::result_::ramping_up>,
        url_attr_path<&T::discharge,&T::discharge_::penalty,&T::discharge_::penalty_::result,&T::discharge_::penalty_::result_::ramping_down>,
        url_attr_path<&T::discharge,&T::discharge_::penalty,&T::discharge_::penalty_::result,&T::discharge_::penalty_::result_::accumulated_min>,
        url_attr_path<&T::discharge,&T::discharge_::penalty,&T::discharge_::penalty_::result,&T::discharge_::penalty_::result_::accumulated_max>
      );
    else if constexpr (std::is_same_v<T, stm::gate>)
      return std::make_tuple(
        url_attr_path<&T::discharge, &T::discharge_::result>,
        url_attr_path<&T::opening, &T::opening_::result>
      );
    else if constexpr (std::is_same_v<T, stm::unit_group>)
      return std::make_tuple(
        url_attr_path<&T::obligation, &stm::unit_group::obligation_::result>,
        url_attr_path<&T::obligation, &stm::unit_group::obligation_::penalty>
      );
    else
      return std::make_tuple();
  }

  // clang-format on

  namespace detail {
    template <typename path, typename buffer, typename component, typename function>
    constexpr auto url_with_attrs_entry(buffer &buf, component &comp, function &func) {

      auto out = std::back_inserter(buf);
      auto &attr =
        boost::hana::fold(
          path{},
          std::ref(comp),
          [&]<auto step>(auto attr_ref, constant_t<step>) {
            out = url_format_attr_to(out, {hana_member_name<step>.c_str()});
            return std::ref((attr_ref.get()).*step);
          })
          .get();

      std::invoke(func, attr, std::string_view{buf.data(), buf.size()});
    }

    template <auto attr_map, typename buffer, typename comp, typename func>
    constexpr auto url_with_attrs_table_impl() {
      using entry = void (*)(buffer &, comp &, func &);
      return std::apply(
        []<typename... path>(path...) {
          return std::array<entry, sizeof...(path)>{&url_with_attrs_entry<path, buffer, comp, func>...};
        },
        attr_map(identity_v<std::remove_const_t<comp>>));
    }

    template <auto attr_map, typename buffer, typename comp, typename func>
    inline constexpr auto url_with_attrs_table = url_with_attrs_table_impl<attr_map, buffer, comp, func>();

  }

  template <auto attr_map>
  auto url_with_attrs(auto &buffer, auto &&comp, auto &&func) {

    using B = std::remove_reference_t<decltype(buffer)>;
    std::array<std::size_t, url_max_path_length() + 1> stack;

    [[maybe_unused]] auto r = component_traverse(
      comp,
      0,
      [&]<typename tag, typename component>(int idx, tag, component &subcomp) {
        stack.at(idx) = buffer.size();

        url_format_step_to(
          std::back_inserter(buffer), url_make_step<subcomponent_member_v<tag>>(component_id(subcomp)));
        using S = std::remove_reference_t<decltype(subcomp)>;
        using F = std::remove_reference_t<decltype(func)>;
        const auto attr_begin = buffer.size();
        for (auto with_attrs : detail::url_with_attrs_table<attr_map, B, S, F>) {
          std::invoke(with_attrs, buffer, subcomp, func);
          buffer.resize(attr_begin);
        }
        return ++idx;
      },
      [&]<typename tag, typename component>(int idx, tag, component &) {
        buffer.resize(stack.at(--idx));
        return idx;
      });
    assert(r == 0);
  }

  constexpr auto url_with_planning_inputs(std::string_view model_id, concepts::stm_system auto &model, auto &&func) {
    url_string_buffer buffer{};
    url_format_root_to(std::back_inserter(buffer), model_id);
    return url_with_attrs< [](auto comp_type) {
      return url_planning_input_paths(comp_type);
    } >(buffer, model, func);
  }

  constexpr auto url_with_planning_outputs(std::string_view model_id, concepts::stm_system auto &model, auto &&func) {
    url_string_buffer buffer{};
    url_format_root_to(std::back_inserter(buffer), model_id);
    return url_with_attrs< [](auto comp_type) {
      return url_planning_output_paths(comp_type);
    } >(buffer, model, func);
  }

  /** @brief Generate ts-result urls for a given stm_system
   *
   * @details
   * Given a stm_system, generate and return a complete vector with all the time-series urls
   * that are considered result, or might contain result, after running an algorithmic process.
   * The primar usage is between the master/slave dstm, but it is also exposed to python,
   * so that it can be utilized in scripting as well.
   * @param prefix like dstm://M<mid>
   * @return a complete list of all ts-urls, like dstm://M<mid>/H1/R1.volume.result etc.
   */
  std::vector<std::string> url_planning_inputs(std::string_view, const stm_system &);
  std::vector<std::string> url_planning_outputs(std::string_view, const stm_system &);

  // FIXME:
  //   move this, and use string_view for parsing, don't need 0-term! - jeh
  using url_parse_buffer = shyft::web_api::grammar::stm_url_result;

  bool url_parse(url_parse_buffer &, const char *);

  bool url_set_attr(stm_system &, const url_parse_buffer &, const any_attr &);

  inline bool url_set_attr(stm_system &model, const std::string &url, const any_attr &attr) {
    url_parse_buffer url_buffer{};
    url_parse(url_buffer, url.c_str());
    return url_set_attr(model, url_buffer, attr);
  }

  // FIXME: don't need c_str() - jeh
  inline std::vector<bool>
    url_set_attrs(stm_system &model, const std::vector<std::pair<std::string, any_attr>> &attrs) {

    url_parse_buffer url_buffer;

    std::vector<bool> result(attrs.size());
    for (auto i : std::views::iota(0ul, attrs.size())) {
      auto &[url, attr] = attrs[i];
      url_parse(url_buffer, url.c_str());
      result[i] = url_set_attr(model, url_buffer, attr);
    }
    return result;
  }

  std::optional<any_attr> url_get_attr(const stm_system &, const url_parse_buffer &);

  inline std::optional<any_attr> url_get_attr(const stm_system &model, const std::string &url) {
    url_parse_buffer url_buffer{};
    url_parse(url_buffer, url.c_str());
    return url_get_attr(model, url_buffer);
  }

  inline std::vector<std::optional<any_attr>>
    url_get_attrs(const stm_system &model, const std::vector<std::string> &attrs) {

    url_parse_buffer url_buffer;

    std::vector<std::optional<any_attr>> result(attrs.size());
    for (auto i : std::views::iota(0ul, attrs.size())) {
      auto &url = attrs[i];
      url_parse(url_buffer, url.c_str());
      result[i] = url_get_attr(model, url_buffer);
    }
    return result;
  }

}
