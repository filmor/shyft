#pragma once
#include <cstdint>
#include <type_traits>

#include <shyft/core/reflection.h>

namespace shyft::energy_market::stm {

SHYFT_DEFINE_ENUM(
    unit_group_type,
    std::uint16_t,
    (unspecified,
     //-- operational reserve groups
     fcr_n_up, fcr_n_down, fcr_d_up, fcr_d_down, afrr_up, afrr_down, mfrr_up, mfrr_down, ffr, rr_up, rr_down,
     //-- spinning
     commit,
     //-- with production requirement
     production));

constexpr bool is_operational_reserve_type(unit_group_type e){
    return unit_group_type::fcr_n_up <= e && e <= unit_group_type::ffr;
}

}

