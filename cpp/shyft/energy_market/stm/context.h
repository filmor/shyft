#pragma once

#include <cstdint>
#include <functional>
#include <future>
#include <map>
#include <memory>
#include <stdexcept>
#include <string>
#include <string_view>
#include <thread>
#include <utility>

#include <fmt/core.h>
#include <boost/asio/post.hpp>
#include <boost/asio/use_future.hpp>

#include <shyft/core/reflection.h>
#include <shyft/core/shared_strand.h>
#include <shyft/energy_market/stm/log_entry.h>
#include <shyft/energy_market/stm/model.h>

namespace shyft::energy_market::stm {

  /// Enum indicating the state of a shared stm-model.
  ///   - idle:  Model is available in full to the user
  ///   - running: Model is currently running an optimizing procedure, limited availability
  ///   - tuning: Indicates that model is being tuned, used is experimenting.
  ///   - finished: Indicates that the algorithm is finished with success and results are ready.
  ///   - failed: Indicates that the algorithm failed to produce results for the model.
  SHYFT_DEFINE_ENUM(model_state, std::int8_t, (idle, running, tuning, finished, failed));

  struct model_info {
    model_state state;
    std::vector<log_entry> log{};
  };

  template <bool is_const>
  struct model_view {
    std::shared_ptr<add_const_if_t<is_const, stm_system>> model;
    std::shared_ptr<add_const_if_t<is_const, model_info>> info;
  };

  using const_model_view = model_view<true>;
  using mutable_model_view = model_view<false>;

  template <typename T>
  concept model_mutator = std::invocable<T, mutable_model_view>;

  template <typename T>
  using model_mutator_result = std::invoke_result_t<T, mutable_model_view>;

  template <typename T, typename U>
  concept model_mutator_fallback =
    std::invocable<T> && std::is_same_v<std::invoke_result_t<T>, model_mutator_result<U>>;

  template <typename T>
  concept model_observer = std::invocable<T, const_model_view>;

  template <typename T>
  using model_observer_result = std::invoke_result_t<T, const_model_view>;

  template <typename T, typename U>
  concept model_observer_fallback =
    std::invocable<T> && std::is_same_v<std::invoke_result_t<T>, model_observer_result<U>>;

  template <typename Executor>
  struct shared_model {

    shared_strand<Executor> strand;
    std::shared_ptr<stm_system> model;
    std::shared_ptr<model_info> info;

    static auto post_impl(auto &&strand, auto view, auto &&action) {
      if(!view.model || !view.info)
        throw std::runtime_error("invalid model");
      return boost::asio::post(
        SHYFT_FWD(strand), boost::asio::use_future([view = std::move(view), action = SHYFT_FWD(action)] {
          return std::invoke(SHYFT_FWD(action), std::move(view));
        }));
    }

    auto mutate(model_mutator auto &&action) {
      return post_impl(strand.unique(), mutable_model_view{.model = model, .info = info}, SHYFT_FWD(action));
    }

    auto observe(model_observer auto &&action) const {
      return post_impl(
        strand, const_model_view{.model = std::const_pointer_cast<stm_system>(model), .info = std::const_pointer_cast<model_info>(info)}, SHYFT_FWD(action));
    }
  };

  template<typename Executor>
  std::shared_ptr<shared_model<Executor>> make_shared_model(Executor executor,std::shared_ptr<stm_system> model){
    return std::make_shared<shared_model<Executor>>(shared_model<Executor>{
        .strand = make_shared_strand(executor),
        .model = std::move(model),
        .info = std::make_shared<model_info>()});
  }

  template <typename E>
  using shared_model_container = std::map<std::string, std::shared_ptr<shared_model<E>>, std::less<>>;

  template <typename T, typename E>
  concept models_mutator = std::invocable<T, shared_model_container<E> &>;

  template <typename T, typename E>
  concept models_observer = std::invocable<T, const shared_model_container<E> &>;

  template <typename Executor>
  struct shared_models {

    shared_strand<Executor> strand;
    shared_model_container<Executor> container;

    static auto post_impl(auto &&strand, auto &models, auto &&action) {
      return boost::asio::post(SHYFT_FWD(strand), boost::asio::use_future([&, action = SHYFT_FWD(action)] {
                                 return std::invoke(SHYFT_FWD(action), models);
                               }));
    }

    auto mutate(models_mutator<Executor> auto &&action) {
      return post_impl(strand.unique(), container, SHYFT_FWD(action));
    }

    auto observe(models_observer<Executor> auto &&action) const {
      return post_impl(strand, container, SHYFT_FWD(action));
    }

    auto find(std::string_view model_id) {
      return observe(
        [&, model_id = std::string(model_id)](const auto &models) -> std::shared_ptr<shared_model<Executor>> {
          auto it = models.find(model_id);
          if (it == models.end())
            return nullptr;
          return it->second;
        });
    }

    template <model_mutator A>
    auto mutate_or(std::string_view model_id, A &&action, model_mutator_fallback<A> auto &&fallback) {
      auto model = find(model_id).get();
      if (!model)
        return std::async(std::launch::deferred, SHYFT_FWD(fallback));
      return model->mutate(SHYFT_FWD(action));
    }

    template <model_observer A>
    auto observe_or(std::string_view model_id, A &&action, model_observer_fallback<A> auto &&fallback) {
      auto model = find(model_id).get();
      if (!model)
        return std::async(std::launch::deferred, SHYFT_FWD(fallback));
      return model->observe(SHYFT_FWD(action));
    }

    template <model_mutator A>
    auto mutate_or_throw(std::string_view model_id, A &&action) {
      return mutate_or(model_id, SHYFT_FWD(action), [model_id = std::string(model_id)]() -> model_mutator_result<A> {
        throw std::runtime_error(fmt::format("invalid model: '{}'", model_id));
      });
    }

    template <model_observer A>
    auto observe_or_throw(std::string_view model_id, A &&action) {
      return observe_or(model_id, SHYFT_FWD(action), [model_id = std::string(model_id)]() -> model_observer_result<A> {
        throw std::runtime_error(fmt::format("invalid model: '{}'", model_id));
      });
    }

    auto add(std::string_view model_id, std::shared_ptr<stm_system> model = std::make_shared<stm_system>()) {
      return mutate([&, model_id = std::string(model_id), model = std::move(model)](auto &container) {
        return container
          .try_emplace(
            model_id,
            make_shared_model(strand.get_inner_executor(), std::move(model))).second;
      });
    }

    auto remove(std::string_view model_id) {
      return mutate([&, model_id = std::string(model_id)](auto &container) {
        auto it = container.find(model_id);
        if (it == container.end())
          return false;
        container.erase(it);
        return true;
      });
    }

    auto copy(std::string_view source, std::string_view target) {
      // FIXME:
      //   not good, need chaining.
      //   not sure cloning should require a mutable ptr.
      //   - jeh
      auto target_model =
        mutate_or(
          source,
          [&](auto &&view) { return stm_system::clone_stm_system(view.model); },
          [&] { return std::shared_ptr<stm_system>{}; })
          .get();
      if (!target_model)
        return std::async(std::launch::deferred, [] { return false; });
      rebind_ts(*target_model, std::string(target));
      return mutate([&, target_model = std::move(target_model), target = std::string(target)](auto &container) {
        if (container.find(target) != container.end())
          return false;
        container.emplace(
          target,
          make_shared_model(strand.get_inner_executor(), std::move(target_model)));
        return true;
      });
    }
  };

  template <typename T>
  concept shared_models_init =
    std::ranges::input_range<T>
    && std::is_same_v<std::ranges::range_value_t<T>, std::pair<const std::string, std::shared_ptr<stm_system>>>;

  template <
    typename Executor,
    shared_models_init T = std::initializer_list<std::pair<const std::string, std::shared_ptr<stm_system>>>>
  auto make_shared_models(Executor executor, T models = {}) {
    auto initial_shared_models = std::views::transform(models, [&](auto &&model) {
      return std::pair{
        model.first,
        make_shared_model(executor, std::move(model.second))
      };
    });
    return shared_models<Executor>{
      .strand = make_shared_strand(executor),
      .container =
        shared_model_container<Executor>{std::begin(initial_shared_models), std::end(initial_shared_models)}
    };
  }

  /**
     * @brief rebind_expression
     * @details
     *
     * This routine traverses all time-series of the locked and mutable stm_system mdl,
     * its purpose is to be used after optimization/computation have updated the result terminals
     * of the stm_system. In that case, any expressions refering to those terminals needs to be
     * properly recomputed/reconfigured for computation.
     *
     * The algorithm outline:
     * for each ts of the model, if it's an expression,
     *   find all the terminal nodes of the expression
     *     and save their bound time-series
     *   then IF there was 1 or more dstm://refs,
     *        unbind the expression (will wipe out bound entities)
     *        then put into place the saved bound time-series
     *        finally, re-evaluate/prepare the expression.
     *
     * @param mdl a locked exclusive mode stm_system
     * @param new_mkey the dstm m_key for the stm_system
     * @param nan_fill_missing if true, then replace empty/unbound terminals with nan ts using mdl. run_time_axis
     * @return true if any rebind occured (e.g. there was at least one dtstm://new_mkey/ ref. in the expressions)
     */
  bool rebind_expression(stm_system& mdl, std::string const & new_mkey, bool nan_fill_missing);


}
