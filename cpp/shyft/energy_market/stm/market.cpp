#include <algorithm>
#include <iterator>
#include <memory>
#include <ranges>
#include <string>
#include <stdexcept>

#include <shyft/energy_market/em_utils.h>
#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/transmission_line.h>
#include <shyft/energy_market/stm/busbar.h>
#include <shyft/energy_market/stm/power_module.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/dd/apoint_ts.h>

namespace shyft::energy_market::stm {
    using std::make_shared;

    energy_market_area::energy_market_area(){mk_url_fx(this);};
    
    energy_market_area::energy_market_area(int id, const string& name, const string& json, const stm_system_& sys) 
        : super{id,name,json,{},{}}, sys{sys}  {mk_url_fx(this);}
        
    void energy_market_area::generate_url(std::back_insert_iterator<std::string>& rbi, int levels, int template_levels) const {
        if (levels) {
            auto tmp = sys_();
            if (tmp) tmp->generate_url(rbi, levels-1, template_levels ? template_levels - 1 : template_levels);
        }
        if (!template_levels) {
            constexpr std::string_view a = "/m{o_id}";
            std::copy(std::begin(a), std::end(a), rbi);
        } else {
            auto a="/m"+std::to_string(id);
            std::copy(std::begin(a), std::end(a), rbi);
        }
    }
    bool energy_market_area::operator==(energy_market_area const&o) const {
        if(this==&o) return true;//equal by addr.
        auto ug_equal= equal_vector_ptr_content<unit_group>(unit_groups, o.unit_groups);
        auto contracts_equal= equal_vector_ptr_content<contract>(contracts, o.contracts);
        return hana::fold( // hana::any_of ... does not compile at all(even the example) on ms windows s c++ , so we use this that seems to be robust cross platform construct
            mp::leaf_accessors(hana::type_c<energy_market_area>),
            ug_equal && contracts_equal && super::operator==(o),//initial value of the fold
            [this, &o](bool s, auto&& a) {
                return s?stm::equal_attribute(mp::leaf_access(*this, a), mp::leaf_access(o, a)):false; // only evaluate equal if the fold state is still true
            }
        );
    }

    unit_group_ energy_market_area::create_busbar_derived_unit_group(int id, const string& name, const string& json) {
        auto sys = sys_();
        if (!sys) return nullptr;
        
        if (busbar_derived_unit_group)
            throw std::runtime_error("A busbar derived unit group is already created. To overwrite, call remove_unit_group() first");

        auto ug = sys->add_unit_group(id, name, json, unit_group_type::production);
        set_unit_group(ug);
        // Populate existing associations
        for (const auto& busbar : busbars) {
            for (const auto& unit_member : busbar->units) {
                ug->add_unit(unit_member->unit, unit_member->active);
            }
        }
        ug->update_sum_expressions();
        busbar_derived_unit_group = ug;
        return ug;
    }

    unit_group_ energy_market_area::get_busbar_derived_unit_group() const {
        return busbar_derived_unit_group;
    }
    void energy_market_area::set_busbar_derived_unit_group(unit_group_ const&ug) {
        busbar_derived_unit_group=ug;//used by add_patch to align patch  models
        if(busbar_derived_unit_group)
            busbar_derived_unit_group->update_sum_expressions();
    }
    energy_market_area::ts_triplet_result energy_market_area::get_busbars_power_sum(power_sum_type sum_type) const {
        using shyft::time_series::dd::ats_vector;
        energy_market_area::ts_triplet_result sum;

        auto to_sum_type = [sum_type](const apoint_ts& ts) {
            if (sum_type == power_sum_type::production_only)
                return max(ts, 0); // Remove negative values

            // Consumption. Remove positive values, then invert
            return min(ts,0)*-1;
        };

        auto sum_power = [this, to_sum_type](auto&& get_unit_attr_fx, auto&& get_power_module_attr_fx) -> apoint_ts {
            ats_vector p;
            for (const auto& busbar : busbars) {
                for (const auto& unit_member : busbar->units) {
                    const auto& unit_ts = get_unit_attr_fx(unit_member);
                    if (unit_ts.ts) {
                        if (unit_member->active.ts) {
                            p.emplace_back(to_sum_type(unit_ts)*unit_member->active);
                        } else {
                            p.emplace_back(to_sum_type(unit_ts));
                        }
                    }
                } 
                for (const auto& pm_member : busbar->power_modules) {
                    const auto& pm_ts = get_power_module_attr_fx(pm_member);
                    if (pm_ts.ts) {
                        if (pm_member->active.ts) {
                            p.emplace_back(to_sum_type(pm_ts)*pm_member->active);
                        } else {
                            p.emplace_back(to_sum_type(pm_ts));
                        }
                    }
                }
            }
            return p.size() > 0 ? p.sum() : apoint_ts{};
        };

        sum.schedule = sum_power([](const auto& unit_member) { return unit_member->unit->production.schedule; },
                                 [](const auto& pwr_mod_member) { return pwr_mod_member->power_module->power.schedule;});

        sum.result = sum_power([](const auto& unit_member) { return unit_member->unit->production.result; },
                                 [](const auto& pwr_mod_member) { return pwr_mod_member->power_module->power.result;});

        sum.realised = sum_power([](const auto& unit_member) { return unit_member->unit->production.realised; },
                                 [](const auto& pwr_mod_member) { return pwr_mod_member->power_module->power.realised;});
        return sum;
    }

    energy_market_area::ts_triplet_result energy_market_area::get_production() const {
        return get_busbars_power_sum(power_sum_type::production_only);
    }

    energy_market_area::ts_triplet_result energy_market_area::get_consumption() const {
        return get_busbars_power_sum(power_sum_type::consumption_only);
    }

    energy_market_area::ts_triplet_result energy_market_area::get_busbars_flow_sum(flow_type flowtype) const {
        using shyft::time_series::dd::ats_vector;
        energy_market_area::ts_triplet_result sum; 

        auto sum_busbars = [this, flowtype](auto&& fx_get_attribute) {
            ats_vector p;
            for (const auto& busbar : busbars) {
                const auto& result = fx_get_attribute(busbar);
                if (result.ts)
                    p.emplace_back(result);
            }
            if (p.size() == 0)
                return apoint_ts{};

            if (flowtype == flow_type::import)
                return max(p.sum(), 0); // positive values == import. Remove negative values

            // negative values == export. Remove positive values, then invert
            return min(p.sum(),0)*-1;
        };

        sum.schedule = sum_busbars([](const auto& busbar) { return busbar->flow.schedule; });
        sum.result = sum_busbars([](const auto& busbar) { return busbar->flow.result; });
        sum.realised = sum_busbars([](const auto& busbar) { return busbar->flow.realised; });
        return sum;
    }

    energy_market_area::ts_triplet_result energy_market_area::get_import() const {
        return get_busbars_flow_sum(flow_type::import);
    }

    energy_market_area::ts_triplet_result energy_market_area::get_export() const {
        return get_busbars_flow_sum(flow_type::exp);
    }

    void energy_market_area::set_unit_group(const unit_group_& ug) {
        if(!ug) {
            if(unit_groups.size()) // assign a nullptr, means remove unit group.
                unit_groups.pop_back();
            return;
        }
        if(ug->group_type != unit_group_type::production) {
            throw std::runtime_error("energy market price area unit group type was "+std::to_string(etoi(ug->group_type))
                                     + ", must be "+ std::to_string(etoi(unit_group_type::production)));
        }
        if(ug->id <= 0 ) {
            throw std::runtime_error("energy market price area unit group id must be >0, name="+ug->name
                                    + ", supplied value was " +std::to_string(ug->id));
        }
        if (unit_groups.size() == 1) {
            unit_groups[0]=ug;
        } else {
            unit_groups.push_back(ug);
        }
    }

    unit_group_ energy_market_area::get_unit_group() const {
        return unit_groups.empty() ? nullptr : unit_groups[0];
    }

    unit_group_ energy_market_area::remove_unit_group() {
        if (unit_groups.empty())
            throw std::runtime_error("there is no unit group associated with this energy market area");
        unit_group_ ug = unit_groups[0];
        unit_groups.clear();
        busbar_derived_unit_group.reset();
        return ug;
    }

    vector<transmission_line_> energy_market_area::transmission_lines_to(const energy_market_area_& to) const {
        vector<transmission_line_> tr_lines;
        // from me, to other market area
        for (auto b : busbars) {
            for (auto t : b->get_transmission_lines_from_busbar()) {
                if (busbar_exists_in_market_area(to, t->to_bb))
                    insert(tr_lines, t);
            }
        }
        return tr_lines;
    }

    vector<transmission_line_> energy_market_area::transmission_lines_from(const energy_market_area_& from) const {
        vector<transmission_line_> tr_lines;
        // to me, from other market area
        for (auto b : busbars) {
            for (auto t : b->get_transmission_lines_to_busbar()) {
                if (busbar_exists_in_market_area(from, t->from_bb))
                    insert(tr_lines, t);
            }
        }
        return tr_lines;
    }

    bool energy_market_area::busbar_exists_in_market_area(const energy_market_area_& ma, const busbar_& b) {
        return std::ranges::find_if(ma->busbars, [&b](const busbar_& b2) { return b == b2; }) != std::ranges::end(ma->busbars);
    }

    void energy_market_area::insert(vector<transmission_line_>& tr_lines, transmission_line_ t) {
        if (std::ranges::find_if(tr_lines, [&t](const transmission_line_& t2) { return t == t2; }) == std::ranges::end(tr_lines))
            tr_lines.push_back(t);
    }
}
