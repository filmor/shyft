#include <shyft/energy_market/em_utils.h>
#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/stm/contract_portfolio.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/stm_system.h>

namespace shyft::energy_market::stm {
    namespace hana = boost::hana;
    namespace mp = shyft::mp;
    
    using std::static_pointer_cast;
    using std::dynamic_pointer_cast;
    using std::make_shared;
    using std::runtime_error;
    using std::to_string;
    using shyft::energy_market::equal_vector_ptr_content;

    void contract::generate_url(std::back_insert_iterator<std::string>& rbi, int levels, int template_levels) const {
        if (levels) {
            auto tmp = sys_();
            if (tmp) tmp->generate_url(rbi, levels-1, template_levels ? template_levels - 1 : template_levels);
        }
        if (!template_levels) {
            constexpr std::string_view a = "/c{o_id}";
            std::copy(std::begin(a), std::end(a), rbi);
        } else {
            auto a="/c"+std::to_string(id);
            std::copy(std::begin(a), std::end(a), rbi);
        }
    }

    bool contract::operator==(contract const&o) const {
        if(this==&o) return true;//equal by addr.
        // compare power_plants
        auto pp_equal= equal_vector_ptr_content<power_plant>(power_plants,o.power_plants);
        auto r_equal = equal_vector_ptr_content<contract_relation>(relations,o.relations);

        return hana::fold( // hana::any_of ... does not compile at all(even the example) on ms windows s c++ , so we use this that seems to be robust cross platform construct
            mp::leaf_accessors(hana::type_c<contract>),
            pp_equal && r_equal && super::operator==(o),//initial value of the fold
            [this, &o](bool s, auto&& a) {
                return s?stm::equal_attribute(mp::leaf_access(*this, a), mp::leaf_access(o, a)):false; // only evaluate equal if the fold state is still true
            }
        );
    }

    contract_ contract::shared_from_this() const {
        auto s = sys_();
        if (s) {
            for (auto const& c : s->contracts)
                if(c.get()==this) return c;
        }
        return nullptr;
    }

    vector<contract_portfolio_> contract::get_portfolios() const {
        vector<contract_portfolio_> result;
        auto s = sys_();
        if(s) {
            for (auto const& cp : s->contract_portfolios) {
                for (auto const& c : cp->contracts) {
                    if (c.get() == this) {
                        result.push_back(cp);
                    }
                }
            }
        }
        return result;
    }
    void contract::add_to_portfolio(contract_portfolio_ portfolio) const {
        auto s = sys_();
        if(s) {
            auto me = shared_from_this();
            if (!me) throw std::runtime_error("this contract is not associated with a stm system");
            portfolio->contracts.push_back(me);
        }
    }

    vector<energy_market_area_> contract::get_energy_market_areas() const {
        vector<energy_market_area_> result;
        auto s = sys_();
        if(s) {
            for (auto const& ema : s->market) {
                for (auto const& c : ema->contracts) {
                    if (c.get() == this) {
                        result.push_back(ema);
                    }
                }
            }
        }
        return result;
    }

    void contract::add_to_energy_market_area(energy_market_area_ market) const {
        auto me = shared_from_this();
        if (!me) throw std::runtime_error("this contract is not associated with a stm system");
        market->contracts.push_back(me);
    }

    bool is_in_loop(contract* c_this, const contract_relation_& cr, uint16_t relation_type) {
        if (cr->relation_type != relation_type) // only consider loops of same relation_type
            return false;
        if (c_this == cr->related.get()) // found self on path, it is a loop
            return true;

        for (const auto& r : cr->related->relations) {
            return is_in_loop(c_this, r, relation_type);
        }
        
        return false;
    }

    contract_relation_ contract::add_relation(int64_t relation_id, contract_ c, uint16_t relation_type) {
        if (c.get() == this)
            throw std::runtime_error("Not possible to create relation to self");
        auto me=shared_from_this(); // require shared ptr paths are healthy here.
        if(!me) {
            throw std::runtime_error("A contract must be part of a StmSystem prior to adding relations");
        }

        // Check if relation/relation-type to c already exists
        for (const auto& existing_r : relations) {
            if ((existing_r->related == c) && (existing_r->relation_type == relation_type)) {
                throw std::runtime_error("contract id=" + to_string(c->id) + " of type " + to_string(relation_type) + " is already related to this contract");
            } else if (existing_r->id == relation_id) {
                throw std::runtime_error("contract_relation of id=" + to_string(relation_id) + " already exists for this contract! Must be unique.");
            }
        }

        for (const auto& r : c->relations) { // Does c.relations already include 'this', e.g. a loop?
            if (is_in_loop(this, r, relation_type))
                throw std::runtime_error("Loop of linked relation_type=" + to_string(relation_type) + " when trying to add relation from contract id=" + to_string(id) + " to id=" + to_string(c->id));
        }

        auto r = make_shared<contract_relation>(relation_id, me, c, relation_type);
        relations.push_back(r);
        return r;
    }

    void contract::remove_relation(const contract_relation_& cr) {
        if (cr == nullptr)
            throw std::runtime_error("remove_relation: relation does not exist");
        auto it = std::find_if(relations.begin(), relations.end(), [&cr](const auto& r) {
            return r->id == cr->id;
        });
        if (it != relations.end())
            relations.erase(it);
    }

    //-- contract_relation
    contract_relation::contract_relation(){mk_url_fx(this);}
    contract_relation::contract_relation(int64_t id, contract_ const& owner, contract_ const& related, uint16_t relation_type)
        :owner{owner},related{related},id{id},relation_type{relation_type}{
        mk_url_fx(this);

    }


    contract_relation_ contract_relation::shared_from_this() const {
        // in short: use the owner.relations to find shared ptr matching this.
        auto o= owner_();
        if(!o)
            return nullptr;
        for(auto const& c:o->relations)
            if(c.get()==this)
                return c;
        return nullptr;
    }

    bool contract_relation::operator==(const contract_relation& other) const {
        return  hana::fold( // hana::any_of ... does not compile at all(even the example) on ms windows s c++ , so we use this that seems to be robust cross platform construct
            mp::leaf_accessors(hana::type_c<contract_relation>),
            true,//initial value of the fold
            [this, &other](bool s, auto&& a) {
                return s?stm::equal_attribute(mp::leaf_access(*this, a), mp::leaf_access(other, a)):false; // only evaluate equal if the fold state is still true
            }
        ) && (related==other.related ||(related&&other.related&&(*related==*other.related)));// require pointers to be equal in this case
    }
    
    void contract_relation::generate_url(std::back_insert_iterator<string>& rbi, int levels, int template_levels) const {
        auto o=owner_();
        if (levels && o) {
            o->generate_url(rbi, levels - 1, template_levels ? template_levels - 1 : template_levels);
        }
        if (!template_levels) {
            constexpr std::string_view a = "/r{o_id}";
            std::copy(std::begin(a), std::end(a), rbi);
        } else {
            auto idstr="/r"+ (related?std::to_string(related->id):std::string("?"));
            std::copy(std::begin(idstr),std::end(idstr),rbi);
        }
    }
}
