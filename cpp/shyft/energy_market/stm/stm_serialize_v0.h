#pragma once
/** This file is part of Shyft. Copyright 2015-2022 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

//
// v0 of stm structs as of the time of 4.26.27, used for deserialization of old
// archives//clients.
// included by stm_serialization.cpp (only!)
//
namespace shyft::energy_market::stm::v0 {
    // waterway.h v0
    // place holder only for _v0 hana leaf struct layout
    struct waterway  {
        struct geometry_ {
            BOOST_HANA_DEFINE_STRUCT(geometry_,
                (apoint_ts,length),  ///< m, the length of the waterway, the ts reflect that it can change
                (apoint_ts,diameter),///< m, the diameter of typically a tunnel
                (apoint_ts,z0),      ///< masl at first end
                (apoint_ts,z1)       ///< masl at second end
            );
        };

        struct discharge_ {
            struct constraint_ {
                BOOST_HANA_DEFINE_STRUCT(constraint_,
                    (apoint_ts, min),
                    (apoint_ts, max),
                    (apoint_ts, ramping_up),
                    (apoint_ts, ramping_down),
                    (apoint_ts, accumulated_min),
                    (apoint_ts, accumulated_max)
                );
            };

            struct penalty_ {
                struct cost_ {
                    BOOST_HANA_DEFINE_STRUCT(cost_,
                        (apoint_ts, constraint_min),
                        (apoint_ts, constraint_max),
                        (apoint_ts, ramping_up),
                        (apoint_ts, ramping_down),
                        (apoint_ts, accumulated_min),
                        (apoint_ts, accumulated_max)
                    );
                };
                struct result_ {
                    BOOST_HANA_DEFINE_STRUCT(result_,
                        (apoint_ts, constraint_min),
                        (apoint_ts, constraint_max),
                        (apoint_ts, ramping_down),
                        (apoint_ts, ramping_up),
                        (apoint_ts, accumulated_min),
                        (apoint_ts, accumulated_max)
                    );
                };
                BOOST_HANA_DEFINE_STRUCT(penalty_,
                    (cost_, cost),
                    (result_, result)
                );
            };

            BOOST_HANA_DEFINE_STRUCT(discharge_,
                (apoint_ts, schedule), // m3/s scheduled flow
                (apoint_ts, reference),// m3/s constraint reference, acc-dev, could be different from schedule
                (apoint_ts, static_max),// m3/s abs. value, both directions
                (apoint_ts, result),    // m3/s as in result from optimization/simulation
                (apoint_ts, realised),  // m3/s as in best estimated/measured/observed historical fact
                (constraint_, constraint),
                (penalty_, penalty)
            );
        };

        BOOST_HANA_DEFINE_STRUCT(waterway,
            (apoint_ts,head_loss_coeff),   ///< m/(m3/s)**2 head_loss = q*abs(q)*head_loss_coeff
            (t_xyz_list_,head_loss_func),  ///< head_loss= f(flow,delta_h) for complex head-loss, using table lookup
            (t_xy_, delay),
            (geometry_,geometry),
            (discharge_,discharge)
        );
    };


    struct gate {
        struct opening_ {
            struct constraint_ {
                BOOST_HANA_DEFINE_STRUCT(constraint_,
                    (t_xy_, positions),     ///< x: arbitrary position / y: opening factor
                    (apoint_ts, continuous) ///< x: time / y: 0 or 1
                );
            };
            BOOST_HANA_DEFINE_STRUCT(opening_,
                (apoint_ts, schedule),
                (apoint_ts, realised),
                (constraint_, constraint),
                (apoint_ts, result)
            );
        };

        struct discharge_ {
            struct constraint_ {
                BOOST_HANA_DEFINE_STRUCT(constraint_,
                    (apoint_ts, min), ///< m3/s
                    (apoint_ts, max)  ///< m3/s
                );
            };
            BOOST_HANA_DEFINE_STRUCT(discharge_,
                (apoint_ts, static_max),
                (apoint_ts, schedule),
                (apoint_ts, realised),
                (constraint_, constraint),
                (apoint_ts, merge_tolerance), ///< m3/s, Max deviation in discharge between two timesteps
                (apoint_ts, result)
            );
        };

        BOOST_HANA_DEFINE_STRUCT(gate,
            (t_xyz_list_, flow_description),
            (t_xyz_list_, flow_description_delta_h),
            (apoint_ts, cost), ///< money
            (opening_, opening),
            (discharge_, discharge)
        );
    };
    //
    // unit_group.h v0
    //
    struct unit_group {
        struct obligation_ {
            BOOST_HANA_DEFINE_STRUCT(obligation_,
                (apoint_ts, schedule), ///< W scheduled or target obligation, system should provide at least this amount
                (apoint_ts, cost),     ///< money/W if schedule violation
                (apoint_ts, result),   ///< W resulting amount of obligation(usually schedule, or economical best result)
                (apoint_ts, penalty)   ///< money used in penalty cost due to violation
            );
        };
        struct delivery_ {
            BOOST_HANA_DEFINE_STRUCT(delivery_,
                (apoint_ts, schedule), ///< sum of unit scheduled delivery for unit group product
                (apoint_ts, realised), ///< sum of unit realised delivery, as in historical fact
                (apoint_ts, result)    ///< sum of unit result delivery result as from optimisation
            );
        };

        // Attributes:
        BOOST_HANA_DEFINE_STRUCT(unit_group,
            (uint16_t, group_type),    ///< unit_group_type
            (obligation_, obligation), ///< obligation description for the group
            (delivery_, delivery),     ///< sum delivery for the units
            (apoint_ts, production),   ///< [W] sum production in units
            (apoint_ts, flow)          ///< [m3/s] sum flow of units .
        );
    };
    struct unit_group_member {
        BOOST_HANA_DEFINE_STRUCT(unit_group_member,
            (apoint_ts, active) ///< [unit_less] if not set, always member, if set, active(t) determines the accumulated sum.
        );
    };
    //
    // file unit.h
    //
    struct unit {
        struct production_ {
            struct constraint_ {
                BOOST_HANA_DEFINE_STRUCT(constraint_,
                    (apoint_ts, min), ///< W
                    (apoint_ts, max)  ///< W
                );
            };
            BOOST_HANA_DEFINE_STRUCT(production_, // units are W, watt
                (apoint_ts, schedule),     ///< W
                (apoint_ts, commitment),   ///< (1, 0), indicating if unit should produce(1) or not(0), alternative to schedule
                (apoint_ts, realised),     ///< W  as in historical fact
                (apoint_ts, static_min),   ///< W
                (apoint_ts, static_max),   ///< W
                (apoint_ts, nominal),      ///< W
                (constraint_, constraint),
                (apoint_ts, result)        ///< W
            );
        };

        struct discharge_ {
            struct constraint_ {
                BOOST_HANA_DEFINE_STRUCT(constraint_,
                    (apoint_ts, min), ///< m3/s
                    (apoint_ts, max), ///< m3/s
                    (t_xy_, max_from_downstream_level) ///< masl -> m3/s
                );
            };
            BOOST_HANA_DEFINE_STRUCT(discharge_,
                (apoint_ts, result),   ///< m3/s
                (apoint_ts, schedule), ///< m3/s
                (apoint_ts, realised), ///< m3/s non trivial computation, based on production.realised etc.
                (constraint_, constraint)
            );
        };

        struct cost_ {
            BOOST_HANA_DEFINE_STRUCT(cost_,
                (apoint_ts, start),  ///< money/#start
                (apoint_ts, stop)    ///< money/#stop
            );
        };
        struct reserve_ {//
            struct spec_ {
                BOOST_HANA_DEFINE_STRUCT(spec_,
                    (apoint_ts, schedule), ///< W or % if droop, if specified 'schedule-mode' and next members ignored
                    (apoint_ts, min),      ///< W or % if droop
                    (apoint_ts, max),      ///< W or % if droop
                    (apoint_ts, cost),     ///< money
                    (apoint_ts, result),   ///< W or % if droop
                    (apoint_ts, penalty),  ///< money
                    (apoint_ts, realised)  ///< as in historical fact,possibly computed from production/min/max etc.
                );
            };
            struct pair_ {
                BOOST_HANA_DEFINE_STRUCT(pair_,
                    (spec_, up), ///< up regulation reserve
                    (spec_, down)///< down regulation reserve
                );
            };
            BOOST_HANA_DEFINE_STRUCT(reserve_,
                (apoint_ts, fcr_static_min),///< W if specified overrides long running static_min for FCR calculations
                (apoint_ts, fcr_static_max),///< W if specified overrides long running static_max for FCR calculations
                (pair_, fcr_n),///< FCR up,down
                (pair_, afrr), ///< aFRR up,down
                (pair_, mfrr), ///< mFRR up,down
                (apoint_ts, mfrr_static_min), ///< minimum production for RR
                (pair_, rr),   ///< RR up/down
                (pair_, fcr_d),///< FCR-D up/down
                (apoint_ts, fcr_mip),///< FCR flag for SHOP
                (pair_, frr),  ///< FRR  up,down
                (spec_, droop), ///< droop control, relates to fcr_n and fcr_d
                (t_xy_, droop_steps) ///< x=step number, asc, 1..n, y= discrete droop setting
            );
        };

        BOOST_HANA_DEFINE_STRUCT(unit,
            (apoint_ts, effective_head), ///< meter
            (t_xy_, generator_description),
            (t_turbine_description_, turbine_description),
            (t_xyz_list_, pump_description),
            (apoint_ts, unavailability), ///< no-unit, 1== unavailable, (0,nan)-> available
            (apoint_ts, priority),       ///< unit priority value for uploading order
            (production_, production),
            (discharge_, discharge),
            (cost_, cost),               ///< start/stop
            (reserve_, reserve)          ///< operational reserve, frequency balancing support
        );
    };

    //
    // run_parameters.h v0
    //
    struct run_parameters {
        BOOST_HANA_DEFINE_STRUCT(run_parameters,
            (uint16_t, n_inc_runs), ///< Number of runs with incremental
            (uint16_t, n_full_runs),///< Number of full runs
            (bool, head_opt),  ///< head optimization on/off
            (generic_dt, run_time_axis), ///< the run_time_axis for optimization/simulation/computation
            (vector<pair<utctime, string>>, fx_log) ///< the logs as collected from the algorithm execution
        );
    };
    //
    // reservoir_aggregate.h v0
    //
    struct reservoir_aggregate {
        struct inflow_ {
            BOOST_HANA_DEFINE_STRUCT(inflow_,
                (apoint_ts, schedule),   ///< m3/s
                (apoint_ts, realised),   ///< m3/s
                (apoint_ts, result)      ///< m3/s
            );
        };

        struct volume_ {
            BOOST_HANA_DEFINE_STRUCT(volume_,
                (apoint_ts, static_max), ///< m3
                (apoint_ts, schedule),   ///< m3
                (apoint_ts, realised),   ///< m3
                (apoint_ts, result)      ///< m3
            );
        };

        BOOST_HANA_DEFINE_STRUCT(reservoir_aggregate,
            (inflow_, inflow),
            (volume_, volume)
        );
    };
    //
    // reservoir.h v0
    //
    struct reservoir {
        struct level_ {
            struct constraint_ {
                BOOST_HANA_DEFINE_STRUCT(constraint_,
                    (apoint_ts, min),
                    (apoint_ts, max)
                );
            };
            BOOST_HANA_DEFINE_STRUCT(level_,
                (apoint_ts, regulation_min), ///< masl
                (apoint_ts, regulation_max), ///< masl
                (apoint_ts, realised),       ///< masl
                (apoint_ts, schedule),       ///< masl
                (apoint_ts, result),         ///< masl
                (constraint_, constraint)
            );
        };

        struct volume_  {
            struct constraint_ {
                struct tactical_ {
                    BOOST_HANA_DEFINE_STRUCT(tactical_,
                        (penalty_constraint, min), ///< m3
                        (penalty_constraint, max)  ///< m3
                    );
                };
                BOOST_HANA_DEFINE_STRUCT(constraint_,
                    (apoint_ts, min),     ///< m3
                    (apoint_ts, max),     ///< m3
                    (tactical_, tactical)
                );
            };
            struct slack_ {
                BOOST_HANA_DEFINE_STRUCT(slack_,
                    (apoint_ts, lower), ///< m3
                    (apoint_ts, upper)  ///< m3
                );
            };
            struct cost_ {
                struct cost_curve_ {
                    BOOST_HANA_DEFINE_STRUCT(cost_curve_,
                        (t_xy_, curve),      ///< m3 -> money/m3
                        (apoint_ts, penalty) ///< money
                    );
                };
                BOOST_HANA_DEFINE_STRUCT(cost_,
                    (cost_curve_, flood),
                    (cost_curve_, peak)
                );
            };
            BOOST_HANA_DEFINE_STRUCT(volume_,
                (apoint_ts, static_max),   ///< m3
                (apoint_ts, schedule),     ///< m3
                (apoint_ts, realised),     ///< m3
                (apoint_ts, result),       ///< m3
                (apoint_ts, penalty),      ///< m3
                (constraint_, constraint),
                (slack_, slack),
                (cost_, cost)
            );
        };

        struct inflow_ {
            BOOST_HANA_DEFINE_STRUCT(inflow_,
                (apoint_ts, schedule),   ///< m3/s
                (apoint_ts, realised),   ///< m3/s
                (apoint_ts, result)      ///< m3/s
            );
        };

        struct water_value_ {
            struct result_ {
                BOOST_HANA_DEFINE_STRUCT(result_,
                (apoint_ts, local_volume),  ///< money/m3
                (apoint_ts, global_volume), ///< money/m3
                (apoint_ts, local_energy),  ///< money/joule
                (apoint_ts, end_value)      ///< money
                );
            };
            BOOST_HANA_DEFINE_STRUCT(water_value_,
                (apoint_ts, endpoint_desc), ///< money/joule
                (result_, result)
            );
        };

        struct ramping_ {
            BOOST_HANA_DEFINE_STRUCT(ramping_,
                (apoint_ts, level_down),
                (apoint_ts, level_up)
            );
        };

        BOOST_HANA_DEFINE_STRUCT(reservoir,
            (t_xy_, volume_level_mapping),
            (level_, level),
            (volume_, volume),
            (inflow_, inflow),
            (ramping_, ramping), // it's really  level constraints that applies to simulation/optimization
            (water_value_, water_value)
        );
    };

    struct power_plant {
        struct production_ {
            BOOST_HANA_DEFINE_STRUCT(production_,
                (apoint_ts, constraint_min),  ///< W
                (apoint_ts, constraint_max),  ///< W
                (apoint_ts, schedule),        ///< W
                (apoint_ts, realised),        ///< W, the sum of unit.production.realised
                (apoint_ts, merge_tolerance), ///< W, Max deviation in production plan
                (apoint_ts, ramping_up),      ///< W, Constraint of ramping up production
                (apoint_ts, ramping_down),    ///< W, Constraint of ramping down production
                (apoint_ts, result)           ///< W
            );
        };// production{ *this };
        struct discharge_ {
            BOOST_HANA_DEFINE_STRUCT(discharge_,
                (apoint_ts, constraint_min),
                (apoint_ts, constraint_max),
                (apoint_ts, schedule),
                (apoint_ts, result),
                (apoint_ts, realised),                     ///< the sum of all unit.discharge.realised
                (apoint_ts, intake_loss_from_bypass_flag), ////< x: time / y: 0 or 1
                (t_xy_, upstream_level_constraint),        ///< x: m : y: m3/s
                (t_xy_, downstream_level_constraint),      ///< x: m / y: m3/s
                (apoint_ts, ramping_up),                   ///< W, Constraint of ramping up discharge
                (apoint_ts, ramping_down)                  ///< W, Constraint of ramping down discharge

            );
        };

        BOOST_HANA_DEFINE_STRUCT(power_plant,
            (apoint_ts, outlet_level),  ///< masl, (input, or result ?)
            (apoint_ts, mip),           ///< bool, opt. with mip
            (apoint_ts, unavailability),///< bool, ..
            (production_, production),
            (discharge_, discharge)
        );
    };
    //
    // power_module.h v0
    //
    struct power_module {
        struct power_ {
            BOOST_HANA_DEFINE_STRUCT(power_,
                (apoint_ts, realised), ///< Historical fact. W, J/s, if positive then production, if negative then consumption
                (apoint_ts, schedule), ///< the current schedule. W, J/s, if positive then production, if negative then consumption
                (apoint_ts, result)    ///< the optimal/simulated/estimated result. W, J/s, if positive then production, if negative then consumption
            );
        };
        BOOST_HANA_DEFINE_STRUCT(power_module,
            (power_, power)
        );
    };
    //
    // optimization_summary.h v0
    //
    struct optimization_summary {
        struct reservoir_ {
            reservoir_() : end_value{nan},sum_ramping_penalty{nan},sum_limit_penalty{nan},end_limit_penalty{nan},hard_limit_penalty{nan}{}
            BOOST_HANA_DEFINE_STRUCT(reservoir_,
                (double, end_value),
                (double, sum_ramping_penalty),
                (double, sum_limit_penalty),
                (double, end_limit_penalty),
                (double, hard_limit_penalty)
            );
        };

        struct gate_ {
            gate_() : ramping_penalty{nan},discharge_cost{nan},discharge_constraint_penalty{nan}{}
            BOOST_HANA_DEFINE_STRUCT(gate_,
                (double, ramping_penalty),
                (double, discharge_cost),
                (double, discharge_constraint_penalty)
            );
        };

        struct waterway_ {
            waterway_() : vow_in_transit{nan},sum_discharge_fee{nan},discharge_group_penalty{nan},discharge_group_ramping_penalty{nan}{}
            BOOST_HANA_DEFINE_STRUCT(waterway_,
                (double, vow_in_transit),
                (double, sum_discharge_fee),
                (double, discharge_group_penalty),
                (double, discharge_group_ramping_penalty)
            );
        };

        struct spill_ {
            spill_() : cost{nan},physical_cost{nan},nonphysical_cost{nan},physical_volume{nan},nonphysical_volume{nan}{}
            BOOST_HANA_DEFINE_STRUCT(spill_,
                (double, cost),
                (double, physical_cost),
                (double, nonphysical_cost),
                (double, physical_volume),
                (double, nonphysical_volume)
            );
        };

        struct bypass_ {
            bypass_() : cost{nan}{}
            BOOST_HANA_DEFINE_STRUCT(bypass_,
                (double, cost)
            );
        };
        struct ramping_ {
            ramping_() : ramping_penalty{nan}{}
            BOOST_HANA_DEFINE_STRUCT(ramping_,
                (double, ramping_penalty)
            );
        };
        struct reserve_ {
            reserve_() : violation_penalty{nan},sale_buy{nan},obligation_value{nan}{}
            BOOST_HANA_DEFINE_STRUCT(reserve_,
                (double, violation_penalty),
                (double, sale_buy),
                (double, obligation_value)
            );
        };
        struct unit_ {
            unit_() : startup_cost{nan},schedule_penalty{nan}{}
            BOOST_HANA_DEFINE_STRUCT(unit_,
                (double, startup_cost),
                (double, schedule_penalty)
            );
        };
        struct plant_ {
            plant_() : production_constraint_penalty{nan},discharge_constraint_penalty{nan},schedule_penalty{nan},ramping_penalty{nan}{}
            BOOST_HANA_DEFINE_STRUCT(plant_,
                (double, production_constraint_penalty),
                (double, discharge_constraint_penalty),
                (double, schedule_penalty),
                (double, ramping_penalty)
            );
        };

        struct market_ {
            market_() : sum_sale_buy{nan},load_penalty{nan},load_value{nan}{}
            BOOST_HANA_DEFINE_STRUCT(market_,
                (double, sum_sale_buy),
                (double, load_penalty),
                (double, load_value)
            );
        };

        BOOST_HANA_DEFINE_STRUCT(optimization_summary,
            (reservoir_, reservoir),
            (waterway_, waterway),
            (gate_, gate),
            (spill_, spill),
            (bypass_, bypass),
            (ramping_, ramping),
            (reserve_, reserve),
            (unit_, unit),
            (plant_, plant),
            (market_, market),
            (double, total),
            (double, sum_penalties),
            (double, minor_penalties),
            (double, major_penalties),
            (double, grand_total)
        );
    };
    //
    // market.h v0
    //
    struct energy_market_area {
        struct ts_triplet_ {
            BOOST_HANA_DEFINE_STRUCT(ts_triplet_,
                (apoint_ts,realised), ///< SI unit, as in historical fact
                (apoint_ts,schedule), ///< the current schedule
                (apoint_ts,result)    ///< the optimal/simulated/estimated result
            );
        };
        struct offering_ {
            BOOST_HANA_DEFINE_STRUCT(offering_,
                (t_xy_, bids),    ///< x= price[money/J], y= amount [J/s] aka [W], ordered by t and then x,y x-ascending
                (ts_triplet_,usage), ///< how much has been used out of the bids
                (ts_triplet_,price)  ///< based on usage, what is the effective price paid for the usage [money/J]
            );
        };

        BOOST_HANA_DEFINE_STRUCT(energy_market_area,
            (apoint_ts, price),   ///< money/J (input, see also deman/supply)
            (apoint_ts, load),    ///< W (requirement for optimiser)
            (apoint_ts, max_buy), ///< W (constraint for optimiser)
            (apoint_ts, max_sale),///< W (constraint for optimiser)
            (apoint_ts, buy),     ///< W (result, as pr. optimiser)
            (apoint_ts, sale),    ///< W (result, as pr. optimiser)
            (apoint_ts, production), ///< W (result, as pr. optimiser)
            (apoint_ts, reserve_obligation_penalty), ///< money (result from optimiser)
            (offering_, demand), ///< the demand side of the market, willing to buy power(as seller you can sell to pricy offering first)
            (offering_, supply)  ///< the supply side of the market, wants to sell power(as buyer, you can pick the cheapest offering first)
        );
    };
    //
    // contract_portfolio.h v0
    //
    struct contract_portfolio {
        BOOST_HANA_DEFINE_STRUCT(contract_portfolio,
            (apoint_ts, quantity),  ///< Quantity (volume) of the portfolio (e.g. sum from contracts)
            (apoint_ts, fee),       ///< Fees of the portfolio (e.g. sum from contracts)
            (apoint_ts, revenue)    ///< Revenue, actual or forecast. Usually a calculated value.
        );
    };
    //
    // contract.h v0
    //
    struct contract {
        BOOST_HANA_DEFINE_STRUCT(contract,
            (apoint_ts, quantity),      ///< Quantity (volume), e.g. W,J/s (maybe energy_flow is better?).
            (apoint_ts, price),         ///< Price per quantity unit, e.g. EUR/J.
            (apoint_ts, fee),           ///< Any contract fees, currency unit.
            (apoint_ts, revenue),       ///< Revenue, actual or forecast. Usually a calculated value, e.g. cash_flow = volume*price - fee [EUR/s].
            (string, parent_id),       ///< Optional reference to parent (contract) (maybe use weakptr?).
            (string, buyer),            ///< Name of the buyer actor (later a ref to actor).
            (string, seller),           ///< Name of the seller actor (later a ref to actor).
            (apoint_ts, active),        ///< Contract status (e.g. active, inactive).
            (apoint_ts, validation)     ///< Validation status (e.g. whether the contract has been validated, and at which level).
        );
    };
    //
    // catchment.h v0
    //
    struct catchment {
        BOOST_HANA_DEFINE_STRUCT(catchment,
            (apoint_ts, inflow_m3s)
        );
    };


}
