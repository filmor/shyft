#include <shyft/energy_market/stm/reservoir_aggregate.h>
#include <shyft/energy_market/em_utils.h>
namespace shyft::energy_market::stm {
    using std::runtime_error;
	
	reservoir_aggregate::reservoir_aggregate(){
        mk_url_fx(this);
    }

    reservoir_aggregate::reservoir_aggregate(int id, const string& name, const string& json, const stm_hps_ &hps)
        : id_base{id,name,json,{},{}},hps(hps) {
        mk_url_fx(this);
    }
	
    reservoir_aggregate::~reservoir_aggregate(){
        for(auto&a:reservoirs)
            if(a) a->rsv_aggregate.reset();
    }
	
	reservoir_aggregate_ reservoir_aggregate::shared_from_this() const {
        auto hps=hps_();
        return hps?shared_from_me(this,hps->reservoir_aggregates):nullptr;
    }
	
	void reservoir_aggregate::generate_url(std::back_insert_iterator<string>& rbi, int levels, int template_levels) const {
        if (levels) {
            auto tmp = dynamic_pointer_cast<stm_hps>(hps_());
            if (tmp) tmp->generate_url(rbi, levels-1, template_levels ? template_levels - 1 : template_levels);
        }

        if (!template_levels) {
            constexpr std::string_view a = "/A{o_id}";
            std::copy(std::begin(a), std::end(a), rbi);
        } else {
            auto idstr="/A"+std::to_string(id);
            std::copy(std::begin(idstr),std::end(idstr),rbi);
        }
    }
	
    void reservoir_aggregate::add_reservoir(const reservoir_aggregate_& ra,const reservoir_ &r) {
        auto rr=ra->shared_from_this();
        if(r && rr && rr->hps_() == r->hps_()) {
            auto f = find(begin(rr->reservoirs), end(rr->reservoirs), r);
            if(f!=end(rr->reservoirs))
                throw runtime_error("reservoir  '" + r->name + "'already added to reservoir aggregate '"+rr->name);
            if(r->rsv_aggregate_())
                throw runtime_error("reservoir  '" + r->name + "'already added to anoter reservoir aggregate '"+r->rsv_aggregate_()->name);
            rr->reservoirs.push_back(r);
            r->rsv_aggregate=rr;            
        } else {
            throw runtime_error("reservoir aggregate '"+string(ra ?ra->name:string("null"))
                   +"' and reservoir '"+string(r?r->name:string("null")) 
                   + "' must be non-null objects, and both belong to the same existing hydro-power-system :"
                + string(ra&&ra->hps_()?ra->hps_()->name:string("null")));
        }
    }
	
    void reservoir_aggregate::remove_reservoir(const reservoir_& r) {
        auto f= find_if(begin(reservoirs),end(reservoirs),[&r](const auto&p)->bool {return r==p;});
        if(f !=end(reservoirs)) {
            (*f)->rsv_aggregate.reset();
            reservoirs.erase(f);
        }
    }

    bool reservoir_aggregate::operator==(const reservoir_aggregate& other) const {
        if(this==&other) return true;//equal by addr.
        auto equal_attributes= hana::fold(mp::leaf_accessors(hana::type_c<reservoir_aggregate>), id_base::operator==(other),
            [this, &other](bool s, auto&& a) {
                return s ? stm::equal_attribute(mp::leaf_access(*this, a), mp::leaf_access(other, a)) : false;
            }
        );
        return equal_attributes && equal_vector_ptr_content<reservoir>(reservoirs,other.reservoirs);// since r-aggretate does not own reservoirs, we could stop at checking shallow identity match
    }



}
