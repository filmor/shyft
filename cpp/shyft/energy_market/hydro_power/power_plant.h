/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <vector>
#include <memory>

#include <shyft/core/core_serialization.h>

#include <shyft/energy_market/hydro_power/hydro_component.h>

namespace shyft::energy_market::hydro_power {

    using std::string;
    using std::vector;
    using std::shared_ptr;
    using std::make_shared;
    using std::weak_ptr;

    struct power_plant;//fwd
    using power_plant_=shared_ptr<power_plant>;
    using power_plant__=weak_ptr<power_plant>;

    /** @brief A hydro power unit, turbine and generator assembly
    *
    * 
    * Can be pure production unit, or a pure pump, or both, depending on the construction.
    * 
    * The power_station is the 'building' that keeps the units (generator-turbine).
    * 
    * For the LTM models, power-stations are always simplified to one 'super-unit' that
    * resembles the properties equal to the sum of physical units.
    *
    * For the STM models, units are accurately described, and power-station is
    * just a natural group of those aggregates.
    * 
    * 
    * Hydrology: It takes one input from a water_route and have one output to water_route&
    */
    struct unit :hydro_component {

        unit()=default;
        unit(int id,const string& name, const string& json="", hydro_power_system_ const&hps=nullptr):hydro_component{id,name,json,hps}{};

        power_plant__ pwr_station;///< should be weak ref. to the ps, but exposure through boost python gives a zero -ref.
        // ref SO: https://stackoverflow.com/questions/8233252/boostpython-and-weak-ptr-stuff-disappearing
        // for similar problem.
        power_plant_ pwr_station_() const {return pwr_station.lock();}//.lock();};
        unit_ shared_from_this() const; // via hps
        static unit_ const& input_from(unit_ const&me, waterway_ const& r);
        static unit_ const& output_to(unit_ const&me, waterway_ const& r);

        bool is_pump() const; ///< returns true if the unit is a pump, otherwise, false
        waterway_ downstream() const;
        waterway_ upstream() const;

        bool operator==(const unit&o) const;
        bool operator!=(const unit&o) const { return !operator==(o); }
        //bool equal_structure(aggregate const&o) const;

        x_serialize_decl();
    };

    /**@brief A hydro power station is a collection of units
    *
    * The station concept in this context is a group of units, that
    * one would like to consider as one unit, where there can be associated
    * requirement/properties that are applicable to all, or to sum-aggregate constraint.
    * 
    * Currently we place it as a separate well defined entity within the hydro power system.
    * 
    * For the STM as such, - there might be other groups of units, where there is similar
    * needs to attach constraints, or properties.
    * 
    * The navigation from station to it's units is by reference.
    * The reverse navigation, could be done using a weak-reference to the station.
    * 
    */
    struct power_plant:id_base {
        power_plant() = default;
        power_plant(int id, const string& name, const string& json="", hydro_power_system_ const& hps=nullptr)
            : id_base{id,name,json,{},{}},hps(hps) {}
        virtual ~power_plant();
        vector<unit_> units;///< the units allocated to this object
        
        static void add_unit(const power_plant_& ps,const unit_ &a);
        void remove_unit(const unit_& a) ;
        bool equal_structure(const power_plant&o) const;
        bool operator==(const power_plant&o) const;
        bool operator!=(const power_plant&o) const { return !operator==(o); }
        //python access hydro_power_system 
        hydro_power_system_ hps_() const { return hps.lock(); }
        hydro_power_system__ hps; ///< reference up to the 'owning' hydro-power system.
        power_plant_ shared_from_this()  const; ///< shared from this using hps.power_plants shared pointers.
        x_serialize_decl();
    };


} 

//x_serialize_export_key(shyft::energy_market::hydro_power::unit);
BOOST_CLASS_EXPORT_KEY2(shyft::energy_market::hydro_power::unit, BOOST_PP_STRINGIZE(shyft::energy_market::hydro_power::aggregate));
//x_serialize_export_key(shyft::energy_market::hydro_power::power_station);
BOOST_CLASS_EXPORT_KEY2(shyft::energy_market::hydro_power::power_plant,BOOST_PP_STRINGIZE(shyft::energy_market::hydro_power::power_station));
BOOST_CLASS_VERSION(shyft::energy_market::hydro_power::unit, 2);// proper handling of base class serial
