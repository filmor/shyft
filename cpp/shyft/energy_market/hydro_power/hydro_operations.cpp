#include <algorithm>
#include <unordered_set>
#include <set>
#include <utility>

#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/power_plant.h>
#include <shyft/energy_market/hydro_power/waterway.h>
#include <shyft/energy_market/hydro_power/hydro_operations.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>


namespace shyft::energy_market::hydro_power {

    using std::to_string;
    using std::set;
    using std::pair;
    using std::vector;
    using std::unordered_set;
    using std::make_shared;
    
    
    void hydro_operations::path_to_ocean(vector<hydro_component_> &path, connection_role role){
        auto len = path.size();
        auto component = path[len - 1];
        auto res = std::dynamic_pointer_cast<reservoir>(component);
        auto wr = std::dynamic_pointer_cast<waterway>(component);
        auto agg = std::dynamic_pointer_cast<unit>(component);
        auto add_path=[&path,role] (hydro_connection const&x)->void{
            if(x.target==nullptr)
                return;
            for(auto const&p:path) 
                if (p==x.target) 
                    return;
            path.push_back(x.target);
            path_to_ocean(path,role);
        };
        

        if (component->name.compare("havet") == 0)
            return;
        if (res) {
            for (auto &d : component->downstreams) {
                if (d.role == role) {
                    if (d.target->downstreams.size() == 0) 
                        return add_path(d);
                    auto _agg = std::dynamic_pointer_cast<unit>(d.target->downstreams[0].target);
                    if (_agg && _agg->is_pump()) 
                        continue;
                    return add_path(d);
                }
            }
            for (auto &u : component->upstreams) {
                auto _wr = std::dynamic_pointer_cast<waterway>(u.target);
                if (_wr && _wr->upstream_role() == role) {
                    if(u.target->upstreams.size()) {
                        auto _agg = std::dynamic_pointer_cast<unit>(u.target->upstreams[0].target);
                        if (_agg && _agg->is_pump())
                            return add_path(u);
                    }
                }
            }
        }
        if (wr) {
            if (component->downstreams.size() == 0)
                return;
            
            if(wr->upstreams.size()) {
                auto agg_u = std::dynamic_pointer_cast<unit>(wr->upstreams[0].target);
                if (agg_u && agg_u->is_pump())
                    return add_path(component->upstreams[0]);
            }
            auto agg_d = std::dynamic_pointer_cast<unit>(wr->downstreams[0].target);
            if (agg_d && agg_d->is_pump()) {
                if(component->upstreams.size())
                    return add_path(component->upstreams[0]);
                return;
            }
            return add_path(component->downstreams[0]);
        }
        if (agg) {
            if (agg->is_pump()) {
                if(component->upstreams.size())
                    return add_path(component->upstreams[0]);
                return;
            }
            if(component->downstreams.size())
                return add_path(component->downstreams[0]);
            return;
        }
    }

    void hydro_operations::path_to_ocean(vector<hydro_component_> &path) { 
        path_to_ocean(path, connection_role::main);
    }

    vector<hydro_component_> hydro_operations::get_path_to_ocean(const hydro_component_ &component, connection_role role) {
        vector<hydro_component_> path{ component };
        path_to_ocean(path, role);
        return path;
    }

    vector<hydro_component_> hydro_operations::get_path_to_ocean(const hydro_component_ &component) { 
        vector<hydro_component_> path{ component };
        path_to_ocean(path, connection_role::main);
        return path;
    }

    vector<hydro_component_> hydro_operations::get_path_between(const hydro_component_ &comp_1, const hydro_component_ &comp_2, connection_role role) {
        auto path_1 = get_path_to_ocean(comp_1, role);
        auto path_2 = get_path_to_ocean(comp_2, role);
        vector<hydro_component_> path;
        auto it_1 = std::find(std::begin(path_1), std::end(path_1), comp_2);
        if (it_1 != std::end(path_1)) {
            for (auto it = path_1.begin(); it != it_1; ++it) {
                path.push_back(*it);
            }
            path.push_back(*it_1);
        } else {
            auto it_2 = std::find(std::begin(path_2), std::end(path_2), comp_1);
            if (it_2 != std::end(path_2)) {
                for (auto it = path_2.begin(); it != it_2; ++it) {
                    path.push_back(*it);
                }
                path.push_back(*it_2);
            }
        }
        return path;
    }

    vector<hydro_component_> hydro_operations::get_path_between(const hydro_component_ &comp_1, const hydro_component_ &comp_2) {
        return get_path_between(comp_1, comp_2, connection_role::main);
    }
    
    bool hydro_operations::is_connected(const hydro_component_ &comp_1, const hydro_component_ &comp_2, connection_role role) {
        return get_path_between(comp_1, comp_2, role).size() != 0;
    }

    bool hydro_operations::is_connected(const hydro_component_ &comp_1, const hydro_component_ &comp_2) { 
        return get_path_between(comp_1, comp_2, connection_role::main).size() != 0; 
    }

    bool hydro_operations::add_to_collection(const hydro_component_ &component, set<hydro_component_> &collection) {
        if (collection.find(component) != collection.end()) {
            return true;
        } else {
            collection.insert(component);
            return false;
        }
    }

    void hydro_operations::add_neighbors(const hydro_component_ &component, set<hydro_component_> &collection) {
        for (auto &connection : component->upstreams) {
            auto us_component = connection.target;

            if (!add_to_collection(us_component, collection) && us_component->name.compare("havet") != 0) {
                add_neighbors(us_component, collection);
            }
        }
        for (auto &connection : component->downstreams) {
            auto ds_component = connection.target;
            if (!add_to_collection(ds_component, collection) && ds_component->name.compare("havet") != 0) {
                add_neighbors(ds_component, collection);
            }
        }
    }

    hydro_power_system_ hydro_operations::get_water_course(const hydro_component_ &component) {
        set<hydro_component_> collection;

        add_to_collection(component, collection);
        add_neighbors(component, collection);

        auto hps = make_shared<hydro_power_system>(component->hps_()->id, "");
        hydro_power_system_builder hpsb(hps);
        hps->populate(collection, hpsb);
        auto parent = (component->hps_())->mdl_area_();
        hps->set_mdl_area(parent);
        return hps;
    }

    vector<hydro_power_system_> hydro_operations::extract_water_courses(hydro_power_system_ &hps) {
        vector<hydro_power_system_> water_courses;
        unordered_set<int> res_ids;

        for (auto &component : hps->reservoirs) {
            if (component->name.compare("havet") == 0 || res_ids.find(component->id) != res_ids.end()) {
                continue;
            }
            auto water_course = get_water_course(component);
            for (auto &res : water_course->reservoirs) {
                res_ids.insert(res->id);
            }
            water_courses.push_back(water_course);
        }
        return water_courses;
    }
        
} //namespace shyft::energy_market::hydro_power
