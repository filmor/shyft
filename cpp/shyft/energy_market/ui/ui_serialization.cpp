/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/energy_market/ui/ui_core.h>
#include <shyft/core/core_archive.h>
#include <shyft/core/core_serialization.h>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/nvp.hpp>

#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wunused-parameter"
#endif
using namespace shyft::core;
using namespace boost::serialization;

namespace shyft::energy_market::ui {

    template<class Archive>
    void layout_info::serialize(Archive & ar, const unsigned int file_version) {
        ar 
        & make_nvp("id", id)
        & make_nvp("name",name)
        & make_nvp("json",json)
        ;
        
    }
}

#define xxx_arch(T) x_serialize_archive(T, boost::archive::binary_oarchive, boost::archive::binary_iarchive)

xxx_arch(shyft::energy_market::ui::layout_info);

namespace shyft::energy_market::ui {
    template<class Archive>
    void register_types(Archive& a) {
        // Empty for now, but we add classes here when we start employing polymorphism
    }
}
