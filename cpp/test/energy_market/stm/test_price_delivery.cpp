#include <doctest/doctest.h>

#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/stm/price_delivery_convert.h>

using namespace shyft::energy_market::stm;
using std::make_shared;
using std::vector;
using std::map;
using std::runtime_error;
using xy_point = shyft::energy_market::hydro_power::point;
using xy_points = shyft::energy_market::hydro_power::xy_point_curve;
using shyft::energy_market::hydro_power::xy_point_curve_;
using shyft::energy_market::stm::convert_to_price_delivery_tsv;

TEST_SUITE_BEGIN("stm");

auto pv_empty = make_shared<t_xy_::element_type>();
calendar utc;
auto t0 = utc.time(2022, 6, 11);
auto t1 = t0 + deltahours(1);
auto t_end = utc.time(2022, 8, 1);

auto pv_one_1_step = make_shared<t_xy_::element_type>(t_xy_::element_type{
  // x=W, y=money
  {t0, make_shared<xy_points>(vector<xy_point>{{100.0, 10.0}})}
});
auto pv_one_2_step = make_shared<t_xy_::element_type>(t_xy_::element_type{
  // x=W, y=money
  {t0, make_shared<xy_points>(vector<xy_point>{{100.0, 10.0}, {110.0, 5.0}})}
});
auto pv_two_2_step = make_shared<t_xy_::element_type>(t_xy_::element_type{
  // x=W, y=money
  {t0,               make_shared<xy_points>(vector<xy_point>{{100.0, 10.0}, {110.0, 5.0}})},
  {t1, make_shared<xy_points>(vector<xy_point>{{102.0, 12.0}, {112.0, 5.2}, {115.0, 3.2}})}
});

TEST_CASE("stm/compute_effective_price") {
  generic_dt ta{t0, deltahours(1), 3};
  apoint_ts v{
    ta, vector<double>{12.0, 19.0, 3.0},
     POINT_AVERAGE_VALUE
  };
  apoint_ts null_ts{};
  SUBCASE("null_input_gives_null_output") {
    FAST_CHECK_EQ(!effective_price(null_ts, pv_two_2_step), true);
    FAST_CHECK_EQ(!effective_price(v, nullptr), true);
    FAST_CHECK_EQ(!effective_price(v, pv_empty), true);
  }
  SUBCASE("time-mismatched-txy-gives-nan-ts") {
    auto a = v.time_shift(deltahours(-5));
    auto r = effective_price(a, pv_two_2_step);
    FAST_CHECK_EQ(a.time_axis(), r.time_axis());
    auto ys = r.values();
    auto all_nans = std::none_of(ys.begin(), ys.end(), [](double const & x) {
      return std::isfinite(x);
    });
    FAST_CHECK_EQ(true, all_nans);
  }
  SUBCASE("ok-input-gives-ok-output") {
    auto r = effective_price(v, pv_two_2_step);
    FAST_CHECK_EQ(v.time_axis(), r.time_axis());
    FAST_CHECK_EQ(r.value(0), doctest::Approx(effective_price(v.value(0), *(*pv_two_2_step)[t0])));
    FAST_CHECK_EQ(r.value(1), doctest::Approx(effective_price(v.value(1), *(*pv_two_2_step)[t1])));
    FAST_CHECK_EQ(r.value(2), doctest::Approx(effective_price(v.value(2), *(*pv_two_2_step)[t1])));
  }
}

TEST_CASE("stm/compute_t_xy_price_from_given_y") {
  xy_points p0{};
  xy_points p1{vector<xy_point>{{0.1, 10}}};
  xy_points p3{
    vector<xy_point>{{1, 1}, {2, 0.6}, {3, 1}}
  };
  bool reverse_order{false}; // compute_effective_price can use asc, or desc merit order(buyer/seller)
  SUBCASE("nan-for-empty-xy") {
    FAST_CHECK_EQ(std::isfinite(effective_price(0.0, p0)), false);
  }
  SUBCASE("nan-in-give-nan-out") {
    FAST_CHECK_EQ(std::isfinite(effective_price(shyft::nan, p1)), false);
  }
  SUBCASE("given-zero-price-is-firststep-price") {
    FAST_CHECK_EQ(effective_price(0.0, p1), doctest::Approx(p1.points[0].x));
    FAST_CHECK_EQ(effective_price(0.0, p3), doctest::Approx(p3.points[0].x));
    FAST_CHECK_EQ(effective_price(0.0, p1, reverse_order), doctest::Approx(p1.points.front().x));
    FAST_CHECK_EQ(effective_price(0.0, p3, reverse_order), doctest::Approx(p3.points.back().x));
  }
  SUBCASE("one-step-always-is-firststep-price") {
    for (double y = -1.0; y < 2 * p1.points[0].y; y += p1.points[0].y / 3.0) {
      FAST_CHECK_EQ(effective_price(0.0, p1), doctest::Approx(p1.points[0].x));
      FAST_CHECK_EQ(effective_price(0.0, p1, reverse_order), doctest::Approx(p1.points[0].x));
    }
  }
  SUBCASE("in-range-values-gives-average-consumed-price") {
    FAST_CHECK_EQ(effective_price(1.5, p3), doctest::Approx((1 * 1 + 0.5 * 2) / 1.5));
    FAST_CHECK_EQ(effective_price(1.9, p3), doctest::Approx((1 * 1 + 0.6 * 2 + 0.3 * 3.0) / 1.9));
    FAST_CHECK_EQ(effective_price(1.5, p3, reverse_order), doctest::Approx((3 * 1 + 0.5 * 2) / 1.5));
    FAST_CHECK_EQ(effective_price(1.9, p3, reverse_order), doctest::Approx((3 * 1 + 0.6 * 2 + 0.3 * 1.0) / 1.9));
  }
  SUBCASE("overshooting-gives-capped-average-price") {
    FAST_CHECK_EQ(effective_price(4.5, p3), doctest::Approx((1 * 1 + 0.6 * 2 + 3 * 1) / 2.6));
    FAST_CHECK_EQ(effective_price(4.5, p3, reverse_order), doctest::Approx((1 * 1 + 0.6 * 2 + 3 * 1) / 2.6));
  }
  SUBCASE("neg-input-gives-1step") {
    FAST_CHECK_EQ(effective_price(-0.5, p3), doctest::Approx(p3.points[0].x));
    FAST_CHECK_EQ(effective_price(-0.5, p3, reverse_order), doctest::Approx(p3.points[2].x));
  }
}

TEST_CASE("stm/transform_t_xy_to_ats_vector") {

  SUBCASE("t_xy_empty") {
    auto x = convert_to_price_delivery_tsv(nullptr, t_end);
    FAST_CHECK_EQ(x.size(), 0u);
    FAST_CHECK_EQ(convert_to_price_delivery_tsv(pv_empty, t_end).size(), 0u);
  }

  SUBCASE("t_xy_one_1step") {
    auto r = convert_to_price_delivery_tsv(pv_one_1_step, t_end);
    generic_dt ta{{t0}, t_end};
    auto x_p = ats_vector{};
    x_p.emplace_back(ta, 100.0, POINT_AVERAGE_VALUE);
    auto x_d = ats_vector{};
    x_d.emplace_back(ta, 10.0, POINT_AVERAGE_VALUE);
    vector<ats_vector> x_r{x_p, x_d};
    FAST_REQUIRE_EQ(r.size(), 2u);
    for (auto i = 0u; i < r.size(); ++i)
      FAST_CHECK_EQ(r[i], x_r[i]);
  }
  SUBCASE("t_xy_one_2step") {
    auto r = convert_to_price_delivery_tsv(pv_one_2_step, t_end);
    generic_dt ta{vector<utctime>{t0}, t_end};
    auto x_p = ats_vector{};
    auto x_d = ats_vector{};
    x_p.emplace_back(ta, 100.0, POINT_AVERAGE_VALUE);
    x_d.emplace_back(ta, 10.0, POINT_AVERAGE_VALUE);
    x_p.emplace_back(ta, 110.0, POINT_AVERAGE_VALUE);
    x_d.emplace_back(ta, 5.0, POINT_AVERAGE_VALUE);
    vector<ats_vector> x_r{x_p, x_d};
    FAST_REQUIRE_EQ(r.size(), 2u);
    FAST_CHECK_EQ(r[0], x_r[0]);
    FAST_CHECK_EQ(r[1], x_r[1]);
  }
  SUBCASE("t_xy_two_2step") {
    auto r = convert_to_price_delivery_tsv(pv_two_2_step, t_end);
    generic_dt ta{
      vector<utctime>{t0, t1},
      t_end
    };
    auto x_p = ats_vector{};
    auto x_d = ats_vector{};
    x_p.emplace_back(ta, vector<double>{100.0, 102.0}, POINT_AVERAGE_VALUE);
    x_d.emplace_back(ta, vector<double>{10.0, 12.0}, POINT_AVERAGE_VALUE);

    x_p.emplace_back(ta, vector<double>{110.0, 112.0}, POINT_AVERAGE_VALUE);
    x_d.emplace_back(ta, vector<double>{5.0, 5.2}, POINT_AVERAGE_VALUE);

    x_p.emplace_back(ta, vector<double>{0.0, 115.0}, POINT_AVERAGE_VALUE);
    x_d.emplace_back(ta, vector<double>{0.0, 3.2}, POINT_AVERAGE_VALUE);

    vector<ats_vector> x_r{x_p, x_d};
    FAST_REQUIRE_EQ(r.size(), 2u);
    FAST_CHECK_EQ(r, x_r);
  }
  SUBCASE("t_xy_two_2step_w_t_end") {
    auto r = convert_to_price_delivery_tsv(pv_two_2_step, t1);
    generic_dt ta{vector<utctime>{t0}, t1};
    auto x_p = ats_vector{};
    auto x_d = ats_vector{};
    x_p.emplace_back(ta, vector<double>{100.0}, POINT_AVERAGE_VALUE);
    x_d.emplace_back(ta, vector<double>{10.0}, POINT_AVERAGE_VALUE);

    x_p.emplace_back(ta, vector<double>{110.0}, POINT_AVERAGE_VALUE);
    x_d.emplace_back(ta, vector<double>{5.0}, POINT_AVERAGE_VALUE);

    vector<ats_vector> x_r{x_p, x_d};
    FAST_REQUIRE_EQ(r.size(), 2u);
    FAST_CHECK_EQ(r, x_r);
  }
}

TEST_SUITE_END();
