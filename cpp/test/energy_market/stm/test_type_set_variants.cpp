#include <doctest/doctest.h>

#include <shyft/mp.h>
#include <shyft/mp/type_set_variant.h>

#include <string>

namespace {
  using namespace shyft::mp;
  using std::string;

  struct Y {
    BOOST_HANA_DEFINE_STRUCT(Y, (int, a), (int, b), (string, name));
  };

  struct X {
    BOOST_HANA_DEFINE_STRUCT(X, (double, f), (double, g), (string, surname), (Y, y));
  };
}

TEST_SUITE_BEGIN("stm");

namespace mp = shyft::mp;

using namespace hana::literals;

Y y{1, 2, "aname"};
X x{
  1.5,
  2.5,
  "asurname",
  {3, 4, "aname"}
};

TEST_CASE("stm/accessor_types") {
  constexpr auto attrs = hana::accessors<Y>();
  constexpr auto attr = attrs[0_c];

  SUBCASE("root_type") {
    // Want to find the holding type of the attribute:
    constexpr auto root_type = mp::accessor_ptr_struct(attr);
    CHECK_EQ(root_type, hana::type_c<Y>);
  }

  SUBCASE("value_type") {
    constexpr auto val_type = mp::accessor_ptr_type(attr);
    CHECK_EQ(val_type, hana::type_c<int>);
  }
}

TEST_CASE("stm/get_types") {
  constexpr auto y_types = mp::get_accessor_types(hana::accessors<Y>());
  CHECK_EQ(y_types, hana::make_tuple(hana::type_c<int>, hana::type_c<int>, hana::type_c<string>));

  constexpr auto x_types = mp::get_accessor_types(hana::accessors<X>());
  CHECK_EQ(
    x_types, hana::make_tuple(hana::type_c<double>, hana::type_c<double>, hana::type_c<string>, hana::type_c<Y>));
}

TEST_CASE("stm/get_type_set") {
  constexpr auto y_typeset = mp::get_accessor_type_set(hana::accessors<Y>());
  CHECK_EQ(y_typeset, hana::make_set(hana::type_c<int>, hana::type_c<string>));

  constexpr auto x_typeset = mp::get_accessor_type_set(hana::accessors<X>());
  CHECK_EQ(x_typeset, hana::make_set(hana::type_c<double>, hana::type_c<string>, hana::type_c<Y>));
}

TEST_CASE("stm/get_leaf_accessor_types") {
  constexpr auto x_leafs = mp::leaf_accessors(hana::type_c<X>);

  SUBCASE("leaf_accessor->type") {
    CHECK_EQ(mp::leaf_accessor_type(x_leafs[0_c]), hana::type_c<double>); // X::f
    CHECK_EQ(mp::leaf_accessor_type(x_leafs[3_c]), hana::type_c<int>);    // X::y.a
  }

  SUBCASE("leaf_accessors->types") {
    CHECK_EQ(
      mp::get_leaf_accessor_types(x_leafs),
      hana::make_tuple(
        hana::type_c<double>,
        hana::type_c<double>,
        hana::type_c<string>,
        hana::type_c<int>,
        hana::type_c<int>,
        hana::type_c<string>));
  }
  SUBCASE("leaf_accessors->typeset") {
    CHECK_EQ(
      mp::get_leaf_accessor_type_set(x_leafs),
      hana::make_set(hana::type_c<double>, hana::type_c<string>, hana::type_c<int>));
  }
}

TEST_CASE("stm/types_as_variants") {
  SUBCASE("metafunction") {
    constexpr auto mf = hana::metafunction<mp::variant_type>;
    CHECK_EQ(mf(hana::type_c<int>), hana::type_c<boost::variant<int>>);
    CHECK_EQ(mf(hana::type_c<int>, hana::type_c<double>), hana::type_c<boost::variant<int, double>>);

    constexpr auto y_typeset = mp::accessor_type_set<Y>;
    constexpr auto y_variant = hana::unpack(y_typeset, mf);
    CHECK_EQ(y_variant, hana::type_c<boost::variant<int, string>>);

    constexpr auto x_typeset = mp::accessor_type_set<X>;
    constexpr auto x_variant = hana::unpack(x_typeset, mf);
    CHECK_EQ(x_variant, hana::type_c<boost::variant<double, string, Y>>);
  }

  SUBCASE("type_variants") {
    constexpr auto y_variant = mp::types_variant<Y>;
    CHECK_EQ(y_variant, hana::type_c<boost::variant<int, string>>);

    constexpr auto x_variant = mp::types_variant<X>;
    CHECK_EQ(x_variant, hana::type_c<boost::variant<double, string, Y>>);

    CHECK_EQ(y_variant, mp::leaf_types_variant<Y>);

    constexpr auto x_leaf_variant = mp::leaf_types_variant<X>;
    CHECK_EQ(x_leaf_variant, hana::type_c<boost::variant<double, string, int>>);
  }
}

TEST_SUITE_END();
