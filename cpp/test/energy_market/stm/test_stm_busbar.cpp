#include <doctest/doctest.h>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/transmission_line.h>
#include <shyft/energy_market/stm/busbar.h>
#include <shyft/energy_market/stm/power_module.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/time_series/dd/apoint_ts.h>

using namespace shyft::energy_market::stm;
using namespace shyft::time_series::dd;
using std::make_shared;

namespace {
  struct test_sys {
    stm_system_ sys;
    network_ net;
    transmission_line_ t1;
    transmission_line_ t2;
    busbar_ b;
    power_module_ pm;
    energy_market_area_ market_area;

    test_sys() {
      sys = make_shared<stm_system>(1, "sys", "{}");
      auto sys_builder = stm_builder(sys);
      net = sys_builder.create_network(2, "net", "{}");
      auto net_builder = network_builder(net);
      t1 = net_builder.create_transmission_line(3, "t1", "{}");
      t2 = net_builder.create_transmission_line(4, "t2", "{}");
      b = net_builder.create_busbar(6, "b", "{}");
      pm = sys_builder.create_power_module(7, "p", "{}");
      market_area = sys_builder.create_market_area(8, "ema", "{}");
    }
  };
}

TEST_SUITE_BEGIN("stm");

TEST_CASE("stm/busbar_construct") {
  auto sys = make_shared<stm_system>(1, "sys", "{}");
  auto net = make_shared<network>(2, "net", "{}", sys);
  auto b = make_shared<busbar>(3, "b1", "{}", net);
  FAST_CHECK_UNARY(b);
  FAST_CHECK_EQ(b->id, 3);
  FAST_CHECK_EQ(b->name, "b1");
  FAST_CHECK_EQ(b->json, "{}");
}

TEST_CASE("stm/busbar_transmission_line_association") {
  auto sys = make_shared<stm_system>(1, "sys", "{}");
  auto sys_builder = stm_builder(sys);
  auto net = sys_builder.create_network(2, "net", "{}");
  auto net_builder = network_builder(net);
  auto t1 = net_builder.create_transmission_line(3, "t1", "{}");
  auto t2 = net_builder.create_transmission_line(4, "t2", "{}");
  auto b1 = net_builder.create_busbar(5, "b1", "{}");
  auto b2 = net_builder.create_busbar(6, "b2", "{}");
  auto b3 = net_builder.create_busbar(7, "b3", "{}");

  // (b1) ---t1--- (b2) ---t2--- (b3)
  b1->add_to_start_of_transmission_line(t1);
  b2->add_to_end_of_transmission_line(t1);
  b2->add_to_start_of_transmission_line(t2);
  b3->add_to_end_of_transmission_line(t2);

  CHECK_EQ(b1->get_transmission_lines_from_busbar().size(), 1);
  CHECK_EQ(b1->get_transmission_lines_to_busbar().size(), 0);
  CHECK_EQ(b2->get_transmission_lines_from_busbar().size(), 1);
  CHECK_EQ(b2->get_transmission_lines_to_busbar().size(), 1);
  CHECK_EQ(b3->get_transmission_lines_from_busbar().size(), 0);
  CHECK_EQ(b3->get_transmission_lines_to_busbar().size(), 1);

  CHECK_EQ(t1->from_bb, b1);
  CHECK_EQ(t1->to_bb, b2);
  CHECK_EQ(t2->from_bb, b2);
  CHECK_EQ(t2->to_bb, b3);
}

TEST_CASE("stm/busbar_power_module_association") {
  auto sys = make_shared<stm_system>(1, "sys", "{}");
  auto sys_builder = stm_builder(sys);
  auto net = sys_builder.create_network(2, "net", "{}");

  auto net_builder = network_builder(net);
  auto b1 = net_builder.create_busbar(5, "b1", "{}");

  auto p1 = sys_builder.create_power_module(8, "p1", "{}");
  auto p2 = sys_builder.create_power_module(9, "p2", "{}");

  auto active = apoint_ts{};
  b1->add_power_module(p1, active);
  b1->add_power_module(p2, active);

  CHECK_EQ(b1->power_modules.size(), 2);
  CHECK_EQ(b1->power_modules.at(0)->power_module, p1);
  CHECK_EQ(b1->power_modules.at(0)->owner, b1.get());
  CHECK_EQ(b1->power_modules.at(0)->active, active);
  b1->remove_power_module(p1);
  CHECK_EQ(b1->power_modules.size(), 1);
  CHECK_EQ(b1->power_modules.at(0)->power_module, p2);
}

TEST_CASE("stm/busbar_unit_association") {
  auto sys = make_shared<stm_system>(1, "sys", "{}");
  auto sys_builder = stm_builder(sys);
  auto net = sys_builder.create_network(2, "net", "{}");

  auto net_builder = network_builder(net);
  auto b1 = net_builder.create_busbar(5, "b1", "{}");

  auto u1 = make_shared<unit>();
  b1->add_unit(u1, apoint_ts{});

  CHECK_EQ(b1->units.size(), 1);
  CHECK_EQ(b1->units.at(0)->unit, u1);
  CHECK_EQ(b1->units.at(0)->owner, b1.get());
  CHECK_EQ(b1->units.at(0)->active, apoint_ts{});
  b1->remove_unit(u1);
  CHECK_EQ(b1->units.size(), 0);
}

TEST_CASE("stm/busbar_market_area_association") {
  auto sys = make_shared<stm_system>(1, "sys", "{}");
  auto sys_builder = stm_builder(sys);
  auto net = sys_builder.create_network(2, "net", "{}");
  auto net_builder = network_builder(net);
  auto b1 = net_builder.create_busbar(5, "b1", "{}");
  auto mkt = sys_builder.create_market_area(8, "ema", "{}");

  // Busbar association
  b1->add_to_market_area(mkt);
  auto mkt_list = b1->get_market_areas();
  CHECK_EQ(mkt_list.at(0), mkt);
  CHECK_EQ(mkt->busbars[0], b1);

  // Adding/removing units on busbar, will update the busbar_derived_unit_group
  auto ug = mkt->create_busbar_derived_unit_group(9, "NO1", "{}");
  CHECK_EQ(mkt->get_busbar_derived_unit_group(), ug);
  CHECK_EQ(mkt->get_unit_group(), ug);
  CHECK_EQ(ug->members.size(), 0);
  auto u1 = make_shared<unit>();
  b1->add_unit(u1, apoint_ts{});
  CHECK_EQ(ug->members.size(), 1);
  CHECK_EQ(ug->members[0]->unit, u1);
  b1->remove_unit(u1);
  CHECK_EQ(ug->members.size(), 0);
}

TEST_CASE("stm/busbar_equality") {
  test_sys sys1;
  test_sys sys2;
  CHECK_EQ(*sys1.b, *sys1.b); // Equal to self
  CHECK_EQ(*sys1.b, *sys2.b); // Equal

  SUBCASE("Transmission lines from/to busbar is equal") {
    sys1.b->add_to_start_of_transmission_line(sys1.t1);
    sys1.b->add_to_end_of_transmission_line(sys1.t2);
    sys2.b->add_to_start_of_transmission_line(sys2.t1);
    sys2.b->add_to_end_of_transmission_line(sys2.t2);
    CHECK_EQ(*sys1.b, *sys2.b);
    CHECK_EQ(*sys1.t1, *sys2.t1);   // transmission lines equal
    CHECK_EQ(*sys1.t2, *sys2.t2);   // transmission lines equal
    CHECK_EQ(*sys1.net, *sys2.net); // networks equal
  }
  SUBCASE("Transmission lines from busbar is not equal") {
    sys1.b->add_to_start_of_transmission_line(sys1.t2);
    CHECK_NE(*sys1.b, *sys2.b);
  }
  SUBCASE("Transmission lines to busbar is not equal") {
    sys1.b->add_to_end_of_transmission_line(sys1.t2);
    CHECK_NE(*sys1.b, *sys2.b);
  }
  SUBCASE("Power modules associations equality by adding and removing") {
    sys1.b->add_power_module(sys1.pm, apoint_ts{});
    CHECK_NE(*sys1.b, *sys2.b);
    sys2.b->add_power_module(sys2.pm, apoint_ts{});
    CHECK_EQ(*sys1.b, *sys2.b);

    sys1.b->remove_power_module(sys1.pm);
    CHECK_NE(*sys1.b, *sys2.b);
    sys2.b->remove_power_module(sys2.pm);
    CHECK_EQ(*sys1.b, *sys2.b);
  }
  SUBCASE("Power Module associations is not equal for different timeseries") {
    calendar utc;
    generic_dt ta{utc.time(2022, 6, 11), deltahours(1), 3};
    apoint_ts null_ts{};
    apoint_ts ts_with_values{
      ta, vector<double>{12.0, 19.0, 3.0},
       POINT_AVERAGE_VALUE
    };
    sys1.b->add_power_module(sys1.pm, null_ts);
    sys2.b->add_power_module(sys1.pm, ts_with_values);
    CHECK_NE(*sys1.b, *sys2.b);
  }
  SUBCASE("Unit associations equality by adding and removing") {
    auto u1 = make_shared<unit>();
    sys1.b->add_unit(u1, apoint_ts{});
    CHECK_NE(*sys1.b, *sys2.b);
    sys2.b->add_unit(u1, apoint_ts{});
    CHECK_EQ(*sys1.b, *sys2.b);
    sys1.b->remove_unit(u1);
    CHECK_NE(*sys1.b, *sys2.b);
    sys2.b->remove_unit(u1);
  }
  SUBCASE("Unit associations is not equal for different timeseries") {
    auto u1 = make_shared<unit>();
    calendar utc;
    generic_dt ta{utc.time(2022, 6, 11), deltahours(1), 3};
    apoint_ts null_ts{};
    apoint_ts ts_with_values{
      ta, vector<double>{12.0, 19.0, 3.0},
       POINT_AVERAGE_VALUE
    };
    sys1.b->add_unit(u1, null_ts);
    sys2.b->add_unit(u1, ts_with_values);
    CHECK_NE(*sys1.b, *sys2.b);
  }
  SUBCASE("Market area associations is equal") {
    sys1.b->add_to_market_area(sys1.market_area);
    sys2.b->add_to_market_area(sys2.market_area);
    CHECK_EQ(*sys1.b, *sys2.b);
  }
  SUBCASE("Market area associations is not equal") {
    sys1.b->add_to_market_area(sys1.market_area);
    CHECK_NE(*sys1.b, *sys2.b);
  }
  SUBCASE("id_base is not equal") {
    sys1.b->id = 99;
    CHECK_NE(*sys1.b, *sys2.b);
  }
}

TEST_CASE("stm/busbar_unit_members_url") {
  std::string s;
  auto rbi = std::back_inserter(s);

  test_sys sys;
  auto u1 = make_shared<unit>();
  sys.b->add_unit(u1, apoint_ts{});
  auto m = sys.b->units.at(0);
  m->generate_url(rbi);
  FAST_CHECK_EQ(s, "/n2/b6/M0");
  s.clear();
  m->generate_url(rbi, -1, 0);
  FAST_CHECK_EQ(
    s, "/n{parent_id}/b{o_id}/M{member_id}"); // Todo: Should it be something else than 'o_id' for the different levels?
  s.clear();
  m->generate_url(rbi, -1, 1);
  FAST_CHECK_EQ(s, "/n{parent_id}/b{o_id}/M0");
  s.clear();
  m->generate_url(rbi, -1, 2);
  FAST_CHECK_EQ(s, "/n{parent_id}/b6/M0");
  s.clear();
  m->generate_url(rbi, -1, 3);
  FAST_CHECK_EQ(s, "/n2/b6/M0");
  s.clear();
}

TEST_CASE("stm/cannot_add_same_unit") {
  test_sys sys;
  auto u1 = make_shared<unit>();
  sys.b->add_unit(u1, apoint_ts{});
  CHECK_THROWS(sys.b->add_unit(u1, apoint_ts{}));
}

TEST_CASE("stm/cannot_add_same_power_module") {
  test_sys sys;
  sys.b->add_power_module(sys.pm, apoint_ts{});
  CHECK_THROWS(sys.b->add_power_module(sys.pm, apoint_ts{}));
}

TEST_CASE("stm/cannot_add_to_non_network_associated_busbar") {
  test_sys sys;
  auto b = make_shared<busbar>(111, "id", "{}", nullptr);
  CHECK_THROWS(b->add_to_end_of_transmission_line(sys.t1));
  CHECK_THROWS(b->add_to_start_of_transmission_line(sys.t1));
  CHECK_THROWS(b->add_to_market_area(sys.market_area));
}

TEST_CASE("stm/busbar_power_module_members_url") {
  std::string s;
  auto rbi = std::back_inserter(s);

  test_sys sys;
  sys.b->add_power_module(sys.pm, apoint_ts{});
  auto m = sys.b->power_modules.at(0);
  m->generate_url(rbi);
  FAST_CHECK_EQ(s, "/n2/b6/P7");
  s.clear();
  m->generate_url(rbi, -1, 0);
  FAST_CHECK_EQ(
    s, "/n{parent_id}/b{o_id}/P{member_id}"); // Todo: Should it be something else than 'o_id' for the different levels?
  s.clear();
  m->generate_url(rbi, -1, 1);
  FAST_CHECK_EQ(s, "/n{parent_id}/b{o_id}/P7");
  s.clear();
  m->generate_url(rbi, -1, 2);
  FAST_CHECK_EQ(s, "/n{parent_id}/b6/P7");
  s.clear();
  m->generate_url(rbi, -1, 3);
  FAST_CHECK_EQ(s, "/n2/b6/P7");
  s.clear();
}

TEST_SUITE_END();
