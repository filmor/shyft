#include <string>
#include <memory>

#include <doctest/doctest.h>

#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/srv/dstm/server.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/aref_ts.h>
#include <shyft/time_series/dd/ipoint_ts.h>
#include <shyft/time_series/time_axis.h>

#include "build_test_system.h"

namespace shyft::energy_market::stm {

  using shyft::core::from_seconds;
  using shyft::core::deltahours;

  TEST_SUITE_BEGIN("stm");

  TEST_CASE("stm/context_aref_ts_id_replace") {
    time_series::dd::apoint_ts ats("dstm://Mold_model_key/H1/R1/A1");
    time_series::dd::aref_ts* rts = time_series::dd::ts_as_mutable<time_series::dd::aref_ts>(ats.ts);
    CHECK_EQ(rts != nullptr, true);
    CHECK_EQ(rts->id, "dstm://Mold_model_key/H1/R1/A1");

    auto rpos = rts->id.find("/", 8);
    rts->id = rts->id.replace(8, rpos - 8, "new_mkey");
    CHECK_EQ(rts->id, "dstm://Mnew_mkey/H1/R1/A1");

    // Check that the id was changed for ats as well:
    auto arts = time_series::dd::ts_as<time_series::dd::aref_ts>(ats.ts);
    CHECK_EQ(arts->id, "dstm://Mnew_mkey/H1/R1/A1");
  }

  TEST_CASE("stm/context_replace_model_key_function") {
    // Main case:
    time_series::dd::apoint_ts ts1("dstm://Momk/H2/R3/A14");
    CHECK_EQ(replace_model_key_in_id(ts1, "new_mkey"), true);
    auto rts = time_series::dd::ts_as<time_series::dd::aref_ts>(ts1.ts);
    CHECK_EQ(rts->id, "dstm://Mnew_mkey/H2/R3/A14");

    // Non-dstm ID
    time_series::dd::apoint_ts ts2("shyft://test/not_a_dstm_series");
    CHECK_EQ(replace_model_key_in_id(ts2, "new_mkey"), false);

    // Non-ref ts:
    time_series::dd::apoint_ts ts3(time_axis::fixed_dt(from_seconds(0), deltahours(1), 2), 1.0);
    CHECK_EQ(replace_model_key_in_id(ts3, "new_mkey"), false);
  }

  TEST_CASE("stm/context_rebind_expression") {
    // Case 1: All nodes needs rebind:
    time_series::dd::apoint_ts ts1("dstm://Momk/H1/R1/A14");
    time_series::dd::apoint_ts ts2("dstm://Momk/H1/R2/A14");
    auto ts3 = ts1 + ts2;
    CHECK_EQ(rebind_ts(ts3, "new_mkey"), true);
    CHECK_EQ(ts1.id(), "dstm://Mnew_mkey/H1/R1/A14");
    CHECK_EQ(ts2.id(), "dstm://Mnew_mkey/H1/R2/A14");

    // Case 2: Not every node needs rebind:
    time_series::dd::apoint_ts ts4("dstm://Momk/H1/R1/A3");
    time_series::dd::apoint_ts ts5("shyft://test/no_rebind");
    auto ts6 = ts4 * ts5;
    CHECK_EQ(rebind_ts(ts6, "new_mkey"), true);
    CHECK_EQ(ts4.id(), "dstm://Mnew_mkey/H1/R1/A3");
    CHECK_EQ(ts5.id(), "shyft://test/no_rebind");

    // Case 3: None of the nodes needs rebind:
    time_series::dd::apoint_ts ts7("shyft://test/no_rebind2");
    auto ts8 = ts7 - ts5;
    CHECK_EQ(rebind_ts(ts8, "new_mkey"), false);

    // Case 4: A single ts:
    time_series::dd::apoint_ts ts9("dstm://Momk/H1");
    CHECK_EQ(rebind_ts(ts9, "new_mkey"), true);
    CHECK_EQ(ts9.id(), "dstm://Mnew_mkey/H1");

    // Case 5: A single ts without rebind:
    time_series::dd::apoint_ts ts10("shyft://test/no_rebind3");
    CHECK_EQ(rebind_ts(ts10, "new_mkey"), false);
  }

  TEST_CASE("stm/context_rebind_stm_system") {
    auto mdl = test::create_simple_system(1, "test-mdl");
    // Let's add a market to it
    auto mkt = std::make_shared<energy_market_area>(1, "test-market", "", mdl);
    // In ids:
    mkt->price = time_series::dd::apoint_ts(time_axis::fixed_dt(from_seconds(0), deltahours(1), 2), 1.0);
    mkt->load = time_series::dd::apoint_ts(time_axis::fixed_dt(from_seconds(0), deltahours(1), 48), 2.0);
    mkt->tsm["price-alternative-1"] = time_series::dd::apoint_ts(
      time_axis::fixed_dt(from_seconds(0), deltahours(1), 48), 1.1);
    mkt->tsm["price-alternative-2"] = time_series::dd::apoint_ts(
      time_axis::fixed_dt(from_seconds(0), deltahours(1), 48), 1.2);
    // In rds:
    mkt->buy = time_series::dd::apoint_ts(time_axis::fixed_dt(from_seconds(0), deltahours(1), 72), 3.0);

    mdl->market.push_back(mkt);

    // Case 1: Nothing to rebind
    CHECK_EQ(false, rebind_ts(*mdl, "new_mkey"));

    // Case 2: Rebind for market:
    mkt->sale = time_series::dd::apoint_ts("dstm://Momk/M1/A5");
    mkt->max_buy = time_series::dd::apoint_ts("dstm://Momk/M1/A2");
    mkt->tsm["price-alternative-2"] = time_series::dd::apoint_ts("dstm://Momk/M1/A12");
    CHECK_EQ(true, rebind_ts(*mdl, "new_mkey"));
    CHECK_EQ(mkt->sale.id(), "dstm://Mnew_mkey/M1/A5");
    CHECK_EQ(mkt->max_buy.id(), "dstm://Mnew_mkey/M1/A2");
    CHECK_EQ(mkt->tsm["price-alternative-2"].id(), "dstm://Mnew_mkey/M1/A12");
    mkt->sale = time_series::dd::apoint_ts();
    mkt->max_buy = time_series::dd::apoint_ts();
    mkt->tsm["price-alternative-2"] = time_series::dd::apoint_ts();
    CHECK_EQ(false, rebind_ts(*mdl, "new_mkey"));

    // Case 3: Rebind for hps:
    auto hps = mdl->hps[0];
    auto rsv = std::dynamic_pointer_cast<reservoir>(hps->reservoirs[0]);
    rsv->level.regulation_max = time_series::dd::apoint_ts("dstm://Momk/H1/R1/A1");
    CHECK_EQ(true, rebind_ts(*mdl, "new_mkey"));
    CHECK_EQ(rsv->level.regulation_max.id(), "dstm://Mnew_mkey/H1/R1/A1");
  }

  TEST_CASE("stm/context_copy") {

    auto mdl = test::create_simple_system(1, "test-mdl");
    auto mkt = mdl->market[0];

    // In ids:
    mkt->price = time_series::dd::apoint_ts(time_axis::fixed_dt(from_seconds(0), deltahours(1), 2), 1.0);
    mkt->load = time_series::dd::apoint_ts(time_axis::fixed_dt(from_seconds(0), deltahours(1), 48), 2.0);
    mkt->max_buy = time_series::dd::apoint_ts("dstm://Mold_mdl/M1/A2");
    // In rds:
    mkt->buy = time_series::dd::apoint_ts(time_axis::fixed_dt(from_seconds(0), deltahours(1), 72), 3.0);
    mkt->sale = time_series::dd::apoint_ts("dstm://Mold_mdl/M1/A5");

    auto hps = mdl->hps[0];
    auto rsv = std::dynamic_pointer_cast<reservoir>(hps->reservoirs[0]);
    rsv->level.regulation_max = time_series::dd::apoint_ts("dstm://Mold_mdl/H1/R1/A1");

    boost::asio::thread_pool execution_context;

    auto models = make_shared_models(
      execution_context.executor(),
      {
        {"old_mdl", mdl}
    });

    CHECK_EQ(models.container.size(), 1);
    CHECK_NOTHROW(models.copy("old_mdl", "new_mdl").get());
    CHECK_EQ(models.container.size(), 2);

    auto nsmdl = models.find("new_mdl").get();
    REQUIRE(nsmdl);
    auto nmdl = nsmdl->model;
    REQUIRE(nmdl);
    auto info = nsmdl->info;
    REQQUIRE(info);
    hps = nmdl->hps[0];
    rsv = std::dynamic_pointer_cast<reservoir>(hps->reservoirs[0]);
    CHECK_EQ(rsv->level.regulation_max.id(), "dstm://Mnew_mdl/H1/R1/A1");
    mkt = nmdl->market[0];
    CHECK_EQ(mkt->max_buy.id(), "dstm://Mnew_mdl/M1/A2");
    CHECK_EQ(mkt->sale.id(), "dstm://Mnew_mdl/M1/A5");
  }

  TEST_CASE("stm/context_remove") {

    auto mdl = test::create_simple_system(1, "test-mdl");
    auto mkt = mdl->market[0];

    // In ids:
    mkt->price = time_series::dd::apoint_ts(time_axis::fixed_dt(from_seconds(0), deltahours(1), 2), 1.0);
    mkt->load = time_series::dd::apoint_ts(time_axis::fixed_dt(from_seconds(0), deltahours(1), 48), 2.0);
    mkt->max_buy = time_series::dd::apoint_ts("dstm://Mold_mdl/M1/A2");
    // In rds:
    mkt->buy = time_series::dd::apoint_ts(time_axis::fixed_dt(from_seconds(0), deltahours(1), 72), 3.0);
    mkt->sale = time_series::dd::apoint_ts("dstm://Mold_mdl/M1/A5");

    auto hps = mdl->hps[0];
    auto rsv = std::dynamic_pointer_cast<reservoir>(hps->reservoirs[0]);
    rsv->level.regulation_max = time_series::dd::apoint_ts("dstm://Mold_mdl/H1/R1/A1");

    boost::asio::thread_pool execution_context;

    auto models = make_shared_models(
      execution_context.executor(),
      {
        {"old_mdl", mdl}
    });

    CHECK(models.container.size() == 1);

    auto nsmdl = models.find("old_mdl").get();
    REQUIRE(nsmdl);

    REQUIRE(models.remove("old_mld"));
    CHECK(models.container.size() == 1);
    CHECK(models.find("old_mdl").get() == nullptr);
  }

  TEST_SUITE_END;

}
