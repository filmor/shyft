#include <doctest/doctest.h>

#include <string>

#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <test/energy_market/stm/utilities.h>

#include <shyft/energy_market/a_wrap.h>

using namespace shyft::energy_market::stm;
using builder = shyft::energy_market::stm::stm_hps_builder;
using shyft::time_series::dd::apoint_ts;
using shyft::core::calendar, shyft::core::utctime, shyft::core::deltahours;
using time_axis = shyft::time_axis::generic_dt;
using shyft::time_series::ts_point_fx;
using std::make_shared;

namespace {
  constexpr ts_point_fx stair_case = ts_point_fx::POINT_AVERAGE_VALUE;
  const calendar utc;
  const utctime t0{0};
  const utctime dt{deltahours(1)};
  constexpr double mega = 1e6;
}

TEST_SUITE_BEGIN("stm");

TEST_CASE("stm/unit_group_construct") {
  auto sys = make_shared<stm_system>(1, "sys", "{}");
  auto ug1 = sys->add_unit_group(1, "ug1", "{}");
  FAST_CHECK_UNARY(ug1);
  FAST_CHECK_EQ(ug1->id, 1);
  FAST_CHECK_EQ(ug1->name, "ug1");
  FAST_CHECK_EQ(ug1->json, "{}");
  FAST_CHECK_EQ(ug1->members.size(), 0u);
  auto all_urls = ug1->all_urls("dstm:/M123");
  FAST_REQUIRE_EQ(all_urls.size(), 10);
  std::vector<string> expected_urls = {
    "dstm:/M123/u1.group_type", // not a ts, but the notify will be ignored
    "dstm:/M123/u1.obligation.schedule",
    "dstm:/M123/u1.obligation.cost",
    "dstm:/M123/u1.obligation.result",
    "dstm:/M123/u1.obligation.penalty",
    "dstm:/M123/u1.delivery.schedule",
    "dstm:/M123/u1.delivery.realised",
    "dstm:/M123/u1.delivery.result",
    "dstm:/M123/u1.production",
    "dstm:/M123/u1.flow"};
  for (auto i = 0u; i < all_urls.size(); ++i)
    FAST_CHECK_EQ(expected_urls[i], all_urls[i]);
}

TEST_CASE("stm/unit_group_add_remove_units") {
  // verify we can add and remove units to unit_group
  // including expressions that should be updated
  // (1) Arrange the test
  auto sys = make_shared<stm_system>(1, "sys", "{}");
  auto g_type = unit_group_type::fcr_n_up;
  auto x_type = unit_group_type::fcr_n_down;

  auto ug1 = sys->add_unit_group(1, "ug1", "{}");
  ug1->group_type = g_type;
  auto ug2 = sys->add_unit_group(2, "ug2", "{}");
  ug2->group_type = g_type;
  auto h = make_shared<stm_hps>(1, "hps");
  sys->hps.push_back(h); // make it a member
  auto b = builder(h);
  auto mk_unit = [&](int id, const char* nm, const char* json, apoint_ts prod, apoint_ts flow) {
    auto u = b.create_unit(id, nm, json);
    u->production.result = prod;
    u->discharge.result = flow;
    return u;
  };
  time_axis ta(t0, dt, 3u);
  apoint_ts flow{
    ta, {10.0, 20.0, 30.0},
     stair_case
  };
  auto prod = flow / 10.0 * mega;
  auto u1 = mk_unit(1, "u1", "", 1.0 * prod, 1.0 * flow);
  auto u2 = mk_unit(2, "u2", "", 2.0 * prod, 2.0 * flow);
  apoint_ts ug1_u2_active(time_axis(t0, dt, 3u), {1.0, 0.0, 1.0}, stair_case);
  auto ug2_u2_active = 1.0 - ug1_u2_active; // inverse of membership to ug1

  ug1->add_unit(u1, apoint_ts{});   // as pr. def, u1, is member all the time
  ug1->add_unit(u2, ug1_u2_active); // notice that u2 is not member all the time
  ug2->add_unit(u2, ug2_u2_active); // u2 is also member in ug2 for one timestep
  // (2) Assert
  // verify we got 2 units,.. an that math operations are correct
  FAST_CHECK_EQ(ug1->members.size(), 2u);
  FAST_CHECK_EQ(ug2->members.size(), 1u);
  // verify math in ug1:
  FAST_CHECK_UNARY(ug1->production == (prod + 2 * prod * ug1_u2_active));
  FAST_CHECK_UNARY(ug1->flow == (flow + 2 * flow * ug1_u2_active));
  // verify math in ug2:
  FAST_CHECK_UNARY(ug2->production == (2 * prod * ug2_u2_active));
  FAST_CHECK_UNARY(ug2->flow == (2 * flow * ug2_u2_active));

  // verify unit group membership ts for u1 and u2
  apoint_ts run_ts(
    ta, 1.0, stair_case); // required for the compute_unit_group_membership_ts algorithm, one's for all time-steps
  FAST_CHECK_EQ(
    compute_unit_group_membership_ts(sys->unit_groups, u1, run_ts, g_type), apoint_ts(ta, {1.0, 1.0, 1.0}, stair_case));
  std::map<int64_t, int64_t> id_map;
  id_map[1] = 10;
  id_map[2] = 20;
  FAST_CHECK_EQ(
    compute_unit_group_membership_ts(sys->unit_groups, u2, run_ts, g_type), apoint_ts(ta, {1.0, 2.0, 1.0}, stair_case));
  FAST_CHECK_EQ(
    compute_unit_group_membership_ts(sys->unit_groups, u2, run_ts, g_type, &id_map),
    apoint_ts(ta, {10.0, 20.0, 10.0}, stair_case));

  FAST_CHECK_UNARY(
    !compute_unit_group_membership_ts(sys->unit_groups, u1, run_ts, x_type)); // should be empty ts for x_type

  // verify unit group active member combinations ts for ug1 and ug2
  FAST_CHECK_EQ(compute_group_unit_combinations_ts(ug1, ta), apoint_ts(ta, {3.0, 1.0, 3.0}, stair_case));
  FAST_CHECK_EQ(compute_group_unit_combinations_ts(ug2, ta), apoint_ts(ta, {0.0, 1.0, 0.0}, stair_case));

  // (3) Re-arrange/act so that we modifies already existing and verified unit-groups
  // now remove u1 from ug1
  ug1->remove_unit(u1);
  // (4) Continue asserting new state
  FAST_CHECK_EQ(ug1->members.size(), 1u);
  // verify that math expressions are updated.
  FAST_CHECK_UNARY(ug1->production == (2 * prod * ug1_u2_active));
  FAST_CHECK_UNARY(ug1->flow == (2 * flow * ug1_u2_active));
  // (5) Remove more units
  ug1->remove_unit(u1); // silently accepted
  ug1->remove_unit(u2); // now we should be empty
  // (6) Finally ensure we are back to zero.
  FAST_CHECK_EQ(ug1->members.size(), 0u);
  FAST_CHECK_UNARY(!compute_unit_group_membership_ts(sys->unit_groups, u1, run_ts, g_type)); // should be empty
  FAST_CHECK_UNARY(!ug1->production);
  FAST_CHECK_UNARY(!ug1->flow);
}

TEST_CASE("stm/unit_group_get_energy_market_area") {
  auto sys = make_shared<stm_system>(1, "sys", "{}");
  auto ema = make_shared<energy_market_area>(1, "ema", "{}", sys);
  sys->market.push_back(ema);
  auto ug1 = sys->add_unit_group(1, "ug1", "{}", unit_group_type::production); // requirement
  ema->set_unit_group(ug1);
  auto ema2 = ug1->get_energy_market_area();
  FAST_CHECK_EQ(ema2, ema);
  ema->remove_unit_group();
  auto ema3 = ug1->get_energy_market_area();
  FAST_CHECK_EQ(ema3, nullptr);
  ema->set_unit_group(ug1);                              // should work, because now there are zero
  ema->set_unit_group(ug1);                              // re-assign should work(
  ema->set_unit_group(nullptr);                          // assign null should remove it. to make it natural in python
  FAST_CHECK_EQ(ug1->get_energy_market_area(), nullptr); // verify it was gone
}

TEST_CASE("stm/unit_group_generate_url") {
  auto sys = make_shared<stm_system>(123, "sys", "{}");
  auto ug = sys->add_unit_group(456, "ug", "{}");
  auto h = make_shared<stm_hps>(789, "hps");
  sys->hps.push_back(h); // make it a member
  auto b = builder(h);
  auto u = b.create_unit(111, "un", "{}");
  ug->add_unit(u, apoint_ts{});

  std::string s;
  auto rbi = std::back_inserter(s);

  ug->generate_url(rbi);
  FAST_CHECK_EQ(s, "/u456");
  s.clear();
  ug->generate_url(rbi, -1, -1);
  FAST_CHECK_EQ(s, "/u456");
  s.clear();
  ug->generate_url(rbi, 0, 0);
  FAST_CHECK_EQ(s, "/u{parent_id}");
  s.clear();
  ug->generate_url(rbi, -1, 1);
  FAST_CHECK_EQ(s, "/u456");
  s.clear();

  auto& ugm = ug->members.front();
  ugm->generate_url(rbi);
  FAST_CHECK_EQ(s, "/u456/M111");
  s.clear();
  ugm->generate_url(rbi, -1, -1);
  FAST_CHECK_EQ(s, "/u456/M111");
  s.clear();
  ugm->generate_url(rbi, -1, 0);
  FAST_CHECK_EQ(s, "/u{parent_id}/M{o_id}");
  s.clear();
  ugm->generate_url(rbi, -1, 1);
  FAST_CHECK_EQ(s, "/u{parent_id}/M111");
  s.clear();
  ugm->generate_url(rbi, -1, 2);
  FAST_CHECK_EQ(s, "/u456/M111");
  s.clear();
}

TEST_SUITE_END();
