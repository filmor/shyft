#include<memory>
#include<map>
#include<limits>

#include<shyft/energy_market/hydro_power/xy_point_curve.h>
#include<shyft/energy_market/stm/power_plant.h>
#include<shyft/energy_market/stm/market.h>
#include<shyft/energy_market/stm/reservoir.h>
#include<shyft/energy_market/stm/stm_system.h>
#include<shyft/energy_market/stm/unit.h>
#include<shyft/energy_market/stm/waterway.h>
#include<shyft/time/utctime_utilities.h>
#include<shyft/time_series/dd/ipoint_ts.h>
#include<shyft/time_series/dd/apoint_ts.h>
#include<shyft/time_series/dd/gpoint_ts.h>
#include<shyft/time_series/time_axis.h>
#include<shyft/time_series/common.h>

namespace shyft::energy_market::stm::test_models{

namespace {

    auto make_breakpoint_ts(const std::vector<shyft::core::utctime> &tall,
                            auto &&v,
                            time_series::ts_point_fx f) {
        return
            time_series::dd::apoint_ts(
                std::dynamic_pointer_cast<time_series::dd::ipoint_ts>(
                    std::make_shared<time_series::dd::gpoint_ts>(
                        time_series::dd::gpoint_ts{
                            time_axis::generic_dt{time_axis::point_dt{tall}},
                            v,
                            f
                        })));
    }
    auto make_breakpoint_ts(const std::vector<shyft::core::utctime> &tp,shyft::core::utctime t_end,
                            auto &&v,
                            time_series::ts_point_fx f) {
        return
            time_series::dd::apoint_ts(
                std::dynamic_pointer_cast<time_series::dd::ipoint_ts>(
                    std::make_shared<time_series::dd::gpoint_ts>(
                        time_series::dd::gpoint_ts{
                            time_axis::generic_dt{time_axis::point_dt{tp,t_end}},
                            v,
                            f
                        })));
    }
}

std::shared_ptr<stm_hps> build_ole_pump_hps(shyft::core::utctime t0){

    auto tN = t0 + shyft::core::deltahours(23);

    auto make_constant_ts = [&](auto value) {
        return time_series::dd::apoint_ts(time_series::dd::gta_t(t0 - shyft::core::deltahours(1),tN + shyft::core::deltahours(1),1),value,
                                          time_series::POINT_AVERAGE_VALUE);
    };

    auto hps = std::make_shared<stm_hps>(0, "a");

    auto r0 = std::make_shared<reservoir>(0,"r0","",hps);
    hps->reservoirs.push_back(std::dynamic_pointer_cast<hydro_power::reservoir>(r0));
    r0->volume_level_mapping = std::make_shared<std::map<shyft::core::utctime,std::shared_ptr<hydro_power::xy_point_curve>>>();
    r0->volume_level_mapping->emplace(
        t0,
        std::make_shared<hydro_power::xy_point_curve>(
            hydro_power::xy_point_curve{
                .points{
                    {.x =  0.0e6, .y = 1.00e2},
                    {.x = 10.0e6, .y = 1.10e2},
                    {.x = 12.0e6, .y = 1.11e2}
                }
            }));
    r0->level.regulation_max = make_breakpoint_ts({t0},t0 + 100 * shyft::core::calendar::YEAR, 110.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    r0->level.realised = make_breakpoint_ts({t0 - shyft::core::calendar::HOUR}, t0 + shyft::core::utctime(1), 102.5, time_series::ts_point_fx::POINT_INSTANT_VALUE);
    r0->water_value.endpoint_desc = make_breakpoint_ts({t0}, t0 + shyft::core::calendar::YEAR, 10e-6, time_series::ts_point_fx::POINT_AVERAGE_VALUE);

    auto r1 = std::make_shared<reservoir>(1,"r1","",hps);
    hps->reservoirs.push_back(std::dynamic_pointer_cast<hydro_power::reservoir>(r1));
    r1->volume_level_mapping = std::make_shared<std::map<shyft::core::utctime,std::shared_ptr<hydro_power::xy_point_curve>>>();
    r1->volume_level_mapping->emplace(
        t0,
        std::make_shared<hydro_power::xy_point_curve>(
            hydro_power::xy_point_curve{
                .points{
                    {.x =  0.0e6, .y = 1.0e1},
                    {.x =  6.0e6, .y = 1.5e1},
                    {.x =  8.0e6, .y = 1.6e1}
                }
            }));
    r1->level.regulation_max = make_breakpoint_ts({t0},t0 + 100 * shyft::core::calendar::YEAR, 15.0, time_series::ts_point_fx::POINT_AVERAGE_VALUE);
    r1->level.realised = make_breakpoint_ts({t0 - shyft::core::calendar::HOUR},
                                            t0 + shyft::core::utctime(1),
                                            15.0,
                                            time_series::ts_point_fx::POINT_INSTANT_VALUE);
    r1->level.constraint.max = make_breakpoint_ts({t0, tN - shyft::core::calendar::HOUR}, tN,
                                                  std::vector{shyft::nan, 12.5},
                                                  time_series::ts_point_fx::POINT_AVERAGE_VALUE);

    auto u0 = std::make_shared<unit>(0, "u0", "", hps);

    u0->generator_description =
        std::make_shared<std::map<shyft::core::utctime,std::shared_ptr<hydro_power::xy_point_curve>>>();
#define WITH_UNIT 0
    u0->generator_description->emplace(
        t0,
        std::make_shared<hydro_power::xy_point_curve>(
            hydro_power::xy_point_curve{
                .points{
                    //{.x = -60000000, .y = 98.0},
                     {.x = -6.0e8, .y = 98.0},
                     {.x = -4.0e8, .y = 95.0},
#if 0
                    {.x =  40000000, .y = 95.0},
                    {.x =  60000000, .y = 98.0},
#endif
                }
            }));

    u0->turbine_description =
        std::make_shared<std::map<shyft::core::utctime,std::shared_ptr<hydro_power::turbine_description>>>();
    u0->turbine_description->emplace(
        t0,
        std::make_shared<hydro_power::turbine_description>(
            hydro_power::turbine_description{
                .operating_zones{
                    {
                        .efficiency_curves{
                            {
                                .xy_curve{
                                    .points{
                                        { .x = -50.0, .y = 84.0 }
                                    }
                                },
                                .z = -95.0
                            },
#if WITH_UNIT
                                {
                                    .xy_curve{
                                        .points{
                                            {.x = 0.0,  .y = 0.0},
                                        }
                                    },
                                    .z = 1.0
                                }
#endif
                        }
                        
                    }
                }
            }));

    hps->units.push_back(u0);

    auto p0 = std::make_shared<power_plant>(0, "p0", "", hps);
    hps->power_plants.push_back(p0);
    power_plant::add_unit(p0,u0);

    auto w0 = std::make_shared<waterway>(0, "w0", "", hps);
    hps->waterways.push_back(w0);
    auto w1 = std::make_shared<waterway>(1, "w1", "", hps);
    hps->waterways.push_back(w1);
    auto w2 = std::make_shared<waterway>(2, "w2", "", hps);
    hps->waterways.push_back(w2);
    auto w3 = std::make_shared<waterway>(3, "w3", "", hps);
    hps->waterways.push_back(w3);

    w0->head_loss_coeff = make_constant_ts(0.0002);
    w1->head_loss_coeff = make_constant_ts(0.0001);

    {
        using hc = hydro_power::hydro_component;
        hc::connect(r0,hydro_power::main,w0);
        hc::connect(w0,w1);
        hc::connect(w1,u0);
        hc::connect(u0,w2);
        hc::connect(w2,w3);
        hc::connect(w3,r1);
    }

    return hps;
}
std::shared_ptr<stm_system> build_ole_pump(shyft::core::utctime t0){

    auto tN = t0 + shyft::core::deltahours(23);

    auto make_constant_ts = [&](auto value) {
        return time_series::dd::apoint_ts(time_series::dd::gta_t(t0,tN-t0,1),value,
                                          time_series::POINT_AVERAGE_VALUE);
    };

    auto hps = build_ole_pump_hps(t0);

    auto model = std::make_shared<stm_system>(0, "x", "");
    model->hps.push_back(hps);

    auto ma = std::make_shared<energy_market_area>(0, "a", "", model);
    model->market.push_back(ma);

    {
        using shyft::core::deltahours;
        ma->price =
            make_breakpoint_ts({t0, t0 + deltahours(12), t0 + deltahours(24)},
                               t0 + shyft::core::calendar::MONTH,
                               std::vector{20e-6, 40e-6, 20e-6},
                               time_series::ts_point_fx::POINT_INSTANT_VALUE);
        ma->max_buy = make_constant_ts(9999.0e6);
        ma->max_sale = make_constant_ts(9999.0e6);
    }
    return model;
}

}
