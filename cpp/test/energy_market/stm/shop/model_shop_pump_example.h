#pragma once

#include<memory>

#include<shyft/energy_market/stm/stm_system.h>
#include<shyft/time/utctime_utilities.h>

namespace shyft::energy_market::stm::test_models{

/** @brief build pump example model from SHOP documentation
*
*  Two reservoirs with a reversible pump-turbine. Price goes from high to low
*  after `t_begin + 6hr`.
*/
std::shared_ptr<stm_hps> build_shop_pump_example_hps(shyft::core::utctime t_begin,shyft::core::utctime t_end);
std::shared_ptr<stm_system> build_shop_pump_example(shyft::core::utctime t_begin,shyft::core::utctime t_end);

}
