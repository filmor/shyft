#pragma once
#include <cmath>
#include <cfloat>
#include <climits>
#include <shop_lib_interface.h> // External Shop library
#include <doctest/doctest.h> // Doctest testing framework

struct model_builder
{
    ShopSystem* shop;
    model_builder(ShopSystem* shop) : shop(shop) {};

    static constexpr int n_scen = 1; // We don't test with multiple scenarios, so always just one!
    void SetGetCompareInt(const int object_ix, const char*  attribute_, int v, bool check_is_default) const
    {
        shop_attr_str attribute;strcpy(attribute,attribute_);
        const int attr_ix = ShopGetAttributeIndex(shop, object_ix, attribute);
        if (check_is_default)
            CHECK_MESSAGE(ShopAttributeIsDefault(shop, object_ix, attr_ix), attribute_);
        CHECK_MESSAGE(ShopCheckAttributeToSet(shop, attr_ix), attribute_);
        CHECK_MESSAGE(ShopSetIntAttribute(shop, object_ix, attr_ix, v), attribute_);
        if (check_is_default)
            CHECK_MESSAGE(!ShopAttributeIsDefault(shop, object_ix, attr_ix), attribute_);
        CHECK_MESSAGE(ShopCheckAttributeToGet(shop, attr_ix), attribute_);
        int v_read = INT_MIN;
        ShopGetIntAttribute(shop, object_ix, attr_ix, v_read);
        CHECK_MESSAGE(v_read == v, attribute_);
    }
    void SetGetCompareDouble(const int object_ix, const char * attribute_, double v, bool check_is_default) const
    {
        shop_attr_str attribute;strcpy(attribute,attribute_);
        const int attr_ix = ShopGetAttributeIndex(shop, object_ix, attribute);
        if (check_is_default)
            CHECK_MESSAGE(ShopAttributeIsDefault(shop, object_ix, attr_ix), attribute_);
        CHECK_MESSAGE(ShopCheckAttributeToSet(shop, attr_ix), attribute_);
        CHECK_MESSAGE(ShopSetDoubleAttribute(shop, object_ix, attr_ix, v), attribute_);
        if (check_is_default)
            CHECK_MESSAGE(!ShopAttributeIsDefault(shop, object_ix, attr_ix), attribute_);
        CHECK_MESSAGE(ShopCheckAttributeToGet(shop, attr_ix), attribute_);
        double v_read = -DBL_MAX;
        ShopGetDoubleAttribute(shop, object_ix, attr_ix, v_read);
        CHECK_MESSAGE(v_read == v, attribute_);
    }
    template<typename VArray>
    void SetGetCompareDoubleArray(const int object_ix, const char* attribute_, const int n, VArray v, bool check_exists) const
    {
        shop_attr_str attribute;strcpy(attribute,attribute_);
        const int attr_ix = ShopGetAttributeIndex(shop, object_ix, attribute);
        if (check_exists)
            CHECK_MESSAGE(!ShopAttributeExists(shop, object_ix, attr_ix), attribute_);
        CHECK_MESSAGE(ShopCheckAttributeToSet(shop, attr_ix), attribute_);
        double* v_ptr = &v[0]; // Type of argument v may already be double*, but in case it is std::array<double,n> or std::vector<double> we need to make it a double* type compatible with the Shop API.
        CHECK_MESSAGE(ShopSetDoubleArrayAttribute(shop, object_ix, attr_ix, n, v_ptr), attribute_);
        if (check_exists)
            CHECK_MESSAGE(ShopAttributeExists(shop, object_ix, attr_ix), attribute_);
        CHECK_MESSAGE(ShopCheckAttributeToGet(shop, attr_ix), attribute_);
        int n_get = INT_MIN;
        CHECK_MESSAGE(ShopGetDoubleArrayLength(shop, object_ix, attr_ix, n_get), attribute_);
        CHECK_MESSAGE(n_get == n, attribute_);
        double* v_get = new double[n_get];
        n_get = INT_MIN;
        CHECK_MESSAGE(ShopGetDoubleArrayAttribute(shop, object_ix, attr_ix, n_get, v_get), attribute_);
        CHECK_MESSAGE(n_get == n, attribute_);
        for (int i=0;i<n_get;++i) {
            CHECK_MESSAGE(v[i] == v_get[i], attribute_);
        }
        delete[] v_get;
    }
    template<typename XArray, typename YArray>
    void SetGetCompareXY(const int object_ix, const char*  attribute_, double ref, const int n, XArray x, YArray y, bool check_exists) const
    {
        shop_attr_str attribute;strcpy(attribute,attribute_);
        const int attr_ix = ShopGetAttributeIndex(shop, object_ix, attribute);
        if (check_exists)
            CHECK_MESSAGE(!ShopAttributeExists(shop, object_ix, attr_ix), attribute_);
        CHECK_MESSAGE(ShopCheckAttributeToSet(shop, attr_ix), attribute_);
        double* x_ptr = &x[0]; // Type of argument x may already be double*, but in case it is std::array<double,n> or std::vector<double> we need to make it a double* type compatible with the Shop API.
        double* y_ptr = &y[0]; // Type of argument y may already be double*, but in case it is std::array<double,n> or std::vector<double> we need to make it a double* type compatible with the Shop API.
        CHECK_MESSAGE(ShopSetXyAttribute(shop, object_ix, attr_ix, ref, n, x_ptr, y_ptr), attribute_);
        if (check_exists)
            CHECK_MESSAGE(ShopAttributeExists(shop, object_ix, attr_ix), attribute_);
        CHECK_MESSAGE(ShopCheckAttributeToGet(shop, attr_ix), attribute_);
        int n_get = INT_MIN;
        CHECK_MESSAGE(ShopGetXyAttributeNPoints(shop, object_ix, attr_ix, n_get), attribute_);
        CHECK_MESSAGE(n_get == n, attribute_);
        n_get = INT_MIN;
        double ref_get = -DBL_MAX;
        double* x_get = new double[n];
        double* y_get = new double[n];
        CHECK_MESSAGE(ShopGetXyAttribute(shop, object_ix, attr_ix, ref_get, n_get, x_get, y_get), attribute_);
        CHECK_MESSAGE(n_get == n, attribute_);
        CHECK_MESSAGE(ref_get == ref, attribute_);
        for (int i=0;i<n_get;++i) {
            CHECK_MESSAGE(x[i] == x_get[i], attribute_);
            CHECK_MESSAGE(y[i] == y_get[i], attribute_);
        }
        delete[] x_get;
        delete[] y_get;
    }
    template<typename RefArray, typename NPointsArray, typename XArray, typename YArray>
    void SetGetCompareXYArray(const int object_ix, const char*  attribute_, const int n_dimensions,
        RefArray ref, NPointsArray n_points, XArray x, YArray y, bool check_exists) const
    {
        shop_attr_str attribute;strcpy(attribute,attribute_);
        const int attr_ix = ShopGetAttributeIndex(shop, object_ix, attribute);
        if (check_exists)
            CHECK_MESSAGE(!ShopAttributeExists(shop, object_ix, attr_ix), attribute_);
        CHECK_MESSAGE(ShopCheckAttributeToSet(shop, attr_ix), attribute_);
        double* ref_ptr = &ref[0]; // Type of argument ref may already be double*, but in case it is std::array<double,n> or std::vector<double> we need to make it a double* type compatible with the Shop API.
        int* n_points_ptr = &n_points[0]; // Type of argument n_points may already be int*, but in case it is std::array<int,n> or std::vector<int> we need to make it a int* type compatible with the Shop API.
        double* x_ptr = &x[0][0];
        double** x_pptr = &x_ptr; // Type of argument x may already be double**, but in case it is std::array<std::array<double,m>,n> or std::vector<std::vector<double>> we need to make it a double** type compatible with the Shop API.
        double* y_ptr = &y[0][0];
        double** y_pptr = &y_ptr; // Type of argument x may already be double**, but in case it is std::array<std::array<double,m>,n> or std::vector<std::vector<double>> we need to make it a double** type compatible with the Shop API.
        CHECK_MESSAGE(ShopSetXyArrayAttribute(shop, object_ix, attr_ix, n_dimensions, ref_ptr, n_points_ptr, x_pptr, y_pptr), attribute_);
        if (check_exists)
            CHECK_MESSAGE(ShopAttributeExists(shop, object_ix, attr_ix), attribute_);
        CHECK_MESSAGE(ShopCheckAttributeToGet(shop, attr_ix), attribute_);
        int n_dimensions_get = INT_MIN;
        int n_points_max_get = INT_MIN;
        CHECK_MESSAGE(ShopGetXyArrayDimensions(shop, object_ix, attr_ix, n_dimensions_get, n_points_max_get), attribute_);
        CHECK_MESSAGE(n_dimensions_get == n_dimensions, attribute_);
        for (int i=0;i<n_dimensions;++i){
            CHECK_MESSAGE(n_points[i] <= n_points_max_get, attribute_);
        }
        double* ref_get = new double[n_dimensions_get];
        int* n_points_get = new int[n_dimensions_get];
        double** x_get = new double*[n_dimensions];
        double** y_get = new double*[n_dimensions];
        for (int i=0;i<n_dimensions;++i){
            x_get[i] = new double[n_points_max_get];
            y_get[i] = new double[n_points_max_get];
        }
        CHECK_MESSAGE(ShopGetXyArrayAttribute(shop, object_ix, attr_ix, n_dimensions_get, ref_get, n_points_get, x_get, y_get), attribute_);
        for (int i=0;i<n_dimensions;++i) {
            CHECK_MESSAGE(ref_get[i] == ref[i], attribute_);
            CHECK_MESSAGE(n_points_get[i] == n_points[i], attribute_);
            for (int j=0;j<n_points_get[i];++j) {
                CHECK_MESSAGE(x[i][j] == x_get[i][j], attribute_);
                CHECK_MESSAGE(y[i][j] == y_get[i][j], attribute_);
            }
        }
        delete[] ref_get;
        delete[] n_points_get;
        for (int i=0;i<n_dimensions;++i) {
            delete[] x_get[i];
            delete[] y_get[i];
        }
        delete[] x_get;
        delete[] y_get;
    }
    template<typename TArray, typename YArray>
    void SetGetCompareTimeSeries(const int object_ix, const char*  start_time_, const int n_time_steps,
        const char* attribute_, const int n_points, TArray t, YArray y, bool check_exists) const
    {
        // Argument type of auto parameters t and y must be some kind of arrays: t an array of ints, y a 2D array of doubles.
        // For example y can be declared as any of the following:
        //    double y[n][m];
        //    double** y;
        //    std::array<std::array<double,m>,n>;
        //    std::vector<std::vector<double>>;
        shop_attr_str attribute;strcpy(attribute,attribute_);
        shop_time_str start_time;strcpy(start_time,start_time_);
        constexpr int n_scen = 1; // We don't test with multiple scenarios, so always just one!
        const int attr_ix = ShopGetAttributeIndex(shop, object_ix, attribute);
        if (check_exists)
            CHECK_MESSAGE(!ShopAttributeExists(shop, object_ix, attr_ix), attribute_);
        // Set as compressed break point data
        CHECK_MESSAGE(ShopCheckAttributeToSet(shop, attr_ix), attribute_);
        int* t_ptr = &t[0]; // Type of argument t may already be int*, but in case it is std::array<int,n> or std::vector<int> we need to make it a int* type compatible with the Shop API.
        double* y_ptr = &y[0][0];
        double** y_pptr = &y_ptr; // Type of argument y may already be double**, but in case it is std::array<std::array<double,m>,n> or std::vector<std::vector<double>> we need to make it a double** type compatible with the Shop API.
        CHECK_MESSAGE(ShopSetTxyAttribute(shop, object_ix, attr_ix, start_time, n_points, n_scen, t_ptr, y_pptr), attribute_);
        if (check_exists)
            CHECK_MESSAGE(ShopAttributeExists(shop, object_ix, attr_ix), attribute_);
        // Get as expanded complete series
        CHECK_MESSAGE(ShopCheckAttributeToGet(shop, attr_ix), attribute_);
        char start_time_get[18];
        int n_scen_get, n_points_get;
        CHECK_MESSAGE(ShopGetTxyAttributeDimensions(shop, object_ix, attr_ix, start_time_get, n_points_get, n_scen_get), attribute_);
        CHECK_MESSAGE(strncmp(start_time_get, start_time, strlen(start_time)) == 0, attribute_);
        CHECK_MESSAGE(n_scen_get == n_scen, attribute_);
        CHECK_MESSAGE(n_points_get == n_time_steps, attribute_); // NB: We get expanded, so one point for each time steps regardless of what we did set!
        int* t_get = new int[n_time_steps];
        double* y_get[n_scen];
        y_get[0] = new double[n_time_steps];
        double** y_get_pptr = &y_get[0];
        CHECK_MESSAGE(ShopGetTxyAttribute(shop, object_ix, attr_ix, n_points_get, n_scen_get, t_get, y_get_pptr), attribute_);
        // Verify that we really have gotten an expanded time axis
        for (int i=0;i<n_points_get;++i) {
            CHECK_MESSAGE(t_get[i]==i, attribute_);
        }
        // Compare the expanded time series data we got with the compressed break point data we set
        for (int get_i=0,set_i=0;get_i<n_points_get;++get_i){
            if (set_i<n_points-1) {
                if (get_i>=t[set_i+1]) {
                    ++set_i;
                }
            }
            if (std::isnan(y[0][set_i])) {
                CHECK_MESSAGE(y_get[0][get_i] == 1e40, attribute_); // Shop returns magic number 1e40 in points where we have set std::numeric_limits<double>::quiet_NaN()
            } else {
                CHECK_MESSAGE(y_get[0][get_i]==y[0][set_i], attribute_);
            }
        }
        delete[] t_get;
        delete[] y_get[0];
    }
    template<typename TArray, typename YArray>
    void GetTimeSeries(const int object_ix, char* const /*start_time*/, const int n_time_steps,
        char* const attribute_, TArray t, YArray y, bool check_exists) const
    {
        constexpr int n_scen = 1; // We don't test with multiple scenarios, so always just one!
        const int attr_ix = ShopGetAttributeIndex(shop, object_ix, attribute_);
        if (check_exists)
            CHECK_MESSAGE(ShopAttributeExists(shop, object_ix, attr_ix), attribute_);
        // Get as expanded complete series
        CHECK_MESSAGE(ShopCheckAttributeToGet(shop, attr_ix), attribute_);
        //TODO: ShopGetTxyAttributeDimensions crashes!
        //int n_scen_get, n_points_get;
        //CHECK_MESSAGE(ShopGetTxyAttributeDimensions(shop, object_ix, attr_ix, start_time, n_points_get, n_scen_get), attribute_);
        //CHECK_MESSAGE(n_scen_get == n_scen, attribute_);
        //CHECK_MESSAGE(n_points_get == n_time_steps, attribute_); // NB: We get expanded, so one point for each time steps regardless of what we did set!
        int* t_ptr = &t[0]; // Type of argument t may already be int*, but in case it is std::array<int,n> or std::vector<int> we need to make it a int* type compatible with the Shop API.
        double* y_ptr = &y[0][0];
        double** y_pptr = &y_ptr; // Type of argument y may already be double**, but in case it is std::array<std::array<double,m>,n> or std::vector<std::vector<double>> we need to make it a double** type compatible with the Shop API.
        CHECK_MESSAGE(ShopGetTxyAttribute(shop, object_ix, attr_ix, n_time_steps, n_scen, t_ptr, y_pptr), attribute_);
        // Verify that we really have gotten an expanded time axis
        for (int i=0;i<n_time_steps;++i) {
            CHECK_MESSAGE(t[i]==i, attribute_);
        }
    }
    template<typename TArray, typename YArray>
    void GetCheckResultSeries(const int object_ix, const char* start_time_, const int n_time_steps, const char* attribute_,
        const int n, TArray t, YArray y, bool check_exists) const
    {
        shop_time_str start_time;strcpy(start_time,start_time_);
        shop_attr_str attribute;strcpy(attribute,attribute_);
        // Get expanded result time series
        constexpr int n_scen = 1; // We don't test with multiple scenarios, so always just one!
        int* t_actual = new int[n_time_steps];
        double* y_actual[n_scen];
        y_actual[0] = new double[n_time_steps];
        double** y_actual_pptr = &y_actual[0];
        GetTimeSeries(object_ix, start_time, n_time_steps, attribute, t_actual, y_actual_pptr, check_exists);
        // Check the value at specific points
        for (int i=0;i<n;++i) {
            CHECK_MESSAGE(t_actual[t[i]] == t[i], attribute_); // We assume that the result time series is expanded (and if not the current test have already been marked as failed since GetTimeSeries checks that t[i]==i for every value of i)
            auto msg =std::string(attribute_)+std::to_string(t[i])+","+std::to_string(i);
            CHECK_MESSAGE(y_actual[0][t[i]] == doctest::Approx(y[0][i]), msg );
        }
        delete[] t_actual;
        delete[] y_actual[0];
    };

};
