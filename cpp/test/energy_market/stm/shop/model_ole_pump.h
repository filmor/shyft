#pragma once

#include<memory>

#include<shyft/energy_market/stm/stm_system.h>
#include<shyft/time/utctime_utilities.h>

namespace shyft::energy_market::stm::test_models{

/** @brief build example bump model
 *
 *  Two reservoirs with a pure pump-turbine in between.
 *  Model valid for 24 hours including t0.
 */
std::shared_ptr<stm_hps> build_ole_pump_hps(shyft::core::utctime t0);
std::shared_ptr<stm_system> build_ole_pump(shyft::core::utctime t0);

}
