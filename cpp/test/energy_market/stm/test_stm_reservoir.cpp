#include <string>
#include <doctest/doctest.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/reservoir_aggregate.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/a_wrap.h>

#include <test/energy_market/stm/utilities.h>

using namespace std;
using namespace shyft::energy_market::stm;
using shyft::time_series::dd::apoint_ts;
using shyft::energy_market::sbi_t;
using shyft::energy_market::a_wrap;
using namespace shyft::energy_market::stm; // enable equal operatos
using builder = shyft::energy_market::stm::stm_hps_builder;

TEST_SUITE_BEGIN("stm");

TEST_CASE("stm/reservoir_basics") {
  const auto name{"res_one"s};
  auto shp = make_shared<stm_hps>(1, "sys");
  stm_hps_builder b(shp);
  string json_string("some json");
  const auto r = b.create_reservoir(1, name, json_string);
  CHECK_EQ(r->id, 1);
  CHECK_EQ(r->name, name);
  CHECK_EQ(r->json, json_string);
  CHECK_THROWS_AS(b.create_reservoir(1, name + "a", ""), stm_rule_exception);
  CHECK_THROWS_AS(b.create_reservoir(2, name, ""), stm_rule_exception);
  string s;
  sbi_t sbi(s);
  r->inflow.url_fx(sbi, -1, -1, ".schedule");
  CHECK_EQ(s, "/H1/R1.inflow.schedule");

  // verify a_wrap at primary and secondary-nested levels
  a_wrap<apoint_ts> ris(
    [o = &r->inflow](sbi_t& so, int l, int tl, string_view sv) -> void {
      o->url_fx(so, l, tl, sv);
    },
    "schedule",
    r->inflow.schedule);

  CHECK_EQ(ris.url("", 0, -1), ".inflow.schedule");
  CHECK_EQ(ris.url("", 0, 0), ".{attr_id}");
  CHECK_EQ(ris.url("", 0, 1), ".inflow.schedule");

  CHECK_EQ(ris.url("", 1, -1), "/R1.inflow.schedule");
  CHECK_EQ(ris.url("", 1, 0), "/R{o_id}.{attr_id}");
  CHECK_EQ(ris.url("", 1, 1), "/R{o_id}.inflow.schedule");
  CHECK_EQ(ris.url("", 1, 2), "/R1.inflow.schedule");

  CHECK_EQ(ris.url("", 2, -1), "/H1/R1.inflow.schedule");
  CHECK_EQ(ris.url("", 2, 0), "/H{parent_id}/R{o_id}.{attr_id}");
  CHECK_EQ(ris.url("", 2, 1), "/H{parent_id}/R{o_id}.inflow.schedule");
  CHECK_EQ(ris.url("", 2, 2), "/H{parent_id}/R1.inflow.schedule");
  CHECK_EQ(ris.url("", 2, 3), "/H1/R1.inflow.schedule");
  string a_name = "water_value.endpoint_desc";
  string a_id = "{attr_id}";
  a_wrap<apoint_ts> res(
    [o = r, a_name, a_id](sbi_t& so, int l, int tl, string_view /*sv*/) -> void {
      if (l)
        o->generate_url(so, l - 1, tl > 0 ? tl - 1 : tl);
    },
    a_name,
    r->water_value.endpoint_desc);
  CHECK_EQ(res.url("", 0, -1), ".water_value.endpoint_desc");
  CHECK_EQ(res.url("", 0, 0), ".{attr_id}");
  CHECK_EQ(res.url("", 0, 1), ".water_value.endpoint_desc");

  CHECK_EQ(res.url("", 1, -1), "/R1.water_value.endpoint_desc");
  CHECK_EQ(res.url("", 1, 0), "/R{o_id}.{attr_id}");
  CHECK_EQ(res.url("", 1, 1), "/R{o_id}.water_value.endpoint_desc");
  CHECK_EQ(res.url("", 1, 2), "/R1.water_value.endpoint_desc");
  r->volume_level_mapping = make_shared<t_xy_::element_type>();
  auto level_volume = make_shared<xy_point_curve_::element_type>();
  using shyft::energy_market::hydro_power::point;
  using shyft::core::from_seconds;
  (*level_volume).points.push_back(point(1000, 0));
  (*level_volume).points.push_back(point(1010, 10));
  (*r->volume_level_mapping)[from_seconds(0)] = level_volume;
  r->tsm["x"] = apoint_ts{}; // ensure we circulate tsm
  auto blob = stm_hps::to_blob(shp);
  auto o = stm_hps::from_blob(blob);
  CHECK_EQ(*o, *shp);
  auto r2 = std::dynamic_pointer_cast<reservoir>(o->reservoirs[0]);
  CHECK_EQ(r2->id, r->id);
  CHECK_EQ(true, r2->volume_level_mapping.get() != nullptr);
  auto const & m1 = *r2->volume_level_mapping;
  auto const & m2 = *r->volume_level_mapping;
  CHECK_EQ(m1.size(), m2.size());
  for (auto const & kv : m2) {
    auto f = m1.find(kv.first);
    if (f == m1.end()) {
      MESSAGE("Failed to find key");
      CHECK(f == m1.end());
      continue;
    }
    if (*(*f).second != (*kv.second)) {
      MESSAGE("Different values..");
      CHECK(false); //
    }
  }
}

TEST_CASE("stm/reservoir_equality") {

  // Test that stm::reservoir::operator== checks stm attributes
  auto hps_1 = make_shared<stm_hps>(1, "test_hps_1");
  auto hps_2 = make_shared<stm_hps>(2, "test_hps_2");

  auto rsv_1 = builder(hps_1).create_reservoir(11, "test", "");
  auto rsv_2 = builder(hps_2).create_reservoir(11, "test", "");

  using shyft::energy_market::hydro_power::xy_point_curve;

  // All attributes left at default, objects should be equal
  CHECK_EQ(*rsv_1, *rsv_2);

  // Add some attributes to first object, no longer equal
  rsv_1->level.regulation_min = test::create_t_double(1, 101.0);
  rsv_1->volume_level_mapping = test::create_t_xy(1, {0., 100.0, 1., 101.0});
  CHECK_NE(*rsv_1, *rsv_2);

  // Add attributes to other object, now equal again
  rsv_2->level.regulation_min = test::create_t_double(1, 101.0);
  rsv_2->volume_level_mapping = test::create_t_xy(1, {0., 100.0, 1., 101.0});
  CHECK_EQ(*rsv_1, *rsv_2);
}

TEST_CASE("stm/reservoir_aggregate_equality") {
  // Test that stm::reservoir_agrgegate::operator== checks stm attributes
  auto hps_1 = make_shared<stm_hps>(1, "test_hps_1");
  auto hps_2 = make_shared<stm_hps>(2, "test_hps_2");

  auto ra_1 = builder(hps_1).create_reservoir_aggregate(11, "test", "");
  auto ra_2 = builder(hps_2).create_reservoir_aggregate(11, "test", "");

  auto r_1 = builder(hps_1).create_reservoir(14, "test", "");
  auto r_2 = builder(hps_2).create_reservoir(14, "test", "");

  // All attributes left at default, objects should be equal
  CHECK_EQ(*ra_1, *ra_2);

  // Add reservoir to one reservoir aggregate, no longer equal
  reservoir_aggregate::add_reservoir(ra_1, r_1);
  CHECK_NE(*ra_1, *ra_2);

  // Add reservoir to the other reservoir aggregate, equal again
  reservoir_aggregate::add_reservoir(ra_2, r_2);
  CHECK_EQ(*ra_1, *ra_2);

  // Add some attributes to first object, no longer equal
  ra_1->volume.static_max = test::create_t_double(1, 0.01);
  CHECK_NE(*ra_1, *ra_2);

  // Add attributes to other object, now equal again
  ra_2->volume.static_max = test::create_t_double(1, 0.01);
  CHECK_EQ(*ra_1, *ra_2);

  // Add reservoir attribute to first object, no longer equal
  r_1->inflow.schedule = test::create_t_double(1, 10.0);
  CHECK_NE(*ra_1, *ra_2);

  // Add reservoir attribute to other object, now equal again
  r_2->inflow.schedule = test::create_t_double(1, 10.0);
  CHECK_EQ(*ra_1, *ra_2);
}

TEST_CASE("stm/reservoir_aggregate_basics") {
  const auto name{"res_one"s};
  auto shp = make_shared<stm_hps>(1, "sys");
  stm_hps_builder b(shp);
  string json_string("some json");
  const auto ra = b.create_reservoir_aggregate(1, name, json_string);
  // Check that attributes were correctly initialized
  CHECK_EQ(ra->id, 1);
  CHECK_EQ(ra->name, name);
  CHECK_EQ(ra->json, json_string);
  // Test that adding reservoir aggregate with same id throws
  CHECK_THROWS_AS(b.create_reservoir_aggregate(1, name + "a", ""), stm_rule_exception);
  // Test that adding reservoir aggregate with same name throws
  CHECK_THROWS_AS(b.create_reservoir_aggregate(2, name, ""), stm_rule_exception);

  // Test url
  string s;
  sbi_t sbi(s);
  ra->inflow.url_fx(sbi, -1, -1, ".schedule");
  CHECK_EQ(s, "/H1/A1.inflow.schedule");

  // verify a_wrap at secondary-nested levels
  a_wrap<apoint_ts> ris(
    [o = &ra->inflow](sbi_t& so, int l, int tl, string_view sv) -> void {
      o->url_fx(so, l, tl, sv);
    },
    "schedule",
    ra->inflow.schedule);

  CHECK_EQ(ris.url("", 0, -1), ".inflow.schedule");
  CHECK_EQ(ris.url("", 0, 0), ".{attr_id}");
  CHECK_EQ(ris.url("", 0, 1), ".inflow.schedule");

  CHECK_EQ(ris.url("", 1, -1), "/A1.inflow.schedule");
  CHECK_EQ(ris.url("", 1, 0), "/A{o_id}.{attr_id}");
  CHECK_EQ(ris.url("", 1, 1), "/A{o_id}.inflow.schedule");
  CHECK_EQ(ris.url("", 1, 2), "/A1.inflow.schedule");

  CHECK_EQ(ris.url("", 2, -1), "/H1/A1.inflow.schedule");
  CHECK_EQ(ris.url("", 2, 0), "/H{parent_id}/A{o_id}.{attr_id}");
  CHECK_EQ(ris.url("", 2, 1), "/H{parent_id}/A{o_id}.inflow.schedule");
  CHECK_EQ(ris.url("", 2, 2), "/H{parent_id}/A1.inflow.schedule");
  CHECK_EQ(ris.url("", 2, 3), "/H1/A1.inflow.schedule");

  ra->volume.static_max = test::create_t_double(1, 0.01);
  auto blob = stm_hps::to_blob(shp);
  auto o = stm_hps::from_blob(blob);
  auto ra2 = std::dynamic_pointer_cast<reservoir_aggregate>(o->reservoir_aggregates[0]);
  CHECK_EQ(*ra2, *ra);

  // Test for shared_from_this function
  CHECK_EQ(ra, ra->shared_from_this());
  // Test that returns nullptr hps is missing
  auto ra3 = make_shared<reservoir_aggregate>(3, "ra3", "", nullptr);
  CHECK_EQ(nullptr, ra3->shared_from_this());
  // Test that returns nullptr if object is missing in the hps
  auto ra4 = make_shared<reservoir_aggregate>(4, "ra4", "", shp);
  CHECK_EQ(nullptr, ra4->shared_from_this());

  const auto r = b.create_reservoir(2, "r", json_string);

  // Tests for add_reservoir function
  // Test successful add
  reservoir_aggregate::add_reservoir(ra, r);
  CHECK(ra->reservoirs.size() == 1);
  CHECK(ra->reservoirs[0] == r);
  CHECK(ra == r->rsv_aggregate_());

  // Test add fails if reservoir already added
  CHECK_THROWS_AS(reservoir_aggregate::add_reservoir(ra, r), runtime_error);

  // Test add fails if reservoir aggregate misses hps
  auto r2 = make_shared<reservoir>(2, "r2", "", shp);
  CHECK_THROWS_AS(reservoir_aggregate::add_reservoir(ra3, r2), runtime_error);

  // Test that adding fails if the reservoir aggregate is missing from the hps
  const auto r3 = b.create_reservoir(3, "r3", json_string);
  CHECK_THROWS_AS(reservoir_aggregate::add_reservoir(ra4, r3), runtime_error);

  // Test that adding fails if reservoir already added to another reservoir_aggregate
  const auto ra5 = b.create_reservoir_aggregate(5, "ra5", json_string);
  CHECK_THROWS_AS(reservoir_aggregate::add_reservoir(ra5, r), runtime_error);

  // Test remove_reservoir function
  ra->remove_reservoir(r);
  CHECK(ra->reservoirs.size() == 0);
  CHECK(nullptr == r->rsv_aggregate_());

  // Test that removing from empty reservoir aggregate does nothing
  ra->remove_reservoir(r);
  CHECK(ra->reservoirs.size() == 0);

  // Test that adding a reservoir back works
  reservoir_aggregate::add_reservoir(ra, r);
  CHECK(ra->reservoirs.size() == 1);
  CHECK(ra->reservoirs[0] == r);
  CHECK(ra == r->rsv_aggregate_());


} // end TEST_CASE

TEST_SUITE_END();
