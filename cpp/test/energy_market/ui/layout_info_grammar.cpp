#include <test/energy_market/ui/layout_info_grammar.h>
#include <shyft/web_api/web_api_grammar.h>

namespace shyft::web_api::grammar {

  template <typename Iterator, typename Skipper = boost::spirit::qi::ascii::space_type>
  struct layout_info_grammar : public qi::grammar< Iterator, layout_info(), Skipper> {
    layout_info_grammar()
      : layout_info_grammar::base_type(start, "layout_info_grammar") {
      static constexpr auto construct = [](int id, string const & s, boost::optional<string> json) {
        return layout_info{id, s, json ? *json : string("")};
      };
      start =
        ('{' > lit("\"layout_id\"") > ':' > int_ > ',' > lit("\"name\"") > ':' > quoted_string
         > -(lit(',') > lit("\"json\"") > ':' > quoted_string) > '}')[_val = phx::bind(construct, _1, _2, _3)];
      start.name("layout_info");
      on_error<fail>(start, error_handler(_4, _3, _2));
    }

    qi::rule<Iterator, layout_info(), Skipper> start;
    quoted_string_grammar<Iterator, Skipper> quoted_string;
    phx::function<error_handler_> const error_handler = error_handler_{};
  };
  template struct layout_info_grammar<request_iterator_t, request_skipper_t>;

  template <typename Iterator, typename Skipper = boost::spirit::qi::ascii::space_type>
  struct layout_info_response_grammar : public qi::grammar< Iterator, layout_info_response(), Skipper> {
    layout_info_response_grammar()
      : layout_info_response_grammar::base_type(start, "layout_info_response_grammar") {

      static constexpr auto constructx =
        [](string const & request_id, boost::optional<vector<layout_info>> const & result) -> layout_info_response {
        return layout_info_response{request_id, result ? *result : vector<layout_info>{}};
      };

      start =
        ('{' > lit("\"request_id\"") > ':' > quoted_string > ',' > lit("\"result\"") > ':' > '[' > -(linfo_ % ',') > ']'
         > '}')[_val = phx::bind(constructx, _1, _2)];
      start.name("layout_info_response");
      linfo_.name("layout_info");
      on_error<fail>(start, error_handler(_4, _3, _2));
    }

    qi::rule<Iterator, layout_info_response(), Skipper> start;
    layout_info_grammar<Iterator, Skipper> linfo_;
    quoted_string_grammar<Iterator, Skipper> quoted_string;
    phx::function<error_handler_> const error_handler = error_handler_{};
  };
  template struct layout_info_response_grammar<request_iterator_t, request_skipper_t>;

  layout_info_response parse_layout_info_response(string const & json) {
    layout_info_response_grammar<request_iterator_t, request_skipper_t> p_;
    layout_info_response a;
    if (!phrase_parser(json.c_str(), p_, a)) {
      throw std::runtime_error("layout_info_response:failed to parse " + json);
    }
    return a;
  }

  layout_info parse_layout_info(string const & json) {
    layout_info_grammar<request_iterator_t, request_skipper_t> p_;
    layout_info a;
    if (!phrase_parser(json.c_str(), p_, a))
      throw std::runtime_error("layout_info:failed to parse " + json);
    return a;
  }


}
