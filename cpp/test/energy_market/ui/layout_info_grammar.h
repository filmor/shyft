#pragma once
#include <vector>
#include <cstdint>
#include <string>
#include <map>

#include <shyft/energy_market/ui/ui_core.h>

namespace shyft::web_api::grammar {
  using shyft::energy_market::ui::layout_info;
  using std::int64_t;
  using std::string;
  using std::vector;

  /** currently just located here because it is to ease tests */
  struct layout_info_response {
    string request_id;
    std::map<int64_t, layout_info> result;
    layout_info_response() = default;

    // constructor to match grammar parser:
    layout_info_response(string const &request_id, vector<layout_info> const &lis)
      : request_id{request_id} {
      for (auto const &li : lis) {
        result[li.id] = li;
      }
    }

    bool operator==(layout_info_response const &o) const {
      return request_id == o.request_id && result == o.result;
    }

    bool operator!=(layout_info_response const &o) const {
      return !operator==(o);
    }
  };

  // using boost spirit to create it from a well formed json string.

  layout_info_response parse_layout_info_response(string const &json);
  layout_info parse_layout_info(string const &json);
}
