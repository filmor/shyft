#include "test_pch.h"
#include "hydro_mocks.h"
#include <shyft/hydrology/methods/kirchner.h>

namespace shyfttest {
  const double EPS = 1.0e-6;

}

static inline shyft::core::utctime _t(int64_t t1970s) {
  return shyft::core::utctime{shyft::core::seconds(t1970s)};
}

using namespace shyft::core::kirchner;
using namespace shyft::core;

TEST_SUITE_BEGIN("hydrology");

TEST_CASE("hydrology/kirchner/kirchner_equal_operator") {
  parameter k1;
  parameter k2{-2.439, 0.966, -0.10};
  parameter k3{99.0, 0.966, -0.10};
  parameter k4{-2.439, 99.0, -0.10};
  parameter k5{-2.439, 0.966, 99.0};

  TS_ASSERT(k1 == k2);
  TS_ASSERT(k1 != k3);
  TS_ASSERT(k1 != k4);
  TS_ASSERT(k1 != k5);

  k3.c1 = -2.439;
  k4.c2 = 0.966;
  k5.c3 = -0.10;

  TS_ASSERT(k1 == k2);
  TS_ASSERT(k1 == k3);
  TS_ASSERT(k1 == k4);
  TS_ASSERT(k1 == k5);
}

TEST_CASE("hydrology/kirchner/single_solve") {
  using namespace shyft::core;
  parameter p;
  calculator<kirchner::trapezoidal_average, parameter> k(p);
  const double P = 2.0;
  const double E = 0.5;
  double q1 = 1.0;
  double q2 = 1.0;
  double qa1 = 0.0;
  double qa2 = 0.0;
  k.step(_t(0), _t(1), q1, qa1, P, E);
  k.step(_t(0), _t(1), q2, qa2, P, E);
  TS_ASSERT_DELTA(q1, q2, shyfttest::EPS);
  TS_ASSERT_DELTA(qa1, qa2, shyfttest::EPS);
}

TEST_CASE("hydrology/kirchner/emit_eventualy_zero_out_when_zero_input") {
  using namespace shyft::core;
  parameter p;
  calculator<kirchner::trapezoidal_average, parameter> k(p);
  double q = 0.0; // 0 mm start level
  double qa = 0.0;
  double qa_sum = 0.0;
  for (size_t i = 0; i < 10; ++i) {
    k.step(_t(0), _t(3600), q, qa, 1.0, 0.0); // let it rain 1 mm/h in 10 hour -> should be 10 mm in sum
    qa_sum += qa;
  }
  size_t n = 0;                        // loop until dry
  while (qa > 0.0 && ++n < 10000000) { // it takes 999 058 hours before this ends out to zero..
    k.step(_t(0), _t(3600), q, qa, 0.0, 0.0);
    qa_sum += qa;
  }
  CHECK_EQ(qa_sum, doctest::Approx(10.0).epsilon(0.01));
  CHECK_GT(n, 10000);
}

TEST_CASE("hydrology/kirchner/solve_from_zero_q") {
  using namespace shyft::core;
  parameter p;
  calculator<kirchner::trapezoidal_average, parameter> k(p);
  double P = 10.0;
  double E = 0.0;
  double q_state = 0.0;
  double q_response = 0.0;
  // after a number of iterations, the output should equal the input
  for (int i = 0; i < 10000000; i++) {
    k.step(_t(0), _t(3600), q_state, q_response, P, E);
    if (fabs(q_state - P) < 0.001 && fabs(q_response - P) < 0.001)
      break;
  }
  TS_ASSERT_DELTA(q_state, P, 0.001);
  TS_ASSERT_DELTA(q_response, P, 0.001);
}

TEST_CASE("hydrology/kirchner/hard_case") {
  using namespace shyft::core;
  parameter p;
  const double P = 5.93591;
  const double E = 0.0;
  double q = 2.29339;
  double q_a = 0.0;
  double atol = 1.0e-2;
  double rtol = 1.0e-4;
  for (size_t i = 0; i < 10; ++i) {
    calculator<kirchner::trapezoidal_average, parameter> k(atol, rtol, p);
    k.step(_t(0), _t(3600), q, q_a, P, E);
    atol /= 2.0;
    rtol /= 2.0;
  }
  TS_ASSERT(q < 100.0);
}

TEST_CASE("hydrology/kirchner/simple_average_loads") {
  using namespace shyft::core;
  parameter p;
  calculator<kirchner::trapezoidal_average, parameter> k(1.0e-6, 1.0e-5, p);
  size_t n_x = 200; // Simulated grid size in x direction
  size_t n_y = 300; // Simulated grid size in x direction
  double sum_q = 0.0;
  double sum_qa = 0.0;
  const std::clock_t start = std::clock();
  double Q, Q_avg;
  const double P = 0.0; // Zero precipitation makes the ode system quite simple to solve
  const double E = 0.2;
  for (size_t i = 0; i < n_x * n_y; ++i) {
    Q = 1.0;
    k.step(_t(0), _t(1), Q, Q_avg, P, E);
    sum_q += Q;
    sum_qa += Q_avg;
  }
}

using namespace std;

TEST_CASE("hydrology/kirchner/composite_average_loads") {
  using namespace shyft::core;
  parameter p;
  calculator<kirchner::composite_trapezoidal_average, parameter> k(1.0e-6, 1.0e-5, p);
  size_t n_x = 200; // Simulated grid size in x direction
  size_t n_y = 300; // Simulated grid size in x direction
  double sum_q = 0.0;
  double sum_qa = 0.0;
  const std::clock_t start = std::clock();
  double Q, Q_avg;
  const double P = 0.0; // Zero precipitation makes the ode system quite simple to solve
  const double E = 0.2;
  for (size_t i = 0; i < n_x * n_y; ++i) {
    Q = 1.0;
    k.step(_t(0), _t(3600), Q, Q_avg, P, E);
    sum_q += Q;
    sum_qa += Q_avg;
  }
}

TEST_SUITE_END();
