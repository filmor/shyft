#include "test_pch.h"
#include <shyft/hydrology/methods/hbv_snow.h>

static inline shyft::core::utctime _t(int64_t t1970s) {
  return shyft::core::utctime{shyft::core::seconds(t1970s)};
}

using namespace shyft::core;
using namespace shyft::core::hbv_snow;
using namespace std;
using SnowModel = calculator<parameter, state>;

TEST_SUITE_BEGIN("hydrology");

TEST_CASE("hydrology/hbv_snow/equal_operator") {
  vector<double> s = {1.0, 1.0, 1.0};
  vector<double> i = {0, 0.5, 1.0};
  double tx = 0.0;
  double cx = 1.0;
  double ts = 0.0;
  double lw = 0.1;
  double cfr = 0.5;
  vector<double> s2 = {
    2.0,
    2.0,
    2.0,
  };
  vector<double> i2 = {0, 1.0};

  parameter p(s, i, tx, cx, ts, lw, cfr);
  parameter p1(s2, i, tx, cx, ts, lw, cfr);
  parameter p2(s, i2, tx, cx, ts, lw, cfr);
  parameter p3(s, i, tx + 1.0, cx, ts, lw, cfr);
  parameter p4(s, i, tx, cx + 1.0, ts, lw, cfr);
  parameter p5(s, i, tx, cx, ts + 1.0, lw, cfr);
  parameter p6(s, i, tx, cx, ts, lw + 1.0, cfr);
  parameter p7(s, i, tx, cx, ts, lw, cfr + 1.0);

  TS_ASSERT(p != p1);
  TS_ASSERT(p != p2);
  TS_ASSERT(p != p3);
  TS_ASSERT(p != p4);
  TS_ASSERT(p != p5);
  TS_ASSERT(p != p6);
  TS_ASSERT(p != p7);

  p1.s = s;
  p2.intervals = i;
  p3.tx = tx;
  p4.cx = cx;
  p5.ts = ts;
  p6.lw = lw;
  p7.cfr = cfr;

  TS_ASSERT(p == p1);
  TS_ASSERT(p == p2);
  TS_ASSERT(p == p3);
  TS_ASSERT(p == p4);
  TS_ASSERT(p == p5);
  TS_ASSERT(p == p6);
  TS_ASSERT(p == p7);
}

TEST_CASE("hydrology/hbv_snow/integral_calculations") {
  vector<double> f = {0.0, 0.5, 1.0};
  vector<double> x = {0.0, 0.5, 1.0};

  double pt0 = 0.25;
  double pt1 = 0.75;
  TS_ASSERT_DELTA(integrate(f, x, x.size(), 0.0, 1.0), 0.5, 1.0e-12);
  TS_ASSERT_DELTA(
    integrate(f, x, x.size(), 0.0, pt0) + integrate(f, x, x.size(), pt0, 1.0),
    integrate(f, x, x.size(), 0.0, 1.0),
    1.0e-12);

  TS_ASSERT_DELTA(
    integrate(f, x, x.size(), pt0, pt1),
    integrate(f, x, x.size(), 0.0, 1.0) - (integrate(f, x, x.size(), 0.0, pt0) + integrate(f, x, x.size(), pt1, 1.0)),
    1.0e-12);
}

TEST_CASE("hydrology/hbv_snow/mass_balance_at_snowpack_reset") {
  vector<double> s = {1.0, 1.0, 1.0, 1.0, 1.0};
  vector<double> a = {0.0, 0.25, 0.5, 0.75, 1.0};
  parameter p(s, a);
  state state;
  response r;
  utctime t0 = _t(0);
  utctime t1 = _t(3600); // One hour
  double precipitation = 0.04;
  double temperature = 1.0;
  double sca = 1.0;
  double swe = 0.05;
  state.swe = swe;
  state.sca = sca;
  state.distribute(p);
  double total_water_before = precipitation + swe;
  SnowModel snow_model(p);
  snow_model.step(state, r, t0, t1, precipitation, temperature);
  double total_water_after = state.swe + r.outflow;
  TS_ASSERT_DELTA(total_water_before, total_water_after, 1.0e-8);
}

TEST_CASE("hydrology/hbv_snow/mass_balance_at_snowpack_buildup") {
  vector<double> s = {1.0, 1.0, 1.0, 1.0, 1.0};
  vector<double> a = {0.0, 0.25, 0.5, 0.75, 1.0};
  parameter p(s, a);
  state state;
  response r;

  utctime t0 = _t(0);
  utctime t1 = _t(3600); // One hour
  double precipitation = 0.15;
  double temperature = -1.0;
  double sca = 0.6;
  double swe = 0.2;
  state.swe = swe;
  state.sca = sca;
  double total_water_before = precipitation + swe;
  state.distribute(p);
  SnowModel snow_model(p);
  snow_model.step(state, r, t0, t1, precipitation, temperature);
  double total_water_after = state.swe + r.outflow;
  TS_ASSERT_DELTA(total_water_before, total_water_after, 1.0e-8);
  state.swe = 0.2;
  state.sca = 0.6;
  state.distribute(p);
  temperature = p.tx; // special check fo tx
  SnowModel snow_model2(p);
  snow_model2.step(state, r, t0, t1, precipitation, temperature);
  TS_ASSERT_DELTA(total_water_before, state.swe + r.outflow, 1.0e-8);
}

TEST_CASE("hydrology/hbv_snow/snow_distr_at_snowpack_buildup") {
  vector<double> s = {1.0, 1.0, 1.0, 0.0, 0.0}; //
  vector<double> a = {0.0, 0.25, 0.5, 0.75, 1.0};
  parameter p(s, a);
  state state;
  response r;

  utctime t0 = _t(0);
  utctime t1 = _t(3600); // One hour
  double precipitation = 0.15;
  double temperature = -1.0;
  double sca = 0.15;
  double swe = 10.0;
  state.swe = swe;
  state.sca = sca;
  state.distribute(p);
  SnowModel snow_model(p);
  snow_model.step(state, r, t0, t1, precipitation, temperature);
  TS_ASSERT_DELTA(state.sca, 0.75, 1e-8);
}

TEST_CASE("hydrology/hbv_snow/snow_uniform_distr_at_snowpack_buildup") {
  vector<double> s = {1.0, 1.0, 1.0, 1.0, 1.0}; //
  vector<double> a = {0.0, 0.25, 0.5, 0.75, 1.0};
  parameter p(s, a);
  state state;
  response r;

  utctime t0 = _t(0);
  utctime t1 = _t(3600); // One hour
  double precipitation = 0.15;
  double temperature = -1.0;
  double sca = 0.15;
  double swe = 10.0;
  state.swe = swe;
  state.sca = sca;
  state.distribute(p);
  SnowModel snow_model(p);
  snow_model.step(state, r, t0, t1, precipitation, temperature);
  TS_ASSERT_DELTA(state.sca, 1.0, 1e-8);
}

TEST_CASE("hydrology/hbv_snow/snow_skewed_distr_at_snowpack_buildup") {
  vector<double> s = {1.0, 0.0, 0.0, 0.0, 0.0}; //
  vector<double> a = {0.0, 0.25, 0.5, 0.75, 1.0};
  parameter p(s, a);
  state state;
  response r;

  utctime t0 = _t(0);
  utctime t1 = _t(3600); // One hour
  double precipitation = 0.15;
  double temperature = -1.0;
  double sca = 0.15;
  double swe = 10.0;
  state.swe = swe;
  state.sca = sca;
  state.distribute(p);
  SnowModel snow_model(p);
  snow_model.step(state, r, t0, t1, precipitation, temperature);
  TS_ASSERT_DELTA(state.sca, 0.25, 1e-8);
}

TEST_CASE("hydrology/hbv_snow/mass_balance_rain_no_snow") {
  vector<double> s = {1.0, 1.0, 1.0, 1.0, 1.0};
  vector<double> a = {0.0, 0.25, 0.5, 0.75, 1.0};
  parameter p(s, a);
  state state;
  response r;

  utctime t0 = _t(0);
  utctime t1 = _t(3600); // One hour
  double precipitation = 0.15;
  double temperature = p.tx;
  double sca = 0.0;
  double swe = 0.0;
  state.swe = swe;
  state.sca = sca;
  state.distribute(p);
  double total_water_before = precipitation + swe;
  SnowModel snow_model(p);
  snow_model.step(state, r, t0, t1, precipitation, temperature);
  double total_water_after = state.swe + r.outflow;
  TS_ASSERT_DELTA(total_water_before, total_water_after, 1.0e-8);
  TS_ASSERT_DELTA(state.sca, 0.0, 1.0e-8);
  TS_ASSERT_DELTA(state.swe, 0.0, 1.0e-8);
}

TEST_CASE("hydrology/hbv_snow/mass_balance_rain_no_snow_24h_step") {
  vector<double> s = {1.0, 1.0, 1.0, 1.0, 1.0};
  vector<double> a = {0.0, 0.25, 0.5, 0.75, 1.0};
  parameter p(s, a);
  state state;
  response r;

  utctime t0 = seconds(0);
  utctime t1 = seconds(3600 * 24); // One day
  double precipitation = 0.15;
  double temperature = p.tx;
  double sca = 0.0;
  double swe = 0.0;
  state.swe = swe;
  state.sca = sca;
  state.distribute(p);
  double total_water_before = precipitation + swe;
  SnowModel snow_model(p);
  snow_model.step(state, r, t0, t1, precipitation, temperature);
  double total_water_after = state.swe + r.outflow;
  TS_ASSERT_DELTA(total_water_before, total_water_after, 1.0e-8);
  TS_ASSERT_DELTA(state.sca, 0.0, 1.0e-8);
  TS_ASSERT_DELTA(state.swe, 0.0, 1.0e-8);
  // now snow and freeze water 1 day
  snow_model.step(state, r, t0, t1, precipitation, -10.0);
  TS_ASSERT_DELTA(state.swe / 24.0, precipitation, 1.0e-8); // ensure all rain stored as snow
  TS_ASSERT_DELTA(r.outflow, 0.0, 1e-8);                    // should be 0.0 outflow
  // now melt the snow in one timestep
  snow_model.step(state, r, t0, t1, 0.0, 30.0);    // very hot one day
  TS_ASSERT_DELTA(state.swe / 24.0, 0.0, 1.0e-8);  // ensure all rain stored as snow
  TS_ASSERT_DELTA(r.outflow, precipitation, 1e-8); // should be 0.0 outflow
}

TEST_CASE("hydrology/hbv_snow/mass_balance_melt_no_precip") {
  vector<double> s = {1.0, 1.0, 1.0, 1.0, 1.0};
  vector<double> a = {0.0, 0.25, 0.5, 0.75, 1.0};
  parameter p(s, a);
  state state;
  response r;

  utctime t0 = _t(0);
  utctime t1 = _t(3600); // One hour
  double precipitation = 0.0;
  double temperature = 3.0;
  double sca = 0.5;
  double swe = 10.0;
  state.swe = swe;
  state.sca = sca;
  state.distribute(p);
  double total_water_before = precipitation + swe;
  SnowModel snow_model(p);

  snow_model.step(state, r, t0, t1, precipitation, temperature);
  double total_water_after = state.swe + r.outflow;
  TS_ASSERT_DELTA(total_water_before, total_water_after, 1.0e-8);
}

TEST_SUITE_END();
