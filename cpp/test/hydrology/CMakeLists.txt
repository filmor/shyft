# CMake configuration for tests

add_executable(test_hydrology
    main.cpp
    hydro_mocks.cpp
    cf_time.cpp
    actual_evapotranspiration.cpp
    api.cpp
    calibration.cpp
    cell_builder.cpp
    gamma_snow.cpp
    glacier_melt.cpp
    gridpp.cpp
    hbv_actual_evapotranspiration.cpp
    hbv_snow.cpp
    hbv_soil.cpp
    hbv_tank.cpp
    snow_tiles.cpp
    inverse_distance.cpp
    kalman.cpp
    kirchner.cpp
    kriging.cpp
    priestley_taylor.cpp
    pt_gs_k.cpp
    pt_hs_k.cpp
    pt_st_k.cpp
    pt_st_hbv.cpp
    pt_ss_k.cpp
    region_model.cpp
    routing.cpp
    skaugen.cpp
    hbv_physical_snow.cpp
    pt_hps_k.cpp
    radiation.cpp
    penman_monteith.cpp
    r_pm_gs_k.cpp
    r_pt_gs_k.cpp
    experimental.cpp
    sceua.cpp
    geo.cpp
    drms.cpp
    precipitation_correction.cpp
    mstack_param.cpp
)

set_target_properties(test_hydrology PROPERTIES INSTALL_RPATH "$ORIGIN/../../shyft/lib")
target_compile_definitions(test_hydrology PUBLIC "__UNIT_TEST__") # Macro used for conditional compilation in gamma_snow.h
target_include_directories(test_hydrology BEFORE PRIVATE ${CMAKE_SOURCE_DIR}/cpp/test ${CMAKE_SOURCE_DIR}/cpp/test/hydrology)
target_link_libraries(
    test_hydrology
    PRIVATE shyft_private_flags doctest::doctest shyft_hydrology shyft_core
    PUBLIC shyft_public_flags
)


include(doctest)
doctest_discover_tests(test_hydrology)
