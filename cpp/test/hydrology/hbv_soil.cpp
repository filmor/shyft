
#include "test_pch.h"
#include <shyft/hydrology/methods/hbv_soil.h>


using namespace shyft::core;

TEST_SUITE_BEGIN("hydrology");

TEST_CASE("hydrology/hbv_soil/equal_operator") {
  hbv_soil::parameter p1;
  hbv_soil::parameter p2(p1.fc + 1);
  hbv_soil::parameter p3(p1.fc, p1.lpdel + 1);

  TS_ASSERT(p1 != p2);
  TS_ASSERT(p1 != p3);

  p2.fc = p1.fc;
  p3.lpdel = p1.lpdel;

  TS_ASSERT(p1 == p2);
  TS_ASSERT(p1 == p3);
};

/** @brief Extra arguments for routine
 *
 * @param insoil
 * @param pe
 * @param sca
 */
struct extra_arguments {
  extra_arguments(double insoil, double pe, double sca)
    : insoil(insoil)
    , pe(pe)
    , sca(sca) {
  }

  double insoil;
  double pe;
  double sca;
};

double water_balance_one_step(
  hbv_soil::state& s,
  hbv_soil::response& r,
  hbv_soil::calculator calc,
  extra_arguments extra_args) {
  auto s_init = s;
  calc.step(s, r, extra_args.insoil, extra_args.pe, extra_args.sca);
  double delta_sm = s.sm - s_init.sm;
  double wb = extra_args.insoil - r.sum() - delta_sm;
  return wb;
};

TEST_CASE("hydrology/hbv_soil/soil_moisture_extremes") {
  hbv_soil::response r;
  hbv_soil::parameter p(50., 0.8, 1.2, 2.);
  double land_fraction = 1;
  hbv_soil::calculator calc(p, land_fraction);

  SUBCASE("soil_moisture_equal_field_capacity") {
    extra_arguments extra_args(0., 3.0, 0.);
    hbv_soil::state s(p.fc);

    CHECK_EQ(water_balance_one_step(s, r, calc, extra_args), doctest::Approx(0));
    CHECK_LE(s.sm, p.fc);
    CHECK_EQ(r.inuz, doctest::Approx(extra_args.insoil));
  }

  SUBCASE("soil_moisture_equal_field_capacity") {
    extra_arguments extra_args(1000., 3.0, 0.);
    hbv_soil::state s(p.fc);

    CHECK_EQ(water_balance_one_step(s, r, calc, extra_args), doctest::Approx(0));
    CHECK_LE(s.sm, p.fc);
    CHECK_EQ(r.inuz, doctest::Approx(extra_args.insoil));
  }

  SUBCASE("soil_moisture_huge_inflow") {
    extra_arguments extra_args(1000., 3.0, 0.);
    hbv_soil::state s(20);

    hbv_soil::parameter p1 = p;
    p1.beta = 1e10;
    hbv_soil::calculator calc1(p1, land_fraction);

    CHECK_EQ(water_balance_one_step(s, r, calc1, extra_args), doctest::Approx(0));
    CHECK_EQ(r.inuz, doctest::Approx(extra_args.insoil - p.infmax));
  }

  SUBCASE("soil_moisture_zero") {
    extra_arguments extra_args(0., 3.0, 0.);
    hbv_soil::state s(0);

    CHECK_EQ(s.sm, doctest::Approx(0));
    hbv_soil::response r0(0, 0);
    CHECK_EQ(r, r0);
  }
}

TEST_CASE("hydrology/hbv_soil/insoil_lt_infmax") {
  hbv_soil::response r;
  hbv_soil::parameter p(50., 0.8, 1.2, 2);
  double land_fraction = 1;
  hbv_soil::calculator calc(p, land_fraction);
  extra_arguments extra_args(8., 3.0, 0);
  hbv_soil::state s(0);

  CHECK_EQ(water_balance_one_step(s, r, calc, extra_args), doctest::Approx(0));
  CHECK_EQ(r.inuz, doctest::Approx(extra_args.insoil - p.infmax));
}

TEST_CASE("hydrology/hbv_soil/no_insoil_no_inuz") {
  hbv_soil::response r;
  hbv_soil::parameter p(50., 0.8, 1.2, 2);
  double land_fraction = 1;
  hbv_soil::calculator calc(p, land_fraction);
  extra_arguments extra_args(0, 3.0, 0);
  hbv_soil::state s(17.62);

  CHECK_EQ(water_balance_one_step(s, r, calc, extra_args), doctest::Approx(0));
  CHECK_EQ(r.inuz, doctest::Approx(0));
}

TEST_CASE("hydrology/hbv_soil/no_insoil_no_evap") {
  hbv_soil::response r;
  hbv_soil::parameter p(50., 0.8, 1.2, 2);
  double land_fraction = 1;
  hbv_soil::calculator calc(p, land_fraction);
  extra_arguments extra_args(0, 0, 0);
  hbv_soil::state s(17.62);

  CHECK_EQ(water_balance_one_step(s, r, calc, extra_args), doctest::Approx(0));
  CHECK_EQ(r.inuz, doctest::Approx(0));
}

TEST_CASE("hydrology/hbv_soil/water_balance_while_drying") {
  hbv_soil::response r;
  hbv_soil::parameter p(50., 0.8, 1.2, 2);
  double land_fraction = 1;
  hbv_soil::calculator calc(p, land_fraction);
  extra_arguments extra_args(0, 4, 0);
  hbv_soil::state s(50);

  // continue here...
  int n_steps = 50;
  for (int i = 0; i < n_steps; ++i) {
    CHECK_EQ(water_balance_one_step(s, r, calc, extra_args), doctest::Approx(0));
  }
}

TEST_CASE("hydrology/hbv_soil/evapotranspiration_with_sca") {
  hbv_soil::response r;
  hbv_soil::parameter p(50., 0.8, 1.2, 2);
  double land_fraction = 1;
  hbv_soil::calculator calc(p, land_fraction);
  hbv_soil::state s(50);

  // continue here...
  std::vector<double> evap_list;
  for (auto const & sca : std::vector<double>{0, 0.5, 1}) {
    extra_arguments extra_args(0, 4, sca);
    water_balance_one_step(s, r, calc, extra_args), doctest::Approx(0);
    evap_list.push_back(r.ae);
  }

  CHECK_EQ(evap_list[1] - evap_list[0] / 2, doctest::Approx(0));
  CHECK_EQ(evap_list[-1], doctest::Approx(0));
}

TEST_SUITE_END();
