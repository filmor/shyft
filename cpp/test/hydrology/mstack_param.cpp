#include "test_pch.h"
#include <shyft/hydrology/mstack_param.h>

using namespace shyft::core;

TEST_SUITE_BEGIN("hydrology");

TEST_CASE("hydrology/mp/equal_operator") {
	mstack_parameter mstack_p1;
	mstack_parameter mstack_p2{ 99.0 };

	TS_ASSERT(mstack_p1 != mstack_p2);

	mstack_p2.reservoir_direct_response_fraction = 1.0;
	
	TS_ASSERT(mstack_p1 == mstack_p2);
}

TEST_SUITE_END();
