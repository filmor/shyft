#include "test_pch.h"
#include <shyft/hydrology/methods/glacier_melt.h>
#include <shyft/time_series/glacier_melt.h>

namespace glacier_test_constant {
  const double EPS = 1.0e-10;
}

using namespace shyft::core;

TEST_SUITE_BEGIN("hydrology");

TEST_CASE("hydrology/glacier_melt/equal_operator") {
  glacier_melt::parameter gm1;
  glacier_melt::parameter gm2{99.0, 0.0};
  glacier_melt::parameter gm3{6.0, 99.0};

  TS_ASSERT(gm1 != gm2);
  TS_ASSERT(gm1 != gm3);

  gm2.dtf = 6.0;
  gm3.direct_response = 0.0;

  TS_ASSERT(gm1 == gm2);
  TS_ASSERT(gm1 == gm3);
}

TEST_CASE("hydrology/glacier_melt/melt") {
  // Model parameters
  const double dtf = 6.0;
  glacier_melt::parameter p(dtf);

  double sca = 0.0;
  while (sca <= 1.0) {
    double gf = 0.0;
    while (gf <= 1.0) {
      double temp = -10;
      while (temp <= 10) {

        double melt = glacier_melt::step(p.dtf, temp, sca, gf);
        TS_ASSERT(melt >= 0.0);
        if (temp <= 0.0)
          TS_ASSERT_DELTA(melt, 0.0, glacier_test_constant::EPS);
        if (sca >= gf)
          TS_ASSERT_DELTA(melt, 0.0, glacier_test_constant::EPS);
        if ((temp > 0.0) && (gf > sca))
          TS_ASSERT(melt > 0.0);
        temp += 2.0;
      }
      gf += 0.2;
    }
    sca += 0.2;
  }

  // Verify one step manually:
  double temp = 1.0;
  sca = 0.0;
  double gf = 1.0;

  const double melt_m3s = glacier_melt::step(p.dtf, temp, sca, gf);
  TS_ASSERT_DELTA(melt_m3s, dtf * (gf - sca) * temp * 0.001 / 86400.0, glacier_test_constant::EPS);
}

TEST_SUITE_END();
