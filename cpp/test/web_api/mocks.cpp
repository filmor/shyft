#include "mocks.h"
#include <shyft/energy_market/stm/srv/dstm/server.h>
#include <shyft/web_api/energy_market/request_handler.h>

#include <csignal>

namespace mocks {

  using namespace shyft::energy_market;
  using std::make_shared;

  vector<apoint_ts> mk_expression(const string &ts_url_base, utctime t, utctimespan dt, int n) {
    auto aa = apoint_ts(
      ts_url_base + "aa", apoint_ts(gta_t(t, dt, n), -double(n) / 2.0, ts_point_fx::POINT_AVERAGE_VALUE));
    auto a = apoint_ts(ts_url_base, aa * 3.0 + aa);
    return {aa, a};
  }

  vector<apoint_ts> mk_expressions(const string &cname) {
    calendar utc;
    auto t = utc.time(1970, 1, 1);
    auto dt = deltahours(1);
    int n = 240;
    time_axis::fixed_dt ta(t, dt, n);
    // Create some time series:
    std::vector<apoint_ts> tsl;
    size_t kb = 4;
    tsl.reserve((16 - kb));
    for (; kb < 16; kb += 2) {
      auto ts_url = string("shyft://" + cname + "/ts") + std::to_string(kb);
      auto tsv = mk_expression(ts_url, t, dt, kb * 1000);
      tsl.insert(tsl.end(), tsv.begin(), tsv.end());
    }
    return tsl;
  }

  namespace {
    struct test_server : stm::srv::dstm::server {
      shyft::web_api::energy_market::request_handler bg_server;
      std::future<int> web_srv; ///< mutex,

      //-- to verify fx-callback
      string fx_mid;
      string fx_arg;

      bool fx_handler(string mid, string json_arg) {
        fx_mid = mid;
        fx_arg = json_arg;
        return true;
      }

      explicit test_server()
        : server() {
        bg_server.srv = this;
        this->fx_cb = [this](string m, string a) -> bool {
          return this->fx_handler(m, a);
        };
      }

      explicit test_server(const string &root_dir)
        : server() {
        bg_server.srv = this;
        dtss->add_container("test", root_dir);
      }

      void start_web_api(string host_ip, int port, string doc_root, int fg_threads, int bg_threads) {
        if (!web_srv.valid()) {
          web_srv = std::async(std::launch::async, [this, host_ip, port, doc_root, fg_threads, bg_threads]() -> int {
            return shyft::web_api::run_web_server(
              bg_server,
              host_ip,
              static_cast<unsigned short>(port),
              make_shared<string>(doc_root),
              fg_threads,
              bg_threads);
          });
        }
      }

      bool web_api_running() const {
        return web_srv.valid();
      }

      void stop_web_api() {
        if (web_srv.valid()) {
          std::raise(SIGINT);
          (void) web_srv.get();
        }
      }
    };
  }

  struct dstm_server::impl {
    dstm_server *p{nullptr};
    test_server s;
    impl(dstm_server *p)
      : p{p} {};

    impl(dstm_server *p, string const &root_dir)
      : p{p}
      , s{root_dir} {
    }
  };

  dstm_server::dstm_server()
    : p_impl(new impl(this)) {
  }

  dstm_server::dstm_server(const std::string &root_dir)
    : p_impl{new impl(this, root_dir)} {
  }

  void dstm_server::start_web_api(std::string host_ip, int port, std::string doc_root, int fg_threads, int bg_threads) {
    p_impl->s.start_web_api(host_ip, port, doc_root, fg_threads, bg_threads);
  }

  int dstm_server::start_server() {
    return p_impl->s.start_server();
  }

  void dstm_server::set_listening_ip(string host_ip) {
    p_impl->s.set_listening_ip(host_ip);
  }

  void dstm_server::stop_web_api() {
    p_impl->s.stop_web_api();
  }

  bool dstm_server::web_api_running() const {
    return p_impl->s.web_api_running();
  }

  string dstm_server::fx_mid() const {
    return p_impl->s.fx_mid;
  }

  string dstm_server::fx_arg() const {
    return p_impl->s.fx_arg;
  }

  dstm_server::~dstm_server() {
  }

  void *dstm_server::srv_dtss() const {
    return p_impl->s.dtss.get();
  }

  void dstm_server::do_add_model(string name, stm::stm_system_ mdl) {
    p_impl->s.do_add_model(name, mdl);
  }

  void dstm_server::dtss_do_store_ts(ats_vector const &tsv, bool cache, bool replace) {
    p_impl->s.dtss->do_store_ts(tsv, replace, cache);
  }

  stm::stm_system_ dstm_server::do_get_model(string name) {
    // FIXME: remove this, not safe... - jeh
    return p_impl->s.models
      .mutate_or_throw(
        name,
        [](auto view) {
          return view.model;
        })
      .get();
  }

  void dstm_server::sm_notify_change(string url_id) {
    p_impl->s.sm->notify_change(url_id);
  }

  ats_vector dstm_server::dtss_read_callback(vector<string> const &ts_ids, utcperiod p) {
    return p_impl->s.dtss_read_callback(ts_ids, p);
  }


}
