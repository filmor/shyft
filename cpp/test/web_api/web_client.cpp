#include "test_pch.h"
#include <shyft/web_api/targetver.h>
#include <boost/asio/connect.hpp>
#include <boost/asio/io_service.hpp>

#include <csignal>

#include <shyft/dtss/dtss.h>
#include <shyft/dtss/dtss_url.h>

#include <shyft/web_api/dtss_web_api.h>
#include <shyft/web_api/web_api_generator.h>
#include <shyft/web_api/web_api_grammar.h>

#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/time_series/dd/apoint_ts.h>

#include <test/test_utils.h>
#include <test/web_api/test_server.h>
#include <shyft/web_api/dtss_client.h>


using namespace shyft::dtss;
using namespace shyft::core;
using namespace shyft;
using namespace shyft::web_api;
namespace qi = boost::spirit::qi;
namespace ka = boost::spirit::karma;
namespace phx = boost::phoenix;

namespace {
  struct dtss_fixture {
    test::test_server a;
    string host_ip{"127.0.0.1"};
    int port{0};
    test::utils::temp_dir tmp_dir{"shyft.web_api.cpp."};

    dtss_fixture() {
      string doc_root = (tmp_dir / "doc_root").string();
      string ts_root = (tmp_dir / "ts_root").string();
      a.add_container("test", ts_root);
      a.queue_manager.add("qa");                                // test queue needed
      a.queue_manager.add("qb");                                // test queue needed
      port = a.start_web_api(host_ip, 0, doc_root, 1, 1, true); // enforce tls
      MESSAGE(
        "dtss.web_api started at " << host_ip << "@" << port << "\n\tts root=" << ts_root
                                   << "\n\tdocroot=" << doc_root);
    }

    ats_vector generate_tsv(
      string container,
      string prefix,
      size_t n,
      double scale = 1.0,
      double dt_sec = 10.0,
      size_t n_pts = 5) {
      ats_vector r;
      using shyft::dtss::shyft_url;
      time_axis::generic_dt ta{from_seconds(0), from_seconds(dt_sec), n_pts};
      for (size_t i = 0u; i < n; ++i) {
        r.push_back(apoint_ts(
          shyft_url(container, prefix + std::to_string(i)),
          apoint_ts(ta, scale * double(i), shyft::time_series::ts_point_fx::POINT_AVERAGE_VALUE)));
      }
      return r;
    }
  };
}

TEST_SUITE_BEGIN("web_api");

TEST_CASE(
  "dtss_nginx" * doctest::description("Test to run on dtss behind an nginx reverse proxy") * doctest::skip(true)) {
  string host_ip{"dtss.helset.org"};
  int port = 443; // https
  dtss_client c(host_ip, port);
  // 0. verify there are zero time-series to begin with
  MESSAGE("Testing find");
  auto f = c.find("shyft://test/^ts"); // matches all that begins with ts
  CHECK(f.size() == 0u);
  dtss_fixture tf;
  // 1. store some time-series
  MESSAGE("Testing store");

  auto tsv = tf.generate_tsv("test", "ts", 5);
  c.store_ts(tsv, true, true);

  // 2. verify that we now do have some time-series
  f = c.find("shyft://test/^ts[0-4]$"); // matches all that begins with ts and exactly digits 0..4
  CHECK(f.size() == 5);

  // 3. and that they exactly matches what we did store
  auto read_period = tsv[0].time_axis().total_period();
  vector<string> ts_ids;
  for (auto const &a : tsv)
    ts_ids.push_back(a.id());
  auto rtsv = c.read_ts(ts_ids, read_period, true);
  CHECK_EQ(rtsv.size(), 5u);
  CHECK_EQ(rtsv, tsv);

  // 4. setup subscribe, single shot, that will run through most cases
  ats_vector stsv; // sub tsv.
  c.subscribe_ts(
    ts_ids,
    read_period,
    true,
    read_period,
    [&](ats_vector const &x) -> bool {
      stsv = x;
      return false;
    },
    []() {
      return true;
    });
  CHECK_EQ(stsv, tsv); // should also be equal to the original vector

  // 5. setup subscribe, with a process that writes to the series being subscribed, and take 5 shots of updates
  //
  std::atomic_bool w_stop{false}; // to control stop of  the writer
  std::atomic_int w_count{0};
  // the writer thread, just generating changes every 50ms for the 5 test ts
  auto w = std::async(std::launch::async, [&]() {
    dtss_client cc(host_ip, port);
    while (!w_stop.load()) {
      std::this_thread::sleep_for(std::chrono::milliseconds(50));
      ++w_count;
      auto changed_tsv = tf.generate_tsv("test", "ts", 5, 1.0 * w_count.load()); // new values each iteration
      cc.store_ts(changed_tsv, false, true);                                     // store, cache, do not recreate ts.
    }
  });
  // 6. run subscription until the tsv handler figure out its time to bail out.

  int notify_count{0}; // require us to take some notification to prove its working!
  int timer_count{0};
  c.subscribe_ts(
    ts_ids,
    read_period,
    true,
    read_period,
    [&](ats_vector const &x) -> bool {
      stsv =
        x; // do something with the received time-series vector, its changed since last time(or it's the initial read)
      return ++notify_count < 5
          || timer_count
               < 5; // continue until we get 5, returning false means subscription is cancelled, and we are done.
    },
    [&]() -> bool {
      ++timer_count;
      return true; // continue
    });

  // 7. run subs until timer only bails out
  timer_count = 0;
  c.subscribe_ts(
    ts_ids,
    read_period,
    true,
    read_period,
    [&](ats_vector const &x) -> bool {
      stsv =
        x; // do something with the received time-series vector, its changed since last time(or it's the initial read)
      ++notify_count;
      return true; // indefinitely, let timer decide when to quite
    },
    [&]() -> bool {
      return ++timer_count < 10; // continue until some more timers.
    });
  CHECK_GE(timer_count, 10);

  // 8. prove we can do other operations while subscribing(currently at the cost of extra connection establishment)
  timer_count = notify_count = 0;
  vector<string> ts_actual_ids;
  for (auto const &n : ts_ids)
    ts_actual_ids.push_back(n + ".actual");
  c.subscribe_ts(
    ts_ids,
    read_period,
    true,
    read_period,
    [&](ats_vector const &x) -> bool {
      stsv =
        x; // do something with the received time-series vector, its changed since last time(or it's the initial read)
      ats_vector actuals;
      for (size_t i = 0u; i < x.size(); ++i)
        actuals.push_back(apoint_ts(ts_actual_ids[i], x[i]));
      c.store_ts(actuals, false, true); // illustrate we can
      return ++notify_count < 5;
    },
    [&]() -> bool {
      if (timer_count == 0) {
        f = c.find("shyft://test/^ts[0-4]$"); // within timer, we can call/operate on the client
        CHECK(f.size() == 5);
      }
      ++timer_count;
      return true; // let the other handler terminate us.
    });
  auto found_actuals = c.find("shyft://test/^ts[0-4].actual$");
  CHECK_EQ(found_actuals.size(), ts_actual_ids.size());
  w_stop = true; // now cleanup the writer thread,
  w.get();       // wait until its really done getting the future.
}

TEST_CASE_FIXTURE(dtss_fixture, "web_api/dtss_client") {
  dtss_client c(host_ip, port);
  // 0. verify there are zero time-series to begin with
  auto f = c.find("shyft://test/ts.*"); // matches all that begins with ts
  CHECK(f.size() == 0u);

  // 1. store some time-series
  auto tsv = generate_tsv("test", "ts", 5);
  c.store_ts(tsv, true, true);

  // 2. verify that we now do have some time-series
  f = c.find("shyft://test/ts[0-4]$"); // matches all that begins with ts and exactly digits 0..4
  CHECK(f.size() == 5);

  // 3. and that they exactly matches what we did store
  auto read_period = tsv[0].time_axis().total_period();
  vector<string> ts_ids;
  for (auto const &a : tsv)
    ts_ids.push_back(a.id());
  auto rtsv = c.read_ts(ts_ids, read_period, true);
  CHECK_EQ(rtsv.size(), 5u);
  CHECK_EQ(rtsv, tsv);

  // 4. setup subscribe, single shot, that will run through most cases
  ats_vector stsv; // sub tsv.
  c.subscribe_ts(
    ts_ids,
    read_period,
    true,
    read_period,
    [&](ats_vector const &x) -> bool {
      stsv = x;
      return false;
    },
    []() {
      return true;
    });
  CHECK_EQ(stsv, tsv); // should also be equal to the original vector

  // 5. setup subscribe, with a process that writes to the series being subscribed, and take 5 shots of updates
  //
  std::atomic_bool w_stop{false}; // to control stop of  the writer
  std::atomic_int w_count{0};
  // the writer thread, just generating changes every 50ms for the 5 test ts
  auto w = std::async(std::launch::async, [&]() {
    while (!w_stop.load()) {
      std::this_thread::sleep_for(std::chrono::milliseconds(50));
      ++w_count;
      auto changed_tsv = generate_tsv("test", "ts", 5, 1.0 * w_count.load()); // new values each iteration
      a.do_store_ts(changed_tsv, false, true);                                // store, cache, do not recreate ts.
    }
  });
  // 6. run subscription until the tsv handler figure out its time to bail out.

  int notify_count{0}; // require us to take some notification to prove its working!
  int timer_count{0};
  c.subscribe_ts(
    ts_ids,
    read_period,
    true,
    read_period,
    [&](ats_vector const &x) -> bool {
      stsv =
        x; // do something with the received time-series vector, its changed since last time(or it's the initial read)
      return ++notify_count < 5
          || timer_count
               < 5; // continue until we get 5, returning false means subscription is cancelled, and we are done.
    },
    [&]() -> bool {
      ++timer_count;
      return true; // continue
    });

  // 7. run subs until timer only bails out
  timer_count = 0;
  c.subscribe_ts(
    ts_ids,
    read_period,
    true,
    read_period,
    [&](ats_vector const &x) -> bool {
      stsv =
        x; // do something with the received time-series vector, its changed since last time(or it's the initial read)
      ++notify_count;
      return true; // indefinitely, let timer decide when to quite
    },
    [&]() -> bool {
      return ++timer_count < 10; // continue until some more timers.
    });
  CHECK_GE(timer_count, 10);

  // 8. prove we can do other operations while subscribing(currently at the cost of extra connection establishment)
  timer_count = notify_count = 0;
  vector<string> ts_actual_ids;
  for (auto const &n : ts_ids)
    ts_actual_ids.push_back(n + ".actual");
  c.subscribe_ts(
    ts_ids,
    read_period,
    true,
    read_period,
    [&](ats_vector const &x) -> bool {
      stsv =
        x; // do something with the received time-series vector, its changed since last time(or it's the initial read)
      ats_vector actuals;
      for (size_t i = 0u; i < x.size(); ++i)
        actuals.push_back(apoint_ts(ts_actual_ids[i], x[i]));
      c.store_ts(actuals, false, true); // illustrate we can
      return ++notify_count < 5;
    },
    [&]() -> bool {
      if (timer_count == 0) {
        f = c.find("shyft://test/ts[0-4]$"); // within timer, we can call/operate on the client
        CHECK(f.size() == 5);
      }
      ++timer_count;
      return true; // let the other handler terminate us.
    });
  auto found_actuals = c.find("shyft://test/ts[0-4].actual$");
  CHECK_EQ(found_actuals.size(), ts_actual_ids.size());
  w_stop = true; // now cleanup the writer thread,
  w.get();       // wait until its really done getting the future.

  // Done with sunshine cases, now
  // cover cases where things happen,
  // 9. attempt to do something bad, so that we get exceptions thrown at us

  auto bad_tsv = generate_tsv("test", "ts", 5, 3.14, 8.0); // change time-resolution to 8.sec.
  CHECK_THROWS_AS(
    c.store_ts(bad_tsv, false, true),
    std::runtime_error const &); // do NOT recreate ts, but try to cache if successful write(but it will not succeed..)

  auto bad_ts_ids = ts_ids;
  for (auto &ts_id : bad_ts_ids)
    ts_id += ".by.obscurity";
  CHECK_THROWS_AS((void) c.read_ts(bad_ts_ids, read_period, true), std::runtime_error const &);

  CHECK_THROWS_AS(
    c.subscribe_ts(
      bad_ts_ids,
      read_period,
      true,
      read_period,
      [](ats_vector const &) -> bool {
        return true;
      },
      []() -> bool {
        return true;
      }),
    std::runtime_error const &);
  // 9. and also test what happens if we try to connect to a server that is closed..
  // TODO:
  a.stop_web_api(); // now it's dead..
  CHECK_THROWS_AS(
    (void) c.find("shyft://test/ts.*"),
    std::runtime_error const &);                   //,std::runtime_error const&);// matches all that begins with ts
  dtss_client d{host_ip, port + 7};                // this we hope fails
  CHECK_THROWS((void) d.find("shyft://test/^ts")); //,std::runtime_error const&);// matches all that begins with ts
}

TEST_CASE_FIXTURE(dtss_fixture, "web_api/dtss_client_q") {
  string token{"Basic sometoken"};             // also verify that it works with
  a.bg_server.auth.add(vector<string>{token}); // require auth/token for this session on the server,
  FAST_CHECK_EQ(true, a.bg_server.auth.needed());
  dtss_client c(host_ip, port, token); // pass token to the client.

  // 0. get the names of configured queues
  auto q_names = c.q_list();
  REQUIRE(q_names.size() == 2u);
  auto a = q_names[0];

  // 1. verify there is 0 elements now
  auto qis = c.q_msg_infos(a);
  CHECK(qis.size() == 0u);

  // 2. verify we can't get anything from the queue now
  utctime max_wait = from_seconds(0.01);
  auto m = c.q_get(a, max_wait); // try get from empty queue
  CHECK(m == nullptr);

  // 3. verify we can maintain empty queue case
  CHECK_NOTHROW(c.q_maintain(a, false));

  // 4. put the first message
  string mid1{"m1"};
  string d1{"activate this"};
  auto ttl = from_seconds(0.1);
  auto tsv = generate_tsv("test", "ts", 5);
  c.q_put(a, mid1, d1, ttl, tsv);
  qis = c.q_msg_infos(a); // now there should be one message in the queue.
  CHECK(qis.size() == 1u);
  auto m1 = c.q_get(a, max_wait); // read it from the queue.
  REQUIRE(m1 != nullptr);

  // 5. verify that we got the right content in the message.
  CHECK(m1->info.description == d1);
  CHECK(m1->tsv == tsv);
  CHECK(m1->info.created <= m1->info.fetched);
  CHECK(m1->info.done == no_utctime);
  CHECK(m1->info.ttl == ttl);
  CHECK(c.q_size(a) == 0u);

  // 6. ack the message (like make it done)
  c.q_ack(a, mid1, "OK");
  auto m1i = c.q_msg_info(a, mid1); // get the ack for this one
  CHECK(m1i.diagnostics == "OK");

  // 7. then maintain the queue to get rid of ack/done messages
  c.q_maintain(a, false);
  CHECK(c.q_size(a) == 0u);
  qis = c.q_msg_infos(a);
  CHECK(qis.size() == 0u);
}

TEST_CASE_FIXTURE(dtss_fixture, "web_api/dtss_client_no_access") {
  string token{"Basic sometoken"};             // also verify that it works with
  a.bg_server.auth.add(vector<string>{token}); // require auth/token for this session on the server,
  FAST_CHECK_EQ(true, a.bg_server.auth.needed());
  dtss_client c(host_ip, port, token + "will not pass"); // pass token to the client.

  // 0. verify it throws
  CHECK_THROWS_AS(c.q_list(), std::runtime_error const &);
}

TEST_SUITE_END();
