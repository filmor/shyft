#pragma once

namespace test {

  //-- test utils for boost spirit qi grammars

  template <typename P, typename V>
  bool parser(char const * input, P const & p, V& v, bool full_match = true) {
    using boost::spirit::qi::parse;
    char const * f(input);
    char const * l(f + strlen(f));
    return parse(f, l, p, v) && (!full_match || (f == l));
  }

  template <typename P, typename V>
  bool phrase_parser(char const * input, P const & p, V& v, bool full_match = true) {
    using boost::spirit::qi::phrase_parse;
    using boost::spirit::qi::ascii::space;
    char const * f(input);
    char const * l(f + strlen(f));
    return phrase_parse(f, l, p, space, v) && (!full_match || (f == l));
  }

}
