#include "test_pch.h"

#include <memory>
#include <map>
#include <string>
#include <string_view>

#include <shyft/time_series/time_series_dd.h> //get all of them
#include <shyft/time_series/dd/resolve_ts.h>

namespace shyft::time_series::dd {

  TEST_SUITE_BEGIN("ts");

  TEST_CASE("ts/resolve_ts") {

    constexpr auto url0 = "test://a/b/c0", url1 = "test://a/b/c1", url2 = "test://a/b/c2", url3 = "test://c3",
                   url_invalid = "test://none";

    std::shared_ptr<const ipoint_ts> ts0{
      new gpoint_ts{time_axis::generic_dt{time_axis::fixed_dt{0, 1, 5}}, 2.0}
    },
      ts1{new aref_ts{url0}}, ts2{(apoint_ts{ts0} + apoint_ts{ts1}).ts},
      ts3{ats_vector{{apoint_ts{url0}, apoint_ts{url1}, apoint_ts{url2}}}.sum().ts};

    auto null_resolver = [](std::shared_ptr<const ipoint_ts> &, std::string_view) {
      return std::shared_ptr<const ipoint_ts>{};
    };

    std::map<std::string, std::shared_ptr<const ipoint_ts>, std::less<>> map{
      {url0, ts0},
      {url1, ts1},
      {url2, ts2},
      {url3, ts3}
    };
    auto map_resolver =
      [=](std::shared_ptr<const ipoint_ts> &source, std::string_view url) -> std::shared_ptr<const ipoint_ts> {
      auto it = map.find(url);
      if (it == map.end())
        return null_resolver(source, url);
      return it->second;
    };
    auto cloning_map_resolver =
      [=](std::shared_ptr<const ipoint_ts> &source, std::string_view url) -> std::shared_ptr<const ipoint_ts> {
      auto target = map_resolver(source, url);
      if (!target)
        return target;
      else if (target->needs_bind())
        return target->clone_expr();
      return target;
    };

    SUBCASE("0") {
      apoint_ts root{url0};

      CHECK(!resolve_ts(null_resolver, root));
      CHECK(resolve_ts(map_resolver, root));
      CHECK(root.ts == ts0);
    }
    SUBCASE("1") {
      apoint_ts root{url_invalid};
      CHECK(!resolve_ts(map_resolver, root));
    }
    SUBCASE("2") {
      apoint_ts root{url1};
      CHECK(resolve_ts(cloning_map_resolver, root));
      CHECK(root.ts == ts0);
    }
    SUBCASE("3") {
      auto root = apoint_ts{url3} + apoint_ts{url2};

      CHECK(resolve_ts(cloning_map_resolver, root));

      auto root_ptr = dynamic_cast<const abin_op_ts *>(root.ts.get());
      REQUIRE(root_ptr);

      auto lhs_ptr = dynamic_cast<const anary_op_ts *>(root_ptr->lhs.ts.get());
      REQUIRE(lhs_ptr);
      CHECK(lhs_ptr != ts2.get());

      REQUIRE(lhs_ptr->args.size() == 3);
      for (auto &&arg : lhs_ptr->args)
        REQUIRE(arg.ts);
      REQUIRE(lhs_ptr->args[0].ts == ts0);
      REQUIRE(lhs_ptr->args[1].ts != ts1);
      REQUIRE(lhs_ptr->args[2].ts != ts2);
    }
  }

  TEST_SUITE_END();

}
