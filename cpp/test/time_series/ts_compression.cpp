#include <limits>

#include <doctest/doctest.h>
#include <shyft/time/utctime_utilities.h>

#include <shyft/time_series/ts_compress.h>
#include <shyft/time_series/dd/apoint_ts.h>

using namespace shyft::time_series;
using shyft::core::utctime;
using shyft::core::utctimespan;
using shyft::core::deltahours;
using shyft::core::utctime_now;
using namespace shyft::time_series::dd;

using namespace std;

utctime round_down(utctime t, utctimespan dt) {
  return dt * (t / dt);
}

template <class T>
void check_complex_sequence() {
  using namespace std;
  T x_nan = numeric_limits<T>::quiet_NaN();
  //                  0  1        2   3   4   5   6     7   8    9  10     11
  vector<T> v{2.0f, x_nan, x_nan, 3.0f, 3.1f, 2.9f, x_nan, -1.0f, -0.9f, 4.0f, 4.05f};
  vector<T> o{2.0f, x_nan, 3.0f, x_nan, -1.0f, 4.0f};
  vector<size_t> b;
  vector<size_t> e;
  vector<size_t> ob{0, 1, 3, 6, 7, 9};
  vector<size_t> oe{1, 3, 6, 7, 9, 11};
  vector<T> c;
  delta_compression<T>(v, 0.11f, [&b, &e, &c](T xx, size_t bx, size_t ex) {
    b.push_back(bx);
    e.push_back(ex);
    ;
    c.push_back(xx);
  });
  REQUIRE_EQ(c.size(), o.size());
  CHECK_EQ(ts_compress_size<T>(v, 0.11f), c.size());
  for (size_t i = 0; i < c.size(); ++i) {
    if (isfinite(o[i]))
      CHECK_LE(abs(o[i] - c[i]), 1e-10);
    else
      CHECK_EQ(isfinite(c[i]), false);
    CHECK_EQ(b[i], ob[i]);
    CHECK_EQ(e[i], oe[i]);
  }
}

TEST_SUITE_BEGIN("ts");

TEST_CASE("ts/delta_compression") {
  using namespace shyft::time_series;
  using namespace std;
  double d_nan = numeric_limits<double>::quiet_NaN();
  // float f_nan = numeric_limits<float>::quiet_NaN();
  SUBCASE("zero elements") {
    vector<double> a0;
    size_t count = 0;
    delta_compression(a0, 0.1, [&count](double, size_t, size_t) {
      ++count;
    });
    CHECK_EQ(count, 0);
  }
  SUBCASE("one element") {
    vector<double> a1{2.0};
    size_t count = 0, b = 10u, e = 10u;
    double x = 0.0;
    delta_compression(a1, 0.1, [&count, &b, &e, &x](double xx, size_t bx, size_t ex) {
      ++count;
      b = bx;
      e = ex;
      x = xx;
    });
    CHECK_EQ(count, 1);
    CHECK_EQ(b, 0);
    CHECK_EQ(e, 1);
    CHECK_LE(abs(x - a1[0]), 0.001);
  }
  SUBCASE("two equal numbers") {
    vector<double> v{2.0, 2.0};
    size_t count = 0, b = 10u, e = 10u;
    double x = 0.0;
    delta_compression(v, 0.01, [&count, &b, &e, &x](double xx, size_t bx, size_t ex) {
      ++count;
      b = bx;
      e = ex;
      x = xx;
    });
    CHECK_EQ(count, 1);
    CHECK_EQ(b, 0);
    CHECK_EQ(e, 2);
    CHECK_LE(abs(x - v[0]), 0.001);
  }
  SUBCASE("two nan") {
    vector<double> v{d_nan, d_nan};
    size_t count = 0, b = 10u, e = 10u;
    double x = 0.0;
    delta_compression(v, 0.01, [&count, &b, &e, &x](double xx, size_t bx, size_t ex) {
      ++count;
      b = bx;
      e = ex;
      x = xx;
    });
    CHECK_EQ(count, 1);
    CHECK_EQ(b, 0);
    CHECK_EQ(e, 2);
    CHECK_EQ(isfinite(x), false);
  }
  SUBCASE("two different numbers") {
    vector<double> v{2.0, 1.0};
    vector<size_t> b;
    vector<size_t> e;
    vector<double> c;
    delta_compression(v, 0.01, [&b, &e, &c](double xx, size_t bx, size_t ex) {
      b.push_back(bx);
      e.push_back(ex);
      ;
      c.push_back(xx);
    });
    REQUIRE_EQ(c.size(), 2u);
    REQUIRE_EQ(ts_compress_size(v, 0.01), 2u);
    for (size_t i = 0; i < c.size(); ++i) {
      CHECK_LE(abs(c[i] - v[i]), 1e-10);
      CHECK_EQ(b[i], i);
      CHECK_EQ(e[i], i + 1);
    }
  }
  SUBCASE("two different num-nan") {
    vector<double> v{2.0, d_nan};
    vector<size_t> b;
    vector<size_t> e;
    vector<double> c;
    delta_compression(v, 0.01, [&b, &e, &c](double xx, size_t bx, size_t ex) {
      b.push_back(bx);
      e.push_back(ex);
      ;
      c.push_back(xx);
    });
    REQUIRE_EQ(c.size(), 2u);
    for (size_t i = 0; i < c.size(); ++i) {
      if (i == 0)
        CHECK_LE(abs(c[i] - v[i]), 1e-10);
      else
        CHECK_EQ(isfinite(c[i]), false);
      CHECK_EQ(b[i], i);
      CHECK_EQ(e[i], i + 1);
    }
  }
  SUBCASE("complex_sequence-float") {
    check_complex_sequence<float>();
  }
  SUBCASE("complex_sequence-double") {
    check_complex_sequence<double>();
  }
}

TEST_CASE("ts/apoint_ts.compress") {
  calendar utc;
  apoint_ts a{
    gta_t{utc.time(2010, 1, 1), from_seconds(10), 4},
    {1, 1.1, 2.0, 2.1},
    POINT_AVERAGE_VALUE
  };
  auto b = a.compress(0.5);
  CHECK(a.compress_size(0.5) == 2u);
  CHECK_EQ(b.value(0), doctest::Approx(1.0));
  CHECK_EQ(b.value(1), doctest::Approx(2.0));
  apoint_ts a1{
    gta_t{utc.time(2010, 1, 1), from_seconds(10), 1},
    {1.0},
    POINT_AVERAGE_VALUE
  };
  CHECK_EQ(a1, a1.compress(0.0));
  apoint_ts a0{
    gta_t{utc.time(2010, 1, 1), from_seconds(10), 0},
    {},
    POINT_AVERAGE_VALUE
  };
  CHECK_EQ(a0, a0.compress(0.0));
}

TEST_SUITE_END();
