#include <map>
#include <set>
#include <string>
#include <string_view>
#include <doctest/doctest.h>
#include <shyft/time_series/dd/resolve_tsv.h>

namespace shyft::time_series::dd {
  using std::map, std::string_view, std::vector, std::string, std::set;
  using ta_t = time_axis::generic_dt;
  using time_series::ts_point_fx;


  TEST_SUITE_BEGIN("ts");

  TEST_CASE("ts/resolve_tsv") {
    size_t n_resolve_calls{0};
    vector<string> resolve_urls;

    auto any_resolve = [&](vector<string> const &urls) {
      ats_vector r;
      ++n_resolve_calls;
      resolve_urls = urls;
      for (size_t i = 0; i < urls.size(); ++i)
        r.emplace_back(apoint_ts{
          ta_t{seconds(0), seconds(1),   3},
          {       1.0,        2.0, 3.0},
          ts_point_fx::POINT_AVERAGE_VALUE
        });
      return r;
    };
    // some test data we use
    apoint_ts a{"a"};
    apoint_ts b{"b"};
    apoint_ts c{
      ta_t{seconds(0), seconds(1),   3},
      {       1.0,        2.0, 3.0},
      ts_point_fx::POINT_AVERAGE_VALUE
    };
    apoint_ts r{"r", c};

    auto is_bound = [](auto const &v) {
      bool bound = true;
      for (auto const &x : v)
        bound &= !x.needs_bind();
      return bound;
    };
    SUBCASE("empty_trivial") {
      ats_vector e{};
      resolve_symbols(e, any_resolve);
      CHECK(n_resolve_calls == 0);
      CHECK(is_bound(e));
    }
    SUBCASE("no_symbols") {
      ats_vector e{c, c + 10.0, 3.0 * c};
      resolve_symbols(e, any_resolve);
      CHECK(n_resolve_calls == 0);
      CHECK(is_bound(e));
    }
    SUBCASE("with_symbols_serveral_occurrences") {
      ats_vector e{c, a, 3.0 * a * a + a * c};
      resolve_symbols(e, any_resolve);
      CHECK(n_resolve_calls == 1);
      CHECK(is_bound(e));
      CHECK(resolve_urls == vector<string>{"a"}); // just one call to resolve , even it is mentioned several times
    }
    SUBCASE("with_multiple_symbols") {
      ats_vector e{c, r, 3.0 * r * r + r * c + a, (a + b) / c / b};
      resolve_symbols(e, any_resolve);
      CHECK(n_resolve_calls == 1);
      CHECK(is_bound(e));
      CHECK(
        set<string>{resolve_urls.begin(), resolve_urls.end()}
        == set<string>{"a", "b"}); // just one call to resolve a,b, even it is mentioned several times
    }

    SUBCASE("with_symbols_resolved_to_unresolved_expressions") {
      auto fx_resolve = [&](vector<string> const &urls) {
        ats_vector r;
        ++n_resolve_calls;
        for (auto x : urls)
          resolve_urls.emplace_back(x);
        auto expr = apoint_ts{"x"} * 10.0; // initial expression for 1st sym
        for (auto const &url : urls) {
          if (url == "a")
            r.emplace_back(expr);
          else
            r.emplace_back(c);
        }
        return r;
      };
      ats_vector e{c, r, 3.0 * r * r + r * c + b, (a + b) / c / b};
      resolve_symbols(e, fx_resolve); // needs more,'x'
      CHECK(set<string>{resolve_urls.begin(), resolve_urls.end()} == set<string>{"a", "b", "x"});
      CHECK(is_bound(e) == true);  // all should be ok by now
      CHECK(n_resolve_calls == 2); // first a,b, then x
    }
    SUBCASE("recursion_guard") {
      ats_vector e{c, r, 3.0 * r * r + r * c + a, (a + b) / c / b};
      CHECK_THROWS_AS(resolve_symbols(e, any_resolve, 1, 1), std::runtime_error);
    }
    SUBCASE("with_symbols_returning_nullptr") {
      auto fx_nullptr_resolve = [&](vector<string> const &urls) {
        ats_vector r;
        ++n_resolve_calls;
        for (auto x : urls)
          resolve_urls.emplace_back(x);
        auto expr = apoint_ts{"x"} * 10.0; // initial expression for 1st sym
        for (auto const &url : urls) {
          if (url == "a")
            r.emplace_back(apoint_ts{}); // let it be a null ptr
          else
            r.emplace_back(c);
        }
        return r;
      };
      ats_vector e{c, r, 3.0 * r * r + r * c + b, (a + b) / c / b};
      CHECK_THROWS_AS(resolve_symbols(e, fx_nullptr_resolve), std::runtime_error);
    }
    SUBCASE("with_symbols_returning_not_complete_tsv") {
      auto fx_size_missmatch = [&](vector<string> const &) {
        return ats_vector{};
      };
      ats_vector e{c, r, 3.0 * r * r + r * c + b, (a + b) / c / b};
      CHECK_THROWS_AS(resolve_symbols(e, fx_size_missmatch), std::runtime_error);
    }
  }

  TEST_SUITE_END();

}
