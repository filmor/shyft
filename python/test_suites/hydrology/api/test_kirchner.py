from shyft.hydrology import KirchnerParameter, KirchnerState, KirchnerCalculator, KirchnerResponse
from shyft.time_series import Calendar


def test_kirchner_parameter():
    p = KirchnerParameter(c1=-4, c2=1.0, c3=0.1, )
    assert round(abs(p.c1 - (-4)), 7) == 0
    assert round(abs(p.c2 - 1.0), 7) == 0
    assert round(abs(p.c3 - 0.1), 7) == 0


def test_kirchner_state():
    s = KirchnerState(q=5.0)
    assert round(abs(s.q - 5.0), 7) == 0


def test_kirchner_calculator_step():
    utc = Calendar()
    s = KirchnerState()
    s.q = 0.1
    r = KirchnerResponse()
    r.q_avg = 0.0
    p = KirchnerParameter()
    p.c1 = -2.439
    p.c2 = 0.966
    p.c3 = -0.1
    calc = KirchnerCalculator(p)
    t1 = utc.time(2016, 10, 1)
    t2 = utc.time(2016, 10, 2)
    prec_mm_h = 3.
    evap_mm_h = 1.
    calc.step(s, r, t1, t2, prec_mm_h, evap_mm_h)
    assert round(abs(s.q - 1.3940080659430292), 7) == 0
    assert round(abs(r.q_avg - 0.5676187025364083), 7) == 0
