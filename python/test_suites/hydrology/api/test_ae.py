from shyft.hydrology import ActualEvapotranspirationCalculate_step, HbvActualEvapotranspirationCalculate_step
import math
from shyft.time_series import time, deltahours


def test_ae():
    expected = 1.0 - math.exp(-3.0*1.0/1.0)
    dt = time(3*3600)
    result = ActualEvapotranspirationCalculate_step(
        water_level=1.0,
        potential_evapotranspiration=1.0,
        scale_factor=1.0,
        snow_fraction=0.0,
        dt=dt)
    r2 = ActualEvapotranspirationCalculate_step(1.0, 1.0, 1.0, 0.0, dt)
    assert result == expected
    assert r2 == result


def test_hbv_ae():
    sca: float = 0.0  #
    pot_evap: float = 5.0  # [mm/h]
    lp: float = 150.0
    dt: time = deltahours(1)
    act_evap_less_moisture = HbvActualEvapotranspirationCalculate_step(50, pot_evap, lp, sca, dt)
    act_evap_more_moisture = HbvActualEvapotranspirationCalculate_step(100, pot_evap, lp, sca, dt)
    assert act_evap_less_moisture < act_evap_more_moisture
    assert act_evap_less_moisture == 1.6666666666666665
    assert act_evap_more_moisture == 3.333333333333333
