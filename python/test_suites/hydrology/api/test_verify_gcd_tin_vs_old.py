from typing import List, Any, Optional, Dict
import numpy as np
import pytest
from pathlib import Path
# from matplotlib import pyplot as plt

"""Verify and illustrate GeoCellData exposure to python
   
 """
#from matplotlib import pyplot as plt  # for plotting

from shyft import hydrology as api
from shyft.time_series import (Calendar, TimeSeries, TimeAxis, POINT_AVERAGE_VALUE, deltahours, IntVector)
from shyft.hydrology.repository.interfaces import GeoTsRepository, ForecastSelectionCriteria
from shyft.hydrology import (TemperatureSource, RadiationSource, PrecipitationSource, WindSpeedSource, RelHumSource, GeoPoint, ARegionEnvironment, GeoCellData, LandTypeFractions)
from shyft.hydrology import (TargetSpecificationPts, TargetSpecCalcType, TargetSpecificationVector)


class MetStationRawData:
    """ define the result of met-station info"""

    def __init__(self, *,
                 lat: float,
                 z: float,
                 area: float,
                 day_length: TimeSeries,
                 precipitation: TimeSeries,
                 radiation: TimeSeries,
                 swe: TimeSeries,
                 temperature_min: TimeSeries,
                 temperature_max: TimeSeries,
                 vp: TimeSeries):
        self.day_length: TimeSeries = day_length  # length in [s]
        self.temperature_min: TimeSeries = temperature_min  # in deg C
        self.temperature_max: TimeSeries = temperature_max  # in deg C
        self.precipitation: TimeSeries = precipitation  # in mm/h
        self.radiation: TimeSeries = radiation  # short wave rad in W/m2
        self.vp: TimeSeries = vp  # vapour pressure in Pa
        self.swe: TimeSeries = swe  # in mm
        self.lat: float = lat  # latitude
        self.z: float = z  # elevation, meter above sea-level (ref....)
        self.area: float = area  # of catchment in m2

    @property
    def temperature(self):
        return (self.temperature_min + self.temperature_max)/2.0


def read_met_station(fname: str) -> MetStationRawData:
    """
    extract data from 06191500_lump_cida_forcing_leap.txt type file
    return MetStationRawData that contains time-series+ station info.

    """
    with open(fname, encoding='utf-8') as data:
        lat = float(data.readline())  # latitude of gauge
        z = float(data.readline())  # elevation of gauge (m)
        area = float(data.readline())  # area of basin (m^2)

    assert z >= 0.0
    assert area > 0.0
    assert -90.0 < lat < 90.0

    utc = Calendar()

    def make_utctime(s: str) -> int:
        e = s.decode('utf-8').split(' ')
        return utc.time(int(e[0]), int(e[1]), int(e[2]))  # ok,int(e[3]))  if it's utc, and start of step, just drop of the hour arg.

    # avoid pandas (and extra dependency, use numpy instead)
    r = np.loadtxt(fname, delimiter='\t', skiprows=4, usecols=[0, 1, 2, 3, 4, 5, 6, 7],
                   converters={
                       0: lambda s: make_utctime(s),  # needs special care, and we deal with it immediately
                       2: lambda s: float(s)/24  # convert from mm/24h -> mm/h,
                   }
                   )
    ta = TimeAxis(r[0][0], Calendar.DAY, r.shape[0])  # assume fixed interval
    assert ta.time(r.shape[0] - 1) == r[-1, 0], 'Ensure last time-point is as read from file(is fixed interval)'
    # column defs to time-series goes like this, picking out column by column. leave calc to time-series
    mr = MetStationRawData(
        lat=lat,
        z=z,
        area=area,
        day_length=TimeSeries(ta, r[:, 1], POINT_AVERAGE_VALUE),
        precipitation=TimeSeries(ta, r[:, 2], POINT_AVERAGE_VALUE),
        radiation=TimeSeries(ta, r[:, 3], POINT_AVERAGE_VALUE),
        swe=TimeSeries(ta, r[:, 4], POINT_AVERAGE_VALUE),
        temperature_min=TimeSeries(ta, r[:, 5], POINT_AVERAGE_VALUE),
        temperature_max=TimeSeries(ta, r[:, 6], POINT_AVERAGE_VALUE),
        vp=TimeSeries(ta, r[:, 7], POINT_AVERAGE_VALUE)
    )
    return mr


class MyGeoTsRepository(GeoTsRepository):
    """
    Example how to make repositories, even if the case is 'simple'
    We do not implement what we not use, rather raise exception as a remainder
    """

    def __init__(self, fname: str, x_pos: float, y_pos: float, rel_hum: float = 0.7, wind_speed: float = 2.0):
        """ ok : stash in enough into the constructor so that you have an easy way of implementing the required members """
        self.fname: str = fname
        self.x_pos: float = x_pos
        self.y_pos: float = y_pos
        assert 0.1 < rel_hum < 1.0, 'reasonable relhum range'
        assert 0 < wind_speed < 25.0, 'reasonable wind speed range'
        self.rel_hum: float = rel_hum
        self.wind_speed: float = wind_speed
        self._met_station_raw_data: MetStationRawData = None  # lazy load this, we use it several times

    @property
    def met_station_raw_data(self):
        if not self._met_station_raw_data:
            self._met_station_raw_data = read_met_station(self.fname)
        return self._met_station_raw_data

    def get_timeseries(self, input_source_types: List[str], utc_period: Any, geo_location_criteria: Optional[Any] = None) -> Dict[str, Any]:
        """ Simply return what we have from one station, using dummy values for all others """
        mr = self.met_station_raw_data
        pos = GeoPoint(self.x_pos, self.y_pos, mr.z)

        def mk_vector(src: Any) -> Any:
            """ just to make next lines easy, given xx source ts, return it in a required vector type"""
            r = src.vector_t()  # yes, it's annotated strongly typed vector, usual habit in shyft
            r.append(src)
            return r

        return {  # here we could to clever filtering on types, geo loc etc, but we just return our best effort always
            'temperature': mk_vector(TemperatureSource(pos, mr.temperature)),
            'precipitation': mk_vector(PrecipitationSource(pos, mr.precipitation)),
            'radiation': mk_vector(RadiationSource(pos, mr.radiation)),
            'wind_speed': mk_vector(WindSpeedSource(pos, TimeSeries(mr.radiation.time_axis, fill_value=self.wind_speed, point_fx=POINT_AVERAGE_VALUE))),
            'relative_humidity': mk_vector(RelHumSource(pos, TimeSeries(mr.radiation.time_axis, fill_value=self.rel_hum, point_fx=POINT_AVERAGE_VALUE)))
        }

    def get_forecast_ensemble_collection(self, input_source_types: List, fc_selection_criteria: ForecastSelectionCriteria, geo_location_criteria: Optional[Any] = None) -> List[List[Dict[str, Any]]]:
        pass

    def get_forecast_collection(self, input_source_types: List, fc_selection_criteria: ForecastSelectionCriteria, geo_location_criteria: Optional[Any] = None) -> List[Dict[str, Any]]:
        raise NotImplementedError("Not implemented/used")

    def get_forecast_ensemble(self, input_source_types: List[str], utc_period: Any, t_c: int, geo_location_criteria: Optional[Any] = None) -> List[Dict[str, Any]]:
        raise NotImplementedError("Not implemented/used")

    def get_timeseries_ensemble(self, input_source_types: List[str], utc_period: Any, geo_location_criteria: Optional[Any] = None) -> List[Dict[str, Any]]:
        raise NotImplementedError("Not implemented/used")

    def get_forecast(self, input_source_types: List[str], utc_period: Any, t_c: int, geo_location_criteria: Optional[Any] = None) -> Dict[str, Any]:
        raise NotImplementedError("Not implemented/used")


def read_flow_station(fname: str) -> TimeSeries:
    """
    Reads flow station data from file like
    guid yyyy mm dd flow

    :param fname: path to the file
    :return: flow in m3/s
    """
    r = np.loadtxt(fname, usecols=[1, 2, 3, 4])
    utc = Calendar()
    t0 = utc.time(int(r[0, 0]), int(r[0, 1]), int(r[0, 2]))
    ta = TimeAxis(t0, Calendar.DAY, r.shape[0])
    tn = utc.time(int(r[-1, 0]), int(r[-1, 1]), int(r[-1, 2]))
    assert ta.time(r.shape[0] - 1) == tn, 'Ensure it is a fixed interval in the file, day resolution'
    feet_to_m = 0.0283168466  # file data is in feet, we need metric/SI-units
    return feet_to_m*TimeSeries(ta, r[:, 3], POINT_AVERAGE_VALUE).min_max_check_linear_fill(v_max=100000.0, v_min=0.0, dt_max=3600*24)  # replace values outside range with nan, allow max 24h dt


class VerificationData:
    def __init__(self):
        self.met_filename: str = str(Path(__file__).parent/"test_data"/"06191500_lump_cida_forcing_leap.txt")
        self.streamflow_filename = str(Path(__file__).parent/"test_data"/"06191500_streamflow_qc.txt")
        self._geo_ts = MyGeoTsRepository(self.met_filename, x_pos=0.0, y_pos=0.0, rel_hum=0.7, wind_speed=2.0)

    @property
    def region_environment(self):
        r_env = self._geo_ts.get_timeseries([], None)
        region_env = ARegionEnvironment()
        region_env.temperature = r_env['temperature']
        region_env.wind_speed = r_env['wind_speed']
        region_env.precipitation = r_env['precipitation']
        region_env.radiation = r_env['radiation']
        region_env.rel_hum = r_env['relative_humidity']
        return region_env

    @property
    def observed_flow(self):
        return read_flow_station(self.streamflow_filename)

    @property
    def area(self):
        return self._geo_ts.met_station_raw_data.area


def create_region_model_to_run(*, tin_flag: bool, area: float):
    """
    
    :param tin_flag: If True, create a single TIN 
    :param area: in m2
    :return: region_model
    """
    model_type = api.pt_gs_k.PTGSKModel
    region_parameter = model_type.parameter_t()
    catchment_parameters = model_type.parameter_t.map_t()  # Constructing a parameter map, that maps a set of parameters to a catchment ID
    catchment_parameters[1] = region_parameter  # same type as region parameters. We map catchment parameters to the cell with catchment ID 1
    x, y, z = 0, 0, 100.0  # cell center coordinates [m]
    cid = 1  # cell's catchment ID
    mid_point = GeoPoint(x, y, z)  # mid point
    glacier = 0.0  # areal glacier fraction
    lake = 0.0
    reservoir = 0.0
    forest = 0.0
    unspecified = 1.0  # residual: 1 - glacier - lake - reservoir - forest
    radiation_slope_factor = 1.0  # A factor to correct radiation input according to the cells orientation
    ltf = LandTypeFractions(glacier, lake, reservoir, forest, unspecified)  # Class to handle land type fractions

    if tin_flag:
        geo_cell_data = GeoCellData(mid_point, area, cid, radiation_slope_factor, ltf)  # Handls all cell data
        p1 = GeoPoint(-150000, -22600, 100)
        p2 = GeoPoint(150000, -22600, 100)
        p3 = GeoPoint(0, 22601.86225, 100)
        # it's a very small change in the area that matters
        geo_cell_data.set_tin_data((p1, p2, p3))
        assert round(abs(geo_cell_data.area()-area))<500 # the area should be as close as possible
        # print(geo_cell_data.area())
        cell = model_type.cell_t()  # Constructing a cell
        cell.geo = geo_cell_data  # Feeding the cell with data
        cell_vector = model_type.cell_t.vector_t()
        cell_vector.append(cell)
        region_model = model_type(cell_vector, region_parameter, catchment_parameters)  # needs all the information from above
    else:
        geo_cell_data = GeoCellData(mid_point, area, cid, radiation_slope_factor, ltf)  # Handls all cell data
        assert round(abs(geo_cell_data.area()-area))<50
        # print(geo_cell_data.area())
        cell = model_type.cell_t()  # Constructing a cell
        cell.geo = geo_cell_data  # Feeding the cell with data
        cell_vector = model_type.cell_t.vector_t()
        cell_vector.append(cell)
        region_model = model_type(cell_vector, region_parameter, catchment_parameters)  # needs all the information from above

    return region_model, cell


def run_single_cell_region_model(region_model, cell, data: VerificationData):
    region_model.region_env = data.region_environment
    cell.state.kirchner.q = 0.8
    utc = Calendar()  # shyft.hydrology calendar, nice for keeping track of time
    n_steps = 365*10  # number of steps 10 years shorter than 18 that just takes a bit longer.
    ta_sim = TimeAxis(utc.time(1990, 9, 1, 0, 0, 0), deltahours(24), n_steps)  # simulation time axis, 20 year at daily resolution
    region_model.initialize_cell_environment(ta_sim)
    region_model.interpolate(region_model.interpolation_parameter, region_model.region_env)
    region_model.run_cells()
    q = region_model.statistics.discharge([1])
    ts_trg_all = data.observed_flow  # TimeSeries(ta_srs, data.discharge, POINT_AVERAGE_VALUE)
    ta_trg = TimeAxis(utc.time(1991, 9, 1, 0, 0, 0), deltahours(24), n_steps)  # simulation time axis, 18 years
    ts_trg = ts_trg_all.average(ta_trg)
    calc_type = TargetSpecCalcType.NASH_SUTCLIFFE  # Which goal function you want to use, more flex alternative kling-gupta
    cids = IntVector([1])  # a list with IDs to let the model know which catchments the target represents
    weight = 1  # Here we have only one target time series. If you have many, you need to tell the model how to weighten them during calibration
    target_specification = TargetSpecificationPts(ts_trg, cids, weight, calc_type)  # construct the target specification
    target_spec_vec = TargetSpecificationVector()
    target_spec_vec.append(target_specification)
    region_model_opt = region_model.create_opt_model_clone()
    p_min = region_model_opt.parameter_t()
    p_max = region_model_opt.parameter_t()
    p_init = region_model_opt.parameter_t()
    p_min.kirchner.c1, p_max.kirchner.c1, p_init.kirchner.c1 = -8, -1, -3  # kirchner response parameter
    p_min.kirchner.c2, p_max.kirchner.c2, p_init.kirchner.c2 = 0.05, 1.0, 0.4  # kirchner response parameter
    p_min.kirchner.c3, p_max.kirchner.c3, p_init.kirchner.c3 = -1, -0.01, -1.0  # kirchner response parameter

    p_min.gs.tx, p_max.gs.tx, p_init.gs.tx = -3, 5, 0  # gamma snow, rain/snow threshold temperature [deg. C]
    p_min.gs.wind_const, p_max.gs.wind_const, p_init.gs.wind_const = 0, 3, 1.5  # wind scaling for turbulent flux calculations
    p_min.gs.wind_scale, p_max.gs.wind_scale, p_init.gs.wind_scale = 1, 6, 3  # wind scaling for turbulent flux calculations

    p_min.p_corr.scale_factor, p_max.p_corr.scale_factor, p_init.p_corr.scale_factor = 0.5, 3.0, 2.0  # precipitation scaling

    p_min.ae.ae_scale_factor, p_max.ae.ae_scale_factor, p_init.ae.ae_scale_factor = 0.5, 0.5, 0.5  # actual evapo scaling
    optimizer = region_model_opt.optimizer_t(region_model_opt)
    optimizer.target_specification = target_spec_vec
    optimizer.parameter_lower_bound = p_min
    optimizer.parameter_upper_bound = p_max
    optimizer.set_verbose_level(0)  # one gives print-outs, but you can get the parameter search from the optimizer anyway afterwards,no need to print.
    print("Calibrating ...")
    opt_params = optimizer.optimize(p_init, 1500, 0.2, 1e-5)  # calibrateing ... this can take a little while...
    # opt_params = optimizer.optimize_global(p_init, 500,60, 0)
    print("... done!")
    NSE = 1 - optimizer.calculate_goal_function(opt_params)
    q_opt = region_model_opt.statistics.discharge([])

    return q, q_opt, NSE, ts_trg


def test_single_cell_region_model_run_and_calibrate():
    data = VerificationData()
    # run model with old-style cell
    region_model, cell = create_region_model_to_run(tin_flag=False, area=data.area)
    q, q_opt, NSE, ts_trg = run_single_cell_region_model(region_model, cell, data)
    # run model with tin-cell
    region_model_tin, cell_tin = create_region_model_to_run(tin_flag=True, area=data.area)
    q_tin, q_opt_tin, NSE_tin, ts_trg_tin = run_single_cell_region_model(region_model_tin, cell_tin, data)
    # TODO:
    #  compare results, give different values, due to at least different calibration results.
    #  So maybe rather compare two runs, with same parameters ? --> exactly the same ?
    # ensure

    for k in range(len(q.values)):
        assert round(abs(q.values[k] - q_tin.values[k]), 0) < 50
    for k in range(len(q_opt.values)):
        assert round(abs(q_opt.values[k] - q_opt_tin.values[k]), 0) < 200

    print(f'NSE={NSE}, NSE_tin={NSE_tin} diff = {abs(NSE-NSE_tin)}')

    assert round(abs(NSE-NSE_tin),2) < 0.08
