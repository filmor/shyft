from numpy import random
import pytest
import tempfile
from os import path
import numpy as np
from shyft import hydrology as api
from shyft.time_series import (TimeAxis, TimeSeries, POINT_AVERAGE_VALUE,
                               IntVector, Calendar, deltahours, TimeAxisFixedDeltaT,
                               UtcTimeVector, DoubleVector, TsFactory, time,
                               byte_vector_to_file, byte_vector_from_file)
from shyft.hydrology import pt_gs_k
from shyft.hydrology import r_pm_gs_k
from shyft.hydrology import r_pt_gs_k
from shyft.hydrology import pt_ss_k
from shyft.hydrology import pt_hs_k
from shyft.hydrology import pt_hps_k
from shyft.hydrology import pt_st_k
from shyft.hydrology import pt_st_hbv


def build_model(model_t, parameter_t, model_size, num_catchments=1):
    cell_area = 1000*1000
    region_parameter = parameter_t()
    gcds = api.GeoCellDataVector()  # creating models from geo_cell-data is easier and more flexible
    for i in range(model_size):
        gp = api.GeoPoint(500 + 1000.0*i, 500.0, 500.0*i/model_size)
        cid = 1
        if num_catchments > 1:
            cid = random.randint(1, num_catchments + 1)
        geo_cell_data = api.GeoCellData(gp, cell_area, cid, 0.9, api.LandTypeFractions(0.01, 0.05, 0.19, 0.3, 0.45))
        geo_cell_data.land_type_fractions_info().set_fractions(glacier=0.01, lake=0.05, reservoir=0.19, forest=0.3)
        geo_cell_data.epsg_id = 32632  # requirement for slope/aspect
        gcds.append(geo_cell_data)

    return model_t(gcds, region_parameter)


def _create_constant_geo_ts(geo_ts_type, geo_point, utc_period, value):
    """Create a time point ts, with one value at the start
    of the supplied utc_period."""
    tv = UtcTimeVector()
    tv.push_back(utc_period.start)
    vv = DoubleVector()
    vv.push_back(value)
    cts = TsFactory().create_time_point_ts(utc_period, tv, vv, POINT_AVERAGE_VALUE)
    return geo_ts_type(geo_point, cts)


def test_source_uid():
    cal = Calendar()
    time_axis = TimeAxisFixedDeltaT(cal.time(2015, 1, 1, 0, 0, 0), deltahours(1), 240)
    mid_point = api.GeoPoint(1000, 1000, 100)
    precip_source = _create_constant_geo_ts(api.PrecipitationSource, mid_point, time_axis.total_period(), 5.0)
    assert precip_source.uid is not None
    precip_source.uid = 'abc'
    assert precip_source.uid == 'abc'


def _create_dummy_region_environment(time_axis, mid_point, temperature: float = 10.0):
    re = api.ARegionEnvironment()
    re.precipitation.append(_create_constant_geo_ts(api.PrecipitationSource, mid_point, time_axis.total_period(), 5.0))
    re.temperature.append(_create_constant_geo_ts(api.TemperatureSource, mid_point, time_axis.total_period(), temperature))
    re.wind_speed.append(_create_constant_geo_ts(api.WindSpeedSource, mid_point, time_axis.total_period(), 2.0))
    re.rel_hum.append(_create_constant_geo_ts(api.RelHumSource, mid_point, time_axis.total_period(), 0.7))
    re.radiation = api.RadiationSourceVector()  # just for testing BW compat
    re.radiation.append(_create_constant_geo_ts(api.RadiationSource, mid_point, time_axis.total_period(), 300.0))
    return re


def test_create_region_environment():
    cal = Calendar()
    time_axis = TimeAxisFixedDeltaT(cal.time(2015, 1, 1, 0, 0, 0), deltahours(1), 240)
    re = _create_dummy_region_environment(time_axis, api.GeoPoint(1000, 1000, 100))
    assert re is not None
    assert len(re.radiation) == 1
    assert round(abs(re.radiation[0].ts.value(0) - 300.0), 7) == 0
    vv = re.radiation.values_at_time(time_axis.time(0))  # verify .values_at_time(t)
    assert len(vv) == len(re.radiation)
    assert round(abs(vv[0] - 300.0), 7) == 0
    # also illustrate serialization.
    re_blob = re.serialize()
    assert re_blob
    re2 = api.ARegionEnvironment.deserialize(re_blob)
    assert re2
    assert re2.radiation[0].ts == re.radiation[0].ts  # verify  the restored blob is equal to source
    assert len(re2.rel_hum) == len(re.rel_hum)


def test_create_source_vector_does_not_leak():
    n = 365*24*1  # 1st checkpoint memory here,
    for i in range(10):
        v = api.TemperatureSourceVector([
            api.TemperatureSource(
                api.GeoPoint(0.0, 1.0, 2.0),
                TimeSeries(
                    TimeAxis(time(0), time(3600), n),
                    fill_value=float(x),
                    point_fx=POINT_AVERAGE_VALUE)
            )
            for x in range(n)]
        )
        assert v is not None
        del v
    pass  # 2nd mem check here, should be approx same as first checkpoint


def verify_state_handler(model):
    cids_unspecified = IntVector()
    states = model.state.extract_state(cids_unspecified)
    assert len(states) == model.size()
    state_str = str(states.serialize_to_str())
    states2 = states.__class__.deserialize_from_str(state_str)
    unapplied_list = model.state.apply_state(states, cids_unspecified)
    assert len(unapplied_list) == 0


def test_pt_ss_k_model_init():
    num_cells = 20
    model_type = pt_ss_k.PTSSKModel
    model = build_model(model_type, pt_ss_k.PTSSKParameter, num_cells)
    assert model.size() == num_cells
    verify_state_handler(model)
    assert model.skaugen_snow_response is not None
    assert model.skaugen_snow_state is not None


def test_pt_hs_k_model_init():
    num_cells = 20
    model_type = pt_hs_k.PTHSKModel
    model = build_model(model_type, pt_hs_k.PTHSKParameter, num_cells)
    assert model.size() == num_cells
    verify_state_handler(model)


def test_pt_hps_k_model_init():
    num_cells = 20
    model_type = pt_hps_k.PTHPSKModel
    model = build_model(model_type, pt_hps_k.PTHPSKParameter, num_cells)
    assert model.size() == num_cells
    verify_state_handler(model)


def test_pt_st_k_model_init():
    num_cells = 20
    model_type = pt_st_k.PTSTKModel
    model = build_model(model_type, pt_st_k.PTSTKParameter, num_cells)
    assert model.size() == num_cells
    verify_state_handler(model)


def test_pt_st_hbv_model_init():
    num_cells = 20
    model_type = pt_st_hbv.PTSTHBVModel
    model = build_model(model_type, pt_st_hbv.PTSTHBVParameter, num_cells)
    assert model.size() == num_cells
    verify_state_handler(model)


def test_model_area_functions():
    num_cells = 20
    model_type = pt_gs_k.PTGSKModel
    model = build_model(model_type, pt_gs_k.PTGSKParameter, num_cells)
    # demo how to get area statistics.
    cids = IntVector()
    total_area = model.statistics.total_area(cids)
    forest_area = model.statistics.forest_area(cids)
    glacier_area = model.statistics.glacier_area(cids)
    lake_area = model.statistics.lake_area(cids)
    reservoir_area = model.statistics.reservoir_area(cids)
    unspecified_area = model.statistics.unspecified_area(cids)
    snow_area = model.statistics.snow_storage_area(cids)
    assert round(abs(snow_area - (total_area - lake_area - reservoir_area)), 7) == 0
    assert round(abs(total_area - (forest_area + glacier_area + lake_area + reservoir_area + unspecified_area)), 7) == 0
    elevation = model.statistics.elevation(cids)
    assert abs(elevation - 475/2.0) < 1e-3, 'average height'
    cids.append(3)
    try:
        model.statistics.total_area(cids)  # now, cids contains 3, that matches no cells
        ok = False
    except RuntimeError as re:
        ok = True
        assert re

    assert ok


def test_model_pt_gs_k_initialize_and_run():
    num_cells = 20
    model_type = pt_gs_k.PTGSKModel
    model = build_model(model_type, pt_gs_k.PTGSKParameter, num_cells, num_catchments=1)
    assert model.size() == num_cells
    verify_state_handler(model)
    # demo of feature for threads
    assert model.ncore >= 1  # defaults to hardware concurrency
    model.ncore = 4  # set it to 4, and
    assert model.ncore == 4  # verify it works
    # verify auto_routing_time_axis
    assert model.auto_routing_time_axis, "should be default on"
    model.auto_routing_time_axis = False
    assert not model.auto_routing_time_axis
    # now modify snow_cv forest_factor to 0.1
    region_parameter = model.get_region_parameter()
    region_parameter.gs.snow_cv_forest_factor = 0.1
    region_parameter.gs.snow_cv_altitude_factor = 0.0001
    assert region_parameter.gs.snow_cv_forest_factor == 0.1
    assert region_parameter.gs.snow_cv_altitude_factor == 0.0001

    assert round(abs(region_parameter.gs.effective_snow_cv(1.0, 0.0) - (region_parameter.gs.snow_cv + 0.1)), 7) == 0
    assert round(abs(region_parameter.gs.effective_snow_cv(1.0, 1000.0) - (region_parameter.gs.snow_cv + 0.1 + 0.1)), 7) == 0
    cal = Calendar()
    time_axis = TimeAxisFixedDeltaT(cal.time(2015, 1, 1, 0, 0, 0), deltahours(1), 240)
    model_interpolation_parameter = api.InterpolationParameter()
    # degC/m, so -0.5 degC/100m
    model_interpolation_parameter.temperature_idw.default_temp_gradient = -0.005
    # if possible use closest neighbor points and solve gradient using equation,(otherwise default min/max height)
    model_interpolation_parameter.temperature_idw.gradient_by_equation = True
    # Max number of temperature sources used for one interpolation
    model_interpolation_parameter.temperature_idw.max_members = 6
    # 20 km is max distance
    model_interpolation_parameter.temperature_idw.max_distance = 20000
    # zscale is used to discriminate neighbors at different elevation than target point
    assert round(abs(model_interpolation_parameter.temperature_idw.zscale - 1.0), 7) == 0
    model_interpolation_parameter.temperature_idw.zscale = 0.5
    assert round(abs(model_interpolation_parameter.temperature_idw.zscale - 0.5), 7) == 0
    # Pure linear interpolation
    model_interpolation_parameter.temperature_idw.distance_measure_factor = 1.0
    # This enables IDW with default temperature gradient.
    model_interpolation_parameter.use_idw_for_temperature = True
    assert round(abs(model_interpolation_parameter.precipitation.scale_factor - 1.02), 7) == 0  # just verify this one is as before change to scale_factor
    model.initialize_cell_environment(time_axis)  # just show how we can split the run_interpolation into two calls(second one optional)
    cids = IntVector()  # optional, we can add selective catchment_ids here
    assert model.statistics.temperature(cids).point_interpretation() == POINT_AVERAGE_VALUE, 'ensure we can observe cell.env statistics with correct point_fx'
    model.interpolate(
        model_interpolation_parameter,
        _create_dummy_region_environment(time_axis,
                                         model.get_cells()[int(num_cells/2)].geo.mid_point()))
    assert model.statistics.temperature(cids).point_interpretation() == POINT_AVERAGE_VALUE, 'ensure we can observe cell.env statistics with correct point_fx'

    m_ip_parameter = model.interpolation_parameter  # illustrate that we can get back the passed interpolation parameter as a property of the model
    assert m_ip_parameter.use_idw_for_temperature == True  # just to ensure we really did get back what we passed in
    assert round(abs(m_ip_parameter.temperature_idw.zscale - 0.5), 7) == 0
    #
    # Section to demo that we can ensure that model.cells[].env_ts.xxx
    # have finite-values only
    #
    for env_ts_x in [model.cells[0].env_ts.temperature,
                     model.cells[0].env_ts.precipitation,
                     model.cells[0].env_ts.rel_hum,
                     model.cells[0].env_ts.radiation,
                     model.cells[0].env_ts.wind_speed]:
        assert model.is_cell_env_ts_ok()  # demon how to verify that cell.env_ts can be checked prior to run
        vx = env_ts_x.value(0)  # save value
        env_ts_x.set(0, float('nan'))  # insert a nan
        assert not model.is_cell_env_ts_ok()  # demo it returns false if nan @cell.env_ts
        env_ts_x.set(0, vx)  # insert back original value
        assert model.is_cell_env_ts_ok()  # ready to run again
        assert env_ts_x.point_interpretation() == POINT_AVERAGE_VALUE

    s0 = pt_gs_k.PTGSKStateVector()
    for i in range(num_cells):
        si = pt_gs_k.PTGSKState()
        si.kirchner.q = 40.0
        s0.append(si)
    model.set_states(s0)
    model.set_state_collection(-1, True)  # enable state collection for all cells
    model2 = model_type(model)  # make a copy, so that we in the stepwise run below get a clean copy with all values zero.
    opt_model = pt_gs_k.create_opt_model_clone(model)  # this is how to make a model suitable for optimizer
    model.run_cells()  # the default arguments applies: thread_cell_count=0,start_step=0,n_steps=0)

    sum_discharge = model.statistics.discharge(cids)
    sum_discharge_value = model.statistics.discharge_value(cids, 0)  # at the first timestep
    sum_charge = model.statistics.charge(cids)
    sum_charge_value = model.statistics.charge_value(cids, 0)
    assert round(abs(sum_charge_value - -110.6998 + 26.17+5.06), 2) == 0
    assert round(abs(sum_charge.values[0] - sum_charge_value), 5) == 0
    cell_charge = model.statistics.charge_value(IntVector([0, 1, 3]), 0, ix_type=api.stat_scope.cell)
    assert round(abs(cell_charge - -16.7138 + 3.95 +0.73), 2) == 0
    charge_sum_1_2_6 = model.statistics.charge(IntVector([1, 2, 6]), ix_type=api.stat_scope.cell).values.to_numpy().sum()
    assert charge_sum_1_2_6 == pytest.approx(-52.1314)
    ae_output = model.actual_evaptranspiration_response.output(cids)
    ae_pot_ratio = model.actual_evaptranspiration_response.pot_ratio(cids)
    assert ae_output is not None
    assert round(abs(ae_output.values.to_numpy().max() - 0.1897396627942494), 7) == 0
    assert ae_pot_ratio is not None
    assert round(abs(ae_pot_ratio.values.to_numpy().min() - 0.9995588456232412), 7) == 0
    assert round(abs(ae_pot_ratio.values.to_numpy().max() - 1.0), 7) == 0
    opt_model.run_cells()  # starting out with the same state, same interpolated values, and region-parameters, we should get same results
    sum_discharge_opt_value = opt_model.statistics.discharge_value(cids, 0)
    assert round(abs(sum_discharge_opt_value - sum_discharge_value), 3) == 0  # verify the opt_model clone gives same value
    assert sum_discharge_value >= 130.0
    opt_model.region_env.temperature[0].ts.set(0, 23.2)  # verify that region-env is different (no aliasing, a true copy is required)
    assert not (abs(model.region_env.temperature[0].ts.value(0) - opt_model.region_env.temperature[0].ts.value(0)) > 0.5)

    #
    # check values
    #
    assert sum_discharge is not None
    # now, re-run the process in 24-hours steps x 10
    model.set_states(s0)  # restore state s0
    assert s0.size() == model.initial_state.size()
    for do_collect_state in [False, True]:
        model2.set_state_collection(-1, do_collect_state)  # issue reported by Yisak, prior to 21.3, this would crash
        model2.set_states(s0)
        # now  after fix, it works Ok
        for section in range(10):
            model2.run_cells(use_ncore=0, start_step=section*24, n_steps=24)
            section_discharge = model2.statistics.discharge(cids)
            assert section_discharge.size() == sum_discharge.size()  # notice here that the values after current step are 0.0
    stepwise_sum_discharge = model2.statistics.discharge(cids)
    # assert stepwise_sum_discharge == sum_discharge
    diff_ts = sum_discharge.values.to_numpy() - stepwise_sum_discharge.values.to_numpy()
    assert round(abs((diff_ts*diff_ts).max() - 0.0), 4) == 0
    # Verify that if we pass in illegal cids, then it raises exception(with first failing
    try:
        illegal_cids = IntVector([0, 4, 5])
        model.statistics.discharge(illegal_cids)
        assert not True, "Failed test, using illegal cids should raise exception"
    except RuntimeError as rte:
        pass

    avg_temperature = model.statistics.temperature(cids)
    avg_precipitation = model.statistics.precipitation(cids)
    assert avg_precipitation is not None
    for time_step in range(time_axis.size()):
        precip_raster = model.statistics.precipitation(cids, time_step)  # example raster output
        assert precip_raster.size() == num_cells
    # example single value spatial aggregation (area-weighted) over cids for a specific timestep
    avg_gs_sc_value = model.gamma_snow_response.sca_value(cids, 1)
    assert avg_gs_sc_value >= 0.0
    avg_gs_sca = model.gamma_snow_response.sca(cids)  # swe output
    assert avg_gs_sca is not None
    # lwc surface_heat alpha melt_mean melt iso_pot_energy temp_sw
    avg_gs_albedo = model.gamma_snow_state.albedo(cids)
    assert avg_gs_albedo is not None
    assert avg_temperature.size() == time_axis.size(), "expect results equal to time-axis size"
    copy_region_model = model.__class__(model)
    assert copy_region_model is not None
    copy_region_model.run_cells()  # just to verify we can copy and run the new model
    #
    # Play with routing and river-network
    #
    # 1st: add a river, with 36.000 meter hydro length, a UHGParameter with 1m/hour speed, alpha/beta suitable
    model.river_network.add(
        api.River(1, api.RoutingInfo(0, 3000.0), api.UHGParameter(1/3.60, 7.0, 0.0)))  # river id =1
    # 2nd: let cells route to the river
    model.connect_catchment_to_river(1, 1)  # now all cells in catchment 1 routes to river with id 1.
    assert model.has_routing()
    # 3rd: now we can have a look at water coming in and out
    river_out_m3s = model.river_output_flow_m3s(1)  # should be delayed and reshaped
    river_local_m3s = model.river_local_inflow_m3s(
        1)  # should be equal to cell outputs (no routing stuff from cell to river)
    river_upstream_inflow_m3s = model.river_upstream_inflow_m3s(
        1)  # should be 0.0 in this case, since we do not have a routing network
    assert river_out_m3s is not None
    assert round(abs(river_out_m3s.value(8) - 28.061248025828114 - 5.0), 0) == 0
    assert river_local_m3s is not None
    assert river_upstream_inflow_m3s is not None
    model.connect_catchment_to_river(1, 0)
    assert not model.has_routing()
    #
    # Test the state-adjustments interfaces
    #

    q_0 = model.cells[0].state.kirchner.q
    model.adjust_q(2.0, cids)
    q_1 = model.cells[0].state.kirchner.q
    assert round(abs(q_0*2.0 - q_1), 7) == 0
    model.revert_to_initial_state()  # ensure we have a known state
    model.run_cells(0, 10, 2)  # just run step 10 and 11
    q_avg = (model.statistics.discharge_value(cids, 10) + model.statistics.discharge_value(cids, 11))/2.0  # get out the discharge for step 10 and 11
    x = 0.7  # we want x*q_avg as target
    model.revert_to_initial_state()  # important, need start state for the test here
    adjust_result = model.adjust_state_to_target_flow(x*q_avg, cids, start_step=10, scale_range=3.0, scale_eps=1e-3,
                                                      max_iter=350, n_steps=2)  # This is how to adjust state to observed average flow for cids for tstep 10
    assert len(adjust_result.diagnostics) == 0  # diag should be len(0) if ok.
    assert round(abs(adjust_result.q_r - q_avg*x), 2) == 0  # verify we reached target
    assert round(abs(adjust_result.q_0 - q_avg), 2) == 0  # .q_0,
    # now verify what happens if we put in bad values for observed value
    adjust_result = model.adjust_state_to_target_flow(float('nan'), cids, start_step=10, scale_range=3.0, scale_eps=1e-3, max_iter=300, n_steps=2)
    assert len(adjust_result.diagnostics) > 0, 'expect diagnostics length be larger than 0'
    # then verify what happens if we put in bad values on simulated result
    model.cells[0].env_ts.temperature.set(10, float('nan'))
    adjust_result = model.adjust_state_to_target_flow(30.0, cids, start_step=10, scale_range=3.0, scale_eps=1e-3, max_iter=300, n_steps=2)
    assert len(adjust_result.diagnostics) > 0, 'expect diagnostics length be larger than 0'


def test_model_clone_with_catchment_parameters():
    """ Verify we can copy an opt-model from full model including catchment specific parameters"""
    m = build_model(pt_gs_k.PTGSKModel, pt_gs_k.PTGSKParameter, model_size=20, num_catchments=2)
    p2 = pt_gs_k.PTGSKParameter()
    p2.kirchner.c1 = 2.3
    m.set_catchment_parameter(2, p2)
    assert m.has_catchment_parameter(2)
    assert not m.has_catchment_parameter(1)
    o = pt_gs_k.create_opt_model_clone(m, True)  # this is how to create an opt-model, with catchm. spec params
    assert o.has_catchment_parameter(2)
    assert not o.has_catchment_parameter(1)
    o = pt_gs_k.create_opt_model_clone(m, False)  # default, only region param is copied(for opt-purposes)
    assert not o.has_catchment_parameter(2)
    assert not o.has_catchment_parameter(1)


def test_optimization_model():
    num_cells = 20
    model_type = pt_gs_k.PTGSKModel
    opt_model_type = pt_gs_k.PTGSKOptModel
    model = build_model(model_type, pt_gs_k.PTGSKParameter, num_cells)
    cal = Calendar()
    t0 = cal.time(2015, 1, 1, 0, 0, 0)
    dt = deltahours(1)
    n = 240
    time_axis = TimeAxisFixedDeltaT(t0, dt, n)
    model_interpolation_parameter = api.InterpolationParameter()
    model.initialize_cell_environment(time_axis)  # just show how we can split the run_interpolation into two calls(second one optional)
    model.interpolate(
        model_interpolation_parameter,
        _create_dummy_region_environment(time_axis,
                                         model.get_cells()[int(num_cells/2)].geo.mid_point()))
    s0 = pt_gs_k.PTGSKStateVector()
    for i in range(num_cells):
        si = pt_gs_k.PTGSKState()
        si.kirchner.q = 40.0
        s0.append(si)
    model.set_snow_sca_swe_collection(-1, True)
    model.set_states(s0)  # at this point the intial state of model is established as well
    model.run_cells()
    cids = IntVector.from_numpy([1])  # optional, we can add selective catchment_ids here
    sum_discharge = model.statistics.discharge(cids)
    sum_discharge_value = model.statistics.discharge_value(cids, 0)  # at the first timestep
    assert sum_discharge_value >= 130.0
    # verify we can construct an optimizer
    opt_model = model.create_opt_model_clone()
    opt_model.set_snow_sca_swe_collection(-1, True)  # ensure to fill in swe/sca
    opt_model.run_cells()
    opt_sum_discharge = opt_model.statistics.discharge(cids)
    opt_swe = opt_model.statistics.snow_swe(cids)  # how to get out swe/sca
    opt_swe_v = opt_model.statistics.snow_swe_value(cids, 0)
    opt_sca = opt_model.statistics.snow_sca(cids)

    for i in range(len(opt_model.time_axis)):
        opt_sca_v = opt_model.statistics.snow_sca_value(cids, i)
        assert 0.0 <= opt_sca_v <= 1.0

    assert opt_sum_discharge is not None
    assert opt_swe is not None
    assert opt_sca is not None
    optimizer = opt_model_type.optimizer_t(opt_model)  # notice that a model type know it's optimizer type, e.g. PTGSKOptimizer
    assert optimizer is not None

    class MyNotify:
        """
        Example how you can connect your python code
        to the optimizer notify that is called
        at the end of each goal function evaluation.

        """

        def __init__(self, optim):
            self.optimizer = optim  # keep this around so we can use it in cb()
            self.count: int = 0
            self.p: pt_gs_k.PTGSKParameter = None
            self.v: float = -10000.0

        def cb(self) -> bool:
            self.count = self.count + 1
            i = self.optimizer.trace_size - 1  # pick last parameter used in goal fx eval
            self.p = self.optimizer.trace_parameter(i)  # you can pull out params
            self.v = self.optimizer.trace_goal_function_value(i)  # and goal value
            # print(f'gf={self.v}') print works ok
            return True  # ensure to let calibration continue, if we return False it stops

    notify = MyNotify(optimizer)  # create a context that is useful
    optimizer.notify_cb = notify.cb  # then ensure our cb is in place
    #
    # create target specification
    #
    opt_model.revert_to_initial_state()  # set_states(s0)  # remember to set the s0 again, so we have the same initial condition for our game
    tsa = api.TsTransform().to_average(t0, dt, n, sum_discharge)
    t_spec_1 = api.TargetSpecificationPts(tsa, cids, 1.0, api.KLING_GUPTA, 1.0, 0.0, 0.0, api.DISCHARGE, 'test_uid')

    target_spec = api.TargetSpecificationVector()
    target_spec.append(t_spec_1)
    upper_bound = model_type.parameter_t(model.get_region_parameter())  # the model_type know it's parameter_t
    lower_bound = model_type.parameter_t(model.get_region_parameter())
    upper_bound.kirchner.c1 = -1.9
    lower_bound.kirchner.c1 = -3.0
    upper_bound.kirchner.c2 = 0.99
    lower_bound.kirchner.c2 = 0.80

    optimizer.set_target_specification(target_spec, lower_bound, upper_bound)
    # Not needed, it will automatically get one.
    # optimizer.establish_initial_state_from_model()
    # s0_0 = optimizer.get_initial_state(0)
    # optimizer.set_verbose_level(1000)
    p0 = model_type.parameter_t(model.get_region_parameter())
    orig_c1 = p0.kirchner.c1
    orig_c2 = p0.kirchner.c2
    # model.get_cells()[0].env_ts.precipitation.set(0, 5.1)
    # model.get_cells()[0].env_ts.precipitation.set(1, 4.9)
    goal_f0 = optimizer.calculate_goal_function(p0)

    p0.kirchner.c1 = -2.4
    p0.kirchner.c2 = 0.91
    opt_param = optimizer.optimize(p0, 1500, 0.1, 1e-5)
    goal_fx = optimizer.calculate_goal_function(opt_param)
    p0.kirchner.c1 = -2.4
    p0.kirchner.c2 = 0.91
    # goal_fx1 = optimizer.calculate_goal_function(p0)
    # At this point verify that the notify mechanism actually works
    assert notify.count > 4
    assert notify.p
    assert notify.v > -10000.0
    optimizer.notify = None  # if not using notify, clear it out

    assert goal_fx <= 10.0
    assert round(abs(orig_c1 - opt_param.kirchner.c1), 4) == 0
    assert round(abs(orig_c2 - opt_param.kirchner.c2), 4) == 0
    # verify the interface to the new optimize_global function
    global_opt_param = optimizer.optimize_global(p0, max_n_evaluations=1500, max_seconds=3.0, solver_eps=1e-5)
    assert global_opt_param is not None  # just to ensure signature and results are covered

def test_rpmgsk_model_run():
    num_cells = 20
    model_type = r_pm_gs_k.RPMGSKModel
    opt_model_type = r_pm_gs_k.RPMGSKOptModel
    model = build_model(model_type, r_pm_gs_k.RPMGSKParameter, num_cells)
    assert model.size() == num_cells
    verify_state_handler(model)
    # demo of feature for threads
    assert model.ncore >= 1  # defaults to hardware concurrency
    model.ncore = 4  # set it to 4, and
    assert model.ncore == 4  # verify it works

    # now modify snow_cv forest_factor to 0.1
    region_parameter = model.get_region_parameter()
    region_parameter.gs.snow_cv_forest_factor = 0.1
    region_parameter.gs.snow_cv_altitude_factor = 0.0001
    assert region_parameter.gs.snow_cv_forest_factor == 0.1
    assert region_parameter.gs.snow_cv_altitude_factor == 0.0001

    assert round(abs(region_parameter.gs.effective_snow_cv(1.0, 0.0) - (region_parameter.gs.snow_cv + 0.1)), 7) == 0
    assert round(abs(region_parameter.gs.effective_snow_cv(1.0, 1000.0) - (region_parameter.gs.snow_cv + 0.1 + 0.1)), 7) == 0
    cal = Calendar()
    t0 = cal.time(2015, 1, 1, 0, 0, 0)
    dt = deltahours(1)
    n = 240
    time_axis = TimeAxisFixedDeltaT(t0, dt, n)
    model_interpolation_parameter = api.InterpolationParameter()
    model.initialize_cell_environment(time_axis)  # just show how we can split the run_interpolation into two calls(second one optional)
    # degC/m, so -0.5 degC/100m
    model_interpolation_parameter.temperature_idw.default_temp_gradient = -0.005
    # if possible use closest neighbor points and solve gradient using equation,(otherwise default min/max height)
    model_interpolation_parameter.temperature_idw.gradient_by_equation = True
    # Max number of temperature sources used for one interpolation
    model_interpolation_parameter.temperature_idw.max_members = 6
    # 20 km is max distance
    model_interpolation_parameter.temperature_idw.max_distance = 20000
    # zscale is used to discriminate neighbors at different elevation than target point
    assert round(abs(model_interpolation_parameter.temperature_idw.zscale - 1.0), 7) == 0
    model_interpolation_parameter.temperature_idw.zscale = 0.5
    assert round(abs(model_interpolation_parameter.temperature_idw.zscale - 0.5), 7) == 0
    # Pure linear interpolation
    model_interpolation_parameter.temperature_idw.distance_measure_factor = 1.0
    # This enables IDW with default temperature gradient.
    model_interpolation_parameter.use_idw_for_temperature = True
    assert round(abs(model_interpolation_parameter.precipitation.scale_factor - 1.02), 7) == 0  # just verify this one is as before change to scale_factor
    model.initialize_cell_environment(
        time_axis)  # just show how we can split the run_interpolation into two calls(second one optional)
    model.interpolate(
        model_interpolation_parameter,
        _create_dummy_region_environment(time_axis,
                                         model.get_cells()[int(num_cells/2)].geo.mid_point()))
    m_ip_parameter = model.interpolation_parameter  # illustrate that we can get back the passed interpolation parameter as a property of the model
    assert m_ip_parameter.use_idw_for_temperature == \
           True  # just to ensure we really did get back what we passed in
    assert round(abs(m_ip_parameter.temperature_idw.zscale - 0.5), 7) == 0

    s0 = r_pm_gs_k.RPMGSKStateVector()
    for i in range(num_cells):
        si = r_pm_gs_k.RPMGSKState()
        si.kirchner.q = 40.0
        s0.append(si)
    model.set_snow_sca_swe_collection(-1, True)
    model.set_states(s0)  # at this point the intial state of model is established as well
    model.run_cells()
    cids = IntVector.from_numpy([1])  # optional, we can add selective catchment_ids here
    sum_discharge = model.statistics.discharge(cids)
    sum_discharge_value = model.statistics.discharge_value(cids, 0)  # at the first timestep
    assert sum_discharge_value >= 130.0
    sum_charge = model.statistics.charge(cids)
    sum_charge_value = model.statistics.charge_value(cids, 0)
    print(sum_charge_value)
    print(sum_charge.values[0])
    assert round(abs(sum_charge_value - -110.6841 + 31.15 + 0.08), 2) == 0
    assert round(abs(sum_charge.values[0] - sum_charge_value), 5) == 0
    cell_charge = model.statistics.charge_value(IntVector([0, 1, 3]), 0, ix_type=api.stat_scope.cell)
    assert round(abs(cell_charge - -16.7138 + 4.66 +0.02), 2) == 0
    charge_sum_1_2_6 = model.statistics.charge(IntVector([1, 2, 6]),
                                               ix_type=api.stat_scope.cell).values.to_numpy().sum()
    # assertAlmostEqual(charge_sum_1_2_6, 107.3981, places=2)
    ae_output = model.actual_evaptranspiration_response.output(cids)
    ae_pot_ratio = model.actual_evaptranspiration_response.pot_ratio(cids)
    assert ae_output is not None
    # assertAlmostEqual(ae_output.values.to_numpy().max(), 0.189214067680088)
    assert ae_pot_ratio is not None  # this one is empty
    # assertAlmostEqual(ae_pot_ratio.values.to_numpy().min(), 0.9995599424191931)
    # assertAlmostEqual(ae_pot_ratio.values.to_numpy().max(), 1.0)
    # verify we can construct an optimizer
    opt_model = model.create_opt_model_clone()
    opt_model.set_snow_sca_swe_collection(-1, True)  # ensure to fill in swe/sca
    opt_model.run_cells()
    opt_sum_discharge = opt_model.statistics.discharge(cids)
    opt_swe = opt_model.statistics.snow_swe(cids)  # how to get out swe/sca
    opt_swe_v = opt_model.statistics.snow_swe_value(cids, 0)
    opt_sca = opt_model.statistics.snow_sca(cids)

    for i in range(len(opt_model.time_axis)):
        opt_sca_v = opt_model.statistics.snow_sca_value(cids, i)
        assert 0.0 <= opt_sca_v <= 1.0

    assert opt_sum_discharge is not None
    assert opt_swe is not None
    assert opt_sca is not None
    optimizer = opt_model_type.optimizer_t(opt_model)  # notice that a model type know it's optimizer type, e.g. PTGSKOptimizer
    assert optimizer is not None
    #
    # create target specification
    #
    opt_model.revert_to_initial_state()  # set_states(s0)  # remember to set the s0 again, so we have the same initial condition for our game
    tsa = api.TsTransform().to_average(t0, dt, n, sum_discharge)
    t_spec_1 = api.TargetSpecificationPts(tsa, cids, 1.0, api.KLING_GUPTA, 1.0, 0.0, 0.0, api.DISCHARGE, 'test_uid')

    target_spec = api.TargetSpecificationVector()
    target_spec.append(t_spec_1)
    upper_bound = model_type.parameter_t(model.get_region_parameter())  # the model_type know it's parameter_t
    lower_bound = model_type.parameter_t(model.get_region_parameter())
    upper_bound.kirchner.c1 = -1.9
    lower_bound.kirchner.c1 = -3.0
    upper_bound.kirchner.c2 = 0.99
    lower_bound.kirchner.c2 = 0.80

    optimizer.set_target_specification(target_spec, lower_bound, upper_bound)
    # Not needed, it will automatically get one.
    # optimizer.establish_initial_state_from_model()
    # s0_0 = optimizer.get_initial_state(0)
    # optimizer.set_verbose_level(1000)
    p0 = model_type.parameter_t(model.get_region_parameter())
    orig_c1 = p0.kirchner.c1
    orig_c2 = p0.kirchner.c2
    # model.get_cells()[0].env_ts.precipitation.set(0, 5.1)
    # model.get_cells()[0].env_ts.precipitation.set(1, 4.9)
    goal_f0 = optimizer.calculate_goal_function(p0)
    p0.kirchner.c1 = -2.4
    p0.kirchner.c2 = 0.91
    opt_param = optimizer.optimize(p0, 1500, 0.1, 1e-5)
    goal_fx = optimizer.calculate_goal_function(opt_param)
    p0.kirchner.c1 = -2.4
    p0.kirchner.c2 = 0.91
    # goal_fx1 = optimizer.calculate_goal_function(p0)

    assert goal_fx <= 10.0
    assert round(abs(orig_c1 - opt_param.kirchner.c1), 4) == 0
    assert round(abs(orig_c2 - opt_param.kirchner.c2), 4) == 0
    # verify the interface to the new optimize_global function
    global_opt_param = optimizer.optimize_global(p0, max_n_evaluations=1500, max_seconds=3.0, solver_eps=1e-5)
    assert global_opt_param is not None  # just to ensure signature and results are covered


def test_rptgsk_model_run():
    num_cells = 20
    model_type = r_pt_gs_k.RPTGSKModel
    opt_model_type = r_pt_gs_k.RPTGSKOptModel
    model = build_model(model_type, r_pt_gs_k.RPTGSKParameter, num_cells)
    assert model.size() == num_cells
    verify_state_handler(model)
    # demo of feature for threads
    assert model.ncore >= 1  # defaults to hardware concurrency
    model.ncore = 4  # set it to 4, and
    assert model.ncore == 4  # verify it works

    # now modify snow_cv forest_factor to 0.1
    region_parameter = model.get_region_parameter()
    region_parameter.gs.snow_cv_forest_factor = 0.1
    region_parameter.gs.snow_cv_altitude_factor = 0.0001
    assert region_parameter.gs.snow_cv_forest_factor == 0.1
    assert region_parameter.gs.snow_cv_altitude_factor == 0.0001

    assert round(abs(region_parameter.gs.effective_snow_cv(1.0, 0.0) - (region_parameter.gs.snow_cv + 0.1)), 7) == 0
    assert round(abs(region_parameter.gs.effective_snow_cv(1.0, 1000.0) - (region_parameter.gs.snow_cv + 0.1 + 0.1)), 7) == 0
    cal = Calendar()
    t0 = cal.time(2015, 1, 1, 0, 0, 0)
    dt = deltahours(1)
    n = 240
    time_axis = TimeAxisFixedDeltaT(t0, dt, n)
    model_interpolation_parameter = api.InterpolationParameter()
    model.initialize_cell_environment(time_axis)  # just show how we can split the run_interpolation into two calls(second one optional)
    # degC/m, so -0.5 degC/100m
    model_interpolation_parameter.temperature_idw.default_temp_gradient = -0.005
    # if possible use closest neighbor points and solve gradient using equation,(otherwise default min/max height)
    model_interpolation_parameter.temperature_idw.gradient_by_equation = True
    # Max number of temperature sources used for one interpolation
    model_interpolation_parameter.temperature_idw.max_members = 6
    # 20 km is max distance
    model_interpolation_parameter.temperature_idw.max_distance = 20000
    # zscale is used to discriminate neighbors at different elevation than target point
    assert round(abs(model_interpolation_parameter.temperature_idw.zscale - 1.0), 7) == 0
    model_interpolation_parameter.temperature_idw.zscale = 0.5
    assert round(abs(model_interpolation_parameter.temperature_idw.zscale - 0.5), 7) == 0
    # Pure linear interpolation
    model_interpolation_parameter.temperature_idw.distance_measure_factor = 1.0
    # This enables IDW with default temperature gradient.
    model_interpolation_parameter.use_idw_for_temperature = True
    assert round(abs(model_interpolation_parameter.precipitation.scale_factor - 1.02), 7) == 0  # just verify this one is as before change to scale_factor
    model.initialize_cell_environment(
        time_axis)  # just show how we can split the run_interpolation into two calls(second one optional)
    model.interpolate(
        model_interpolation_parameter,
        _create_dummy_region_environment(time_axis,
                                         model.get_cells()[int(num_cells/2)].geo.mid_point()))
    m_ip_parameter = model.interpolation_parameter  # illustrate that we can get back the passed interpolation parameter as a property of the model
    assert m_ip_parameter.use_idw_for_temperature == \
           True  # just to ensure we really did get back what we passed in
    assert round(abs(m_ip_parameter.temperature_idw.zscale - 0.5), 7) == 0

    s0 = r_pt_gs_k.RPTGSKStateVector()
    for i in range(num_cells):
        si = r_pt_gs_k.RPTGSKState()
        si.kirchner.q = 40.0
        s0.append(si)
    model.set_snow_sca_swe_collection(-1, True)
    model.set_states(s0)  # at this point the intial state of model is established as well
    model.run_cells()

    cids = IntVector.from_numpy([1])  # optional, we can add selective catchment_ids here
    model.actual_evaptranspiration_response.output(cids)
    print(model.actual_evaptranspiration_response.output(cids))
    model.priestley_taylor_response.output(cids)
    print(model.priestley_taylor_response.output(cids))
    print(model.priestley_taylor_response.output([1]))
    sum_discharge = model.statistics.discharge(cids)
    sum_discharge_value = model.statistics.discharge_value(cids, 0)  # at the first timestep
    assert sum_discharge_value >= 130.0
    sum_charge = model.statistics.charge(cids)
    sum_charge_value = model.statistics.charge_value(cids, 0)
    # print(sum_charge_value)
    # print(sum_charge.values[0])
    assert round(abs(sum_charge_value - -110.6736 + 31.23), 2) == 0
    assert round(abs(sum_charge.values[0] - sum_charge_value), 5) == 0
    cell_charge = model.statistics.charge_value(IntVector([0, 1, 3]), 0, ix_type=api.stat_scope.cell)
    assert round(abs(cell_charge - -16.7138 + 4.67), 2) == 0
    charge_sum_1_2_6 = model.statistics.charge(IntVector([1, 2, 6]),
                                               ix_type=api.stat_scope.cell).values.to_numpy().sum()
    # assertAlmostEqual(charge_sum_1_2_6, 107.3981, places=2)
    ae_output = model.actual_evaptranspiration_response.output(cids)
    ae_pot_ratio = model.actual_evaptranspiration_response.pot_ratio(cids)
    assert ae_output is not None
    # assertAlmostEqual(ae_output.values.to_numpy().max(), 0.189214067680088)
    assert ae_pot_ratio is not None  # this one is empty
    # assertAlmostEqual(ae_pot_ratio.values.to_numpy().min(), 0.9995599424191931)
    # assertAlmostEqual(ae_pot_ratio.values.to_numpy().max(), 1.0)
    # radiation model output
    rad_output = model.radiation_response.output(cids)
    assert rad_output is not None
    print(rad_output.values[0])
    # verify we can construct an optimizer
    opt_model = model.create_opt_model_clone()
    opt_model.set_snow_sca_swe_collection(-1, True)  # ensure to fill in swe/sca
    opt_model.run_cells()
    opt_sum_discharge = opt_model.statistics.discharge(cids)
    opt_swe = opt_model.statistics.snow_swe(cids)  # how to get out swe/sca
    opt_swe_v = opt_model.statistics.snow_swe_value(cids, 0)
    opt_sca = opt_model.statistics.snow_sca(cids)

    for i in range(len(opt_model.time_axis)):
        opt_sca_v = opt_model.statistics.snow_sca_value(cids, i)
        assert 0.0 <= opt_sca_v <= 1.0

    assert opt_sum_discharge is not None
    assert opt_swe is not None
    assert opt_sca is not None
    optimizer = opt_model_type.optimizer_t(opt_model)  # notice that a model type know it's optimizer type, e.g. PTGSKOptimizer
    assert optimizer is not None
    #
    # create target specification
    #
    opt_model.revert_to_initial_state()  # set_states(s0)  # remember to set the s0 again, so we have the same initial condition for our game
    tsa = api.TsTransform().to_average(t0, dt, n, sum_discharge)
    t_spec_1 = api.TargetSpecificationPts(tsa, cids, 1.0, api.KLING_GUPTA, 1.0, 0.0, 0.0, api.DISCHARGE, 'test_uid')

    target_spec = api.TargetSpecificationVector()
    target_spec.append(t_spec_1)
    upper_bound = model_type.parameter_t(model.get_region_parameter())  # the model_type know it's parameter_t
    lower_bound = model_type.parameter_t(model.get_region_parameter())
    upper_bound.kirchner.c1 = -1.9
    lower_bound.kirchner.c1 = -3.0
    upper_bound.kirchner.c2 = 0.99
    lower_bound.kirchner.c2 = 0.80

    optimizer.set_target_specification(target_spec, lower_bound, upper_bound)
    # Not needed, it will automatically get one.
    # optimizer.establish_initial_state_from_model()
    # s0_0 = optimizer.get_initial_state(0)
    # optimizer.set_verbose_level(1000)
    p0 = model_type.parameter_t(model.get_region_parameter())
    orig_c1 = p0.kirchner.c1
    orig_c2 = p0.kirchner.c2
    # model.get_cells()[0].env_ts.precipitation.set(0, 5.1)
    # model.get_cells()[0].env_ts.precipitation.set(1, 4.9)
    goal_f0 = optimizer.calculate_goal_function(p0)
    p0.kirchner.c1 = -2.4
    p0.kirchner.c2 = 0.91
    opt_param = optimizer.optimize(p0, 1500, 0.1, 1e-5)
    goal_fx = optimizer.calculate_goal_function(opt_param)
    p0.kirchner.c1 = -2.4
    p0.kirchner.c2 = 0.91
    # goal_fx1 = optimizer.calculate_goal_function(p0)

    assert goal_fx <= 10.0
    assert round(abs(orig_c1 - opt_param.kirchner.c1), 4) == 0
    assert round(abs(orig_c2 - opt_param.kirchner.c2), 4) == 0
    # verify the interface to the new optimize_global function
    global_opt_param = optimizer.optimize_global(p0, max_n_evaluations=1500, max_seconds=3.0, solver_eps=1e-5)
    assert global_opt_param is not None  # just to ensure signature and results are covered


def test_pt_ss_k_model_initialize_and_run():
    num_cells = 20
    model_type = pt_ss_k.PTSSKModel
    model = build_model(model_type, pt_ss_k.PTSSKParameter, num_cells, num_catchments=1)
    assert model.size() == num_cells
    verify_state_handler(model)
    # demo of feature for threads
    assert model.ncore >= 1  # defaults to hardware concurrency
    model.ncore = 4  # set it to 4, and
    assert model.ncore == 4  # verify it works

    # now modify snow_cv forest_factor to 0.1
    region_parameter = model.get_region_parameter()
    cal = Calendar()
    time_axis = TimeAxisFixedDeltaT(cal.time(2015, 1, 1, 0, 0, 0), deltahours(1), 240)
    model_interpolation_parameter = api.InterpolationParameter()
    model_interpolation_parameter.temperature_idw.default_temp_gradient = -0.005  # degC/m, so -0.5 degC/100m
    # if possible use closest neighbor points and solve gradient using equation,(otherwise default min/max height)
    model_interpolation_parameter.temperature_idw.gradient_by_equation = True
    # Max number of temperature sources used for one interpolation
    model_interpolation_parameter.temperature_idw.max_members = 6
    # 20 km is max distance
    model_interpolation_parameter.temperature_idw.max_distance = 20000
    # zscale is used to discriminate neighbors at different elevation than target point
    assert round(abs(model_interpolation_parameter.temperature_idw.zscale - 1.0), 7) == 0
    model_interpolation_parameter.temperature_idw.zscale = 0.5
    assert round(abs(model_interpolation_parameter.temperature_idw.zscale - 0.5), 7) == 0
    # Pure linear interpolation
    model_interpolation_parameter.temperature_idw.distance_measure_factor = 1.0
    # This enables IDW with default temperature gradient.
    model_interpolation_parameter.use_idw_for_temperature = True
    assert round(abs(model_interpolation_parameter.precipitation.scale_factor - 1.02), 7) == 0  # just verify this one is as before change to scale_factor
    model.initialize_cell_environment(time_axis)  # just show how we can split the run_interpolation into two calls(second one optional)
    re = _create_dummy_region_environment(time_axis,
                                          model.get_cells()[int(num_cells/2)].geo.mid_point(), temperature=-1.0)
    wanted_temperature_ts = TimeSeries(time_axis, DoubleVector.from_numpy(np.linspace(-1, 20, len(time_axis))), POINT_AVERAGE_VALUE)
    re.temperature[0].ts = wanted_temperature_ts
    model.interpolate(model_interpolation_parameter, re)
    m_ip_parameter = model.interpolation_parameter  # illustrate that we can get back the passed interpolation parameter as a property of the model
    assert m_ip_parameter.use_idw_for_temperature == True  # just to ensure we really did get back what we passed in
    assert round(abs(m_ip_parameter.temperature_idw.zscale - 0.5), 7) == 0
    #
    # Section to demo that we can ensure that model.cells[].env_ts.xxx
    # have finite-values only
    #
    for env_ts_x in [model.cells[0].env_ts.temperature,
                     model.cells[0].env_ts.precipitation,
                     model.cells[0].env_ts.rel_hum,
                     model.cells[0].env_ts.radiation,
                     model.cells[0].env_ts.wind_speed]:
        assert model.is_cell_env_ts_ok()  # demon how to verify that cell.env_ts can be checked prior to run

    s0 = pt_ss_k.PTSSKStateVector()
    for i in range(num_cells):
        si = pt_ss_k.PTSSKState()
        si.kirchner.q = 40.0
        s0.append(si)
    model.set_states(s0)
    model.set_state_collection(-1, True)  # enable state collection for all cells
    model2 = model_type(model)  # make a copy, so that we in the stepwise run below get a clean copy with all values zero.
    opt_model = pt_ss_k.create_opt_model_clone(model)  # this is how to make a model suitable for optimizer
    model.run_cells()  # the default arguments applies: thread_cell_count=0,start_step=0,n_steps=0)
    cids = IntVector()  # optional, we can add selective catchment_ids here
    sum_discharge = model.statistics.discharge(cids)
    sum_discharge_value = model.statistics.discharge_value(cids, 0)  # at the first timestep
    sum_charge = model.statistics.charge(cids)
    sum_charge_value = model.statistics.charge_value(cids, 0)
    assert round(abs(sum_charge_value - -107.7700344649026 + 30.42 + 0.13), 2) == 0
    assert round(abs(sum_charge.values[0] - sum_charge_value), 5) == 0
    cell_charge = model.statistics.charge_value(IntVector([0, 1, 3]), 0, ix_type=api.stat_scope.cell)
    assert cell_charge == pytest.approx(-21.055426571440783)
    charge_sum_1_2_6 = model.statistics.charge(IntVector([1, 2, 6]), ix_type=api.stat_scope.cell).values.to_numpy().sum()
    assert charge_sum_1_2_6  == pytest.approx(-52.67224831094702)
    ae_output = model.actual_evaptranspiration_response.output(cids)
    ae_pot_ratio = model.actual_evaptranspiration_response.pot_ratio(cids)
    assert ae_output is not None
    assert round(abs(ae_output.values.to_numpy().max() - 0.2748795578815271), 7) == 0
    assert ae_pot_ratio is not None
    assert round(abs(ae_pot_ratio.values.to_numpy().min() - 0.9244990947328042), 7) == 0
    assert round(abs(ae_pot_ratio.values.to_numpy().max() - 1.0), 7) == 0
    opt_model.run_cells()  # starting out with the same state, same interpolated values, and region-parameters, we should get same results
    sum_discharge_opt_value = opt_model.statistics.discharge_value(cids, 0)
    assert round(abs(sum_discharge_opt_value - sum_discharge_value), 3) == 0  # verify the opt_model clone gives same value
    assert sum_discharge_value >= 130.0
    opt_model.region_env.temperature[0].ts.set(0, 23.2)  # verify that region-env is different (no aliasing, a true copy is required)
    assert not (abs(model.region_env.temperature[0].ts.value(0) - opt_model.region_env.temperature[0].ts.value(0)) > 0.5)

    #
    # check values
    #
    assert sum_discharge is not None
    # now, re-run the process in 24-hours steps x 10
    model.set_states(s0)  # restore state s0
    assert s0.size() == model.initial_state.size()
    for do_collect_state in [False, True]:
        model2.set_state_collection(-1, do_collect_state)  # issue reported by Yisak, prior to 21.3, this would crash
        model2.set_states(s0)
        # now  after fix, it works Ok
        for section in range(10):
            model2.run_cells(use_ncore=0, start_step=section*24, n_steps=24)
            section_discharge = model2.statistics.discharge(cids)
            assert section_discharge.size() == sum_discharge.size()  # notice here that the values after current step are 0.0
    stepwise_sum_discharge = model2.statistics.discharge(cids)
    # assert stepwise_sum_discharge == sum_discharge
    diff_ts = sum_discharge.values.to_numpy() - stepwise_sum_discharge.values.to_numpy()
    assert round(abs((diff_ts*diff_ts).max() - 0.0), 4) == 0
    # Verify that if we pass in illegal cids, then it raises exception(with first failing
    try:
        illegal_cids = IntVector([0, 4, 5])
        model.statistics.discharge(illegal_cids)
        assert not True, "Failed test, using illegal cids should raise exception"
    except RuntimeError as rte:
        pass

    avg_temperature = model.statistics.temperature(cids)
    avg_precipitation = model.statistics.precipitation(cids)
    assert avg_precipitation is not None
    for time_step in range(time_axis.size()):
        precip_raster = model.statistics.precipitation(cids, time_step)  # example raster output
        assert precip_raster.size() == num_cells
    # example single value spatial aggregation (area-weighted) over cids for a specific timestep
    avg_gs_sc_value = model.skaugen_snow_response.sca_value(cids, 1)
    assert avg_gs_sc_value >= 0.0
    avg_gs_sca = model.skaugen_snow_response.sca(cids)
    assert avg_gs_sca is not None
    avg_swe_value = model.skaugen_snow_response.swe_value(cids, 1)
    assert avg_swe_value >= 0.0
    avg_swe = model.skaugen_snow_response.swe(cids)
    assert avg_swe is not None
    assert avg_swe.values.to_numpy().max() < 60.0

    assert avg_temperature.size() == time_axis.size(), "expect results equal to time-axis size"
    copy_region_model = model.__class__(model)
    assert copy_region_model is not None
    copy_region_model.run_cells()  # just to verify we can copy and run the new model


def test_pt_st_k_model_initialize_and_run():
    num_cells = 20
    model_type = pt_st_k.PTSTKModel
    model = build_model(model_type, pt_st_k.PTSTKParameter, num_cells, num_catchments=1)
    assert model.size() == num_cells
    verify_state_handler(model)
    # demo of feature for threads
    assert model.ncore >= 1  # defaults to hardware concurrency
    model.ncore = 4  # set it to 4, and
    assert model.ncore == 4  # verify it works

    # now modify snow_cv forest_factor to 0.1
    region_parameter = model.get_region_parameter()
    cal = Calendar()
    time_axis = TimeAxisFixedDeltaT(cal.time(2015, 1, 1, 0, 0, 0), deltahours(1), 240)
    model_interpolation_parameter = api.InterpolationParameter()
    model_interpolation_parameter.temperature_idw.default_temp_gradient = -0.005  # degC/m, so -0.5 degC/100m
    # if possible use closest neighbor points and solve gradient using equation,(otherwise default min/max height)
    model_interpolation_parameter.temperature_idw.gradient_by_equation = True
    # Max number of temperature sources used for one interpolation
    model_interpolation_parameter.temperature_idw.max_members = 6
    # 20 km is max distance
    model_interpolation_parameter.temperature_idw.max_distance = 20000
    # zscale is used to discriminate neighbors at different elevation than target point
    assert round(abs(model_interpolation_parameter.temperature_idw.zscale - 1.0), 7) == 0
    model_interpolation_parameter.temperature_idw.zscale = 0.5
    assert round(abs(model_interpolation_parameter.temperature_idw.zscale - 0.5), 7) == 0
    # Pure linear interpolation
    model_interpolation_parameter.temperature_idw.distance_measure_factor = 1.0
    # This enables IDW with default temperature gradient.
    model_interpolation_parameter.use_idw_for_temperature = True
    assert round(abs(model_interpolation_parameter.precipitation.scale_factor - 1.02), 7) == 0  # just verify this one is as before change to scale_factor
    model.initialize_cell_environment(time_axis)  # just show how we can split the run_interpolation into two calls(second one optional)
    re = _create_dummy_region_environment(time_axis,
                                          model.get_cells()[int(num_cells/2)].geo.mid_point(), temperature=-1.0)
    wanted_temperature_ts = TimeSeries(time_axis, DoubleVector.from_numpy(np.linspace(-1, 20, len(time_axis))), POINT_AVERAGE_VALUE)
    re.temperature[0].ts = wanted_temperature_ts
    model.interpolate(model_interpolation_parameter, re)
    m_ip_parameter = model.interpolation_parameter  # illustrate that we can get back the passed interpolation parameter as a property of the model
    assert m_ip_parameter.use_idw_for_temperature  # just to ensure we really did get back what we passed in
    assert round(abs(m_ip_parameter.temperature_idw.zscale - 0.5), 7) == 0
    #
    # Section to demo that we can ensure that model.cells[].env_ts.xxx
    # have finite-values only
    #
    for env_ts_x in [model.cells[0].env_ts.temperature,
                     model.cells[0].env_ts.precipitation,
                     model.cells[0].env_ts.rel_hum,
                     model.cells[0].env_ts.radiation,
                     model.cells[0].env_ts.wind_speed]:
        assert model.is_cell_env_ts_ok()  # demon how to verify that cell.env_ts can be checked prior to run

    s0 = pt_st_k.PTSTKStateVector()
    for i in range(num_cells):
        si = pt_st_k.PTSTKState()
        si.kirchner.q = 40.0
        s0.append(si)
    model.set_states(s0)
    model.set_state_collection(-1, True)  # enable state collection for all cells
    model2 = model_type(model)  # make a copy, so that we in the stepwise run below get a clean copy with all values zero.
    opt_model = pt_st_k.create_opt_model_clone(model)  # this is how to make a model suitable for optimizer
    model.run_cells()  # the default arguments applies: thread_cell_count=0,start_step=0,n_steps=0)
    cids = IntVector()  # optional, we can add selective catchment_ids here
    #
    # Verify the cell.sc.snow_swe, and snow_sca time-shifted one step vs. resource collector
    # that keep representative values for the time-step.
    #
    c0_rc_swe = model.cells[0].rc.snow_swe
    c0_rc_sca = model.cells[0].rc.snow_sca
    c0_sc_swe = model.cells[0].sc.snow_swe
    c0_sc_sca = model.cells[0].sc.snow_sca
    assert (c0_sc_sca.time_shift(-3600) - TimeSeries(c0_rc_sca)).values.to_numpy().sum() < 1e-10
    assert (c0_sc_swe.time_shift(-3600) - TimeSeries(c0_rc_swe)).values.to_numpy().sum() < 1e-10

    sum_discharge = model.statistics.discharge(cids)
    sum_discharge_value = model.statistics.discharge_value(cids, 0)  # at the first timestep
    sum_charge = model.statistics.charge(cids)
    sum_charge_value = model.statistics.charge_value(cids, 0)
    assert round(abs(sum_charge_value - -107.78744148265199 + 30.43 + 0.12), 2) == 0
    assert round(abs(sum_charge.values[0] - sum_charge_value), 5) == 0
    cell_charge = model.statistics.charge_value(IntVector([0, 1, 3]), 0, ix_type=api.stat_scope.cell)
    assert round(abs(cell_charge - -16.417069591604303 + 4.59 + 0.02), 2) == 0
    charge_sum_1_2_6 = model.statistics.charge(IntVector([1, 2, 6]), ix_type=api.stat_scope.cell).values.to_numpy().sum()
    assert round(abs(charge_sum_1_2_6 - 106.60150830237195 + 151.52 + 7.49), 2) == 0
    ae_output = model.actual_evaptranspiration_response.output(cids)
    ae_pot_ratio = model.actual_evaptranspiration_response.pot_ratio(cids)
    assert ae_output is not None
    assert round(abs(ae_output.values.to_numpy().max() - 0.21562758689131353), 7) == 0
    assert ae_pot_ratio is not None
    assert round(abs(ae_pot_ratio.values.to_numpy().min() - 0.9650266943235941), 7) == 0
    assert round(abs(ae_pot_ratio.values.to_numpy().max() - 1.0), 7) == 0
    opt_model.run_cells()  # starting out with the same state, same interpolated values, and region-parameters, we should get same results
    sum_discharge_opt_value = opt_model.statistics.discharge_value(cids, 0)
    assert round(abs(sum_discharge_opt_value - sum_discharge_value), 3) == 0  # verify the opt_model clone gives same value
    assert sum_discharge_value >= 130.0
    opt_model.region_env.temperature[0].ts.set(0, 23.2)  # verify that region-env is different (no aliasing, a true copy is required)
    assert not (abs(model.region_env.temperature[0].ts.value(0) - opt_model.region_env.temperature[0].ts.value(0)) > 0.5)

    #
    # check values
    #
    assert sum_discharge is not None
    # now, re-run the process in 24-hours steps x 10
    model.set_states(s0)  # restore state s0
    assert s0.size() == model.initial_state.size()
    for do_collect_state in [False, True]:
        model2.set_state_collection(-1, do_collect_state)  # issue reported by Yisak, prior to 21.3, this would crash
        model2.set_states(s0)
        # now  after fix, it works Ok
        for section in range(10):
            model2.run_cells(use_ncore=0, start_step=section*24, n_steps=24)
            section_discharge = model2.statistics.discharge(cids)
            assert section_discharge.size() == sum_discharge.size()  # notice here that the values after current step are 0.0
    stepwise_sum_discharge = model2.statistics.discharge(cids)
    # assert stepwise_sum_discharge == sum_discharge
    diff_ts = sum_discharge.values.to_numpy() - stepwise_sum_discharge.values.to_numpy()
    assert round(abs((diff_ts*diff_ts).max() - 0.0), 4) == 0
    # Verify that if we pass in illegal cids, then it raises exception(with first failing
    try:
        illegal_cids = IntVector([0, 4, 5])
        model.statistics.discharge(illegal_cids)
        assert not True, "Failed test, using illegal cids should raise exception"
    except RuntimeError as rte:
        pass

    avg_temperature = model.statistics.temperature(cids)
    avg_precipitation = model.statistics.precipitation(cids)
    assert avg_precipitation is not None
    for time_step in range(time_axis.size()):
        precip_raster = model.statistics.precipitation(cids, time_step)  # example raster output
        assert precip_raster.size() == num_cells
    # example single value spatial aggregation (area-weighted) over cids for a specific timestep
    avg_gs_sc_value = model.snow_tiles_response.sca_value(cids, 1)
    assert avg_gs_sc_value >= 0.0
    avg_st_state_sca_value = model.snow_tiles_state.sca_value(cids, 1)
    assert avg_st_state_sca_value >= 0.0
    assert model.snow_tiles_state.swe(cids)  # ensure we hit the exposed state-collector time-series
    assert model.snow_tiles_state.sca(cids)
    avg_gs_sca = model.snow_tiles_response.sca(cids)
    assert avg_gs_sca is not None
    avg_swe_value = model.snow_tiles_response.swe_value(cids, 1)
    assert avg_swe_value >= 0.0
    avg_swe = model.snow_tiles_response.swe(cids)
    assert avg_swe is not None
    assert avg_swe.values.to_numpy().max() < 70.0

    assert avg_temperature.size() == time_axis.size(), "expect results equal to time-axis size"
    copy_region_model = model.__class__(model)
    assert copy_region_model is not None
    copy_region_model.run_cells()  # just to verify we can copy and run the new model


def test_pt_st_k_glacier_melt():
    num_cells = 1
    model_type = pt_st_k.PTSTKModel
    model = build_model(model_type, pt_st_k.PTSTKParameter, num_cells, num_catchments=1)

    # adapt land_type_fractions
    glacier = 0.5
    lake = 0.15
    reservoir = 0.25
    forest = 0.0
    unspecified = 0.1
    cell = model.cells[0]
    ltf = api.LandTypeFractions(glacier, lake, reservoir, forest, unspecified)
    cell.geo.set_land_type_fractions(ltf)

    # now modify snow_cv forest_factor to 0.1
    cal = Calendar()
    time_axis = TimeAxisFixedDeltaT(cal.time(2015, 1, 1, 0, 0, 0), deltahours(1), 240)
    model_interpolation_parameter = api.InterpolationParameter()
    model_interpolation_parameter.temperature_idw.default_temp_gradient = 0.0  # degC/m, so -0.5 degC/100m
    model.initialize_cell_environment(time_axis)  # just show how we can split the run_interpolation into two calls(second one optional)
    re = _create_dummy_region_environment(time_axis,
                                          model.get_cells()[int(num_cells/2)].geo.mid_point(), temperature=-1.0)
    wanted_temperature_ts = TimeSeries(ta=time_axis, fill_value=0.0, point_fx=POINT_AVERAGE_VALUE)  # only positive
    wanted_precip_ts = TimeSeries(ta=time_axis, fill_value=0, point_fx=POINT_AVERAGE_VALUE)  # only positive

    re.temperature[0].ts = wanted_temperature_ts
    re.precipitation[0].ts = wanted_precip_ts
    model.interpolate(model_interpolation_parameter, re)

    #
    # Section to demo that we can ensure that model.cells[].env_ts.xxx
    # have finite-values only
    #
    for env_ts_x in [model.cells[0].env_ts.temperature,
                     model.cells[0].env_ts.precipitation,
                     model.cells[0].env_ts.rel_hum,
                     model.cells[0].env_ts.radiation,
                     model.cells[0].env_ts.wind_speed]:
        assert model.is_cell_env_ts_ok()  # demon how to verify that cell.env_ts can be checked prior to run

    s0 = pt_st_k.PTSTKStateVector()
    for i in range(num_cells):
        si = pt_st_k.PTSTKState()
        si.snow.fw = [2., 2., 0., 0., 0., 0., 0., 0., 0., 0.]  # -> sca 0.2 over snow coverable area
        s0.append(si)
    model.set_states(s0)
    model.set_state_collection(-1, True)  # enable state collection for all cells
    cell = model.cells[0]
    cell.parameter.st.cx = 0.0
    # cell.parameter.st.lwmax=0.0
    model.run_cells()  # the default arguments applies: thread_cell_count=0,start_step=0,n_steps=0)
    glacier_outflow = cell.rc.glacier_melt.values.to_numpy()
    snow_outflow = cell.rc.snow_outflow.values.to_numpy()
    snow_sca = cell.rc.snow_sca.values.to_numpy()
    cell_sca = snow_sca*cell.geo.land_type_fractions_info().snow_storage()
    glacier_frac = cell.geo.land_type_fractions_info().glacier()
    bare_glacier_frac = glacier_frac - cell_sca
    gm_trg_mm_h = cell.env_ts.temperature.values.to_numpy()*cell.parameter.gm.dtf/24.*bare_glacier_frac
    gm_trg_m3_s = gm_trg_mm_h*cell.geo.area()/1000./3600.
    assert np.allclose(snow_outflow, 0.0)
    assert np.allclose(glacier_outflow, gm_trg_m3_s)


def test_pt_st_hbv_model_initialize_and_run():
    # TODO: parametize? a lot of the same logic for pt_st_k and pt_st_hbv
    num_cells = 20
    model_type = pt_st_hbv.PTSTHBVModel
    model = build_model(model_type, pt_st_hbv.PTSTHBVParameter, num_cells, num_catchments=1)
    assert model.size() == num_cells
    verify_state_handler(model)
    # demo of feature for threads
    assert model.ncore >= 1  # defaults to hardware concurrency
    model.ncore = 4  # set it to 4, and
    assert model.ncore == 4  # verify it works

    # now modify snow_cv forest_factor to 0.1
    region_parameter = model.get_region_parameter()
    cal = Calendar()
    time_axis = TimeAxisFixedDeltaT(cal.time(2015, 1, 1, 0, 0, 0), deltahours(1), 240)
    model_interpolation_parameter = api.InterpolationParameter()
    model_interpolation_parameter.temperature_idw.default_temp_gradient = -0.005  # degC/m, so -0.5 degC/100m
    # if possible use closest neighbor points and solve gradient using equation,(otherwise default min/max height)
    model_interpolation_parameter.temperature_idw.gradient_by_equation = True
    # Max number of temperature sources used for one interpolation
    model_interpolation_parameter.temperature_idw.max_members = 6
    # 20 km is max distance
    model_interpolation_parameter.temperature_idw.max_distance = 20000
    # zscale is used to discriminate neighbors at different elevation than target point
    assert round(abs(model_interpolation_parameter.temperature_idw.zscale - 1.0), 7) == 0
    model_interpolation_parameter.temperature_idw.zscale = 0.5
    assert round(abs(model_interpolation_parameter.temperature_idw.zscale - 0.5), 7) == 0
    # Pure linear interpolation
    model_interpolation_parameter.temperature_idw.distance_measure_factor = 1.0
    # This enables IDW with default temperature gradient.
    model_interpolation_parameter.use_idw_for_temperature = True
    assert round(abs(model_interpolation_parameter.precipitation.scale_factor - 1.02), 7) == 0  # just verify this one is as before change to scale_factor
    model.initialize_cell_environment(time_axis)  # just show how we can split the run_interpolation into two calls(second one optional)
    re = _create_dummy_region_environment(time_axis,
                                          model.get_cells()[int(num_cells/2)].geo.mid_point(), temperature=-1.0)
    wanted_temperature_ts = TimeSeries(time_axis, DoubleVector.from_numpy(np.linspace(-1, 20, len(time_axis))), POINT_AVERAGE_VALUE)
    re.temperature[0].ts = wanted_temperature_ts
    model.interpolate(model_interpolation_parameter, re)
    m_ip_parameter = model.interpolation_parameter  # illustrate that we can get back the passed interpolation parameter as a property of the model
    assert m_ip_parameter.use_idw_for_temperature  # just to ensure we really did get back what we passed in
    assert round(abs(m_ip_parameter.temperature_idw.zscale - 0.5), 7) == 0
    #
    # Section to demo that we can ensure that model.cells[].env_ts.xxx
    # have finite-values only
    #
    for env_ts_x in [model.cells[0].env_ts.temperature,
                     model.cells[0].env_ts.precipitation,
                     model.cells[0].env_ts.rel_hum,
                     model.cells[0].env_ts.radiation,
                     model.cells[0].env_ts.wind_speed]:
        assert model.is_cell_env_ts_ok()  # demon how to verify that cell.env_ts can be checked prior to run

    s0 = pt_st_hbv.PTSTHBVStateVector()
    for i in range(num_cells):
        si = pt_st_hbv.PTSTHBVState()
        si.soil.sm = 15.0
        s0.append(si)
    model.set_states(s0)
    model.set_state_collection(-1, True)  # enable state collection for all cells
    model2 = model_type(model)  # make a copy, so that we in the stepwise run below get a clean copy with all values zero.
    opt_model = pt_st_hbv.create_opt_model_clone(model)  # this is how to make a model suitable for optimizer
    model.run_cells()  # the default arguments applies: thread_cell_count=0,start_step=0,n_steps=0)
    cids = IntVector()  # optional, we can add selective catchment_ids here
    #
    # Verify the cell.sc.snow_swe, and snow_sca time-shifted one step vs. resource collector
    # that keep representative values for the time-step.
    #
    c0_rc_swe = model.cells[0].rc.snow_swe
    c0_rc_sca = model.cells[0].rc.snow_sca
    c0_sc_swe = model.cells[0].sc.snow_swe
    c0_sc_sca = model.cells[0].sc.snow_sca
    assert (c0_sc_sca.time_shift(-3600) - TimeSeries(c0_rc_sca)).values.to_numpy().sum() < 1e-10
    assert (c0_sc_swe.time_shift(-3600) - TimeSeries(c0_rc_swe)).values.to_numpy().sum() < 1e-10

    sum_discharge = model.statistics.discharge(cids)
    sum_discharge_value = model.statistics.discharge_value(cids, 0)  # at the first timestep
    assert sum_discharge_value > 0
    np.testing.assert_allclose(sum_discharge.v[0], sum_discharge_value)

    # brief regression testing
    np.testing.assert_allclose(sum_discharge_value, 11.011774234715283, atol=1e-5)
    np.testing.assert_allclose(model.soil_state.sm_value(cids,1), 307.7453559120004, atol=1e-5)
    np.testing.assert_allclose(model.tank_state.uz_value(cids,1), 182.5553437582326, atol=1e-5)
    np.testing.assert_allclose(model.tank_state.lz_value(cids,1), 205.57004883681907, atol=1e-5)


def test_geo_cell_data_serializer():
    """
    This test the bulding block for the geo-cell caching mechanism that can be
    implemented in GeoCell repository to cache complex information from the GIS system.
    The test illustrates how to convert existing cell-vector geo info into a DoubleVector,
    that can be converted .to_nump(),
    and then how to re-create the cell-vector,(of any given type actually) based on
    the geo-cell data DoubleVector (that can be created from .from_numpy(..)

    Notice that the from_numpy(np array) could have limited functionality when it comes
    to strides etc, so if problem flatten out the np.array before passing it.

    """
    n_cells = 3
    n_values_pr_gcd = 11  # number of values in a geo_cell_data stride
    model = build_model(pt_gs_k.PTGSKModel, pt_gs_k.PTGSKParameter, n_cells)
    cell_vector = model.get_cells()
    geo_cell_data_vector = cell_vector.geo_cell_data_vector(cell_vector)  # This gives a string, ultra fast, containing the serialized form of all geo-cell data
    assert len(geo_cell_data_vector) == n_values_pr_gcd*n_cells
    cell_vector2 = pt_gs_k.PTGSKCellAllVector.create_from_geo_cell_data_vector(
        geo_cell_data_vector)  # This gives a cell_vector, of specified type, with exactly the same geo-cell data as the original
    assert len(cell_vector) == len(cell_vector2)  # just verify equal size, and then geometry, the remaining tests are covered by C++ testing
    for i in range(len(cell_vector)):
        assert round(abs(cell_vector[i].geo.mid_point().z - cell_vector2[i].mid_point().z), 7) == 0
        assert round(abs(cell_vector[i].geo.mid_point().x - cell_vector2[i].mid_point().x), 7) == 0
        assert round(abs(cell_vector[i].geo.mid_point().y - cell_vector2[i].mid_point().y), 7) == 0


def test_model_catchment_ids():
    num_cells = 50
    model_type = pt_gs_k.PTGSKModel
    for n_cids in [1, 2, 3]:
        model = build_model(model_type, pt_gs_k.PTGSKParameter, num_cells, n_cids)
        cids = sorted(model.catchment_ids)
        assert len(cids) == n_cids
        for i in range(n_cids):
            assert i + 1 == cids[i]


def test_region_environment_variable_list():
    """ just to verify ARegionEnvironment.variables container exposed to ease scripting"""
    cal = Calendar()
    time_axis = TimeAxisFixedDeltaT(cal.time(2015, 1, 1, 0, 0, 0), deltahours(1), 240)
    e = _create_dummy_region_environment(time_axis, api.GeoPoint(0.0, 1.0, 2.0))
    assert e is not None
    assert len(e.variables) == 5
    for v in e.variables:
        assert v[1] is not None
        assert getattr(e, v[0])[0].mid_point_ == v[1][0].mid_point_  # equivalent, just check first midpoint
        assert getattr(e, v[0])[0].ts.value(0) == v[1][0].ts.value(0)  # and value


def test_state_with_id_handler():
    num_cells = 20
    model_type = pt_gs_k.PTGSKModel
    model = build_model(model_type, pt_gs_k.PTGSKParameter, num_cells, 2)
    cids_unspecified = IntVector()
    cids_1 = IntVector([1])
    cids_2 = IntVector([2])

    model_state_12 = model.state.extract_state(cids_unspecified)  # this is how to get all states from model
    model_state_1 = model.state.extract_state(cids_1)  # this is how to get only specified states from model
    model_state_2 = model.state.extract_state(cids_2)
    assert len(model_state_1) + len(model_state_2) == len(model_state_12)
    # We need to store state into yaml text files, for this its nice to have it as a string:
    ms2 = pt_gs_k.PTGSKStateWithIdVector.deserialize_from_str(model_state_2.serialize_to_str())  # [0] verify state serialization
    assert len(ms2) == len(model_state_2)
    for a, b in zip(ms2, model_state_2):
        assert a.id == b.id
        assert round(abs(a.state.kirchner.q - b.state.kirchner.q), 7) == 0

    assert len(model_state_1) > 0
    assert len(model_state_2) > 0
    for i in range(len(model_state_1)):  # verify selective extract catchment 1
        assert model_state_1[i].id.cid == 1
    for i in range(len(model_state_2)):  # verify selective extract catchment 2
        assert model_state_2[i].id.cid == 2
    for i in range(len(model_state_12)):
        model_state_12[i].state.kirchner.q = 100 + i
    model.state.apply_state(model_state_12, cids_unspecified)  # this is how to put all states into  model
    ms_12 = model.state.extract_state(cids_unspecified)
    for i in range(len(ms_12)):
        assert round(abs(ms_12[i].state.kirchner.q - (100 + i)), 7) == 0
    for i in range(len(model_state_2)):
        model_state_2[i].state.kirchner.q = 200 + i
    unapplied = model.state.apply_state(model_state_2, cids_2)  # this is how to put a limited set of state into model
    assert len(unapplied) == 0
    ms_12 = model.state.extract_state(cids_unspecified)
    for i in range(len(ms_12)):
        if ms_12[i].id.cid == 1:
            assert round(abs(ms_12[i].state.kirchner.q - (100 + i)), 7) == 0

    ms_2 = model.state.extract_state(cids_2)
    for i in range(len(ms_2)):
        assert round(abs(ms_2[i].state.kirchner.q - (200 + i)), 7) == 0

    # feature test: serialization support, to and from bytes
    #
    bytes = ms_2.serialize_to_bytes()  # first make some bytes out of the state
    with tempfile.TemporaryDirectory() as tmpdirname:
        file_path = str(path.join(tmpdirname, "pt_gs_k_state_test.bin"))
        byte_vector_to_file(file_path, bytes)  # stash it into a file
        bytes = byte_vector_from_file(file_path)  # get it back from the file and into ByteVector
    ms_2x = pt_gs_k.deserialize_from_bytes(bytes)  # then restore it from bytes to a StateWithIdVector

    assert ms_2x is not None
    for i in range(len(ms_2x)):
        assert round(abs(ms_2x[i].state.kirchner.q - (200 + i)), 7) == 0

    # feature test: given a state-with-id-vector, get the pure state-vector
    # suitable for rm.initial_state= <state_vector>
    # note however that this is 'unsafe', you need to ensure that size/ordering is ok
    # - that is the purpose of the cell-state-with-id
    #   better solution could be to use
    #     rm.state.apply( state_with_id) .. and maybe check the result, number of states== expected applied
    #     rm.initial_state=rm.current_state  .. a new property to ease typical tasks
    sv_2 = ms_2.state_vector
    assert len(sv_2) == len(ms_2)
    for s, sid in zip(sv_2, ms_2):
        assert round(abs(s.kirchner.q - sid.state.kirchner.q), 7) == 0
    # example apply, then initial state:
    model.state.apply_state(ms_2, cids_unspecified)
    model.initial_state = model.current_state
