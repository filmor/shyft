from shyft.dashboard.time_series.dt_selector import calendar_unit_to_str, tdiff_to_str, dt_to_str
from shyft.time_series import Calendar


def test_dt_to_string():
    assert dt_to_str(Calendar.YEAR) == '1 Year'
    assert dt_to_str(Calendar.QUARTER) == '1 Quarter'
    assert dt_to_str(Calendar.MONTH) == '1 Month'
    assert dt_to_str(Calendar.WEEK) == '1 Week'
    assert dt_to_str(Calendar.DAY) == '1 Day'
    assert dt_to_str(Calendar.HOUR) == '1 Hour'
    assert dt_to_str(3600) == '1 Hour'
    assert dt_to_str(Calendar.MINUTE) == '1 Minute'
    assert dt_to_str(1) == '1 Second'

    assert dt_to_str(2*Calendar.YEAR) == '2 Years'
    assert dt_to_str(2*Calendar.QUARTER) == '2 Quarters'
    assert dt_to_str(2*Calendar.MONTH) == '2 Months'
    assert dt_to_str(2*Calendar.WEEK) == '2 Weeks'
    assert dt_to_str(2*Calendar.DAY) == '2 Days'
    assert dt_to_str(2*Calendar.HOUR) == '2 Hours'
    assert dt_to_str(2*Calendar.MINUTE) == '2 Minutes'
    assert dt_to_str(2) == '2 Seconds'
    assert dt_to_str(3600+60) == '1 Hour 1 Minute'

    assert dt_to_str(0) == ""


def test_tdiff_to_str():
    cal = Calendar("Europe/Oslo")
    def dt_to_str_loc(t1, t2):
        return tdiff_to_str(cal, t1, t2)

    t1 = cal.time(2021, 1, 1)
    t2 = cal.time(2022, 1, 1)
    dt_to_str_loc(t1, t2), "1 Year"
    dt_to_str_loc(t2, t1), "1 Year"

    t1 = cal.time(2021, 1, 1, 1, 1, 1, 50)
    t2 = cal.time(2022, 4, 4, 2, 2, 1, 40) # ignores mili-secs
    string = "1 Year 3 Months 4 Days 1 Hour 1 Second"
    dt_to_str_loc(t1, t2), string
    dt_to_str_loc(t2, t1), string

    t1 = cal.time(2021, 3, 27, 1, 50, 0)
    t2 = cal.time(2022, 3, 27, 3, 10, 0)
    string = "20 Minutes"
    dt_to_str_loc(t1, t2), string
    dt_to_str_loc(t2, t1), string


def test_calendar_unit_to_str():
    assert calendar_unit_to_str(Calendar.YEAR) == 'Year'
    assert calendar_unit_to_str(int(Calendar.YEAR)) == 'Year'
    assert calendar_unit_to_str(3* Calendar.MONTH) == 'Quarter'
    assert calendar_unit_to_str(3*int(Calendar.MONTH)) == 'Quarter'

    assert isinstance(calendar_unit_to_str(Calendar.YEAR), str)
