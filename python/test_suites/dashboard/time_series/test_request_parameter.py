
import pytest

from shyft.dashboard.time_series.state import State
from shyft.time_series import TimeAxis, Calendar
from shyft.dashboard.time_series.sources.source import TsAdapterRequestParameter
from shyft.dashboard.time_series.axes_handler import DsViewTimeAxisType


def test_request_parameter_empty():
    p6 = TsAdapterRequestParameter.create_empty()
    assert p6.view_time_axis is None
    assert p6.padded_view_time_axis is None
    assert p6.request_time_axis_type is None
    assert p6.unit is None

    assert p6.is_empty


def test_request_time_axis():

    # view time axis
    week = Calendar().WEEK

    u1 = State.unit_registry("MW")
    tv1 = TimeAxis(0, week, 10)
    tp1 = TimeAxis(-10, week, 30)
    tt1 = DsViewTimeAxisType.view_time_axis
    p1 = TsAdapterRequestParameter(view_time_axis=tv1, unit=u1, padded_view_time_axis=tp1, request_time_axis_type=tt1)

    assert tv1 == p1.request_time_axis

    # padded view time axis
    u2 = State.unit_registry("MW")
    tv2 = TimeAxis(0, week, 10)
    tp2 = TimeAxis(-10, week, 30)
    tt2 = DsViewTimeAxisType.padded_view_time_axis
    p2 = TsAdapterRequestParameter(view_time_axis=tv2, unit=u2, padded_view_time_axis=tp2, request_time_axis_type=tt2)

    assert tp2 == p2.request_time_axis

    # wrong definition
    u3 = State.unit_registry("MW")
    tv3 = TimeAxis(0, week, 10)
    tp3 = TimeAxis(-10, week, 30)
    tt3 = None
    p3 = TsAdapterRequestParameter(view_time_axis=tv3, unit=u3, padded_view_time_axis=tp3, request_time_axis_type=tt3)

    with pytest.raises(RuntimeError):
        p3.request_time_axis


def test_request_parameter_is_equal():
    week = Calendar().WEEK

    u1 = State.unit_registry("MW")
    tv1 = TimeAxis(0, week, 10)
    tp1= TimeAxis(0, week, 10)
    tt1 = DsViewTimeAxisType.view_time_axis
    p1 = TsAdapterRequestParameter(view_time_axis=tv1, unit=u1, padded_view_time_axis=tp1, request_time_axis_type=tt1)

    assert p1.is_equiv(p1)
    assert p1 == p1  #check on uid

    # different unit
    u2 = State.unit_registry("W")
    tv2 = TimeAxis(0, week, 10)
    tp2 = TimeAxis(0, week, 10)
    tt2 = DsViewTimeAxisType.view_time_axis
    p2 = TsAdapterRequestParameter(view_time_axis=tv2, unit=u2, padded_view_time_axis=tp2, request_time_axis_type=tt2)

    assert not p1.is_equiv(p2)
    assert not p2.is_equiv(p1)
    assert p1 != p2

    # different view time axis
    u3 = State.unit_registry("MW")
    tv3 = TimeAxis(0, week, 5)
    tp3 = TimeAxis(0, week, 10)
    tt3 = DsViewTimeAxisType.view_time_axis
    p3 = TsAdapterRequestParameter(view_time_axis=tv3, unit=u3, padded_view_time_axis=tp3, request_time_axis_type=tt3)

    assert not p1.is_equiv(p3)
    assert not p3.is_equiv(p1)

    # different request time axis
    u5 = State.unit_registry("MW")
    tv5 = TimeAxis(0, week, 10)
    tp5 = TimeAxis(0, week, 5)
    tt5 = DsViewTimeAxisType.padded_view_time_axis
    p5 = TsAdapterRequestParameter(view_time_axis=tv5, unit=u5, padded_view_time_axis=tp5, request_time_axis_type=tt5)

    assert not p1.is_equiv(p5)
    assert not p5.is_equiv(p1)

    # empty
    p6 = TsAdapterRequestParameter.create_empty()

    assert not p1.is_equiv(p6)
    assert not p6.is_equiv(p1)

    # different thing
    assert not p6.is_equiv(2)

    # wrong definition
    u3 = State.unit_registry("MW")
    tv3 = None
    tp3 = None
    tt3 = None
    p3w = TsAdapterRequestParameter(view_time_axis=tv3, unit=u3, padded_view_time_axis=tp3, request_time_axis_type=tt3)
    p3.request_time_axis_type = None
    # should not be equivalent
    assert not p3w.is_equiv(p3)
    assert not p3.is_equiv(p3w)
