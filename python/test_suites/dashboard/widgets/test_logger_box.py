import inspect
import pytest

from shyft.dashboard.widgets.logger_box import LoggerBox, LogLevel


def test_logger_box(mock_bokeh_document):

    log_level = 10
    max_history = 5

    with pytest.raises(ValueError):
        LoggerBox(mock_bokeh_document, log_level=25)

    logger = LoggerBox(mock_bokeh_document, log_level=log_level, max_history=max_history, enable_dark_scheme=False, text_font_size=10)

    assert logger
    assert logger.layout
    assert logger.layout_components['figures'], logger.layout_components['widgets']
    assert logger.level == LogLevel(log_level)
    assert not logger.get_class_from_frame(inspect.stack()[1][0])

    msg = 'Test message'
    logger.debug(msg)
    assert len(logger.ds_text.data['text']) == 1
    assert logger.ds_text.data['text'][0] == f'DEBUG - msg: {msg}'

    for i in range(max_history-1):
        logger.debug(msg=msg)
    assert len(logger.ds_text.data['text']) == max_history

    logger.debug(msg=msg)
    assert len(logger.ds_text.data['text']) == max_history

    y_max = logger.ds_text.data['y'].max()
    logger.selector_fontsize.value = '12'
    assert logger.text_size == '12pt'
    assert logger.ds_text.data['y'].max() > y_max

    new_level = 40
    logger.selector_level.value = str(new_level)
    assert logger.level.value == new_level


def test_logger_box_levels(mock_bokeh_document):
    msg = 'Test message'
    logger_items = []

    for log_level in 10, 30, 50:

        logger = LoggerBox(mock_bokeh_document, log_level=log_level)

        logger.critical(msg=msg)
        logger.error(msg=msg)
        logger.warning(msg=msg)
        logger.info(msg=msg)
        logger.debug(msg=msg)

        logger_items.append(len(logger.ds_text.data['text']))

    assert logger_items == [5, 3, 1]


def test_logger_box_isEnabledFor(mock_bokeh_document):
    for log_level in 10, 30, 50:
        logger = LoggerBox(mock_bokeh_document, log_level=log_level)
        assert logger.isEnabledFor(log_level)
        assert not logger.isEnabledFor(log_level + 10)
        assert logger.isEnabledFor(log_level-10)


def test_logger_box_async(mock_bokeh_document):

    logger = LoggerBox(mock_bokeh_document, log_level=10)

    logger.exception('Test message', extra=dict(async_on=True))

    assert len(mock_bokeh_document.next_tick_callbacks) == 1
    assert len(logger.ds_text.data['text']) == 0

    mock_bokeh_document.next_tick()

    assert len(mock_bokeh_document.next_tick_callbacks) == 0
    assert len(logger.ds_text.data['text']) == 1


def test_logger_box_frame(mock_bokeh_document):

    class Foo:
        def __init__(self, log: LoggerBox):
            self.name = log.get_class_from_frame(fr=inspect.stack()[0][0])

    logger = LoggerBox(mock_bokeh_document, log_level=10, long_class_name=True)
    foo = Foo(logger)
    assert "Foo" in foo.name
    assert foo.name == "<class 'test_suites.dashboard.widgets.test_logger_box.test_logger_box_frame.<locals>.Foo'>"

    # not a class calling this so no name should be there
    name = logger.get_class_from_frame(inspect.stack()[0][0])
    assert name == ''

    logger = LoggerBox(mock_bokeh_document, log_level=10, long_class_name=False)
    bar = Foo(logger)
    assert bar.name == "Foo"


