import shyft.time_series as sa
import logging
from shyft.dashboard.base.ports import connect_ports, Receiver
from shyft.dashboard.widgets.date_selector import DateSelector


class ListHandler(logging.Handler):  # Inherit from logging.Handler
    def __init__(self, log_list):
        # run the regular Handler __init__
        logging.Handler.__init__(self)
        # Our custom argument
        self.log_list = log_list

    def emit(self, record):
        # record.message is the log message
        self.log_list.append(record.msg)


def test_date_selector():

    log_list = []
    lh = ListHandler(log_list)
    lh.setLevel(logging.DEBUG)
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    logger.addHandler(lh)

    date = 1518649200 ## 15.02.2018
    date_picker = DateSelector(title='From', width=10)

    call_count = [0]

    def receive_data(d: int):
        assert d == date
        call_count[0] += 1

    receive_date = Receiver(parent="test", name="test received date", func=receive_data, signal_type=int)
    connect_ports(date_picker.send_selected_date, receive_date)

    date_picker.receive_selected_date(date)
    assert call_count[0] == 1

    date_picker.receive_max_date(date)
    date_picker.receive_selected_date(date+sa.Calendar().DAY)
    # should no be possible to set the date
    assert call_count[0] == 1
    assert log_list[-1] == "Error: date: '2018.2.16' is larger than max_date '2018.2.15'"

    date_picker.receive_max_date(date+sa.Calendar.WEEK)
    date_picker.receive_min_date(date)
    date_picker.receive_selected_date(date-sa.Calendar().DAY)
    assert call_count[0] == 1
    assert log_list[-1] == "Error: date: '2018.2.14' is smaller than min_date '2018.2.15'"
