import pytest
from shyft.dashboard.maps.map_viewer import MapViewer
from shyft.dashboard.maps.layer_data import LayerDataHandle
from shyft.dashboard.maps.map_layer import MapLayer, MapLayerType, MapLayerError, find_tags_from_tooltips
from shyft.dashboard.time_series.bindable import BindableError

import bokeh.plotting as bokeh_plotting


def test_map_layer(base_map):
    test_file = 'test_map_viewer.html'
    bokeh_plotting.output_file(test_file)

    map_viewer = MapViewer(width=300, height=300, base_map=base_map)
    map_viewer.fig_axes_ranges.set_axes_bounds(26621 + 22373 * 0.5, 6599653 + 18731 * 0.5,
                                               26621 + 22373 * 0.5 + 100, 6599653 + 18731 * 0.5 + 100)
    map_viewer.fig_axes_ranges.padding = 10

    # init error handling
    with pytest.raises(MapLayerError):
        layer1 = MapLayer(map_viewer=map_viewer, name='first layer', layer_type=MapLayerType.POINT,
                          glyph_fixed_kwargs={'radius': 5},
                          glyph_variable_kwargs=None)
    # double defined property
    with pytest.raises(MapLayerError):
        layer1 = MapLayer(map_viewer=map_viewer, name='first layer', layer_type=MapLayerType.POINT,
                          glyph_fixed_kwargs={'radius': 5},
                          glyph_variable_kwargs={'radius': 5})
    # missing coordinate variable in source kwargs
    with pytest.raises(MapLayerError):
        layer1 = MapLayer(map_viewer=map_viewer, name='first layer', layer_type=MapLayerType.POINT,
                          glyph_fixed_kwargs={'linewidth': 5},
                          glyph_variable_kwargs={'x': 5})
    with pytest.raises(MapLayerError):
        layer1 = MapLayer(map_viewer=map_viewer, name='first layer', layer_type=MapLayerType.POINT,
                          glyph_fixed_kwargs={'linewidth': 5},
                          glyph_variable_kwargs={'y': 5})
    with pytest.raises(MapLayerError):
        layer1 = MapLayer(map_viewer=map_viewer, name='first layer', layer_type=MapLayerType.POLYGON,
                          glyph_fixed_kwargs={'linewidth': 5},
                          glyph_variable_kwargs={'xs': 5})
    with pytest.raises(MapLayerError):
        layer1 = MapLayer(map_viewer=map_viewer, name='first layer', layer_type=MapLayerType.POLYGON,
                          glyph_fixed_kwargs={'linewidth': 5},
                          glyph_variable_kwargs={'ys': 5})

    # glyph creation error
    with pytest.raises(MapLayerError):
        layer1 = MapLayer(map_viewer=map_viewer, name='first layer', layer_type=MapLayerType.POLYGON,
                          glyph_fixed_kwargs={'linewidth': '12'},
                          glyph_variable_kwargs={'ys': 'ys', 'xs': 'xs'})

    # on_bind error
    with pytest.raises(BindableError):
        layer1 = MapLayer(map_viewer=MapLayer, name='first layer', layer_type=MapLayerType.POLYGON,
                          glyph_fixed_kwargs={'line_width': '12'},
                          glyph_variable_kwargs={'ys': 'ys', 'xs': 'xs'})
    # add real layer
    assert not map_viewer.layers
    layer1 = MapLayer(map_viewer=map_viewer, name='first layer', layer_type=MapLayerType.POINT,
                      glyph_fixed_kwargs={'radius': 5}, visible=False,
                      glyph_variable_kwargs={'x': 'x', 'y': 'y'})
    assert layer1 in map_viewer.layers
    assert layer1.renderer == map_viewer.layers[layer1]

    # test visible
    assert not layer1.visible
    assert not layer1.renderer.visible
    layer1.visible = True
    layer1.visible = True
    assert layer1.visible
    assert layer1.renderer.visible

    # test bbox with no data
    bbox = layer1.bbox
    assert bbox is None

    x_val = 26621 + 22373*0.5 + 50
    y_val = 6599653 + 18731*0.5 + 50
    # test update data with actuall data
    layer1.updated_data({'x': [x_val],
                         'y': [y_val]})

    # test bbox once more
    bbox = layer1.bbox
    assert bbox == (x_val, y_val, x_val, y_val)

    # test update errors
    with pytest.raises(MapLayerError):
        layer1.updated_data({'x': [x_val],
                             'y': [y_val],
                             'bar': ['foo']})

    with pytest.raises(MapLayerError):
        layer1.updated_data({'bar': ['foo']})

    with pytest.raises(MapLayerError):
        layer1.updated_data({'x': [x_val, x_val],
                             'y': [y_val],})

    with pytest.raises(MapLayerError):
        layer1.updated_data({'x': x_val,
                             'y': [y_val],})

    # label text without text field in source
    with pytest.raises(MapLayerError):
        layer2 = MapLayer(map_viewer=map_viewer, name='second layer', layer_type=MapLayerType.LABEL,
                          glyph_fixed_kwargs={'radius': 5}, visible=True,
                          glyph_variable_kwargs={'x': 'x', 'y': 'y'})
    # wrong variale for map layer: render_mode
    with pytest.raises(MapLayerError):
        layer2 = MapLayer(map_viewer=map_viewer, name='second layer', layer_type=MapLayerType.LABEL,
                          glyph_fixed_kwargs={'x_offset': 5, 'y_offset': 5, 'render_mode': 'canvas'}, visible=False,
                          glyph_variable_kwargs={'x': 'x', 'y': 'y', 'text': 'names'})
    layer2 = MapLayer(map_viewer=map_viewer, name='second layer', layer_type=MapLayerType.LABEL,
                      glyph_fixed_kwargs={'x_offset': 5, 'y_offset': 5}, visible=False,
                      glyph_variable_kwargs={'x': 'x', 'y': 'y', 'text': 'names'})
    with pytest.raises(MapLayerError):
        layer2.updated_data({'x': [x_val],
                             'y': [y_val]})
    layer2.updated_data({'x': [x_val+20],
                         'y': [y_val+20],
                         'names': ["test"]})
    # test visibility
    assert not layer2.visible
    layer2.visible = True
    assert layer2.visible

    map_viewer.update_axes()

    # bokeh_plotting.show(map_viewer.layout)


def test_layer_hover_tool(base_map):
    test_file = 'test_map_viewer.html'
    bokeh_plotting.output_file(test_file)

    map_viewer = MapViewer(width=300, height=300, base_map=base_map)
    map_viewer.fig_axes_ranges.set_axes_bounds(26621 + 22373 * 0.5, 6599653 + 18731 * 0.5,
                                               26621 + 22373 * 0.5 + 100, 6599653 + 18731 * 0.5 + 100)
    map_viewer.fig_axes_ranges.padding = 10

    # init error handling
    layer1 = MapLayer(map_viewer=map_viewer, name='first layer', layer_type=MapLayerType.POINT,
                      glyph_fixed_kwargs={'radius': 5},
                      glyph_variable_kwargs={'x': 'x', 'y': 'y'},
                      hover_tool_tips=[("foo", '@foo'), ('x', '@x')])
    x_val = 26621 + 22373 * 0.5 + 50
    y_val = 6599653 + 18731 * 0.5 + 50
    # test update data with actuall data
    with pytest.raises(MapLayerError):
        layer1.updated_data({'x': [x_val],
                             'y': [y_val]})

    layer1.updated_data({'x': [x_val],
                         'y': [y_val],
                         'foo': ['bar']})
    map_viewer.update_axes()

    # only if manual tests and visible, that we do not have at the test-servers.
    # bokeh_plotting.show(map_viewer.layout)


def test_find_tags_from_tooltips():
    tt = [("Type", "@type"),
          ("Name", "@name@field"),
          ("Uid", "@uid{sd}"),
          ("Max filling", "@{max filling} @max_filling_unit{asd}"),
          ("Filling", "@filling @filling_unit "),
          ("Filling in %", "@filling_pct_100 %")]
    target_tt = {'type',
                 'name',
                 'field',
                 'uid',
                 'max filling',
                 'max_filling_unit',
                 'filling',
                 'filling_unit',
                 'filling_pct_100'}
    tag_names = find_tags_from_tooltips(tt)
    assert tag_names == target_tt

    tool_tips = [('name', '@names'), ('id', '@id'), ('comment', '@comment'), ('precipitation', '@precipitation'),
                 ('snow', '@snow'), ('temperature', '@temperature'), ('voltage', '@voltage'),
                 ('water_reservoir', '@water_reservoir'), ('water_river', '@water_river'),
                 ('water_temperature', '@water_temperature'), ('wind_direction', '@wind_direction'),
                 ('wind_speed', '@wind_speed')]
    target_tt = {'names',
                 'id',
                 'comment',
                 'precipitation',
                 'snow',
                 'temperature',
                 'voltage',
                 'water_reservoir',
                 'water_river',
                 'water_temperature',
                 'wind_direction',
                 'wind_speed'}
    tag_names = find_tags_from_tooltips(tool_tips)
    assert tag_names == target_tt
