from shyft.hydrology.viz.data_extractors.shyft_GeoTsVector_data import GeoTsVectorDataExtractor
from shyft.time_series import TimeAxis, Calendar, time,  POINT_AVERAGE_VALUE, TimeSeries
import shyft.hydrology as api
from typing import Any


def _create_constant_geo_ts(geo_ts_type:Any, geo_point:api.GeoPoint, time_axis: TimeAxis, value:float):
    """Create a time point ts, with one value at the start
    of the supplied utc_period."""
    return geo_ts_type(geo_point, TimeSeries(time_axis, fill_value=value, point_fx=POINT_AVERAGE_VALUE))


def _create_dummy_region_environment(time_axis, mid_point, temperature: float = 10.0):
    re = api.ARegionEnvironment()
    re.precipitation.append(_create_constant_geo_ts(api.PrecipitationSource, mid_point, time_axis, 5.0))
    re.temperature.append(_create_constant_geo_ts(api.TemperatureSource, mid_point, time_axis, temperature))
    re.wind_speed.append(_create_constant_geo_ts(api.WindSpeedSource, mid_point, time_axis, 2.0))
    re.rel_hum.append(_create_constant_geo_ts(api.RelHumSource, mid_point, time_axis, 0.7))
    re.radiation.append(_create_constant_geo_ts(api.RadiationSource, mid_point, time_axis, 300.0))
    return re


def test_tsv_extractor():
    utc = Calendar()
    ta = TimeAxis(utc.time(2021, 1, 1), time(3600), 10)
    re = _create_dummy_region_environment(ta, api.GeoPoint(1000, 1000, 100))
    e = GeoTsVectorDataExtractor(re, as_pt_dataset=True)
    assert e
    assert len(e.temporal_vars) == 5
    t = utc.time(2021, 1, 1, 1)
    e.t_ax_shyft = ta  # to allow next tests
    assert e.get_closest_time(t) == t
    assert e.time_num_2_str(t) == utc.to_string(t)
    assert e._time_num_2_idx(t) == 1
    for n in e.temporal_vars:
        v = e.get_map(var_name=n, cat_id_lst_grp='', t=t)
        assert len(v)
