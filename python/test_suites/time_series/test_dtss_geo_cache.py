from shyft.time_series import DtssCfg, DtsServer, DtsClient, Calendar, \
    GeoPointVector, GeoPoint, StringVector, GeoGridSpec, \
    GeoTimeSeriesConfiguration, UtcTimeVector, GeoSlice, TimeAxis, \
    time_axis_extract_time_points_as_utctime, POINT_AVERAGE_VALUE as stair_case, \
    TimeSeries, GeoQuery, GeoEvalArgs


def test_dtss_geo_cache(tmp_path):
    """ as pr. issue prepared by Jonas Gliss, https://gitlab.com/shyft-os/shyft/-/issues/975 """

    rootdir = str(tmp_path)
    M = 1024*1024
    geo_cfg = DtssCfg(
        ppf=1024,
        compress=False,
        max_file_size=400*M,
        write_buffer_size=4*M)

    s = DtsServer()
    s.set_container(
        name='',
        root_dir=rootdir,
        container_type='ts_ldb'
    )

    s.default_geo_db_config = geo_cfg

    port = s.start_server()

    # print(port)
    utc = Calendar()
    client = DtsClient(f'localhost:{port}')

    # FILL GEODTSS WITH FAKEDATA
    container_name = 'test'
    # forecast step
    fc_dt = 12*3600
    # 10 forecasts starting at 1.1.2020 with 12 hours between them
    fc_t0 = TimeAxis(utc.time(2020, 1, 1), fc_dt, 10)
    num_vals = 24*3
    freq_inv = 3600
    fc_len = freq_inv*num_vals  # 3 day forcasts
    variables = StringVector(['var1', 'var2'])
    points = GeoPointVector([GeoPoint(0, 0, 0), GeoPoint(1, 1, 0)])
    grid = GeoGridSpec(points=points, epsg=4326)  # latlon CRS
    n_ensembles = 1

    gdb = GeoTimeSeriesConfiguration(
        prefix='shyft://',
        name=container_name,
        description='Basic test',
        grid=grid,
        t0_times=UtcTimeVector(),
        dt=fc_len,
        n_ensembles=n_ensembles,
        variables=variables)

    client.add_geo_ts_db(gdb)
    containers = client.get_geo_db_ts_info()
    assert container_name in [x.name for x in containers]

    tp = time_axis_extract_time_points_as_utctime(fc_t0)[:-1]
    g_slice = GeoSlice(v=range(2), e=[0], g=[gix for gix in range(len(gdb.grid.points))], t=tp, ts_dt=gdb.dt)
    assert g_slice
    fcm = gdb.create_ts_matrix(g_slice)  # create the structure for our forecasts
    assert fcm
    # now fill the forecast matrix, with forecast time-series
    # dimensions time, variables, ensembles,geopoints
    for t in range(fcm.shape.n_t0):
        for v in range(fcm.shape.n_v):
            for e in range(fcm.shape.n_e):
                for g in range(fcm.shape.n_g):
                    tax = TimeAxis(fc_t0.time(t), freq_inv, num_vals)
                    fc_ts = TimeSeries(tax, fill_value=t*10 + g,
                                       point_fx=stair_case)
                    fcm.set_ts(t, v, e, g, fc_ts)  # indicies happens to be correct here

    client.geo_store(container_name, fcm, replace=True, cache=True)
    print('success')

    # READ FROM SERVER WITH AND WITHOUT CACHING
    fcs_to_read = [utc.time(2020, 1, 3), utc.time(2020, 1, 3, 12)]
    time_axis_read = TimeAxis(fcs_to_read, fcs_to_read[-1] + 1)
    uncached_ta = TimeAxis()
    cached_ta = TimeAxis()
    for cache in [True, False]:
        eval_args = GeoEvalArgs(
            geo_ts_db_id=gdb.name,
            variables=variables,
            ensembles=[0],
            time_axis=time_axis_read,
            ts_dt=fc_len,
            geo_range=GeoQuery(),
            concat=False,
            cc_dt0=0
        )

        gm = client.geo_evaluate(eval_args=eval_args, use_cache=cache, update_cache=False)

        # get first timeseries
        ts = gm.get_ts(t=0, v=0, e=0, g=0)
        if cache:
            cached_ta = ts.time_axis
        else:
            uncached_ta = ts.time_axis
        # print(f'Cache: {cache}: {ts.time_axis}')
    del client
    s.stop_server()
    del s
    assert cached_ta == uncached_ta
