import math

from shyft.time_series import Calendar, deltahours, DoubleVector, TimeAxisFixedDeltaT, TimeSeries, point_interpretation_policy, TsVector



def test_abs():
    c = Calendar()
    t0 = c.time(2016, 1, 1)
    dt = deltahours(1)
    n = 4
    v = DoubleVector([1.0, -1.5, float("nan"), 3.0])
    ta = TimeAxisFixedDeltaT(t0, dt, n)
    ts0 = TimeSeries(ta=ta, values=v, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    tsa = TimeSeries('a')
    ts1 = tsa.abs()
    ts1_blob = ts1.serialize()
    ts1 = TimeSeries.deserialize(ts1_blob)
    assert ts1.needs_bind()
    bts = ts1.find_ts_bind_info()
    assert len(bts) == 1
    bts[0].ts.bind(ts0)
    ts1.bind_done()
    assert not ts1.needs_bind()
    assert round(abs(ts0.value(0) - ts1.value(0)), 6) == 0
    assert round(abs(abs(ts0.value(1)) - ts1.value(1)), 6) == 0
    assert math.isnan(ts1.value(2))
    assert round(abs(ts0.value(3) - ts1.value(3)), 6) == 0
    tsv0 = TsVector()
    tsv0.append(ts0)
    tsv1 = tsv0.abs()
    assert round(abs(tsv0[0].value(0) - tsv1[0].value(0)), 6) == 0
    assert round(abs(abs(tsv0[0].value(1)) - tsv1[0].value(1)), 6) == 0
    assert math.isnan(tsv1[0].value(2))
    assert round(abs(tsv0[0].value(3) - tsv1[0].value(3)), 6) == 0
