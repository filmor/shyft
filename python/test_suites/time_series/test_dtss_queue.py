import tempfile
import pytest
from time import sleep

from shyft.time_series import (Calendar,DtsClient,DtsServer,IntVector,UtcTimeVector,
    StringVector, TimeAxis, TimeSeries , TsInfo, TsInfoVector, TsVector
    ,UtcPeriod,deltahours,point_interpretation_policy as point_fx
    ,utctime_now,ts_stringify,time,no_utctime
)


def test_dtss_queue():
    with tempfile.TemporaryDirectory() as c_dir:
        master=DtsServer()
        master.set_container("test", c_dir)
        port = master.start_server()
        c = DtsClient(f'localhost:{port}')
        assert len(c.q_list()) == 0
        q1 = "q1"
        q2 = "q2"
        c.q_add(q1)
        c.q_add(q2)
        qs = c.q_list()
        assert set([q1,q2]) == set( [qn for qn in qs])
        assert len(c.q_msg_infos(q1)) == 0
        # store some time-series through master connection(slave does not know this)
        n = 3
        ta=TimeAxis(time('2021-04-07T00:00:00Z'),time(3600),10)
        some_tsv=TsVector(
            [TimeSeries(
                f'shyft://test/a{i}',
                TimeSeries(ta,fill_value=float(i),point_fx=point_fx.POINT_AVERAGE_VALUE))
             for i in range(n)]
        )
        m1="m1"
        d1="{}"
        ttl=time(1.0)
        t_now=time.now()
        c.q_put(q1,m1,d1,ttl,some_tsv)  # put into the queue.
        mis = c.q_msg_infos(q1)
        assert len(mis) == 1
        assert mis[0].msg_id == m1
        assert mis[0].description == d1
        assert mis[0].created-t_now < time(1.0)
        assert mis[0].fetched ==  no_utctime
        assert mis[0].done == no_utctime
        assert c.q_size(q1) == 1

        max_wait = time(2.0)
        e1 = c.q_get(q1,max_wait)
        assert c.q_size(q1) == 0   # because element is now OUT of the queue, but kept alive because we wait for q_ack
        assert e1
        assert e1.tsv == some_tsv
        assert e1.info.fetched-t_now < time(1.0)
        c.q_ack(q1,e1.info.msg_id,"OK")
        i1= c.q_msg_info(q1,e1.info.msg_id)
        assert i1.done-t_now < time(1.0)
        assert i1.diagnostics == "OK"

        c.q_remove(q2)
        c.q_maintain(q1,keep_ttl_items=False,flush_all=False)
        assert c.q_size(q1) == 0
        #  close down the show
        c.close()
        master.stop_server()

