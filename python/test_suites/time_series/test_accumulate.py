from shyft.time_series import Calendar, deltahours, TimeAxis, TimeSeries, point_interpretation_policy



def test_accumulate():
    c = Calendar()
    t0 = c.time(2016, 1, 1)
    dt = deltahours(1)
    n = 240
    ta = TimeAxis(t0, dt, n)
    ts0 = TimeSeries(ta=ta, fill_value=1.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    tsa = 1.0*TimeSeries('a') + 0.0  # an expression, that need bind

    ts1 = tsa.accumulate(ta)  # ok, maybe we should make method that does time-axis implicit ?
    assert ts1.needs_bind()
    ts1_blob = ts1.serialize()
    ts1 = TimeSeries.deserialize(ts1_blob)
    tsb = ts1.find_ts_bind_info()
    assert len(tsb) == 1
    tsb[0].ts.bind(ts0)
    ts1.bind_done()
    assert not ts1.needs_bind()

    ts1_values = ts1.values
    for i in range(n):
        expected_value = float(i*dt*1.0)
        assert round(abs(expected_value - ts1.value(i)), 3) == 0, "expect integral f(t)*dt"
        assert round(abs(expected_value - ts1_values[i]), 3) == 0, "expect value vector equal as well"
