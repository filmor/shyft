import numpy as np
import pytest
from shyft.time_series import TimeSeries, ts_stringify, Calendar, deltahours, TimeAxis, DoubleVector, point_interpretation_policy


def test_unbound_ts():
    a = TimeSeries('a')
    assert a.needs_bind() == True
    assert a.ts_id() == 'a'
    with pytest.raises(RuntimeError):
        a.size()
    with pytest.raises(RuntimeError):
        a.time_axis
    with pytest.raises(RuntimeError):
        a.values
    with pytest.raises(RuntimeError):
        a.point_interpretation()

    s = ts_stringify(a*3.0 + 1.0)
    assert len(s) > 0
    a.set_ts_id('b')
    assert a.ts_id() == 'b'

    pass


def test_ts_reference_and_bind():
    c = Calendar()
    t0 = c.time(2016, 9, 1)
    dt = deltahours(1)
    n = c.diff_units(t0, c.time(2017, 9, 1), dt)

    ta = TimeAxis(t0, dt, n)
    pattern_values = DoubleVector.from_numpy(np.arange(len(ta)))  # increasing values

    a = TimeSeries(ta=ta, values=pattern_values, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    b_id = "netcdf://path_to_file/path_to_ts"
    b = TimeSeries(b_id)
    c = (a + b)*4.0  # make an expression, with a ts-reference, not yet bound
    c_blob = c.serialize()  # converts the entire stuff into a blob
    bind_info = c.find_ts_bind_info()

    assert len(bind_info) == 1, "should find just one ts to bind"
    assert bind_info[0].id == b_id, "the id to bind should be equal to b_id"
    try:
        c.value(0)  # verify touching a unbound ts raises exception
        assert not True, "should not reach here!"
    except RuntimeError:
        pass
    assert c.needs_bind() == True  # verify this expression needs binding
    # verify we can bind a ts
    bind_info[0].ts.bind(a)  # it's ok to bind same series multiple times, it takes a copy of a values
    c.bind_done()
    assert c.needs_bind() == False  # verify this expression do not need binding anymore
    # and now we can use c expression as pr. usual, evaluate etc.
    assert round(abs(c.value(10) - a.value(10)*2*4.0), 3) == 0

    c_resurrected = TimeSeries.deserialize(c_blob)

    bi = c_resurrected.find_ts_bind_info()
    bi[0].ts.bind(a)
    c_resurrected.bind_done()
    assert round(abs(c_resurrected.value(10) - a.value(10)*2*4.0), 3) == 0
    # verify we can create a ref.ts with something that resolves to a point ts.
    bind_expr_ts = TimeSeries("some_sym", 3.0*a)  # notice that we can bind with something that is an expression
    assert bind_expr_ts is not None
    assert round(abs(bind_expr_ts.value(0) - 3.0*a.value(0)), 7) == 0  # just to check, its for real
