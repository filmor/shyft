from shyft.time_series import TimeSeries, TimeAxis, Calendar, POINT_AVERAGE_VALUE, DoubleVector, TsVector
import numpy as np


def test_yearly_repeat():
    utc = Calendar()
    a = TimeSeries(TimeAxis(utc.time(2000, 1, 1), Calendar.DAY, 366), DoubleVector.from_numpy(np.linspace(1, 366, 366)), POINT_AVERAGE_VALUE)
    yearly = TimeAxis(utc, utc.time(1931, 1, 1), Calendar.YEAR, 100)  # notice that we pass in calendar to get it calendric repeat..
    r = a.repeat(yearly)
    assert yearly.total_period() == r.total_period()
    for i in range(len(r.time_axis)):
        doy = utc.day_of_year(r.time(i))
        assert round(abs(float(doy) - r.value(i)), 7) == 0

    tsv = TsVector([a, 2.0*a])
    rtsv = tsv.repeat(yearly)  # supported on expressions and  vector op as well

    for j in range(len(rtsv)):
        r = rtsv[j]
        for i in range(len(r.time_axis)):
            doy = utc.day_of_year(r.time(i))
            assert round(abs(float(doy)*(j + 1) - r.value(i)), 7) == 0
