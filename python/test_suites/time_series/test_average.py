import math

from shyft.time_series import TimeAxis, TimeSeries, POINT_AVERAGE_VALUE, POINT_INSTANT_VALUE


def test_average_outside_give_nan():
    ta1 = TimeAxis(0, 10, 10)
    ta2 = TimeAxis(-10, 10, 21)
    tsa = TimeSeries(ta1, fill_value=1.0, point_fx=POINT_AVERAGE_VALUE)
    tsb = tsa.average(ta2)
    assert math.isnan(tsb.value(11))  # nan when a ends
    assert math.isnan(tsb.value(0))  # nan before first a
    tsa = TimeSeries(ta1, fill_value=1.0, point_fx=POINT_INSTANT_VALUE)
    tsb = tsa.average(ta2)
    assert math.isnan(tsb.value(10))  # notice we get one less due to linear-between, it ends at last point in tsa.
    assert math.isnan(tsb.value(0))
