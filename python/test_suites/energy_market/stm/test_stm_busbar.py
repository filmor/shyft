import pytest
from shyft.energy_market.stm import shyft_with_stm
if not shyft_with_stm:
    pytest.skip('requires shyft_with_stm', allow_module_level=True)
from functools import reduce
from shyft.energy_market.stm import StmSystem, Network, PowerModule, MarketArea, HydroPowerSystem
from shyft.time_series import TimeSeries

def test_busbar_attributes():
    sys = StmSystem(1, "A", "{}")
    n = sys.create_network(2, 'Network', '{}')
    b = n.create_busbar(3, 'Busbar #1')
    # generics, from id_base
    assert hasattr(b, "id")
    assert hasattr(b, "name")
    assert hasattr(b, "json")
    # specifics
    assert hasattr(b, "flow")
    assert hasattr(b, "price")

    assert hasattr(b.flow, "result")
    assert hasattr(b.price, "schedule")
    assert str(b)
    assert repr(b)
    assert b.tag == "/n2/b3"

def test_busbar_attributes_flattened():
    sys = StmSystem(1, "A", "{}")
    n = sys.create_network(2, 'Network', '{}')
    assert str(n)
    assert repr(n)
    b = n.create_busbar(3, 'Busbar #1')
    assert b.flattened_attributes()
    for (path, val) in b.flattened_attributes().items():
        attr = reduce(getattr, path.split('.'), b)
        assert type(val) is type(attr)
        assert val == attr
        assert val.url() == attr.url()

def test_transmission_line_association():
    sys = StmSystem(1, "A", "{}")
    n = sys.create_network(2, 'Network', '{}')
    t = n.create_transmission_line(3, 'TransmissionLine #1')
    b1 = n.create_busbar(3, 'Busbar #1')
    b2 = n.create_busbar(4, 'Busbar #2')

    # Associate busbar to start of transmission line
    b1.add_to_start_of_transmission_line(t)
    assert b1.get_transmission_lines_from_busbar()[0] == t
    assert len(b1.get_transmission_lines_to_busbar()) == 0
    assert t.from_bb.id == 3
    # Associate busbar to end of transmission line
    b2.add_to_end_of_transmission_line(t)
    assert len(b2.get_transmission_lines_from_busbar()) == 0
    assert b2.get_transmission_lines_to_busbar()[0] == t
    assert t.to_bb.id == 4
    assert str(b1)
    assert repr(b2)

def test_power_module_association():
    sys = StmSystem(1, "A", "{}")
    pm = sys.create_power_module(2, 'PowerModule #1', '{}')
    pm2 = sys.create_power_module(3, 'PowerModule #2', '{}')
    n = sys.create_network(4, 'Network', '{}')
    b = n.create_busbar(5, 'Busbar #1')
    b.add_power_module(pm, TimeSeries());
    b.add_power_module(pm2, TimeSeries());
    assert str(b)
    assert repr(b)
    assert len(b.power_modules) == 2
    assert b.power_modules[0].power_module.id == 2

def test_unit_association():
    sys = StmSystem(1, "A", "{}")
    n = sys.create_network(4, 'Network', '{}')
    b = n.create_busbar(5, 'Busbar #1')

    hps = HydroPowerSystem(1, 'test')
    unit_1 = hps.create_unit(1, 'unit_1')
    unit_2 = hps.create_unit(2, 'unit_2')

    b.add_unit(unit_1, TimeSeries())
    b.add_unit(unit_2, TimeSeries())
    assert len(b.units) == 2
    assert b.units[0].unit == unit_1
    b.remove_unit(unit_1)
    assert len(b.units) == 1
    assert b.units[0].unit == unit_2

def test_market_area_association():
    sys = StmSystem(1, "A", "{}")
    mkt = sys.create_market_area(2, 'MarketArea #1', '{}')
    n = sys.create_network(3, 'Network', '{}')
    b = n.create_busbar(4, 'Busbar #1')
    b.add_to_market_area(mkt)

    market_areas = b.get_market_areas()

    assert len(market_areas) == 1
    assert market_areas[0].busbars[0] == b

def test_busbar_equal():
    sys = StmSystem(1, "A", "{}")
    # Create two networks to compare busbars
    n = sys.create_network(2, 'Network #1', '{}')
    n2 = sys.create_network(3, 'Network #2', '{}')
    a = n.create_busbar(4, 'Busbar #1')
    b = n2.create_busbar(4, 'Busbar #1')
    c = n.create_busbar(5, 'hmm')
    assert len(n.busbars) == 2
    assert a == a
    assert a == b
    assert a != c

def test_busbar_equal_with_associations():
    sys = StmSystem(1, "A", "{}")
    sys2 = StmSystem(2, "B", "{}")

    pm = sys.create_power_module(2, 'PowerModule #1', '{}')
    pm_2 = sys2.create_power_module(2, 'PowerModule #1', '{}')

    mkt = sys.create_market_area(5, 'MarketArea #1', '{}')
    mkt_2 = sys2.create_market_area(5, 'MarketArea #1', '{}')

    # Create two networks, to be compared
    n = sys.create_network(2, 'Network #1', '{}')
    n2 = sys2.create_network(2, 'Network #1', '{}')

    t = n.create_transmission_line(4, 'TransmissionLine #1')
    t2 = n.create_transmission_line(5, 'TransmissionLine #2')
    b = n.create_busbar(6, 'Busbar #1')

    t_2 = n2.create_transmission_line(4, 'TransmissionLine #1')
    t2_2 = n2.create_transmission_line(5, 'TransmissionLine #2')
    b_2 = n2.create_busbar(6, 'Busbar #1')
    assert b == b_2

    # Add associations to first busbar
    b.add_power_module(pm, TimeSeries())
    b.add_to_start_of_transmission_line(t)
    b.add_to_end_of_transmission_line(t2)
    b.add_to_market_area(mkt)
    assert b != b_2
    assert str(b.power_modules[0])

    # Add associations to second busbar
    b_2.add_power_module(pm_2, TimeSeries());
    b_2.add_to_start_of_transmission_line(t_2)
    b_2.add_to_end_of_transmission_line(t2_2)
    b_2.add_to_market_area(mkt_2)
    assert b == b_2

    # Check for Unit association equality
    hps = HydroPowerSystem(1, 'test')
    unit_1 = hps.create_unit(1, 'unit_1')

    b.add_unit(unit_1, TimeSeries())
    assert b != b_2
    b_2.add_unit(unit_1, TimeSeries())
    assert str(b_2.units[0])
    assert repr(b_2.units[0])
    assert b == b_2
    assert str(t2_2)
    assert repr(t2_2)
