import pytest
from shyft.energy_market.stm import shyft_with_stm
if not shyft_with_stm:
    pytest.skip('requires shyft_with_stm', allow_module_level=True)

from shyft.energy_market.stm import StmSystem

def test_network_attributes():
    sys = StmSystem(1, "A", "{}")
    n = sys.create_network(2, 'Network', '{}')
    # generics, from id_base
    assert hasattr(n, "id")
    assert hasattr(n, "name")
    assert hasattr(n, "json")
    # specifics
    assert hasattr(n, "busbars")
    assert hasattr(n, "transmission_lines")
    assert n.tag == "/n2"

def test_network_equal():
    sys = StmSystem(1, "A", "{}")
    sys2 = StmSystem(2, "B", "{}")
    # Create two separate networks, to be compared
    n = sys.create_network(2, 'Network', '{}')
    n_2 = sys2.create_network(2, 'Network', '{}')
    assert n == n_2

    # Compare added transmission lines
    t = n.create_transmission_line(3, 'TransmissionLine #1')
    assert n != n_2
    t_2 = n_2.create_transmission_line(3, 'TransmissionLine #1')
    assert n == n_2

    # Compare added busbars
    b = n.create_busbar(4, 'Busbar #1')
    assert n != n_2
    b_2 = n_2.create_busbar(4, 'Busbar #1')
    assert n == n_2
