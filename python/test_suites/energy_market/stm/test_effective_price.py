import pytest
from shyft.energy_market.stm import shyft_with_stm
if not shyft_with_stm:
    pytest.skip('requires shyft_with_stm', allow_module_level=True)
from shyft.energy_market.stm import compute_effective_price, t_xy
from shyft.energy_market.core import XyPointCurve, Point, PointList
from shyft.time_series import time, Calendar, TimeSeries, TimeAxis, POINT_AVERAGE_VALUE


def test_compute_effective_price():
    utc = Calendar()
    ta = TimeAxis(utc.time(2020, 10, 1), time(3600), 5)
    usage = TimeSeries(ta, [0, 1, 2, 3, 4], POINT_AVERAGE_VALUE)
    bids = t_xy()
    bids[ta.time(0)] = XyPointCurve(PointList([Point(10, 1), Point(12, 2), Point(13, 2)]))
    bids[ta.time(2)] = XyPointCurve(PointList([Point(20, 1), Point(24, 2), Point(26, 2)]))
    bids[ta.time(3)] = XyPointCurve(PointList([Point(20, 1), Point(25, 2), Point(28, 2)]))
    price_cheap = compute_effective_price(usage, bids, True)  # buy,use cheapest
    price_expns = compute_effective_price(usage, bids, False) # sell, choose highest
    #                             0  1      2             3               4
    e_price_cheap = TimeSeries(ta, [10, 10, (20*1 + 24*1)/2.0, (20*1 + 25*2)/3.0, (20*1 + 25*2 + 28*1)/4.0], POINT_AVERAGE_VALUE)
    e_price_expns = TimeSeries(ta, [13, 13, (26*2 + 24*0)/2.0, (28*2 + 25*1)/3.0, (28*2 + 25*2)/4.0], POINT_AVERAGE_VALUE)
    assert price_cheap == e_price_cheap
    assert price_expns == e_price_expns

