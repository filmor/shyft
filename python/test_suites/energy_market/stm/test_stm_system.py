import pytest
from shyft.energy_market.stm import shyft_with_stm
if not shyft_with_stm:
    pytest.skip('requires shyft_with_stm', allow_module_level=True)
from .models import create_test_hydro_power_system, create_complete_stm_system
from shyft.energy_market.stm import HydroPowerSystem, StmSystem, MarketArea
from shyft.energy_market.stm import t_xy,UnitGroup, Reservoir,StmSystemList
from shyft.energy_market.core import Point, PointList, XyPointCurve, ConnectionRole
from shyft.time_series import time, TimeSeries, TimeAxis, POINT_INSTANT_VALUE, POINT_AVERAGE_VALUE, byte_vector_from_file
from shyft.energy_market.stm.utilities import create_t_xy

from pathlib import Path

def test_can_create_hps():
    a = create_test_hydro_power_system(hps_id=1, name='hps')
    assert a is not None
    a_blob = a.to_blob()
    assert len(a_blob) > 0
    b = HydroPowerSystem.from_blob(a_blob)
    assert b is not None
    assert b.equal_structure(a)

def test_serialize_and_deserialize_blob():
    a = create_test_hydro_power_system(hps_id=1, name='hps')
    assert a is not None
    a_blob = a.to_blob()
    assert len(a_blob) > 0
    b = HydroPowerSystem.from_blob(a_blob)
    assert b is not None
    assert b.equal_structure(a)

def test_stm_system_to_blob_from_blob_equality():
    sys_a = create_complete_stm_system()
    assert sys_a is not None
    a_blob = sys_a.to_blob()
    assert len(a_blob) > 0
    sys_b = StmSystem.from_blob(a_blob)
    assert sys_a == sys_b

#@pytest.mark.skip(reason="not supported to deserialize older blobs, yet")
def test_read_previous_stm_system_version_blob():
    base_file="stm_system_blob_v0.0.0.blob"
    #base_file ="stm_system_blob_v4.27.1.blob"
    blob_file = Path(__file__).parent/"model_data"/base_file
    blob_v000 = byte_vector_from_file(str(blob_file)) #"model_data/stm_system_blob_v0.0.0.blob")
    sys_v000 = StmSystem.from_blob(blob_v000)
    assert sys_v000 == create_complete_stm_system()

def test_can_set_rsv_storage_description():
    a = HydroPowerSystem(1, 'a')
    r = a.create_reservoir(1, "x", "{'a':1}")
    a_r = a.reservoirs[0]
    assert not a_r.volume_level_mapping.exists, 'no storage_description set yet'
    v = t_xy()
    v[time('2018-10-17T00:00:00Z')] = XyPointCurve(PointList([Point(0.0, 0.0), Point(10.0, 10.0)]))
    a_r.volume_level_mapping = v
    assert a_r.volume_level_mapping.exists
    a_blob = a.to_blob()
    b = HydroPowerSystem.from_blob(a_blob)
    assert b.reservoirs
    b_r = b.find_reservoir_by_id(1)
    assert b_r
    assert b_r.volume_level_mapping
    assert b_r.volume_level_mapping.exists

def test_rsv_plan_mm3():
    a = HydroPowerSystem(1, 'a')
    r = a.create_reservoir(1, "x", "{'a':1}")
    assert not r.level.schedule.exists, 'no level_schedule set yet'
    r.level.schedule = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=3.0, point_fx=POINT_INSTANT_VALUE)
    assert r.level.schedule.exists
    a_blob = a.to_blob()
    b = HydroPowerSystem.from_blob(a_blob)
    b_r = b.reservoirs[0]
    assert b_r.level.schedule.exists


def test_can_create_stm_system():
    a = StmSystem(1, "A", "{}")
    assert a
    assert a.id == 1
    assert a.name == 'A'
    assert a.json == '{}'
    assert len(a.hydro_power_systems) == 0
    assert len(a.market_areas) == 0


def test_unit_group_types():
    assert UnitGroup.unit_group_type.UNSPECIFIED == 0
    assert UnitGroup.unit_group_type.FCR_N_UP > 0
    assert UnitGroup.unit_group_type.FCR_N_DOWN > 0
    assert UnitGroup.unit_group_type.FCR_D_UP > 0
    assert UnitGroup.unit_group_type.FCR_D_DOWN > 0
    assert UnitGroup.unit_group_type.AFRR_DOWN > 0
    assert UnitGroup.unit_group_type.AFRR_UP > 0
    assert UnitGroup.unit_group_type.MFRR_DOWN > 0
    assert UnitGroup.unit_group_type.MFRR_UP > 0
    assert UnitGroup.unit_group_type.FFR > 0
    assert UnitGroup.unit_group_type.RR_UP > 0
    assert UnitGroup.unit_group_type.RR_DOWN > 0
    assert UnitGroup.unit_group_type.COMMIT > 0
    assert UnitGroup.unit_group_type.PRODUCTION > 0 and UnitGroup.unit_group_type.PRODUCTION != UnitGroup.unit_group_type.COMMIT

def test_can_blobify_stm_system():
    """ verify serialization roundtrip for blob"""
    a = StmSystem(1, "A", "{}")
    a.hydro_power_systems.append(create_test_hydro_power_system(hps_id=1, name='ulla-førre'))
    no_1 = MarketArea(1, 'NO1', '{}', a)
    ta=TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240)
    z=TimeSeries(ta,fill_value=3.0,point_fx=POINT_AVERAGE_VALUE)
    no_1.price.value = TimeSeries(ta, fill_value=3.0, point_fx=POINT_AVERAGE_VALUE)
    no_1.load.value = TimeSeries(ta, fill_value=1300.0, point_fx=POINT_AVERAGE_VALUE)
    no_1.max_buy.value = TimeSeries('shyft://stm/no_1/max_buy_mw',z)
    no_1.max_sale.value = TimeSeries('shyft://stm/no_1/max_sale_mw',z)
    mega = 1e6
    no_1.supply.bids.value = create_t_xy(time('2018-10-17T10:00:00Z'),
                    XyPointCurve(PointList([Point(1000.0*mega, 10.0/mega),
                                            Point(500.0*mega, 8.0/mega),
                                            Point(200.0*mega, 6.0/mega),
                                            Point(100.0*mega, 3.0/mega),
                                            Point(50.0*mega, 1.0/mega)])))
    no_1.demand.bids.value = create_t_xy(time('2018-10-17T10:00:00Z'),
                    XyPointCurve(PointList([Point(1000.0*mega, 10.0/mega),
                                            Point(500.0*mega, 8.0/mega),
                                            Point(200.0*mega, 6.0/mega),
                                            Point(100.0*mega, 3.0/mega),
                                            Point(50.0*mega, 1.0/mega)])))

    ug = a.add_unit_group(1, "ug1", "{}")
    ug.group_type = UnitGroup.unit_group_type.FCR_N_UP
    assert ug
    assert a.unit_groups
    assert len(a.unit_groups) == 1
    u = a.hydro_power_systems[0].find_unit_by_id(1)
    ug.add_unit(u,TimeSeries('shyft://stm/ug1/membership',z))
    assert ug.members
    ug.remove_unit(u)
    assert len(ug.members) == 0
    ug.add_unit(u, TimeSeries('shyft://stm/ug1/membership',z))
    assert len(ug.members) == 1
    a.market_areas.append(no_1)
    a_blob = a.to_blob()
    b = StmSystem.from_blob(a_blob)
    assert b
    assert b.id == a.id
    assert b.name == a.name
    assert b.json == a.json
    assert len(b.hydro_power_systems) == len(a.hydro_power_systems)
    assert len(b.market_areas) == len(a.market_areas)
    assert len(b.unit_groups) == len(a.unit_groups)
    assert len(b.unit_groups[0].members) == len(a.unit_groups[0].members)
    assert b.unit_groups[0].group_type == a.unit_groups[0].group_type
    assert a.market_areas == b.market_areas
    assert a==b
    # change no_1 demand, just to check
    no_1.demand.bids.value = create_t_xy(time('2018-10-17T10:20:30Z'),
                    XyPointCurve(PointList([Point(1000.0*mega, 10.0/mega),
                                            Point(500.0*mega, 8.0/mega),
                                            Point(200.0*mega, 6.0/mega),
                                            Point(100.0*mega, 3.0/mega),
                                            Point(50.0*mega, 1.0/mega)])))
    assert a.market_areas != b.market_areas

def test_stm_systm_list():
    a=StmSystemList([StmSystem(1, "A", "{}"),StmSystem(2,"B","{}")])
    b = StmSystemList([StmSystem(1, "A", "{}"), StmSystem(2, "B", "{}")])
    assert a==b
    assert not a!=b
    b.append(StmSystem(3,"C","{}"))
    assert a!=b

def test_stm_system_proxy_attributes():
    """ Verify the proxy attributes in StmSystem"""
    a = StmSystem(1, "A", "{misc}")
    rp = a.run_parameters
    assert str(rp)
    assert repr(rp)
    assert rp.n_inc_runs == 0
    assert not rp.run_time_axis.exists
    rp.n_inc_runs = 2
    rp.run_time_axis = TimeAxis(time('2018-01-01T00:00:00Z'), time(3600), 240)
    assert rp.n_inc_runs == 2
    assert rp.run_time_axis.exists
    a_blob = a.to_blob()
    b = StmSystem.from_blob(a_blob)
    assert b
    assert b.run_parameters.n_inc_runs == rp.n_inc_runs
    assert b.run_parameters.run_time_axis == rp.run_time_axis


def test_hps_can_find_by_name():
    s = create_test_hydro_power_system(hps_id=1, name='hps')
    w=s.find_waterway_by_id(1)
    assert w and 'saurdal' in w.name
    g1 = w.add_gate(11,'tunx_g1') # we can add gate to a model
    assert g1
    assert not g1.opening.schedule.exists, 'check that we got stm gate'
    p=s.find_power_plant_by_id(1)
    r=s.find_reservoir_by_id(1)
    ra=s.find_reservoir_aggregate_by_id(1)
    u=s.find_unit_by_id(165061)
    assert p and r and u and ra
    assert p.id == s.find_power_plant_by_name(p.name).id
    assert ra.id == s.find_reservoir_aggregate_by_name(ra.name).id
    assert r.id == s.find_reservoir_by_name(r.name).id
    assert u.id == s.find_unit_by_name(u.name).id
    # ensure we got real stm objects back
    assert not p.outlet_level.exists
    assert not r.level.regulation_max.exists
    assert not u.production.result.exists
    assert not w.discharge.result.exists


def test_reservoir_aggregate():
    s = create_test_hydro_power_system(hps_id=1, name='hps')
    a = HydroPowerSystem(1, 'a')
    r1 = a.create_reservoir(1, "r1", "{'a':1}")
    r2 = a.create_reservoir(2, "r2", "{'a':1}")
    ra = a.create_reservoir_aggregate(1, "ra", "{}")
    assert ra
    ra.add_reservoir(r1)
    assert len(ra.reservoirs) == 1
    assert ra.reservoirs[-1] == r1
    ra.add_reservoir(r2)
    assert len(ra.reservoirs) == 2
    assert ra.reservoirs[-1] == r2
    ra.remove_reservoir(r2)
    assert len(ra.reservoirs) == 1
    assert ra.reservoirs[-1] == r1
    ra.remove_reservoir(r1)
    assert len(ra.reservoirs) == 0


def test_attribute_keep_alive():
    """Test that the attribute data is kept alive while the Python attribute object exists"""
    rsv = create_test_hydro_power_system(hps_id=1, name='hps').reservoirs[0]
    pass


def test_operator_eq_for_hps_wtr():
    """
    Ref issue #760 as reported  Diako, wtr did not have operator== defined,
    thus some algos on py hps traversal did not work.
    PLEASE NOTE: id(x) is not reliable for any x in py-exposed libraries,
    as it reflects the id of the py-proxy/wrapper, and not the underlying cpp
    exposed object.
    We leave the demo/test here for a reminder(easy to forget)

    """
    hps = HydroPowerSystem(1, 'test')
    rsv_1 = hps.create_reservoir(1, 'rsv_1')
    rsv_2 = hps.create_reservoir(2, 'rsv_2')
    wtr_1 = hps.create_tunnel(1, 'wtr_1').input_from(rsv_1)
    wtr_2 = hps.create_tunnel(2, 'wtr_2').input_from(rsv_2)
    wtr_3 = hps.create_tunnel(3, 'wtr_3').input_from(wtr_1).input_from(wtr_2)
    wtr_4 = hps.create_tunnel(4, 'wtr_4').input_from(wtr_3)
    wtr_5 = hps.create_tunnel(5, 'wtr_5').input_from(wtr_3)
    unit_1 = hps.create_unit(1, 'unit_1').input_from(wtr_4)
    unit_2 = hps.create_unit(2, 'unit_2').input_from(wtr_5)
    wtr_6 = hps.create_tunnel(6, 'wtr_6').input_from(unit_1)
    wtr_7 = hps.create_tunnel(7, 'wtr_7').input_from(unit_2)
    rsv_3 = hps.create_reservoir(3, 'rsv_3').input_from(wtr_6).input_from(wtr_7)
    my_wtr = hps.water_routes[0]  # returns a proxy python object for wtr1
    assert my_wtr.upstreams[0].role == ConnectionRole.input
    assert isinstance(my_wtr.upstreams[0].target, Reservoir)
    assert my_wtr.upstreams[0].target.downstreams[0].role == ConnectionRole.main
    my_wtr_too = my_wtr.upstreams[0].target.downstreams[0].target  # returns A NEW py proxy for wtr1
    assert my_wtr_too.name == my_wtr.name  # yes it's the same object
    assert my_wtr_too == my_wtr  # and now we can compare safely on wtr objects!

def test_transmission_lines_to_market_area():
    sys = StmSystem(1, "A", "{}")
    n = sys.create_network(4, 'Network', '{}')

    mkt1 = sys.create_market_area(2, 'MarketArea #1', '{}')
    mkt2 = sys.create_market_area(3, 'MarketArea #2', '{}')

    #    ___________________                             ___________________
    #   |  Energy Market 1  |                           | Energy Market 2   |
    #   |                   |                           |                   |
    #   |   |Busbar1| ------>----|TransmissionLine1|---->---- |Busbar2|     |
    #   |                   |                           |                   |
    #   |   |Busbar3| ------<----|TransmissionLine2|----<-----|Busbar4|     |
    #   |___________________|                           |___________________|
    

    b1 = n.create_busbar(21, 'Busbar #1')
    b2 = n.create_busbar(22, 'Busbar #2')
    b3 = n.create_busbar(23, 'Busbar #3')
    b4 = n.create_busbar(24, 'Busbar #4')

    t1 = n.create_transmission_line(11, 'TransmissionLine #1')
    t2 = n.create_transmission_line(12, 'TransmissionLine #2')

    b1.add_to_market_area(mkt1)
    b3.add_to_market_area(mkt1)
    b2.add_to_market_area(mkt2)
    b4.add_to_market_area(mkt2)

    b1.add_to_start_of_transmission_line(t1)
    b2.add_to_end_of_transmission_line(t1)
    b4.add_to_start_of_transmission_line(t2)
    b3.add_to_end_of_transmission_line(t2)

    t_lines = mkt1.transmission_lines_to(mkt2)
    assert len(t_lines) == 1
    assert t_lines[0] == t1

    t_lines = mkt1.transmission_lines_from(mkt2)
    assert len(t_lines) == 1
    assert t_lines[0] == t2

    t_lines = mkt2.transmission_lines_to(mkt1)
    assert len(t_lines) == 1
    assert t_lines[0] == t2

    t_lines = mkt2.transmission_lines_from(mkt1)
    assert len(t_lines) == 1
    assert t_lines[0] == t1

    # Add transmission line from b1 to b4
    t3 = n.create_transmission_line(13, 'TransmissionLine #3')
    b1.add_to_start_of_transmission_line(t3)
    b4.add_to_end_of_transmission_line(t3)
    t_lines = mkt1.transmission_lines_to(mkt2)
    assert len(t_lines) == 2
    assert t_lines[0] == t1
    assert t_lines[1] == t3

def test_create_busbar_derived_unit_group():
    sys = StmSystem(1, "A", "{}")
    n = sys.create_network(4, 'Network', '{}')
    mkt = sys.create_market_area(2, 'MarketArea #1', '{}')
    b1 = n.create_busbar(21, 'Busbar #1')
    b1.add_to_market_area(mkt)
    unit_group = mkt.create_busbar_derived_unit_group(9, "NO1", '{}')

    assert len(unit_group.members) == 0

     # Add units to busbar, verify busbar_derived_unit_group is updated
    hps = HydroPowerSystem(1, 'test')
    unit_1 = hps.create_unit(1, 'unit_1')

    b1.add_unit(unit_1, TimeSeries())
    assert len(unit_group.members) == 1
    assert unit_group.members[0].unit == unit_1
    b1.remove_unit(unit_1)
    assert len(unit_group.members) == 0

def test_get_import_get_export():
    sys = StmSystem(1, "A", "{}")
    n = sys.create_network(4, 'Network', '{}')
    mkt = sys.create_market_area(2, 'MarketArea #1', '{}')
    b1 = n.create_busbar(21, 'Busbar #1')
    b2 = n.create_busbar(22, 'Busbar #2')
    b1.add_to_market_area(mkt)
    b2.add_to_market_area(mkt)

    ta = TimeAxis(0, 1, 4)
    b1.flow.result = TimeSeries(ta, [ 2, 2, 3,-3], point_fx=POINT_AVERAGE_VALUE)
    b2.flow.result = TimeSeries(ta, [-1,-3, 4, 0], point_fx=POINT_AVERAGE_VALUE)

    tot_import = mkt.get_import()
    assert tot_import.result == TimeSeries(ta, [1, 0, 7, 0], point_fx=POINT_AVERAGE_VALUE)

    tot_export = mkt.get_export()
    assert tot_export.result == TimeSeries(ta, [0, 1, 0, 3], point_fx=POINT_AVERAGE_VALUE)

def test_get_production_get_consumption():
    sys = StmSystem(1, "A", "{}")
    n = sys.create_network(2, 'Network', '{}')
    mkt = sys.create_market_area(3, 'MarketArea #1', '{}')
    b1 = n.create_busbar(21, 'Busbar #1')
    b2 = n.create_busbar(22, 'Busbar #2')
    b1.add_to_market_area(mkt)
    b2.add_to_market_area(mkt)

    ta = TimeAxis(0, 1, 4)

    hps = HydroPowerSystem(10, 'test')
    unit_1 = hps.create_unit(11, 'unit_1')

    pm = sys.create_power_module(31, 'PowerModule #1', '{}')

    active = TimeSeries()
    b1.add_unit(unit_1, active)
    b2.add_power_module(pm, active)

    # note: consumption == negative, production == positive
    unit_1.production.result = TimeSeries(ta, [ 2, 2, 0,-1], point_fx=POINT_AVERAGE_VALUE)
    pm.power.result =          TimeSeries(ta, [-2,-6, 1, 0], point_fx=POINT_AVERAGE_VALUE)

    production = mkt.get_production()
    assert production.result == TimeSeries(ta, [2, 2, 1, 0], point_fx=POINT_AVERAGE_VALUE)
    assert len(production.realised.values) == 0
    assert len(production.schedule.values) == 0

    consumption = mkt.get_consumption()
    # note: consumption is presented as positive values
    assert consumption.result == TimeSeries(ta, [2, 6, 0, 1], point_fx=POINT_AVERAGE_VALUE)
    assert len(consumption.realised.values) == 0
    assert len(consumption.schedule.values) == 0
