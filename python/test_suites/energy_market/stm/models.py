from shyft.energy_market.stm import HydroPowerSystem, StmSystem, MarketArea, PowerModule, Network, Busbar, TransmissionLine, UnitGroup, Contract, ContractPortfolio
from shyft.energy_market.core import ConnectionRole
from shyft.energy_market.core import Point, PointList, XyPointCurve, XyPointCurveWithZ
from shyft.time_series import time, TimeAxis, TimeSeries, point_interpretation_policy
from shyft.energy_market.stm.utilities import create_t_xy, create_t_double, create_t_turbine_description, create_t_xyz_list


def create_test_hydro_power_system(*, hps_id: int = 1, name: str = 'ulla-førre') -> HydroPowerSystem:
    """
    Demonstrates how to build an inmemory representation of  HydroPowerSystem
    corresponding to the term 'detailed-hydro' in EMPS,
    using part of the blåsjø/ulla-førre systems

    Returns
    -------
    HydroPowerSystem with reservoirs, tunnels, powerplants, including pumps
    """
    hps = HydroPowerSystem(hps_id, name)
    t0 = time('2000-01-01T00:00:00Z')
    # How to create a reservoir, using blåsjø as example
    # 1. create the reservoir, with unique id and name
    blasjo1 = hps.create_reservoir(16606, 'blåsjø1')
    blasjo1.volume_level_mapping = create_t_xy(t0, XyPointCurve(PointList([Point(0.0, 1000.0), Point(1200.0, 2000.0), Point(1350.0, 3100.0)])))
    blasjo1.level.regulation_max = create_t_double(t0, 1349.0)
    blasjo1.level.regulation_min = create_t_double(t0, 1000.0)
    # done !
    blasjo2 = hps.create_reservoir(16607, 'blåsjø2')
    blasjo = hps.create_reservoir_aggregate(1,'blåsjø')
    blasjo.add_reservoir(blasjo1)
    blasjo.add_reservoir(blasjo2)

    saurdal_a1 = hps.create_aggregate(1, 'saurdal_a1')
    saurdal = hps.create_power_station(1, 'saurdal')
    saurdal.add_unit(saurdal_a1)  # add the aggregate to the station.
    tunx = hps.create_tunnel(1, 'blåsjø-saurdal_a1')
    blasjo1.output_to(tunx, ConnectionRole.main)
    blasjo_flood = hps.create_river(2, 'blåsjø_flood')
    blasjo_gt = blasjo_flood.add_gate(3, "blåsjø.gate 2")
    blasjo_gt.flow_description = create_t_xyz_list(t0, XyPointCurve(PointList([Point(1349.0, 0.0), Point(1349.5, 20.0), Point(1351.0, 200.0)])), 0.0)
    blasjo1.output_to(blasjo_flood, ConnectionRole.flood)

    saurdal_a1.input_from(tunx)
    sandsavatn = hps.create_reservoir(16602, 'sandsvatn')
    lauvastolsvatn = hps.create_reservoir(16603, 'lauvastølsvatn')
    kvilldal = hps.create_unit(165061, 'kvilldal', '{"info":"Ek=1.3,520m,4x310 MW max,260 m3/s,utløp=70moh"}')
    # create a turbine description with one TurbineEffiency ..
    kvilldal.turbine_description = create_t_turbine_description(t0,
        [XyPointCurveWithZ(
            XyPointCurve(PointList([Point(10.0, 0.6), Point(15.0, 0.8), Point(20.0, 0.7)])),
            400.0)
        ])
    # done, kvilldal now have a turbine eff. description
    kvilldal2 = hps.create_aggregate(165062, 'kvilldal2')

    t_kvilldal = hps.create_tunnel(100, 'kvilldal hovedtunnel', '{"alpha":0.000053}')
    t_kvilldal_g1=t_kvilldal.add_gate(1,"kvilldal.gate 1")
    assert t_kvilldal_g1.waterway and t_kvilldal_g1.waterway.name==t_kvilldal.name, 'ensure weak ptr mapping works'
    kvilldal_penstock_1 = hps.create_tunnel(101, 'kvilldal penstock 1', '{"alpha":0.000053}')
    kvilldal_penstock_2 = hps.create_tunnel(102, 'kvilldal penstock 2', '{"alpha":0.000053}')
    t_saur_kvill = hps.create_tunnel(103, 'saurdal_a1-kvilldal-hoved-tunnel')
    t_sandsa_kvill = hps.create_tunnel(104, 'sandsavatn-til-kvilldal')
    t_lauvas_kvill = hps.create_tunnel(105, 'lauvastølsvatn-til-kvilldal')
    t_lauvas_kvill.add_gate(2, '#2', "{'type':'binary'}")
    t_saur_kvill.input_from(saurdal_a1).output_to(t_kvilldal)
    t_kvilldal.output_to(kvilldal_penstock_1)
    t_kvilldal.output_to(kvilldal_penstock_2)
    kvilldal_penstock_1.output_to(kvilldal)
    kvilldal_penstock_2.output_to(kvilldal2)

    t_sandsa_kvill.input_from(sandsavatn).output_to(t_kvilldal)
    t_lauvas_kvill.input_from(lauvastolsvatn).output_to(t_kvilldal)

    vassbotvatn = hps.create_reservoir(106, 'vassbotvatn')
    stoelsdal_pumpe = hps.create_aggregate(16510, 'stølsdal pumpe', '{"info":"pump-curve[[120,2.1],[145,1.6],[151,1.5]],p_avg=6MW"}')

    hps.create_tunnel(107, 'stølsdals kraftstasjon(pumpe) til vassbotvatn det pumpes fra') \
        .input_from(stoelsdal_pumpe) \
        .output_to(vassbotvatn)
    hps.create_tunnel(108, 'fra sandsvatn til stølsdal pump') \
        .input_from(sandsavatn) \
        .output_to(stoelsdal_pumpe)

    suldalsvatn = hps.create_reservoir(16500, 'suldalsvatn')
    hylen = hps.create_aggregate(16501, 'hylen', '{"info":"uid:16508,2x80MW,2x95m3/s,Ek=0.165,utløps=0.0moh,nom fallh=66m"}')
    hps.create_river(200, 'fra kvilldal til suldalsvatn', '{"max_cap":260}') \
        .input_from(kvilldal) \
        .input_from(kvilldal2) \
        .output_to(suldalsvatn)

    hps.create_tunnel(1107, 'hylen-tunnel', '{"max_cap":275}') \
        .input_from(suldalsvatn) \
        .output_to(hylen)

    havet = hps.create_reservoir(1, 'havet')

    hps.create_river(1018, 'utløp hylen', '{"max_cap:275,"alpha":0.000003}') \
        .input_from(hylen) \
        .output_to(havet)

    hps.create_river(1109, 'bypass suldal til havet', '{"max_cap":30}') \
        .input_from(suldalsvatn, ConnectionRole.bypass) \
        .output_to(havet)

    hps.create_river(1110, 'flom suldal til havet', '{"max_cap":10000}') \
        .input_from(suldalsvatn, ConnectionRole.flood) \
        .output_to(havet)
    return hps


def create_test_hydro_power_system_for_regression_old_data_test(*, hps_id: int = 1, name: str = 'ulla-førre') -> HydroPowerSystem:
    """
    Demonstrates how to build an inmemory representation of  HydroPowerSystem
    corresponding to the term 'detailed-hydro' in EMPS,
    using part of the blåsjø/ulla-førre systems

    Returns
    -------
    HydroPowerSystem with reservoirs, tunnels, powerplants, including pumps
    """
    hps = HydroPowerSystem(hps_id, name)
    t0 = time('2000-01-01T00:00:00Z')
    # How to create a reservoir, using blåsjø as example
    # 1. create the reservoir, with unique id and name
    blasjo = hps.create_reservoir(16606, 'blåsjø')
    blasjo.volume_level_mapping.value = create_t_xy(t0, XyPointCurve(PointList([Point(0.0, 1000.0), Point(1200.0, 2000.0), Point(1350.0, 3100.0)])))
    blasjo.level.regulation_max.value = create_t_double(t0, 1349.0)
    blasjo.level.regulation_min.value = create_t_double(t0, 1000.0)

    blasjo_flood = hps.create_river(2, "blåsjø flood")
    blasjo_gt = blasjo_flood.add_gate(3, "blåsjo.flood 2")
    blasjo_gt.flow_description.value = create_t_xyz_list(t0, XyPointCurve(PointList([Point(1349.0, 0.0), Point(1349.5, 20.0), Point(1351.0, 200.0)])), 0.0)
    blasjo.output_to(blasjo_flood, ConnectionRole.flood)
    # done !

    saurdal_a1 = hps.create_aggregate(1, 'saurdal_a1')
    saurdal = hps.create_power_station(1, 'saurdal')
    saurdal.add_unit(saurdal_a1)  # add the aggregate to the station.
    tunx = hps.create_tunnel(1, 'blåsjø-saurdal_a1')
    blasjo.output_to(tunx, ConnectionRole.main)
    saurdal_a1.input_from(tunx)
    sandsavatn = hps.create_reservoir(16602, 'sandsvatn')
    lauvastolsvatn = hps.create_reservoir(16603, 'lauvastølsvatn')
    kvilldal = hps.create_unit(165061, 'kvilldal', '{"info":"Ek=1.3,520m,4x310 MW max,260 m3/s, utløp=70moh"}')
    # create a turbine description with one TurbineEffiency ..
    kvilldal.turbine_description.value = create_t_turbine_description(t0,
        [XyPointCurveWithZ(
            XyPointCurve(PointList([Point(10.0, 0.6), Point(15.0, 0.8), Point(20.0, 0.7)])),
            400.0)
        ])
    # done, kvilldal now have a turbine eff. description
    kvilldal2 = hps.create_aggregate(165062, 'kvilldal2')

    t_kvilldal = hps.create_tunnel(100, 'kvilldal hovedtunnel', '{"alpha":0.000053}')
    kvilldal_penstock_1 = hps.create_tunnel(101, 'kvilldal penstock 1', '{"alpha":0.000053}')
    kvilldal_penstock_2 = hps.create_tunnel(102, 'kvilldal penstock 2', '{"alpha":0.000053}')
    t_saur_kvill = hps.create_tunnel(103, 'saurdal_a1-kvilldal-hoved-tunnel')
    t_sandsa_kvill = hps.create_tunnel(104, 'sandsavatn-til-kvilldal')
    t_lauvas_kvill = hps.create_tunnel(105, 'lauvastølsvatn-til-kvilldal')
    t_lauvas_kvill.add_gate(1, '#1', "{'type':'binary'}")
    t_saur_kvill.input_from(saurdal_a1).output_to(t_kvilldal)
    t_kvilldal.output_to(kvilldal_penstock_1)
    t_kvilldal.output_to(kvilldal_penstock_2)
    kvilldal_penstock_1.output_to(kvilldal)
    kvilldal_penstock_2.output_to(kvilldal2)

    t_sandsa_kvill.input_from(sandsavatn).output_to(t_kvilldal)
    t_lauvas_kvill.input_from(lauvastolsvatn).output_to(t_kvilldal)

    vassbotvatn = hps.create_reservoir(106, 'vassbotvatn')
    stoelsdal_pumpe = hps.create_aggregate(16510, 'stølsdal pumpe', '{"info":"pump-curve[[120,2.1],[145,1.6],[151,1.5]], p_avg=6MW"}')

    hps.create_tunnel(107, 'stølsdals kraftstasjon(pumpe) til vassbotvatn det pumpes fra') \
        .input_from(stoelsdal_pumpe) \
        .output_to(vassbotvatn)
    hps.create_tunnel(108, 'fra sandsvatn til stølsdal pump') \
        .input_from(sandsavatn) \
        .output_to(stoelsdal_pumpe)

    suldalsvatn = hps.create_reservoir(16500, 'suldalsvatn')
    hylen = hps.create_aggregate(16501, 'hylen', '{"info":"uid:16508, 2x80MW,2x95m3/s, Ek=0.165, utløps=0.0moh, nom fallh=66m"}')
    hps.create_river(200, 'fra kvilldal til suldalsvatn', '{"max_cap":260}') \
        .input_from(kvilldal) \
        .input_from(kvilldal2) \
        .output_to(suldalsvatn)

    hps.create_tunnel(1107, 'hylen-tunnel', '{"max_cap":275}') \
        .input_from(suldalsvatn) \
        .output_to(hylen)

    havet = hps.create_reservoir(1, 'havet')

    hps.create_river(1018, 'utløp hylen', '{"max_cap":275,"alpha":0.000003}') \
        .input_from(hylen) \
        .output_to(havet)

    hps.create_river(1109, 'bypass suldal til havet', '{"max_cap":30}') \
        .input_from(suldalsvatn, ConnectionRole.bypass) \
        .output_to(havet)

    hps.create_river(1110, 'flom suldal til havet', '{"max_cap":10000}') \
        .input_from(suldalsvatn, ConnectionRole.flood) \
        .output_to(havet)
    return hps


def create_complete_stm_system(stm_id: int=1, name: str='a', json: str='{}') -> StmSystem:
    sys = StmSystem(uid=stm_id, name=name, json=json)
    sys.hydro_power_systems.append(create_test_hydro_power_system_for_regression_old_data_test(hps_id=1, name='ulla-førre'))
    no_1 = MarketArea(1, 'NO1', '{}', sys)
    no_1.price.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=3.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    no_1.load.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=1300.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    some_ts=TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=1.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    #no_1.max_buy.value = TimeSeries('shyft://stm/no_1/max_buy_mw')
    #no_1.max_sale.value = TimeSeries('shyft://stm/no_1/max_sale_mw')
    sys.market_areas.append(no_1)
    # Unit group
    ug = sys.add_unit_group(1, "ug1", "{}")
    ug.group_type = UnitGroup.unit_group_type.PRODUCTION
    ug.production = some_ts
    u = sys.hydro_power_systems[0].find_unit_by_id(1)
    ug.add_unit(u, TimeSeries())

    # PowerModules..
    pm1 = PowerModule(1, "PowerModule #1", "{}",sys)
    pm1.power.result = some_ts
    sys.power_modules.append(pm1)
    #
    # contracts..
    #
    a = Contract(3, 'Contract #1', '{}', sys)
    b = Contract(3, 'Contract #1', '{}', sys)
    c = Contract(4, 'hmm', '{}', sys)
    c.power_plants.append(u.power_plant)
    sys.contracts.extend([a, b, c])
    #
    # contract portfolio
    #
    pa = ContractPortfolio(2, 'Portfolio A', '{}', sys)
    sys.contract_portfolios.append(pa)
    # not yet equal, most likely comparison operator for portfolios? a.add_to_portfolio(pa)

    return sys

