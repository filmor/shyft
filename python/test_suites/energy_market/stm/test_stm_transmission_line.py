import pytest
from shyft.energy_market.stm import shyft_with_stm
if not shyft_with_stm:
    pytest.skip('requires shyft_with_stm', allow_module_level=True)
from functools import reduce
from shyft.energy_market.stm import StmSystem

def test_transmission_line_attributes():
    sys = StmSystem(1, "A", "{}")
    n = sys.create_network(2, 'Network', '{}')
    t = n.create_transmission_line(3, 'TransmissionLine #1')
    # generics, from id_base
    assert hasattr(t, "id")
    assert hasattr(t, "name")
    assert hasattr(t, "json")
    # specifics
    assert hasattr(t, "capacity")
    assert t.tag == "/n2/t3"

def test_transmission_line_attributes_flattened():
    sys = StmSystem(1, "A", "{}")
    n = sys.create_network(2, 'Network', '{}')
    t = n.create_transmission_line(3, 'TransmissionLine #1')
    assert t.flattened_attributes()
    for (path, val) in t.flattened_attributes().items():
        attr = reduce(getattr, path.split('.'), t)
        assert type(val) is type(attr)
        assert val == attr
        assert val.url() == attr.url()

def test_transmission_line_association():
    sys = StmSystem(1, "A", "{}")
    n = sys.create_network(2, 'Network', '{}')
    t = n.create_transmission_line(3, 'TransmissionLine #1')
    b1 = n.create_busbar(3, 'Busbar #1')
    b2 = n.create_busbar(4, 'Busbar #2')
    b1.add_to_start_of_transmission_line(t)
    b2.add_to_end_of_transmission_line(t)
    assert t.from_bb == b1
    assert t.to_bb == b2

def test_transmission_line_equal():
    sys = StmSystem(1, "A", "{}")
    # Create two networks, to compare transmission lines
    n = sys.create_network(2, 'Network #1', '{}')
    n2 = sys.create_network(3, 'Network #2', '{}')

    t = n.create_transmission_line(4, 'TransmissionLine #1')
    t_2 = n2.create_transmission_line(4, 'TransmissionLine #1')
    t_3 = n.create_transmission_line(5, 'hmm')

    assert t == t
    assert t == t_2
    assert t != t_3
    # Equal busbar associations
    b1 = n.create_busbar(6, 'Busbar #1')
    b1_2 = n2.create_busbar(6, 'Busbar #1')
    b1.add_to_start_of_transmission_line(t)
    assert t != t_2
    b1_2.add_to_start_of_transmission_line(t_2)
    assert t == t_2