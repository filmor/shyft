import numpy as np
import pytest
from shyft.energy_market.stm import shyft_with_stm
if not shyft_with_stm:
    pytest.skip('requires shyft_with_stm', allow_module_level=True)
from shyft.energy_market.stm import StmSystem, MarketArea, HydroPowerSystem, UnitGroup, HydroPowerSystem, t_xy, t_xyz_list, shop
from shyft.energy_market.core import XyPointCurve, PointList, Point, XyPointCurveWithZ, XyPointCurveWithZList, \
    ConnectionRole
from shyft.energy_market.stm.utilities import create_t_turbine_description, create_t_xy, \
    create_t_xyz_list
from shyft.time_series import time, TimeAxis, Calendar, TimeSeries, POINT_AVERAGE_VALUE, max_utctime, deltahours

def create_t_double(t0: time, v: float) -> TimeSeries:
    return TimeSeries(TimeAxis([t0, max_utctime]), v, POINT_AVERAGE_VALUE)

def _create_prod_unit_id(plant_id: int, unit_number: int):
    assert 0 < unit_number < 10
    return plant_id*10 + unit_number

def create_leirdola_hps() -> HydroPowerSystem:
    """
    Test and learn how to model hydro power system with new stm-model in python
    """
    M = 1e6
    hps = HydroPowerSystem(10014, "Leirdøla")
    t0 = time(0)  # time("2000-01-01T00:00:00Z")

    module_id = 31103

    # region Reservoirs ------------------------------------------------------------------------------------------------
    rsv_tunsberg = hps.create_reservoir(module_id, "Tunsbergsdalsvatn")

    # Lowest regulated water level
    rsv_tunsberg.level.regulation_min.value = create_t_double(t0, 0.0)
    # Highest regulated water level
    rsv_tunsberg.level.regulation_max.value = create_t_double(t0, 500.0)

    # Volume curve [m3, masl]:
    rsv_tunsberg.volume_level_mapping.value = create_t_xy(t0, XyPointCurve(PointList([
        Point(129.111*M, 473.0),
        Point(151.831*M, 476.0),
        Point(185.8*M, 480.0)
    ])))

    # endregion

    # region Units -----------------------------------------------------------------------------------------------------

    leirdola_G1 = hps.create_unit(_create_prod_unit_id(module_id, 1), 'Leirdøla G1')

    # Production limits
    leirdola_G1.production.static_max.value = create_t_double(t0, 100.0*M)
    leirdola_G1.production.static_min.value = create_t_double(t0,   0.0*M)

    # Generator efficiency (W, %)
    leirdola_G1.generator_description.value = create_t_xy(t0, XyPointCurve(PointList([
        Point(40.0*M, 80),
        Point(50.0*M, 85)
    ])))

    # Turbine description (m3/s, %)
    leirdola_G1.turbine_description.value = create_t_turbine_description(t0, [
        XyPointCurveWithZ(XyPointCurve(PointList([
            Point(15.00000, 80),
            Point(30.15000, 85)
        ])), 200)
    ])

    # Start and stop cost
    leirdola_G1.cost.start.value = TimeSeries(
        TimeAxis(time('2010-01-01T00:00:00Z'), deltahours(8760), 25), fill_value=10.0, point_fx=POINT_AVERAGE_VALUE)
    leirdola_G1.cost.stop.value = TimeSeries(
        TimeAxis(time('2010-01-01T00:00:00Z'), deltahours(8760), 25), fill_value=10.0, point_fx=POINT_AVERAGE_VALUE)

    # endregion

    # region Power plants ----------------------------------------------------------------------------------------------
    leirdola_pp = hps.create_power_plant(module_id, 'Leirdøla')
    leirdola_pp.add_unit(leirdola_G1)

    # Outlet level
    leirdola_pp.outlet_level.value = create_t_double(t0, 11.5)  # Outlet level might to be moved to waterway object
    # endregion

    # region Waterways -------------------------------------------------------------------------------------------------
    ww_main = hps.create_waterway(311030, 'w_Tunsbergsdalvatn_to_Leirdøla')
    flood = hps.create_river(3110331, 'f_Tunsbergsdalsvatn_Havet')
    bypass = hps.create_waterway(3110321, 'b_Tunsbergsdalsvatn_Havet')
    pen1 = hps.create_waterway(311033, 'Penstock 1 Leirdøla')
    tr1 = hps.create_waterway(311034, 'Tail race for Leirdøla')

    # Head loss coefficients
    ww_main.head_loss_coeff.value = create_t_double(t0, 0.0070)
    pen1.head_loss_coeff.value = create_t_double(t0, 0.00547)

    # Waterway connections
    ww_main.input_from(rsv_tunsberg)
    pen1.input_from(ww_main).output_to(leirdola_G1)
    tr1.input_from(leirdola_G1)

    flood.input_from(rsv_tunsberg, ConnectionRole.flood)
    bypass.input_from(rsv_tunsberg, ConnectionRole.bypass)
    # endregion

    # region Gates -----------------------------------------------------------------------------------------------------
    flood_gate = flood.add_gate(flood.id*10 + 1, 'f_Tunsbergsdalsvatn_Havet_gate')
    # # Spill description (List[(m, m3/s)], gate-opening [%])
    flood_gate.flow_description.value = create_t_xyz_list(t0,
                                                          XyPointCurve(PointList([
                                                              Point(478.0, 0.0),
                                                              Point(478.66, 108.0),
                                                              Point(479.34, 313.0),
                                                              Point(480.0, 571.0)])),
                                                          100)
    # ww_main.add_gate(ww_main.id*10 + 0, 'w_Tunsbergsdalsvatn_to_Leirdøla_gate', '{}')
    bypass_gate = bypass.add_gate(bypass.id*10 + 1, 'b_Tunsbergsdalsvatn_Havet_gate', '{}')
    # endregion
    return hps


@pytest.mark.skipif(shop.shyft_with_shop == False, reason="requires shyft with SHOP")
def test_time_delay():
    hps = create_leirdola_hps()
    r_ds = hps.create_reservoir(1, 'Downstream reservoir')
    t0 = time("2022-01-01T00:00:00Z")  # Time of first existence

    # Create volume-level mapping
    vol_lvl_map = t_xy()
    xs = [0, 10e6]  # m3
    ys = [0, 11]  # m
    vol_lvl_map[t0] = XyPointCurve(PointList([Point(x, y) for x, y in zip(xs, ys)]))
    r_ds.volume_level_mapping.value = vol_lvl_map

    bypass = hps.find_waterway_by_name("b_Tunsbergsdalsvatn_Havet")
    spillway = hps.find_waterway_by_name("f_Tunsbergsdalsvatn_Havet")
    tail_race = hps.find_waterway_by_name("Tail race for Leirdøla")
    
    for ww in [bypass, spillway, tail_race]:
        ww.output_to(r_ds)

    # Time axis
    # Define analysis time axis, hourly resolution, 24 hrs horizon
    ta_run = TimeAxis(time("2022-03-01T00:00:00Z"), Calendar.HOUR, 24)

    # Unit
    u = hps.find_unit_by_id(311031)

    # Power plant
    p = hps.find_power_plant_by_name("Leirdøla")
    p.discharge.schedule.value = TimeSeries(ta_run, 1e5 / 3600, POINT_AVERAGE_VALUE)  # Discharge schedule of 0.1Mm3/h = 27.77 m3/s

    # Reservoir:
    r = hps.find_reservoir_by_id(31103)
    # Set realised reservoir level (used as initial reservoir level in SHOP)
    r.level.realised.value = TimeSeries(ta_run.slice(0, 1), 475, POINT_AVERAGE_VALUE)
    r.water_value.endpoint_desc.value = TimeSeries(ta_run, 10 / 1e6, POINT_AVERAGE_VALUE)  # Set water value (EUR/Wh)

    # Bypass:
    # Add discharge schedule to the bypass (0.1Mm3/h = 27.77 m3/s)
    bypass.gates[0].discharge.schedule.value = TimeSeries(ta_run, 1e5 / 3600, POINT_AVERAGE_VALUE)

    # Stm:
    leirdola_stm = StmSystem(10014, "Leirdøla", json='')
    leirdola_stm.hydro_power_systems.append(hps)
    leirdola_stm.market_areas.append(MarketArea(uid=100005, name='NO5', json='{}', stm_sys=leirdola_stm))

    # Market:
    m = leirdola_stm.market_areas[0]
    m.price.value = TimeSeries(ta_run, 1 / 1e6, POINT_AVERAGE_VALUE)  # Set market price (EUR/Wh)
    m.max_sale.value = TimeSeries(ta_run, 1000 * 1e6, POINT_AVERAGE_VALUE)  # Set maximum allowed sale in market (W)
    m.max_buy.value = TimeSeries(ta_run, 1000 * 1e6, POINT_AVERAGE_VALUE)  # Set maximum allowed buying in market (W)

    # Create SHOP command list:
    cmds = shop.ShopCommandList([
        shop.ShopCommand.start_sim(3),
        shop.ShopCommand.set_code_incremental(),
        shop.ShopCommand.start_sim(3),
    ])
    # Run commands:
    shop.ShopSystem.optimize(stm_system=leirdola_stm, time_axis=ta_run, commands=cmds, logging_to_stdstreams=False,
                             logging_to_files=False)

    expected_rsv_volumes = [
        [144.258, 144.058, 143.858, 143.658, 143.458, 143.258, 143.058, 142.858],
        [0.0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4]
    ]

    for rsv, expected_volume in zip(hps.reservoirs, expected_rsv_volumes):
        v = np.round(rsv.volume.result.value.values.to_numpy() / 1e6, 3)
        assert all(v[0:8] == np.array(expected_volume))

    # Time delay to bypass
    delay_descr = t_xy()
    dt = 3600
    delay_descr[t0] = XyPointCurve(
        PointList([Point(dt * 0, 0.0), Point(dt * 1, 0.0), Point(dt * 2, 0.0), Point(dt * 3, 0.5),
                   Point(dt * 4, 0.5)]))  # TODO expected to have seconds as x-values in future version
    # delay_descr[t0] = XyPointCurve(PointList([Point(dt*3, 0.5)]))  # Constant 3hr time delay

    bypass.delay.value = delay_descr

    # Re-run SHOP
    shop.ShopSystem.optimize(stm_system=leirdola_stm, time_axis=ta_run, commands=cmds, logging_to_stdstreams=False,
                             logging_to_files=False)

    expected_rsv_volumes = [
        [144.258, 144.058, 143.858, 143.658, 143.458, 143.258, 143.058, 142.858],
        [0.0, 0.1, 0.2, 0.3, 0.45, 0.65, 0.85, 1.05]
    ]

    for rsv, expected_volume in zip(hps.reservoirs, expected_rsv_volumes):
        v = np.round(rsv.volume.result.value.values.to_numpy() / 1e6, 3)
        assert all(v[0:8] == np.array(expected_volume))

    # What if the unit discharge and bypass have a common river as their downstream ad this has a time delay?
    tail_race.disconnect_from(r_ds)
    # Common downstream of plant
    w_tailrace_ds_rsv = hps.create_waterway(1, 'w_tailrace_ds_rsv')
    w_tailrace_ds_rsv.input_from(tail_race).output_to(r_ds)

    for ww in [bypass, spillway]:
        ww.disconnect_from(r_ds)  # Disconnect from downstream reservoir
        ww.output_to(w_tailrace_ds_rsv)  # Connect

    # Remove time delay from bypass
    bypass.delay.remove()

    # Add delay to tailrace
    w_tailrace_ds_rsv.delay.value = delay_descr

    shop.ShopSystem.optimize(stm_system=leirdola_stm, time_axis=ta_run, commands=cmds, logging_to_stdstreams=False,
                             logging_to_files=False)

    expected_rsv_volumes = [
        [144.258, 144.058, 143.858, 143.658, 143.458, 143.258, 143.058, 142.858],
        [0.0, 0.0, 0.0, 0.0, 0.1, 0.3, 0.5, 0.7]
    ]

    for rsv, expected_volume in zip(hps.reservoirs, expected_rsv_volumes):
        v = np.round(rsv.volume.result.value.values.to_numpy() / 1e6, 3)
        assert all(v[0:8] == np.array(expected_volume))
