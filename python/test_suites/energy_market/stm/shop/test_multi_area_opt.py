import math
import numpy as np
import pytest
shop = pytest.importorskip("shyft.energy_market.stm.shop")
if not shop.shyft_with_shop:
    pytest.skip('Skip shop-releated test for non-shop build', allow_module_level=True)

from typing import Tuple
from shyft.energy_market.core import ConnectionRole, Point, PointList, XyPointCurve, XyPointCurveWithZ
from shyft.energy_market.stm import HydroPowerSystem, StmSystem, MarketArea, UnitGroup, t_xy
from shyft.energy_market.stm.utilities import create_t_xy, create_t_double, create_t_turbine_description, create_t_xyz_list
from shyft.time_series import time, TimeSeries, TimeAxis, POINT_AVERAGE_VALUE as stair_case, DoubleVector




def create_multi_area_system(run_ta: TimeAxis, id: int, name: str, n_units: int = 3) -> StmSystem:
    """
    create a demo stm system with load obligation first period, then market-access last period

    """
    _i = [id]

    def uid() -> int:
        """ unique id-generator"""
        _i[0] += 1
        return _i[0]

    mega = 1000000
    sys = StmSystem(id, name, '{}')
    n = len(run_ta)
    mv = n//2*[1.0]
    mv.extend(n//2*[0.0])  # make 1-0 masks that we can multiply to mask in/out values in the periods
    load_period_mask = TimeSeries(run_ta, mv, point_fx=stair_case)
    sale_period_mask = 1.0 - load_period_mask

    def wave_(ta: TimeAxis, offset: float, amplitude: float, n_periods: float = 3.0) -> DoubleVector:
        """ ts-generator: f(t) = offset + amplitude*sin( w*t) , w= .. """
        n = len(ta)
        return DoubleVector([offset + amplitude*math.sin(n_periods*2*3.14*i/n) for i in range(n)])

    def create_offering_table(offering_mask: TimeSeries, n_steps: int, price_average: Tuple[float, float], price_amplitude: float, load_average: Tuple[float, float], load_amplitude: float) -> t_xy:
        def sine_(step: int, n_steps: int, offset: float, aml: float, n_periods: float = 3.0) -> float:
            return offset + aml*math.sin(n_periods*2*3.14*step/n_steps)

        r = t_xy()
        for i in range(len(run_ta)):
            price = sine_(i, len(run_ta), price_average[0], price_amplitude)
            price_d = (price_average[1] - price_average[0])/float(n_steps)
            load = sine_(i, len(run_ta), load_average[0], load_amplitude)*offering_mask(run_ta.time(i))
            load_d = (load_average[1] - load_average[0])/float(n_steps)
            r[run_ta.time(i)] = XyPointCurve(
                PointList([Point(price + s*price_d, load + s*load_d) for s in range(n_steps)])
            )
        return r

    def create_market_area(id: int, name: str, price_average: float, price_amplitude: float, load_average: float, load_amplitude: float) -> MarketArea:
        """ create a market area with load first period,
            then open for market exchange in second period,
            with a  minor load requirement"""
        ma = MarketArea(id, name, '{}', sys)
        ma.price = TimeSeries(run_ta, values=wave_(run_ta, price_average/mega, price_amplitude/mega), point_fx=stair_case)  # the price for the load in base area
        ma.max_buy = sale_period_mask*TimeSeries(run_ta, fill_value=0.0*mega, point_fx=stair_case)  # disallow buy
        ma.max_sale = sale_period_mask*TimeSeries(run_ta, fill_value=0.0*mega, point_fx=stair_case)  # disallow sale in the base market, use demand table instead
        ma.load = (load_period_mask*TimeSeries(run_ta, values=wave_(run_ta, load_average*mega, load_amplitude*mega), point_fx=stair_case)
                   + sale_period_mask*TimeSeries(run_ta, values=wave_(run_ta, 37.0*mega, 2*mega), point_fx=stair_case))
        ma.demand.bids = create_offering_table(sale_period_mask, 2, (1.0*price_average/mega, 1.5*price_average/mega), price_amplitude/mega, (90*mega, 120*mega), 50*mega)
        ma.supply.bids = create_offering_table(load_period_mask, 2, (1.1*price_average/mega, 1.6*price_average/mega), price_amplitude/mega, (30*mega, 50*mega), 5*mega)
        return ma

    def create_hps(name: str, n_units: int, water_value: float) -> HydroPowerSystem:
        hps = HydroPowerSystem(uid(), name)
        t0 = time('2000-01-01T00:00:00Z')  # historical point where this hps was constructed, so time-dependent constant are valid from this time.
        # reservoir ~elevation 80..100 meter, 0..160 Mm3, realised filling is 99 masl
        rsv = hps.create_reservoir(uid(), f'{name}.rsv')
        rsv.volume_level_mapping = create_t_xy(t0, XyPointCurve(PointList([Point(0.0*mega, 80.0), Point(20.0*mega, 90.0), Point(30.0*mega, 95.0), Point(50.0*mega, 100.0), Point(160.0*mega, 105.0)])))
        rsv.level.regulation_max = create_t_double(t0, 100.0)
        rsv.level.regulation_min = create_t_double(t0, 80.0)
        rsv.water_value.endpoint_desc = TimeSeries(run_ta, fill_value=water_value/mega, point_fx=stair_case)
        rsv.level.realised = TimeSeries(run_ta, fill_value=99.0, point_fx=stair_case).time_shift(-run_ta.total_period().timespan() + time(3600))  # make it overlap
        rsv.inflow.schedule = TimeSeries(run_ta, values=wave_(run_ta, 55.0, 10.0), point_fx=stair_case)

        # plant, - with indicated outlet level 10 masl, gives 70..90 masl net head for the units.
        plant = hps.create_power_plant(uid(), f'{name}.plant')
        plant.outlet_level = create_t_double(t0, 10.0)

        # a flood waterroute, with max 150 m3/s, with a gate attached, 0 m3/s @ 100 masl, 150 m3 @ 104 masl
        flood = hps.create_river(uid(), f'{name}.flood')
        flood.discharge.static_max = create_t_double(t0, 150.0)
        gt = flood.add_gate(1, f'{name}.flood_gate', "")
        gt.flow_description = create_t_xyz_list(t0, XyPointCurve([Point(100.0, 0.0), Point(101.5, 25.0), Point(103.0, 80.0), Point(104.0, 150.0)]), 0.0)

        # the main tunnel from reservoir to the unit's inside the plant
        main = hps.create_tunnel(uid(), f'{name}.main')
        main.head_loss_coeff = create_t_double(t0, 0.000030)

        # a common tailrace for the units
        tailrace = hps.create_tunnel(uid(), f'{name}.tailrace')

        # downstream powerplant, a river, that receives the flow from tailrace and the flood gate
        river = hps.create_river(uid(), f'{name}.river')
        for i in range(n_units):
            u = hps.create_unit(uid(), f'{name}.u{i + 1}')
            plant.add_unit(u)
            p_min = 10*mega  #
            p_max = 80*mega  #
            p_nom = p_max
            u.production.static_min = TimeSeries(run_ta, fill_value=p_min, point_fx=stair_case)
            u.production.static_max = TimeSeries(run_ta, fill_value=p_max, point_fx=stair_case)
            u.production.nominal = TimeSeries(run_ta, fill_value=p_nom, point_fx=stair_case)
            # generator and turbine-description (efficiency and operating range)
            u.generator_description = create_t_xy(t0,
                                                  XyPointCurve(PointList(
                                                      [Point(10.0*mega, 96.0), Point(20.0*mega, 96.0), Point(40.0*mega, 98.0), Point(60.0*mega, 99.0), Point(80.0*mega, 98.0)])))
            u.turbine_description = create_t_turbine_description(t0,
                                                                 [XyPointCurveWithZ(
                                                                     XyPointCurve([Point(10.0, 60.0 + i), Point(20.0, 70.0 + i), Point(40.0, 85.0 + i), Point(60.0, 92.0 + i), Point(80.0, 94.0 + i), Point(100.0, 92.0 + i), Point(110.0, 90.0 + i)]),
                                                                     70.0
                                                                 ), XyPointCurveWithZ(
                                                                     XyPointCurve([Point(8.0, 60.0 + i), Point(18.0, 70.0 + i), Point(40.0, 86.0 + i), Point(60.0, 93.0 + i), Point(80.0, 95.0 + i), Point(100.0, 93.0 + i), Point(110.0, 89.0 + i)]),
                                                                     90.0
                                                                 )]

                                                                 )

            penstock = hps.create_tunnel(uid(), f'{name}.penstock{i}')
            penstock.head_loss_coeff.value = create_t_double(t0, 0.00005)
            penstock.input_from(main).output_to(u)
            u.output_to(tailrace)

        # connect reservoir->flood -> river
        flood.input_from(rsv, ConnectionRole.flood).output_to(river)
        main.input_from(rsv)  # reservoir -> tunnel
        tailrace.output_to(river)  # tailrace ->  river
        return hps

    no1 = create_market_area(id=uid(), name='NO1', price_average=36.0, price_amplitude=10.0, load_average=70.0*n_units, load_amplitude=10.0)
    no2 = create_market_area(id=uid(), name='NO2', price_average=136.0, price_amplitude=10.0, load_average=70.0*n_units, load_amplitude=10.0)
    sys.market_areas.append(no1)
    sys.market_areas.append(no2)
    no1.unit_group = sys.add_unit_group(uid(), name=no1.name, json='{"description": "units delivering production into NO1"}', group_type=UnitGroup.unit_group_type.PRODUCTION)
    no2.unit_group = sys.add_unit_group(uid(), name=no2.name, json='{"description": "units delivering production into NO2"}', group_type=UnitGroup.unit_group_type.PRODUCTION)
    hps1 = create_hps('hps1', n_units=n_units, water_value=36.5)
    hps2 = create_hps('hps2', n_units=n_units, water_value=50.5)
    sys.hydro_power_systems.append(hps1)
    sys.hydro_power_systems.append(hps2)
    # add unit market price area association
    active = TimeSeries(run_ta, fill_value=1.0, point_fx=stair_case)  # always active
    for u in hps1.units:
        no1.unit_group.add_unit(u, active)

    for u in hps2.units:
        no2.unit_group.add_unit(u, active)
    # now modify unit in no1 to deliver in no2 during optimization
    no1.unit_group.members[0].active.value = load_period_mask  # only in no1 during load period
    no2.unit_group.add_unit(no1.unit_group.members[0].unit, sale_period_mask)  # over to no2 during sale period
    return sys


def create_optimization_commands(run_id: int, write_files: bool) -> shop.ShopCommandList:
    return shop.ShopCommandList([
        # shop.ShopCommand.set_universal_mip_on(),# takes 4 seconds:
        shop.ShopCommand.set_method_primal(),
        shop.ShopCommand.set_code_full(),
        shop.ShopCommand.start_sim(2),
        shop.ShopCommand.set_universal_mip_on(),
        shop.ShopCommand.start_sim(2),
        shop.ShopCommand.set_code_incremental(),
        shop.ShopCommand.start_sim(3)
    ])


_p_min: float = 0.1  # smallest prod recognized as production
_p_max: float = 1e33  # largest prod recognized as production


def present_demo_results(run_ta: TimeAxis, sys: StmSystem):
    """
    Just print out the input and the results including
    balances for the result.
    """
    MW: float = 1.0/1e6  # just to make reading and scaling easier+consistent

    def show_ts(title: str, ts: TimeSeries, scale: float = 1.0):
        s = ""
        if ts:
            r = TimeSeries(run_ta, fill_value=0.0, point_fx=stair_case)
            xx = ts.use_time_axis_from(r)  # just to resample all values to the run-time-axis
            for v in xx.values:
                if math.isfinite(v):
                    s = s + f"{v*scale:5.1f} "
                else:
                    s = s + "   .  "
        print(f"{title:20}:{s}")

    for market in sys.market_areas:
        print(f"\nEnergy market area {market.name}\n")
        show_ts('load [MW]', market.load.value, MW)
        show_ts('prod [MW]', market.unit_group.production.value*MW)
        show_ts('.diff[MW]', (market.load.value - market.unit_group.production.value)*MW)
        print("--- market")
        show_ts('base.price[x]', market.price.value, 1.0/MW)
        show_ts('obtained.s.price[x]', market.demand.price.result.value, 1.0/MW)
        show_ts('sale[MW]', -market.sale.value*MW)
        show_ts('obtained.b.price[x]', market.supply.price.result.value, 1.0/MW)
        show_ts('buy [MW]', market.buy.value*MW)
        show_ts('mdiff[MW]', (market.load.value - market.unit_group.production.value - market.sale.value - market.buy.value)*MW)

        print("--- details ")
        for ugm in market.unit_group.members:
            show_ts(f'..{ugm.unit.name}[MW]', (ugm.unit.production.result.value*ugm.active.value)*MW)


def test_multi_area_optimisation():
    """ Demo of a simple system, two market areas, NO1, NO2.
        Each with a hydro power system, with one plant, several units.
        We let one unit contribute to NO1 ,then NO2, then NO1
        48 hour simulation period, hourly resolution
        first 24 hours with fixed load : SHOP finds the optimal load distribution, no market exchange(no sale/buy option allowed)
        last  24 hours with 99MW sale/buy openings, SHOP finds the most profitable solution that also fulfils(almost) the obligations

        Demonstrates
        * Building a multi area system from scratch, pure-python
        * Running SHOP directly (you can also have the dstm service doing the same!)
        * Market access for unit groups to NO1 and NO2, and unit-group membership that changes over the period

        Skeleton sketch for the hydro power system in each of the areas.

             Reservoir ( 90..100 masl, 0..50 Mm3, X eur/MWh fixed endpoint water-value)
        f  /   |   tunnell
        l |   /|\\   penstocks
        o |  U U U   units 10..80MW
        o |  \\|/  outlet
        d \\   |
             |   downstream river

    """
    run_ta = TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 48)
    sys = create_multi_area_system(run_ta, 1, 'sys1', n_units=3)
    run_id = 123
    opts = create_optimization_commands(run_id, write_files=False)
    shop_sys = shop.ShopSystem(run_ta)
    shop_sys.set_logging_to_stdstreams(False)  # Set this to true if you want to have a look at shop-printouts during optimization
    shop_sys.emit(sys)
    for opt in opts:
        shop_sys.commander.execute(opt)
    shop_sys.collect(sys)
    shop_sys.complete(sys)
    present_demo_results(run_ta, sys)
    # Please note that the expected results below are just valid for the current example and shop-version
    # If we get new shop-versions, or changes internal logic, it will be captured in these asserts,
    # then we would need to do a *manual validation*, adjust expectation (or fix a bug)
    ug_mw = [
        [
            153.7, 193.3, 192.9, 192.5, 192.2, 191.8, 191.5, 191.1, 153.8, 153.8, 153.9, 153.9, 153.9, 154.0, 154.0, 154.0, 154.0, 188.0, 187.6, 187.3, 186.9, 186.6, 186.2, 185.8, 159.6, 159.5, 150.2, 128.0, 120.0,
            127.5, 149.4, 157.2, 155.2, 154.5, 153.8, 153.1, 152.4, 151.7, 151.0, 150.4, 149.7, 149.0, 148.3, 128.1, 120.0, 127.4, 145.8, 145.0
        ]
        ,
        [
            210.0, 213.8, 217.1, 219.2, 220.0, 219.2, 217.1, 213.8, 210.0, 206.2, 202.9, 200.8, 200.0, 200.8, 202.9, 206.1, 210.0, 213.8, 217.0, 219.2, 220.0, 219.3, 217.1, 213.9,
            37.0, 36.2, 35.6, 35.2, 35.0, 35.1, 35.6, 36.2, 37.0, 37.8, 38.4, 38.8, 39.0, 38.9, 38.4, 37.8, 37.0, 36.2, 35.6, 35.2, 35.0, 35.1, 35.6, 36.2
        ]
    ]

    for expected_mw, ema in zip(ug_mw, sys.market_areas):
        assert np.allclose(expected_mw, (ema.unit_group.production.value/1e6).values, atol=1.0), "Regression values for the current shop version, validate and adjust if needed for newer version/configurations"
