from shyft.time_series import time, deltahours, Calendar, TimeAxis, POINT_AVERAGE_VALUE, TimeSeries
from shyft.energy_market.core import Point
from os import path, remove, getpid

import pytest
shop = pytest.importorskip("shyft.energy_market.stm.shop")
if not shop.shyft_with_shop:
    pytest.skip('Skip shop-releated test for non-shop build', allow_module_level=True)


def test_stm_shop_best_profit(system_to_optimize):
    """ Verify best profit creation by shop optimization """
    mega = 1000000
    plant = system_to_optimize.hydro_power_systems[0].power_plants[0]
    agg = plant.units[0]

    utc = Calendar()
    t_begin = time('2018-10-17T10:00:00Z')
    t_end = time('2018-10-18T10:00:00Z')
    t_step = deltahours(1)
    n_steps = (t_end - t_begin)/t_step
    ta = TimeAxis(t_begin.seconds, t_step.seconds, n_steps.seconds)

    # Optimize without print_bp_curves command
    # and verify that best_profit results are not produced
    commands = shop.ShopCommandList()
    commands.extend([
        shop.ShopCommand.set_method_primal(),
        shop.ShopCommand.set_code_full(),
        shop.ShopCommand.start_sim(1),
        shop.ShopCommand.set_code_incremental(),
        shop.ShopCommand.start_sim(1)
    ])
    shop_system = shop.ShopSystem(ta)
    shop_system.emit(system_to_optimize)
    for command in commands:
        shop_system.commander.execute(command)
    shop_system.collect(system_to_optimize)
    assert False == path.exists(f"bp_{plant.name}.xml")
    assert False == path.exists(f"mc_{plant.name}.xml")
    assert plant.best_profit.discharge.exists
    assert len(plant.best_profit.discharge.value) == 0
    assert plant.best_profit.cost_average.exists
    assert len(plant.best_profit.cost_average.value) == 0
    assert plant.best_profit.cost_marginal.exists
    assert len(plant.best_profit.cost_marginal.value) == 0
    assert plant.best_profit.cost_commitment.exists
    assert len(plant.best_profit.cost_commitment.value) == 0
    assert agg.best_profit.discharge.exists
    assert len(agg.best_profit.discharge.value) == 0
    assert agg.best_profit.production.exists
    assert len(agg.best_profit.production.value) == 0
    assert agg.best_profit.discharge_production_ratio.exists
    assert len(agg.best_profit.discharge_production_ratio.value) == 0
    assert agg.best_profit.operating_zone.exists
    assert len(agg.best_profit.operating_zone.value) == 0

    # Execute print_bp_curves command and collect results again
    # and verify that best_profit discharge results are now included
    # for the first time step (since we didn't specify start/end timestep
    # arguments to the command).
    shop_system.commander.print_bp_curves()
    shop_system.collect(system_to_optimize)
    # Verify that files bp_plant.xml and mc_plant.xml are produced in the
    # working directory, but make sure to delete them both first to avoid
    # breaking additional test runs.
    # TODO: We really would like the files to not be produced, we only
    # want Shop to fill the API attributes, but currently we can only
    # get all or nothing.
    bp_file_was_created = path.exists(f"bp_{plant.name}.xml")
    if bp_file_was_created:
        remove(f"bp_{plant.name}.xml")
    mc_file_was_created = path.exists(f"mc_{plant.name}.xml")
    if mc_file_was_created:
        remove(f"mc_{plant.name}.xml")
    assert True == bp_file_was_created
    assert True == mc_file_was_created
    # Verify result attributes, we should have 1 best_profit curve representing
    # the start of time axis, since we did not specify anything else with
    # starttime/endtime arguments to print_bp_curves command.
    assert len(plant.best_profit.discharge.value) == 1
    assert len(plant.best_profit.cost_average.value) == 1
    assert len(plant.best_profit.cost_marginal.value) == 1
    assert len(plant.best_profit.cost_commitment.value) == 1
    assert len(agg.best_profit.discharge.value) == 1
    assert len(agg.best_profit.production.value) == 1
    assert len(agg.best_profit.discharge_production_ratio.value) == 1
    assert len(agg.best_profit.operating_zone.value) == 1
    # Take a closer look at agg.best_profit.discharge curve
    # On a manual run there was 101 points, including these points:
    #   Point(0.000000, 0.000000)
    #   Point(19999995.818144, 33.016775)
    #   ...
    #   Point(75727939.862086, 109.849088)
    bp_t = next(i.key() for i in agg.best_profit.discharge.value)
    assert bp_t == t_begin
    bp_xy = agg.best_profit.discharge.value[bp_t]
    assert len(bp_xy.points) == 101
    t0 = next(i.key() for i in agg.generator_description.value)
    assert bp_xy.points[0] == Point(0.0, 0.0)
    assert bp_xy.points[1].x == pytest.approx(20*mega)
    assert bp_xy.points[1].x == pytest.approx(agg.generator_description.value[t0].points[0].x)
    assert bp_xy.points[-1].x == pytest.approx(76*mega, rel=1e-2)
    assert bp_xy.points[-1].y == pytest.approx(agg.turbine_description.value[t0].operating_zones[0].efficiency_curves[0].xy_point_curve.points[-1].x, rel=1e-2)


def test_stm_shop_best_profit_with_reference_production(system_to_optimize):
    """
    Verify best profit creation by shop optimization when setting prod schedule
    which should be considered as the reference production for the best profit calculations.
    """
    mega = 1000000
    plant = system_to_optimize.hydro_power_systems[0].power_plants[0]
    agg = plant.units[0]

    utc = Calendar()
    t_begin = time('2018-10-17T10:00:00Z')
    t_end = time('2018-10-18T10:00:00Z')
    t_step = deltahours(1)
    n_steps = (t_end - t_begin)/t_step
    ta = TimeAxis(t_begin.seconds, t_step.seconds, n_steps.seconds)

    # Unit production schedule (or realised) will be used as reference production
    agg.production.schedule.value = TimeSeries(ta, fill_value=60*mega, point_fx=POINT_AVERAGE_VALUE)
    commands = shop.ShopCommandList()
    commands.extend([
        shop.ShopCommand.set_method_primal(),
        shop.ShopCommand.set_code_full(),
        shop.ShopCommand.start_sim(1),
        shop.ShopCommand.set_code_incremental(),
        shop.ShopCommand.start_sim(1),
        shop.ShopCommand.set_prod_from_ref_prod(),
        shop.ShopCommand.print_bp_curves()
        #shop.ShopCommand("print bp_curves 0 0")
    ])
    shop_system = shop.ShopSystem(ta)
    shop_system.emit(system_to_optimize)
    for command in commands:
        shop_system.commander.execute(command)
    shop_system.collect(system_to_optimize)

    # Verify that files bp_plant.xml and mc_plant.xml are produced in the
    # working directory, but make sure to delete them both first to avoid
    # breaking additional test runs.
    # TODO: We really would like the files to not be produced, we only
    # want Shop to fill the API attributes, but currently we can only
    # get all or nothing.
    bp_file_was_created = path.exists(f"bp_{plant.name}.xml")
    if bp_file_was_created:
        remove(f"bp_{plant.name}.xml")
    mc_file_was_created = path.exists(f"mc_{plant.name}.xml")
    if mc_file_was_created:
        remove(f"mc_{plant.name}.xml")
    assert True == bp_file_was_created
    assert True == mc_file_was_created
    # Verify result attributes, we should have 1 best_profit curve representing the start of time axis
    assert len(plant.best_profit.discharge.value) == 1
    assert len(plant.best_profit.cost_average.value) == 1
    assert len(plant.best_profit.cost_marginal.value) == 1
    assert len(plant.best_profit.cost_commitment.value) == 1
    assert len(agg.best_profit.discharge.value) == 1
    assert len(agg.best_profit.production.value) == 1
    assert len(agg.best_profit.discharge_production_ratio.value) == 1
    assert len(agg.best_profit.operating_zone.value) == 1
    # Take a closer look at agg.best_profit.discharge
    # Should be only one point, matching the scheduled production.
    # On a manual run the point was:
    #   Point(60000000.000000, 82.546937)
    bp_t = next(i.key() for i in agg.best_profit.discharge.value)
    assert bp_t == t_begin
    bp_xy = agg.best_profit.discharge.value[bp_t]
    print(bp_xy)
    assert len(bp_xy.points) == 1
    t0 = next(i.key() for i in agg.generator_description.value)
    assert bp_xy.points[0].x == 60*mega
    assert bp_xy.points[0].y == pytest.approx(82.546937)
