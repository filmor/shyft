import pytest
shop = pytest.importorskip("shyft.energy_market.stm.shop")
if not shop.shyft_with_shop:
    pytest.skip('Skip shop-releated test for non-shop build', allow_module_level=True)


from shyft.energy_market.stm import StmSystem, MarketArea
from shyft.energy_market.stm import shop
import shyft.time_series as sts
from .test_multi_area_mfrr import create_simple_hps

def test_best_profit_calc():
    """
    Test case from Ole Andreas Ramsdal,
    Ref. https://gitlab.com/shyft-os/shyft/-/issues/982,
    note: If we adjust start hour to 0, then timestamps are correctly interpreted.
          Any other value offsets the value by hours selected.
          Seems to be a feature of shop 15.2.1.0
    """
    dt = 15*sts.Calendar.MINUTE
    ta = sts.TimeAxis(sts.time("2023-01-01T00:00:00Z"), dt, 4*24*2)
    hps = create_simple_hps(123)
    stm_sys = StmSystem(1, "sys", "")
    stm_sys.hydro_power_systems.append(hps)
    ma = MarketArea(1, "ma", "", stm_sys)
    stm_sys.market_areas.append(ma)

    ma.max_sale.value = sts.TimeSeries(ta, 9999e6, sts.POINT_AVERAGE_VALUE)
    ma.max_buy.value = sts.TimeSeries(ta, 9999e6, sts.POINT_AVERAGE_VALUE)
    ma.price.value = sts.TimeSeries(ta, 40e-6, sts.POINT_AVERAGE_VALUE)

    r = hps.reservoirs[0]
    r.level.realised = sts.TimeSeries(ta.slice(0, 1), 244, sts.POINT_AVERAGE_VALUE)
    r.water_value.endpoint_desc.value = sts.TimeSeries(ta, 42e-6, sts.POINT_AVERAGE_VALUE)

    u = hps.units[0]
    u.production.schedule.value = sts.TimeSeries(ta, 50e6, sts.POINT_AVERAGE_VALUE)

    bp_calc_rel_start_hour = 0  # Notice: shop 15.2.1.0 subtracts the bp start_hour from epoch time, giving wrong timestamps on return
    bp_calc_rel_end_hour = bp_calc_rel_start_hour + 12
    cmds = shop.ShopCommandList([
        shop.ShopCommand.set_code_incremental(),
        shop.ShopCommand.start_sim(1),
        shop.ShopCommand(f"print bp_curves {bp_calc_rel_start_hour} {bp_calc_rel_end_hour}"),
    ])

    shop_sys = shop.ShopSystem(ta)
    shop_sys.set_logging_to_stdstreams(False)
    shop_sys.set_logging_to_files(False)
    shop_sys.emit(stm_sys)
    for cmd in cmds:
        shop_sys.commander.execute(cmd)
    shop_sys.collect(stm_sys)
    shop_sys.complete(stm_sys)

    t0_bp_data = next(iter(u.power_plant.best_profit.cost_average.value)).key()
    assert t0_bp_data == ta(0).start + bp_calc_rel_start_hour*sts.Calendar.HOUR
