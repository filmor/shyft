import gc
from shyft.energy_market.core import Model

del_count: int = 0


class FancyPyObject:

    def __init__(self):
        self.content = ["a", "b", "c"]

    def __del__(self):
        global del_count
        del_count = del_count + 1


def test_can_manage_py_objects():
    def local_x():
        m = Model(1, 'm')
        a1 = m.create_model_area(1, 'a1')
        pm1 = a1.create_power_module(1, "solar")

        pm1.obj = FancyPyObject()
        del pm1
        del a1
        del m
        gc.collect()

    local_x()
    gc.collect()
    global del_count
    assert del_count == 1
    pass
